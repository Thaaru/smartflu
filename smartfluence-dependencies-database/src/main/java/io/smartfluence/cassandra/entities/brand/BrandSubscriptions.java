package io.smartfluence.cassandra.entities.brand;

import java.util.Date;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandSubscriptionsKey;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.constants.ValidityType;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_subscriptions")
public class BrandSubscriptions implements Copyable {

	private static final long serialVersionUID = 3686560342692157753L;

	public BrandSubscriptions(BrandSubscriptions subscriptions) {
		this.key = new BrandSubscriptionsKey(subscriptions.getKey());
		this.subscriptionName = subscriptions.getSubscriptionName();
		this.subscriptionType = subscriptions.getSubscriptionType();
		this.credits = subscriptions.getCredits();
		this.validity = subscriptions.getValidity();
		this.validityType = subscriptions.getValidityType();
		this.subscriptionDate = subscriptions.getSubscriptionDate();
		this.resetCredit = subscriptions.isResetCredit();
		this.renewable = subscriptions.isRenewable();
		this.modifiedAt = subscriptions.getModifiedAt();
		this.createdAt = subscriptions.getCreatedAt();
		this.brandName = subscriptions.getBrandName();
	}

	@PrimaryKey
	private BrandSubscriptionsKey key;

	@Column("brand_name")
	private String brandName;

	@Column("subscription_name")
	private String subscriptionName;

	@Column("subscription_type")
	private SubscriptionType subscriptionType;

	@Column("credits")
	private Integer credits;

	@Column("validity")
	private Integer validity;

	@Column("validity_type")
	private ValidityType validityType;

	@Column("subscription_date")
	private Date subscriptionDate;

	@Column("reset_credit")
	private boolean resetCredit;

	@Column("renewable")
	private boolean renewable;

	@LastModifiedDate
	@Column("modified_at")
	private Date modifiedAt;

	@Column("created_at")
	private Date createdAt;
}