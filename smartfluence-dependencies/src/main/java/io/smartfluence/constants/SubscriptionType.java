package io.smartfluence.constants;

public enum SubscriptionType {
	PROMOTIONAL, FREE, STRIPE, PERMANENT, PREMIUM
}