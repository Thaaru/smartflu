(function($) {



    var _search = function(term, value, option) {

    };
    var _onSearch = function(term, match) {

    };
    var _onSelect = function(value) {

    };
    var _onUnselect = function() {

    };
    var _onSelectAll = function(values) {

    };
    var _onUnSelectAll = function() {

    };




   
    $.fn.mSelect = function(options) {
        this.each(function() {
            var selectObj = $(this);
            var settings = $.extend($.fn.mSelect.defaults, options);
            /* Creating master container */
            selectObj.wrap('<div class="m-select-container"/>')
                .parent()
                .wrapInner('<div class="m-select-hidden"/>');

            /* creating input container */
            selectObj.closest('.m-select-container')
                .prepend('<div class="m-select-wrapper"/>');

            let selectWrapper = $(this).closest('.m-select-container')
                .find('.m-select-wrapper');

            /* Creating input box */
            selectWrapper
                .append(`<div class="m-select-input-container"><input readonly type="text" id="form1" class="m-select-input " value="${$.fn.mSelect.defaults.selectPlaceHolder}">` +
                    `<label for="form1" class="m-select-label">${$.fn.mSelect.defaults.selectLabel}</label><span class="caret" style="position: absolute;top: 0;right: 3%;"><i class="fas fa-caret-down fa-xs"></i></span></div>`);
            let inputWrapper = selectWrapper.find('.m-select-input');


            /* Creating input dropdown */
            selectWrapper.append('<div class="m-select-options-container"><div class="m-select-list-container"><ul class="m-select-list"></ul></div></div>');
            let optionsWrapper = selectWrapper.find('.m-select-options-container');
            let listWrapper = selectWrapper.find('.m-select-list-container');
            let optionList = listWrapper.find('.m-select-list');
            if ($.fn.mSelect.defaults.enableSearch) {
            	//TODO: search
            }
            let listObj = selectWrapper.find('ul');
            buildList(selectObj,inputWrapper,listObj,optionsWrapper,optionList);
            
            this.selectObj=selectObj;
            this.inputWrapper=inputWrapper;
            this.optionsWrapper=optionsWrapper;
            this.listObj=listObj;
            this.optionList=optionList;
            
            this.select=function(val){
            	if(val==''){
            		return;
            	}
            	if(typeof val == 'string'){
            		var check=this.listObj.find(`input.m-select-item[value="${val}"]`);
                	if(!check.prop('checked')){
                		check.next().click();
                	}
            	}else{
            		for(var v in val){
            			var check=this.listObj.find(`input.m-select-item[value="${val[v]}"]`);
                    	if(!check.prop('checked')){
                    		check.next().click();
                    	}
            		}
            	}
            	
            };
            this.unselect=function(val){
            	if(val==''){
            		return;
            	}
            	if(typeof val == 'string'){
            		var check=this.listObj.find(`input.m-select-item[value="${val}"]`);
                	if(check.prop('checked')){
                		check.next().click();
                	}
            	}else{
            		for(var v in val){
            			var check=this.listObj.find(`input.m-select-item[value="${val[v]}"]`);
                    	if(check.prop('checked')){
                    		check.next().click();
                    	}
            		}
            	}
            	
            }



        });
        this.rebuild=function(){
        	this.each(function(){
        		buildList(this.selectObj,this.inputWrapper,this.listObj,this.optionsWrapper,this.optionList);
        	})
        }
        
        function buildList(selectObj,inputWrapper,listObj,optionsWrapper,optionList){
        	listObj.html('');
        	/*listObj.append('<span class="cur-pointer d-flex justify-content-center m-select-close p-2 text-danger"><i style="font-size: 1.4em;" class=" far fa-times-circle"></i></span>');*/
        	if (selectObj.find('option').length > 0) {

                listObj.append(`<li><div class="form-check"><input class="form-check-input m-select-all" type="checkbox"><label class="form-check-label  m-select-all-label ${selectObj[0].id}">${$.fn.mSelect.defaults.selectAllLabel}</label></li>`)
                selectObj.find('option').each(function() {
                    console.log(this);
                    var outerHTML = this.outerHTML;
                    if (outerHTML.match(/(disabled|hidden)/gi) != null) {
                        return;
                    }
                    outerHTML = outerHTML.replace(/<option\s?(\s.+)*>(.*)<\/option>/gi, function(term, g1, g2, g3, g4, g5) {
                        return `<li><div class="form-check"><input class="form-check-input m-select-item" type="checkbox" ${g1.replace(/(\w+=)/gi,function(termInner,g1Inner){
                            
                            if(g1Inner.toLowerCase()=='class='||g1Inner.toLowerCase()=='value='){
                                return g1Inner;
                            }else if(g1Inner.toLowerCase()=='selected='){
                                return 'checked=';
                            }else{
                                return 'data-'+g1Inner;
                            }
                        })}><label title="${g2}" class="form-check-label m-select-item-label" >${g2}</label></li>`;
                    });
                    listObj
                        .append(outerHTML);
                });
            } else {
                listObj.append('<li><span class="text-muted">No options available</span></li>')
            }
            


            /* Event */
            inputWrapper.on('click', function(e) {
                $('.m-select-options-container').removeClass('active');
               
                optionsWrapper.addClass('active');
                e.stopPropagation();
            });
            listObj.on('click', '.m-select-close', function() {
                optionsWrapper.removeClass('active');
            });
            listObj.children().on('click', '.m-select-item-label', function(event) {
                var input=$(this).parent().find('input')
                $(input).click();
                

                var checked = $(input).prop('checked');
                if (checked) {
                	selectObj.find(`option[value="${input.val()}"]`).attr('selected','selected');
                    $.fn.mSelect.defaults.onSelect($(input).val());
                } else {
                	selectObj.find(`option[value="${input.val()}"]`).removeAttr('selected');
                    $.fn.mSelect.defaults.onUnselect($(input).val());
                }
                selectObj.change();
                var allChecked = true;
                var nonChecked=true;
                var selectedNumber=0;
                optionList.find('.m-select-item').each(function() {
                    if (!$(this).prop('checked')) {
                        allChecked = false;
                    }else{
                    	selectedNumber++;
                    	nonChecked=false;
                    }
                })
                var label;
                if(nonChecked){
                	label=$.fn.mSelect.defaults.selectPlaceHolder;
                }else if(allChecked){
                	label=`All selected (${selectedNumber})`;
                }else{
                	label=`Some selected (${selectedNumber})`;
                }
                inputWrapper.val(label);
                listObj.find('.m-select-all').prop('checked', allChecked);


            }).on('click', '.m-select-all-label', function(event) {
                var input=$(this).parent().find('input')
                $(input).click();
                var checked = $(input).prop('checked');
                var values=[];
                var selectedNumber=0;
                optionList.find('.m-select-item').each(function() {
                	selectedNumber++;
                    $(this).prop('checked',checked);
                    values.push($(input).val());
                });
                var label;
                
                if(checked){
                	selectObj.find(`option`).attr('selected','selected');
                    $.fn.mSelect.defaults.onSelectAll(values);
                    label=`All selected (${selectedNumber})`;
                }else{
                	selectObj.find(`option`).removeAttr('selected');
                    $.fn.mSelect.defaults.onUnSelectAll();
                    label=$.fn.mSelect.defaults.selectPlaceHolder;
                }
                inputWrapper.val(label);
                selectObj.change();

            });
        }
        return this;

    };
    $.fn.mSelect.defaults = {
        enableSearch: true,
        enableChecbox: true,
        searchText: 'Search for...',
        search: _search,
        onSearch: _onSearch,
        selectLabel: '',
        selectPlaceHolder:'Select..',
        selectAllLabel: 'Select All',
        onSelect: _onSelect,
        onUnselect: _onUnselect,
        onSelectAll: _onSelectAll,
        onUnSelectAll: _onUnSelectAll
    };


}(jQuery));