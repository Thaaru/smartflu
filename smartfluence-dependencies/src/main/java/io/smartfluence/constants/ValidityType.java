package io.smartfluence.constants;

public enum ValidityType {
	MINUTE, HOUR, DAY, MONTH, YEAR
}