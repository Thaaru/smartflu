package io.smartfluence.modal.brand.campaignv1;

import lombok.Data;


@Data
public class InfluOfferObj {
    private String campaignId;
    private String influencerId;
    private OfferTypeEntityV1 offerType;
    private Double paymentOffer;
    private Double commissionValue;
    private String commissionValueType;
    private String commissionAffiliateType;
    private String commissionAffiliateCustomName;
    private String commissionAffiliateDetails;
    private Integer commissionValueTypeId;
    private Integer commissionAffiliateTypeId;
}
