package io.smartfluence.constants;

public enum BidStatus {
	NEW, BIDSENT, EMAILFOLLOWUP, PAIDINFLUENCER, POSTED, NOTACTIVE
}