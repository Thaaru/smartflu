package io.smartfluence.cassandra.entities.brand.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import io.smartfluence.constants.Platforms;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class BrandBidDetailsKey implements Serializable {

	private static final long serialVersionUID = 8089142559032984619L;

	public BrandBidDetailsKey(BrandBidDetailsKey key) {
		this.brandId = key.getBrandId();
		this.campaignId = key.getCampaignId();
		this.platform = key.getPlatform();
		this.influencerId = key.getInfluencerId();
		this.bidId = key.getBidId();
	}

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.PARTITIONED)
	private UUID brandId;

	@PrimaryKeyColumn(name = "campaign_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID campaignId;

	@PrimaryKeyColumn(name = "influencer_id", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private UUID influencerId;

	@PrimaryKeyColumn(name = "bid_id", type = PrimaryKeyType.CLUSTERED, ordinal = 3)
	private UUID bidId;

	@PrimaryKeyColumn(name = "platform", type = PrimaryKeyType.CLUSTERED, ordinal = 4)
	private Platforms platform;
}