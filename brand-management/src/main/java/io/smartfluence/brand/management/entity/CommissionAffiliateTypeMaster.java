package io.smartfluence.brand.management.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "commission_affilate_type_master")
public class CommissionAffiliateTypeMaster implements Serializable {
    @Id
    @Column(name = "comm_affi_type_id")
    private int commAffiTypeId;

    @Column(name = "comm_affi_name")
    private String commAffiName;

}
