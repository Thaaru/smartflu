export const SUCCESS_CODE = "600";
export const NODATA = "615";
export const ERROR = "604";
export const HOMEPATH = "home";
export const  PROPOSAL_STATUS = {
    INVITE_SENT : "INVITE_SENT",
    INFLUENCER_ACCEPTED : "INFLUENCER_ACCEPTED"
}
export const STEPS_ACCEPTED_STATUS = "True";
export const ACCEPTED_MESSAGE = "Accepted Successfully";
export const ERROR_MESSAGE = "Sorry, Something went wrong. Refresh the page or try again later.";
export const DETAILS_ACCEPTED_MESSAGE = "Details updated successfully.";
export const SP_ARROWBUTTON_MESSAGE = "Warning! Your changes won't be saved if you exit this screen now.";
export const LOGIN_ERROR_MESSAGE = "Sorry, You must create an account before logging in.";
export const CREATED_ACCOUNT_MESSAGE = "Your new account has been successfully created. Please check the confirmation mail sent to you to proceed further.";
export const PASSWORD_SET_MESSAGE = "Please set the password by confirmation email sent from smartfluence.";
export const ACCOUNT_STATUS = "Active";
export const PASSWORD_STATUS = "Active";
export const SMARTFLUENCE_LOGO = "../../static/img/smartfluence_logo.png";
export const SMARTFLUENCE_NAME_LOGO = "../../static/img/new-logo-transparent.png";
export const PRIMARY_COLOR = "#2F008D";
export const SECONDARY_COLOR = "#C4C4C4";
export const GREEN_COLOR = "#49B52E";
export const BBB_COLOR = "#bbb";
export const GREY_COLOR = "grey";
export const BLACK_COLOR = "#black";
export const INSTAGRAM = "Instagram";
export const TIKTOK = "Tiktok";
export const YOUTUBE = "Youtube";
export const SUBMIT_PROPOSAL_BUTTON = "Submit Proposal";


export function getStatus(status){

    switch (status){

        case "BRAND_REJECTED":

            return "Rejected by brand";

        case "BRAND_ACCEPTED":

            return "Accepted by brand";

        case "NOT_PAID":

            return "Payment pending";

        case "PAID":

            return "Payment done";

        case "INFLUENCER_REJECTED":

            return "Declined by influencer";

        case "INVITE_SENT":

            return "Invitation sent";

        case "INFLUENCER_ACCEPTED":
            return "Accepted by influencer";

        case "PRODUCT_FULFILLED":
            return "Product fulfilled";

        case "TRACK_POST":
            return "Track posted";
            
        case "INVITE_NOT_SENT":
            return "Invitation not sent";

    }

}