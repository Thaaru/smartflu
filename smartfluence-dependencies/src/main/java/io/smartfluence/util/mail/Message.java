package io.smartfluence.util.mail;

class Message extends AbstractMailContent {

	private static final String SECTION_PREFIX = "<tr style=\"background-color: #efefef;\"><td style=\"padding: 20px; font-size: 12px; line-height: 17px;\">";

	private static final String SECTION_SUFFIX = "<p style=\"margin: 0; font-family: Helvetica, Arial, sans-serif;color: #61696d; padding-bottom: 0px; padding-top: 10px;\">Need help? Email us at help@smartfluence.io and we'll get back to you!</p></td></tr>";

	private static final String ITEM_PREFIX = "<p style=\"padding-bottom: 10px; margin: 0; font-family: Helvetica, Arial, sans-serif;\">";

	private static final String ITEM_SUFFIX = "</p>";

	@Override
	protected String getItemPrefix() {
		return ITEM_PREFIX;
	}

	@Override
	protected String getItemSuffix() {
		return ITEM_SUFFIX;
	}

	public Message(SmartMailBuilder smartMailBuilder) {
		this.setBuilder(smartMailBuilder);
	}

	@Override
	public StringBuilder content() {
		String content= super.content().toString();
		super.content().setLength(0);
		return super.content().append(SECTION_PREFIX).append(content).append(SECTION_SUFFIX);
	}
}