package io.smartfluence.influencer.services;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import io.smartfluence.influencer.ResourceServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@Slf4j
public class StorageService {

    @Value("${application.bucket.name}")
    private String bucketName;

    @Autowired
    private AmazonS3 s3Client;

    final static Logger LOG = Logger.getLogger(StorageService.class.getName());

    public String getFileKeyName(String key,String filename){
        return "campaigns/"+ ResourceServer.getUserId().toString()+"/"+key+"/"+filename;
    }


    public String uploadFile(MultipartFile file,String filename) {
        File fileObj = convertMultiPartFileToFile(file);
        PutObjectResult putObjectResult = s3Client.putObject(new PutObjectRequest(bucketName,getFileKeyName(filename,file.getOriginalFilename()), fileObj));
        ObjectMetadata objectMetadata = new ObjectMetadata();
        Map fileNameObj=new HashMap<String,String>();
        fileNameObj.put("filename",file.getOriginalFilename());
        objectMetadata.setUserMetadata(fileNameObj);
        putObjectResult.setMetadata(objectMetadata);
        URL smartfluence = s3Client.getUrl("smartfluence", filename);
        System.out.println(smartfluence.getPath());
        fileObj.delete();
        System.out.println(putObjectResult);
        return getFileKeyName(filename,file.getOriginalFilename());
    }


    public byte[] downloadFile(String fileName) {
        S3Object s3Object = s3Client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            byte[] content = IOUtils.toByteArray(inputStream);
            String url = generatePresignedUrl(fileName, HttpMethod.GET);
            System.out.println("download file"+url);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String deleteFile(String fileName) {
        s3Client.deleteObject(bucketName, fileName);
        return fileName + " removed ...";
    }


    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Error converting multipartFile to file", e);
        }
        return convertedFile;
    }

    public String generatePresignedUrl
            (String fileName, HttpMethod method) {
        try {
            // Set the pre-signed URL to expire after 10 mins.
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * 30;
            expiration.setTime(expTimeMillis);

            // Generate the pre-signed URL
            GeneratePresignedUrlRequest generatePresignedUrlRequest =
                    new GeneratePresignedUrlRequest(bucketName, fileName)
                            .withMethod(method)
                            .withExpiration(expiration);
            URL url = s3Client.generatePresignedUrl(
                    generatePresignedUrlRequest);
            LOG.log(Level.INFO, "pre-signed URL for " + method.toString()
                    + " operation has been generated.");
            return url.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOG.log(Level.SEVERE, "URL could not be generated");
        return null;

    }

    public String getFileName(String campaignId){
      return   s3Client.getObject(bucketName,campaignId).getObjectMetadata().getUserMetaDataOf("filename");
    }
}
