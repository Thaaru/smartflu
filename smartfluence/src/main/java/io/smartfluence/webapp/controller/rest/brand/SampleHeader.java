package io.smartfluence.webapp.controller.rest.brand;

import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("brand")
public class SampleHeader {

    @GetMapping("/listHeaders")
    public ResponseEntity<Map<String, String>> listAllHeaders(
      @RequestHeader Map<String, String> headers) {
        headers.forEach((key, value) -> {
            System.out.println(String.format("Header '%s' = %s", key, value));
        });
        return new ResponseEntity<Map<String, String>>(headers, HttpStatus.OK);
    }
}

	