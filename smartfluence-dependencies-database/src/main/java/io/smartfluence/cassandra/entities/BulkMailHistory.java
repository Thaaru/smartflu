package io.smartfluence.cassandra.entities;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.BulkMailHistoryKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("bulk_mail_history")
public class BulkMailHistory implements Serializable {

	private static final long serialVersionUID = -4347016388583847037L;

	@PrimaryKey
	private BulkMailHistoryKey key;

	@Column("influencers_count")
	private Integer influencersCount;

	@Indexed
	@Column("created_date")
	private Date createdAt;
}