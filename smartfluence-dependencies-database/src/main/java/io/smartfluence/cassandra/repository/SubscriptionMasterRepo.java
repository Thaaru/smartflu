package io.smartfluence.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.primary.SubscriptionMasterKey;

@Repository
public interface SubscriptionMasterRepo extends CassandraRepository<SubscriptionMaster, SubscriptionMasterKey> {

}