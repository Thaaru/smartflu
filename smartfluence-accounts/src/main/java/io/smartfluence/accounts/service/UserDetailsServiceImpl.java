package io.smartfluence.accounts.service;

import static io.smartfluence.constants.SecurityConstants.ADMIN;
import static io.smartfluence.constants.SecurityConstants.BRAND;
import static io.smartfluence.constants.SecurityConstants.BRAND_ENTERPRISE;
import static io.smartfluence.constants.SecurityConstants.INFLUENCER;
import static io.smartfluence.constants.SecurityConstants.SMART_ADMIN;
import static io.smartfluence.constants.SecurityConstants.BRAND_APPROVED;
import static io.smartfluence.constants.SecurityConstants.BRAND_PENDING;
import static io.smartfluence.constants.SecurityConstants.BRAND_PREMIUM;
import static io.smartfluence.constants.SecurityConstants.BRAND_FREE;
import static io.smartfluence.constants.SecurityConstants.BRAND_DECLINED;
import static io.smartfluence.constants.SecurityConstants.BRAND_INACTIVE;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.UserAccountsRepo;
import lombok.Getter;
import lombok.ToString;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Override
	@Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRED)
	public UserDetails loadUserByUsername(String username) {
		UserAccounts userAccounts = userAccountsRepo.findByEmail(username);

		if (userAccounts == null)
			throw new BadCredentialsException("Invalid credentials");
		if (userAccounts.getUserStatus().equals(UserStatus.REGISTERED))
			throw new BadCredentialsException("User is not verified");

		return new User(userAccounts.getEmail(), userAccounts.getPassword(),
				!userAccounts.getUserStatus().equals(UserStatus.REGISTERED),
				authorities(userAccounts, brandDetailsRepo.findById(new UserDetailsKey(userAccounts.getUserStatus()
						, UUID.fromString(userAccounts.getUserId()))).get()), userAccounts);
	}

	public static List<GrantedAuthority> authorities(UserAccounts userAccounts, BrandDetails brandDetails) {
		List<GrantedAuthority> authorities = new LinkedList<>();
		switch (userAccounts.getUserType()) {
		case SMART_ADMIN:
			authorities.add(SMART_ADMIN);
			break;
		case ADMIN:
			authorities.add(ADMIN);
			break;
		case BRAND:
			authorities.add(BRAND);
			authorities.add(BRAND_ENTERPRISE);

			if (userAccounts.getUserStatus().equals(UserStatus.APPROVED)) {
				authorities.add(BRAND_APPROVED);

				if (brandDetails.getSubscriptionType().equals(SubscriptionType.FREE))
					authorities.add(BRAND_FREE);
				if (brandDetails.getSubscriptionType().equals(SubscriptionType.PREMIUM))
					authorities.add(BRAND_PREMIUM);
			}
			else if (userAccounts.getUserStatus().equals(UserStatus.PENDING))
				authorities.add(BRAND_PENDING);
			else if (userAccounts.getUserStatus().equals(UserStatus.DECLINED))
				authorities.add(BRAND_DECLINED);
			else if (userAccounts.getUserStatus().equals(UserStatus.INACTIVE))
				authorities.add(BRAND_INACTIVE);
			break;
		case INFLUENCER:
			authorities.add(INFLUENCER);
			break;
		default:
			break;
		}
		return authorities;
	}

	@Getter
	@ToString
	public static class User extends org.springframework.security.core.userdetails.User {
		private static final long serialVersionUID = -8192492179384079634L;
		private Object userId;

		public User(String username, String password, boolean enabled,
				Collection<? extends GrantedAuthority> authorities, UserAccounts userAccounts) {
			super(username, password, enabled, true, true, true, authorities);
			userId = userAccounts.getUserId();
		}
	}
}