package io.smartfluence.util.image;

import java.awt.image.BufferedImage;

interface ImageFilter {

	BufferedImage processImageBuffer(BufferedImage bufferedImage);
}