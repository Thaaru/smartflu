package io.smartfluence.modal.validation.brand.dashboard;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MailTemplateModal {

	@NotBlank(message = "Enter campaign name")
	private String templateName;

	private String templateId;

	@NotBlank(message = "Enter Subject")
	private String templateSubject;

	@NotBlank(message = "Enter Mail Body")
	private String templateBody;

	private boolean isNew;

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean isNew) {
		this.isNew = isNew;
	}
}