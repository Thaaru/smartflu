package io.smartfluence;

//import org.apache.solr.client.solrj.SolrClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.data.solr.core.SolrTemplate;
//import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import io.smartfluence.beans.WebContainer;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@RefreshScope
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
@EntityScan({ "io.smartfluence.mysql.entities", "io.smartfluence.cassandra.entities",
		"io.smartfluence.brand.management.solr.entity","io.smartfluence.brand.management.entity",
		"io.smartfluence.brand.management.entityv1"
})
@EnableJpaRepositories({"io.smartfluence.mysql.repository","io.smartfluence.brand.management.repository",
"io.smartfluence.brand.management.repositoryv1"})
@EnableCassandraRepositories("io.smartfluence.cassandra.repository")
//@EnableSolrRepositories("io.smartfluence.brand.management.solr.repo")

public class BrandManagementApplication {


	public static void main(String[] args) {
		SpringApplication.run(BrandManagementApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(prefix="smartfluence",name="use-ssl")
	public WebContainer webContainer() {
		return new WebContainer();
	}

//	@Bean
//	public MultipartResolver multipartResolver() {
//		CommonsMultipartResolver multipartResolver
//				= new CommonsMultipartResolver();
//		multipartResolver.setMaxUploadSize(10485760);
//		return multipartResolver;
//	}


//	@Bean
//	public SolrTemplate solrTemplate(SolrClient solrClient) {
//		return new SolrTemplate(solrClient);
//	}
}