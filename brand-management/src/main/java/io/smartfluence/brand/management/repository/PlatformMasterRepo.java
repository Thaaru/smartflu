package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.PlatformMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlatformMasterRepo extends JpaRepository<PlatformMaster, Long> {
}
