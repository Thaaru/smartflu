package io.smartfluence.cassandra.repository.brand;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.brand.BrandCreditAllowance;
import io.smartfluence.cassandra.entities.brand.primary.BrandCreditAllowanceKey;
import io.smartfluence.constants.CreditType;

public interface BrandCreditAllowanceRepo extends CassandraRepository<BrandCreditAllowance, BrandCreditAllowanceKey> {

	Optional<BrandCreditAllowance> findAllByKeyBrandIdAndKeyInfluencerIdAndKeyCreditType(UUID brandId, UUID influencerId, CreditType creditType);
}