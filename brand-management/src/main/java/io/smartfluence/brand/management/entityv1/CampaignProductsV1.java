package io.smartfluence.brand.management.entityv1;


import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_products_v1")
public class CampaignProductsV1 {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
//    @Column(name = "product_id")
    private Long productId;

//    @Column(name = "campaign_id")
    private String campaignId;
//    @Column(name = "product_name")
    private String productName;

//    @Column(name = "product_link")
    private String productLink;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignProductsV1 that = (CampaignProductsV1) o;
        return Objects.equals(productId, that.productId) && Objects.equals(campaignId, that.campaignId) && Objects.equals(productName, that.productName) && Objects.equals(productLink, that.productLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, campaignId, productName, productLink);
    }
}
