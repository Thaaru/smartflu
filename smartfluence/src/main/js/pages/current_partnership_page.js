import React, { useEffect, useState } from "react";
import NavSideBarComponent from "../components/nav_sidebar";
import { getCurrentPartnershipApi } from "../services/api/campaign_details_api";
import { getStatus } from "../utils/string_constants";

export default function CurrentPartnerShipPage({ data }) {
    const [partnershipData, setParnershipData] = useState(null);
    const [code, setCode] = useState(null);

    useEffect(() => {
        async function getCampaignData() {
            const responeData = await getCurrentPartnershipApi();
            if (responeData.value != null) {
                setParnershipData(responeData.value);
            }
            setCode(responeData.code);
        }
        getCampaignData();
    }, [])
    return (
        <>
            <div class='content-wrapper' style={{ "padding": "0px" }}>
                <NavSideBarComponent />
                {
                    code == 604 || code == 603 ?
                        <div className="col" style={{ "alignContent": "center" }}>
                            Something went wrong please try again later!!!!
                        </div>
                        : code == 615 ? <div className="col" style={{ "alignContent": "center" }}>
                            No Partnership Data Available
                        </div> :
                            partnershipData == null ? <div className="loading" style={{ "alignContent": "center" }}>
                                <div className='spinner-wrapper'>
                                    <div class="heading mb-2">Please Wait ....</div>
                                    <div className="spinner-border" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div> :
                                <div>
                                    <div style={{ "textAlign": "left", "backgroundColor": "#F8F4FF", "justifyItems": "center" }}>
                                        <div className="col-md heading primaryColor" style={{ "lineHeight": "2" }}>
                                            Welcome
                                        </div>
                                        <div className="col-md normalText primaryColor">
                                            {partnershipData.userEmail}
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="heading" style={{ "textAlign": "left", "lineHeight": "3", padding: 0 }}>Current Partnerships</div>
                                        {!partnershipData.campainDetailsList ? <div className="loading" style={{ "alignContent": "center" }}>
                                            <div className='spinner-wrapper'>
                                                <div class="heading mb-2">Please Wait ....</div>
                                                <div className="spinner-border" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        </div>
                                            : <div className="col" style={{ "textAlign": "left", "padding": "0" }}>
                                                {partnershipData.campainDetailsList.map((item, index) => (
                                                    <div className="col mb-4">
                                                        <div class="card shadow-sm bg-white rounded" >
                                                            <div class="card-body" style={{ "padding": "0px" }}>
                                                                <div class="row" style={{ "justifyContent": "center", "alignItems": "center" }}>
                                                                    <div className="verticalLine"></div>
                                                                    <div class="col-9" style={{ "textAlign": "left" }}>
                                                                        <p className="heading">
                                                                            {item.name}
                                                                        </p>
                                                                        <span className="primaryColor" style={{ "backgroundColor": "#c9bfdc" }}>
                                                                            Status : {getStatus(item.proposalStatus)}
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-2" style={{ "textAlign": "right" }}>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        }
                                    </div>
                                </div>
                }

            </div>
        </>
    );
}