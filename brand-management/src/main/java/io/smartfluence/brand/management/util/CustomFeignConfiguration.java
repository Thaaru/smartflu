package io.smartfluence.brand.management.util;


    import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.optionals.OptionalDecoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

    @Configuration
    public class CustomFeignConfiguration {


        @Autowired
        private ObjectFactory<HttpMessageConverters> messageConverters;

        @Bean
        public RequestInterceptor gzipInterceptor() {
            return new RequestInterceptor() {
                @Override
                public void apply(RequestTemplate template) {
                    template.header("Accept-Encoding", "gzip, deflate");
                }
            };
        }

        @Bean
        public CustomGZIPResponseDecoder customGZIPResponseDecoder() {
            OptionalDecoder feignDecoder = new OptionalDecoder(new ResponseEntityDecoder(new SpringDecoder(this.messageConverters)));
            return new CustomGZIPResponseDecoder(feignDecoder);
        }
    }

