package io.smartfluence.webapp.controller.rest.brand;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.modal.validation.brand.promote.PromoteCampaignRequestModel;
import io.smartfluence.webapp.proxy.BrandManagementServiceProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandPromoteCampaignRestController {

	@Autowired
	private BrandManagementServiceProxy brandManagementServiceProxy;

	@PostMapping("sf-promote/manage-promote-campaign")
	public ResponseEntity<Object> createPromoteCampaign(@Validated @ModelAttribute PromoteCampaignRequestModel promoteCampaignRequest,
			BindingResult bindingResult) {
		log.info(promoteCampaignRequest);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.managePromoteCampaign(promoteCampaignRequest.getCampaignId(), promoteCampaignRequest.getCampaignName(),
					promoteCampaignRequest.getBrandDescription(), promoteCampaignRequest.getCampaignDescription(), promoteCampaignRequest.getPostType(),
					promoteCampaignRequest.getPayment(), promoteCampaignRequest.getProduct(), promoteCampaignRequest.getProductValue(),
					promoteCampaignRequest.getRequirements(), promoteCampaignRequest.getRestrictions(), promoteCampaignRequest.getActiveUntil(),
					promoteCampaignRequest.getComparisonProfile(), promoteCampaignRequest.getPlatform(), promoteCampaignRequest.getFollowersRange(),
					promoteCampaignRequest.getEngagementsRange(), promoteCampaignRequest.getAudienceIndustry(), promoteCampaignRequest.getInfluencerIndustry(),
					promoteCampaignRequest.getAudienceGeo(), promoteCampaignRequest.getInfluencerGeo(), promoteCampaignRequest.getAudienceAge(), 
					promoteCampaignRequest.getInfluencerAge(), promoteCampaignRequest.getAudienceGender(), promoteCampaignRequest.getInfluencerGender(),
					promoteCampaignRequest.getIsEdit());

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}

	@PostMapping("sf-promote/process-campaign")
	public ResponseEntity<String> processCampaign(@RequestParam UUID campaignId, @RequestParam String action) {
		if (!StringUtils.isEmpty(campaignId.toString()) && !StringUtils.isEmpty(action))
			return brandManagementServiceProxy.processPromoteCampaign(campaignId, action);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("CampaignId not available");
	}

	@PostMapping("sf-promote/influencer-action")
	public ResponseEntity<String> changeInfluencerStatus(@RequestParam UUID campaignId, @RequestParam String influencerDeepSocialId,
			@RequestParam String action) {
		if (!StringUtils.isEmpty(campaignId.toString()) && !StringUtils.isEmpty(influencerDeepSocialId) && !StringUtils.isEmpty(action))
			return brandManagementServiceProxy.influencerStatus(campaignId, influencerDeepSocialId, action);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Influencer not available");
	}
}