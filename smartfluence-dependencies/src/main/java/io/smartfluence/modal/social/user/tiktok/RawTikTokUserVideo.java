package io.smartfluence.modal.social.user.tiktok;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokUserVideo implements Serializable {

	private static final long serialVersionUID = 7741612579281012501L;

	@JsonAlias("id")
	private String id;

	@JsonAlias("cover")
	private String cover;
}