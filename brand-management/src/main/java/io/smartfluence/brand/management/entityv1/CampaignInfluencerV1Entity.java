package io.smartfluence.brand.management.entityv1;

import io.smartfluence.brand.management.entity.CampaignInfluencerKey;
import io.smartfluence.mysql.entities.SocialIdMaster;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_influencer_details_v1")
public class CampaignInfluencerV1Entity {

    @EmbeddedId
    CampaignInfluencerKey campaignInfluencerId;
//    @Column(name = "influencer_email")
    private String influencerEmail;
//    @Column(name = "platform")
    private String platform;

//    @Enumerated(EnumType.STRING)
//    @Column(name = "proposal_status")
    private String proposalStatus;
//    private ProposalStatus proposalStatus;
//    @Column(name = " is_offer_accepted")
    private String isOfferAccepted;
//    @Column(name = "offer_accepted_at")
    private LocalDateTime OfferAcceptedAt;
//    @Column(name = " is_products_accepted")
    private String isProductsAccepted;
//    @Column(name = "products_accepted_at")
    private LocalDateTime productsAcceptedAt;
//    @Column(name = "is_deliverables_accepted")
    private String isDeliverablesAccepted;
//    @Column(name = "deliverables_accepted_at")
    private LocalDateTime deliverablesAcceptedAt;
//    @Column(name = " is_shipping_accepted")
    private String isShippingAccepted;
//    @Column(name = "shipping_accepted_at")
    private LocalDateTime shippingAcceptedAt;
//    @Column(name = "is_payment_accepted")
    private String isPaymentAccepted;
//    @Column(name = "payment_accepted_at")
    private LocalDateTime paymentAcceptedAt;
//    @Column(name = " is_terms_accepted")
    private String isTermsAccepted;
//    @Column(name = "terms_accepted_at")
    private LocalDateTime termsAcceptedAt;
//    @Column(name = "payment_method_type")
    private String paymentMethodType;
//    @Column(name = "payment_email_address")
    private String paymentEmailAddress;
//    @Column(name = "campaign_paid_at")
    private LocalDateTime campaignPaidAt;

//   @Column(name = "influencer_handle")
   private String influencerHandle;
//   @Column(name = "influencer_audience_credibility")
   private String influencerAudienceCredibility;
//   @Column(name = "influencer_engagement")
   private String influencerEngagement;
//   @Column(name = "influencer_followers")
   private String influencerFollowers;
//   @Column(name = "influencer_first_name")
   private String influencerFirstName;
//   @Column(name = "influencer_last_name")
   private String influencerLastName;
//   @Column(name = "tracking_id")
   private String trackingId;
//   @Column(name = "post_url")
   private String postUrl;

   @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
   @JoinColumns({@JoinColumn(name = "campaignId",insertable = false,updatable = false  ),@JoinColumn(name = "influencerId",insertable = false,updatable = false  )})
  private Set<CampaignInfluencerOfferV1Entity> influencerOffers;
   @OneToMany(cascade = CascadeType.ALL)
   @JoinColumns({@JoinColumn(name = "campaignId",insertable = false,updatable = false  ),@JoinColumn(name = "influencerId",insertable = false,updatable = false  )})
   private  Set<CampaignInfluencerAddress> influencerAddresses;
   @OneToMany(cascade = CascadeType.ALL)
   @JoinColumns({@JoinColumn(name = "campaignId",insertable = false,updatable = false  ),@JoinColumn(name = "influencerId",insertable = false,updatable = false  )})
   private Set<CampaignInfluencerProducts> influencerProducts;
   @OneToMany(cascade = CascadeType.ALL)
   @JoinColumns({@JoinColumn(name = "campaignId",insertable = false,updatable = false  ),@JoinColumn(name = "influencerId",insertable = false,updatable = false  )})
       private Set<CampaignInfluencerTask> influencerTasks;
   @OneToOne
   @NotFound(action = NotFoundAction.IGNORE)
   @JoinColumn(
           name = "influencerId",
           referencedColumnName = "user_id",
           insertable = false,
           updatable = false,
           foreignKey = @javax.persistence
                   .ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
   private SocialIdMaster socialIdMasterList;

}