package io.smartfluence.modal.social.search.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudienceData implements Serializable {

	private static final long serialVersionUID = 1632423984771866445L;

	@JsonAlias("audience_credibility")
	private Double audienceCredibility;
}