package io.smartfluence.modal;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SignInModel {

	@NotBlank(message = "Enter your valid email")
	@Email(message = "Enter your valid email")
	private String email;

	@NotBlank(message = "Password should contain alteast 8 characters")
	@Size(min = 8, message = "Password should contain alteast 8 characters")
	private String password;
}