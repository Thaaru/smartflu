package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entityv1.BrandCampaignDetailsV1Entity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
public interface BrandCampaignDetailsV1Repo extends JpaRepository<BrandCampaignDetailsV1Entity, String> {
    Optional<BrandCampaignDetailsV1Entity> findByCampaignIdAndBrandId(String CampaignId, String BrandId);

    Optional<BrandCampaignDetailsV1Entity> findByCampaignId(String CampaignId);

    //    @Query(value = "SELECT * FROM brand_campaign_details_v1 WHERE BrandId = ?1 AND inprogress = ?2 || active =?3", nativeQuery = true)
    List<BrandCampaignDetailsV1Entity> findAllByBrandIdAndCampaignStatus(String BrandId, String campaignStatusNew);

    List<BrandCampaignDetailsV1Entity> findAllByBrandId(String BrandId);

}
