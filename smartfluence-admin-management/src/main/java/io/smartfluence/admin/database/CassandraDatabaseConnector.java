package io.smartfluence.admin.database;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class CassandraDatabaseConnector implements AutoCloseable {

	private Cluster cluster;

	private Session session;

	public void connectDataBase(String userName, String password, String keySpace, String contactPoints, int port) {
		try {
			this.cluster = Cluster.builder().withCredentials(userName, password).addContactPoint(contactPoints).withPort(port).build();
			this.session = cluster.connect(keySpace);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Session getSession() {
		return this.session;
	}

	public void close() {
		cluster.close();
		session.close();
	}
}