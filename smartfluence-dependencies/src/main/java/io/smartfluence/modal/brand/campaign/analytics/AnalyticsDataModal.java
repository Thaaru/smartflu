package io.smartfluence.modal.brand.campaign.analytics;

import java.util.Map;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class AnalyticsDataModal {

	private String influencerHandle;

	private UUID influencerId;

	private Double engagement;

	private Double followers;

	private Double audienceCredibility;

	private Double influencerPricing;

	private Double likes;

	private Double comments;

	private Double views;

	private Map<String, Double> ageRange;

	private Map<String, Double> country;

	private Map<String, Double> state;

	private Map<String, Double> gender;

	private Map<String, Double> industry;

	private String platfrom;

	public String influencerLink;
}