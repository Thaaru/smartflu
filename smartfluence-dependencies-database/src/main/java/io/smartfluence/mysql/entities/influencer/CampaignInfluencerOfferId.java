package io.smartfluence.mysql.entities.influencer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignInfluencerOfferId implements Serializable{
    String campaignId;
    String influencerId;
    @Enumerated(EnumType.STRING)
    OffersType offerType;
}
