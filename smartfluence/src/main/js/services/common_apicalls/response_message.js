export const ResponseMessage = {
    600:"success",
    605:"field_validation",
    604:"error",
    615:"empty_results" ,
}