package io.smartfluence.cassandra.entities.primary;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class TemporaryInfluencerContentKey {

	@PrimaryKeyColumn(name = "report_id", type = PrimaryKeyType.PARTITIONED)
	private String reportId;

	@PrimaryKeyColumn(name = "is_profile_pic", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private boolean isProfilePic;

	@PrimaryKeyColumn(name = "content_rank", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private Integer contentRank;
}