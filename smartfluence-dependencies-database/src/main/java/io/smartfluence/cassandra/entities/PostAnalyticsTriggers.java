package io.smartfluence.cassandra.entities;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.PostAnalyticsTriggersKey;
import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("post_analytics_triggers")
public class PostAnalyticsTriggers implements Copyable {

	private static final long serialVersionUID = 9122135466380258761L;

	public PostAnalyticsTriggers(PostAnalyticsTriggers postAnalyticsTriggers) {
		this.key = new PostAnalyticsTriggersKey(postAnalyticsTriggers.getKey());
		this.createdAt = postAnalyticsTriggers.getCreatedAt();
		this.postUrl = postAnalyticsTriggers.getPostUrl();
		this.postName = postAnalyticsTriggers.getPostName();
		this.influencerHandle = postAnalyticsTriggers.getInfluencerHandle();
		this.brandName = postAnalyticsTriggers.getBrandName();
		this.postTimeHourIndex = postAnalyticsTriggers.getPostTimeHourIndex();
		this.postId = postAnalyticsTriggers.getPostId();
		this.nextTrigger = postAnalyticsTriggers.getNextTrigger();
		this.isTriggerEnd = postAnalyticsTriggers.getIsTriggerEnd();
		this.postDataTime = postAnalyticsTriggers.getPostDataTime();
	}

	@PrimaryKey
	private PostAnalyticsTriggersKey key;

	@Column("post_time_hour_index")
	private Integer postTimeHourIndex;

	@Column("brand_name")
	private String brandName;

	@Column("influencer_handle")
	private String influencerHandle;

	@Column("post_url")
	private String postUrl;

	@Column("post_id")
	private UUID postId;

	@Column("post_name")
	private String postName;

	@Column("next_trigger")
	private Date nextTrigger;

	@Column("initial_trigger")
	private Date initialTrigger;

	@Default
	@Indexed
	@Column("is_trigger_end")
	private Boolean isTriggerEnd = false;

	@Column("platform")
	private Platforms platform;

	@Column("created_date")
	private Date createdAt;

	@Column("post_data_time")
	private Date postDataTime;
}