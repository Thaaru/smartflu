package io.smartfluence.cassandra.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.StripeSubscriptionHistory;
import io.smartfluence.cassandra.entities.primary.StripeSubscriptionHistoryKey;
import io.smartfluence.constants.UserType;

@Repository
public interface StripeSubscriptionHistoryRepo extends CassandraRepository<StripeSubscriptionHistory, StripeSubscriptionHistoryKey> {

	Optional<StripeSubscriptionHistory> findOneByKeyUserTypeAndKeyUserIdAndKeySubscriptionIdAndKeySubscriptionItemId(
			UserType userType, UUID userId, String subscriptionId, String subscriptionItem);
}