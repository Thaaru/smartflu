package io.smartfluence.util.image;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

import javax.imageio.ImageIO;

public class ImageProcessor {

	private URL imageURL;
	private BufferedImage bufferedImage;
	private BufferedImage bufferedImageOriginal;
	private ImageFilter advancedBlurImageFilter;
	private ImageFilter antiAliasFilter;
	private ImageFilter blurImageFilter;
	private ImageFilter brightDownFilter;
	private ImageFilter brightUpFilter;
	private ImageFilter edgeFilter;
	private ImageFilter flipFilter;
	private ImageFilter grayScaleFilter;
	private ImageFilter sharpenFilter;
	private ImageFilter tiltFilter;

	private void setImageURL(URL imageURL) throws IOException {
		this.imageURL = imageURL;
		bufferedImage = ImageIO.read(this.imageURL);
		bufferedImageOriginal = deepCopy(bufferedImage);
	}

	private BufferedImage deepCopy(BufferedImage bi) {
		ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}

	public BufferedImage deepCopy() {
		return deepCopy(bufferedImage);
	}

	public void resetImage() {
		bufferedImage = deepCopy(bufferedImageOriginal);
	}

	public ImageProcessor(String imageUrlString) throws MalformedURLException, IOException {
		setImageURL(new URL(imageUrlString));
	}

	public ImageProcessor(URL imageUrl) throws IOException {
		setImageURL(imageUrl);
	}

	public void advancedBlurImage() {
		ImageFilter imageFilter = advancedBlurImageFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void antiAlias() {
		ImageFilter imageFilter = antiAliasFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void blurImage() {
		ImageFilter imageFilter = blurImageFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void brightDown() {
		ImageFilter imageFilter = brightDownFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void brightUp() {
		ImageFilter imageFilter = brightUpFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void edge() {
		ImageFilter imageFilter = edgeFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void flip() {
		ImageFilter imageFilter = flipFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void grayScale() {
		ImageFilter imageFilter = grayScaleFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void sharpen() {
		ImageFilter imageFilter = sharpenFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public void tilt() {
		ImageFilter imageFilter = tiltFilter();
		bufferedImage = imageFilter.processImageBuffer(bufferedImage);
	}

	public byte[] processFinalImage() {
		byte[] data = new byte[] {};
		try {
			ByteArrayOutputStream processedData = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, "jpg", processedData);
			data = processedData.toByteArray();
			processedData.flush();
		} catch (IOException e) {

		}
		return data;
	}

	public String processFinalImageEncoded() {
		return Base64.getEncoder().encodeToString(processFinalImage());
	}

	private ImageFilter advancedBlurImageFilter() {
		if (advancedBlurImageFilter == null)
			advancedBlurImageFilter = new AdvancedBlurImageFilter();
		return advancedBlurImageFilter;
	}

	private ImageFilter antiAliasFilter() {
		if (antiAliasFilter == null)
			antiAliasFilter = new AntiAliasFilter();
		return antiAliasFilter;
	}

	private ImageFilter blurImageFilter() {
		if (blurImageFilter == null)
			blurImageFilter = new BlurImageFilter();
		return blurImageFilter;
	}

	private ImageFilter brightDownFilter() {
		if (brightDownFilter == null)
			brightDownFilter = new BrightDownFilter();
		return brightDownFilter;
	}

	private ImageFilter brightUpFilter() {
		if (brightUpFilter == null)
			brightUpFilter = new BrightUpFilter();
		return brightUpFilter;
	}

	private ImageFilter edgeFilter() {
		if (edgeFilter == null)
			edgeFilter = new EdgeFilter();
		return edgeFilter;
	}

	private ImageFilter flipFilter() {
		if (flipFilter == null)
			flipFilter = new FlipFilter();
		return flipFilter;
	}

	private ImageFilter grayScaleFilter() {
		if (grayScaleFilter == null)
			grayScaleFilter = new GrayScaleFilter();
		return grayScaleFilter;
	}

	private ImageFilter sharpenFilter() {
		if (sharpenFilter == null)
			sharpenFilter = new SharpenFilter();
		return sharpenFilter;
	}

	private ImageFilter tiltFilter() {
		if (tiltFilter == null)
			tiltFilter = new TiltFilter();
		return tiltFilter;
	}
}