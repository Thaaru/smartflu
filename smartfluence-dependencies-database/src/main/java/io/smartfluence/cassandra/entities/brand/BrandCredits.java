package io.smartfluence.cassandra.entities.brand;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_credits")
public class BrandCredits implements Serializable {

	private static final long serialVersionUID = 1174824546571820755L;

	@PrimaryKey("brand_id")
	private UUID brandId;

	@Column("credits")
	private Integer credits;

	@Column("valid_upto")
	private Date validUpto;

	@LastModifiedDate
	@Column("modified_at")
	private Date modifiedAt;

	@Column("created_at")
	private Date createdAt;
}