package io.smartfluence.mysql.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "subscription_master")
public class StripeSubscriptionMaster implements Serializable {

	private static final long serialVersionUID = -6638924806869746437L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne()
	@JoinColumn(name = "user_id")
	private UserAccounts userAccounts;

	@Column(name = "customer_id")
	private String customerId;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "subscription_id")
	private String subscriptionId;

	@Column(name = "item_id")
	private String itemId;

	@Column(name = "plan_id")
	private String planId;

	@Column(name = "product_id")
	private String productId;
}