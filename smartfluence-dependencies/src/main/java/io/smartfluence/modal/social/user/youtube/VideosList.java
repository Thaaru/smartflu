package io.smartfluence.modal.social.user.youtube;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VideosList implements Serializable {

	private static final long serialVersionUID = -5388201653672230425L;

	@JsonAlias("videos")
	private List<Videos> videos;
}