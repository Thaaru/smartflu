package io.smartfluence.modal.social.user.instagram;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Caption {

	@JsonAlias("caption_text")
    private String text;
}