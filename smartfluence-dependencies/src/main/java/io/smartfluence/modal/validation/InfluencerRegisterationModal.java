package io.smartfluence.modal.validation;


import java.util.TimeZone;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import io.smartfluence.constants.UserType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerRegisterationModal {
    @NotNull(message = "Select whether you want to register as a brand or an infuencer")
	private UserType userType = UserType.INFLUENCER;

	private String firstName;

	private String lastName;

	@NotBlank(message = "Enter your valid email")
	@Email(message = "Enter your valid email")
	private String email;

	
	@Size(min = 8, message = "Password should contain alteast 8 characters")
	private String password;

	private TimeZone timeZone;
}
