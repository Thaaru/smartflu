package io.smartfluence.modal.social.search.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InfluencersDataResponse implements Serializable {

	private static final long serialVersionUID = 6485973692757082846L;

	@JsonAlias("total")
	private Long total;

	@JsonAlias("accounts")
	private List<SearchAccounts> accounts;
}