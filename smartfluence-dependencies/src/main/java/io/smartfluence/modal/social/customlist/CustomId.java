package io.smartfluence.modal.social.customlist;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomId implements Serializable {

	private static final long serialVersionUID = -7633182049441641848L;

	@JsonAlias("id")
	private Integer id;

	@JsonAlias("type")
	private String type;
}