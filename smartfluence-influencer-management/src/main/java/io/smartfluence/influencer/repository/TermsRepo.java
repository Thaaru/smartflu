package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.smartfluence.influencer.entities.TermsConditions;

public interface TermsRepo extends JpaRepository<TermsConditions,Long>{
    TermsConditions findByTermId(Long termId);
}
