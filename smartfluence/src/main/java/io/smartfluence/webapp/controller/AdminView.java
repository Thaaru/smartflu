package io.smartfluence.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import io.smartfluence.ResourceServer;
import io.smartfluence.webapp.proxy.AdminManagementServiceProxy;

@Controller
@RequestMapping("admin")
public class AdminView {

	private static final String ACTIVE = "active";

	@Autowired
	private AdminManagementServiceProxy adminManagementServiceProxy;

	@GetMapping("brand-management")
	public String brandManagementView(Model model) {
		ResourceServer.getUserId();
		model.addAttribute("brand", ACTIVE);
		model.addAttribute("brandList", adminManagementServiceProxy.getBrandList().getBody());
		model.addAttribute("subscriptions", adminManagementServiceProxy.getSubscriptions().getBody());
		//model.addAttribute("dbUpdateHistories", adminManagementServiceProxy.getDBUpdateHistory().getBody());
		return "Admin/BrandManagement";
	}	
}