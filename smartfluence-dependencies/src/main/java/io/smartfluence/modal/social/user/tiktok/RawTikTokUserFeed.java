package io.smartfluence.modal.social.user.tiktok;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokUserFeed implements Serializable {

	private static final long serialVersionUID = 8290452345761497336L;

	@JsonAlias("success")
	private Boolean success;

	@JsonAlias("user_feed")
	private RawTikTokUserFeedValue userFeed;
}