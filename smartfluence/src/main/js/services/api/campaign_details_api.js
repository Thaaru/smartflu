import React from "react";
import { createAccount, details,  getCurrentPartnership,  getStateDetails,  updateDetails } from "../../utils/http_urls";
import { serviceCall } from "../common_apicalls/server_call";
import {getCookie} from '../common_apicalls/helpers';

const getQueryParams = (key) => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const token = urlParams.get(key);
  return token;

}

export async function getCampaignDetailsApi(){
  const token = getQueryParams("token");
  const apiCall = new serviceCall();
  const apiUrl = details(token);
  const response = await apiCall.getCall({url:`${apiUrl}`});
  return JSON.parse(JSON.stringify(response));
}

export async function updateCampaignDetailsApi(data){
  const token = getQueryParams("token");
  const apiCall = new serviceCall();
  const apiUrl = updateDetails(token);
  const response = await apiCall.putCall({url:`${apiUrl}`,body:data});
  return JSON.parse(JSON.stringify(response));
}

export async function createInfluencerAccountApi(influencerId){
  const apiCall = new serviceCall();
  const apiUrl = createAccount();
  const response = await apiCall.postCall({url:`${apiUrl}?influencerId=${influencerId}`});
  return JSON.parse(JSON.stringify(response));
}

export async function getStateListApi(countryId){
  const apiCall = new serviceCall();
  const apiUrl = getStateDetails();
  const response = await apiCall.getCall({url:`${apiUrl}/${countryId}`});
  return JSON.parse(JSON.stringify(response));
}



export async function getCurrentPartnershipApi(){
  const apiCall = new serviceCall();
  const apiUrl = getCurrentPartnership;
  const response = await apiCall.getCall({url:`${apiUrl}`});
  return JSON.parse(JSON.stringify(response));
}