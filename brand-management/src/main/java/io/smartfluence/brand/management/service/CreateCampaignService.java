package io.smartfluence.brand.management.service;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.constants.*;
import io.smartfluence.brand.management.dao.AddInfluencerDaoImpl;
import io.smartfluence.brand.management.entity.*;
import io.smartfluence.brand.management.entityv1.BrandCampaignDetailsV1Entity;
import io.smartfluence.brand.management.entityv1.CampaignInfluencerV1Entity;
import io.smartfluence.brand.management.entityv1.CampaignOffersV1;
import io.smartfluence.brand.management.repository.*;
import io.smartfluence.brand.management.servicev1.CampaignEntityServiceV1;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.repository.brand.BrandCampaignDetailsRepo;
import io.smartfluence.modal.brand.campaignv1.OfferTypeEntityV1;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.brand.viewinfluencer.BrandCampaigns;
import io.smartfluence.modal.validation.brand.dashboard.RemoveInfluencerFromCampaignModel;
import io.smartfluence.modal.validation.brand.explore.CampaignInfluencerReqEntity;
import io.smartfluence.mysql.entities.CostPerEngagementMaster;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.repository.CostPerEngagementMasterRepo;
import io.smartfluence.mysql.repository.SocialIdMasterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class CreateCampaignService {

    private static final double PPF = 0.01;

    @Autowired
    private SocialIdMasterRepo socialIdMasterRepo;
    @Autowired
    private ReportsService reportsService;
    @Autowired
    private CostPerEngagementMasterRepo costPerEngagementMasterRepo;

    @Autowired
    private CampaignInfOfferV1Repo campaignInfOfferV1Repo;

    @Autowired
    private CampaignInfluencerRepo campaignInfluencerRepo;

    @Autowired
    BrandCampaignDetailsRepository brandCampaignDetailsRepository;

    @Autowired
    CampaignTaskRepo campaignTaskRepo;

    @Autowired
    ProductsRepo productsRepo;

    @Autowired
    TermsAndConditionsRepo termsAndConditionsRepo;

    @Autowired
    PlatformMasterRepo platformMasterRepo;

    @Autowired
    PostMasterRepo postMasterRepo;

    @Autowired
    CurrencyMasterRepo currencyMasterRepo;

    @Autowired
    CommissionAffiliateTypeRepo commissionAffiliateTypeRepo;
    @Autowired
    AddInfluencerDaoImpl addInfluencerDaoImpl;

    @Autowired
    BrandCampaignDetailsV1Repo brandCampaignDetailsV1Repo;

    @Autowired
    private BrandCampaignDetailsRepo brandCampaignDetailsRepo;


    @Autowired
    private CampaignEntityServiceV1 campaignEntityServiceV1;


    public ResponseEntity<Object> saveNewCampaign(FullCampaignDetailEntity fullCampaignDetailEntity) {
        BrandCampaignDetailsNew brandCampaignDetailsNew = new BrandCampaignDetailsNew();
        //CampaignTask campaignTask = new CampaignTask();
        TermsAndConditions termsAndConditions = new TermsAndConditions();
        //List<Products> products = new LinkedList<Products>();

        if (fullCampaignDetailEntity.getCampaignId() == null) {
            brandCampaignDetailsNew.setCampaignId(UUID.randomUUID().toString());
            brandCampaignDetailsNew.setBrandId(ResourceServer.getUserId().toString());
            brandCampaignDetailsNew.setBrandDescription(fullCampaignDetailEntity.getBrandDescription());
            brandCampaignDetailsNew.setCampaignName(fullCampaignDetailEntity.getCampaignName());
            brandCampaignDetailsNew.setCampaignDescription(fullCampaignDetailEntity.getCampaignDescription());
            if (fullCampaignDetailEntity.getCampaignStatusNew() == null) {
                brandCampaignDetailsNew.setCampaignStatus(CampaignStatusNew.NEW.toString());
            } else if (fullCampaignDetailEntity.
                    getCampaignStatusNew().toString() != CampaignStatusNew.LAUNCH.toString()) {
                brandCampaignDetailsNew.setCampaignStatus(fullCampaignDetailEntity.getCampaignStatusNew().toString());
                if (fullCampaignDetailEntity.getCampaignStatusNew().toString() == CampaignStatusNew.CLOSED.toString()) {
                    brandCampaignDetailsNew.setDeletedAt(LocalDateTime.now());
                    brandCampaignDetailsNew.setEndDate(LocalDateTime.now());
                }
            } else {
                brandCampaignDetailsNew.setCampaignStatus(fullCampaignDetailEntity.getCampaignStatusNew().toString());
            }
            if (fullCampaignDetailEntity.getOfferType() != null) {
                brandCampaignDetailsNew.setOfferType(fullCampaignDetailEntity.getOfferType().toString());
            }
            brandCampaignDetailsNew.setIsNew(IsNew.newcampaign.toString());
            brandCampaignDetailsNew.setRestrictions(fullCampaignDetailEntity.getRestrictions());
            brandCampaignDetailsNew.setCurrency(fullCampaignDetailEntity.getCurrency());
            brandCampaignDetailsNew.setCreatedAt(new Date());

            brandCampaignDetailsNew.setModifiedAt(LocalDateTime.now());
            brandCampaignDetailsNew.setHashTags(fullCampaignDetailEntity.getHashTags());
            brandCampaignDetailsNew.setMaxProductCount(fullCampaignDetailEntity.getMaxProductCount());
            brandCampaignDetailsNew.setPaymentOffer(fullCampaignDetailEntity.getPaymentOffer());
            brandCampaignDetailsNew.setCommissionAffiliate(fullCampaignDetailEntity.getCommissionAffiliate());
            brandCampaignDetailsNew.setCommissionAffiliateType(fullCampaignDetailEntity.getCommissionAffiliateType());
            brandCampaignDetailsNew.setCommissionAffiliateDetails(fullCampaignDetailEntity.getCommissionAffiliateDetails());
            brandCampaignDetailsNew.setTermsId(fullCampaignDetailEntity.getTermId());
            brandCampaignDetailsNew.setCommissionAffiliateCustomName(fullCampaignDetailEntity.getCommissionAffiliateCustomName());


            if (fullCampaignDetailEntity.getTermsAndConditions() != null) {

                fullCampaignDetailEntity.getTermsAndConditions().stream().forEach(termsAndConditions1 -> {
                    if (termsAndConditions1.getTermId() != 0 && termsAndConditions1.getTermId() == null) {
                        TermsAndConditions termsAndConditionsObj = new TermsAndConditions();
                        termsAndConditionsObj.setTermsCondition(termsAndConditions1.getTermsCondition());
                        termsAndConditionsObj.setTermsConditionFilePath(termsAndConditions1.getTermsConditionFilePath());
                        termsAndConditionsObj.setStatus(Status.custom.toString());
                        TermsAndConditions termsAndConditions2 = termsAndConditionsRepo.save(termsAndConditions);
                        if (termsAndConditions2 != null) {
                            brandCampaignDetailsNew.setTermsId(termsAndConditions2.getTermId());
                        }
                    }
                });
            }


            BrandCampaignDetailsNew brandCampaignDetailsNew1 = brandCampaignDetailsRepository.save(brandCampaignDetailsNew);
            if (!Objects.isNull(brandCampaignDetailsNew1)) {
                if (fullCampaignDetailEntity.getProductList() != null) {
                    fullCampaignDetailEntity.getProductList().stream().forEach(products1 -> {
                        products1.setCampaignId(brandCampaignDetailsNew1.getCampaignId());
                        productsRepo.save(products1);
                    });
                }
                if (fullCampaignDetailEntity.getCampaignTaskList() != null) {
                    fullCampaignDetailEntity.getCampaignTaskList().stream().forEach(campaignTask1 -> {
                        campaignTask1.setCampaignId(brandCampaignDetailsNew1.getCampaignId());
                        campaignTask1.setPlatformId(campaignTask1.getPlatformId());
                        campaignTaskRepo.save(campaignTask1);
                    });
                }

            }
            return ResponseEntity.ok(fullCampaignDetailEntity);
        } else {
            brandCampaignDetailsNew.setCampaignId(fullCampaignDetailEntity.getCampaignId());
            brandCampaignDetailsNew.setBrandId(ResourceServer.getUserId().toString());
            brandCampaignDetailsNew.setBrandDescription(fullCampaignDetailEntity.getBrandDescription());
            brandCampaignDetailsNew.setCampaignName(fullCampaignDetailEntity.getCampaignName());
            brandCampaignDetailsNew.setCampaignDescription(fullCampaignDetailEntity.getCampaignDescription());
            if (fullCampaignDetailEntity.getCampaignStatusNew() == null) {
                brandCampaignDetailsNew.setCampaignStatus(CampaignStatusNew.NEW.toString());
            } else if (fullCampaignDetailEntity.
                    getCampaignStatusNew().toString() != CampaignStatusNew.LAUNCH.toString()) {
                brandCampaignDetailsNew.setCampaignStatus(fullCampaignDetailEntity.getCampaignStatusNew().toString());
                if (fullCampaignDetailEntity.getCampaignStatusNew().toString() == CampaignStatusNew.CLOSED.toString()) {
                    brandCampaignDetailsNew.setDeletedAt(LocalDateTime.now());
                    brandCampaignDetailsNew.setEndDate(LocalDateTime.now());
                }
            } else {
                brandCampaignDetailsNew.setCampaignStatus(fullCampaignDetailEntity.getCampaignStatusNew().toString());
            }
            if (fullCampaignDetailEntity.getOfferType() != null) {
                brandCampaignDetailsNew.setOfferType(fullCampaignDetailEntity.getOfferType().toString());
            }
            brandCampaignDetailsNew.setIsNew(IsNew.newcampaign.toString());
            brandCampaignDetailsNew.setRestrictions(fullCampaignDetailEntity.getRestrictions());
            brandCampaignDetailsNew.setCurrency(fullCampaignDetailEntity.getCurrency());
            brandCampaignDetailsNew.setCreatedAt(new Date());

            brandCampaignDetailsNew.setModifiedAt(LocalDateTime.now());
            brandCampaignDetailsNew.setHashTags(fullCampaignDetailEntity.getHashTags());
            brandCampaignDetailsNew.setMaxProductCount(fullCampaignDetailEntity.getMaxProductCount());
            brandCampaignDetailsNew.setPaymentOffer(fullCampaignDetailEntity.getPaymentOffer());
            brandCampaignDetailsNew.setCommissionAffiliate(fullCampaignDetailEntity.getCommissionAffiliate());
            brandCampaignDetailsNew.setCommissionAffiliateType(fullCampaignDetailEntity.getCommissionAffiliateType());
            brandCampaignDetailsNew.setCommissionAffiliateDetails(fullCampaignDetailEntity.getCommissionAffiliateDetails());
            brandCampaignDetailsNew.setTermsId(fullCampaignDetailEntity.getTermId());


            if (fullCampaignDetailEntity.getTermsAndConditions() != null) {

                fullCampaignDetailEntity.getTermsAndConditions().stream().forEach(termsAndConditions1 -> {
                    if (termsAndConditions1.getTermId() != 0 && termsAndConditions1.getTermId() == null) {
                        TermsAndConditions termsAndConditionsObj = new TermsAndConditions();
                        termsAndConditionsObj.setTermsCondition(termsAndConditions1.getTermsCondition());
                        termsAndConditionsObj.setTermsConditionFilePath(termsAndConditions1.getTermsConditionFilePath());
                        termsAndConditionsObj.setStatus(Status.custom.toString());
                        TermsAndConditions termsAndConditions2 = termsAndConditionsRepo.save(termsAndConditions);
                        if (termsAndConditions2 != null) {
                            brandCampaignDetailsNew.setTermsId(termsAndConditions2.getTermId());
                        }
                    }
                });
            }


            BrandCampaignDetailsNew brandCampaignDetailsNew1 = brandCampaignDetailsRepository.save(brandCampaignDetailsNew);
            if (!Objects.isNull(brandCampaignDetailsNew1)) {
                if (fullCampaignDetailEntity.getProductList() != null) {
                    fullCampaignDetailEntity.getProductList().stream().forEach(products1 -> {
                        products1.setCampaignId(brandCampaignDetailsNew1.getCampaignId());
                        productsRepo.save(products1);
                    });
                }
                if (fullCampaignDetailEntity.getCampaignTaskList() != null) {
                    fullCampaignDetailEntity.getCampaignTaskList().stream().forEach(campaignTask1 -> {
                        campaignTask1.setCampaignId(brandCampaignDetailsNew1.getCampaignId());
                        campaignTaskRepo.save(campaignTask1);
                    });
                }

            }
            return ResponseEntity.ok(fullCampaignDetailEntity);
        }
//        return ResponseEntity.ok(fullCampaignDetailEntity);
    }

    public ResponseEntity<Object> getCampaignById(UUID campaignId) {
        FullCampaignDetailEntity fullCampaignDetailEntity = new FullCampaignDetailEntity();
        if (campaignId != null) {
            BrandCampaignDetailsNew brandCampaignDetailsNew = brandCampaignDetailsRepository.findByCampaignId(campaignId.toString());
            List<CampaignTask> campaignTask = campaignTaskRepo.findByCampaignId(campaignId.toString());
            List<Products> products = productsRepo.findByCampaignId(campaignId.toString());
            long termId = brandCampaignDetailsNew.getTermsId();
            List<TermsAndConditions> termsAndCondition = termsAndConditionsRepo.findByTermId(termId);
            fullCampaignDetailEntity.setTermsAndConditions(termsAndCondition);
            List<CurrencyMaster> currencyMasters = currencyMasterRepo.findAll();
            fullCampaignDetailEntity.setCurrencyMasters(currencyMasters);
            List<PostMaster> postMasters = postMasterRepo.findAll();
            fullCampaignDetailEntity.setPostMasters(postMasters);
            List<CommissionAffiliateTypeMaster> commissionAffiliateTypeMasters = commissionAffiliateTypeRepo.findAll();
            fullCampaignDetailEntity.setCommissionAffiliateTypeMasters(commissionAffiliateTypeMasters);
            List<PlatformMaster> platformMasters = platformMasterRepo.findAll();
            fullCampaignDetailEntity.setPlatformMasters(platformMasters);
            fullCampaignDetailEntity.setCampaignId(brandCampaignDetailsNew.getCampaignId());
            fullCampaignDetailEntity.setBrandId(brandCampaignDetailsNew.getBrandId());
            fullCampaignDetailEntity.setBrandDescription(brandCampaignDetailsNew.getBrandDescription());
            fullCampaignDetailEntity.setCampaignName(brandCampaignDetailsNew.getCampaignName());
            fullCampaignDetailEntity.setCampaignDescription(brandCampaignDetailsNew.getCampaignDescription());
            fullCampaignDetailEntity.setCampaignStatusNew(CampaignStatusNew.valueOf(brandCampaignDetailsNew.getCampaignStatus()));
            fullCampaignDetailEntity.setDeletedStatus(DeletedStatus.valueOf(brandCampaignDetailsNew.getDeletedStatus()));
            fullCampaignDetailEntity.setOfferType(OfferType.valueOf(brandCampaignDetailsNew.getOfferType()));
            fullCampaignDetailEntity.setIsNew(IsNew.valueOf(brandCampaignDetailsNew.getIsNew()));
            fullCampaignDetailEntity.setRestrictions(brandCampaignDetailsNew.getRestrictions());
            fullCampaignDetailEntity.setCurrency(brandCampaignDetailsNew.getCurrency());
            fullCampaignDetailEntity.setHashTags(brandCampaignDetailsNew.getHashTags());
            fullCampaignDetailEntity.setMaxProductCount(brandCampaignDetailsNew.getMaxProductCount());
            fullCampaignDetailEntity.setCommissionAffiliateType(brandCampaignDetailsNew.getCommissionAffiliateType());
            fullCampaignDetailEntity.setPaymentOffer(brandCampaignDetailsNew.getPaymentOffer());

            fullCampaignDetailEntity.setCampaignTaskList(campaignTask);
            fullCampaignDetailEntity.setProductList(products);
            return ResponseEntity.status(HttpStatus.OK).body(fullCampaignDetailEntity);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Campaign Id not available");
        }
    }

    public ResponseEntity<Object> launchNewCampaign(LaunchFullCampaignEntity launchFullCampaignEntity) {
        BrandCampaignDetailsNew brandCampaignDetailsNew = new BrandCampaignDetailsNew();
        //CampaignTask campaignTask = new CampaignTask();
        TermsAndConditions termsAndConditions = new TermsAndConditions();
        //List<Products> products = new LinkedList<Products>();

        if (launchFullCampaignEntity.getCampaignId() == null) {
            brandCampaignDetailsNew.setCampaignId(UUID.randomUUID().toString());
        } else {
            brandCampaignDetailsNew.setCampaignId(launchFullCampaignEntity.getCampaignId());
        }
        brandCampaignDetailsNew.setBrandId(ResourceServer.getUserId().toString());
        brandCampaignDetailsNew.setBrandDescription(launchFullCampaignEntity.getBrandDescription());
        brandCampaignDetailsNew.setCampaignName(launchFullCampaignEntity.getCampaignName());
        brandCampaignDetailsNew.setCampaignDescription(launchFullCampaignEntity.getCampaignDescription());
        if (launchFullCampaignEntity.getCampaignStatusNew() == null) {
            brandCampaignDetailsNew.setCampaignStatus(CampaignStatusNew.NEW.toString());
        } else if (launchFullCampaignEntity.
                getCampaignStatusNew().toString() != CampaignStatusNew.LAUNCH.toString()) {
            brandCampaignDetailsNew.setCampaignStatus(launchFullCampaignEntity.getCampaignStatusNew().toString());
            if (launchFullCampaignEntity.getCampaignStatusNew().toString() == CampaignStatusNew.CLOSED.toString()) {
                brandCampaignDetailsNew.setDeletedAt(LocalDateTime.now());
                brandCampaignDetailsNew.setEndDate(LocalDateTime.now());
            }
        } else {
            brandCampaignDetailsNew.setCampaignStatus(launchFullCampaignEntity.getCampaignStatusNew().toString());
        }
        if (launchFullCampaignEntity.getOfferType() != null) {
            brandCampaignDetailsNew.setOfferType(launchFullCampaignEntity.getOfferType().toString());
        }
//            brandCampaignDetailsNew.setOfferType(fullCampaignDetailEntity.getOfferType().toString());
        brandCampaignDetailsNew.setIsNew(IsNew.newcampaign.toString());
        brandCampaignDetailsNew.setRestrictions(launchFullCampaignEntity.getRestrictions());
        brandCampaignDetailsNew.setCurrency(launchFullCampaignEntity.getCurrency());
        brandCampaignDetailsNew.setCreatedAt(new Date());

        brandCampaignDetailsNew.setModifiedAt(LocalDateTime.now());
        brandCampaignDetailsNew.setHashTags(launchFullCampaignEntity.getHashTags());
        brandCampaignDetailsNew.setMaxProductCount(launchFullCampaignEntity.getMaxProductCount());
        brandCampaignDetailsNew.setPaymentOffer(launchFullCampaignEntity.getPaymentOffer());
        brandCampaignDetailsNew.setCommissionAffiliate(launchFullCampaignEntity.getCommissionAffiliate());
        brandCampaignDetailsNew.setCommissionAffiliateType(launchFullCampaignEntity.getCommissionAffiliateType());
        brandCampaignDetailsNew.setCommissionAffiliateDetails(launchFullCampaignEntity.getCommissionAffiliateDetails());
        brandCampaignDetailsNew.setTermsId(launchFullCampaignEntity.getTermId());


        if (launchFullCampaignEntity.getTermsAndConditions() != null) {

            launchFullCampaignEntity.getTermsAndConditions().stream().forEach(termsAndConditions1 -> {
                if (termsAndConditions1.getTermId() != 0 && termsAndConditions1.getTermId() == null) {
                    TermsAndConditions termsAndConditionsObj = new TermsAndConditions();
                    termsAndConditionsObj.setTermsCondition(termsAndConditions1.getTermsCondition());
                    termsAndConditionsObj.setTermsConditionFilePath(termsAndConditions1.getTermsConditionFilePath());
                    termsAndConditionsObj.setStatus(Status.custom.toString());
                    TermsAndConditions termsAndConditions2 = termsAndConditionsRepo.save(termsAndConditions);
                    if (termsAndConditions2 != null) {
                        brandCampaignDetailsNew.setTermsId(termsAndConditions2.getTermId());
                    }
                }
            });
        }
        BrandCampaignDetailsNew brandCampaignDetailsNew1 = brandCampaignDetailsRepository.save(brandCampaignDetailsNew);
        if (Objects.isNull(brandCampaignDetailsNew1)) {
            if (launchFullCampaignEntity.getProductList() != null) {
                launchFullCampaignEntity.getProductList().stream().forEach(products1 -> {
                    productsRepo.save(products1);
                });
            }
            if (launchFullCampaignEntity.getCampaignTaskList() != null) {
                launchFullCampaignEntity.getCampaignTaskList().stream().forEach(campaignTask1 -> {
                    campaignTaskRepo.save(campaignTask1);
                });
            }

        }


        return ResponseEntity.ok(launchFullCampaignEntity);
    }

    public ResponseEntity<Object> addInfluencerToNewCampaign(List<CampaignInfluencerReqEntity> campaignInfluencerReqEntity) {
        return addInfluencerDaoImpl.addInfluencerToNewCampaign(campaignInfluencerReqEntity);
    }

}