package io.smartfluence.brand.management.servicev1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.entityv1.ActiveCampaignMapper;

import io.smartfluence.brand.management.mailbean.MailService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class ProcedureClass {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private MailService mailService;

    public List<ActiveCampaignMapper> getAllActiveCampaign() throws JsonProcessingException {
        List<ActiveCampaignMapper> influencerList = new ArrayList<>();
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("get_active_campaign_with_influencers");
        storedProcedure.registerStoredProcedureParameter("brand_id", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("campaign_status", String.class, ParameterMode.IN);
        storedProcedure.setParameter("brand_id", ResourceServer.getUserId().toString());
        storedProcedure.setParameter("campaign_status", "ACTIVE");
        storedProcedure.execute();
        Object resultList = storedProcedure.getSingleResult();
        if (Objects.nonNull(resultList)) {
            ObjectMapper objectMapper = new ObjectMapper();
            JSONArray array = new JSONArray(resultList.toString());

            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                ActiveCampaignMapper activeCampaignMapper = null;
                try {
                    activeCampaignMapper = objectMapper.readValue(object.toString(), ActiveCampaignMapper.class);
                    influencerList.add(activeCampaignMapper);
                } catch (Exception e) {
                    mailService.sendErrorMailDetail(e);
                }
            }
            return influencerList;
        } else {
            return influencerList;
        }
    }

}
