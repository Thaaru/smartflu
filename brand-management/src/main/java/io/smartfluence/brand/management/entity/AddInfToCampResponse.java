package io.smartfluence.brand.management.entity;

import io.smartfluence.brand.management.constants.ProposalStatus;
import io.smartfluence.brand.management.entityv1.CampaignProductsV1;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AddInfToCampResponse {

    private String campaignId;
    private String campaignName;
    private String brandName;
    private String influencerId;
    private String influencerEmail;
    private String platform;
    private String campaignStatus;
    private String proposalStatus;
    private String influencerPricing;
    private String isOfferAccepted;

    private LocalDateTime OfferAcceptedAt;

    private String isProductsAccepted;

    private LocalDateTime productsAcceptedAt;

    private String isDeliverablesAccepted;

    private LocalDateTime deliverablesAcceptedAt;

    private String isShippingAccepted;

    private LocalDateTime shippingAcceptedAt;

    private String isPaymentAccepted;

    private LocalDateTime paymentAcceptedAt;

    private String isTermsAccepted;

    private LocalDateTime termsAcceptedAt;

    private String paymentMethodType;

    private String paymentEmailAddress;

    private LocalDateTime campaignPaidAt;

    private String influencerHandle;

    private String influencerAudienceCredibility;

    private String influencerEngagement;

    private String influencerFollowers;

    private String influencerFirstName;

    private String influencerLastName;

    private String offerType;

    private OfferTypeResponse offerTypeResponse;


}
