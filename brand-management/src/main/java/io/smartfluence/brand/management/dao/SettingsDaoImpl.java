package io.smartfluence.brand.management.dao;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandPaymentDetails;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandPaymentDetailsRepo;
import io.smartfluence.mysql.entities.BrandEmailConnection;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.BrandEmailConnectionRepo;
import io.smartfluence.mysql.repository.UserAccountsRepo;

@Repository
public class SettingsDaoImpl implements SettingsDao {

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private BrandPaymentDetailsRepo brandPaymentDetailsRepo;

	@Autowired
	private BrandEmailConnectionRepo brandEmailConnectionRepo;

	@Override
	public void saveBrandDetails(BrandDetails brandDetails) {
		brandDetailsRepo.save(brandDetails);
	}

	@Override
	public Optional<BrandDetails> getBrandDetailsById(UserDetailsKey key) {
		return brandDetailsRepo.findById(key);
	}

	@Override
	public void saveBrandPaymentDetails(BrandPaymentDetails brandPaymentDetails) {
		brandPaymentDetailsRepo.save(brandPaymentDetails);
	}

	@Override
	public Optional<BrandPaymentDetails> getBrandPaymentDetailsById(UUID userId) {
		return brandPaymentDetailsRepo.findById(userId);
	}

	@Override
	public void saveUserAccounts(UserAccounts userAccounts) {
		userAccountsRepo.save(userAccounts);
	}

	@Override
	public Optional<UserAccounts> getUserAccountsById(String userId) {
		return userAccountsRepo.findById(userId);
	}

	@Override
	public Optional<UserAccounts> getUserAccountsByEmailAndUserIdNotEqual(String email, String userId) {
		return userAccountsRepo.findByEmailAndUserIdNotEqual(email, userId);
	}

	@Override
	public Optional<BrandEmailConnection> getEmailConnection(String brandId) {
		return brandEmailConnectionRepo.findByBrandIdAndStatus(brandId, 1);
	}

	@Override
	public void saveEmailConnection(BrandEmailConnection brandEmailConnection) {
		brandEmailConnectionRepo.save(brandEmailConnection);
	}

	@Override
	public void removeEmailConnection(String brandId) {
		brandEmailConnectionRepo.deleteById(brandId);
	}
}