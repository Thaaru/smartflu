// const httpUrl = window.location.origin;
const commonurl = window.location.origin;

export const details = (token) => `${commonurl}/influencer/pub/campaign-influencer-details?token=${token}`;

export const updateDetails = (token) => `${commonurl}/influencer/pub/campaign-influencer-details?token=${token}`;

export const getStateDetails = ()=>`${commonurl}/influencer/pub/states`;

export const getCurrentPartnership =`${commonurl}/influencer/current-partnership`;

export const createAccount = ()=>`${commonurl}:9000/registerinfluencer`;

export const login = ()=>`${commonurl}/login`;

export const logout = ()=>`${commonurl}/logout`;

export const home = ()=>`${commonurl}/influencer/home`;