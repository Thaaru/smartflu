package io.smartfluence.influencer.entities;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "currency_master")
public class CurrencyMasterEntity {

    @Id
    private Long currencyId;
    private String currency;

}
