package io.smartfluence.brand.management.constants;

public enum OfferType {
    PRODUCT, COMMISSION, PAYMENT
}

