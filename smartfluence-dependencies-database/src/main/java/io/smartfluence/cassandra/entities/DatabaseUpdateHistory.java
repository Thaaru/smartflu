package io.smartfluence.cassandra.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.constants.DatabaseUpdateStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("db_update_history")
public class DatabaseUpdateHistory implements Serializable {

	private static final long serialVersionUID = -9088312934466281792L;

	@PrimaryKey("id")
	private UUID id;

	@Column("start_date")
	private Date startDate;

	@Column("end_date")
	private Date endDate;

	@Column("status")
	private DatabaseUpdateStatus status;

	@Column("created_at")
	private Date createdAt;
}