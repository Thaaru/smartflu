package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InfluPostTypeMaster {
private long postTypeId;

private String postTypeName;

private int platformId;
    
}
