package io.smartfluence.cassandra.repository;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.PromoteTargetingDetails;

@Repository
public interface PromoteTargetingDetailsRepo extends CassandraRepository<PromoteTargetingDetails, UUID> {

}