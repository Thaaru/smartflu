package io.smartfluence.cassandra.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.InfluencerPrice;
import io.smartfluence.cassandra.entities.primary.InfluencerPriceKey;

@Repository
public interface InfluencerPriceRepo extends CassandraRepository<InfluencerPrice, InfluencerPriceKey> {

	List<InfluencerPrice> findAllByKeyUserId(UUID id);
}