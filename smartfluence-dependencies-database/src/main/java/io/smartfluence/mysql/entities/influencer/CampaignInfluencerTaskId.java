package io.smartfluence.mysql.entities.influencer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignInfluencerTaskId implements Serializable{
    private String campaignId;
    private String influencerId;
    private int campaignTaskId;
}
