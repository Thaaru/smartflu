package io.smartfluence.cassandra.entities;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("campaign_export_to_csv_path")
public class CampaignExportToCsvPath {

	@PrimaryKey("campaign_id")
	private UUID campaignId;

	@Column("csv_path")
	private String csvPath;

	@Column("created_at")
	private Date createdAt;
}
