package io.smartfluence.brand.management.entityv1;

import java.time.LocalDateTime;
import java.util.List;

public interface ManageCampaignMapperNew {
    String getCampaign_id();
    String getCampaign_name();
    String getCampaign_status();
    int getInfluencer_count();
    List<String> getPlatform();
    LocalDateTime getCreated_at();
}
