package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StoriesItems implements Serializable {

	private static final long serialVersionUID = -3353353899903787820L;

	@JsonAlias("pk")
	private Long pk;

	@JsonAlias("taken_at")
	private Long takenAt;

	@JsonAlias("expiring_at")
	private Long expiringAt;

	@JsonAlias("media_type")
	private Integer mediaType;

	@JsonAlias("caption")
	private String caption;

	@JsonAlias("caption_is_edited")
	private Boolean isCaptionEdited;

	@JsonAlias("image_versions2")
	private ImageStories images;

	@JsonAlias("video_versions")
	private List<VideoStories> videos;

	@JsonAlias("has_audio")
	private Boolean hasAudio;

	@JsonAlias("video_duration")
	private Double videoDuration;
}