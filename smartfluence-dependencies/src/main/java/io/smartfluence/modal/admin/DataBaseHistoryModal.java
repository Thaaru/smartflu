package io.smartfluence.modal.admin;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DataBaseHistoryModal {

	private UUID id;

	private String startDate;

	private String endDate;

	private String status;
}