package io.smartfluence.influencer.services;

import io.smartfluence.influencer.entities.CustomResponseEntity;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.entities.UserDetails;
import io.smartfluence.influencer.repository.UserAccountRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class UserAccountService {

    @Autowired
    UserAccountRepo userAccountRepo;

    public FullCampaignInfluencerDetails getBrandNameAndEmailDetails(String brandId, FullCampaignInfluencerDetails fullCampaignInfluencerDetails){
        Logger.getLogger("getBrandNameAndEmailDetails").log(Level.FINE,"Getting the BrandName And Email Details");
        UserDetails userDetails = userAccountRepo.findByUserId(brandId);
        if(userDetails!=null){
            fullCampaignInfluencerDetails.setBrandName(userDetails.getBrandName());
            fullCampaignInfluencerDetails.setBrandEmail(userDetails.getEmail());
        }
        return  fullCampaignInfluencerDetails;
    }

    public FullCampaignInfluencerDetails getInfluencerStatus(FullCampaignInfluencerDetails fullCampaignInfluencerDetails){
        Logger.getLogger("getInfluencerStatus").log(Level.FINE,"Getting the User Account Details");
        CustomResponseEntity customResponseEntity = new CustomResponseEntity();
        UserDetails userDetails = userAccountRepo.findByEmail(fullCampaignInfluencerDetails.getInfluencerEmail());
        if(userDetails!=null){
            fullCampaignInfluencerDetails.setInfluencerAccountStatus("Active");
            if(userDetails.getPassword()!=null){
                fullCampaignInfluencerDetails.setInfluencerPasswordStatus("Active");
                return fullCampaignInfluencerDetails;
            }else{
                return fullCampaignInfluencerDetails;
            }

        }

        return  fullCampaignInfluencerDetails;
    }


    public CustomResponseEntity getUserAccountDetails(String email){
        Logger.getLogger("getUserAccountDetails").log(Level.FINE,"Getting the User Account Details");
        CustomResponseEntity customResponseEntity = new CustomResponseEntity();
        UserDetails userDetails = userAccountRepo.findByEmail(email);
        if(userDetails!=null){
            customResponseEntity.setCode("600");
            customResponseEntity.setValue(userDetails);
            return  customResponseEntity;
        }
        customResponseEntity.setCode("615");
        customResponseEntity.setValue(null);
        return  customResponseEntity;
    }

}
