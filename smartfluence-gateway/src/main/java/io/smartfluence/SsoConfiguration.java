package io.smartfluence;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2AuthenticationFailureEvent;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetailsSource;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.Assert;
import org.springframework.web.filter.CompositeFilter;

import io.smartfluence.constants.SecurityConstants;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Configuration
@EnableOAuth2Client
public class SsoConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	OAuth2ClientContext oauth2ClientContext;

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().anonymous() //allow anonymous access
		.and()
		.authorizeRequests()
		.antMatchers("/influencer/pub/**").permitAll()
		.and()
		.antMatcher("/**").authorizeRequests()
		.antMatchers("/admin/**").hasAuthority(SecurityConstants.ADMIN.getAuthority())
		.antMatchers("/brand/**").hasAuthority(SecurityConstants.BRAND.getAuthority())
		.antMatchers("/influencer/**").hasAuthority(SecurityConstants.INFLUENCER.getAuthority())
		.and().httpBasic().disable()
		.addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class).logout()
		.deleteCookies("ACCOUNTS", "SMARTFLUENCE").logoutSuccessUrl("/");

	}

	private Filter ssoFilter() {
		CompositeFilter filter = new CompositeFilter();
		List<Filter> filters = new LinkedList<>();
		filters.add(ssoFilter(smartfluence(), "/"));
		filters.add(ssoFilter(smartfluence(), "/login"));
		filters.add(ssoFilter(facebook(), "/login/facebook"));
		filter.setFilters(filters);

		return filter;
	}

	private Filter ssoFilter(ClientResources client, String path) {

		OAuth2ClientAuthenticationProcessingFilter filter = new OAuth2ClientAuthenticationProcessingFilter(path);
		OAuth2RestTemplate template = new OAuth2RestTemplate(client.getClient(), oauth2ClientContext);
		filter.setRestTemplate(template);
		filter.setTokenServices(tokenServices(client, template));
		filter.setAuthenticationSuccessHandler(new SimpleUrlAuthenticationSuccessHandler("/user/me"));

		return filter;
	}

	private ResourceServerTokenServices tokenServices(ClientResources client, OAuth2RestTemplate template) {

		if (client.getClient().getId().equals("smartfluence")) {
			RemoteTokenServices tokenServices = new RemoteTokenServices();
			tokenServices.setCheckTokenEndpointUrl(client.getResource().getTokenInfoUri());
			tokenServices.setClientId(client.getClient().getClientId());
			tokenServices.setClientSecret(client.getClient().getClientSecret());
			tokenServices.setRestTemplate(template);

			return tokenServices;
		} else {
			UserInfoTokenServices tokenServices = new UserInfoTokenServices(client.getResource().getUserInfoUri(),
			        client.getClient().getClientId());
			tokenServices.setRestTemplate(template);

			return tokenServices;
		}
	}

	@Bean
	@ConfigurationProperties("facebook")
	public ClientResources facebook() {
		return new ClientResources();
	}

	@Bean
	@ConfigurationProperties("smartfluence")
	public ClientResources smartfluence() {
		return new ClientResources();
	}

	@Bean
	public FilterRegistrationBean<Filter> oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
		FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<>();
		registration.setFilter(filter);
		registration.setOrder(-100);

		return registration;
	}

	class OAuth2ClientAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {

		public OAuth2RestOperations restTemplate;

		private ResourceServerTokenServices tokenServices;

		private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new OAuth2AuthenticationDetailsSource();

		private ApplicationEventPublisher eventPublisher;

		public void setTokenServices(ResourceServerTokenServices tokenServices) {
			this.tokenServices = tokenServices;
		}

		public void setRestTemplate(OAuth2RestOperations restTemplate) {
			this.restTemplate = restTemplate;
		}

		@Override
		public void setApplicationEventPublisher(ApplicationEventPublisher eventPublisher) {
			this.eventPublisher = eventPublisher;
			super.setApplicationEventPublisher(eventPublisher);
		}

		public OAuth2ClientAuthenticationProcessingFilter(String defaultFilterProcessesUrl) {
			super(defaultFilterProcessesUrl);
			setAuthenticationManager(new NoopAuthenticationManager());
			setAuthenticationDetailsSource(authenticationDetailsSource);
		}

		@Override
		public void afterPropertiesSet() {
			Assert.state(restTemplate != null, "Supply a rest-template");
			super.afterPropertiesSet();
		}

		@Override
		public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
		throws AuthenticationException, IOException, ServletException {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			log.info(authentication);
			if (authentication != null && authentication.isAuthenticated()) {
				return authentication;
			}
			OAuth2AccessToken accessToken;
			try {
				accessToken = restTemplate.getAccessToken();
			} catch (OAuth2Exception e) {
				BadCredentialsException bad = new BadCredentialsException("Could not obtain access token", e);
				publish(new OAuth2AuthenticationFailureEvent(bad));
				throw bad;
			}
			try {
				OAuth2Authentication result = tokenServices.loadAuthentication(accessToken.getValue());
				if (authenticationDetailsSource != null) {
					request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_VALUE, accessToken.getValue());
					request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE, accessToken.getTokenType());
					result.setDetails(authenticationDetailsSource.buildDetails(request));
				}
				publish(new AuthenticationSuccessEvent(result));
				return result;
			} catch (InvalidTokenException e) {
				BadCredentialsException bad = new BadCredentialsException("Could not obtain user details from token",
				        e);
				publish(new OAuth2AuthenticationFailureEvent(bad));
				throw bad;
			}
		}

		private void publish(ApplicationEvent event) {
			if (eventPublisher != null) {
				eventPublisher.publishEvent(event);
			}
		}

		@Override
		protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
		                                        FilterChain chain, Authentication authResult) throws IOException, ServletException {
			super.successfulAuthentication(request, response, chain, authResult);
			restTemplate.getAccessToken();
		}

		@Override
		protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
		        AuthenticationException failed) throws IOException, ServletException {
			if (failed instanceof AccessTokenRequiredException) {
				// Need to force a redirect via the OAuth client filter, so rethrow here
				throw failed;
			} else {
				// If the exception is not a Spring Security exception this will result in a
				// default error page
				super.unsuccessfulAuthentication(request, response, failed);
			}
		}
	}

	private static class NoopAuthenticationManager implements AuthenticationManager {

		@Override
		public Authentication authenticate(Authentication authentication) throws AuthenticationException {
			throw new UnsupportedOperationException("No authentication should be done with this AuthenticationManager");
		}
	}
}