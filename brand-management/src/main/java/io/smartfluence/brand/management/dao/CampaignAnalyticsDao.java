package io.smartfluence.brand.management.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.http.ResponseEntity;

import io.smartfluence.cassandra.entities.PostAnalyticsData;
import io.smartfluence.cassandra.entities.PostAnalyticsTriggers;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.entities.instagram.InstagramCountryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.instagram.InstagramIndustryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramStateDemographic;
import io.smartfluence.cassandra.entities.tiktok.TikTokCountryDemographic;
import io.smartfluence.cassandra.entities.tiktok.TikTokDemographics;
import io.smartfluence.cassandra.entities.youtube.YouTubeCountryDemographic;
import io.smartfluence.cassandra.entities.youtube.YouTubeDemographics;
import io.smartfluence.modal.brand.dashboard.PostDataModal;

public interface CampaignAnalyticsDao {

	ResponseEntity<Object> getAnalyticsData(UUID campaignId);

	List<PostAnalyticsTriggers> getPostAnalyticsTriggers(Boolean isTriggerEnd);

	List<PostAnalyticsData> getPostAnalyticsDataByBrandIdAndCampaignId(UUID brandId, UUID campaignId);

	void updatePostAnalyticsTriggers(PostAnalyticsTriggers postAnalyticsTriggers);

	void insertPostData(PostAnalyticsTriggers postAnalyticsTriggers, PostDataModal postDataModal, Date updateDate);

	Set<BrandCampaignDetails> getBrandCampaignDetailsByBrandIdAndCampaignId(UUID brandId, UUID campaignId);

	List<InstagramDemographics> getInstagramDemographicsById(List<String> influencerHandles);

	List<InstagramCountryDemographic> getInstagramCountryDemographicByKey(List<String> influencerHandles);

	List<InstagramStateDemographic> getInstagramStateDemographicByKey(List<String> instagramHandles);

	List<InstagramIndustryDemographic> getInstagramIndustryDemographicByKey(List<String> instagramHandles);

	List<YouTubeDemographics> getYoutubeDemographicsById(List<String> influencerHandles);

	List<YouTubeCountryDemographic> getYoutubeCountryDemographicByKey(List<String> influencerHandles);

	List<TikTokDemographics> getTiktokDemographicsById(List<String> influencerHandles);

	List<TikTokCountryDemographic> getTiktokCountryDemographicByKey(List<String> influencerHandles);
}