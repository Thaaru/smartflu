package io.smartfluence.modal.validation.custom;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.ObjectUtils;

import io.smartfluence.modal.validation.brand.explore.GetInfluencersRequestModel;

public class ExploreInfluencerValidations implements ConstraintValidator<Conditional, GetInfluencersRequestModel> {

	@Override
	public boolean isValid(GetInfluencersRequestModel value, ConstraintValidatorContext context) {

		/*if (value.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name())) {
			if (ObjectUtils.isEmpty(value.getAudienceBrandId()) || ObjectUtils.isEmpty(value.getAudienceBrandWeight()))
				return false;
		}*/

		if (value.getSortBy().equals("audience_relevance")) {
			if (ObjectUtils.isEmpty(value.getComparisonProfile()))
				return false;
		}
		return true;
	}

	@Override
	public void initialize(Conditional constraintAnnotation) {

	}
}