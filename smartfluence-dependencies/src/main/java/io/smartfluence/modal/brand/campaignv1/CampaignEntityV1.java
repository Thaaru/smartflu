package io.smartfluence.modal.brand.campaignv1;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CampaignEntityV1 {
    private String campaignId;
    private String brandId;
    @NotEmpty(message = "brand description should not be empty")
    @Size(min = 1,max = 5000,message = "brand description should be max 5000 characters")
    private String brandDescription;
    @NotEmpty(message = "campaign name should not be empty")
    @Size(min = 1,max = 50,message = "campaign name should be max 50 characters")
    private String campaignName;
    @NotEmpty(message = "campaign description should not be empty")
    @Size(min = 1,max = 5000,message = "campaign description should be  max 5000 characters")
    private String campaignDescription;
    private CampaignStatusEntityV1 campaignStatus;
    private String hashTags;
    @Size(max = 2500,message = "restrictions should be  max 2500 characters")
    private String restrictions;
    @Min(value = 1,message = "product count should be minimum 1")
    @Max(value = 3,message = "product count should be maximum 3")
    private int maxProductCount;
    private Long termId;
    private StatusType statusType;
    private TermsConditionsEntityV1 termsCondition;
    @NotEmpty(message = "offers shouldn't be empty")
    private List<CampaignOffersEntityV1> offers;
    @NotEmpty(message ="products shouldn't be empty")
    private List<CampaignProductsEntityV1> products;
    @NotEmpty(message ="tasks shouldn't be empty")
    private List<CampaignTasksEntityV1> tasks;
    private List<PlatformMasterEntityV1> platformMaster;
    private List<PlatformPostTypeEntityV1> platformPostMaster;
    private List<TermsConditionsEntityV1> termsConditionMaster;
    private List<CurrencyMasterEntityV1> currencyMaster;
    private List<CommissionAffiliateMasterEntityV1> commissionAffiliateMaster;
    private Map<String, String> errorObj;
    private String termType;
    private String tempCampaignId;
    private Long tempTermsId;
}
