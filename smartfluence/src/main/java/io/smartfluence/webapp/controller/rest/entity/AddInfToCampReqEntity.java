//package io.smartfluence.webapp.controller.rest.entity;
//
//import lombok.*;
//
//import java.time.LocalDateTime;
//
//@Getter
//@Setter
//@Builder
//@ToString
//@NoArgsConstructor
//@AllArgsConstructor
//public class AddInfToCampReqEntity {
//
//    private String campaignId;
//    private String influencerId;
//    private String influencerEmail;
//    private String platform;
//    private String proposalStatus;
//    private String isOfferAccepted;
//    private LocalDateTime OfferAcceptedAt;
//    private String isProductsAccepted;
//    private LocalDateTime productsAcceptedAt;
//    private String isDeliverablesAccepted;
//    private LocalDateTime deliverablesAcceptedAt;
//    private String isShippingAccepted;
//    private LocalDateTime shippingAcceptedAt;
//    private String isPaymentAccepted;
//    private LocalDateTime paymentAcceptedAt;
//    private String isTermsAccepted;
//    private LocalDateTime termsAcceptedAt;
//    private String paymentMethodType;
//    private String paymentEmailAddress;
//    private LocalDateTime campaignPaidAt;
//
////    private String influencerHandle;
////
////    private String influencerAudienceCredibility;
////
////    private String influencerEngagement;
////
////    private String influencerFollowers;
////
////    private String influencerFirstName;
////
////    private String influencerLastName;
//}
