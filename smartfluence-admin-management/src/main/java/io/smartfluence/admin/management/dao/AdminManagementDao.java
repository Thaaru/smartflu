package io.smartfluence.admin.management.dao;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import io.smartfluence.cassandra.entities.DatabaseUpdateHistory;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSubscriptions;
import io.smartfluence.cassandra.entities.instagram.InstagramContent;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.mysql.entities.UserAccounts;

public interface AdminManagementDao {

	List<InstagramDemographics> getInfluencers();

	DatabaseUpdateHistory saveInDBHistory();

	List<InstagramContent> getInstagramContent(String influencerHandle);

	void updateDB(List<InstagramDemographics> instagramDemographics, List<InstagramContent> contents, UUID updateId);

	List<DatabaseUpdateHistory> getDBUpdateHistory();

	void saveBrandDetails(BrandDetails brandDetails);

	void deleteBrandDetails(BrandDetails brandDetails);

	Optional<BrandDetails> getBrandDetails(UserDetailsKey key);

	Set<BrandDetails> getBrandDetailsByUserStatus(Set<UserStatus> userStatus);

	Optional<BrandDetails> getBrandDetailsByUserStatusAndUserId(Set<UserStatus> userStatus, UUID userId);

	void saveUserAccounts(UserAccounts userAccounts);

	Optional<UserAccounts> getUserAccounts(String brandId);

	SubscriptionMaster getSubscriptionMaster(UUID subscriptionId, SubscriptionType subscriptionType);

	List<SubscriptionMaster> getAllSubscriptionMaster();

	void saveBrandSubscriptions(BrandSubscriptions brandSubscriptions);

	void deleteBrandSubscriptions(BrandSubscriptions brandSubscriptions);

	Optional<BrandSubscriptions> getBrandSubscriptionsByBrandIdAndIsActive(UUID brandId);

	void saveBrandCredits(BrandCredits brandCredits);

	List<ReportSearchHistory> findAllBySearchId(UUID searchId);
}