package io.smartfluence.influencer;

import io.smartfluence.beans.WebContainer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;

@RefreshScope
@SpringBootApplication
@EnableDiscoveryClient
public class InfluencerManagementApplication {

	public static void main(String[] args) {

		SpringApplication.run(InfluencerManagementApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(prefix="smartfluence",name="use-ssl")
	public WebContainer webContainer() {
		return new WebContainer();
	}

}

