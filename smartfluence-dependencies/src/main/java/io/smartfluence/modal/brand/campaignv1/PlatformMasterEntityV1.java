package io.smartfluence.modal.brand.campaignv1;

import lombok.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlatformMasterEntityV1 {
    private Long platformId;
    private String platformName;
}
