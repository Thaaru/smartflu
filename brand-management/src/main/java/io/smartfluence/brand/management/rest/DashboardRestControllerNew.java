package io.smartfluence.brand.management.rest;


import io.smartfluence.brand.management.service.DashBoardServiceNew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("brand")
public class DashboardRestControllerNew {
    @Autowired
    DashBoardServiceNew dashBoardServiceNew;

    @GetMapping("view-campaign-v1")
    ResponseEntity<Object> getCampaignNew() {
        return ResponseEntity.ok(dashBoardServiceNew.getListOfCampaigns());
    }
}
