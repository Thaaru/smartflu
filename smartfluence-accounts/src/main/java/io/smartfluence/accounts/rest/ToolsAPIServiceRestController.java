package io.smartfluence.accounts.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.accounts.service.ToolsAPIService;
import io.smartfluence.modal.validation.DownloadReportModal;
import io.smartfluence.modal.validation.GenerateReportModal;
import io.smartfluence.modal.validation.InfluencerDataModal;
import io.smartfluence.modal.validation.InfluencerLookalikeModal;
import io.smartfluence.modal.validation.InfluencerOverlapModal;

@RestController
public class ToolsAPIServiceRestController {

	@Autowired
	private ToolsAPIService toolsService;

	@GetMapping("verify-user-recaptcha")
	public ResponseEntity<Object> verifyUserRecaptcha(@RequestParam String value) {
		return toolsService.verifyUserRecaptcha(value);
	}

	@GetMapping("get-influencer-data")
	public ResponseEntity<Object> getInfluencerData(@Validated @ModelAttribute InfluencerDataModal influencerDataModal, BindingResult bindingResult,
			HttpServletRequest servletRequest) {
		if (!bindingResult.hasFieldErrors())
			return toolsService.getInfluencerData(influencerDataModal, servletRequest);

		return new ResponseEntity<>("Mandatory fields required", HttpStatus.BAD_REQUEST);
	}

	@GetMapping("get-influencers-overlap")
	public ResponseEntity<Object> getInfluencersOverlap(@Validated @ModelAttribute InfluencerOverlapModal influencerOverlapModal, BindingResult bindingResult,
			HttpServletRequest servletRequest) {
		if (!bindingResult.hasFieldErrors())
			return toolsService.getInfluencersOverlap(influencerOverlapModal, servletRequest);

		return new ResponseEntity<>("Mandatory fields required", HttpStatus.BAD_REQUEST);
	}

	@GetMapping("get-influencer-lookalike")
	public ResponseEntity<Object> getInfluencerslookalike(@Validated @ModelAttribute InfluencerLookalikeModal influencerLookalikeModal,
			BindingResult bindingResult, HttpServletRequest servletRequest) {
		if (!bindingResult.hasFieldErrors())
			return toolsService.getInfluencerslookalike(influencerLookalikeModal, servletRequest);
	
		return new ResponseEntity<>("Mandatory fields required", HttpStatus.BAD_REQUEST);
	}

	@GetMapping("get-audience-report")
	public ResponseEntity<Object> getAudienceReportId(@Validated @ModelAttribute GenerateReportModal generateReportModal, BindingResult bindingResult
			, HttpServletRequest servletRequest) {

		if (!bindingResult.hasFieldErrors())
			return toolsService.getAudienceReportId(generateReportModal);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("download-report")
	public ResponseEntity<Object> getAudienceReport(@ModelAttribute DownloadReportModal downloadReportModal, BindingResult bindingResult,
			HttpServletRequest servletRequest) {
		if (!bindingResult.hasFieldErrors())
			return toolsService.getAudienceReport(downloadReportModal, servletRequest);

		return new ResponseEntity<>("Mandatory fields required", HttpStatus.BAD_REQUEST);
	}
}