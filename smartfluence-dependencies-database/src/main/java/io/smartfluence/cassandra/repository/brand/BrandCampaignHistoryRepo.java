package io.smartfluence.cassandra.repository.brand;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.brand.BrandCampaignHistory;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignHistoryKey;

public interface BrandCampaignHistoryRepo extends CassandraRepository<BrandCampaignHistory, BrandCampaignHistoryKey> {

}