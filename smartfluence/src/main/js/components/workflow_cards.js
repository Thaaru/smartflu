import React from "react";
import { IoIosCheckmarkCircle } from "react-icons/io";
import { BBB_COLOR, BLACK_COLOR, GREEN_COLOR, GREY_COLOR, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";

export function CardName({acceptedStatus,buttonHandler,cardName}){
    return <div className="col-md">
    <div className="row " style={{ "alignItems": "center" }}>
        <IoIosCheckmarkCircle size={30} className="dot1" style={{ color: acceptedStatus == STEPS_ACCEPTED_STATUS ? GREEN_COLOR : BBB_COLOR, "cursor": "pointer" }} />
        <a style={{ "textDecoration": "underline", color: acceptedStatus == STEPS_ACCEPTED_STATUS ? GREEN_COLOR : BLACK_COLOR, "cursor": "pointer" }} className="heading sm" onClick={buttonHandler}> {cardName}</a>
    </div>
</div>
}

export function CardVLine(){
    return <div className="col-md justify-content-center" >
    <div className="vl" style={{ "color": GREY_COLOR}} ></div>
</div>
}