package io.smartfluence.modal.social.user.tiktok;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokUserFeedItems implements Serializable {

	private static final long serialVersionUID = 6990507940134397071L;

	@JsonAlias("video")
	private RawTikTokUserVideo video;

	@JsonAlias("author")
	private RawTikTokAuthor author;
}