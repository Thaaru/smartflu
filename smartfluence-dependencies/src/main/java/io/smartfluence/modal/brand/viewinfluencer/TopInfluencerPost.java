package io.smartfluence.modal.brand.viewinfluencer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class TopInfluencerPost {

	@Default
	private String url = "";

	@Default
	private Object value = "";
}