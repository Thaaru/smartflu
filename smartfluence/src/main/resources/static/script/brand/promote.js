var platform = "instagram";
var audienceLocationName = '';
var influencerLocationName = '';
var comparisonProfileName = '';
var mailTemplate = '';
var mailSubject = '';

		$('#promoteHome')
							.DataTable(
									{
										dom : "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4 btn-explore'><'col-sm-12 col-md-6'f>>"
												+ "<'row'<'col-sm-12'tr>>"
												+ "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
										"order" : [ [ 0, "desc" ] ],
										drawCallback : function() {
											$('.btn-explore')
														.html('<a href="https://www.smartfluence.io/help/promote-by-smartfluence-faq" target="_blank" id="howToPromote" class="btn btn-primary btn-sm waves-effect" style="">How to use Promote</a>');
										},
									});

		$('#influencerManagement')
							.DataTable(
									{
										dom : "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4 '><'col-sm-12 col-md-6'f>>"
												+ "<'row'<'col-sm-12'tr>>"
												+ "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
										"order" : [ [ 0, "desc" ] ],
										drawCallback : function() {

										},
									});

$(document).ready(function() {
	var utility = "location";

	$('#audienceLocation').typeahead({
		minLength : 3,
		source : function(name, result) {
			if (name.trim().length > 0) {
				$.ajax({
					url			: window.location.origin + "/brand/get-utilities/" + name + "?data=" + utility
					,method		: "GET"
					,dataType	: "json"
					,success	: function(data) {
						audienceLocationName = '';
						result(data);
					}
				})
			}
		},
		updater : function(selection) {
			audienceLocationName = selection.name;
			return selection;
		}
	});

	$('#influencerLocation').typeahead({
		minLength : 3,
		source : function(name, result) {
			if (name.trim().length > 0) {
				$.ajax({
					url			: window.location.origin + "/brand/get-utilities/" + name + "?data=" + utility
					,method		: "GET"
					,dataType	: "json"
					,success	: function(data) {
						influencerLocationName = '';
						result(data);
					}
				})
			}
		},
		updater : function(selection) {
			influencerLocationName = selection.name;
			return selection;
		}
	});

	$('#comparisonProfile').typeahead({
		minLength : 2,
		source : function(query, result) {
			if (query.trim().length > 0) {
				$.ajax({
					url			: window.location.origin + "/brand/get-suggestions/" + query + "?platform=" + platform
					,method		: "GET"
					,dataType	: "json"
					,success	: function(data) {
						comparisonProfileName = '';
						result(data);
					}
				})
			}
		},
		updater : function(selection) {
			comparisonProfileName = selection.name;
			return selection;
		}
	});

	if (promoteViewCampaign != '' || promoteViewCampaign != undefined) {
		processCampaignDetailsData(promoteViewCampaign);
	}
});

$('#preview').click(function() {
	$('pre').remove();
	$('#preview-subject').append("<pre>" + mailSubject + "</pre>");
	$('#preview-body').append("<pre>" + mailTemplate + "<br><br><br></pre>");
});

function processCampaignDetailsData(promoteCampaignData) {
	if (promoteCampaignData != null) {
		if (promoteCampaignData.pageType == 'view' || promoteCampaignData.pageType == 'edit') {
			$('#preview').removeClass("d-none");
			mailSubject  = promoteMail.mailSubject;
			mailTemplate = promoteMail.mailTemplate;
			$("#campaignId").val(promoteCampaignData.campaignId);
			$('#promoteCampaignName').prop("disabled", true);
			$("#promoteCampaignName").val(promoteCampaignData.campaignName);
			$("#promoteLabel").addClass("active");
			$("#brandDescription").val(promoteCampaignData.brandDescription);
			$("#campaignDescription").val(promoteCampaignData.campaignDescription);
			$("#postType").val(promoteCampaignData.postType);
			$("#payment").val(promoteCampaignData.payment);
			$("#product").val(promoteCampaignData.product);
			$("#productValue").val(promoteCampaignData.productValue);
			$("#requirements").val(promoteCampaignData.requirements);
			$("#restrictions").val(promoteCampaignData.restrictions);
			$("#activeUntil").val(promoteCampaignData.activeUntil);

			if (promoteCampaignData.product != 'NO') {
				$('#productValue').removeAttr("disabled");

				if (promoteCampaignData.product == 'PRODUCT_ONLY') {
					$('#postTypeRequired').addClass("d-none");
					$('#paymentRequired').addClass("d-none");
				}
			}

			$("#comparisonProfile").val(promoteCampaignData.comparisonProfile);
			comparisonProfileName = promoteCampaignData.comparisonProfile;

			var followersMin = promoteCampaignData.followersRange.substring(0, promoteCampaignData.followersRange.lastIndexOf('-'));
			var followersMax = promoteCampaignData.followersRange.substring(promoteCampaignData.followersRange.lastIndexOf('-') + 1);
			$('#followers').slider('setValue', [Number(followersMin), Number(followersMax)]);

			var engagementMin = promoteCampaignData.engagementsRange.substring(0, promoteCampaignData.engagementsRange.lastIndexOf('-'));
			var engagementMax = promoteCampaignData.engagementsRange.substring(promoteCampaignData.engagementsRange.lastIndexOf('-') + 1);
			$('#engagement').slider('setValue', [Number(engagementMin), Number(engagementMax)]);

			$("#audienceIndustry").val(promoteCampaignData.audienceIndustry);
			$("#influencerIndustry").val(promoteCampaignData.influencerIndustry);
			$("#audienceLocation").val(promoteCampaignData.audienceGeo);
			$("#influencerLocation").val(promoteCampaignData.influencerGeo);
			audienceLocationName = promoteCampaignData.audienceGeo;
			influencerLocationName = promoteCampaignData.influencerGeo;

			// Ages
			var audienceAgeFrom = '';
			var audienceAgeTo = '';

			// Age From
			if (promoteCampaignData.audienceAge.substring(0,2) == '13') {
				audienceAgeFrom = '13-17';
			}
			if (promoteCampaignData.audienceAge.substring(0,2) == '18') {
				audienceAgeFrom = '18-24';
			}
			if (promoteCampaignData.audienceAge.substring(0,2) == '25') {
				audienceAgeFrom = '25-34';
			}
			if (promoteCampaignData.audienceAge.substring(0,2) == '35') {
				audienceAgeFrom = '35-44';
			}
			if (promoteCampaignData.audienceAge.substring(0,2) == '45') {
				audienceAgeFrom = '45-64';
			}
			if (promoteCampaignData.audienceAge.substring(0,2) == '65') {
				audienceAgeFrom = '65+';
			}

			// Age To
			if (promoteCampaignData.audienceAge.slice(-2) == '24') {
				audienceAgeTo = '25-34';
			}
			if (promoteCampaignData.audienceAge.slice(-2) == '34') {
				audienceAgeTo = '35-44';
			}
			if (promoteCampaignData.audienceAge.slice(-2) == '44') {
				audienceAgeTo = '45-64';
			}
			if (promoteCampaignData.audienceAge.slice(-2) == '64') {
				audienceAgeTo = '65+';
			}
			if (promoteCampaignData.audienceAge.slice(-3) == '65+') {
				audienceAgeTo = '65+';
			}

			$("#audienceAgeFrom").val(audienceAgeFrom);
			$("#audienceAgeTo").val(audienceAgeTo);

			// Gender
			if (promoteCampaignData.audienceGender == 'MALE') {
				$('#audience-male-gender').prop('checked', true);
			} else if (promoteCampaignData.audienceGender == 'FEMALE') {
				$('#audience-female-gender').prop('checked', true);
			}

			if (promoteCampaignData.influencerGender == 'MALE') {
				$('#influencer-male-gender').prop('checked', true);
			} else if (promoteCampaignData.influencerGender == 'FEMALE') {
				$('#influencer-female-gender').prop('checked', true);
			}

			$("#influencerAgeFrom").val(promoteCampaignData.influencerAge.substring(0,2));
			$("#influencerAgeTo").val(promoteCampaignData.influencerAge.slice(-2));

			if (promoteCampaignData.pageType == 'view') {
				processViewPage();
			}
		}
	}
}

function processViewPage() {
	$('#createNewPromoteCampaign').addClass("d-none");
	$('#startPromoteCampaign').addClass("d-none");
	$('#brandDescription').prop("disabled", true);
	$('#campaignDescription').prop("disabled", true);
	$('#postType').prop("disabled", true);
	$('#payment').prop("disabled", true);
	$('#product').prop("disabled", true);
	$('#productValue').prop("disabled", true);
	$('#requirements').prop("disabled", true);
	$('#restrictions').prop("disabled", true);
	$('#activeUntil').prop("disabled", true);

	$('#comparisonProfile').prop("disabled", true);
	// sliders disabled
	$('#followers').slider('disable');
	$('#engagement').slider('disable');

	$('#audienceIndustry').prop("disabled", true);
	$('#influencerIndustry').prop("disabled", true);
	$('#audienceLocation').prop("disabled", true);
	$('#influencerLocation').prop("disabled", true);
	$('#audienceAgeFrom').prop("disabled", true);
	$('#audienceAgeTo').prop("disabled", true);

	$('#audience-male-gender').prop("disabled", true);
	$('#audience-female-gender').prop("disabled", true);

	$('#influencerAgeFrom').prop("disabled", true);
	$('#influencerAgeTo').prop("disabled", true);

	$('#influencer-male-gender').prop("disabled", true);
	$('#influencer-female-gender').prop("disabled", true);
}

$("#product").change(function() {
	if ($("#product").val() == 'YES') {
		$('#productValue').removeAttr("disabled");
		$('#productValueRequired').removeClass("d-none");
	} else {
		$("#productValue").val('');
		$('#productValue').prop("disabled", true);
		$('#productValueRequired').addClass("d-none");
	}
	if ($("#product").val() == 'PRODUCT_ONLY') {
		$("#payment").val('');
		$('#payment').prop("disabled", true);
		$('#productValue').removeAttr("disabled");
		$('#postTypeRequired').addClass("d-none");
		$('#paymentRequired').addClass("d-none");
	} else {
		$('#payment').removeAttr("disabled");
		$('#postTypeRequired').removeClass("d-none");
		$('#paymentRequired').removeClass("d-none");
	}
});

$("#createNewPromoteCampaign").click(function() {
	var campaignId = '';
	var isEdit = false;

	campaignId = $("#campaignId").val();
	if (campaignId != '' && campaignId != undefined) {
		isEdit = true;
	}

	var campaignName = $("#promoteCampaignName").val();
	var brandDescription = $("#brandDescription").val();
	var campaignDescription = $("#campaignDescription").val();
	var postType = $("#postType").val();
	var payment = $("#payment").val();
	var product = $("#product").val();
	var productValue = $("#productValue").val();
	var requirements = $("#requirements").val();
	var restrictions = $("#restrictions").val();
	var activeUntil = $("#activeUntil").val().replace(/(\d*)-(\d*)-(\d*)/,'$2-$3-$1');

	var followersMin = followerSlider.val().split(',')[0];
	var followersMax = followerSlider.val().split(',')[1];
	var followersRange = followersMin + '-' + followersMax;
	var engagementMin = engagementSlider.val().split(',')[0];
	var engagementMax = engagementSlider.val().split(',')[1];
	var engagementsRange = engagementMin + '-' + engagementMax;

	var audienceAgeFrom = $("#audienceAgeFrom").val();
	var audienceAgeTo = $("#audienceAgeTo").val();
	var influencerAgeFrom = $("#influencerAgeFrom").val();
	var influencerAgeTo = $("#influencerAgeTo").val();
	var audienceAgeRanges = setAgeRanges(audienceAgeFrom, audienceAgeTo, 1);
	var influencerAgeRanges = setAgeRanges(influencerAgeFrom, influencerAgeTo, 2);

	var audienceIndustry = $("#audienceIndustry").val();
	var influencerIndustry = '';	// $("#influencerIndustry").val();
	var audienceLocation = audienceLocationName;
	var influencerLocation = '';	// influencerLocationName;
	var comparisonProfile = comparisonProfileName;

	var audienceGender = '';
	var influencerGender = '';

	if ($('#audience-male-gender').is(":checked")) {
		audienceGender = $('#audience-male-gender').val();
	} else if ($('#audience-female-gender').is(":checked")) {
		audienceGender = $('#audience-female-gender').val();
	}

	if ($('#influencer-male-gender').is(":checked")) {
		influencerGender = $('#influencer-male-gender').val();
	} else if ($('#influencer-female-gender').is(":checked")) {
		influencerGender = $('#influencer-female-gender').val();
	}

	// Validation Start
	var selectedDate = new Date(activeUntil);
	var now = new Date();

	if (selectedDate < now) {
		WARNING("ActiveUntil Date must be in the future");
		return;
	}

	if (campaignName == '' || campaignName == undefined) {
		WARNING("Enter Campaign Name");
		return;
	}
	if (brandDescription == '' || brandDescription == undefined) {
		WARNING("Enter Brand Description");
		return;
	}
	if (campaignDescription == '' || campaignDescription == undefined) {
		WARNING("Enter Campaign Description");
		return;
	}
	if (product == '' || product == undefined) {
		WARNING("Select Product");
		return;
	}
	if (product == 'YES' && (productValue == '' || productValue == '0')) {
		WARNING("Enter Product Value");
		return;
	}
	if (product != 'PRODUCT_ONLY') {
		if (postType == '' || postType == undefined) {
			WARNING("Select Post Type");
			return;
		}
		if (payment == '0' || payment == '') {
			WARNING("Enter Payment");
			return;
		}
	}
	if (activeUntil == '' || activeUntil == undefined) {
		WARNING("Select Active Until Date");
		return;
	}
	if (comparisonProfile == '' || comparisonProfile == undefined) {
		WARNING("Enter Comparison Profile");
		return;
	}
	if (audienceIndustry == '' || audienceIndustry == undefined) {
		WARNING("Enter Audience Industry");
		return;
	}
	if (audienceLocation == '' || audienceLocation == undefined) {
		WARNING("Enter Audience Location");
		return;
	}
	// Validation End

	$('#createNewPromoteCampaign').addClass("disabled");

	$.post({
		url : window.location.origin + '/brand/sf-promote/manage-promote-campaign',
		data : {
			campaignId				: campaignId
			,campaignName			: campaignName
			,brandDescription		: brandDescription
			,campaignDescription	: campaignDescription
			,comparisonProfile		: comparisonProfile
			,postType				: postType
			,payment				: payment
			,product				: product
			,productValue			: productValue
			,requirements			: requirements
			,restrictions			: restrictions
			,activeUntil			: activeUntil
			,platform				: platform
			,followersRange			: followersRange
			,engagementsRange		: engagementsRange
			,audienceIndustry		: audienceIndustry
			,influencerIndustry		: influencerIndustry
			,audienceGeo			: audienceLocation
			,influencerGeo			: influencerLocation
			,audienceAge			: audienceAgeRanges
			,influencerAge			: influencerAgeRanges
			,audienceGender			: audienceGender
			,influencerGender		: influencerGender
			,additionalInfo			: ''
			,isEdit					: isEdit
		},
	}).done(function(data) {
		SUCCESS(data.responseMessage);
		$('#preview').removeClass("d-none");
		mailTemplate = data.mailTemplate;
		mailSubject = data.mailSubject;
		$("#startPromoteCampaign").removeClass("not-saved");
		$("#startPromoteCampaign").attr('onclick', 'processCampaign("' + data.campaignId + '","start")');
		$("#campaignId").val(data.campaignId);
		$('#createNewPromoteCampaign').removeClass("disabled");
	}).fail(function(data) {
		if (data.status == 400) {
			WARNING("Mandatory fields required..!");
		} else if (data.status != 500) {
			ERROR(data.responseText);
		} else {
			ERROR("Failed to save Promote Campaign");
		}
		$('#preview').addClass("d-none");
		$("#startPromoteCampaign").addClass("not-saved");
		$('#createNewPromoteCampaign').removeClass("disabled");
	});
})

 $("#startPromoteCampaign").click(function() {
	if($("#startPromoteCampaign").hasClass("not-saved")) {
		WARNING("Save your campaign before starting");
		return;
	}
});

function processCampaign(campaignId, action) {
	$("#startPromoteCampaign").addClass("disabled");
	if (action == 'start') {
		$('#promoteCampaignStartBth' + campaignId).addClass("disabled");
	} else if (action == 'paused') {
		$('#promoteCampaignPauseBth' + campaignId).addClass("disabled");
	} else if (action == 'resumed') {
		$('#promoteCampaignResumeBth' + campaignId).addClass("disabled");
	}

	$.post({
		url : window.location.origin + '/brand/sf-promote/process-campaign',
		data : {
			campaignId	: campaignId
			,action		: action
		},
	}).done(function(data) {
		SUCCESS(data);
		removeCampaignBtnDisables(campaignId, action);
		window.location.replace(window.location.origin + '/brand/sf-promote');
	}).fail(function(data) {
		if (data.status != 500) {
			ERROR(data.responseText);
		} else {
			ERROR("Failed to start Promote Campaign");
		}
		$('#createNewPromoteCampaign').removeClass("disabled");
		removeCampaignBtnDisables(campaignId, action);
	});
}

function removeCampaignBtnDisables(campaignId, action) {
	if (action = 'start') {
		$('#promoteCampaignStartBth' + campaignId).removeClass("disabled");
	} else if (action = 'paused') {
		$('#promoteCampaignPauseBth' + campaignId).removeClass("disabled");
	} else if (action = 'resumed') {
		$('#promoteCampaignResumeBth' + campaignId).removeClass("disabled");
	}
}

function approveInfluencer(campaignId, influencerDeepSocialId, action) {
	changeInfluencerStatus(campaignId, influencerDeepSocialId, action, $('#influencerApproveBth' + influencerDeepSocialId));
}

function rejectInfluencer(campaignId, influencerDeepSocialId, action) {
	changeInfluencerStatus(campaignId, influencerDeepSocialId, action, $('#influencerRejectBth' + influencerDeepSocialId));
}

function removeInfluencer(campaignId, influencerDeepSocialId, action) {
	changeInfluencerStatus(campaignId, influencerDeepSocialId, action, $('#influencerRemoveBth' + influencerDeepSocialId));
}

function changeInfluencerStatus(campaignId, deepSocialId, status, id) {
	$(id).addClass("disabled");

	$.post({
		url : window.location.origin + '/brand/sf-promote/influencer-action',
		data : {
			campaignId					: campaignId
			,influencerDeepSocialId		: deepSocialId
			,action						: status
		},
	}).done(function(data) {
		SUCCESS(data);
		$(id).removeClass("disabled");
		window.location.reload();
	}).fail(function(data) {
		if (data.status != 500) {
			ERROR(data.responseText);
		} else {
			ERROR("Failed to change status");
		}
		$(id).removeClass("disabled");
	});
}

function viewInfluencerAnalytics(influencerDeepSocialId, influencerSfId) {
	var isValid;
	var influencerId;

	if (influencerSfId == 'null' || influencerSfId == null || influencerSfId != '') {
		isValid = false;
		influencerId = influencerDeepSocialId;
	} else {
		isValid = true;
		influencerId = influencerSfId;
	}

	window.open(window.location.origin + '/brand/view-influencer/' + influencerId + '?isValid=' + isValid + '&platform=instagram');
}

function onlyOne(id, name) {
	var checkboxes = document.getElementsByName(name)
	checkboxes.forEach((item) => {
		if (item !== id) item.checked = false
	})
}

function setAgeRanges(from, to, flag) {
	var audienceFullAgeRange = "13-17,18-24,25-34,35-44,45-64,65+";
	var subStr = audienceFullAgeRange.match(from + "(.*)" + to);
	var ageRange = '';

	if (from != '' && to != '') {
		if (flag == 1) {
			if (from == '65+' && to == '65+') {
				ageRange = from;
			}
			else if (to == '65+') {
				ageRange = from + subStr[1] + to;
			} else {
				ageRange = from + subStr[1];
				ageRange = ageRange.substring(0, ageRange.length - 1);
			}
		}
		if (flag == 2) {
			ageRange = from + "-" + to;
		}
	}

	return ageRange;
}

$("#audienceAgeFrom").change(function() {
	var fromAge = $('#audienceAgeFrom').val();
	var ageContent = '';

	if (fromAge == '13-17') {
		ageContent += '<option value="18-24">18</option>';
		ageContent += '<option value="25-34">25</option>';
		ageContent += '<option value="35-44">35</option>';
		ageContent += '<option value="45-64">45</option>';
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '18-24') {
		ageContent += '<option value="25-34">25</option>';
		ageContent += '<option value="35-44">35</option>';
		ageContent += '<option value="45-64">45</option>';
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '25-34') {
		ageContent += '<option value="35-44">35</option>';
		ageContent += '<option value="45-64">45</option>';
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '35-44') {
		ageContent += '<option value="45-64">45</option>';
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '45-64') {
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '65+') {
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '') {
		ageContent += '<option value="">To</option>';
	}

	$('#audienceAgeTo').html(ageContent);
});

$("#influencerAgeFrom").change(function() {
	var fromAge = $('#influencerAgeFrom').val();
	var ageContent = '';

	if (fromAge == '18') {
		ageContent += '<option value="25">25</option>';
		ageContent += '<option value="35">35</option>';
		ageContent += '<option value="45">45</option>';
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '25') {
		ageContent += '<option value="35">35</option>';
		ageContent += '<option value="45">45</option>';
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '35') {
		ageContent += '<option value="45">45</option>';
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '45') {
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '65') {
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '') {
		ageContent += '<option value="">To</option>';
	}

	$('#influencerAgeTo').html(ageContent);
});

function formatNumber(val) {
	if (!isNaN(Number(val))) {
		var temp = Number(val);
		var prefix;
		if (temp < 1000) {
			prefix = '';
		} else if (temp < 1000000) {
			prefix = 'K';
		} else if (temp < 1000000000) {
			prefix = 'M';
		} else {
			prefix = 'B';
		}
		switch (prefix) {
		case 'K':
			temp = (temp / 1000);
			break;
		case 'M':
			temp = (temp / 1000000);
			break;
		case 'B':
			temp = (temp / 1000000000);
			break;
		}
		temp = temp.toFixed(2);
		temp = Number(temp).toLocaleString();
		temp += prefix;
		val = temp;
	} else {
		val = 0;
	}
	return val;
}

var engagementSlider = $('#engagement').slider({
	formatter : function(a) {
		var engagementEnd;
		if (typeof a == 'object') {
			if (formatNumber(a[1]) == 20) {
				engagementEnd = 'Max';
			} else {
				engagementEnd = formatNumber(a[1]);
			}
			return formatNumber(a[0]) + ' to ' + engagementEnd;
		}
		return formatNumber(a);
	}
});

var followerSlider = $('#followers').slider({
	formatter : function(a) {
		if (typeof a == 'object') {
			return formatNumber(a[0]) + ' to ' + formatNumber(a[1]);
		}
		return formatNumber(a);
	}
});

$('.slider.slider-horizontal').each(function() {
	var slider = $(this)
	slider.width(slider.parent().width()
			- slider.parent().find('.slider-left').width()
			- slider.parent().find('.slider-right').width() - 30);
});