package io.smartfluence.constants;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class SecurityConstants {

	public static final GrantedAuthority ADMIN = new SimpleGrantedAuthority("ROLE_ADMIN");
	public static final GrantedAuthority SMART_ADMIN = new SimpleGrantedAuthority("ROLE_SMART_ADMIN");
	public static final GrantedAuthority BRAND_PENDING = new SimpleGrantedAuthority("ROLE_BRAND_PENDING");
	public static final GrantedAuthority BRAND_APPROVED = new SimpleGrantedAuthority("ROLE_BRAND_APPROVED");
	public static final GrantedAuthority BRAND_FREE = new SimpleGrantedAuthority("ROLE_BRAND_FREE");
	public static final GrantedAuthority BRAND_PREMIUM = new SimpleGrantedAuthority("ROLE_BRAND_PREMIUM");
	public static final GrantedAuthority BRAND_DECLINED = new SimpleGrantedAuthority("ROLE_BRAND_DECLINED");
	public static final GrantedAuthority BRAND_INACTIVE = new SimpleGrantedAuthority("ROLE_BRAND_INACTIVE");
	public static final GrantedAuthority BRAND = new SimpleGrantedAuthority("ROLE_BRAND");
	public static final GrantedAuthority BRAND_ENTERPRISE = new SimpleGrantedAuthority("ROLE_BRAND_ENTERPRISE");
	public static final GrantedAuthority INFLUENCER = new SimpleGrantedAuthority("ROLE_INFLUENCER");
}