package io.smartfluence.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.AudienceReportHistory;
import io.smartfluence.cassandra.entities.primary.AudienceReportHistoryKey;

@Repository
public interface AudienceReportHistoryRepo extends CassandraRepository<AudienceReportHistory, AudienceReportHistoryKey> {

	AudienceReportHistory findByKeyBusinessEmailAddress(String businessEmailAddress);
}