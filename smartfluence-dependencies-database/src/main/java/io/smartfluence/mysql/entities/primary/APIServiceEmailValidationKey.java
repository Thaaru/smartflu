package io.smartfluence.mysql.entities.primary;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@Embeddable
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class APIServiceEmailValidationKey implements Serializable {

	private static final long serialVersionUID = 3055887681570577989L;

	@Column(name = "email_id")
	private String emailId;

	@Column(name = "service")
	private String service;
}