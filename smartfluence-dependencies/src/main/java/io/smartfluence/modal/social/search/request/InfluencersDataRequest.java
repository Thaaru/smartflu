package io.smartfluence.modal.social.search.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InfluencersDataRequest implements Serializable {

	private static final long serialVersionUID = -224235755676424858L;

	@JsonProperty("filter")
	private Filter filter;

	@JsonProperty("sort")
	private SortResponse sortResponse;

	@JsonProperty("paging")
	private Paging paging;

	@JsonProperty("audience_source")
	private String audienceSource;
}