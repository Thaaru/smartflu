package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.CampaignProductsV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CampaignProductsRepositoryV1 extends JpaRepository<CampaignProductsV1,Long> {
    List<CampaignProductsV1> findByCampaignId(String campaignId);
    void deleteByProductId(long campaignId);

}
