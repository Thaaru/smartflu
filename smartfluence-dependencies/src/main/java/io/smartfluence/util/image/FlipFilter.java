package io.smartfluence.util.image;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

class FlipFilter implements ImageFilter {

	@Override
	public BufferedImage processImageBuffer(BufferedImage bufferedImage) {
		AffineTransform at = new AffineTransform(new double[] { -1.0, 0.0, 0.0, 1.0 });
		at.translate(-bufferedImage.getWidth(), 0.0);
		BufferedImageOp filter = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		return filter.filter(bufferedImage, null);
	}
}