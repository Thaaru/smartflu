package io.smartfluence.brand.management.mailbean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Component
public class DateUtil {
    @Autowired
    Environment env;
    public Date getDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        // Convert Date to Calendar
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        // Perform addition/subtraction
        c.add(Calendar.YEAR, Integer.parseInt(Objects.requireNonNull(env.getProperty(Constants.YEAR))));
        c.add(Calendar.MONTH, Integer.parseInt(Objects.requireNonNull(env.getProperty(Constants.MONTH))));
        c.add(Calendar.DATE, Integer.parseInt(Objects.requireNonNull(env.getProperty(Constants.DATE))));
        c.add(Calendar.HOUR, Integer.parseInt(Objects.requireNonNull(env.getProperty(Constants.HOUR))));
        c.add(Calendar.MINUTE, Integer.parseInt(Objects.requireNonNull(env.getProperty(Constants.MINUTE))));
        c.add(Calendar.SECOND, Integer.parseInt(Objects.requireNonNull(env.getProperty(Constants.SECOND))));

        // Convert calendar back to Date

        return c.getTime();
    }
}
