package io.smartfluence.cassandra.repository.brand;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignDetailsKey;
import org.springframework.data.cassandra.repository.Query;

public interface BrandCampaignDetailsRepo extends CassandraRepository<BrandCampaignDetails, BrandCampaignDetailsKey> {

    Set<BrandCampaignDetails> findAllByKeyBrandIdAndKeyCampaignId(UUID brandId, UUID campaignId);

    List<BrandCampaignDetails> findAllByKeyCampaignId(UUID campaignId);

    Optional<BrandCampaignDetails> findByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(UUID brandId, UUID campaignId, UUID influencerId);
}
