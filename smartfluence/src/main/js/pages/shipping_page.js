import React, { useEffect, useState } from "react";
import { HeaderComponent } from "../components/header_component";
import ColoredLine from "../components/line_component";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import PhoneInput, { isValidPhoneNumber, parsePhoneNumber } from 'react-phone-number-input/input'
import 'react-phone-number-input/style.css'
import { getStateListApi, updateCampaignDetailsApi } from "../services/api/campaign_details_api";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { DETAILS_ACCEPTED_MESSAGE, ERROR_MESSAGE, SP_ARROWBUTTON_MESSAGE, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";
import { PoweredBy } from "../components/powered_by";
import { CProgressBar } from "../components/progress_bar";
import { loaderMethod } from "../components/loader";
import { ArrowButton } from "../components/arrow_button";


export default function ShippingPage({ data, code, next, back, close }) {
    const shippingAddresss = data.shippingAddress;
    const [backButtonController, setBackButtonController] = useState(0);
    const [frontButtonController, setFrontButtonController] = useState(0);
    const [stateList, setStateList] = useState(null);
    useEffect(() => {
        const shippingAddress = data.shippingAddress;
        if (shippingAddress != null) {
            setValue("fullName",  shippingAddress != null ? shippingAddress.fullName : "");
            setValue("addressLine1",  shippingAddress != null ? shippingAddress.addressLine1 : "");
            setValue("addressLine2", shippingAddress != null ? shippingAddress.addressLine2 : "");
            setValue("cityName", shippingAddress != null ? shippingAddress.cityName : "");
            setValue("zipcode",shippingAddress != null ? shippingAddress.zipcode : "");
            setValue("country",shippingAddress != null ? shippingAddress.countryId : "");
            setValue("state", shippingAddress != null ? shippingAddress.stateId : "");
            setValue("phoneNumber", shippingAddress != null ? shippingAddress.phoneNumber : "");
    
            getStateList(shippingAddress.countryId, setStateList);
        }
    }, [])
    const regExs = {
        "US": /^([0-9]{5})(?:[-\s]*([0-9]{4}))?$/,
        "other": /^[ABCEGHJ-NPRSTVXY][0-9][ABCEGHJ-NPRSTV-Z] [0-9][ABCEGHJ-NPRSTV-Z][0-9]$/,
    }
    const schema = yup.object().shape({
        fullName: yup.string().max(50).required("FullName is required").matches("^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$", "FullName must be alphabets only"),
        addressLine1: yup.string().required("AddressLine is required").matches("^[a-zA-Z0-9#]+(?:[\w --_,/]*[a-zA-Z0-9]+)*$", "Address should contains only alphaNumeric and (- , / #)"),
        addressLine2: yup.string().nullable().notRequired().when("addressLine2", {
            is: value => value?.length,
            then: rule => rule.matches("^[a-zA-Z0-9#]+(?:[\w --,/]*[a-zA-Z0-9]+)*$", "Address should contains only alphaNumeric and (- , / #)"),
        }),
        cityName: yup.string().max(20).required("CityName is required").matches("^[A-Za-z]+[A-Za-z ]*$", "City must be alphabets only"),
        zipcode: yup.string().required("Zip Code is required").test("validZipcode", "Zip Code is invalid", function (value) {
            let countryId = this.parent.country;
            let countryList = data.countryList;
            let country = countryList.find(x => x.countryId == countryId)?.countryName;
            let isValidZipcode = (regExs[country] ? regExs[country] : regExs['other']).test(value);
            return isValidZipcode;
        }),
        phoneNumber: yup.string().required("Phone Number is required").test("validPhonenumber", "Phone Number is invalid", function (value) {
                if (value != null && value != "") {
                  
                    const phoneNumber = parsePhoneNumber(value);      
                    if (phoneNumber != null && phoneNumber != '') {
                        if (phoneNumber.country == "US" || phoneNumber.country == "CA") {
                            return isValidPhoneNumber(value, phoneNumber.country);
                        } 
                    }
                }
                return false;
            }
            ),
        country: yup.string().required("Please select country"),
        state: yup.string().required("Please select state")

    },
        [
            // Add Cyclic deps here because when require itself
            ['addressLine2', 'addressLine2'],
        ]
    );
    const { register,
        control,
        watch,
        setValue,
        handleSubmit,
        getValues,
        clearErrors,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });
    const onSubmit = async (event) => {
        const shippingAddress = {};
        shippingAddress["campaignInfluencerAddressType"] = null;
        shippingAddress["fullName"] = event.fullName;
        shippingAddress["addressLine1"] = event.addressLine1;
        shippingAddress["addressLine2"] = event.addressLine2;
        shippingAddress["countryId"] = event.country;
        shippingAddress["cityName"] = event.cityName;
        shippingAddress["zipcode"] = event.zipcode;
        shippingAddress["phoneNumber"] = event.phoneNumber;
        shippingAddress["stateId"] = event.state;
        data.shippingAddress = shippingAddress;
        data.shippingAcceptedStatus = STEPS_ACCEPTED_STATUS;
        loaderOpen(false);
        const result = await updateCampaignDetailsApi(data);
        if (result.code == 600) {
            toast.success(DETAILS_ACCEPTED_MESSAGE, { delay: 1000 })
            loaderOpen(true);
            next(result.value);
        } else {
            toast.error(ERROR_MESSAGE, { delay: 1000 })
            loaderOpen(true);
            data.shippingAcceptedStatus = null;
            return null;
        }
    }
    const backButtonHandler = () => {
        if (backButtonController == 0) {
            const fieldData = getValues();
            if (fieldData.fullName != "" && fieldData.addressLine1 != "" && fieldData.country != "" && fieldData.cityName != "" && fieldData.zipcode != "" && fieldData.phoneNumber != "" && fieldData.state != "") {
                back();
            } else {
                setBackButtonController(1);
                toast.info(SP_ARROWBUTTON_MESSAGE);
            }
        } else {
            back();
        }
    }
    const frontButtonHandler = () => {
        if (frontButtonController == 0) {
            const fieldData = getValues();
            if (fieldData.fullName != "" && fieldData.addressLine1 != "" && fieldData.country != "" && fieldData.cityName != "" && fieldData.zipcode != "" && fieldData.phoneNumber != "" && fieldData.state != "") {
                next();
            } else {
                setFrontButtonController(1);
                toast.info(SP_ARROWBUTTON_MESSAGE);
            }
        } else {
            next();
        }
    }
    const handleChange = event => {
        if(event.target.value!=null){
            clearErrors("country")
            if(getValues("zipcode")!=null){
                clearErrors("zipcode")
            }
        }

        getStateList(event.target.value);
    }
    const getStateList = async (countryId) => {
        if (countryId != null) {
            const responeData = await getStateListApi(countryId);
            const stateList = responeData.value;
            setStateList(stateList);
        }
    }
    const [loaderComponent, setLoaderComponent] = useState(true);

    const loaderOpen = (status) => {
        if (status == true) {
            setLoaderComponent(status);
        } else {
            setLoaderComponent(status)
        }
    }
    return (
        <>
            <div className="content-wrapper">
                <form >
                    <div>
                        <HeaderComponent data={data} close={close} headerName={"Shipping Information"} stepNo={4} />
                        <ColoredLine color={'secondaryColor'} />
                        <div className="col-lg forScroll" style={{ "textAlign": "left" }}>
                            <div class="form-group col-lg">
                                <label for="fullName">Full Name</label>
                                <input class="form-control" {...register("fullName")}
                                    defaultValue={shippingAddresss != null ? shippingAddresss.fullName : null}
                                    error={errors.fullName ? true : false} />
                                <p style={{ "color": "red" }}>{errors.fullName?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="phoneNumber">Phone Number</label>
                                <PhoneInput
                                    initialValueFormat="national"
                                    className="form-control"
                                    value={shippingAddresss != null ? shippingAddresss.phoneNumber : null}
                                    countries={["US", "CA"]}
                                    onChange={(value) => {
                                        setValue('phoneNumber', value);
                                        clearErrors("phoneNumber")
                                    }}
                                />
                                <p style={{ "color": "red" }}>{errors.phoneNumber?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="country">Country</label>
                                <select class="form-control" id="country" 
                                 {...register("country")} 
                                 onChange={handleChange}
                                    defaultValue={shippingAddresss != null ? shippingAddresss.countryId :null}
                                    error={errors.country ? true : false} >
                                    <option ></option>
                                    {data.countryList && data.countryList.map((option) => (
                                        <option
                                            key={option.countryId}
                                            value={option.countryId}
                                        >
                                            {option.countryName}
                                        </option>
                                    ))}
                                </select>
                                <p style={{ "color": "red" }}>{errors.country?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="addressLine1">Address Line 1</label>
                                <input type="text"
                                    class="form-control"
                                    id="addressLine1"
                                    {...register("addressLine1")}
                                    defaultValue={shippingAddresss != null ? shippingAddresss.addressLine1 : null}
                                />
                                <p style={{ "color": "red" }}>{errors.addressLine1?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="addressLine2">Address Line 2 (Optional)</label>
                                <input type="text"
                                    class="form-control"
                                    id="addressLine2"
                                    {...register("addressLine2")}
                                    defaultValue={shippingAddresss != null ? shippingAddresss.addressLine2 : null} />
                            </div>
                            <div class="form-group col-lg">
                                <label for="cityName">City</label>
                                <input type="text"
                                    class="form-control"
                                    id="cityName"
                                    {...register("cityName")}
                                    defaultValue={shippingAddresss != null ? shippingAddresss.cityName : null}
                                    error={errors.cityName ? true : false} />
                                <p style={{ "color": "red" }}>{errors.cityName?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="state">State / Province</label>
                                {
                                    !stateList ? <input type="text"
                                        class="form-control"
                                        disabled="true"
                                    /> :
                                        <select class="form-control" id="state"{...register("state")}
                                            defaultValue={shippingAddresss != null ? shippingAddresss.stateId : null}
                                            error={errors.state ? true : false}
                                        >
                                            {stateList && stateList.map((option) => (
                                                <option
                                                    key={option.stateId}
                                                    value={option.stateId}
                                                >
                                                    {option.stateName}
                                                </option>
                                            ))}
                                        </select>
                                }
                                <p style={{ "color": "red" }}>{errors.state?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="zipcode">Zip Code</label>
                                <input type="text"
                                    class="form-control"
                                    id="zipcode"
                                    {...register("zipcode")}
                                    defaultValue={shippingAddresss != null ? shippingAddresss.zipcode : null}
                                    error={errors.zipcode ? true : false} />
                                <p style={{ "color": "red" }}>{errors.zipcode?.message}</p>
                            </div>
                        </div>
                        <div className="footer">
                            <ColoredLine color={'secondaryColor'} />
                            <div className="row " style={{ justifyContent: 'center', alignItems: "center" }}>
                                <ArrowButton status={STEPS_ACCEPTED_STATUS} navigator="backward" buttonHandler={backButtonHandler} />
                                <div class="col-6 ">
                                    <button type="button" class="btn btn-outline-secondary acceptButton" style={{ borderWidth: "2px", "borderColor": "#2F008D", color: "#2F008D", }} onClick={handleSubmit(onSubmit)}>
                                        {loaderComponent == true ? "Save & Accept" : loaderMethod()}
                                    </button>
                                </div>
                                <ArrowButton status={STEPS_ACCEPTED_STATUS} navigator="forward" buttonHandler={frontButtonHandler} />
                            </div>
                            <CProgressBar data={data} />
                            <PoweredBy />
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}