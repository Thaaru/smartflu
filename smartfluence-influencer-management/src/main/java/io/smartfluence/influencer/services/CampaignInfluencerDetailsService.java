package io.smartfluence.influencer.services;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.smartfluence.influencer.entities.CampaignInfluencerDetails;
import io.smartfluence.influencer.entities.CampaignInfluencerId;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.repository.CampaignInfluencerDetailsRepo;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class CampaignInfluencerDetailsService {

    @Autowired
    CampaignInfluencerDetailsRepo campaignInfluencerDetailsRepo;




    public FullCampaignInfluencerDetails setCampaignInfluencerDetails(
        FullCampaignInfluencerDetails fullCampaignInfluencerDetails) {
        Logger.getLogger("setCampaignInfluencerDetails").log(Level.FINE,"Setting the Campaign Influencer details");
        CampaignInfluencerDetails campaignInfluencerDetails = campaignInfluencerDetailsRepo.findByCampaignInfluencerId(new CampaignInfluencerId(fullCampaignInfluencerDetails.getCampaignId(), fullCampaignInfluencerDetails.getInfluencerId()));
   if(campaignInfluencerDetails!=null) {
       fullCampaignInfluencerDetails.setInfluencerEmail(campaignInfluencerDetails.getInfluencerEmail());
      fullCampaignInfluencerDetails.setProductsAcceptedStatus(campaignInfluencerDetails.getIsProductAccepted());
       fullCampaignInfluencerDetails.setOfferAcceptedStatus(campaignInfluencerDetails.getIsOfferAccepted());
       fullCampaignInfluencerDetails
               .setDeliverablesAcceptedStatus(campaignInfluencerDetails.getIsDeliverablesAccepted());
       fullCampaignInfluencerDetails.setShippingAcceptedStatus(campaignInfluencerDetails.getIsShippingAccepted());
       fullCampaignInfluencerDetails.setPaymentAcceptedStatus(campaignInfluencerDetails.getIsPaymentAccepted());
       fullCampaignInfluencerDetails.setTermsConditionStatus(campaignInfluencerDetails.getIsTermsAccepted());
       fullCampaignInfluencerDetails.setProposalStatus(campaignInfluencerDetails.getProposalStatus());
       fullCampaignInfluencerDetails.setProductsAcceptedAt(campaignInfluencerDetails.getProductsAcceptedAt());
       fullCampaignInfluencerDetails.setOfferAcceptedAt(campaignInfluencerDetails.getOfferAcceptedAt());
       fullCampaignInfluencerDetails.setDeliverablesAcceptedAt(campaignInfluencerDetails.getDeliverablesAcceptedAt());
       fullCampaignInfluencerDetails.setShippingAcceptedAt(campaignInfluencerDetails.getShippingAcceptedAt());
       fullCampaignInfluencerDetails.setPaymentAcceptedAt(campaignInfluencerDetails.getPaymentAcceptedAt());
       fullCampaignInfluencerDetails.setTermsConditionAcceptedAt(campaignInfluencerDetails.getTermsAcceptedAt());
       fullCampaignInfluencerDetails.setCampaignPaidAt(campaignInfluencerDetails.getCampaignPaidAt());
       fullCampaignInfluencerDetails.setPaymentMethodType(campaignInfluencerDetails.getPaymentMethodType());
       fullCampaignInfluencerDetails.setPaymentEmailAddress(campaignInfluencerDetails.getPaymentEmailAddress());
       fullCampaignInfluencerDetails.setInfluencerFirstName(campaignInfluencerDetails.getInfluencerFirstName());
       fullCampaignInfluencerDetails.setInfluencerLastName(campaignInfluencerDetails.getInfluencerLastName());
   }
    return fullCampaignInfluencerDetails;
}



    public void updateCampaignInfluencerDetails(FullCampaignInfluencerDetails fullCampaignInfluencerDetails){
        Logger.getLogger("updateCampaignInfluencerDetails").log(Level.FINE,"Updating the Campaign Influencer details");
        CampaignInfluencerDetails campaignInfluencerDetails = campaignInfluencerDetailsRepo.findByCampaignInfluencerId(new CampaignInfluencerId(fullCampaignInfluencerDetails.getCampaignId(), fullCampaignInfluencerDetails.getInfluencerId()));
            CampaignInfluencerId campaignInfluencerId = new CampaignInfluencerId();
            campaignInfluencerId.setCampaignId(fullCampaignInfluencerDetails.getCampaignId());
            campaignInfluencerId.setInfluencerId(fullCampaignInfluencerDetails.getInfluencerId());
            campaignInfluencerDetails.setCampaignInfluencerId(campaignInfluencerId);
            campaignInfluencerDetails.setIsProductAccepted(fullCampaignInfluencerDetails.getProductsAcceptedStatus());
            campaignInfluencerDetails.setIsOfferAccepted(fullCampaignInfluencerDetails.getOfferAcceptedStatus());
            campaignInfluencerDetails.setIsDeliverablesAccepted(fullCampaignInfluencerDetails.getDeliverablesAcceptedStatus());
            campaignInfluencerDetails.setIsShippingAccepted(fullCampaignInfluencerDetails.getShippingAcceptedStatus());
            campaignInfluencerDetails.setIsPaymentAccepted(fullCampaignInfluencerDetails.getPaymentAcceptedStatus());
            campaignInfluencerDetails.setProposalStatus(fullCampaignInfluencerDetails.getProposalStatus());
            campaignInfluencerDetails.setIsTermsAccepted(fullCampaignInfluencerDetails.getTermsConditionStatus());
            campaignInfluencerDetails.setProductsAcceptedAt(fullCampaignInfluencerDetails.getProductsAcceptedAt());
            campaignInfluencerDetails.setOfferAcceptedAt(fullCampaignInfluencerDetails.getOfferAcceptedAt());
            campaignInfluencerDetails.setDeliverablesAcceptedAt(fullCampaignInfluencerDetails.getDeliverablesAcceptedAt());
            campaignInfluencerDetails.setShippingAcceptedAt(fullCampaignInfluencerDetails.getShippingAcceptedAt());
            campaignInfluencerDetails.setPaymentAcceptedAt(fullCampaignInfluencerDetails.getPaymentAcceptedAt());
            campaignInfluencerDetails.setTermsAcceptedAt(fullCampaignInfluencerDetails.getTermsConditionAcceptedAt());
            campaignInfluencerDetails.setPaymentMethodType(fullCampaignInfluencerDetails.getPaymentMethodType());
            campaignInfluencerDetails.setCampaignPaidAt(fullCampaignInfluencerDetails.getCampaignPaidAt());
            campaignInfluencerDetails.setPaymentEmailAddress(fullCampaignInfluencerDetails.getPaymentEmailAddress());
            campaignInfluencerDetailsRepo.save(campaignInfluencerDetails);

    }










    
}
