package io.smartfluence.influencer.services;


import io.smartfluence.influencer.entities.*;
import io.smartfluence.influencer.repository.CampaignInfluencerAddressRepo;
import io.smartfluence.influencer.utilities.CommonConstants;
import io.smartfluence.influencer.utilities.CommonConstantsEnum;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.smartfluence.influencer.utilities.InfluencerEmailTokenUtils;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class FullCampaignInfluencerDetailsService {

    @Autowired
    InfluencerEmailTokenUtils influencerEmailTokenUtils;
    @Autowired
    BrandCampaignDetailsService brandCampaignDetailsService;
    @Autowired
    CampaignInfluencerDetailsService campaignInfluencerDetailsService;
    @Autowired
    CampaignInfluencerProductsService campaignInfluencerProductsService;
    @Autowired
    CampaignInfluencerTaskService campaignInfluencerTaskService;
    @Autowired
    SPCountryStateMasterService SpCountryStateMasterService;
    @Autowired
    CampaignInfluencerAddressRepo campaignInfluencerAddressRepo;
    @Autowired
    CampaignInfluencerOffersService campaignInfluencerOffersService;
    @Autowired
    PlatformMasterService platformMasterService;
    @Autowired
    PlatformPostTypeMasterService platformPostTypeMasterService;

    @Autowired
    TermsConditionService termsConditionService;

    @Autowired
    UserAccountService userAccountService;



    public CustomResponseEntity getFullCampaignInfluencerDetails(String emailTokenCode) {
        CustomResponseEntity customResponseEntity = new CustomResponseEntity();
        try {
            Logger.getLogger("getFullCampaignInfluencerDetails").log(Level.FINE,"Getting the Full required influencer details");
            CommonConstantsEnum validateTokenResult = influencerEmailTokenUtils.validateToken(emailTokenCode);
            if (Objects.equals(validateTokenResult, CommonConstantsEnum.EXPIRED)){
                customResponseEntity.setCode(CommonConstants.EXPIRE_CODE);
                customResponseEntity.setError("token expired!!!!");
                return customResponseEntity;
            } else if (Objects.equals(validateTokenResult, CommonConstantsEnum.FAILURE)) {
                customResponseEntity.setCode(CommonConstants.ERROR_CODE);
                customResponseEntity.setError("No Data Found for token Ids!!!!");
                return customResponseEntity;
            } else {
                final Claims tokenData = influencerEmailTokenUtils.getAllClaimsFromToken(emailTokenCode);
                FullCampaignInfluencerDetails fullCampaignInfluencerDetails = new FullCampaignInfluencerDetails();
                String campaignId = (String) tokenData.get("campaignId");
                String influencerId = (String) tokenData.get("influencerId");
                BrandCampaignDetails brandCampaignDetails = brandCampaignDetailsService.getBrandCampaignDetails(campaignId);
                if (brandCampaignDetails != null) {
                    fullCampaignInfluencerDetails = brandCampaignDetailsService.setBrandCampaignDetailsInInflu(brandCampaignDetails, campaignId, influencerId);
                    fullCampaignInfluencerDetails.setTermsConditionDetails(termsConditionService.getTermsConditionsDetails(fullCampaignInfluencerDetails.getTermsId()));
                    fullCampaignInfluencerDetails = userAccountService.getBrandNameAndEmailDetails(fullCampaignInfluencerDetails.getBrandId(),fullCampaignInfluencerDetails);
                        fullCampaignInfluencerDetails = campaignInfluencerDetailsService.setCampaignInfluencerDetails(fullCampaignInfluencerDetails);
                        fullCampaignInfluencerDetails = userAccountService.getInfluencerStatus(fullCampaignInfluencerDetails);
                    fullCampaignInfluencerDetails = campaignInfluencerOffersService.getCampaignInfluencerOffer(fullCampaignInfluencerDetails, campaignId, influencerId);
                    fullCampaignInfluencerDetails = campaignInfluencerProductsService.getInfluencerProducts(campaignId, influencerId, fullCampaignInfluencerDetails);
                    if (campaignInfluencerTaskService.getInfluencerTasks(campaignId, influencerId, fullCampaignInfluencerDetails) != null) {
                        fullCampaignInfluencerDetails = campaignInfluencerTaskService.getInfluencerTasks(campaignId, influencerId, fullCampaignInfluencerDetails);
                        fullCampaignInfluencerDetails = platformMasterService.getPlatformMasterService(fullCampaignInfluencerDetails.getDeliverables(), fullCampaignInfluencerDetails);
                        fullCampaignInfluencerDetails = platformPostTypeMasterService.getPlatformPostTypeMasterService(fullCampaignInfluencerDetails.getDeliverables(), fullCampaignInfluencerDetails);
                    }
                    fullCampaignInfluencerDetails = SpCountryStateMasterService.getCountryList(fullCampaignInfluencerDetails);
                    fullCampaignInfluencerDetails.setShippingAddress(getAddressDetails(campaignId, influencerId, AddressType.SHIPPING));
                    fullCampaignInfluencerDetails.setPaymentAddress(getAddressDetails(campaignId, influencerId, AddressType.BILLING));
                    customResponseEntity.setCode(CommonConstants.SUCCESS_CODE);
                    customResponseEntity.setValue(fullCampaignInfluencerDetails);
                    return customResponseEntity;
                } else {
                    customResponseEntity.setCode(CommonConstants.NO_DATA_CODE);
                    customResponseEntity.setError("No Data available for campaign");
                    return customResponseEntity;
                }
            }
        } catch (Exception e) {
            final Claims tokenData = influencerEmailTokenUtils.getAllClaimsFromToken(emailTokenCode);
            Logger.getLogger("getFullCampaignInfluencerDetails").log(Level.SEVERE,"campaignId : "+ tokenData.get("campaignId")+"  influencerId : "+tokenData.get("influencerId") +"  Time"+ LocalDateTime.now() +e.getMessage());
            customResponseEntity.setCode(CommonConstants.ERROR_CODE);
            customResponseEntity.setError(e.getMessage());
            return customResponseEntity;
        }
    }

   public CustomResponseEntity updateFullCampaignInfluencerDetails(
            FullCampaignInfluencerDetails fullCampaignInfluencerDetails, String emailTokenCode) {
        CustomResponseEntity customResponseEntity = new CustomResponseEntity();
        try {
            Logger.getLogger("updateFullCampaignInfluencerDetails").log(Level.FINE,"Updating the Full required influencer details");
            if (influencerEmailTokenUtils.validateToken(emailTokenCode) == CommonConstantsEnum.EXPIRED) {
                customResponseEntity.setCode(CommonConstants.EXPIRE_CODE);
                customResponseEntity.setError("token expired!!!!");
                return customResponseEntity;
            } else if (influencerEmailTokenUtils.validateToken(emailTokenCode) == CommonConstantsEnum.FAILURE) {
                customResponseEntity.setCode(CommonConstants.ERROR_CODE);
                customResponseEntity.setError("No Data Found for token Ids!!!!");
                return customResponseEntity;
            } else {
                campaignInfluencerProductsService.updateCampaignInfluencerProductService(fullCampaignInfluencerDetails);
                campaignInfluencerTaskService.updateCampaignInfluencerTaskService(fullCampaignInfluencerDetails);
                campaignInfluencerDetailsService.updateCampaignInfluencerDetails(fullCampaignInfluencerDetails);
                updateAddressDetails(fullCampaignInfluencerDetails, AddressType.SHIPPING);
                updateAddressDetails(fullCampaignInfluencerDetails, AddressType.BILLING);
                customResponseEntity = getFullCampaignInfluencerDetails(emailTokenCode);
                return customResponseEntity;
            }
        } catch (Exception e) {
            final Claims tokenData = influencerEmailTokenUtils.getAllClaimsFromToken(emailTokenCode);
            Logger.getLogger("updateFullCampaignInfluencerDetails").log(Level.SEVERE,"campaignId : "+ tokenData.get("campaignId")+"  influencerId : "+tokenData.get("influencerId") +"  Time"+ LocalDateTime.now() +e.getMessage());
            customResponseEntity.setCode(CommonConstants.ERROR_CODE);
            customResponseEntity.setError(e.getMessage());
            return customResponseEntity;
        }
    }

    public Address getAddressDetails(String campaignId, String influencerId, AddressType addressType) {
        Logger.getLogger("getAddressDetails").log(Level.FINE,"Getting the Influencer Address details");
        return campaignInfluencerAddressRepo.findAddressByCampaignInfluencerAddressType(new CampaignInfluencerAddressId(campaignId, influencerId, addressType));
    }

    public void updateAddressDetails(FullCampaignInfluencerDetails fullCampaignInfluencerDetails, AddressType addressType) {
        Logger.getLogger("updateAddressDetails").log(Level.FINE,"Updating the Influencer Address details");
        Address address = new Address();
        switch (addressType){
            case BILLING : if (fullCampaignInfluencerDetails.getPaymentAddress() != null) {
                address = fullCampaignInfluencerDetails.getPaymentAddress();
                address.setCampaignInfluencerAddressType(new CampaignInfluencerAddressId(fullCampaignInfluencerDetails.getCampaignId(), fullCampaignInfluencerDetails.getInfluencerId(), addressType));
                campaignInfluencerAddressRepo.save(address);
            }
            case SHIPPING: if (fullCampaignInfluencerDetails.getShippingAddress() != null) {
                address = fullCampaignInfluencerDetails.getShippingAddress();
                address.setCampaignInfluencerAddressType(new CampaignInfluencerAddressId(fullCampaignInfluencerDetails.getCampaignId(), fullCampaignInfluencerDetails.getInfluencerId(), addressType));
                campaignInfluencerAddressRepo.save(address);
            }
        }

    }
}
