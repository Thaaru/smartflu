var table;
var flag = '';
var today = '';
var dbUpdateHistoryTable;
var subscriptionsMasterTable;

$(document)
		.ready(
				function() {
					table = $('#brand_approval_table')
							.DataTable(
									{
										dom : "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4 btn-explore mt-1'><'col-sm-12 col-md-6'f>>"
												+ "<'row'<'col-sm-12'tr>>"
												+ "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
										drawCallback : function() {
											$('.btn-explore')
													.html(
															'<a onclick="return openDownloadReportModal(true)" href="">'
															+ '	<i class="fas fa-file-export fa-lg ml-2" style="cursor: pointer;margin-left: 0 !important;" title="Export To CSV"></i>'
															+ '</a>');
										},
										select : {
											style : 'multi',
											selector : 'td:first-child'
										},
										order : [ [ 1, 'asc' ] ]
									});

					subscriptionsMasterTable = $('#subscriptions_master_data')
							.DataTable(
									{
										dom : "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4 btn-dbUpdate'><'col-sm-12 col-md-6'f>>"
												+ "<'row'<'col-sm-12'tr>>"
												+ "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
										"order" : [ [ 0, "desc" ] ],
										drawCallback : function() {
											$('.btn-dbUpdate').html();
										},
									});

					/*dbUpdateHistoryTable = $('#dbUpdateHistory')
							.DataTable(
									{
										dom : "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4 btn-dbUpdate'><'col-sm-12 col-md-6'f>>"
												+ "<'row'<'col-sm-12'tr>>"
												+ "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
										"order" : [ [ 0, "desc" ] ],
										drawCallback : function() {
											$('.btn-dbUpdate')
													.html(
															'<button id="dbUpdateBtn" name="DB Update" onclick="dbUpdate()" class="disabled btn btn-primary btn-sm m-0  waves-effect waves-light">DB Update in Progress</button>');
										},
									});*/
				});

/*function dbUpdate() {
	$("#dbUpdateBtn").text("DB Update in Progress");
	$("#dbUpdateBtn").addClass('disabled');
	$.post({
		url : window.location.origin + '/admin/update-db',
	}).done(
			function(data) {
				dbUpdateHistoryTable.row.add(
						[ data.startDate, data.endDate, data.status ]).draw(
						false);
			}).fail(function(data) {
		toast('Failed to Update', 'error');
		$("#dbUpdateBtn").removeClass('disabled');
	});
}

checkDbUpdateStatus();

function checkDbUpdateStatus() {
	$.get({
		url : window.location.origin + '/admin/check-db-update-status',
	}).done(function(data) {
		if (!data) {
			$("#dbUpdateBtn").removeClass('disabled');
			$("#dbUpdateBtn").text("DB Update");
		}
	}).fail(function(data) {
		$("#dbUpdateBtn").addClass('disabled');
	});
}*/

function approveBrand(brandId, userStatus) {

	if ($('select.' + brandId + ' option:selected').val() == '') {
		WARNING("Please select Subscription");
		return;
	}

	var userData = new FormData();
	var value = $('select.' + brandId + ' option:selected').val();
	var subscriptionType = value.substring(0, value.lastIndexOf('|'))
	var subscriptionId = value.substring(value.lastIndexOf('|') + 1)
	userData.append('brandId', brandId)
	userData.append('subscriptionType', subscriptionType)
	userData.append('subscriptionId', subscriptionId)

	$.ajax({
		url			: "approve-brand",
		enctype		: 'multipart/form-data',
		contentType	: false,
		type		: "POST",
		processData	: false,
		async		: true,
		crossDomain	: true,
		data		: userData,
		headers : {
			'Authorization' : localStorage.getItem('Authorization')
		},
		success : function(data, statusMsg, xhr) {
			SUCCESS("Approved Successfully");
			window.location.reload();
		},
		error : function(xhr) {
			if (xhr.status == 401) {
				window.location.href = window.location.origin
			} else {
				var start = xhr.responseText.indexOf("message");
				var end = xhr.responseText.indexOf("path");
				var message = xhr.responseText.substring(start + 10, end - 3);
				ERROR(message);
				window.location.reload();
			}
		}
	});
}

function inviteBrand() {
	var rows_selected = table.rows({
		selected : true
	}).data().toArray();
	var selectedUserId = [];

	rows_selected.forEach(function(element) {
		selectedUserId.push($(element[0]).attr("id"));
	});
	var userData = new FormData();
	userData.append('selectedUserId', selectedUserId)

	if (!isEmpty(selectedUserId)) {
		$.ajax({
			url			: "send-invitation",
			enctype		: 'multipart/form-data',
			contentType	: false,
			type		: "POST",
			processData	: false,
			async		: true,
			crossDomain	: true,
			data		: userData,
			/* headers : {
				'Authorization' : localStorage.getItem('Authorization')
			},*/
			success : function(data, statusMsg, xhr) {
				SUCCESS("Invited Successfully");
				window.location.reload();
			},
			error : function(xhr) {
				if (xhr.status == 401) {
					window.location.href = window.location.origin
				} else {
					ERROR("Invite Failed");
					window.location.reload();
				}
			}
		});
	} else {
		ERROR('Select the brand');
	}
}

function declineBrand(brandId, userStatus) {
	if (userStatus == 'DECLINED') {
		WARNING("Already Declined");
		return;
	}

	var userData = new FormData();
	userData.append('brandId', brandId)

	$.ajax({
		url			: "decline-brand",
		enctype		: 'multipart/form-data',
		contentType	: false,
		type		: "POST",
		processData	: false,
		async		: true,
		crossDomain	: true,
		data		: userData,
		/* headers : {
			'Authorization' : localStorage.getItem('Authorization')
		},*/
		success : function(data, statusMsg, xhr) {
			SUCCESS("Declined Successfully");
			window.location.reload();
		},
		error : function(xhr) {
			if (xhr.status == 401) {
				window.location.href = window.location.origin
			} else {
				var start = xhr.responseText.indexOf("message");
				var end = xhr.responseText.indexOf("path");
				var message = xhr.responseText.substring(start + 10, end - 3);
				ERROR(message);
				window.location.reload();
			}
		}
	});
}

function edit(id) {
	if($("#subscription" + id).hasClass('disabled')) {
		$("#editInfluencer" + id).html('Cancel');
		$("#row" + id).addClass('table-primary');
		$("#subscription" + id).removeClass('disabled');
		$("#approve" + id).removeClass('disabled');
		$("#remove" + id).removeClass('disabled');
	} else {
		$("#editInfluencer" + id).html('Edit');
		$("#row" + id).removeClass('table-primary');
		$("#subscription" + id).addClass('disabled');
		$("#approve" + id).addClass('disabled');
		$("#remove" + id).addClass('disabled');
	}
}

function isEmpty(obj) {
	for (var key in obj) {
		if (obj.hasOwnProperty(key))
			return false;
	}
	return true;
}

function reportPopUpModal() {
	$('#downloadReportModal').modal('show');
	$('#fileReadyMessage').removeClass('d-none');
	$('#reportMessageLoader').removeClass('d-none');
	$('#downloadReport').addClass('d-none');
}

function openDownloadReportModal(isBrandReport) {
	if (isBrandReport) {
		reportPopUpModal();
		downloadAllBrandsReport();
	} else {
		if (validateReportCriteria()) {
			reportPopUpModal();
			getReportData();
		}
	}

	return false;
}

function validateReportCriteria() {
	if ($('#startDate').val() == null || $('#startDate').val() == undefined || $('#startDate').val() == '') {
		WARNING("Please select Start date");
		return false;
	}
	if ($('#endDate').val() == null || $('#endDate').val() == undefined || $('#endDate').val() == '') {
		WARNING("Please select End date");
		return false;
	}
	if ($('#startDate').val() >= $('#endDate').val()) {
		WARNING("End Date must be greater than Start Date");
		return false;
	}
	if ($('#reportType').val() == null || $('#reportType').val() == undefined || $('#reportType').val() == '') {
		WARNING("Please select ReportType");
		return false;
	}

	var currentDate = new Date();
	var month = (1 + currentDate.getMonth()).toString();
	month = month.length > 1 ? month : '0' + month;

	var day = currentDate.getDate().toString();
	day = day.length > 1 ? day : '0' + day;

	today = currentDate.getFullYear() + "-" + month + "-" + day;

	if ($('#startDate').val() > today) {
		WARNING("Start date cannot be future date");
		return false;
	}
	if ($('#endDate').val() > today) {
		WARNING("End date cannot be future date");
		return false;
	}
	return true;
}

function downloadAllBrandsReport() {
	$('#fileReadyMessage').html('Your file is getting ready.');
	$.get({
		url : window.location.origin + '/admin/export-data',
	}).done(function(data) {
		$('#fileReadyMessage').addClass('d-none');
		$('#reportMessageLoader').addClass('d-none');
		$('#downloadReport').removeClass('d-none');
		flag = data;
	}).fail(function(data) {
		$('#downloadReportModal').modal('hide');
		toast('Failed to download', 'error');
	});
}

function getReportData() {
	var reportType = $('#reportType').val();
	var startDate = $('#startDate').val();
	var endDate = $('#endDate').val();
	$('#fileReadyMessage').html('Searching for data');

	$.get({
		url : window.location.origin + '/admin/export-report-data',
		data : {
			reportType		: reportType
			,fromDate		: startDate
			,toDate			: endDate
		},
	}).done(function(data) {
		if (data.status == 200) {
			$('#fileReadyMessage').addClass('d-none');
			$('#reportMessageLoader').addClass('d-none');
			$('#downloadReport').removeClass('d-none');
			flag = data.flag;
		} else if (data.status == 403) {
			$('#downloadReportModal').modal('hide');
			WARNING(data.message);
		}
	}).fail(function(data) {
		$('#downloadReportModal').modal('hide');
		$('#fileReadyMessage').addClass('d-none');
		$('#reportMessageLoader').addClass('d-none');
		$('#downloadReport').removeClass('d-none');
		toast('Failed to download', 'error');
	});
}

function downloadReport() {
	window.open(window.location.origin + '/admin/download/' + flag + '/report');
	$('#downloadReportModal').modal('hide');
}