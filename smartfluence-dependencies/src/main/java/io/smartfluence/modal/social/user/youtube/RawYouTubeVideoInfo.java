package io.smartfluence.modal.social.user.youtube;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawYouTubeVideoInfo implements Serializable {

	private static final long serialVersionUID = -35729601459480902L;

	@JsonAlias("success")
	private Boolean success;

	@JsonAlias("video_info")
	private RawYouTubeVideo video;
}