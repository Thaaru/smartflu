package io.smartfluence.mysql.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;

@Repository
public interface BrandCampaignDetailsMysqlRepo extends CrudRepository<BrandCampaignDetailsMysql, String> {

	@EntityGraph(value = "BrandCampaignDetailsMysql.eagerAll", attributePaths = {
			"campaignInfluencerMappings" }, type = EntityGraphType.LOAD)
	List<BrandCampaignDetailsMysql> findAllByBrandIdAndIsPromote(String brandId, Boolean isPromote);

	@EntityGraph(value = "BrandCampaignDetailsMysql.eagerAll", attributePaths = {
			"campaignInfluencerMappings" }, type = EntityGraphType.LOAD)
	List<BrandCampaignDetailsMysql> findAllByBrandIdAndIsPromoteAndCampaignStatusIn(String brandId, Boolean isPromote, Set<CampaignStatus> campaignStatus);

	@EntityGraph(value = "BrandCampaignDetailsMysql.eagerAll", attributePaths = {
			"campaignInfluencerMappings" }, type = EntityGraphType.LOAD)
	List<BrandCampaignDetailsMysql> findAllByBrandIdAndCampaignStatusIn(String brandId, Set<CampaignStatus> campaignStatus);

	Optional<BrandCampaignDetailsMysql> findByCampaignIdAndBrandIdAndIsPromote(String campaignId, String brandId, Boolean isPromote);

	Optional<BrandCampaignDetailsMysql> findByCampaignIdAndBrandId(String campaignId, String brandId);

	Long countByBrandId(String string);

	Optional<BrandCampaignDetailsMysql> findByCampaignNameAndBrandId(String campaignName, String brandId);

	Optional<BrandCampaignDetailsMysql> findByCampaignId(String campaignId);

	List<BrandCampaignDetailsMysql> findAllByCampaignStatusAndIsPromote(CampaignStatus campaignStatus, Boolean isPromote);
}