package io.smartfluence.modal.social.user.tiktok;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokAuthor {

	@JsonAlias("id")
	private String id;

	@JsonAlias("uniqueId")
	private String uniqueName;

	@JsonAlias("nickname")
	private String fullName;

	@JsonAlias("avatarLarger")
	private String profilePic;

	@JsonAlias("signature")
	private String signature;

	@JsonAlias("verified")
	private boolean isVerified;

	@JsonAlias("privateAccount")
	private boolean isPrivate;
}