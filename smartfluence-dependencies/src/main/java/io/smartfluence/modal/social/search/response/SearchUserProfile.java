package io.smartfluence.modal.social.search.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchUserProfile implements Serializable {

	private static final long serialVersionUID = -7154807877548197283L;

	@JsonAlias("user_id")
	private String userId;

	@JsonAlias("username")
	private String userName;

	@JsonAlias("fullname")
	private String fullname;

	@JsonAlias("url")
	private String url;

	@JsonAlias("picture")
	private String picture;

	@JsonAlias("is_verified")
	private Boolean isVerified;

	@JsonAlias("account_type")
	private Integer accountType;

	@JsonAlias("followers")
	private Double followers;

	@JsonAlias("engagements")
	private Double engagements;

	@JsonAlias("engagement_rate")
	private Double engagementRate;

	@JsonAlias("avg_views")
	private Double avgViews = 0.0;
}