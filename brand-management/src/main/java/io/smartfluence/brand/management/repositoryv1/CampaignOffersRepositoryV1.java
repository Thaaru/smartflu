package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.CampaignOfferId;
import io.smartfluence.brand.management.entityv1.CampaignOffersV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CampaignOffersRepositoryV1 extends JpaRepository<CampaignOffersV1, CampaignOfferId> {
    List<CampaignOffersV1> findByCampaignOfferId_CampaignId(String campaignId);
    void deleteByCampaignOfferId(CampaignOfferId campaignOfferId);
}
