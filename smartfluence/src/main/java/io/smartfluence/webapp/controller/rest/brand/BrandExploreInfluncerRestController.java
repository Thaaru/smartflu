package io.smartfluence.webapp.controller.rest.brand;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.smartfluence.modal.validation.brand.explore.CampaignDetail;
import io.smartfluence.modal.validation.brand.explore.CampaignInfluencerReqEntity;
import io.smartfluence.modal.validation.brand.explore.GetInfluencersRequestModel;
import io.smartfluence.modal.validation.brand.explore.SearchInfluencerRequestModel;
import io.smartfluence.modal.validation.brand.explore.SubmitBidModel;
import io.smartfluence.webapp.proxy.BrandManagementServiceProxy;
import io.smartfluence.webapp.proxy.NewBrandCampManagementServiceProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandExploreInfluncerRestController {

	@Autowired
	private BrandManagementServiceProxy brandManagementServiceProxy;

	@Autowired
    private NewBrandCampManagementServiceProxy newBrandCampManagementServiceProxy;

	@GetMapping("get-influencers")
	public ResponseEntity<Object> getInfluencers(@Valid @ModelAttribute GetInfluencersRequestModel getInfluencersRequestModel,
			BindingResult bindingResult) {
		log.info(getInfluencersRequestModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.getInfluencers(getInfluencersRequestModel.getAudienceBrandName(),
					getInfluencersRequestModel.getAudienceGeoName(), getInfluencersRequestModel.getAudienceGender(),
					getInfluencersRequestModel.getAudienceAge(), getInfluencersRequestModel.getComparisonProfile(),
					getInfluencersRequestModel.getInfluencerGeoName(), getInfluencersRequestModel.getInfluencerGender(),
					getInfluencersRequestModel.getInfluencerAge(), getInfluencersRequestModel.getFollowersMin(), getInfluencersRequestModel.getFollowersMax(),
					getInfluencersRequestModel.getEngagementMin(),getInfluencersRequestModel.getEngagementMax(), getInfluencersRequestModel.getKeyword(),
					getInfluencersRequestModel.getProfileBio(), getInfluencersRequestModel.getHashTag(), getInfluencersRequestModel.getWithContact(),
					getInfluencersRequestModel.getVerified(), getInfluencersRequestModel.getSponsoredPost(), getInfluencersRequestModel.getSortBy(),
					getInfluencersRequestModel.getPlatform(), getInfluencersRequestModel.getLimitCount(), getInfluencersRequestModel.getSkipCount(),
					getInfluencersRequestModel.getIsNewSearch());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("view-influencer/{userId}/profile-image")
	public ResponseEntity<byte[]> getInfluencerProfilePic(@PathVariable String userId) {
		return brandManagementServiceProxy.viewInfluencerProfilePic(userId);
	}

	@GetMapping("view-influencer/{userId}/profile-name")
	public ResponseEntity<Object> viewInfluencerProfileName(@PathVariable UUID userId) {
		return brandManagementServiceProxy.viewInfluencerProfileName(userId);
	}

	@PostMapping("view-influencer/submit-bid")
	public ResponseEntity<Object> submitBid(@Validated @ModelAttribute SubmitBidModel submitBidModel, BindingResult bindingResult) {
		log.info(submitBidModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.submitBid(submitBidModel.getPostType(), submitBidModel.getPostDuration(),
					submitBidModel.getPaymentType(), submitBidModel.getIncludedTag(), submitBidModel.getGeneralMessage(),
					submitBidModel.getBidAmount(), submitBidModel.getPlatform(), submitBidModel.getCampaignId(),
					submitBidModel.getAffiliateType(), submitBidModel.getCampaignName(), submitBidModel.getMailTemplateId(),
					submitBidModel.getMailDescription(), submitBidModel.getMailSubject(), submitBidModel.getMailCc(),
					submitBidModel.getIsValid(), submitBidModel.getInfluencerId(), submitBidModel.getInfluencerHandle(),
					submitBidModel.getReportId(), submitBidModel.getReportPresent());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-influencer/send-proposal")
	public ResponseEntity<Object> sendProposal(@Validated @ModelAttribute SubmitBidModel submitBidModel, BindingResult bindingResult) {
		log.info(submitBidModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.sendProposal(submitBidModel.getPostType(), submitBidModel.getPostDuration(),
					submitBidModel.getPaymentType(), submitBidModel.getIncludedTag(), submitBidModel.getGeneralMessage(),
					submitBidModel.getBidAmount(), submitBidModel.getPlatform(), submitBidModel.getCampaignId(),
					submitBidModel.getAffiliateType(), submitBidModel.getCampaignName(), submitBidModel.getMailTemplateId(),
					submitBidModel.getMailDescription(), submitBidModel.getMailSubject(), submitBidModel.getMailCc(),
					submitBidModel.getIsValid(), submitBidModel.getInfluencerId(), submitBidModel.getInfluencerHandle(),
					submitBidModel.getReportId(), submitBidModel.getReportPresent());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}
	@GetMapping("view-influencer/{userName}/chartData/{platform}")
	public ResponseEntity<Object> getInfluencerChartData(@PathVariable String userName, @PathVariable String platform) {
		return brandManagementServiceProxy.getInfluencerChartData(userName, platform);
	}

	@GetMapping("view-influencer/{userId}/content/{contentId}")
	public ResponseEntity<byte[]> viewInfluencerContentImage(@PathVariable String userId, @PathVariable int contentId,
			@RequestParam String platform) {
		return brandManagementServiceProxy.viewInfluencerContentImage(userId, contentId, platform);
	}

	@GetMapping("view-influencer/{userId}/stories/{type}")
	public ResponseEntity<Object> viewInfluencerInstagramStories(@PathVariable String userId, @PathVariable int type) {
		return brandManagementServiceProxy.viewInfluencerInstagramStories(userId, type);
	}

	@PostMapping("view-influencer/add-to-campaign")
	public ResponseEntity<Object> addToCampaign(@Validated @ModelAttribute CampaignDetail addToCampaign, BindingResult bindingResult) {
		log.info(addToCampaign);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.addToCampaign(addToCampaign.getCampaignName(), addToCampaign.getCampaignId(),
					addToCampaign.getIsValid(), addToCampaign.getInfluencerId(), addToCampaign.getInfluencerHandle(),
					addToCampaign.getReportId(), addToCampaign.getReportPresent(), addToCampaign.getPlatform());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}


	@PostMapping("view-influencer/add-to-new-campaign")
	public ResponseEntity<Object> addInfluencerToNewCampaign(@RequestBody List<CampaignInfluencerReqEntity> campaignInfluencerReqEntity, BindingResult bindingResult) {
		log.info(campaignInfluencerReqEntity);

		if (!bindingResult.hasFieldErrors())
//			return brandManagementServiceProxy.addToNewCampaign(campaignInfluencerReqEntity.getCampaignName(), campaignInfluencerReqEntity.getCampaignId(),
//					campaignInfluencerReqEntity.getIsValid(), campaignInfluencerReqEntity.getInfluencerId(), campaignInfluencerReqEntity.getInfluencerHandle(),
//					campaignInfluencerReqEntity.getReportId(), campaignInfluencerReqEntity.getReportPresent(), campaignInfluencerReqEntity.getPlatform());

			return brandManagementServiceProxy.addInfluencerToNewCampaign(campaignInfluencerReqEntity);
		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}


	@GetMapping("view-influencer/{influencerHandle}/check-influencer")
	public ResponseEntity<Object> checkInfluencer(@Validated @ModelAttribute CampaignDetail campaignDetail, BindingResult bindingResult,
			@PathVariable String influencerHandle) {
		log.info(campaignDetail);
		if (!bindingResult.hasFieldErrors()) {
			return brandManagementServiceProxy.checkInfluencer(campaignDetail.getCampaignName(),
					campaignDetail.getCampaignId(), influencerHandle, campaignDetail.getInfluencerId(), campaignDetail.getPlatform());
		}
		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("search-influencer")
	public ResponseEntity<Object> searchInfluencer(@Validated @ModelAttribute SearchInfluencerRequestModel searchInfluencerRequestModel,
			BindingResult bindingResult) {
		log.info(searchInfluencerRequestModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.searchInfluencer(searchInfluencerRequestModel.getInfluencerHandle(),
					searchInfluencerRequestModel.getDeepSocialInfluencerId(), searchInfluencerRequestModel.getPlatform());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("get-suggestions/{influencerHandle}")
	public ResponseEntity<Object> getSuggestions(@PathVariable String influencerHandle, @RequestParam String platform) {
		return brandManagementServiceProxy.getAutoSuggestions(influencerHandle, platform);
	}

	@GetMapping("get-utilities/{name}")
	public ResponseEntity<Object> getUtilities(@PathVariable String name, @RequestParam String data) {
		return brandManagementServiceProxy.getUtilities(name, data);
	}

	@GetMapping("get-mail-templates")
	public ResponseEntity<Object> getMailTemplates() {
		return brandManagementServiceProxy.getMailTemplates();
	};
}