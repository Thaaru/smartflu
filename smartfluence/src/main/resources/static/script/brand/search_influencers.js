var profileName;
var allActiveCampaigns;
var campaign;
console.log(allActiveCampaigns);
$('#search,.slide-anim').click(function () {
	$('.slide-anim').removeClass('slide-anim');
})

$(document).on('scrolldown', function (e) {
	$('.arrow').addClass('active');
	$('.slide-explore-Influencer,.inf-grid').removeClass('expand');
});

function audienceCard() {
	$("#influencerCard").addClass("d-none");
	$("#audienceCard").removeClass("d-none");
	$("#audienceCard").addClass("slide-right");
	$("#dataSection").addClass("expandAudience");
	$("#dataSection").removeClass("expandInfluencer");
}

function influencerCard() {
	$("#audienceCard").addClass("d-none");
	$("#influencerCard").removeClass("d-none");
	$("#influencerCard").addClass("slide-left");
	$("#dataSection").addClass("expandInfluencer");
	$("#dataSection").removeClass("expandAudience");
}

function validationPopUp(message) {
	$('#validationPopup').modal('show');
	$('#validationMessage').html(message);
	setTimeout(function () {
		$("#validationPopup").modal('hide');
	}, 10000);
}

function avoidSpace(event) {
	var k = event ? event.which : window.event.keyCode;
	if (k == 32) return false;
}

function onlyOne(id, name) {
	var checkboxes = document.getElementsByName(name)
	checkboxes.forEach((item) => {
		if (item !== id) item.checked = false
	})
}

function setAgeRanges(from, to, flag) {
	var audienceFullAgeRange = "13-17,18-24,25-34,35-44,45-64,65+";
	var subStr = audienceFullAgeRange.match(from + "(.*)" + to);
	var ageRange = '';

	if (from != '' && to != '') {
		if (flag == 1) {
			if (from == '65+' && to == '65+') {
				ageRange = from;
			}
			else if (to == '65+') {
				ageRange = from + subStr[1] + to;
			} else {
				ageRange = from + subStr[1];
				ageRange = ageRange.substring(0, ageRange.length - 1);
			}
		}
		if (flag == 2) {
			ageRange = from + "-" + to;
		}
	}

	return ageRange;
}



$("#audienceAgeFrom").change(function () {
	var fromAge = $('#audienceAgeFrom').val();
	var ageContent = '';

	if (fromAge == '13-17') {
		ageContent += '<option value="18-24">18</option>';
		ageContent += '<option value="25-34">25</option>';
		ageContent += '<option value="35-44">35</option>';
		ageContent += '<option value="45-64">45</option>';
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '18-24') {
		ageContent += '<option value="25-34">25</option>';
		ageContent += '<option value="35-44">35</option>';
		ageContent += '<option value="45-64">45</option>';
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '25-34') {
		ageContent += '<option value="35-44">35</option>';
		ageContent += '<option value="45-64">45</option>';
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '35-44') {
		ageContent += '<option value="45-64">45</option>';
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '45-64') {
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '65+') {
		ageContent += '<option value="65+">65+</option>';
	} else if (fromAge == '') {
		ageContent += '<option value="">To</option>';
	}

	$('#audienceAgeTo').html(ageContent);
});

$("#influencerAgeFrom").change(function () {
	var fromAge = $('#influencerAgeFrom').val();
	var ageContent = '';

	if (fromAge == '18') {
		ageContent += '<option value="25">25</option>';
		ageContent += '<option value="35">35</option>';
		ageContent += '<option value="45">45</option>';
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '25') {
		ageContent += '<option value="35">35</option>';
		ageContent += '<option value="45">45</option>';
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '35') {
		ageContent += '<option value="45">45</option>';
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '45') {
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '65') {
		ageContent += '<option value="85">65+</option>';
	} else if (fromAge == '') {
		ageContent += '<option value="">To</option>';
	}

	$('#influencerAgeTo').html(ageContent);
});

$("#sortBy").change(function () {
	if ($('#sortBy').val() == 'audience_relevance') {
		$('#comparisonProfileRequired').removeClass("d-none");
	}
	else {
		$('#comparisonProfileRequired').addClass("d-none");
	}
});

$("#search-platform").change(function () {
	loadCampaignList();
	if ($("#search-platform").val() == "instagram") {

		$('#insta-sponsored-posts').removeClass("d-none");
		$('#youtube-keyword').addClass("d-none");
		$('#insta-industry').removeClass("d-none");
		$('#insta-industry').addClass("active");
	} else if ($("#search-platform").val() == "youtube") {
		$('#insta-sponsored-posts').addClass("d-none");
		$('#insta-industry').addClass("d-none");
		$('#youtube-keyword').removeClass("d-none");
	} else if ($("#search-platform").val() == "tiktok") {
		$('#insta-sponsored-posts').addClass("d-none");
		$('#insta-industry').addClass("d-none");
		$('#youtube-keyword').addClass("d-none");
	}
	loadCampaignList();
	resetForms();
});

function formatNumber(val) {
	if (!isNaN(Number(val))) {
		var temp = Number(val);
		var prefix;
		if (temp < 1000) {
			prefix = '';
		} else if (temp < 1000000) {
			prefix = 'K';
		} else if (temp < 1000000000) {
			prefix = 'M';
		} else {
			prefix = 'B';
		}
		switch (prefix) {
			case 'K':
				temp = (temp / 1000);
				break;
			case 'M':
				temp = (temp / 1000000);
				break;
			case 'B':
				temp = (temp / 1000000000);
				break;
		}
		temp = temp.toFixed(2);
		temp = Number(temp).toLocaleString();
		temp += prefix;
		val = temp;
	} else {
		val = 0;
	}
	return val;
}

function resetForms() {
	audienceGeoLocationName = '';
	influencerGeoLocationName = '';
	comparisonProfileName = '';
	$('.top-influencer').addClass("d-none");
	$('#influencerInstagramLink').attr("href", "");
	$('#influencerYoutubeLink').attr("href", "");
	$('#influencerTiktokLink').attr("href", "");
	$("#comparisonProfile").val("");
	$("#audience-location").val("");
	$("#influencer-location").val("");
	$("#videoKeyword").val("");

	$("#industry option[value=" + "''" + "]").prop(
		"selected", true).change();

	$("#influencerAgeFrom option[value=" + "''" + "]").prop(
		"selected", true).change();

	$("#influencerAgeTo option[value=" + "''" + "]").prop(
		"selected", true).change();

	$("#audienceAgeFrom option[value=" + "''" + "]").prop(
		"selected", true).change();

	$("#audienceAgeTo option[value=" + "''" + "]").prop(
		"selected", true).change();

	$("#with-contact option[value=" + "''" + "]").prop(
		"selected", true).change();

	$("#sortBy option[value=" + "'engagement_rate'" + "]").prop(
		"selected", true).change();

	$("#bioText").val("");
	$("#hashTag").val("");

	$('.form-check-input').prop('checked', false);

	$('#followers').slider('setValue', [Number(10000), Number(1000000)]);
	$('#engagement').slider('setValue', [Number(0), Number(10000)]);
}

var limitCount = 12;
var skipCount = 0;
var totalResult = 0;

function getInfluencerData() {

	skipCount = 0;
	totalResult = 0;
	var comparisonProfile = comparisonProfileName;

	/*if (($('#industry').val() == '' || $('#industry').val() == undefined) && $("#search-platform").val() == 'instagram') {
		WARNING('Please select an Industry from the Industry dropdown');
		return;
	}*/

	if (($('#comparisonProfile').val() == '' || $('#comparisonProfile').val() == undefined)) {
		comparisonProfile = '';

		if ($('#sortBy').val() == 'audience_relevance') {
			WARNING('The Comparison Profile field is mandatory if the Sort By dropdown is selected as "Audience Similarity"');
			return;
		}
	}

	var audienceIndustryValue = $('#industry').val();
	var withContact = $('#with-contact').val();
	var sortBy = $('#sortBy').val();
	var audienceGender = '';
	var influencerGender = '';
	var sponsoredPosts = false;
	var isVerified = false;
	var audienceAgeFrom = $('#audienceAgeFrom').val();
	var audienceAgeTo = $('#audienceAgeTo').val();
	var influencerAgeFrom = $('#influencerAgeFrom').val();
	var influencerAgeTo = $('#influencerAgeTo').val();
	var videoKeyword = $('#videoKeyword').val();
	var bioText = $('#bioText').val();
	var hashTag = $('#hashTag').val();
	var engagementMin = engagementSlider.val().split(',')[0];
	var engagementMax = engagementSlider.val().split(',')[1];
	var followersMin = followerSlider.val().split(',')[0];
	var followersMax = followerSlider.val().split(',')[1];
	var audienceAgeRanges = setAgeRanges(audienceAgeFrom, audienceAgeTo, 1);
	var influencerAgeRanges = setAgeRanges(influencerAgeFrom, influencerAgeTo, 2);

	if ($('#audience-male-gender').is(":checked")) {
		audienceGender = $('#audience-male-gender').val();
	} else if ($('#audience-female-gender').is(":checked")) {
		audienceGender = $('#audience-female-gender').val();
	}

	if ($('#influencer-male-gender').is(":checked")) {
		influencerGender = $('#influencer-male-gender').val();
	} else if ($('#influencer-female-gender').is(":checked")) {
		influencerGender = $('#influencer-female-gender').val();
	}

	if ($('#sponsoredPosts').is(":checked")) {
		sponsoredPosts = true;
	}
	if ($('#isVerified').is(":checked")) {
		isVerified = true;
	}

	document.cookie = "audienceIndustryValue=" + audienceIndustryValue;
	document.cookie = "audienceLocationName=" + audienceGeoLocationName;
	document.cookie = "audienceGender=" + audienceGender;
	document.cookie = "audienceAgeFrom=" + audienceAgeFrom;
	document.cookie = "audienceAgeTo=" + audienceAgeTo;
	document.cookie = "audienceAgeRanges=" + audienceAgeRanges;
	document.cookie = "comparisonProfile=" + comparisonProfile;
	document.cookie = "influencerLocationName=" + influencerGeoLocationName;
	document.cookie = "influencerGender=" + influencerGender;
	document.cookie = "influencerAgeFrom=" + influencerAgeFrom;
	document.cookie = "influencerAgeTo=" + influencerAgeTo;
	document.cookie = "influencerAgeRanges=" + influencerAgeRanges;
	document.cookie = "followersMin=" + followersMin;
	document.cookie = "followersMax=" + followersMax;
	document.cookie = "engagementMin=" + engagementMin;
	document.cookie = "engagementMax=" + engagementMax;
	document.cookie = "videoKeyword=" + videoKeyword;
	document.cookie = "profileBio=" + bioText;
	document.cookie = "hashTag=" + hashTag;
	document.cookie = "withContact=" + withContact;
	document.cookie = "isVerified=" + isVerified;
	document.cookie = "sponsoredPosts=" + sponsoredPosts;
	document.cookie = "sortBy=" + sortBy;
	document.cookie = "platform=" + $("#search-platform").val();

	$('#searchLoading').removeClass("d-none");
	$('#search').addClass("disabled");

	$.get({
		url: window.location.origin + '/brand/get-influencers',
		data: {
			audienceBrandName: audienceIndustryValue
			, audienceGeoName: audienceGeoLocationName
			, audienceGender: audienceGender
			, audienceAge: audienceAgeRanges
			, comparisonProfile: comparisonProfile
			, influencerGeoName: influencerGeoLocationName
			, influencerGender: influencerGender
			, influencerAge: influencerAgeRanges
			, followersMin: followersMin
			, followersMax: followersMax
			, engagementMin: engagementMin
			, engagementMax: engagementMax
			, keyword: videoKeyword
			, profileBio: bioText
			, hashTag: hashTag
			, withContact: withContact
			, verified: isVerified
			, sponsoredPost: sponsoredPosts
			, sortBy: sortBy
			, platform: $("#search-platform").val()
			, limitCount: limitCount
			, skipCount: skipCount
			, isNewSearch: true
		},
	}).done(function (data) {
		$("#campName").prop("disabled", false);
		$('#search').removeClass("disabled");
		$("#searchLoading").addClass("d-none");
		$("#disabledError").html("");

		if (data != undefined && data.length != 0) {
			totalResult = data[0].totalResult;
			$('#total-result').text("Total Results : " + totalResult);
			document.cookie = "totalResult=" + totalResult;
			document.cookie = "searchId=" + data[0].searchId;
			totalResult = totalResult - data.length;
		} else {
			$('#total-result').text('');
		}

		$('section').html('');
		window.scrollTo(0, 0);
		skipCount = skipCount + 12;
		document.cookie = "skipCount=" + skipCount;
		showRankedInfluencers(data);
	}).fail(function (data) {
		if (data.status == 400) {
			WARNING("Mandatory fields required..!");
		} else if (data.status != 500) {
			WARNING(data.responseText);
		} else {
			WARNING("Failed to get Influencers");
		}
		$('#search').removeClass("disabled");
		$("#searchLoading").addClass("d-none");
	});

}

$("#search").click(function () {

	$("#campName").val("")
	getInfluencerData();
});

$('#sortBy').on('change', function () {
	$("#campName").val("")
	getInfluencerData();
});

$("#showMoreInfluencers").click(function () {
	$('#showMoreLoading').removeClass("d-none");
	$('#showMoreArrow').addClass("d-none");
	$('#showMoreInfluencers').addClass("disabled");

	$.get({
		url: window.location.origin + '/brand/get-influencers',
		data: {
			audienceBrandName: getCookie("audienceIndustryValue")
			, audienceGeoName: getCookie("audienceGeoLocationName")
			, audienceGeoId: getCookie("audienceLocationId")
			, audienceGeoWeight: getCookie("audienceLocationWeight")
			, audienceGender: getCookie("audienceGender")
			, audienceAge: getCookie("audienceAgeRanges")
			, comparisonProfile: getCookie("comparisonProfile")
			, influencerGeoName: getCookie("influencerLocationName")
			, influencerGender: getCookie("influencerGender")
			, influencerAge: getCookie("influencerAgeRanges")
			, followersMin: getCookie("followersMin")
			, followersMax: getCookie("followersMax")
			, engagementMin: getCookie("engagementMin")
			, engagementMax: getCookie("engagementMax")
			, keyword: getCookie("videoKeyword")
			, profileBio: getCookie("bioText")
			, hashTag: getCookie("hashTag")
			, withContact: getCookie("withContact")
			, verified: getCookie("isVerified")
			, sponsoredPost: getCookie("sponsoredPosts")
			, sortBy: getCookie("sortBy")
			, platform: getCookie("platform")
			, limitCount: limitCount
			, skipCount: skipCount
			, isNewSearch: false
		},
	}).done(function (data) {
		$("#showMoreLoading").addClass("d-none");
		$('#showMoreArrow').removeClass("d-none");
		$('#showMoreInfluencers').removeClass("disabled");

		if (data != undefined && data.length != 0) {
			totalResult = totalResult - data.length;
		}

		skipCount = skipCount + 12;
		document.cookie = "skipCount=" + skipCount;
		showRankedInfluencers(data);
		greyOutInfluencer(campaign);
	}).fail(function (data) {
		if (data.status != 500) {
			WARNING(data.responseText);
		} else {
			WARNING("Failed to get Influencers");
		}
		$("#showMoreLoading").addClass("d-none");
		$('#showMoreArrow').removeClass("d-none");
		$('#showMoreInfluencers').removeClass("disabled");
	});
});

var influencersList = [];
async function loadPreviousData() {

	var skip = 0;
	var perviousCount = parseInt(getCookie("skipCount"));
	totalResult = parseInt(getCookie("totalResult"));

	while (perviousCount > 0) {
		var limit = perviousCount;

		if (perviousCount > 30) {
			limit = 30;
		}

		await $.get({
			url: window.location.origin + '/brand/get-influencers',
			data: {
				audienceBrandName: getCookie("audienceIndustryValue")
				, audienceGeoName: getCookie("audienceGeoLocationName")
				, audienceGeoId: getCookie("audienceLocationId")
				, audienceGeoWeight: getCookie("audienceLocationWeight")
				, audienceGender: getCookie("audienceGender")
				, audienceAge: getCookie("audienceAgeRanges")
				, comparisonProfile: getCookie("comparisonProfile")
				, influencerGeoName: getCookie("influencerLocationName")
				, influencerGender: getCookie("influencerGender")
				, influencerAge: getCookie("influencerAgeRanges")
				, followersMin: getCookie("followersMin")
				, followersMax: getCookie("followersMax")
				, engagementMin: getCookie("engagementMin")
				, engagementMax: getCookie("engagementMax")
				, keyword: getCookie("videoKeyword")
				, profileBio: getCookie("bioText")
				, hashTag: getCookie("hashTag")
				, withContact: getCookie("withContact")
				, verified: getCookie("isVerified")
				, sponsoredPost: getCookie("sponsoredPosts")
				, sortBy: getCookie("sortBy")
				, platform: getCookie("platform")
				, limitCount: limit
				, skipCount: skip
				, isNewSearch: false
			},
		}).done(function (data) {
			influencersList = data;
			showRankedInfluencers(data);
		}).fail(function () {
			console.log("Failed, Data not available");
		});

		skip = skip + limit;
		perviousCount = perviousCount - limit;
	}

	var yAxis = parseInt(getCookie("windowScroll"));
	skipCount = parseInt(getCookie("skipCount"));
	window.scrollTo(0, yAxis);
}

function greyOutInfluencer(campaign){
    $('.influencer-card').css('background-color', '#ffffff');
    		if (campaign) {
    			// if (campaign.influencers != null) {
    				selectedInfluencerIds = campaign.influencers.map(x => x.social_influencer_id);
    				if (selectedInfluencerIds.length > 0) {
    					selectedInfluencerIds.map(influencerId => {
    						$('#card_' + influencerId).css({ 'background-color': '#e5e5e5' });
    						$('#checkbox_' + influencerId).prop('checked', true).attr('disabled', true);
    					});
    				}
    			// }

    		}
}

function showRankedInfluencers(d) {
	if (totalResult < 1) {
		$('.search-more').removeClass('show-more').addClass('no-more');
	} else if (!$('.search-more').hasClass('show-more')) {
		$('.search-more').addClass('show-more').removeClass('no-more');
	}
	var content = '';
	var temprowCount = 1;
	for (var e in d) {
		temprowCount++;
		if (e % 3 == 0) {
			temprowCount = 1;
			content += '<div class="row">'
		}
		changeHiddenContent(d[e])
		content += $('.hidden-content').html();
		if (temprowCount == 3) {
			content += '</div>';
		}
	}

	$('section').append(content);

	var selectedInfluencerIds = [];
	$("#campName").change(function (event) {
		$('.influencer-card').css('background-color', '#ffffff');
		$(".profile__id:checkbox").prop('checked', false).attr('disabled', false);
		$("#total").text(0);
		$('#invalidCampaign').html("")
		selectedInfluencerIds = [];
		campaign = allActiveCampaigns.find(x => x.campaign_id == event.target.value);
		greyOutInfluencer(campaign);
//		if (campaign) {
//			// if (campaign.influencers != null) {
//				selectedInfluencerIds = campaign.influencers.map(x => x.social_influencer_id);
//				if (selectedInfluencerIds.length > 0) {
//					selectedInfluencerIds.map(influencerId => {
//						$('#card_' + influencerId).css({ 'background-color': '#e5e5e5' });
//						$('#checkbox_' + influencerId).prop('checked', true).attr('disabled', true);
//					});
//				}
//			// }
//
//		}

	});
	var count = 0
	$(".influencersList > input[type=checkbox]").change(function (event) {
		event.preventDefault()
		event.stopPropagation();
		count++;
		var checkedLength = $(".profile__id:checkbox:checked").not(":disabled").length;
		$("#total").text(checkedLength);
		var campaignName = $('#campName').val();
		if (campaignName === "") {
			$("#total").text(0);
			$('#buttonLoading').addClass("d-none");
			toast("Select Campaign Name", "error")
			$('.profile__id:checkbox:checked').prop('checked', false);

		}
	});
}

function changeHiddenContent(p) {
	p.profileImage = p.profileImage == null ? '/static/img/avatar.png'
		: p.profileImage;
	if (p.influencerHandle == null) {
		profileName = p.influencerId.replace(/-/g, '');
		$('.hidden-content .profile__full_name').addClass("blurred-name");
		$('.hidden-content .reveal-name').removeClass("d-none");
		$('.hidden-content .submit-bid').addClass("d-none");
		$('.hidden-content .profile__full_name').text(profileName);
		$('.hidden-content .profile__id').attr('id', 'checkbox_' + p.influencerId);
		$('.hidden-content .profile__id_label').attr('for', p.influencerId);
		$('.hidden-content .influencer-card').attr('id', 'card_' + p.influencerId);
		//$('.hidden-content .profile__id').attr('onchange',onProfileSelected);

	} else {
		profileName = p.influencerHandle;
		$('.hidden-content .profile__full_name').removeClass("blurred-name");
		$('.hidden-content .reveal-name').addClass("d-none");
		$('.hidden-content .submit-bid').removeClass("d-none");
		$('.hidden-content .profile__full_name').text(profileName);
		$('.hidden-content .profile__id').attr('id', 'checkbox_' + p.influencerId);
		$('.hidden-content .profile__id_label').attr('for', p.influencerId);
		$('.hidden-content .profile__id_label').attr('for', p.influencerId);
		//$('.hidden-content .profile__id').attr('onchange',onProfileSelected);
	}

	$('.hidden-content .submit-bid').attr('onclick',
		'viewSubmitBidModal(' + "'" + p.influencerId + "'" + ',' + p.pricing + ',' + "'" + profileName + "'" + ')');

	$('.hidden-content .reveal-name').attr('id', 'viewName' + p.influencerId);

	$('.hidden-content .submit-bid').attr('id', 'submitBid' + p.influencerId);

	$('.hidden-content .profile__link').attr('onclick',
		'viewInfluencer(' + "'" + p.influencerId + "'" + ',' + "'" + getCookie("platform") + "'" + ',' + "'" + getCookie("searchId") + "'" + ')');

	$('.hidden-content .influencer-card').attr('id', 'card_' + p.influencerId);

	$('.hidden-content .profile').attr('id', p.influencerId);

	//	$('.hidden-content .profile__link').attr('href', 'view-influencer/' + p.influencerId +
	//			'?isValid=false&platform=' + getCookie("platform") + '&id=' + getCookie("searchId"))

	$('.hidden-content .profile__image').attr('src',
		'view-influencer/' + p.influencerId + '/profile-image');

	$('.hidden-content .profile__location').text(
		p.location == null ? 'N/A' : p.location);
	$('.hidden-content .profile__engagement_score').text(
		(formatNumber(p.engagementScore * 100)) + '%');
	$('.hidden-content .profile__target_reach').text(p.campaignTargetReach);
	$('.hidden-content .profile__followers').text(formatNumber(p.followers));
	$('.hidden-content .profile__brand_relevance').text(p.brandRelevance);

	if (getCookie("platform") != 'instagram') {
		$('#avgViews').show();
		$('#audienceAuthenticity').hide();
		$('.hidden-content .profile__avg_views').text(formatNumber(p.avgViews));
	} else {
		$('#avgViews').hide();
		$('#audienceAuthenticity').show();
		$('.hidden-content .profile__audience_check').text(formatNumber(p.audienceCheck * 100) + '%');
	}

	$('.hidden-content .profile__recent_partnership').text(p.recentPartnership);
	$('.hidden-content .profile__recent_partnership_profile_image').attr('src',
		p.profileImage);
}

function allCampaignCall(){
	$('#buttonLoading').addClass("d-none");
	$.get(
		{
			url : window.location.origin + '/brand/get-all-new-campaign',
		}).done(function(data) {
			allActiveCampaigns = data;
			
		}).fail(function(data) {
			console.log(data)
		});
}

function addProfilestoCampaign(event) {
	$("#addtocampbutton").removeClass("disabled");
	$('#buttonLoading').removeClass("d-none");
	var campaignId = $("#campName").val();
	if (!campaignId) {
		toast("Select Campaign Name", "error")
		$('#buttonLoading').addClass("d-none");
	} else {
		toast("Adding Influencer(s) to campaign")
		$("#buttonLoading").removeClass("d-none");
		$("#addtocampbutton").addClass("disabled");
		var selectedProfiles = getSelectedprofiles();
		console.log(selectedProfiles, "selectedProfiles");
		if (selectedProfiles.length > 0) {
			var options = {
				type: "POST",
				url: "/brand/view-influencer/add-to-new-campaign",
				headers: { "content-type": "Application/json" },
				data: JSON.stringify(selectedProfiles)
			};

			$(".profile__id:checkbox").attr('disabled', true);
			$.ajax(options).done(function (data) {
		
				$('#buttonLoading').addClass("d-none");
				toast("Influencers were successfully added to the campaign")
				loadCampaignList();
				$("#addtocampbutton").removeClass("disabled");
				$('.influencer-card').css('background-color', '#ffffff');
				$(".profile__id:checkbox").prop('checked', false).attr('disabled', false);
				$("#total").text("0")
				$("#campName").val("")
				allCampaignCall();
			

			}).fail(function (data) {
				$('#buttonLoading').addClass("d-none");
				if (data.status == "409") {
					toast("Not more than 30 Influencers can be added to a Campaign! Contact us for upgrading to the Enterprise plan.", "error")
				}
				else if (data.status == "404") {
					toast(data.responseText, "error")
				}
			
				else {
					toast("Something went wrong, Please try again later !", "error")
				}
				$("#addtocampbutton").removeClass("disabled");
				$('.influencer-card').css('background-color', '#ffffff');
				$(".profile__id:checkbox").prop('checked', false).attr('disabled', false);
				$("#total").text("0")
				$("#campName").val("")
				loadCampaignList();
			});
		}
		else {
			toast("Please select atleast one profile", "error")
			$('#buttonLoading').addClass("d-none");
			$("#addtocampbutton").removeClass("disabled");
		}

	}
}

function getSelectedprofiles() {
	var count = 0;
	var profileList = [];
	var campaignList = {
		"campaignName": $("#campName option:selected").text(),
		"campaignId": $("#campName").val(),
		"isValid": false,
		"reportId": "",
		"reportPresent": false,
		"platform": getCookie('platform')
	}

	$(".profile__id:checkbox:checked").not(":disabled").each(function () {
		var id = $(this).attr('id');
		id = id.replace("checkbox_", '');
		console.log("id", id)
		var handlerName = $(this).parent().parent().find('.profile__full_name').html();
		var profileData = Object.assign({ "influencerId": id, "influencerHandle": handlerName }, campaignList);
		profileList.push(profileData);

	});

	return profileList
}



function viewInfluencer(k, platform, searchId) {
	document.cookie = "influencerId=" + k;
	document.cookie = "loadOldData=" + "true";
	document.cookie = "windowScroll=" + window.pageYOffset || document.documentElement.scrollTop;
	window.location.href = 'view-influencer/' + k + '?isValid=false&platform=' + platform + '&id=' + searchId;
}

async function backToSearch() {

	var loadOldData = getCookie("loadOldData");

	if (loadOldData == "true") {
		document.cookie = "loadOldData=" + "false";

		$("#search-platform option[value=" + "'" + getCookie("platform") + "'" + "]").prop(
			"selected", true).change();

		$("#industry option[value=" + "'" + getCookie("audienceIndustryValue") + "'" + "]").prop(
			"selected", true).change();

		$("#comparisonProfile").val(getCookie("comparisonProfile"));

		audienceGeoLocationName = getCookie("audienceLocationName");
		$("#audience-location").val(getCookie("audienceLocationName"));

		influencerGeoLocationName = getCookie("influencerLocationName");
		$("#influencer-location").val(getCookie("influencerLocationName"));

		$("#bioText").val(getCookie("bioText"));
		$("#hashTag").val(getCookie("hashTag"));

		$("#videoKeyword").val(getCookie("videoKeyword"));

		if (getCookie("isVerified") == 'true') {
			document.getElementById("isVerified").checked = true;
		}

		if (getCookie("sponsoredPosts") == 'true') {
			document.getElementById("sponsoredPosts").checked = true;
		}

		if (getCookie("audienceGender") != "" && getCookie("audienceGender") != null) {
			if (getCookie("audienceGender") == 'MALE') {
				document.getElementById("audience-male-gender").checked = true;
			}
			else if (getCookie("audienceGender") == 'FEMALE') {
				document.getElementById("audience-female-gender").checked = true;
			}
		}

		if (getCookie("influencerGender") != "" && getCookie("influencerGender") != null) {
			if (getCookie("influencerGender") == 'MALE') {
				document.getElementById("influencer-male-gender").checked = true;
			}
			else if (getCookie("influencerGender") == 'FEMALE') {
				document.getElementById("influencer-female-gender").checked = true;
			}
		}

		$("#audienceAgeFrom option[value=" + "'" + getCookie("audienceAgeFrom") + "'" + "]").prop(
			"selected", true).change();

		$("#audienceAgeTo option[value=" + "'" + getCookie("audienceAgeTo") + "'" + "]").prop(
			"selected", true).change();

		$("#influencerAgeFrom option[value=" + "'" + getCookie("influencerAgeFrom") + "'" + "]").prop(
			"selected", true).change();

		$("#influencerAgeTo option[value=" + "'" + getCookie("influencerAgeTo") + "'" + "]").prop(
			"selected", true).change();

		$("#sortBy option[value=" + "'" + getCookie("sortBy") + "'" + "]").prop(
			"selected", true).change();

		$('#followers').slider('setValue', [Number(getCookie("followersMin")), Number(getCookie("followersMax"))]);
		$('#engagement').slider('setValue', [Number(getCookie("engagementMin")), Number(getCookie("engagementMax"))]);

		if ($('#sortBy').val() == 'audience_relevance') {
			$('#comparisonProfileRequired').removeClass("d-none");
		}
		else {
			$('#comparisonProfileRequired').addClass("d-none");
		}
		$('#total-result').text("Total Results : " + getCookie("totalResult"));

		await loadPreviousData();
	}
}

$("#comparisonProfile").on("keyup change", function (e) {
	var val = $('#comparisonProfile').val();

	if (val == "" && val.length < 3) {
		$('.top-influencer').addClass("d-none");
	}
});

var engagementSlider = $('#engagement').slider({
	formatter: function (a) {
		var engagementEnd;
		if (typeof a == 'object') {
			if (formatNumber(a[1]) == 20) {
				engagementEnd = 'Max';
			} else {
				engagementEnd = formatNumber(a[1]);
			}
			return formatNumber(a[0]) + ' to ' + engagementEnd;
		}
		return formatNumber(a);
	}
});

var followerSlider = $('#followers').slider({
	formatter: function (a) {
		if (typeof a == 'object') {
			return formatNumber(a[0]) + ' to ' + formatNumber(a[1]);
		}
		return formatNumber(a);
	}
});

$('.slider.slider-horizontal').each(function () {
	var slider = $(this)
	slider.width(slider.parent().width()
		- slider.parent().find('.slider-left').width()
		- slider.parent().find('.slider-right').width() - 30);
});

$('.view-filter').on('click', function () {
	$('.arrow').toggleClass('active');
	$('.slide-explore-Influencer,.inf-grid').toggleClass('expand');
});

var influencerId;
var influencerPricing;

function viewSubmitBidModal(id, pricing, profile) {
	influencerPricing = pricing;
	$('#platform-submit-bid').val(getCookie("platform"));

	$("#paymentType").empty();
	$("#postType").empty();
	$("#postDuration").empty();

	var paymentTypeInsta = '<option value="" hidden="true">Select payment type</option>' +
		'<option value="Cash">Cash</option>' +
		'<option value="Product/Service">Product/Service</option>' +
		'<option value="Shoutout">Shoutout</option>' +
		'<option value="Affiliate">Affiliate</option>';

	var paymentTypeYT = '<option value="" hidden="true">Select payment type</option>' +
		'<option value="Cash">Cash</option>' +
		'<option value="Product/Service">Product/Service</option>' +
		'<option value="Affiliate">Affiliate</option>';

	var postTypeInsta = '<option value="" hidden="true">Select post type</option>' +
		'<option value="Image">Image</option>' +
		'<option value="Video">Video</option>' +
		'<option value="IGTV">IGTV</option>' +
		'<option value="Story">Story</option>';

	var postTypeYoutube = '<option value="" hidden="true">Select post type</option>' +
		'<option value="IntegratedVideo">Integrated Video</option>' +
		'<option value="DedicatedVideo">Dedicated Video</option>' +
		'<option value="PostRoll">Post Roll</option>' +
		'<option value="Intro">Intro</option>' +
		'<option value="Shoutout">Shout Out</option>' +
		'<option value="Other">Other</option>';

	var postTypeTiktok = '<option value="" hidden="true">Select post type</option>' +
		'<option value="SponsoredPost">Sponsored Post</option>';

	var postDurationInsta = '<option value="" hidden="true">Select post duration</option>' +
		'<option id="2H" class="story" value="2H" hidden="hidden">2 Hours</option>' +
		'<option id="6H" class="story" value="6H" hidden="hidden">6 Hours</option>' +
		'<option id="12H" class="story" value="12H" hidden="hidden">12 Hours</option>' +
		'<option id="24H" class="story image video" value="24H" hidden="hidden">24 Hours</option>' +
		'<option id="1W" class="image video" value="1W" hidden="hidden">1 Week</option>' +
		'<option id="P" class="igtv image video" value="P" hidden="hidden">Permanent</option>';

	if (getCookie("platform") == "instagram") {
		$("#paymentType").append(paymentTypeInsta);
		$("#postType").append(postTypeInsta);
		$("#postDuration").append(postDurationInsta);
		$('#postDuration').removeClass("disabled");
		$('#search-postDuration').addClass("col-md-3 md-form pl-0 d-inline-block");
		$('#search-postDuration').removeClass("d-none");
		$('.yt').addClass("col-md-3");
		$('.yt').removeClass("col-md-4");
		$('#pricing').removeClass('d-none');
		$('.affiliate').addClass("d-none");
		$('.affiliate').removeClass('d-inline-block');
		$('.out').addClass("col-md-3 md-form pl-0 d-inline-block");
		$('#bidAmountLabel').html('Bid Amount in $');
		$('#payment-non-cash').hide();
	}
	if (getCookie("platform") == "youtube" || getCookie("platform") == "tiktok") {
		$("#paymentType").append(paymentTypeYT);
		$('#postDuration').empty();
		$('#postDuration').addClass("disabled");
		$('#search-postDuration').removeClass("col-md-3 md-form pl-0 d-inline-block");
		$('#search-postDuration').addClass("d-none");
		$('.yt').addClass("col-md-4");
		$('.yt').removeClass("col-md-3");
		$('#pricing').addClass('d-none');

		if (getCookie("platform") == "youtube") {
			$("#postType").append(postTypeYoutube);
		} else {
			$("#postType").append(postTypeTiktok);
		}
	}

	if (id.replace(/-/g, '') == profile) {
		profile = influenerNameJson[id];
	}
	profileName = profile;
	$('#price').html('$' + influencerPricing.toFixed(2));
	influencerId = id;
	$('#SubmitBid').modal();

	$.get({
		url: window.location.origin + '/brand/get-campaign',
	}).done(function (data) {
		var content = '';
		for (var i in data) {
			if (data[i].campaignName.indexOf("\'") == -1) {
				content += "<option id='"
					+ data[i].campaignId
					+ "' value='" + data[i].campaignName + "'></option>";
			} else {
				content += '<option id="'
					+ data[i].campaignId
					+ '" value="' + data[i].campaignName + '"></option>';
			}
		}
		$('#campaigns').html(content);
	}).fail(function (data) {

	});
}

function onProfileSelected(e) {
	e.stopPropagation();
	console.log(e.target.id)
}

$("#getCampaigns").on('input', function () {
	console.log("****************************************")
	$.get({
		url: window.location.origin + '/brand/get-campaign',
	}).done(function (data) {
		var content = '';
		for (var i in data) {
			if (data[i].campaignName.indexOf("\'") == -1) {
				content += "<option id='"
					+ data[i].campaignId
					+ "' value='" + data[i].campaignName + "'></option>";
			} else {
				content += '<option id="'
					+ data[i].campaignId
					+ '" value="' + data[i].campaignName + '"></option>';
			}
		}
		$('#campaigns').html(content);
	}).fail(function (data) {

	});
})
//
// $(function () {
//            var values = [];
//            $('.dynamic').click(function () {
//                $('[id*=check]').attr('style', 'display:block');
//                if ($(this).find('input').is(":checked")) {
//                    values.push($(this)[0].innerText);
//                }
//                else {
//                    removevalue = $(this)[0].innerText;
//                    values = jQuery.grep(values, function (value) {
//                        return value != removevalue;
//                    });
//                }
//                $('[id*=checkbox_0]').html(values.join(','));
//            });
//
//        });


/*$('input[type="checkbox"]').each(function(i, v) {
	console.log("*************************CHECK")
	var last_id = $('input[type="checkbox"]:last').attr('id');
	console.log(last_id)
	  last_id++;
	  $('input[type="checkbox"]:last').append('<input type="checkbox" name="'+last_id+' id="'+last_id+'">');
	 $(this).attr("id", (last_id))
	 console.log($(this).attr("id"))
	 $(this).after($("<label>").attr("for", $(this).attr("id")));
});*/

function submitBid() {
	var postTypeVal = $('#postType').val();
	var paymentTypeVal = $('#paymentType').val();
	var postDurationVal = $('#postDuration').val();
	if (paymentTypeVal == 'Affiliate') {
		postTypeVal = null;
		postDurationVal = null;
	}
	var affiliateType = $('#affiliateType').val();
	var campaign = $('#campaign').val().trim();
	var campaignId;
	var opt;

	if (campaign.indexOf('\"') != -1) {
		opt = $("option[value='" + campaign + "']");
	} else {
		opt = $('option[value="' + campaign + '"]');
	}

	if (opt.length) {
		campaignId = opt.attr('id');
	} else {
		campaignId = '';
	}
	var mailTemplate = $("#mailTemplate").val();
	var mailDescription = $('#mailTextArea').val();
	var mailSubject = $('#mailSubject').val();
	var mailCc = $('#mailCc').val();
	mailCc = mailCc.split(' ').join(',');
	$('#skipForNow').addClass('disabled');
	$('.submitBid').addClass('disabled');
	$('.back').addClass('disabled');
	$('#submitBid').addClass('disabled');

	$.post({
		url: window.location.origin + '/brand/view-influencer/submit-bid',
		data: {
			postType: postTypeVal
			, postDuration: postDurationVal
			, paymentType: paymentTypeVal
			, bidAmount: (parseFloat($('#bid').val())).toFixed(2)
			, affiliateType: affiliateType
			, campaignName: $('#campaign').val().trim()
			, campaignId: campaignId
			, isValid: false
			, mailTemplateId: mailTemplate
			, mailDescription: mailDescription
			, mailSubject: mailSubject
			, mailCc: mailCc
			, influencerId: influencerId
			, influencerHandle: profileName
			, reportId: ''
			, reportPresent: false
			, platform: getCookie("platform")
		},
	}).done(function (data) {
		$('#SubmitBid').modal('hide');
		$('#submitBid').removeClass('disabled');
		toast(data.data);
		$('#skipForNow').removeClass('disabled');
		$('.submitBid').removeClass('disabled');
		$('.back').removeClass('disabled');
		$('#addToCampaign').removeClass('d-none');
		resetFormSubmitBid();
	}).fail(function (data) {
		$('#SubmitBid').modal('hide');
		$('#submitBid').removeClass('disabled');
		$('#skipForNow').removeClass('disabled');
		$('.submitBid').removeClass('disabled');
		$('.back').removeClass('disabled');
		resetFormSubmitBid();

		if (data.responseText != null) {
			toast(data.responseText, 'error');
		} else {
			toast('Failed to submid bid', 'error');
		}
	});
}

$('#postType').change(function (event) {
	$('#postType').removeClass("border-danger");
	$('#postTypeInvalid').empty();
	if ($('#postType').val() == "Story") {
		$('#bid').removeClass("border-danger");
		$('#bidAmountInvalid').empty();
	}
});

$('#postDuration').change(function (event) {
	$('#postDuration').removeClass("border-danger");
	$('#postDurationInvalid').empty();
	if ($('#postDuration').val() == "24H" || $('#postDuration').val() == "1W") {
		$('#bid').removeClass("border-danger");
		$('#bidAmountInvalid').empty();
	}
});

$('#paymentType').change(function (event) {
	$('#paymentType').removeClass("border-danger");
	$('#bid').removeClass("border-danger");
	$('#bidAmountInvalid').empty();
	$('#paymentModeInvalid').empty();
	$('#paymentTypeInvalid').empty();

	if ($('#paymentType').val() == 'Product/Service' || $('#paymentType').val() == 'Shoutout') {
		$('.not-affiliate').show();
		$('.not-affiliate').addClass('d-inline-block');
		$('.affiliate').addClass('col-md-3');
		$('.affiliate').removeClass('col-md-4');
		$('#payment-non-cash').html("Please include the cash equivalent value of the product, service or shoutout you are providing in the bid amount.").show();
		$('#affiliateSuffixDiv').addClass('d-none');
		$('#bidAmountLabel').html('Bid Amount in $');
	} else if ($('#paymentType').val() == 'Affiliate') {
		$('#bid').val(0);
		$('.not-affiliate').hide();
		$('.not-affiliate').removeClass('d-inline-block');
		$('.affiliate').removeClass('col-md-3');
		$('.affiliate').addClass('col-md-4');
		$('#payment-non-cash').html("Smartfluence will recommend an affiliate post schedule for the influencer. Please select whether you will be paying as a % of sales or $ per sale.").show();
		$('#affiliateSuffixDiv').removeClass('d-none');
		$('#bidAmountLabel').html('Affiliate %');
	} else {
		$('.not-affiliate').show();
		$('.not-affiliate').addClass('d-inline-block');
		$('.affiliate').addClass('col-md-3');
		$('.affiliate').removeClass('col-md-4');
		$('#payment-non-cash').hide();
		$('#affiliateSuffixDiv').addClass('d-none');
		$('#bidAmountLabel').html('Bid Amount in $');
	}
	$('#mailTemplate option').attr('hidden', 'hidden');
	$('#mailTemplate .true').removeAttr('hidden');

	if ($('#paymentType').val() == 'Affiliate') {
		if ($('#affiliateType').val() == '%') {
			$('#mailTemplate .Affiliate.percentage')
				.removeAttr('hidden');
		} else if ($('#affiliateType').val() == '$') {
			$('#mailTemplate .Affiliate.fee').removeAttr(
				'hidden');
		}
	} else if ($('#paymentType').val() == 'Product/Service') {
		$('#mailTemplate .Product').removeAttr('hidden');
	} else {
		$(
			'#mailTemplate '
			+ (this.value.trim() == '' ? '' : '.'
				+ this.value.trim().replace(
					/ /g, '_')))
			.removeAttr('hidden');
	}

	if ($("#platform-submit-bid").val() != 'instagram') {
		$('#search-postDuration').removeClass("col-md-3 md-form pl-0 d-inline-block");
		$('#search-postDuration').addClass("d-none");
		$('.yt').removeClass("col-md-3");
		$('.yt').addClass("col-md-4");
	}

	$('#mailTemplate').val('').change();
	$('#mailTextArea').val('');
	$('#mailSubject').val('');
});

$('#affiliateType').change(function (event) {
	var affiliateType = $('#affiliateType').val();
	if (affiliateType == "%") {
		$('#bidAmountLabel').html('Affiliate %');
	} else if (affiliateType == "$") {
		$('#bidAmountLabel').html('Affiliate Fee');
	}
	$('#bid').removeClass("border-danger");
	$('#bidAmountInvalid').empty();
	$('#paymentModeInvalid').empty();

	$('#mailTemplate option').attr('hidden', 'hidden');
	$('#mailTemplate .true').removeAttr('hidden');
	if ($('#paymentType').val() == 'Affiliate') {
		if ($('#affiliateType').val() == '%') {
			$('#mailTemplate .Affiliate.percentage')
				.removeAttr('hidden');
		} else if ($('#affiliateType').val() == '$') {
			$('#mailTemplate .Affiliate.fee').removeAttr(
				'hidden');
		}
	} else if ($('#paymentType').val() == 'Product/Service') {
		$('#mailTemplate .Product').removeAttr('hidden');
	} else {
		$(
			'#mailTemplate '
			+ (this.value.trim() == '' ? '' : '.'
				+ this.value.trim().replace(
					/ /g, '_')))
			.removeAttr('hidden');
	}
	$('#mailTemplate').val('').change();
	$('#mailTextArea').val('');
	$('#mailSubject').val('');
});

$('#bid').on('input', function () {
	var value = $('#bid').val();
	if ($("#platform-submit-bid").val() == "instagram") {
		if ($('#postType').val() == "Story"
			|| ($('#postType').val() == "Image" && ($(
				'#postDuration').val() == "24H" || $(
					'#postDuration').val() == "1W"))) {

			if ($('#paymentType').val() == "Affiliate") {
				var affiliateType = $('#affiliateType').val();

				if (affiliateType == '' || affiliateType == null) {
					$('#paymentModeInvalid').html("Please select Payment Mode");
					bidBoolean = false;
				}
				if (affiliateType == "%"
					&& (value <= 0 || value > 100)) {
					$('#bid').addClass("border-danger");
					$('#bidAmountInvalid').html("Enter a valid percentage");
					bidBoolean = false;
				} else if (affiliateType == "$" && (value <= 0)) {
					$('#bid').addClass("border-danger");
					$('#bidAmountInvalid').html("Enter a valid fee");
					bidBoolean = false;
				} else {
					$('#bid').removeClass("border-danger");
					$('#bidAmountInvalid').empty();
				}
			} else {
				var value = $('#bid').val();
				var bid = document.getElementById("bid");
				bid.setCustomValidity('Enter the bid amount');
				if (value == "" || Number(value) <= 0) {
					$('#bid').addClass("border-danger");
					$('#bidAmountInvalid').html(bid.validationMessage);
				} else {
					$('#bid').removeClass("border-danger");
					$('#bidAmountInvalid').empty();
				}
			}
		} else {
			if ($('#paymentType').val() == "Affiliate") {
				var affiliateType = $('#affiliateType').val();
				if (affiliateType == "%"
					&& (value <= 0 || value > 100)) {
					$('#bid').addClass("border-danger");
					$('#bidAmountInvalid').html(
						"Enter a valid percentage");
					bidBoolean = false;
				} else if (affiliateType == "$" && (value <= 0)) {
					$('#bid').addClass("border-danger");
					$('#bidAmountInvalid').html(
						"Enter a valid fee");
					bidBoolean = false;
				} else {
					$('#bid').removeClass("border-danger");
					$('#bidAmountInvalid').empty();
				}
			} else {
				var value = $('#bid').val();
				var bid = document.getElementById("bid");
				var minimumAmount = (influencerPricing * 0.75)
					.toFixed(0);
				bid.setCustomValidity('Your Bid should be greater than 0$');
				if (Number(value) > 0) {
					$('#bid').removeClass("border-danger");
					$('#bidAmountInvalid').empty();
				} else {
					$('#bid').addClass("border-danger");
					$('#bidAmountInvalid').html(
						bid.validationMessage);
				}
			}
		}
	} else {
		$('#bidAmountInvalid').empty();
	}
})

$('#postType').on('change', function () {
	var postType = $('#postType').val();
	if (postType.length > 0) {
		$('#postDuration').val('');
		$('#postDuration option').attr('hidden', 'hidden');
		$('.' + postType.toLowerCase()).removeAttr('hidden');
	}
})

$('#campaign').on('change', function () {
	var campaign = $('#campaign').val().trim();
	var opt;
	$('#skipForNow').removeClass('d-none');

	if (campaign.indexOf('\"') != -1) {
		opt = $("option[value='" + campaign + "']");
	} else {
		opt = $('option[value="' + campaign + '"]');
	}
	if (opt.length) {
		campaignId = opt.attr('id');
	} else {
		campaignId = '';
	}

	if (campaignId != undefined && campaignId != '' && campaignId.length > 0) {
		$.get({
			url: window.location.origin + '/brand/view-influencer/' + profileName + '/check-influencer',
			data: {
				campaignName: campaign
				, campaignId: campaignId
				, influencerId: influencerId
				, platform: getCookie("platform")
			},
		}).done(function (data) {
			if (data == true) {
				$('#skipForNow').addClass('d-none');
				$('#invalidCampaign').removeClass('d-none');
				$('#invalidCampaign').html('Influencer already exists in the campaign. Clicking on Next will take you to submit bid.');
			} else {
				$('#skipForNow').removeClass('d-none');
				$('#invalidCampaign').addClass('d-none');
			}
		}).fail(function (data) {
		});
	}
})

$('#campaign').on('input', 'change', function () {
	var campaign = $('#campaign').val().trim();
	$('#skipForNow').removeClass('d-none');
	var opt = $('option[value="' + campaign + '"]');

	if (opt.length) {
		campaignId = opt.attr('id');
	} else {
		campaignId = '';
	}

	if (campaignId != undefined && campaignId != '' && campaignId.length > 0) {
		$.get({
			url: window.location.origin + '/brand/view-influencer/' + profileName + '/check-influencer',
			data: {
				campaignName: campaign
				, campaignId: campaignId
				, influencerId: influencerId
				, platform: getCookie("platform")
			},
		}).done(function (data) {
			if (data == true) {
				$('#skipForNow').addClass('d-none');
				$('#invalidCampaign').removeClass('d-none');
				$('#invalidCampaign').html('Influencer already exists in the campaign. Clicking on Next will take you to submit bid.');
			} else {
				$('#skipForNow').removeClass('d-none');
				$('#invalidCampaign').addClass('d-none');
			}
		}).fail(function (data) {
			toastr.error("failed");
		});
	}
})

var campaign;
function validateCampaign() {
	campaign = $('#campaign').val().trim();
	var opt;

	if (campaign.indexOf('\"') != -1) {
		opt = $("option[value='" + campaign + "']");
	} else {
		opt = $('option[value="' + campaign + '"]');
	}

	if (opt.length) {
		campaignId = opt.attr('id');
	} else {
		campaignId = '';
	}

	if (campaign == "") {
		$('#invalidCampaign').removeClass('d-none');
		$('#invalidCampaign').html('Enter Campaign Name');
	} else {
		sendEvent('#SubmitBid', 2);
		$('#bid').blur();
	}
}

$('#campaign').on('input', function () {
	$('#invalidCampaign').addClass('d-none');
});
$('#campaign').on('input', createOrSelect).change(createOrSelect);
function createOrSelect() {
	var templateValue = this.value;
	var exists;

	if (templateValue.indexOf('\"') != -1) {
		exists = $("datalist option[value='" + templateValue.trim() + "']").length != 0;
	} else {
		var exists = $('datalist option[value="' + templateValue.trim() + '"]').length != 0;
	}
}

function saveCampaign() {
	var campaign = $('#campaign').val().trim();
	var campaignId;
	var hasCampaign;
	var opt;

	if (campaign.indexOf('\"') != -1) {
		hasCampaign = $("#campaigns [value='" + campaign + "']").data('value');
		opt = $("option[value='" + campaign + "']");
	} else {
		hasCampaign = $('#campaigns [value="' + campaign + '"]').data('value');
		opt = $('option[value="' + campaign + '"]');
	}

	if (opt.length) {
		campaignId = opt.attr('id');
	} else {
		campaignId = '';
	}

	if (campaign != "") {
		$('.back-2').addClass('disabled');
		$('.yes').addClass('disabled');
		$('#addToCampaign').addClass('disabled');

		$.post({
			url: window.location.origin + '/brand/view-influencer/add-to-campaign',
			data: {
				campaignName: campaign
				, campaignId: campaignId
				, isValid: false
				, influencerId: influencerId
				, influencerHandle: profileName
				, reportId: ''
				, reportPresent: false
				, platform: getCookie("platform")
			},
		}).done(function (data) {
			$('#SubmitBid').modal('hide');
			$('#addToCampaign').removeClass('disabled');
			$(".close").click();
			toast('The Influencer has been successfully added to the Campaign!');
			$('.addToCampaignMesage').addClass('d-none');
			$('#submitBid').removeClass('d-none');
		}).fail(function (data) {
			$('#SubmitBid').modal('hide');
			$('#addToCampaign').removeClass('disabled');
			$(".close").click();
			$('.back-2').removeClass('disabled');
			$('.yes').removeClass('disabled');

			toast('Failed to add influencer to campaign', 'error');
		});
	}
}

function resetFormSubmitBid() {
	$('.text-danger').html('');
	$('.submitBid-tab-label').removeClass('skip');
	$('.mailTemplate-label').removeClass('skip');
	$('#invalidCampaign').html('');
	$('.border-danger').removeClass('border-danger');
	$('#SubmitBid input,#SubmitBid select').val('').change();
	$('#payment-non-cash').hide();
	$('#mailTextArea').val('');
}

function setSubmitBidTrue() {
	submitBidBoolean = true;

	$('#step3Body').html('')
	var postTypeBoolean = true;
	var postDurationBoolean = true;
	var bidBoolean = true;
	var paymentTypeBoolean = true;
	var campaignBoolean = true;
	var postType = document.getElementById("postType");
	var postDuration = document.getElementById("postDuration");
	var paymentType = $('#paymentType').val();
	var bidAmount = $('#bid').val();
	var affiliateType = $('#affiliateType').val();
	var paymentTypeMessage = 'Select Payment Type';

	if (paymentType == '') {
		$('#paymentType').addClass("border-danger");
		$('#paymentTypeInvalid').html(paymentTypeMessage);
		paymentTypeBoolean = false;
	} else if (paymentType == "Affiliate") {
		var affiliateType = $('#affiliateType').val();
		if (affiliateType == '' || affiliateType == null) {
			$('#paymentModeInvalid').html("Please select Payment Mode");
			bidBoolean = false;
		}
		if (affiliateType == "%" && (bidAmount <= 0 || bidAmount > 100)) {
			$('#bid').addClass("border-danger");
			$('#bidAmountInvalid').html("Enter a valid percentage");
			bidBoolean = false;
		} else if (affiliateType == "$" && (bidAmount <= 0)) {
			$('#bid').addClass("border-danger");
			$('#bidAmountInvalid').html("Enter a valid fee");
			bidBoolean = false;
		}
	}

	if (paymentType != "Affiliate") {
		affiliateType = null;
		postType.setCustomValidity('Select Post Type');
		if (postType.value == '') {
			$('#postType').addClass("border-danger");
			$('#postTypeInvalid').html(postType.validationMessage);
			postTypeBoolean = false;
		}

		if (getCookie("platform") == 'instagram') {
			postDuration.setCustomValidity('Select Post Duration');
			if (postDuration.value == '') {
				$('#postDuration').addClass("border-danger");
				$('#postDurationInvalid').html(postDuration.validationMessage);
				postDurationBoolean = false;
			}
		}

		if ($('#postType').val() == "Story"
			|| ($('#postType').val() == "Image" && ($('#postDuration')
				.val() == "24H" || $('#postDuration').val() == "1W"))) {
			var bid = document.getElementById("bid");
			bid.setCustomValidity('Enter the bid amount');
			if (bidAmount == "" || Number(bidAmount) <= 0) {
				$('#bid').addClass("border-danger");
				$('#bidAmountInvalid').html(bid.validationMessage);
				bidBoolean = false;
			}
		} else {
			var bid = document.getElementById("bid");
			var minimumAmount = (influencerPricing * 0.75).toFixed(0);
			bid.setCustomValidity('Your Bid should be greater than 0$');

			if (Number(bidAmount) > 0) {
				bidBoolean = true;
			} else {
				$('#bid').addClass("border-danger");
				$('#bidAmountInvalid').html(bid.validationMessage);
				bidBoolean = false;
			}
		}
	}

	if (postTypeBoolean && postDurationBoolean && bidBoolean && paymentTypeBoolean && campaignBoolean) {
		$('#step3Body').html('The Influencer <span style="font-style: oblique;font-weight:bold;">' + profileName + '</span> will be added to the Campaign <span style="font-style: oblique;font-weight:bold;">' + campaign + '</span> and a bid will be submitted with the following details:');
		$('.submitBidTable').removeClass('d-none');
		if (paymentType == "Affiliate") {
			var affilateType = $('#affiliateType').val();
			var affilateMode;
			$('#postTypeTableLabel').addClass('d-none');
			$('#postDurationTableLabel').addClass('d-none');
			$('#paymentModeTableLabel').removeClass('d-none');
			$('#postTypeTable').addClass('d-none');
			$('#postDurationTable').addClass('d-none');
			$('#paymentModeTable').removeClass('d-none');

			if (affilateType == '$') {
				$('#bidAmountTableLabel').text('Affiliate Fee');
				affilateMode = '$ per Sale';
				$('#bidAmountTable').html('$' + $("#bid").val());
			} else if (affilateType == '%') {
				$('#bidAmountTableLabel').text('Affiliate %');
				affilateMode = '% of Sales';
				$('#bidAmountTable').html($("#bid").val() + '%');
			}
			$('#paymentModeTable').html(affilateMode);
		} else {
			$('#bidAmountTable').html('$' + $("#bid").val());
			$('#postTypeTableLabel').removeClass('d-none');
			$('#paymentModeTableLabel').addClass('d-none');
			$('#postTypeTable').removeClass('d-none');
			$('#paymentModeTable').addClass('d-none');
			$('#bidAmountTableLabel').text('Bid Amount');

			if (getCookie("platform") != 'instagram') {
				$('#postDurationTableLabel').addClass('d-none');
				$('#postDurationTable').addClass('d-none');
			} else {
				$('#postDurationTableLabel').removeClass('d-none');
				$('#postDurationTable').removeClass('d-none');
			}
		}

		$('#postTypeTable').html($("#postType option:selected").text());
		$('#postDurationTable').html($("#postDuration option:selected").text());
		$('#paymentTypeTable').html($("#paymentType option:selected").text());

		$('.addToCampaignMesage').addClass('d-none');
		$('#submitBid').removeClass('d-none');
		$('#addToCampaign').addClass('d-none');
		sendEvent('#SubmitBid', 3);
		$('#mailCc').val(profileDetails.email);
		$('#mailTemplate .true').removeAttr('hidden');
		if ($('#paymentType').val() == 'Affiliate') {
			if ($('#affiliateType').val() == '%') {
				$('#mailTemplate .Affiliate.percentage').removeAttr('hidden');
				$('#mailTemplate .Affiliate.percentage').prop('selected', true);
				$('#mailTemplate .Affiliate.fee').change();
			} else if ($('#affiliateType').val() == '$') {
				$('#mailTemplate .Affiliate.fee').removeAttr('hidden');
				$('#mailTemplate .Affiliate.fee').prop('selected', true);
				$('#mailTemplate .Affiliate.fee').change();
			}
		} else if ($('#paymentType').val() == 'Product/Service') {
			$('#mailTemplate .Product').removeAttr('hidden');
			$('#mailTemplate .Product').prop('selected', true);
			$('#mailTemplate .Affiliate.fee').change();
		} else {
			$('#mailTemplate '
				+ ($('#paymentType').val().trim() == '' ? '' : '.'
					+ $('#paymentType').val().trim().replace(
						/ /g, '_'))).removeAttr('hidden');
			$('#mailTemplate '
				+ ($('#paymentType').val().trim() == '' ? '' : '.'
					+ $('#paymentType').val().trim().replace(
						/ /g, '_'))).prop('selected', true);
			$('#mailTemplate .Affiliate.fee').change();
		}
		$('#dataStep4Back').attr('onclick', "sendEvent('#SubmitBid', 3)");
	}
}

function setSubmitBidFalse() {
	$('.submitBid-tab-label').addClass('skip');
	$('.mailTemplate-label').addClass('skip');
	submitBidBoolean = false;
	$('.submitBidTable').addClass('d-none');
	$('#step3Body').html('The Influencer <span style="font-style: oblique;font-weight:bold;">' + profileName + '</span> will be added to the Campaign <span style="font-style: oblique;font-weight:bold;">' + campaign + '</span> without submitting a bid.');
	$('.addToCampaignMesage').removeClass('d-none');
	$('#submitBid').addClass('d-none');
	$('#addToCampaign').removeClass('d-none');
	sendEvent('#SubmitBid', 4);
	$('#dataStep4Back').attr('onclick', "sendEvent('#SubmitBid', 2)");
}

$('.back-2').click(function () {
	$('.submitBid-tab-label').removeClass('skip');
	$('.mailTemplate-label').removeClass('skip');
})

$('#influencerHandle').on('input', function () {
	if ($('#influencerHandle').val().length < 3) {
		$('#searchInfluencerModalFooter').addClass('d-none');
	}
	var platformType = $('#platform').val();
	$('#platformInvalid').html("");

	if (platformType != 'youtube') {
		$('#influencerHandle').val($('#influencerHandle').val().toLowerCase());
	}
});

function searchInfluncer(handle) {
	$('#searchInfluencerModalFooter').addClass('d-none');
	$('#generateReport').addClass('d-none');
	$('#viewInfluencer').addClass('d-none');
	$('#reportMessageCount').html('');
	$('#viewInfluencerSuccessMessageDiv').addClass('d-none');
	var platformType = $('#platform').val();
	var deepSocialId = '';

	if (platformType != undefined && platformType.length != 0 &&
		platformType != "" && platformType != null) {
		var influencerHandle = $('#influencerHandle').val();
		if (influencerHandle != undefined && influencerHandle.length != 0 &&
			influencerHandle != "" && influencerHandle != null) {
			if (influencerHandle.charAt(0) == '@') {
				influencerHandle = influencerHandle.substr(1);
				$('#influencerHandle').val(influencerHandle);
			}
			if (platformType != "instagram" && selectedUser != undefined && influencerHandle == selectedUser.name) {
				influencerHandle = selectedUser.id;
			}
			if (platformType == 'youtube') {
				var ytPattern = new RegExp('(https?:\/\/)?(www\.)?youtu((\.be)|(be\..{2,5}))\/((user)|(channel))\/.+');
				if (ytPattern.test(influencerHandle)) {
					var urlValues = influencerHandle.split('/');
					influencerHandle = urlValues.pop();
				}
			}
			if (handle != null && handle != '' && handle != undefined) {
				influencerHandle = handle.name;
				deepSocialId = handle.id;
			}
			$('#searchLoader').removeClass('d-none');
			$('#searchInfluencerIcon').addClass('d-none');

			$.get({
				url: window.location.origin + '/brand/search-influencer',
				data: {
					deepSocialInfluencerId: deepSocialId
					, influencerHandle: influencerHandle
					, platform: platformType
				}
			}).done(function (data) {
				reportId = data.reportId;
				influencerSFId = data.influencerId;
				reportPresent = data.reportPresent;
				reportInfluencerHandle = data.influencerHandle;

				var influencerId = data.deepSocialInfluencerId;

				if (platformType != 'instagram') {
					reportInfluencerId = data.deepSocialInfluencerId;
				} else {
					reportInfluencerId = deepSocialId;
				}
				if (data.newInfluencer) {
					influencerId = data.influencerId
				}

				var href = window.location.origin + '/brand/view-influencer/' + influencerId
					+ '?isValid=' + data.newInfluencer + '&platform=' + platformType;
				$('#viewInfluencer').attr('href', href);
				$('#viewInfluencer').removeClass('d-none');
				$('#generateReport').removeClass('d-none');
				$('#searchInfluencerModalFooter').removeClass('d-none');
				$('#searchLoader').addClass('d-none');
				$('#searchInfluencerIcon').removeClass('d-none');
			}).fail(function (data) {
				if (data.status == 409) {
					$('#reportMessage').html("You have reached the weekly limit of 50 reports! Please contact help@smartfluence.io to increase your limit.");
				} else {
					var errorMessage;
					if (platformType == 'instagram') {
						if (data.status == 500) {
							errorMessage = influencerHandle + " seems to be an invalid Instagram handle.";
						} else {
							errorMessage = data.responseText;
						}
					} else if (platformType == 'tiktok') {
						errorMessage = influencerHandle + " seems to be an invalid TikTok handle.";
					} else if (platformType == 'youtube') {
						errorMessage = "The given Youtube Channel seems to be invalid."
					}
					$('#influencerHandleInvalid').html(errorMessage);
				}
				$('#searchLoader').addClass('d-none');
				$('#searchInfluencerIcon').removeClass('d-none');
			});
		} else {
			$('#influencerHandleInvalid').html("Enter an Influencer Handle");
		}
	} else {
		$('#platformInvalid').html("Select a Platform");
	}
}

var reportId;
var reportInfluencerId;
var influencerSFId;
var reportPresent;
var reportInfluencerHandle;

function generateReport() {
	var influencerHandle = reportInfluencerHandle;
	var platformType = $('#platform').val();

	if (influencerHandle.charAt(0) == '@') {
		influencerHandle = influencerHandle.substr(1);
	}

	var generatingMessage = 'Generating report for @' + influencerHandle + ', hold tight this will take a few seconds!';
	if (platformType == 'youtube') {
		var ytPattern = new RegExp('(https?:\/\/)?(www\.)?youtu((\.be)|(be\..{2,5}))\/((user)|(channel))\/.+');

		if (ytPattern.test(influencerHandle)) {
			var urlValues = influencerHandle.split('/');
			influencerHandle = urlValues.pop();
		}
		generatingMessage = 'Generating report for above URL, hold tight this will take a few seconds!';
	}
	$('#reportMessageCount').addClass('d-none');
	$('#reportMessageDiv').removeClass('d-none');
	$('#reportMessage').html(generatingMessage);
	$('#reportMessageLoader').removeClass('d-none');
	$('#reportMessage').removeClass('text-danger');
	$('#generateReport').addClass('disabled');
	$('#viewInfluencer').addClass('disabled');

	$.get({
		url: window.location.origin + '/brand/reports/audience-data',
		data: {
			influencerId: influencerSFId
			, deepSocialInfluencerId: reportInfluencerId
			, influencerHandle: influencerHandle
			, reportId: reportId
			, reportPresent: reportPresent
			, platform: platformType
		},
	}).done(function (data) {
		$('#reportMessageCount').html(0 + '%');
		$('#reportMessageCount').removeClass('d-none');
		$('#reportMessage').html('Download in progress ');
		var reportUrl = data["report_url"];
		downloadFile(reportUrl);
		setTimeout(function () {
			var count = $('#reportMessageCount').html();
			if (parseInt(count) < 40) {
				$('#reportMessageCount').html(20 + '%');
			}
		}, 1000);
	}).fail(function (data) {
		if (data.status == 409) {
			$('#reportMessage').html("You have reached the weekly limit of 50 reports! Please contact help@smartfluence.io to increase your limit.");
		} else if (data.status == 403) {
			WARNING(data.responseText);
			$('#reportMessage').html('');
			$('#reportMessageLoader').addClass('d-none');
			$('#reportMessage').addClass('text-danger');
		} else {
			var errorMessage;
			if (data.responseText.includes("retry_later")) {
				errorMessage = "We are pulling data for this influencer! Please try again in few minutes";
			} else if (data.responseText.includes("empty_audience_data")) {
				errorMessage = "Currently we do not have data for this influencer";
			} else if (platformType == 'instagram') {
				errorMessage = influencerHandle + " seems to be an invalid Instagram handle. Please try a different username.";
			} else if (platformType == 'tiktok') {
				errorMessage = influencerHandle + " seems to be an invalid TikTok handle. Please try a different username.";
			} else if (platformType == 'youtube') {
				errorMessage = "The given URL seems to be an invalid Youtube Channel. Please try a different channel."
			}
			$('#reportMessage').html(errorMessage);
		}
		$('#reportMessageLoader').addClass('d-none');
		$('#generateReport').removeClass('disabled');
		$('#reportMessage').addClass('text-danger');
	});
}

function resetFormSearchModal() {
	$('#influencerHandleInvalid').html('');
	$('#searchInfluencerModalFooter').addClass('d-none');
	$('#searchInfluencerModal input,#searchInfluencerModal select').val('').change();
}

function downloadFile(url) {
	console.log('Download started at ' + new Date());
	setTimeout(function () {
		var count = $('#reportMessageCount').html();
		if (parseInt(count) < 65) {
			$('#reportMessageCount').html(45 + '%');
		}
	}, 4500);
	setTimeout(function () {
		var count = $('#reportMessageCount').html();
		if (parseInt(count) < 85) {
			$('#reportMessageCount').html(65 + '%');
		}
	}, 5500);
	setTimeout(function () {
		var count = $('#reportMessageCount').html();
		if (parseInt(count) < 95) {
			$('#reportMessageCount').html(85 + '%');
		}
	}, 7500);
	fetch(url)
		.then(resp => {
			$('#reportMessageCount').html(95 + '%');
			var filename = 'report.pdf';
			var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
			var matches = filenameRegex.exec(resp.headers.get('Content-Disposition'));

			if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
			resp.blob().then(function (b) {
				const url = window.URL.createObjectURL(b);
				const a = document.createElement('a');
				a.style.display = 'none';
				a.href = url;
				a.download = filename;
				document.body.appendChild(a);
				a.click();
				$('#reportMessage').html('Download completed ');
				$('#reportMessageCount').html(100 + '%');
				$('#reportMessageLoader').addClass('d-none');

				window.URL.revokeObjectURL(url);
				$('#generateReport').removeClass('disabled');
				$('#viewInfluencer').removeClass('disabled');
			}).catch((e) => {
				$('#reportMessage').html('Download failed, try again later');
				$('#generateReport').removeClass('disabled');
				$('#viewInfluencer').removeClass('disabled');
				$('#reportMessageLoader').addClass('d-none');
			});
		}).catch((e) => {
			$('#reportMessage').html('Download failed, try again later');
			$('#generateReport').removeClass('disabled');
			$('#viewInfluencer').removeClass('disabled');
			$('#reportMessageLoader').addClass('d-none');
		});
}

$('#platform').on('change', function () {
	$('#searchInfluencerModalFooter').addClass('d-none');
	$('#influencerHandleInvalid').html('');
	$('#viewInfluencerSuccessMessageDiv').addClass('d-none');
	$('#reportMessageDiv').addClass('d-none');
	$('#platformInvalid').html("");
	var platformType = $('#platform').val();
	$('#influencerHandle').val('');

	if (platformType == 'instagram') {
		$('#influencerHandleLabel').text('Influencer Handle');
		$('#material-addon1').text('@');
	} else if (platformType == 'tiktok') {
		$('#influencerHandleLabel').text('Influencer Handle');
		$('#material-addon1').text('@');
	} else if (platformType == 'youtube') {
		$('#influencerHandleLabel').text('Channel');
		$('#material-addon1').text('URL/Name');
	}
});

var selectedUser;
var audienceGeoLocationName = '';
var influencerGeoLocationName = '';
var comparisonProfileName = '';

$(document).ready(function () {
	$('#searchLoading').addClass("d-none");
	$("#campName").prop("disabled", true);
	$('#buttonLoading').addClass("d-none");
	$('#disabledError').html("Please click search to select campaign");
	var utility = "location";

	$('#audience-location').typeahead({
		minLength: 2,
		source: function (name, result) {
			if (name.trim().length > 0) {
				$.ajax({
					url: "get-utilities/" + name + "?data=" + utility
					, method: "GET"
					, dataType: "json"
					, success: function (data) {
						audienceGeoLocationName = '';
						result(data);
					}
				})
			}
		},
		updater: function (selection) {
			audienceGeoLocationName = selection.name;
			return selection;
		}
	});

	$('#influencer-location').typeahead({
		minLength: 2,
		source: function (name, result) {
			if (name.trim().length > 0) {
				$.ajax({
					url: "get-utilities/" + name + "?data=" + utility
					, method: "GET"
					, dataType: "json"
					, success: function (data) {
						influencerGeoLocationName = '';
						result(data);
					}
				})
			}
		},
		updater: function (selection) {
			influencerGeoLocationName = selection.name;
			return selection;
		}
	});

	$('#influencerHandle').typeahead({
		minLength: 2,
		source: function (query, result) {
			if ($('#platform').val().trim().length > 0) {
				$.ajax({
					url: "get-suggestions/" + query + "?platform=" + $('#platform').val()
					, method: "GET"
					, dataType: "json"
					, success: function (data) {
						result(data);
					}
				})
			} else {
				$('#platformInvalid').html("Select a Platform");
			}
		},
		updater: function (selection) {
			selectedUser = selection;
			searchInfluncer(selection);
			return selection;
		}
	});

	$('#comparisonProfile').typeahead({
		minLength: 2,
		source: function (query, result) {
			$.ajax({
				url: "get-suggestions/" + query + "?platform=" + $("#search-platform").val()
				, method: "GET"
				, dataType: "json"
				, success: function (data) {
					comparisonProfileName = '';
					result(data);
				}
			})
		},
		updater: function (selection) {
			$('.top-influencer').removeClass("d-none");

			if ($("#search-platform").val() == 'instagram') {
				$('#influencerYoutubeLink').addClass('d-none');
				$('#influencerTiktokLink').addClass('d-none');
				$('#influencerInstagramLink').removeClass('d-none');
				$('#influencerInstagramLink').attr("href", "https://www.instagram.com/" + selection.name);
			}
			else if ($("#search-platform").val() == 'youtube') {
				$('#influencerInstagramLink').addClass('d-none');
				$('#influencerTiktokLink').addClass('d-none');
				$('#influencerYoutubeLink').removeClass('d-none');
				$('#influencerYoutubeLink').attr("href", "https://www.youtube.com/channel/" + selection.id);
			}
			else if ($("#search-platform").val() == 'tiktok') {
				$('#influencerInstagramLink').addClass('d-none');
				$('#influencerYoutubeLink').addClass('d-none');
				$('#influencerTiktokLink').removeClass('d-none');
				$('#influencerTiktokLink').attr("href", "https://www.tiktok.com/share/user/" + selection.id);
			}

			selectedUser = selection;
			comparisonProfileName = selectedUser.name;
			return selection;
		}
	});
	loadCampaignList();
	backToSearch();

});

function loadCampaignList() {
	var selectedPlatform = $("#search-platform option:selected").text();
	var campsbyPlatform = allActiveCampaigns.filter(x => x.platform_name?.toLowerCase() == selectedPlatform?.toLowerCase());

	$('#invalidCampaign').html("")
	if (campsbyPlatform.length == 0) {
		$('#campName').find('option').remove().end();
		$('#campName').append(`<option value="">Select Campaign Name</option>`)
		$('#invalidCampaign').html("Please click <a style='color: #2f008d' href='/brand/new-campaign'><u>here</u></a> to create your new campaign")
	}
	else {
		$('#campName').find('option').remove().end();
		$('#campName').append(`<option value="">Select Campaign Name</option>`)
		campsbyPlatform.forEach((element, index) => {
			$('#campName').append(`<option value="${element.campaign_id}">${element.campaign_name}</option>`)
		});
	}
}

$("#influencerHandle").keydown(function (event) {
	var platformType = $('#platform').val();
	$('#viewInfluencer').addClass('d-none');
	$('#searchInfluencerModalFooter').addClass('d-none');
	$('#influencerHandleInvalid').html('');
	$('#viewInfluencerSuccessMessageDiv').addClass('d-none');
	$('#reportMessageDiv').addClass('d-none');

	if ($("#influencerHandle").val() != "" && $("#influencerHandle").val() != undefined) {
		if (event.keyCode <= 46 || event.keyCode == 255) {
			$('#viewInfluencer').removeClass('d-none');
			$('#searchInfluencerModalFooter').removeClass('d-none');
		}
	}

	if (event.keyCode == 8) {
		$('#viewInfluencer').addClass('d-none');
		$('#searchInfluencerModalFooter').addClass('d-none');
	}

	if (event.keyCode === 13 && ($('.typeahead.dropdown-menu').css('display') == 'none' || $('.typeahead.dropdown-menu').css('display') == undefined)) {
		searchInfluncer();
	}
});

$(function () {
	$("#mailTemplate").change(
		function () {
			$('#mailTemplateInvalid').text('');
			if ($("#mailTemplate").val() != '' && $("#mailTemplate").val() != undefined && $("#mailTemplate").val() != null) {
				var element = $(this).find('option:selected');
				var mailDescription = element.attr("data-value");
				var mailSubject = element.attr("data-subject");

				var mailPlatfrom = getPlatform(getCookie("platform"));
				mailDescription = mailDescription.split("<Platform>")
					.join(mailPlatfrom);
				mailSubject = mailSubject.split("<Brand Name>").join(
					profileDetails.brandName);
				mailSubject = mailSubject.split("<Influencer Handle>")
					.join(profileName);
				mailSubject = mailSubject.split("<Estimated Pricing>")
					.join('$' + Number(influencerPricing).toFixed(2));
				mailDescription = mailDescription.split("<Influencer Handle>")
					.join(profileName);
				mailDescription = mailDescription.split("<Brand Name>").join(
					profileDetails.brandName);
				mailDescription = mailDescription.split("<Bid Amount>").join(
					$('#bid').val());
				mailDescription = mailDescription.split("<Post Type>").join(
					getPostType($('#postType').val()));
				if (getCookie("platform") == 'instagram') {
					var postDuration = getPostDuration($('#postDuration').val());
					mailDescription = mailDescription.split("<Post Duration>")
						.join(postDuration);
					var affiliateInstaText = '\nTo maximize your potential earnings, we recommend you to do the following:\n\n'
						+ '\tkeep your unique affiliate link in your bio (direct your audience to your bio)\n'
						+ '\tcreate 1 feed post\n'
						+ '\tcreate 2 stories every week\n';
					mailDescription = mailDescription.split("<Affiliate Instagram>").join(affiliateInstaText);
				} else {
					mailDescription = mailDescription.split("<Post Duration>")
						.join('');
					mailDescription = mailDescription.split("<Affiliate Instagram>").join('');
				}
				mailDescription = mailDescription.split("<Affilitate %>").join(
					$('#bid').val());
				mailDescription = mailDescription.split("<Affiliate Fee>")
					.join($('#bid').val());
				mailDescription = mailDescription.split("<apostrophe>")
					.join("'");
				$('#mailTextArea').val(mailDescription);
				$('#mailSubject').val(mailSubject);
			}
		});
});

function getPlatform(platform) {
	switch (platform) {
		case "instagram":
			platform = "Instagram";
			break;
		case "youtube":
			platform = "YouTube";
			break;
		case "tiktok":
			platform = "TikTok";
			break;
		default:
			break;
	}
	return platform;
}

function getPostType(postType) {
	switch (postType) {
		case "IntegratedVideo":
			postType = "Integrated Video";
			break;
		case "DedicatedVideo":
			postType = "Dedicated Video";
			break;
		case "PostRoll":
			postType = "Post Roll";
			break;
		case "Shoutout":
			postType = "Shout Out";
			break;
		case "SponsoredPost":
			postType = "Sponsored Post";
			break;
		default:
			break;
	}
	return postType;
}

function getPostDuration(postDuration) {
	switch (postDuration) {
		case "2H":
			postDuration = "2 hours";
			break;
		case "6H":
			postDuration = "6 hour";
			break;
		case "12H":
			postDuration = "12 hour";
			break;
		case "24H":
			postDuration = "24 hour";
			break;
		case "1W":
			postDuration = "1 week";
			break;
		case "P":
			postDuration = "permanent";
			break;
		default:
			break;
	}
	return postDuration;
}

function validateMailContent() {
	var mailTemplate = $("#mailTemplate").val();
	var mailDescription = $('#mailTextArea').val();
	var mailSubject = $('#mailSubject').val();

	if (mailTemplate == '' || mailTemplate == undefined) {
		$('#mailTemplateInvalid').text('Select Mail Template');
		return;
	}
	if (mailDescription == '' || mailDescription == undefined) {
		$('#mailTextAreaInvalid').text('Enter data');
		return;
	}
	if (mailSubject == '' || mailSubject == undefined) {
		$('#mailSubjectInvalid').text('Enter Subject');
		return;
	}
	sendEvent('#SubmitBid', 4);
}

$('#mailTextArea').on('input', function () {
	$('#mailTextAreaInvalid').text('');
});

$('#mailSubject').on('input', function () {
	$('#mailSubjectInvalid').text('');
});
