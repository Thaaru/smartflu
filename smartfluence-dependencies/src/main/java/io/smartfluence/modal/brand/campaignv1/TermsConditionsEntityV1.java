package io.smartfluence.modal.brand.campaignv1;


import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TermsConditionsEntityV1 {

    private Long termId;
    private String termsCondition;
    private String termsConditionFilePath;
    private TermsTypeEntity type;
    private IsActive isActive;
    private String fileName;
}
