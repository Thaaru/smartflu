package io.smartfluence.influencer.entities;

import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CurrentPartnershipCampainDetails {
    String campaignId;
    String name;
    @Enumerated(EnumType.STRING)
    ProposalStatus proposalStatus;
}
