package io.smartfluence.brand.management.entity;

import io.smartfluence.brand.management.entityv1.CampaignProductsV1;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OfferTypeResponse {

    private Double paymentOffer;

    private Double commissionValue;

    private int commissionAffTypeId;
    private String commissionValueType;
    private String commissionAffiliateType;
    private String commissionAffiliateTypeName;
    private String commissionAffiliateCustomName;
    private String commissionAffiliateDetails;
    private String currency;
    private List<CampaignProductsV1> campaignProductsV1List;
}
