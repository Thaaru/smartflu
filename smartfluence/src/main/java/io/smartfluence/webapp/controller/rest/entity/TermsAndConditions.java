package io.smartfluence.webapp.controller.rest.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "terms_conditions")
public class TermsAndConditions implements Serializable {
    @Id
    @Column(name = "term_id")
    private Long termId;

    @Column(name = "terms_condition")
    private String termsCondition;

    @Column(name = "terms_condition_file_path")
    private String termsConditionFilePath;

    @Column(name = "status")
    private String status;

}
