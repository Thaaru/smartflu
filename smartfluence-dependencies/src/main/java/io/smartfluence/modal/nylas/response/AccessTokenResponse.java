package io.smartfluence.modal.nylas.response;

import lombok.Data;

@Data
public class AccessTokenResponse {

	private String accessToken;

	private String accountId;

	private String accountName;

	private String accountProvider;

	private String accountEmailAddesss;

	private String accountSyncState;
}