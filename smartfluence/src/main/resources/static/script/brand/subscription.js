$('.mini-loader').hide();
var activeSubscriptionJson;
var activePlanId = '';
var willCancel = false;
var endDate = null;
var startDate = null
function subscription() {
	$('.mini-loader').show();
	activePlan();
}
function activePlan() {
	$.getJSON({
		url : 'plans/active'
	}).done(function(res) {
		activeSubscriptionJson = res;
		stripePlans()
	}).fail(function() {
		stripePlans();
	});
}
function stripePlans() {
	$.getJSON({
		url : 'plans'
	}).done(function(res) {
		$('.mini-loader').hide();
		if (!res.error) {
			bindPlans(res.data);
		} else {
			bindPlans({});
		}
	}).fail(function() {
		$('.mini-loader').hide();
	});
}
function bindPlans(data) {

	if (activeSubscriptionJson) {
		if (activeSubscriptionJson.plan) {
			activePlanId = activeSubscriptionJson.plan.id;
			willCancel = activeSubscriptionJson.cancelAtPeriodEnd;
			startDate = activeSubscriptionJson.currentPeriodStart * 1000;
			endDate = activeSubscriptionJson.currentPeriodEnd * 1000;
		}
	}

	var content = '';
	for ( var i in data) {
		var plan = data[i];
		content += '<div class="col-md">' + '<div class="generic_content '
				+ ((plan.nickname == 'Standard' && activePlanId == '')
						|| plan.id == activePlanId ? 'active ' : '')
				+ (willCancel ? 'cancel ' : '')
				+ ' clearfix">'
				+ '<div class="generic_head_price clearfix">'
				+ '<div class="generic_head_content clearfix">'
				+ '<div class="head_bg"></div>'
				+ '<div class="head">'
				+ '<span>'
				+ plan.nickname
				+ '</span>'
				+ '</div>'
				+ '</div>'
				+ '<div class="generic_price_tag clearfix">'
				+ '<span class="price"> <span class="sign">$</span> <span class="currency">'
				+ (Number(plan.amount) / 100).toFixed(0)
				+ '</span> <span class="cent">.'
				+ ((Number(plan.amount) % 100) + '').lpad('0', 2)
				+ '</span> <span class="month">/'
				+ plan.interval.substring(0, 3).toUpperCase()
				+ '</span>'
				+ '</span>'
				+ '</div>'
				+ '</div>'
				+ '<div class="generic_feature_list" style="min-height: 228px;">'
				+ '<ul>'
				+ '<li><span>'
				+ ((plan.transform_usage != null && plan.transform_usage) ? plan.transform_usage.divide_by
						: 1) + '</span> Credits</li>'
		for ( var k in plan.metadata) {
			content += '<li><span>' + plan.metadata[k] + '</span></li>';
		}

		var activateBtn;
		if (plan.id == activePlanId) {
			content += '<li><span>Invoice:</span>'
					+ moment(startDate).format('DD to ')
					+ moment(endDate).format('DD MMM, YYYY') + '</li>'
			if (willCancel) {
				content += '<li class="cancel"><span>Scheduled for cancellation</span></li>';
				activateBtn = '<a class="cancel scheduled" href="#" >';
				for (i = 0; i <= 30; i++) {
					activateBtn += '&nbsp;';
				}
				activateBtn += '</a>';
			} else {
				activateBtn = '<a class="cancel" href="#" onclick="cancel();">';
				for (i = 0; i <= 30; i++) {
					activateBtn += '&nbsp;';
				}
				activateBtn += '</a>';
			}

		} else {
			activateBtn = '<a class="" href="#" onclick="activate(\'' + plan.id
					+ '\')">Activate</a>';
		}
		content += '</ul>' + '</div>'
				+ '<div class="generic_price_btn clearfix">' + activateBtn
				+ '</div>' + '</div>' + '</div>';
	}

	content += '<div class="col-md">'
			+ '<div class="generic_content clearfix">'
			+ '<div class="generic_head_price clearfix">'
			+ '<div class="generic_head_content clearfix">'
			+ '<div class="head_bg"></div>'
			+ '<div class="head">'
			+ '<span>Enterprise</span>'
			+ '</div>'
			+ '</div>'
			+ '<div class="generic_price_tag clearfix">'
			+ '<span class="price"> <span class="sign">$</span> <span class="currency">???</span> <span class="cent"></span> <span class="month">/MON</span>'
			+ '</span>' + '</div>' + '</div>'
			+ '<div class="generic_feature_list" style="min-height: 228px;">'
			+ '<ul>' + '<li><span>X</span> Credits</li>'
			+ '<li><span>Standard Package</span></li>'
			+ '<li><span>Advanced Influencer Analytics</span></li>' + '</ul>'
			+ '</div>' + '<div class="generic_price_btn clearfix">'
			+ '<a class="" href="#">Contact us</a>' + '</div>' + '</div>'
			+ '</div>';

	$('#generic_price_table .container .row').html(content);
	var minHeight = 0;
	$('.generic_feature_list').each(function() {
		if ($(this).height() > minHeight) {
			minHeight = $(this).height();
		}
	});
	$('.generic_feature_list').css('min-height', minHeight);
}

function activate(planId) {
	$('.mini-loader').show();
	$('#subscriptionModalLabel').html('Activate plan');
	$('#subscriptionModalMsg')
			.html(
					activePlanId == '' ? 'Are you sure that you want to subscribe to this plan?'
							: 'Are you sure that you want to switch to this plan?');
	$('#subscriptionModalConfirm').attr('onclick',
			"activateConfirm('" + planId + "')");
	$('#subscriptionModal').modal();

}

function activateConfirm(planId) {
	$.post('plans/activate/' + planId).done(function(res) {
		SUCCESS('Subscribed to plan!');
		setTimeout(function() {
			location.reload();
		}, 1000)

	}).fail(function(res) {
		ERROR(res.responseJSON.message);
		$('.mini-loader').hide();

	});
}

function cancel() {

	$('.mini-loader').show();
	$('#subscriptionModalLabel').html('Cancel plan');
	$('#subscriptionModalMsg').html(
			'Are you sure that you want to cancel this plan?');
	$('#subscriptionModalConfirm').attr('onclick',
			"cancelConfirm()");
	$('#subscriptionModal').modal();

}

function cancelConfirm() {
	$.post('plans/cancel').done(function(res) {
		SUCCESS('Subscribtion to plan cancelled!');
		setTimeout(function() {
			location.reload();
		}, 1000)
	}).fail(function(res) {
		ERROR(res.responseJSON.message);
		$('.mini-loader').hide();

	});
}