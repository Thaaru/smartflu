package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.influencer.entities.CampaignInfluencerDetails;
import io.smartfluence.influencer.entities.CampaignInfluencerId;

import java.util.List;

@Repository
public interface CampaignInfluencerDetailsRepo extends JpaRepository<CampaignInfluencerDetails, CampaignInfluencerId>{
    CampaignInfluencerDetails findByCampaignInfluencerId(CampaignInfluencerId campaignInfluencerId);
List<CampaignInfluencerDetails> findAllByCampaignInfluencerIdInfluencerId(String influencerId);
List<CampaignInfluencerDetails> findAllByInfluencerEmail(String email);
}
