package io.smartfluence.accounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import io.smartfluence.accounts.dao.ToolsAPIDao;
import io.smartfluence.accounts.modal.InfluencerDataResponseModal;
import io.smartfluence.accounts.modal.InfluencersLookalikes;
import io.smartfluence.accounts.modal.InfluencersSimilarUsers;
import io.smartfluence.accounts.modal.InfluencersUserProfile;
import io.smartfluence.cassandra.entities.AudienceReportHistory;
import io.smartfluence.cassandra.entities.primary.AudienceReportHistoryKey;
import io.smartfluence.constants.Platforms;
import io.smartfluence.mail.MailService;
import io.smartfluence.modal.brand.exploreinfluencer.GetInfluencersResponseModel;
import io.smartfluence.modal.google.recaptcha.VerifyRecaptchaResponse;
import io.smartfluence.modal.social.report.AudienceGeo;
import io.smartfluence.modal.social.report.InfluencersOverlap;
import io.smartfluence.modal.social.report.NameAndWeight;
import io.smartfluence.modal.social.report.OverlapData;
import io.smartfluence.modal.social.report.ReportData;
import io.smartfluence.modal.social.report.ReportResult;
import io.smartfluence.modal.social.search.request.CodeWeight;
import io.smartfluence.modal.social.search.request.ComparisonProfile;
import io.smartfluence.modal.social.search.request.Filter;
import io.smartfluence.modal.social.search.request.IndustryAndLocation;
import io.smartfluence.modal.social.search.request.InfluencersDataRequest;
import io.smartfluence.modal.social.search.request.Paging;
import io.smartfluence.modal.social.search.request.Range;
import io.smartfluence.modal.social.search.request.SortResponse;
import io.smartfluence.modal.social.search.request.WithContact;
import io.smartfluence.modal.social.search.response.InfluencersDataResponse;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserInfo;
import io.smartfluence.modal.social.user.youtube.RawYouTubeChannelInfo;
import io.smartfluence.modal.validation.DownloadReportModal;
import io.smartfluence.modal.validation.GenerateReportModal;
import io.smartfluence.modal.validation.InfluencerDataModal;
import io.smartfluence.modal.validation.InfluencerLookalikeModal;
import io.smartfluence.modal.validation.InfluencerOverlapModal;
import io.smartfluence.mysql.entities.APIServiceEmailValidation;
import io.smartfluence.mysql.entities.APIServiceIPValidation;
import io.smartfluence.mysql.entities.APIServiceMaster;
import io.smartfluence.mysql.entities.IndustryMaster;
import io.smartfluence.props.SearchInfluencerProps;
import io.smartfluence.proxy.social.SocialDataProxy;
import io.smartfluence.util.EmailUtil;
import io.smartfluence.util.image.ImageProcessor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Component
@Configuration
public class ToolsAPIServiceImpl implements ToolsAPIService {

	@Autowired
	private ToolsAPIDao toolsAPIDao;

	@Autowired
	private SocialDataProxy socialDataProxy;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private MailService mailService;

	@Autowired
	private EmailUtil emailUtil;

	@Autowired
	private SearchInfluencerProps searchInfluencerProps;

	private static final String JSON_FMT = "json";

	private static final String PDF_FMT = "pdf";

	private static final String STATUS = "ok";

	private static final String INFLUENCERDATA = "InfluencerData";

	private static final String INFLUENCERSOVERLAP = "InfluencersOverlap";

	private static final String INFLUENCERLOOKALIKE = "InfluencerLookalike";

	private static final String AUDIENCEREPORT = "AudienceReport";

	private static final String DOWNLOADREPORT = "DownloadReport";

	private static final String SMARTFLUENCE_IO = "www.smartfluence.io";

	@Value("${google.recaptcha.secret.key}")
	private String recaptchaSecretKey;

	@Value("${google.api.verify.recaptcha.url}")
	private String verifyRecaptchaUrl;

	@Override
	public ResponseEntity<Object> verifyUserRecaptcha(String value) {
		log.info("Verifying Recaptcha......!");
		return new ResponseEntity<>(verifyRecaptcha(value), HttpStatus.OK);
	}

	private VerifyRecaptchaResponse verifyRecaptcha(String value) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("secret", recaptchaSecretKey);
		map.add("response", value);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

		ResponseEntity<VerifyRecaptchaResponse> response = restTemplate.postForEntity(verifyRecaptchaUrl, request, VerifyRecaptchaResponse.class);

		if (response.getStatusCode().is2xxSuccessful())
			return response.getBody();

		return null;
	}

	private int validateAPIRequest(String service, String email, String recaptcha, String ip) {

		APIServiceMaster apiServiceMaster = toolsAPIDao.getApiServiceMasterByServiceName(service);

		if ((apiServiceMaster.getValidations() & 1) == 1 && !apiServiceMaster.isActive())
			return 1;

		if ((apiServiceMaster.getValidations() & 2) == 2 && !emailUtil.emailValidation(email.toLowerCase()))
			return 2;

		if ((apiServiceMaster.getValidations() & 4) == 4) {
			Optional<APIServiceEmailValidation> apiServiceEmailValidation = toolsAPIDao.getAPIServiceEmailValidation(email, service);
			if (apiServiceEmailValidation.isPresent() && apiServiceEmailValidation.get().getDailyCount() > apiServiceMaster.getValidationCount())
				return 4;
		}

		if ((apiServiceMaster.getValidations() & 8) == 8) {
			Optional<APIServiceIPValidation> apiServiceIPValidation = toolsAPIDao.getAPIServiceIPValidation(ip, service);
			if (apiServiceIPValidation.isPresent() && apiServiceIPValidation.get().getDailyCount() > apiServiceMaster.getValidationCount())
				return 8;
		}

		if ((apiServiceMaster.getValidations() & 16) == 16) {
			VerifyRecaptchaResponse verifyRecaptchaResponse = verifyRecaptcha(recaptcha);

			if (Objects.isNull(verifyRecaptchaResponse))
				return 16;

			if (!verifyRecaptchaResponse.getSuccess())
				return 16;

			if (!verifyRecaptchaResponse.getHostname().equals(SMARTFLUENCE_IO))
				return 16;
		}

		return 0;
	}

	private ResponseEntity<Object> apiFailureResponse(int flag) {
		if (flag == 1)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		if (flag == 2)
			return new ResponseEntity<>("Enter valid business email address", HttpStatus.BAD_REQUEST);
		if (flag == 4 || flag == 8)
			return new ResponseEntity<>("You have crossed maximun request per day, please try again tomorrow", HttpStatus.BAD_REQUEST);
		if (flag == 16)
			return new ResponseEntity<>("Recaptcha verification expired, please try again", HttpStatus.BAD_REQUEST);
		return null;
	}

	private byte[] extractProfileImage(String profileImage) {
		try {
			return new ImageProcessor(profileImage).processFinalImage();
		} catch (Exception e) {
			e.getStackTrace();
		}
		return null;
	}

	private String checkInfluencerName(String userName) {
		return userName = userName.startsWith("@") ? userName.substring(1).toLowerCase() : userName.toLowerCase();
	}

	private void saveAPIServiceReportData(String email, String remoteAddr, String userId, String fullName, String service, boolean saveReportHistory) {
		toolsAPIDao.saveAPIServiceEmailValidation(email, service);
		toolsAPIDao.saveAPIServiceIPValidation(remoteAddr, service);

		if (saveReportHistory) {
			toolsAPIDao.saveAudienceHistory(AudienceReportHistory.builder()
					.key(AudienceReportHistoryKey.builder()
							.businessEmailAddress(email).influencerDeepSocialId(userId)
							.createdAt(new Date()).build()).service(service).influencerUserName(fullName).build());
		}
	}

	@Override
	public ResponseEntity<Object> getInfluencerData(InfluencerDataModal influencerDataModal, HttpServletRequest servletRequest) {

		int apiValidationResponse = validateAPIRequest(INFLUENCERDATA, influencerDataModal.getEmail(), influencerDataModal.getGrecaptcha(),
				servletRequest.getRemoteAddr());

		if (apiValidationResponse != 0)
			return apiFailureResponse(apiValidationResponse);

		log.info("Getting InfluencerData for = " + influencerDataModal.getInfluencerHandle());
		ReportData reportData = null;

		try {
			if ((reportData = checkInfluencerReport(influencerDataModal.getInfluencerHandle(), influencerDataModal.getPlatform())) == null)
				return new ResponseEntity<>("This " + influencerDataModal.getPlatform() + " user seems to be invalid", HttpStatus.NOT_FOUND);
			saveAPIServiceReportData(influencerDataModal.getEmail(), servletRequest.getRemoteAddr(), reportData.getUserProfile().getUserId(),
					reportData.getUserProfile().getFullName().replace(" ", ""), INFLUENCERDATA, true);
		} catch (Exception e) {
			log.info("Failed to get DeepSocial Data for Get Influencers Data");
			return new ResponseEntity<>("We are facing some technical issue, please try after sometime.", HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(setInfluencerData(reportData), HttpStatus.OK);
	}

	private ReportData checkInfluencerReport(String userName, String platform) {
		userName = checkInfluencerName(userName);
		ReportData reportData = getReportIdIfUserExists(userName, platform);

		try {
			if (Objects.isNull(reportData)) {
				if ((reportData = createNewReport(userName, platform)) == null)
					return null;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}

		return reportData;
	}

	private ReportData getReportIdIfUserExists(String influencerUserName, String platform) {
		Map<String, ArrayList<ReportResult>> reportList = socialDataProxy.getReportsList(40, platform, influencerUserName);

		if (reportList.containsKey("results") && reportList.get("results") != null) {
			ArrayList<ReportResult> results = reportList.get("results");
			for (ReportResult result : results) {
				if (result.getUser_profile().getUserId().equals(influencerUserName)
						|| result.getUser_profile().getFullName().equals(influencerUserName)
						|| result.getUser_profile().getHandle().equals(influencerUserName)) {
					return socialDataProxy.getReport(result.getId(), JSON_FMT);
				}
			}
		}
		return null;
	}

	private ReportData createNewReport(String influencerUserName, String platform) {
		log.info("Report not found for {}. Creating new Report", influencerUserName);
		ReportData reportData = socialDataProxy.createNewReport(influencerUserName, 0, 1, true, platform);

		if (reportData.isSuccess() && !Objects.isNull(reportData.getReportInfo())
				&& !Objects.isNull(reportData.getReportInfo().getReportId())) {
			log.info("New Report {}, generated for {}", reportData.getReportInfo().getReportId(), reportData.getUserProfile().getHandle());
			return reportData;
		}
		return null;
	}

	private InfluencerDataResponseModal setInfluencerData(ReportData reportData) {
		InfluencerDataResponseModal influencerDataResponseModal = InfluencerDataResponseModal.builder()
				.userProfile(setInfluencerUserProfileData(reportData))
				.audienceCredibility(reportData.getAudienceFollowers().getData().getAudienceCredibility())
				.audienceGender(reportData.getAudienceFollowers().getData().getAudienceGender())
				.audienceAge(reportData.getAudienceFollowers().getData().getAudienceAge())
				.audienceInterests(setTopAudienceInterestsData(reportData))
				.audienceGeo(setAudienceGeoData(reportData))
				.audienceLookalikes(setAudienceLookalikeData(reportData)).build();

		return influencerDataResponseModal;
	}

	private InfluencersUserProfile setInfluencerUserProfileData(ReportData reportData) {
		return InfluencersUserProfile.builder().platformType(reportData.getUserProfile().getPlatformType())
				.handle(reportData.getUserProfile().getHandle()).url(reportData.getUserProfile().getUrl())
				.profileImageUrl(extractProfileImage(reportData.getUserProfile().getProfileImage()))
				.fullName(reportData.getUserProfile().getFullName().replace(" ", "")).description(reportData.getUserProfile().getDescription())
				.followers(reportData.getUserProfile().getFollowers()).engagementRate(reportData.getUserProfile().getEngagementRate())
				.similarUsers(setSimilarUsersData(reportData)).build();
	}

	private List<InfluencersSimilarUsers> setSimilarUsersData(ReportData reportData) {
		List<InfluencersSimilarUsers> influencersSimilarUsersList = new LinkedList<>();

		for (int i = 0; i < reportData.getUserProfile().getSimilarUsers().size(); i++) {
			InfluencersSimilarUsers influencersSimilarUsers = InfluencersSimilarUsers.builder()
					.profileImageUrl(extractProfileImage(reportData.getUserProfile().getSimilarUsers().get(i).getProfileImage()))
					.handle(reportData.getUserProfile().getSimilarUsers().get(i).getHandle())
					.followers(reportData.getUserProfile().getSimilarUsers().get(i).getFollowers())
					.fullName(reportData.getUserProfile().getSimilarUsers().get(i).getFullName())
					.url(reportData.getUserProfile().getSimilarUsers().get(i).getUrl())
					.engagement(reportData.getUserProfile().getSimilarUsers().get(i).getEngagement()).build();

			influencersSimilarUsersList.add(influencersSimilarUsers);

			if (i == 2)
				break;
		}
		return influencersSimilarUsersList;
	}

	private List<NameAndWeight> setTopAudienceInterestsData(ReportData reportData) {
		List<NameAndWeight> audienceInterestsList = new LinkedList<>();

		for (int i = 0; i < reportData.getAudienceFollowers().getData().getAudienceInterests().size(); i++) {
			NameAndWeight audienceInterests = new NameAndWeight();
			audienceInterests.setName(reportData.getAudienceFollowers().getData().getAudienceInterests().get(i).getName());
			audienceInterests.setWeight(reportData.getAudienceFollowers().getData().getAudienceInterests().get(i).getWeight());

			audienceInterestsList.add(audienceInterests);
			if (i == 2)
				break;
		}
		return audienceInterestsList;
	}

	private AudienceGeo setAudienceGeoData(ReportData reportData) {
		AudienceGeo audienceGeo = new AudienceGeo();
		audienceGeo.setCountries(setAudienceCountries(reportData));
		audienceGeo.setStates(setAudienceStates(reportData));

		return audienceGeo;
	}

	private List<NameAndWeight> setAudienceCountries(ReportData reportData) {
		List<NameAndWeight> audienceCountriesList = new LinkedList<>();
		for (int i = 0; i < reportData.getAudienceFollowers().getData().getAudienceGeo().getCountries().size(); i++) {
			NameAndWeight audienceCountries = new NameAndWeight();
			audienceCountries.setName(reportData.getAudienceFollowers().getData().getAudienceGeo().getCountries().get(i).getName());
			audienceCountries.setWeight(reportData.getAudienceFollowers().getData().getAudienceGeo().getCountries().get(i).getWeight());

			audienceCountriesList.add(audienceCountries);
			if (i == 2)
				break;
		}
		return audienceCountriesList;
	}

	private List<NameAndWeight> setAudienceStates(ReportData reportData) {
		List<NameAndWeight> audienceStatesList = new LinkedList<>();
		for (int i = 0; i < reportData.getAudienceFollowers().getData().getAudienceGeo().getStates().size(); i++) {
			NameAndWeight audienceStates = new NameAndWeight();
			audienceStates.setName(reportData.getAudienceFollowers().getData().getAudienceGeo().getStates().get(i).getName());
			audienceStates.setWeight(reportData.getAudienceFollowers().getData().getAudienceGeo().getStates().get(i).getWeight());

			audienceStatesList.add(audienceStates);
			if (i == 2)
				break;
		}
		return audienceStatesList;
	}

	private List<InfluencersLookalikes> setAudienceLookalikeData(ReportData reportData) {
		List<InfluencersLookalikes> influencersAudienceLookalikeList = new LinkedList<>();

		for (int i = 0; i < reportData.getAudienceFollowers().getData().getAudienceLookalikes().size(); i++) {
			InfluencersLookalikes influencersLookalikes = InfluencersLookalikes.builder()
					.profileImageUrl(extractProfileImage(reportData.getAudienceFollowers().getData().getAudienceLookalikes().get(i).getProfileImage()))
					.handle(reportData.getAudienceFollowers().getData().getAudienceLookalikes().get(i).getHandle())
					.followers(reportData.getAudienceFollowers().getData().getAudienceLookalikes().get(i).getFollowers())
					.fullName(reportData.getAudienceFollowers().getData().getAudienceLookalikes().get(i).getFullName())
					.url(reportData.getAudienceFollowers().getData().getAudienceLookalikes().get(i).getUrl())
					.engagement(reportData.getAudienceFollowers().getData().getAudienceLookalikes().get(i).getEngagement()).build();

			influencersAudienceLookalikeList.add(influencersLookalikes);

			if (i == 2)
				break;
		}
		return influencersAudienceLookalikeList;
	}

	@Override
	public ResponseEntity<Object> getInfluencersOverlap(InfluencerOverlapModal influencerOverlapModal, HttpServletRequest servletRequest) {

		int apiValidationResponse = validateAPIRequest(INFLUENCERSOVERLAP, influencerOverlapModal.getEmail(), influencerOverlapModal.getGrecaptcha(),
				servletRequest.getRemoteAddr());

		if (apiValidationResponse != 0)
			return apiFailureResponse(apiValidationResponse);

		try {
			influencerOverlapModal.setRemoteIP(servletRequest.getRemoteAddr());
			return getInfluencersOverlapData(influencerOverlapModal);
		} catch (Exception e) {
			log.error("Influencers Overlap not found");
			return ResponseEntity.status(HttpStatus.FOUND).body("Influencers overlap data is being populated, Please try after sometime");
		}
	}

	private ResponseEntity<Object> getInfluencersOverlapData(InfluencerOverlapModal influencerOverlapModal) {
		List<OverlapData> overlapData = Lists.newLinkedList();

		if (influencerOverlapModal.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			return getInstagramOverlapData(influencerOverlapModal, overlapData);

		else if (influencerOverlapModal.getPlatform().equalsIgnoreCase(Platforms.YOUTUBE.name()))
			return getYouTubeOverlapData(influencerOverlapModal, overlapData);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please provide valid platform");
	}

	private ResponseEntity<Object> getInstagramOverlapData(InfluencerOverlapModal influencerOverlapModal, List<OverlapData> overlapData) {
		try {
			RawInstagramUserInfo rawBaseUser = socialDataProxy.getInstagramUserInfo(checkInfluencerName(influencerOverlapModal.getBaseUser()));
			RawInstagramUserInfo rawComparisonUser = socialDataProxy.getInstagramUserInfo(checkInfluencerName(influencerOverlapModal.getComparisonUser()));

			if (rawBaseUser.getStatus().equalsIgnoreCase(STATUS) && rawComparisonUser.getStatus().equalsIgnoreCase(STATUS)) {
				String urls = getOverlapUrls(rawBaseUser.getUser().getPk().toString(), rawComparisonUser.getUser().getPk().toString());
				ResponseEntity<InfluencersOverlap> influencersOverlapData = getOverlapData(influencerOverlapModal.getPlatform(), urls);

				if (influencersOverlapData.getStatusCode().is2xxSuccessful()) {
					processInstagramInfluencer(overlapData, influencersOverlapData, rawBaseUser, rawComparisonUser);
					saveAPIServiceReportData(influencerOverlapModal.getEmail(), influencerOverlapModal.getRemoteIP(),
							rawBaseUser.getUser().getPk().toString() + " & " + rawComparisonUser.getUser().getPk().toString(),
							rawBaseUser.getUser().getFullName() + " & " + rawComparisonUser.getUser().getFullName(), INFLUENCERSOVERLAP, true);
					return new ResponseEntity<>(overlapData, HttpStatus.OK);
				}
			}
		} catch (Exception e) {
			log.info("Failed to get DeepSocial Data for Instagram Overlap Data");
			return new ResponseEntity<>("We are facing some technical issue, please try after sometime.", HttpStatus.NOT_FOUND);
		}
	
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Influencers seems to be invalid");
	}

	private ResponseEntity<Object> getYouTubeOverlapData(InfluencerOverlapModal influencerOverlapModal, List<OverlapData> overlapData) {
		try {
			RawYouTubeChannelInfo rawBaseUser = socialDataProxy.getYouTubeChannelInfo(checkInfluencerName(influencerOverlapModal.getBaseUser()));
			RawYouTubeChannelInfo rawComparisonUser = socialDataProxy.getYouTubeChannelInfo(checkInfluencerName(influencerOverlapModal.getComparisonUser()));

			if (rawBaseUser.getSuccess() && rawComparisonUser.getSuccess()) {
				String urls = getOverlapUrls(rawBaseUser.getChannel().getChannelId(), rawComparisonUser.getChannel().getChannelId());
				ResponseEntity<InfluencersOverlap> influencersOverlapData = getOverlapData(influencerOverlapModal.getPlatform(), urls);

				if (influencersOverlapData.getStatusCode().is2xxSuccessful()) {
					processYouTubeInfluencer(overlapData, influencersOverlapData, rawBaseUser, rawComparisonUser);
					saveAPIServiceReportData(influencerOverlapModal.getEmail(), influencerOverlapModal.getRemoteIP(),
							rawBaseUser.getChannel().getChannelId() + " & " + rawComparisonUser.getChannel().getChannelId(),
							rawBaseUser.getChannel().getFullName() + " & " + rawComparisonUser.getChannel().getFullName(), INFLUENCERSOVERLAP, true);
					return new ResponseEntity<>(overlapData, HttpStatus.OK);
				}
			}
		} catch (Exception e) {
			log.info("Failed to get DeepSocial Data for Youtube Overlap Data");
			return new ResponseEntity<>("We are facing some technical issue, please try after sometime.", HttpStatus.NOT_FOUND);
		}
	
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Influencers seems to be invalid");
	}

	private ResponseEntity<InfluencersOverlap> getOverlapData(String platform, String urls) {
		return socialDataProxy.getOverlapData(urls, platform, JSON_FMT, false);
	}

	private String getOverlapUrls(String rawBaseUser, String rawComparisonUser) {
		return rawBaseUser + "," + rawComparisonUser;
	}

	private void processInstagramInfluencer(List<OverlapData> overlapData, ResponseEntity<InfluencersOverlap> influencersOverlapData,
			RawInstagramUserInfo rawBaseUser, RawInstagramUserInfo rawComparisonUser) {

		influencersOverlapData.getBody().getOverlap().forEach(data -> {
			if(!StringUtils.isEmpty(data.getUserId())) {
				if (data.getUserId().equalsIgnoreCase(rawBaseUser.getUser().getPk().toString()))
					setInstagramOverlapResponse(data, overlapData, influencersOverlapData, rawBaseUser, "base");
				else
					setInstagramOverlapResponse(data, overlapData, influencersOverlapData, rawComparisonUser, "comparison");
			}
		});
	}

	private void processYouTubeInfluencer(List<OverlapData> overlapData,
			ResponseEntity<InfluencersOverlap> influencersOverlapData, RawYouTubeChannelInfo rawBaseUser,
			RawYouTubeChannelInfo rawComparisonUser) {

		influencersOverlapData.getBody().getOverlap().forEach(data -> {
			if(!StringUtils.isEmpty(data.getUserId())) {
				if (data.getUserId().equalsIgnoreCase(rawBaseUser.getChannel().getChannelId()))
					setYouTubeOverlapResponse(data, overlapData, influencersOverlapData, rawBaseUser, "base");
				else
					setYouTubeOverlapResponse(data, overlapData, influencersOverlapData, rawComparisonUser, "comparison");
			}
		});
	}

	private void setInstagramOverlapResponse(OverlapData data, List<OverlapData> overlapData,
			ResponseEntity<InfluencersOverlap> influencersOverlapData, RawInstagramUserInfo instagramUser, String profileName) {
		overlapData.add(OverlapData.builder().profileName(profileName)
				.handle(data.getHandle()).description(instagramUser.getUser().getBiography())
				.profileImageUrl(extractProfileImage(instagramUser.getUser().getProfilePicUrl())).overlappingPercentage(data.getOverlappingPercentage())
				.totalFollowers(influencersOverlapData.getBody().getReportInfo().getTotalFollowers())
				.totalUniqueFollowers(influencersOverlapData.getBody().getReportInfo().getTotalUniqueFollowers()).build());
	}

	private void setYouTubeOverlapResponse(OverlapData data, List<OverlapData> overlapData,
			ResponseEntity<InfluencersOverlap> influencersOverlapData, RawYouTubeChannelInfo youtubeUser, String profileName) {
		overlapData.add(OverlapData.builder().profileName(profileName)
				.handle(data.getHandle()).description(youtubeUser.getChannel().getDescription())
				.profileImageUrl(extractProfileImage(youtubeUser.getChannel().getProfileImage())).overlappingPercentage(data.getOverlappingPercentage())
				.totalFollowers(influencersOverlapData.getBody().getReportInfo().getTotalFollowers())
				.totalUniqueFollowers(influencersOverlapData.getBody().getReportInfo().getTotalUniqueFollowers()).build());
	}

	@Override
	public ResponseEntity<Object> getInfluencerslookalike(InfluencerLookalikeModal influencerLookalikeModal, HttpServletRequest servletRequest) {

		int apiValidationResponse = validateAPIRequest(INFLUENCERLOOKALIKE, influencerLookalikeModal.getEmail(), influencerLookalikeModal.getGrecaptcha(),
				servletRequest.getRemoteAddr());

		if (apiValidationResponse != 0)
			return apiFailureResponse(apiValidationResponse);

		List<GetInfluencersResponseModel> getInfluencersResponseModel = Lists.newLinkedList();
		log.info("Props {}", searchInfluencerProps);

		try {
			Optional<IndustryMaster> industryMaster = toolsAPIDao.getIndustryMaster(influencerLookalikeModal.getIndustry());

			if (industryMaster.isPresent()) {
				ObjectMapper mapper = new ObjectMapper();
				InfluencersDataRequest request = new InfluencersDataRequest();
				createSearchInfluencerRequest(influencerLookalikeModal, request, industryMaster.get());
				log.info("Request {}", mapper.writeValueAsString(request));

				ResponseEntity<InfluencersDataResponse> response = socialDataProxy
						.searchinfluencers(searchInfluencerProps.getAutoUnhide(), influencerLookalikeModal.getPlatform(), request);
				log.info("Response {}", mapper.writeValueAsString(response));

				if(response.getStatusCode().is2xxSuccessful()) {
					processSearchInfluencersResponse(influencerLookalikeModal.getEmail(), getInfluencersResponseModel, response);
					saveAPIServiceReportData(influencerLookalikeModal.getEmail(), servletRequest.getRemoteAddr(), "", "", INFLUENCERLOOKALIKE, false);
				}
			}
		} catch (Exception e) {
			log.info("Failed to get DeepSocial Data for Influencers Lookalike");
			return new ResponseEntity<>("We are facing some technical issue, please try after sometime.", HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(getInfluencersResponseModel, HttpStatus.OK);
	}

	private void processSearchInfluencersResponse(String email, List<GetInfluencersResponseModel> getInfluencersResponseModel,
			ResponseEntity<InfluencersDataResponse> response) {
		response.getBody().getAccounts().forEach(data -> {

			if(!StringUtils.isEmpty(data.getAccount().getUser().getUserId())) {
				getInfluencersResponseModel.add(GetInfluencersResponseModel.builder()
						.url(data.getAccount().getUser().getUrl())
						.influencerHandle(data.getAccount().getUser().getUserName())
						.profileImageUrl(extractProfileImage(data.getAccount().getUser().getPicture()))
						.followers(data.getAccount().getUser().getFollowers())
						.engagementScore(data.getAccount().getUser().getEngagementRate())
						.audienceCheck(data.getMatch().getAudienceLikers().getAudienceData().getAudienceCredibility()).build());

				toolsAPIDao.saveAudienceHistory(AudienceReportHistory.builder()
						.key(AudienceReportHistoryKey.builder()
								.businessEmailAddress(email).influencerDeepSocialId(data.getAccount().getUser().getUserId())
								.createdAt(new Date()).build()).service(INFLUENCERLOOKALIKE)
						.influencerUserName(data.getAccount().getUser().getUserName()).build());
			}
		});
	}

	private void createSearchInfluencerRequest(InfluencerLookalikeModal influencerLookalikeModal, InfluencersDataRequest request,
			IndustryMaster industryMaster) {
		Filter filter = new Filter();
		Paging paging = new Paging();

		setFilterParamsRequest(filter);
		setFilterObjectsRequest(request, filter, influencerLookalikeModal, industryMaster);

		if(!StringUtils.isEmpty(searchInfluencerProps.getAudienceSource()))
			request.setAudienceSource(searchInfluencerProps.getAudienceSource());

		paging.setLimit(3);
		paging.setSkip(0);

		request.setFilter(filter);
		request.setPaging(paging);
	}

	private void setFilterParamsRequest(Filter filter) {
		if(!StringUtils.isEmpty(searchInfluencerProps.getSponsoredPost()))
			filter.setSponsoredPost(searchInfluencerProps.getSponsoredPost());

		if(!StringUtils.isEmpty(searchInfluencerProps.getIsVerified()))
			filter.setVerified(searchInfluencerProps.getIsVerified());

		if(!StringUtils.isEmpty(searchInfluencerProps.getHidden()))
			filter.setHidden(searchInfluencerProps.getHidden());

		if(!StringUtils.isEmpty(searchInfluencerProps.getHasAudienceData()))
			filter.setHasAudienceData(searchInfluencerProps.getHasAudienceData());

		if(!StringUtils.isEmpty(searchInfluencerProps.getLastPosted()))
			filter.setLastPosted(searchInfluencerProps.getLastPosted());

		if(!StringUtils.isEmpty(searchInfluencerProps.getAccountType()))
			filter.setAccountType(searchInfluencerProps.getAccountType());

		if(!StringUtils.isEmpty(searchInfluencerProps.getAudienceCredibility()))
			filter.setAudienceCredibility(searchInfluencerProps.getAudienceCredibility());

		if(!StringUtils.isEmpty(searchInfluencerProps.getAudienceCredibilityClass()))
			filter.setAudienceCredibilityClass(searchInfluencerProps.getAudienceCredibilityClass());
	}

	private void setFilterObjectsRequest(InfluencersDataRequest request, Filter filter, InfluencerLookalikeModal influencerLookalikeModal,
			IndustryMaster industryMaster) {
		setSortBy(request);
		setWithContactFilter(filter);
		setInfluencerGenderFilter(filter);
		setFollowersFilter(influencerLookalikeModal, filter);
		setAudienceRelevance(influencerLookalikeModal, filter);
		setAudienceIndustryFilter(industryMaster, filter);
	}

	private void setSortBy(InfluencersDataRequest request) {
		if(!StringUtils.isEmpty(searchInfluencerProps.getSortBy())) {
			SortResponse sort = new SortResponse();
			sort.setField(searchInfluencerProps.getSortBy());
			request.setSortResponse(sort);
		}
	}

	private void setWithContactFilter(Filter filter) {
		if(!StringUtils.isEmpty(searchInfluencerProps.getWithContactDefault())
				&& !StringUtils.isEmpty(searchInfluencerProps.getWithContactMust())) {
			WithContact withContact = new WithContact();
			List<WithContact> withContactArray = new LinkedList<WithContact>();
			withContact.setType(searchInfluencerProps.getWithContactDefault());
			withContact.setAction(searchInfluencerProps.getWithContactMust());
			withContactArray.add(withContact);

			filter.setWithContact(withContactArray);
		}
	}

	private void setInfluencerGenderFilter(Filter filter) {
		if (StringUtils.isEmpty(searchInfluencerProps.getInfluencerGender())) {
			CodeWeight gender = new CodeWeight();
			gender.setCode(searchInfluencerProps.getInfluencerGender());
			filter.setInfluencerGender(gender);
		}
	}

	private void setFollowersFilter(InfluencerLookalikeModal influencerLookalikeModal, Filter filter) {
		Range followers = new Range();
		followers.setMinValue(influencerLookalikeModal.getFollowersMin());
		followers.setMaxValue(influencerLookalikeModal.getFollowersMax());
		filter.setFollowers(followers);
	}

	private void setAudienceRelevance(InfluencerLookalikeModal influencerLookalikeModal, Filter filter) {
		if(!StringUtils.isEmpty(influencerLookalikeModal.getComparisonProfile())) {
			ComparisonProfile comparisonProfile = new ComparisonProfile();
			comparisonProfile.setValue(checkInfluencerName(influencerLookalikeModal.getComparisonProfile()));

			filter.setComparisonProfile(comparisonProfile);
		}
	}

	private void setAudienceIndustryFilter(IndustryMaster industryMaster, Filter filter) {
		IndustryAndLocation industry = new IndustryAndLocation();
		List<IndustryAndLocation> industryArray = new LinkedList<IndustryAndLocation>();
		industry.setId(industryMaster.getId());
		industry.setWeight(industryMaster.getWeight());
		industryArray.add(industry);
		filter.setAudienceBrand(industryArray);
	}

	@Override
	public ResponseEntity<Object> getAudienceReportId(GenerateReportModal generateReportModal) {

		int apiValidationResponse = validateAPIRequest(AUDIENCEREPORT, "", "",	"");

		if (apiValidationResponse != 0)
			return apiFailureResponse(apiValidationResponse);

		if (!toolsAPIDao.isAudienceHistoryExist(generateReportModal.getBusinessEmailAddress())) {
			try {
				ReportData reportResult = socialDataProxy.createNewReport(generateReportModal.getInstagramUserName(), 0, 1, true, "instagram");
				if (!Objects.isNull(reportResult) && reportResult.isSuccess()
						&& !Objects.isNull(reportResult.getReportInfo())
						&& !Objects.isNull(reportResult.getReportInfo().getReportId())) {
					toolsAPIDao.saveAudienceHistory(AudienceReportHistory.builder()
							.key(AudienceReportHistoryKey.builder()
									.businessEmailAddress(generateReportModal.getBusinessEmailAddress())
									.influencerDeepSocialId(reportResult.getUserProfile().getUserId())
									.createdAt(new Date()).build()).service(AUDIENCEREPORT)
							.influencerUserName(reportResult.getUserProfile().getFullName().replace(" ", ""))
							.name(generateReportModal.getName()).brandName(generateReportModal.getBrandName())
							.describe(generateReportModal.getDescribe()).noOfInfluencers(generateReportModal.getName()).build());

					return new ResponseEntity<>(reportResult.getReportInfo().getReportId(), HttpStatus.OK);
				} else
					return new ResponseEntity<>("This instagram handle seems to be invalid", HttpStatus.CONFLICT);
			} catch (Exception e) {
				return new ResponseEntity<>("We are facing some technical issue, please try after sometime.", HttpStatus.NOT_FOUND);
			}
		}
		return new ResponseEntity<>("This email id has already been used to download a report", HttpStatus.CONFLICT);
	}

	@Override
	public ResponseEntity<Object> getAudienceReport(DownloadReportModal downloadReportModal, HttpServletRequest servletRequest) {

		int apiValidationResponse = validateAPIRequest(DOWNLOADREPORT, "", "", "");

		if (apiValidationResponse != 0)
			return apiFailureResponse(apiValidationResponse);

		ReportData reportData = null;
		String reportId = StringUtils.isEmpty(downloadReportModal.getReportId()) ? "" : downloadReportModal.getReportId();

		try {
			if (StringUtils.isEmpty(reportId)) {
				if ((reportData = checkInfluencerReport(downloadReportModal.getInfluencerHandle(), downloadReportModal.getPlatform())) != null)
					reportId = reportData.getReportInfo().getReportId();
				else
					return new ResponseEntity<>("This " + downloadReportModal.getPlatform() + " user seems to be invalid", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.info("Failed to get DeepSocial Data for Audience Report");
			return new ResponseEntity<>("We are facing some technical issue, please try after sometime.", HttpStatus.NOT_FOUND);
		}

		downloadReportModal.setFile(socialDataProxy.downloadReport(reportId, PDF_FMT));
		downloadReportModal.setFileName(downloadReportModal.getInfluencerHandle() + "." + PDF_FMT);
		mailService.sendMailForReportDownload(downloadReportModal);

		return new ResponseEntity<>("Requested report will be sent to provided mail id", HttpStatus.OK);
	}
}