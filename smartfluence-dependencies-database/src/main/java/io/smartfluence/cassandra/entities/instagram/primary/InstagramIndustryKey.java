package io.smartfluence.cassandra.entities.instagram.primary;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InstagramIndustryKey {
	

	@PrimaryKeyColumn(name = "influencer_handle", type = PrimaryKeyType.PARTITIONED)
	private String influencerHandle;

	@PrimaryKeyColumn(name = "industry", type = PrimaryKeyType.CLUSTERED)
	private String industry;
}
