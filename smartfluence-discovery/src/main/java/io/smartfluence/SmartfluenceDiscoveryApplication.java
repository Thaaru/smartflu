package io.smartfluence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;

import io.smartfluence.beans.WebContainer;

@EnableEurekaServer
@SpringBootApplication
public class SmartfluenceDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartfluenceDiscoveryApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(prefix="smartfluence",name="use-ssl")
	public WebContainer webContainer() {
		return new WebContainer();
	}
}