package io.smartfluence.brand.management.entityv1;

import io.smartfluence.constants.BidStatus;
import io.smartfluence.constants.UserStatus;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class BrandInfluObj {

    private String campaignId;

    private String influencerId;


    private String campaignName;


    private String brandEmail;

    private String brandIndustry;


    private String brandInstagramHandle;

    private String brandName;



    private String influencerMail;


    private String influencerFirstName;


    private String influencerIndustry;


    private String influencerHandle;

    private String influencerLastName;


    private UserStatus influencerStatus;


    private String postDuration;


    private String paymentType;


    private String postType;

    private Date modifiedAt;

    private UUID creditId;

    private String transYrMonth;

    private Date createdAt;

    private BidStatus bidStatus;


}
