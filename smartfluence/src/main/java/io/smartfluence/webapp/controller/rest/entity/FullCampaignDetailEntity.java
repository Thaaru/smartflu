package io.smartfluence.webapp.controller.rest.entity;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;


@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FullCampaignDetailEntity {

    private String campaignId;
    private String brandId;
    private String brandDescription;
    private String campaignName;
    private String campaignDescription;

    private String campaignStatus;
    private String deletedStatus;

    private String offerType;

    private String isNew;
    private String restrictions;
    private String currency;
    private List<CurrencyMaster> currencyMasters;
    private List<PostMaster> postMasters;
    private List<CommissionAffiliateTypeMaster> commissionAffiliateTypeMasters;
    private List<PlatformMaster> platformMasters;
    //    private Boolean isPromote = false;
    private LocalDateTime createdAt;
    private LocalDateTime deletedAt;
    private LocalDateTime modifiedAt;
    private LocalDateTime endDate;
    private String hashTags;
    private int maxProductCount;
    private double paymentOffer;
    private double commissionAffiliate;
    private String commissionAffiliateType;
    private String commissionAffiliateDetails;
    private String commissionAffiliateTypeName;
    private String commissionAffiliateCustomName;
    private Boolean isDefaultTermsConditions;
    private String platformName;
    private List<Products> productList;
    private List<CampaignTask> campaignTaskList;
    private long termId;
    private List<TermsAndConditions> termsAndConditions;


}