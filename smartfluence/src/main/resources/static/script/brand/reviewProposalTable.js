var rowsSelected = [];
var campaignId;
var campaignWFRPDT;
var campaignStatus;

$(document).ready(function () {
    // console.log("status for campaign review proposal : ", campaignStatusFromCampaignWF);

    $.fn.dataTable.ext.errMode = 'none';
    campaignId = window.location.href.split("/")[5];
    init_reviewProposalTable(campaignId);

    $(".reviewProposalAction").click(function () {
        // console.log($(this))
        alert("Handler for .change() called.");
    });

    campaignWFRPDT.on('user-select', function (e, dt, type, cell, originalEvent) {
        //if ( originalEvent.target.nodeName.toLowerCase() === 'img' ) {
        e.preventDefault();
        //}
    });


});

var influencersList_rp = [];
var currencyMaster = [];
var affiliateMaster = [];
var platformMasters = [];
var postTypeMasters = [];

//table_campaign_review
function init_reviewProposalTable(campaignId) {
    campaignWFRPDT = $('#table_campaign_review').DataTable({
        dom: "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-7 pl-0'><'col-sm-12 col-md-3'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
        ajax: {
            url: `/brand/view-campaign-v1/view-influencers?id=${campaignId}&proposalStatus=INFLUENCER_ACCEPTED,INVITE_SENT,BRAND_REJECTED`,
            dataSrc: function (response) {
                if (response?.code == 600) {
                    // console.log("influencerlist RP : ", response.data)
                    currencyMaster = response.data.currencyMaster;
                    affiliateMaster = response.data.affiliateMaster;
                    influencersList_rp = response.data.influencersList;
                    campaignStatus = influencersList_rp.find(x => x.campaignStatus)
                    // console.log("campaign status from review proposal : ", campaignStatus);
                    if (influencersList_rp?.length > 0) {
                        platformMasters = influencersList_rp[0].platformMasters;
                        postTypeMasters = influencersList_rp[0].postTypeMasters;
                        return influencersList_rp;
                    }
                    return [];
                }
                return [];
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                targets: 0,
                searchable: false,
                orderable: false,
                className: "text-center",
                render: function (data, type, full, meta) {
                    //meta.settings.aoData[0]._aData.influencerId
                    return '<input disabled type="checkbox" class="inviteCheckbox">';
                }
            },
            {
                targets: 1,
                width: "10%",
                className: "text-left",
                data: "influencerHandle",
                render: function (data, type, row, meta) {
                    return `<a style=" color: #2f008d" href="/brand/view-influencer/${row.influencerId}?isValid=true&platform=${row.platform}"
                   target="_blank">${data}</a>`
                }
            },
            {
                targets: 2,
                width: "10%",
                data: "offers",
                className: "text-left",
                render: function (data, type, row, meta) {
                    var payment = data.find(x => x.offerType === 'PAYMENT');
                    var commission = data.find(x => x.offerType === 'COMMISSION');
                    var product = data.find(x => x.offerType === 'PRODUCT');
                    // payment = { paymentOffer: 10.05 }
                    // commission = { commissionValue: 13.05 }
                    var result = '';
                    if (payment && typeof payment?.paymentOffer === "number") {
                        result += `<span>$ ${payment?.paymentOffer} Cash</span><br>`;
                    }
                    if (product) {
                        result += `<span>Product Only</span><br>`;
                    }
                    if (commission && typeof commission?.commissionValue === "number") {
                        var affilateTypeValue = affiliateMaster.find(x => x.caId === commission?.commissionAffiliateTypeId);
                        var commissionTypeValue = currencyMaster.find(x => x.currencyId === commission?.commissionValueTypeId);
                       
                        if (affilateTypeValue.caValue == "Custom") {
                            result += `<span>${commission?.commissionValue}${commissionTypeValue.currencyValue} Per ${commission?.commissionAffiliateCustomName}</span><br>`;
                        } else {
                            result += `<span>${commission?.commissionValue}${commissionTypeValue.currencyValue} Per ${affilateTypeValue.caValue}</span><br>`;
                        }
                    }

                    return result;
                }
            },
            {
                targets: 3,
                width: "10%",
                className: "text-left",
                data: "products",
                render: function (data, type, row, meta) {

                    // var productLink = data.map(x => x.productLink);
                    var result = '';
                    if (campaignStatusFromCampaignWF == "ENDED" || row.proposalStatus == "INVITE_SENT") {
                        result += `<a style=" color: #2f008d" class="disabled"  style="color:blue; pointer-events:none;">View Selected<br>Products</a>`;
                    } else {
                        result += `<a style=" color: #2f008d" onClick=viewSelectedProductsRp("${row.influencerId}") style="color:blue;pointer-events:none;">View Selected<br>Products</a>`;
                    }
                    return result
                }
            },
            {
                targets: 4,
                width: "10%",
                className: "text-left",
                data: "tasks",
                render: function (data, type, row, meta) {
                   
                    if (typeof data == "object" && data?.length > 0) {
                        var platformId = data[0].platformId;
                        var postTypeId = data[0].postTypeId;
                        var postCustomValue = data[0].postCustomName
        
                        var postName = postTypeMasters.find(x => x.platformId == platformId && x.postTypeId == postTypeId);

                        var html = ``;
                        // if (obj) {
                        var platform = platformMasters.find(x => x.platformId == platformId);
                        var post = postTypeMasters.find(x => x.postTypeId == postTypeId);
                  
                        var logo = "";
                        if (platform && platform.platformName && post && post.postTypeName) {
                            if (platform.platformName == "Instagram") {
                                logo = `<i class="fab fa-instagram"></i> `
                            } if (platform.platformName == "Youtube") {
                                
                                logo = ` <i class="fab fa-youtube"></i> `
                            } if (platform.platformName == "TikTok") {
                             
                                logo = ` <img style="width: 18px; height: 18px; margin-bottom: 5px;"; src="/static/svgs/brands/tiktok.svg"></img>`
                            }
                            if(post.postTypeName == "Custom"){
                            html += `<a style=" color: #2f008d; font-size: 1.2rem;">${logo}</a>    <span style="padding-left:3px"> ${postCustomValue}</span>`;
                            }else{
                                html += `<a style=" color: #2f008d; font-size: 1.2rem;">${logo}</a>    <span style="padding-left:3px"> ${post.postTypeName}</span>`;
                            }
                            return html;
                        }
                    } else {
                        return '-';
                    }
                }
            },
            {
                targets: 5,
                className: "text-left",
                width: "12%",
                data: "address",
                render: function (data, type, row, meta) {
                    var address = data.find(x => x.addressType === 'SHIPPING');

                    var result = '';
                    if (address) {
                        if (address.addressLine1) {
                            result += `<span> ${address?.addressLine1}</span><br>`;
                        } if (address.addressLine2) {
                            result += `<span>${address?.addressLine2}</span><br>`
                        } if (address.cityName) {
                            result += `<span>${address?.cityName}</span>`
                        }
                    } else {
                        result = "-"
                    }
                    return result;
                }
            },
            {
                targets: 6,
                searchable: false,
                width: "10%",
                data: "proposalStatus",
                className: "text-left",
                render: function (data, type, row, meta) {
                    var obj = {
                        INFLUENCER_ACCEPTED: "Proposal Accepted",
                        INFLUENCER_REJECTED: "Proposal Rejected",
                        BRAND_REJECTED: "Rejected by Brand",
                        INVITE_SENT: "Awaiting Proposal"
                    }
                    // console.log("data of staus RP : ", obj[data])
                    var status = obj[data];
                    return `<a style="color: #2f008d";>${status}</a>`
                }

            },
            {
                targets: 7,
                width: "0%",
                className: "text-center",
                render: function (data, type, row, meta) {
                    var tabIdRp = ''

                    //pproduct
                    var payment = row.offers.find(x => x.offerType === 'PAYMENT');
                    // var commission = row.offers.find(x => x.offerType === 'COMMISSION');
                    var product = row.offers.find(x => x.offerType === 'PRODUCT');

                    if (product && !payment) {
                        tabIdRp = '#fulfillProducts-tab'
                    } else {
                        tabIdRp = '#makePayments-tab'
                    }

                    
                    var influencerId = row.influencerId;
                    var campaignId = row.campaignId;
                    var proposalStatus = row.proposalStatus;
                    if (proposalStatus == "INFLUENCER_ACCEPTED" || "INVITE_SENT") {
                        // console.log("inside action : RP");
                        if (campaignStatusFromCampaignWF == "ENDED" || proposalStatus == "INVITE_SENT") {
                            return `
                            <div class="dropdown">
                            <span class="dot-icon disabled"><i class="fa fa-ellipsis-v"></i></span>
                        </div>
                        `;
                        } else if (proposalStatus == "BRAND_REJECTED") {
                            return `
                            <div class="dropdown reviewProposalAction" id=${meta.settings.aoData[0]._aData.influencerId}>
                                <span class="dot-icon"><i class="fa fa-ellipsis-v"></i></span>
                                <ul class="dropdown-content">
                                <li name="actionDisabledWF" id="re-accept" onclick="inviteApproval('accept','${influencerId}','${campaignId}','${tabIdRp}')">Re-accept</li>
                                </ul>
                            </div>
                        `;
                        } else {
                            return ` <div class="dropdown reviewProposalAction" id=${meta.settings.aoData[0]._aData.influencerId}>
         <span class="dot-icon"><i class="fa fa-ellipsis-v"></i></span>
         <ul class="dropdown-content">
             <li  name="actionDisabledWF" id="accept" onclick="inviteApproval('accept','${influencerId}','${campaignId}','${tabIdRp}')">Accept</li>
             <li  name="actionDisabledWF" id="reject" onclick="inviteApproval('reject','${influencerId}','${campaignId}','${tabIdRp}')">Reject</li>
         </ul>
     </div>
 `;
                        }
                    }

                }
            },
            {
                className: 'control',
                targets: [0, 3, 4, 7],
                orderable: false
            }
        ],
        select: {
            style: 'multi'
        },
        order: [
            [1, 'asc']
        ],
        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            var rowId = data.DT_RowId;
            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rowsSelected) !== -1) {
                $(row).find('input[type="checkbox"]').prop(
                    'checked', true);
                $(row).addClass('selected');
            }
        }
    });


    $('#table_campaign_review tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');
        // Get row data
        var data = campaignWFRPDT.row($row).data();
        // Get row ID
        var rowId = data.DT_RowId;
        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rowsSelected);
        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rowsSelected.push(rowId);
            // Otherwise, if checkbox is not checked and row ID is in list of
            // selected
            // row IDs
        } else if (!this.checked && index !== -1) {
            rowsSelected.splice(index, 1);
        }
        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(campaignWFRPDT);
        // Prevent click event from propagating to parent
        e.stopPropagation();
    });


    function updateDataTableSelectAllCtrl(table) {
        var $table = table.table().node();
        var $chkbox_all = $('tbody input[type="checkbox"]', $table);
        var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
        var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

        // If none of the checkboxes are checked
        if ($chkbox_checked.length === 0) {
            chkbox_select_all.checked = false;
            if ('indeterminate' in chkbox_select_all) {
                chkbox_select_all.indeterminate = false;
            }
            // If all of the checkboxes are checked
        } else if ($chkbox_checked.length === $chkbox_all.length) {
            chkbox_select_all.checked = true;
            if ('indeterminate' in chkbox_select_all) {
                chkbox_select_all.indeterminate = false;
            }
            // If some of the checkboxes are checked
        } else {
            chkbox_select_all.checked = true;
            if ('indeterminate' in chkbox_select_all) {
                chkbox_select_all.indeterminate = true;
            }
        }
        if (rowsSelected.length != 0) {
            $("#bulkMail").removeClass("disabled");
            $('#deleteCampaignBtn').removeClass("d-none");
            $('#copyInfluencers').parent().removeClass('disabled');
            $('#copyInfluencers').css("color", "rgb(47, 0, 141)");
            $('#copyInfluencers').css("cursor", "pointer");
        } else {
            $('#bulkMail').addClass('disabled');
            $('#deleteCampaignBtn').addClass("d-none");
            $('#copyInfluencers').parent().addClass('disabled');
            $('#copyInfluencers').css("color", "rgb(227, 226, 226)");
            $('#copyInfluencers').css("cursor", "");
        }
    }
}

function viewSelectedProductsRp(selectedInfluencer) {
    let selectedProducts = influencersList_rp.find(x => x.influencerId == selectedInfluencer)?.products;
    var productHtml = ""
    $("#productPopup").empty()
    if (!selectedProducts || selectedProducts?.length == 0) {
        $("#productPopup").html("No products found")
    } else {
        selectedProducts.map((x, index) => {
            // console.log("product details : ", x.productName, " : ", x.productLink);
            productHtml += `<div style="display:flex;">
        <input type="checkbox" checked  style="visibility:visible;left: 13px;width:18px;height:18px"/>
        <span id="productNameModal_${index}"  style="padding-left:24px; font-weight:400; line-height:1; transform: translateY(-2px);">
        <span style='font-size:18px; text-transform:capitalize;'>${x.productName}</span>
        <br/><br><span><a style='white-space: pre-wrap;word-break: break-all;' href="${x.productLink}"> <u>${x.productLink}</u></a></span></span></div>`
            $("#productPopup").html(productHtml)
            // $(`#productNameModal`).text();
            // $(`#productLinkModal`).text();
            // $(`#productLinkModal`).attr("href", x.productLink);
        });
    }
    $("#viewProducts").modal("show");


}

//accept/reject
function inviteApproval(status, influencerId, campaignid, tabId) {

    // console.log(" status of approval : ", status, " : ", campaignid, " : ", influencerId)
    var requestReviewPropsalData = {
        campaignId: campaignid,
        influencerId: influencerId,
        status: status,
        workflowStatus: "BRAND_REVIEW"
    }
    // console.log("request data : ", typeof JSON.stringify(requestReviewPropsalData))
    // console.log("request data : ", requestReviewPropsalData)
    var code = "600"

    $.ajax({
        url: "/brand/view-campaign-v1/view-influencers/brandapproval",
        type: "PUT",
        data: JSON.stringify(requestReviewPropsalData),
        dataType: "json",
        contentType: "application/json",
    }).done(function (data) {
        // data = JSON.parse(data);
        // console.log("+++++++++++++++++++ data response : ", data)
        if (data.code == "600" && status == "reject") {
            campaignWFRPDT.ajax.reload(null, false);
            toast("Proposal Rejected")
        } if (data.code == "600" && status == "accept") {
            campaignWFRPDT.ajax.reload(null, false);
            campaignWFMPDT.ajax.reload(null, false);
            campaignWFFPDT.ajax.reload(null, false);
            var someTabTriggerEl = document.querySelector(tabId);
            var tab = new bootstrap.Tab(someTabTriggerEl);
            tab.show();
            toast("Proposal Accepted");


        } else {
            toast(data.errors.message, "error");
        }

        //window.location.href = "/brand/view-campaign-v1/"+msg.campaignId;
        // $("#campaignForm").trigger("reset");
    }).fail(function (jqXHR, textStatus) {
        toast("Request Failed " + textStatus, "error")
    });
}

