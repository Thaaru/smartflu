package io.smartfluence.constants;

public enum UserStatus {
	REGISTERED, PENDING, INACTIVE, APPROVED, DECLINED, PREMIUM, FREE
}