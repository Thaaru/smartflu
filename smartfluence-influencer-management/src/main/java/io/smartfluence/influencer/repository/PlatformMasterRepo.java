package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.influencer.entities.PlatformMaster;

@Repository
public interface PlatformMasterRepo extends JpaRepository<PlatformMaster,String>{
    PlatformMaster findByPlatformId(long platformId);
}
