package io.smartfluence.modal.social.customlist;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddUsersToList implements Serializable {

	private static final long serialVersionUID = -3019727803715983652L;

	@JsonProperty("user_ids")
	private List<String> userIds;
}