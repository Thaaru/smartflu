package io.smartfluence.modal.zerobounce.response;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ZeroBounceResponse {

	private String emailAddress;

	private String status;

	private String subStatus;

	private String account;

	private Boolean freeEmail;

	private String smtpProvider;

	private Date processedAt;
}