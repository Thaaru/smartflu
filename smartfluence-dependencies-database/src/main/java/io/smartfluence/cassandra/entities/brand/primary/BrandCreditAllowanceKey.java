package io.smartfluence.cassandra.entities.brand.primary;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import io.smartfluence.constants.CreditType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class BrandCreditAllowanceKey implements Serializable {

	private static final long serialVersionUID = 4134212713529915946L;

	public BrandCreditAllowanceKey(BrandCreditAllowanceKey brandInfluencerBidAllowanceMapKey) {
		this.brandId = brandInfluencerBidAllowanceMapKey.getBrandId();
		this.influencerId = brandInfluencerBidAllowanceMapKey.getInfluencerId();
		this.creditType = brandInfluencerBidAllowanceMapKey.getCreditType();
		this.creditId = brandInfluencerBidAllowanceMapKey.getCreditId();
	}

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.PARTITIONED)
	private UUID brandId;

	@PrimaryKeyColumn(name = "influencer_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID influencerId;

	@Enumerated(EnumType.STRING)
	@PrimaryKeyColumn(name = "credit_type", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private CreditType creditType;

	@PrimaryKeyColumn(name = "credit_id", type = PrimaryKeyType.CLUSTERED, ordinal = 3)
	private UUID creditId;
}