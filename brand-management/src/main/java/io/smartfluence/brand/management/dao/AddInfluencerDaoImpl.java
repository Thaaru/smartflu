package io.smartfluence.brand.management.dao;

import feign.FeignException;
import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.entity.*;
import io.smartfluence.brand.management.entityv1.CampaignOffersV1;
import io.smartfluence.brand.management.entityv1.OfferTypeV1;
import io.smartfluence.brand.management.constants.ProposalStatus;
import io.smartfluence.brand.management.entityv1.*;
import io.smartfluence.brand.management.entity.CampaignInfluencerKey;
import io.smartfluence.brand.management.mailbean.MailService;
import io.smartfluence.brand.management.repository.BrandCampaignDetailsV1Repo;
import io.smartfluence.brand.management.repository.CampaignInfOfferV1Repo;
import io.smartfluence.brand.management.repository.CampaignInfluencerRepo;
import io.smartfluence.brand.management.repositoryv1.BrandCampaignDetailsRepositoryV1;
import io.smartfluence.brand.management.repositoryv1.CampaignOffersRepositoryV1;
import io.smartfluence.brand.management.repositoryv1.CampaignProductsRepositoryV1;
import io.smartfluence.brand.management.service.ReportsService;
import io.smartfluence.brand.management.servicev1.CampaignEntityServiceV1;
import io.smartfluence.brand.management.servicev1.MasterServiceV1;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.TemporaryInfluencerContent;
import io.smartfluence.cassandra.entities.brand.*;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignDetailsKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignInfluencerHistoryKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandSearchHistoryKey;
import io.smartfluence.cassandra.entities.instagram.*;
import io.smartfluence.cassandra.entities.primary.ReportSearchHistoryKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.entities.tiktok.TikTokContent;
import io.smartfluence.cassandra.entities.tiktok.TikTokCountryDemographic;
import io.smartfluence.cassandra.entities.tiktok.TikTokDemographics;
import io.smartfluence.cassandra.entities.youtube.YouTubeContent;
import io.smartfluence.cassandra.entities.youtube.YouTubeCountryDemographic;
import io.smartfluence.cassandra.entities.youtube.YouTubeDemographics;
import io.smartfluence.cassandra.repository.brand.BrandCampaignDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignInfluencerHistoryRepo;
import io.smartfluence.constants.CreditType;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.constants.ViewType;
import io.smartfluence.modal.brand.campaignv1.CampaignOffersEntityV1;
import io.smartfluence.modal.brand.dashboard.DashBoardResponseModal;
import io.smartfluence.modal.brand.dashboard.MailTemplates;
import io.smartfluence.modal.brand.dashboard.UploadInfluencersResponseModel;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.social.search.response.InfluencersDataResponse;
import io.smartfluence.modal.validation.brand.dashboard.UploadInfluencersRequestModel;
import io.smartfluence.modal.validation.brand.explore.CampaignDetail;
import io.smartfluence.modal.validation.brand.explore.CampaignInfluencerReqEntity;
import io.smartfluence.modal.validation.brand.explore.GetInfluencersRequestModel;
import io.smartfluence.modal.validation.brand.explore.SubmitBidModel;
import io.smartfluence.mysql.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import lombok.extern.log4j.Log4j2;

import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class AddInfluencerDaoImpl extends ExploreInfluencerDaoImpl {
    private static final int MAX_INFLUENCER_PER_CAMPAIGN = 30;
    private static final String BRAND_NAME = "<Brand Name>";

    private static final String ESTIMATED_PRICING = "<Estimated Pricing>";

    private static final String INFLUENCER_HANDLE = "<Influencer Handle>";

    private static final int MAX_INFLUENCER_PER_UPLOAD = 100;

    private static final String UPLOAD_SUCCESS = "SUCCESS";

    private static final String PLATFORM_ERROR = "PLATFORM_ERROR";

    private static final String UPLOAD_ERROR = "ERROR";

    private static final String DUPLICATE = "DUPLICATE";

    private static final String NAME = "Influencer Username";

    private static final String PLATFORM = "Platform";

    private static final String DEFAULT_NAME = "smartfluence";
    @Autowired
    private ReportsService reportsService;
    @Autowired
    private CampaignInfluencerRepo campaignInfluencerRepo;
    @Autowired
    private BrandCampaignDetailsV1Repo brandCampaignDetailsV1Repo;
    @Autowired
    private BrandCampaignDetailsRepo brandCampaignDetailsRepo;
    @Autowired
    private BrandCampaignInfluencerHistoryRepo brandCampaignInfluencerHistoryRepo;
    @Autowired
    private CampaignInfOfferV1Repo campaignInfOfferV1Repo;

    @Autowired
    private DashboardDao dashboardDao;
    @Autowired
    private CampaignProductsRepositoryV1 campaignProductsRepositoryV1;

    @Autowired
    private CampaignOffersRepositoryV1 campaignOffersRepositoryV1;

    @Autowired
    private BrandCampaignDetailsRepositoryV1 brandCampaignDetailsRepositoryV1;

    @Autowired
    private CampaignEntityServiceV1 campaignEntityServiceV1;

    @Autowired
    private MasterServiceV1 masterServiceV1;

    @Autowired
    private MailService mailService;

    @Override
    public Optional<UserAccounts> getUserAccounts(String brandId) {
        return super.getUserAccounts(brandId);
    }

    @Override
    public Optional<InstagramDemographics> getInstagramDemographics(String influencerHandle) {
        return super.getInstagramDemographics(influencerHandle);
    }

    @Override
    public List<InstagramCountryDemographic> getAllInstagramCountryByHandle(String userName) {
        return super.getAllInstagramCountryByHandle(userName);
    }

    @Override
    public List<InstagramIndustryDemographic> getAllInstagramIndustryByHandle(String userName) {
        return super.getAllInstagramIndustryByHandle(userName);
    }

    @Override
    public List<InstagramStateDemographic> getAllInstagramStateByHandle(String userName) {
        return super.getAllInstagramStateByHandle(userName);
    }

    @Override
    public Optional<YouTubeDemographics> getYouTubeDemographics(String influencerHandle) {
        return super.getYouTubeDemographics(influencerHandle);
    }

    @Override
    public List<YouTubeCountryDemographic> getAllYoutubeCountryByHandle(String userName) {
        return super.getAllYoutubeCountryByHandle(userName);
    }

    @Override
    public Optional<TikTokDemographics> getTikTokDemographics(String influencerHandle) {
        return super.getTikTokDemographics(influencerHandle);
    }

    @Override
    public List<TikTokCountryDemographic> getAllTiktokCountryByHandle(String userName) {
        return super.getAllTiktokCountryByHandle(userName);
    }

    @Override
    public Optional<CampaignInfluencerMapping> getCampaignInfluencerMapping(String campaignId, String influencerId) {
        return super.getCampaignInfluencerMapping(campaignId, influencerId);
    }

    @Override
    public Optional<SocialIdMaster> getSocialIdMasterById(String userId) {
        return super.getSocialIdMasterById(userId);
    }

    @Override
    public Optional<SocialIdMaster> getSocialIdMasterByHandleAndPlatform(String influencerHandle, Platforms platform) {
        return super.getSocialIdMasterByHandleAndPlatform(influencerHandle, platform);
    }

    @Override
    public Optional<SocialIdMaster> getSocialIdMasterByInfluencerId(String influencerId) {
        return super.getSocialIdMasterByInfluencerId(influencerId);
    }

    @Override
    public void saveTemporaryInfluencerContent(TemporaryInfluencerContent temporaryInfluencerContent) {
        super.saveTemporaryInfluencerContent(temporaryInfluencerContent);
    }

    @Override
    public Optional<TemporaryInfluencerContent> getTemporaryInfluencerContent(String influencerId, boolean isProfilePic, int contentRank) {
        return super.getTemporaryInfluencerContent(influencerId, isProfilePic, contentRank);
    }

    @Override
    public void saveBrandCreditHistory(BrandCreditHistory brandCreditHistory) {
        super.saveBrandCreditHistory(brandCreditHistory);
    }

    @Override
    public void saveBrandCredits(BrandCredits brandCredits) {
        super.saveBrandCredits(brandCredits);
    }

    @Override
    public void saveBrandCreditAllowance(BrandCreditAllowance brandCreditAllowance) {
        super.saveBrandCreditAllowance(brandCreditAllowance);
    }

    @Override
    public Optional<BrandCredits> getBrandCredits(UUID brandId) {
        return super.getBrandCredits(brandId);
    }

    @Override
    public Optional<BrandDetails> getBrandDetails(UserDetailsKey userDetailsKey) {
        return super.getBrandDetails(userDetailsKey);
    }

    @Override
    public Optional<BrandCreditAllowance> getBrandCreditAllowance(UUID brandId, UUID influencerId, CreditType creditType) {
        return super.getBrandCreditAllowance(brandId, influencerId, creditType);
    }

    @Override
    public void saveInstagramDemographics(InstagramDemographics instagramDemographics) {
        super.saveInstagramDemographics(instagramDemographics);
    }

    @Override
    public void saveInstagramContents(List<InstagramContent> instagramContent) {
        super.saveInstagramContents(instagramContent);
    }

    @Override
    public void saveYoutubeDemographics(YouTubeDemographics youtubeDemographics) {
        super.saveYoutubeDemographics(youtubeDemographics);
    }

    @Override
    public void saveYoutubeContents(List<YouTubeContent> youtubeContent) {
        super.saveYoutubeContents(youtubeContent);
    }

    @Override
    public void saveTiktokDemographics(TikTokDemographics tiktokDemographics) {
        super.saveTiktokDemographics(tiktokDemographics);
    }

    @Override
    public void saveTiktokContents(List<TikTokContent> tiktokContent) {
        super.saveTiktokContents(tiktokContent);
    }

    @Override
    public ResponseEntity<Object> submitBid(SubmitBidModel submitBidModel) {
        return super.submitBid(submitBidModel);
    }

    @Override
    public ResponseEntity<Object> addToCampaign(CampaignDetail addToCampaign) {
        return super.addToCampaign(addToCampaign);
    }

    private void saveReportHistory(SocialIdMaster socialIdMaster, InfluencerDemographicsMapper influencerDemographics, ViewType viewType, String platform) {
        Optional<UserAccounts> userAccounts = getUserAccounts(ResourceServer.getUserId().toString());
        reportsService.saveReportHistory(ReportSearchHistory.builder()
                .key(ReportSearchHistoryKey.builder().brandId(UUID.fromString(userAccounts.get().getUserId())).isNew(false).createdAt(new Date()).build())
                .influencerUsername(influencerDemographics.getInfluencerHandle()).brandName(userAccounts.get().getBrandName())
                .influencerDeepSocialId(StringUtils.isEmpty(socialIdMaster.getInfluencerId()) ? "" : socialIdMaster.getInfluencerId())
                .reportId(StringUtils.isEmpty(socialIdMaster.getInfluencerReportId()) ? "" : socialIdMaster.getInfluencerReportId())
                .viewType(viewType).platform(reportsService.getPlatform(platform)).build());
    }

    public Optional<CampaignInfluencerV1Entity> getCampaignInfluencerMap(String campaignId, String influencerId) {
        return campaignInfluencerRepo
                .findByCampaignInfluencerId(new CampaignInfluencerKey(campaignId, influencerId));
    }


    public ResponseEntity<Object> addInfluencerToNewCampaign(List<CampaignInfluencerReqEntity> campaignInfluencerReqEntity) {
        Logger.getLogger("addInfluencerToNewCampaign").log(Level.FINE, campaignInfluencerReqEntity.toString());

        try {
            UUID brandId = ResourceServer.getUserId();
            Optional<BrandDetails> brandDetails = getBrandDetails(new UserDetailsKey(UserStatus.APPROVED, brandId));
            if (brandDetails.isPresent()) {
                if (!campaignInfluencerReqEntity.isEmpty()) {
                    if (Objects.nonNull(campaignInfluencerReqEntity.get(0).getCampaignId())) {
                        Optional<BrandCampaignDetailsV1Entity> brandCampaignDetailsV1Entity = brandCampaignDetailsV1Repo
                                .findByCampaignIdAndBrandId(campaignInfluencerReqEntity.get(0).getCampaignId().toString(), brandId.toString());
                        List<CampaignTasksV1> campaignTasksV1 = campaignEntityServiceV1.getTasksByCampaignId(brandCampaignDetailsV1Entity.get().getCampaignId());
                        PlatformMasterV1 platformName = masterServiceV1.getPlatformById(campaignTasksV1.get(0).getPlatformId());

                        List<CampaignOffersV1> campaignOfferV1Entity = campaignOffersRepositoryV1
                                .findByCampaignOfferId_CampaignId(campaignInfluencerReqEntity.get(0).getCampaignId().toString());
                        int influencerCount = campaignInfluencerRepo.countByCampaignInfluencerId_CampaignId(campaignInfluencerReqEntity.get(0).getCampaignId().toString());
                        if (ResourceServer.isEnterpriseBrand() || ((influencerCount + campaignInfluencerReqEntity.size()) < MAX_INFLUENCER_PER_CAMPAIGN)) {
                            for (int i = 0; i < campaignInfluencerReqEntity.size(); i++) {
                                try {
                                    UUID userId;
                                    SocialIdMaster socialIdMaster = null;
                                    if (campaignInfluencerReqEntity.get(i).getIsValid()) {
                                        userId = UUID.fromString(campaignInfluencerReqEntity.get(i).getInfluencerId());
                                        if (getSocialIdMasterById(userId.toString()).isPresent()) {
                                            socialIdMaster = getSocialIdMasterById(userId.toString()).get();
                                        }
                                    } else {
                                        if (getSocialIdMasterByInfluencerId(campaignInfluencerReqEntity.get(i).getInfluencerId()).isPresent()) {
                                            socialIdMaster = getSocialIdMasterByInfluencerId(campaignInfluencerReqEntity.get(i).getInfluencerId()).get();
                                        }
                                        if (Objects.nonNull(socialIdMaster)) {
                                            userId = UUID.fromString(socialIdMaster.getUserId());
                                        } else {
                                            userId = reportsService.saveNewInfluencer(campaignInfluencerReqEntity.get(i).getInfluencerId(), campaignInfluencerReqEntity.get(i).getPlatform(), ViewType.CAMPAIGN);
                                            if (getSocialIdMasterById(userId.toString()).isPresent()) {
                                                socialIdMaster = getSocialIdMasterById(userId.toString()).get();
                                            }
                                        }
                                    }

                                    if (Objects.nonNull(socialIdMaster)) {
                                        String influencerUserId = socialIdMaster.getUserId();
                                        InfluencerDemographicsMapper influencerDemographics = reportsService.getInfluencerDemographics(socialIdMaster,
                                                campaignInfluencerReqEntity.get(i).getInfluencerId(), ViewType.CAMPAIGN, true);
                                        if (influencerDemographics.isSaveReportHistory()) {
                                            saveReportHistory(socialIdMaster, influencerDemographics, ViewType.CAMPAIGN, campaignInfluencerReqEntity.get(i).getPlatform());
                                        }
                                        if (campaignInfluencerReqEntity.get(i).getCampaignId() != null) {
                                            if (brandCampaignDetailsV1Entity.isPresent()) {
                                                Optional<CampaignInfluencerV1Entity> campaignInfluencerV1Entity = getCampaignInfluencerMap(
                                                        campaignInfluencerReqEntity.get(i).getCampaignId().toString(), userId.toString());
                                                if (!campaignInfluencerV1Entity.isPresent()) {
                                                    addInfluencerToCampaign(brandDetails, influencerUserId, brandCampaignDetailsV1Entity.get(), campaignInfluencerReqEntity.get(i),
                                                            influencerDemographics, campaignOfferV1Entity, platformName);
                                                } else {
                                                    Logger.getLogger("Add to influencer")
                                                            .log(Level.SEVERE, "campaign influencer data  already exists for campaignId="
                                                                    + campaignInfluencerReqEntity.get(i).getCampaignId() + " influencerId=" + campaignInfluencerReqEntity.get(i).getInfluencerId()
                                                            +" influencerHandle="+campaignInfluencerReqEntity.get(i).getInfluencerHandle());
                                                    continue;
                                                }

                                            }
                                        }
                                    }
                                }catch (Exception exception){
                                Logger.getLogger("Add to influencer").log(Level.SEVERE,"feign exception in social iq data for influencerid="
                                        +campaignInfluencerReqEntity.get(i).getInfluencerId()+" campaignId="+campaignInfluencerReqEntity.get(i).getCampaignId().toString()+"exception="
                                        +exception.getMessage());
                                Logger.getLogger("Add to Influencer").log(Level.SEVERE,exception.getMessage());
                                exception.getStackTrace();
                                }
                            }
                            return ResponseEntity.status(HttpStatus.OK).body(campaignInfluencerReqEntity);
                        } else {
                            return ResponseEntity.status(HttpStatus.CONFLICT).body("Not more than 30 Influencers can be added to a Campaign! Contact us for upgrading to the Enterprise plan.");
                        }
                    } else {
                        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Campaign details not found");
                    }
                } else {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Campaign and Influencer details not found");
                }

            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brand Details not found");
        } catch (Exception e) {
            e.printStackTrace();
            mailService.sendErrorMailDetail(e);
            Logger.getLogger("Add to influencer").log(Level.SEVERE, e.getMessage());
            return ResponseEntity.badRequest().build();
        }
    }

    private ResponseEntity<Object> addInfluencerToCampaign(Optional<BrandDetails> brandDetails, String influencerUserId, BrandCampaignDetailsV1Entity brandCampaignDetailsV1Entity,
                                                           CampaignInfluencerReqEntity campaignInfluencerReqEntity, InfluencerDemographicsMapper influencerDemographics, List<CampaignOffersV1> campaignOfferV1Entity, PlatformMasterV1 platformName) {
        Logger.getLogger("addInfluencerToCampaign").log(Level.FINE, "Add influencer to campaign method called");
        processInfluencerToCampaign(brandDetails, influencerUserId, brandCampaignDetailsV1Entity, campaignInfluencerReqEntity, influencerDemographics, campaignOfferV1Entity, platformName);
        return ResponseEntity.ok().build();
    }

    public void processInfluencerToCampaign(Optional<BrandDetails> brandDetails, String influencerUserId, BrandCampaignDetailsV1Entity brandCampaignDetailsV1Entity,
                                            CampaignInfluencerReqEntity campaignInfluencerReqEntity, InfluencerDemographicsMapper influencerDemographics, List<CampaignOffersV1> campaignOfferV1Entity, PlatformMasterV1 platformName) {
        Logger.getLogger("processInfluencerToCampaign").log(Level.FINE, "Process influencer to campaign method called");

        BrandCampaignDetails brandCampaignDetails = saveToBrandCampaignDetails(brandDetails, brandCampaignDetailsV1Entity,
                influencerDemographics);
        saveToBrandCampaignInfluencerHistory(brandCampaignDetails);
        campaignInfluencerReqEntity.setInfluencerId(influencerUserId);
        ResponseEntity<CampaignInfluencerV1Entity> campaignInfluencerDetails = saveToCampaignInfluencer(campaignInfluencerReqEntity, influencerDemographics, platformName);
        if (campaignInfluencerDetails != null) {
            saveToInfluencerOffer(brandCampaignDetailsV1Entity, campaignInfluencerReqEntity, campaignOfferV1Entity);
        }
    }

    private void saveToInfluencerOffer(BrandCampaignDetailsV1Entity brandCampaignDetailsV1Entity, CampaignInfluencerReqEntity campaignInfluencerReqEntity, List<CampaignOffersV1> campaignOfferV1Entity) {
        Logger.getLogger("saveToInfluencerOffer").log(Level.FINE, "Save to influencer method called");
        try {
            CampaignInfluencerOfferV1Entity campaignInfluencerOfferV1Entity = new CampaignInfluencerOfferV1Entity();

            if (!campaignOfferV1Entity.isEmpty()) {
                for (int i = 0; i < campaignOfferV1Entity.size(); i++) {
                    campaignInfluencerOfferV1Entity.setCampaignOfferInfluencerId(new CampaignInfOfferKey(campaignOfferV1Entity.get(i).getCampaignOfferId().getCampaignId(), campaignInfluencerReqEntity.getInfluencerId(), campaignOfferV1Entity.get(i).getCampaignOfferId().getOfferType().toString()));
                    campaignInfluencerOfferV1Entity.setPaymentOffer(campaignOfferV1Entity.get(i).getPaymentOffer());
                    campaignInfluencerOfferV1Entity.setCommissionAffiliateCustomName(campaignOfferV1Entity.get(i).getCommissionAffiliateCustomName());
                    campaignInfluencerOfferV1Entity.setCommissionAffiliateDetails(campaignOfferV1Entity.get(i).getCommissionAffiliateDetails());
                    campaignInfluencerOfferV1Entity.setCommissionValue(campaignOfferV1Entity.get(i).getCommissionValue());
                    campaignInfluencerOfferV1Entity.setCommissionAffiliateType(campaignOfferV1Entity.get(i).getCommissionAffiliateType());
                    campaignInfluencerOfferV1Entity.setCommissionValueType(campaignOfferV1Entity.get(i).getCommissionValueType());
                    campaignInfluencerOfferV1Entity.setCommissionAffiliateTypeId(campaignOfferV1Entity.get(i).getCommissionAffiliateTypeId());
                    campaignInfluencerOfferV1Entity.setCommissionValueTypeId(campaignOfferV1Entity.get(i).getCommissionValueTypeId());
                    campaignInfOfferV1Repo.save(campaignInfluencerOfferV1Entity);
                }
            }
        } catch (Exception e) {
            mailService.sendErrorMailDetail(e);
            e.printStackTrace();
            Logger.getLogger("saveToInfluencerOffer").log(Level.SEVERE, e.getMessage());
        }
    }

    private BrandCampaignDetails saveToBrandCampaignDetails(Optional<BrandDetails> brandDetails,
                                                            BrandCampaignDetailsV1Entity brandCampaignDetailsV1Entity, InfluencerDemographicsMapper influencerDemographics) {
        Logger.getLogger("saveToBrandCampaignDetails").log(Level.FINE, "Save to brand campaign details method called");
        BrandCampaignDetails brandCampaignDetails = BrandCampaignDetails.builder()
                .brandName(brandDetails.get().getBrandName()).campaignName(brandCampaignDetailsV1Entity.getCampaignName())
                .createdAt(new Date())
                .influencerAudienceCredibilty(influencerDemographics.getCredibility().doubleValue())
                .influencerEngagement(influencerDemographics.getEngagement())
                .influencerFollowers(influencerDemographics.getFollowers())
                .influencerHandle(influencerDemographics.getInfluencerHandle()).bidCount(0)
                .key(BrandCampaignDetailsKey.builder().brandId(brandDetails.get().getKey().getUserId())
                        .campaignId(UUID.fromString(brandCampaignDetailsV1Entity.getCampaignId()))
                        .influencerId(influencerDemographics.getUserId()).build())
                .build();
        brandCampaignDetailsRepo.save(brandCampaignDetails);
        return brandCampaignDetails;
    }

    private BrandCampaignDetails saveUploadToBrandCampaignDetails(Optional<BrandDetails> brandDetails,
                                                                  BrandCampaignDetailsV1 brandCampaignDetailsV1Entity, InfluencerDemographicsMapper influencerDemographics) {
        Logger.getLogger("saveToBrandCampaignDetails").log(Level.FINE, "Save to brand campaign details method called");
        BrandCampaignDetails brandCampaignDetails = BrandCampaignDetails.builder()
                .brandName(brandDetails.get().getBrandName()).campaignName(brandCampaignDetailsV1Entity.getCampaignName())
                .createdAt(new Date())
                .influencerAudienceCredibilty(influencerDemographics.getCredibility().doubleValue())
                .influencerEngagement(influencerDemographics.getEngagement())

                .influencerFollowers(influencerDemographics.getFollowers())
                .influencerHandle(influencerDemographics.getInfluencerHandle()).bidCount(0)
                .key(BrandCampaignDetailsKey.builder().brandId(brandDetails.get().getKey().getUserId())
                        .campaignId(UUID.fromString(brandCampaignDetailsV1Entity.getCampaignId()))
                        .influencerId(influencerDemographics.getUserId()).build())
                .build();
        brandCampaignDetailsRepo.save(brandCampaignDetails);
        return brandCampaignDetails;
    }

    private void saveToBrandCampaignInfluencerHistory(BrandCampaignDetails brandCampaignDetails) {
        Logger.getLogger("saveToBrandCampaignInfluencerHistory").log(Level.FINE, brandCampaignDetails.toString());
        brandCampaignInfluencerHistoryRepo.save(BrandCampaignInfluencerHistory.builder()
                .brandName(brandCampaignDetails.getBrandName()).campaignName(brandCampaignDetails.getCampaignName())
                .createdAt(new Date())
                .influencerAudienceCredibilty(brandCampaignDetails.getInfluencerAudienceCredibilty())
                .influencerEngagement(brandCampaignDetails.getInfluencerEngagement())
                .influencerFollowers(brandCampaignDetails.getInfluencerFollowers())
                .influencerHandle(brandCampaignDetails.getInfluencerHandle())
                .key(BrandCampaignInfluencerHistoryKey.builder().brandId(brandCampaignDetails.getKey().getBrandId())
                        .campaignId(brandCampaignDetails.getKey().getCampaignId())
                        .influencerId(brandCampaignDetails.getKey().getInfluencerId()).build())
                .build());
    }

    private ResponseEntity<CampaignInfluencerV1Entity> saveToCampaignInfluencer(CampaignInfluencerReqEntity campaignInfluencerReqEntity, InfluencerDemographicsMapper influencerDemographics, PlatformMasterV1 platformName) {
        Logger.getLogger("saveToCampaignInfluencer").log(Level.FINE, "Save to campaign influencer method called");
        try {
            if (Objects.equals(campaignInfluencerReqEntity.getPlatform().toLowerCase(), platformName.getPlatformName().toString().toLowerCase())) {
                CampaignInfluencerV1Entity campaignInfluencerV1Entity = new CampaignInfluencerV1Entity();
                CampaignInfluencerKey campaignInfluencerKey = new CampaignInfluencerKey();
                campaignInfluencerKey.setCampaignId(campaignInfluencerReqEntity.getCampaignId().toString());
                campaignInfluencerKey.setInfluencerId(campaignInfluencerReqEntity.getInfluencerId());
                Optional<CampaignInfluencerV1Entity> campaignInfluencerDetails = campaignInfluencerRepo.findByCampaignInfluencerId(campaignInfluencerKey);
                if (campaignInfluencerDetails.isPresent()) {
                    return ResponseEntity.notFound().build();
                } else {
                    campaignInfluencerV1Entity.setCampaignInfluencerId(campaignInfluencerKey);
                    campaignInfluencerV1Entity.setInfluencerEmail(influencerDemographics.getEmail());
                    campaignInfluencerV1Entity.setPlatform(influencerDemographics.getPlatform());
                    campaignInfluencerV1Entity.setInfluencerAudienceCredibility(influencerDemographics.getCredibility().toString());
                    campaignInfluencerV1Entity.setInfluencerEngagement(influencerDemographics.getEngagement().toString());
                    campaignInfluencerV1Entity.setInfluencerEmail(influencerDemographics.getEmail());
                    campaignInfluencerV1Entity.setProposalStatus(String.valueOf(ProposalStatus.INVITE_NOT_SENT));
                    campaignInfluencerV1Entity.setInfluencerHandle(influencerDemographics.getInfluencerHandle());
                    campaignInfluencerV1Entity.setInfluencerFollowers(influencerDemographics.getFollowers().toString());
                    campaignInfluencerV1Entity.setInfluencerFirstName(influencerDemographics.getInfluencerName());
                    campaignInfluencerV1Entity.setPlatform(campaignInfluencerReqEntity.getPlatform());
                    return ResponseEntity.ok(campaignInfluencerRepo.save(campaignInfluencerV1Entity));
                }
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (Exception e) {
            Logger.getLogger("saveToCampaignInfluencer").log(Level.SEVERE, e.getMessage());
            mailService.sendErrorMailDetail(e);
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    public ResponseEntity<Object> copyNewInfluencers(DashBoardModalNew dashBoardModal) {
        UploadInfluencersResponseModel response = new UploadInfluencersResponseModel();
        Logger.getLogger("Copy New influencer").log(Level.FINE, "Copy new influencer method called");
        try {

            UUID brandId = ResourceServer.getUserId();
            DashBoardResponseModal.DashBoardResponseModalBuilder dashBoardResponseModalBuilder = DashBoardResponseModal.builder();
            Set<String> influencersNotAdded = new HashSet<String>();
            Set<String> influencersAdded = new HashSet<String>();
            if (ResourceServer.isFreeBrand()) {
                return new ResponseEntity<>(dashBoardResponseModalBuilder.status(403)
                        .message("Please subscribe for Higher plans to copy influencers from campaign"
                                + "or please contact help@smartfluence.io to upgrade your plan").build(), HttpStatus.OK);
            } else {
                Optional<BrandDetails> brandDetails = getBrandDetails(new UserDetailsKey(UserStatus.APPROVED, brandId));
                if (brandDetails.isPresent()) {
                    Optional<BrandCampaignDetailsV1Entity> brandCampaignDetailsV1Entity = brandCampaignDetailsV1Repo
                            .findByCampaignIdAndBrandId(dashBoardModal.getCampaignId().toString(), brandId.toString());
                    if (brandCampaignDetailsV1Entity.isPresent()) {
                        List<InfluencerDemographicsMapper> influencerDemographics = dashboardDao.getAllInfluencerDemographics(dashBoardModal.getInfluencerIds());
                        for (InfluencerDemographicsMapper data : influencerDemographics) {
                            Optional<CampaignInfluencerV1Entity> campaignInfluencerV1Entity = getCampaignInfluencerMap(brandCampaignDetailsV1Entity.get().getCampaignId(), data.getUserId().toString());
                            if (!campaignInfluencerV1Entity.isPresent()) {
                                processNewInfluencerToCampaign(brandDetails, brandCampaignDetailsV1Entity.get(), data);
                                response.setUploadStatus(UPLOAD_SUCCESS);
                                influencersAdded.add(data.getInfluencerHandle());
                                response.setResponseMessage("Influencers were successfully added to the campaign");
                                response.setInfluencersAdded(influencersAdded.toString());
                            } else {
                                response.setUploadStatus(DUPLICATE);
                                influencersNotAdded.add(data.getInfluencerHandle());
                                response.setResponseMessage("Influencer already exist");
                                response.setInfluencersNotAdded(influencersNotAdded.toString());
                                continue;
                            }
                        }
                    } else {
                        response.setUploadStatus(UPLOAD_ERROR);
                        response.setResponseMessage("Campaign not found");
                    }
                } else {
                    response.setUploadStatus(UPLOAD_ERROR);
                    response.setResponseMessage("Brand details not found");
                }
            }

        } catch (Exception e) {
            Logger.getLogger("Copy influencers").log(Level.SEVERE, e.getMessage());
            mailService.sendErrorMailDetail(e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    public void processNewInfluencerToCampaign(Optional<BrandDetails> brandDetails, BrandCampaignDetailsV1Entity brandCampaignDetailsV1Entity,
                                               InfluencerDemographicsMapper influencerDemographics) {
        Logger.getLogger("processNewInfluencerToCampaign").log(Level.FINE, "Process new influncer method called");

        BrandCampaignDetails brandCampaignDetails = saveToBrandCampaignDetails(brandDetails, brandCampaignDetailsV1Entity,
                influencerDemographics);
        saveToBrandCampaignInfluencerHistory(brandCampaignDetails);
        List<CampaignInfluencerV1Entity> campaignInfluencerDetails = saveToNewCampaignInfluencer(brandCampaignDetailsV1Entity, influencerDemographics);
        if (!campaignInfluencerDetails.isEmpty()) {
            saveToNewInfluencerOffer(brandCampaignDetailsV1Entity, influencerDemographics);
        }
    }

    private void saveToNewInfluencerOffer(BrandCampaignDetailsV1Entity brandCampaignDetailsV1Entity, InfluencerDemographicsMapper influencerDemographicsMapper) {
        Logger.getLogger("saveToNewInfluencerOffer").log(Level.FINE, brandCampaignDetailsV1Entity.toString());
        List<CampaignOffersV1> campaignOfferV1Entity = campaignOffersRepositoryV1
                .findByCampaignOfferId_CampaignId(brandCampaignDetailsV1Entity.getCampaignId());
        CampaignInfluencerOfferV1Entity campaignInfluencerOfferV1Entity = new CampaignInfluencerOfferV1Entity();
        List<CampaignInfluencerOfferV1Entity> campaignInfluencerOfferV1EntityList = new ArrayList<>();
        if (!campaignOfferV1Entity.isEmpty()) {
            for (int i = 0; i < campaignOfferV1Entity.size(); i++) {
                campaignInfluencerOfferV1Entity.setCampaignOfferInfluencerId(new CampaignInfOfferKey(campaignOfferV1Entity.get(i).getCampaignOfferId().getCampaignId(),
                        influencerDemographicsMapper.getUserId().toString(), campaignOfferV1Entity.get(i).getCampaignOfferId().getOfferType().toString()));
                campaignInfluencerOfferV1Entity.setPaymentOffer(campaignOfferV1Entity.get(i).getPaymentOffer());
                campaignInfluencerOfferV1Entity.setCommissionAffiliateCustomName(campaignOfferV1Entity.get(i).getCommissionAffiliateCustomName());
                campaignInfluencerOfferV1Entity.setCommissionAffiliateDetails(campaignOfferV1Entity.get(i).getCommissionAffiliateDetails());
                campaignInfluencerOfferV1Entity.setCommissionValue(campaignOfferV1Entity.get(i).getCommissionValue());
                campaignInfluencerOfferV1Entity.setCommissionAffiliateType(campaignOfferV1Entity.get(i).getCommissionAffiliateType());
                campaignInfluencerOfferV1Entity.setCommissionValueType(campaignOfferV1Entity.get(i).getCommissionValueType());
                campaignInfluencerOfferV1Entity.setCommissionAffiliateTypeId(campaignOfferV1Entity.get(i).getCommissionAffiliateTypeId());
                campaignInfluencerOfferV1Entity.setCommissionValueTypeId(campaignOfferV1Entity.get(i).getCommissionValueTypeId());

                CampaignInfluencerOfferV1Entity saveObj = campaignInfOfferV1Repo.save(campaignInfluencerOfferV1Entity);
                campaignInfluencerOfferV1EntityList.add(saveObj);
            }
        } else {
            Logger.getLogger("saveToNewInfluencerOffer").log(Level.WARNING, brandCampaignDetailsV1Entity.toString());
        }
    }


    private List<CampaignInfluencerV1Entity> saveToNewCampaignInfluencer(BrandCampaignDetailsV1Entity brandCampaignDetailsV1Entity, InfluencerDemographicsMapper influencerDemographics) {
        Logger.getLogger("saveToNewCampaignInfluencer").log(Level.FINE, brandCampaignDetailsV1Entity.toString());
        CampaignInfluencerV1Entity campaignInfluencerV1Entity = new CampaignInfluencerV1Entity();
        List<CampaignInfluencerV1Entity> campaignInfluencerV1EntityList = new ArrayList<>();
        CampaignInfluencerKey campaignInfluencerKey = new CampaignInfluencerKey();
        campaignInfluencerKey.setCampaignId(brandCampaignDetailsV1Entity.getCampaignId());
        campaignInfluencerKey.setInfluencerId(influencerDemographics.getUserId().toString());
        Optional<CampaignInfluencerV1Entity> campaignInfluencerDetails = campaignInfluencerRepo.findAllByCampaignInfluencerId(campaignInfluencerKey);
        if (campaignInfluencerDetails.isPresent()) {
            Logger.getLogger("saveToNewCampaignInfluencer").log(Level.SEVERE, "Brand campaign details already present");
            return null;
        } else {
            campaignInfluencerV1Entity.setCampaignInfluencerId(campaignInfluencerKey);
            campaignInfluencerV1Entity.setInfluencerEmail(influencerDemographics.getEmail());
            campaignInfluencerV1Entity.setPlatform(influencerDemographics.getPlatform());
            campaignInfluencerV1Entity.setInfluencerAudienceCredibility(influencerDemographics.getCredibility().toString());
            campaignInfluencerV1Entity.setInfluencerEngagement(influencerDemographics.getEngagement().toString());
            campaignInfluencerV1Entity.setInfluencerEmail(influencerDemographics.getEmail());
            campaignInfluencerV1Entity.setProposalStatus(String.valueOf(ProposalStatus.INVITE_NOT_SENT));
            campaignInfluencerV1Entity.setInfluencerHandle(influencerDemographics.getInfluencerHandle());
            campaignInfluencerV1Entity.setInfluencerFollowers(influencerDemographics.getFollowers().toString());
            campaignInfluencerV1Entity.setPlatform(influencerDemographics.getPlatform());
            campaignInfluencerV1Entity.setInfluencerFirstName(influencerDemographics.getInfluencerName());
            CampaignInfluencerV1Entity saveObj = campaignInfluencerRepo.save(campaignInfluencerV1Entity);
            campaignInfluencerV1EntityList.add(saveObj);

        }
        return campaignInfluencerV1EntityList;
    }

    public ResponseEntity<Object> uploadNewInfluencers(UploadInfluencersRequestModelNew request, UUID campaignId) {
        Logger.getLogger("uploadNewInfluencers").log(Level.FINE, request.toString());
        String influencerUserId = null;
        UploadInfluencersResponseModel response = new UploadInfluencersResponseModel();
        try {
            if (validateUploadInfluencers(request, response)) {
                UUID brandId = ResourceServer.getUserId();
                JSONObject wookBookJson = new JSONObject(request.getInfluencerHandles());
                Optional<BrandDetails> brandDetails = dashboardDao.getBrandDetails(new UserDetailsKey(UserStatus.APPROVED, brandId));

                if (brandDetails.isPresent()) {
                    Optional<BrandCampaignDetailsV1> brandCampaignDetailsV1 = brandCampaignDetailsRepositoryV1
                            .findByCampaignIdAndBrandId(campaignId.toString(), brandId.toString());
                    if (brandCampaignDetailsV1.isPresent()) {
                        UserAccounts userAccounts = dashboardDao.getUserAccounts();
                        List<CampaignTasksV1> campaignTasksV1 = campaignEntityServiceV1.getTasksByCampaignId(brandCampaignDetailsV1.get().getCampaignId());
                        PlatformMasterV1 platformName = masterServiceV1.getPlatformById(campaignTasksV1.get(0).getPlatformId());

                        for (int i = 0; i < request.getWorkbookSheets().size(); i++) {
                            Set<String> influencersNotAdded = new HashSet<String>();
                            Set<String> influencersNotFound = new HashSet<String>();
                            Set<String> influencersPulling = new HashSet<String>();
                            JSONArray sheetJsonArray = wookBookJson.getJSONArray(request.getWorkbookSheets().get(i));

                            if (sheetJsonArray.length() <= MAX_INFLUENCER_PER_UPLOAD) {
                                for (int j = 0; j < sheetJsonArray.length(); j++) {
                                    JSONObject influencers = new JSONObject(sheetJsonArray.get(j).toString());
                                    if (!influencers.getString(PLATFORM).equalsIgnoreCase(platformName.getPlatformName())) {
                                        response.setUploadStatus(PLATFORM_ERROR);
                                        response.setResponseMessage("Platform should be Campaign specific");
                                    } else if (influencers.has(NAME) && influencers.has(PLATFORM)) {
                                        String influencerHandle = influencers.getString(NAME).startsWith("@")
                                                ? influencers.getString(NAME).substring(1) : influencers.getString(NAME);
                                        influencerHandle = influencerHandle.toLowerCase();
                                        String platform = influencers.getString(PLATFORM);

                                        if (!platform.equalsIgnoreCase(Platforms.INSTAGRAM.name()) && !platform.equalsIgnoreCase(Platforms.YOUTUBE.name())
                                                && !platform.equalsIgnoreCase(Platforms.TIKTOK.name()))
                                            continue;
                                        if (influencerHandle.equalsIgnoreCase(DEFAULT_NAME))
                                            continue;

                                        Optional<SocialIdMaster> socialIdMaster = dashboardDao.getSocialIdMasterByHandleAndPlatform
                                                ("@" + influencerHandle, reportsService.getPlatform(platform.toLowerCase()));

                                        if (!socialIdMaster.isPresent()) {
                                            try {
                                                UUID userid = reportsService.saveNewInfluencer(influencerHandle, platform, ViewType.UPLOAD);
                                                socialIdMaster = dashboardDao.getSocialIdMasterById(userid.toString());
                                                influencerUserId = socialIdMaster.get().getUserId();
                                            } catch (Exception e) {
                                                if (e.getMessage().contains("retry_later")) {
                                                    influencersPulling.add(influencerHandle);
                                                    response.setInfluencersPulling(influencersPulling.toString());
                                                } else {
                                                    influencersNotFound.add(influencerHandle);
                                                    response.setInfluencersNotFound(influencersNotFound.toString());
                                                }
                                                continue;
                                            }
                                        } else {
                                            influencerUserId = socialIdMaster.get().getUserId();
                                            reportsService.saveReportHistory(ReportSearchHistory.builder()
                                                    .key(ReportSearchHistoryKey.builder().brandId(UUID.fromString(userAccounts.getUserId())).isNew(false)
                                                            .createdAt(new Date()).build())
                                                    .influencerUsername(socialIdMaster.get().getInfluencerHandle())
                                                    .brandName(userAccounts.getBrandName()).influencerDeepSocialId("").reportId("")
                                                    .viewType(ViewType.UPLOAD).platform(reportsService.getPlatform(platform.toLowerCase())).build());
                                        }
                                        InfluencerDemographicsMapper influencerDemographics = reportsService.getInfluencerDemographics(socialIdMaster.get(),
                                                influencerUserId, ViewType.CAMPAIGN, true);

                                        Optional<CampaignInfluencerV1Entity> campaignInfluencerV1Entity = getCampaignInfluencerMap(campaignId.toString(), influencerUserId);

                                        if (!campaignInfluencerV1Entity.isPresent()) {
                                            processUploadInfluencerToCampaign(brandDetails, influencerUserId, platform, brandCampaignDetailsV1.get(), influencerDemographics, campaignId);
                                            response.setUploadStatus(UPLOAD_SUCCESS);
                                            response.setResponseMessage("Influencers were successfully added to the campaign");
                                        } else {
                                            influencersNotAdded.add(influencerHandle);
                                            response.setInfluencersNotAdded(influencersNotAdded.toString());
                                            continue;
                                        }
                                    } else if (j == 0) {
                                        response.setUploadStatus(UPLOAD_ERROR);
                                        response.setResponseMessage("Please upload valid template");
                                        break;
                                    }
                                }
                            } else {
                                response.setUploadStatus(UPLOAD_ERROR);
                                response.setResponseMessage("Not more than 100 Influencers can be processed during upload! "
                                        + "Contact us for any assistance.");
                                break;
                            }
                        }
                    } else {
                        response.setUploadStatus(UPLOAD_ERROR);
                        response.setResponseMessage("Campaign not found");
                    }
                } else {
                    response.setUploadStatus(UPLOAD_ERROR);
                    response.setResponseMessage("Brand details not found");
                }
            }
        } catch (JSONException e) {
            mailService.sendErrorMailDetail(e);
            Logger.getLogger("uploadNewInfluencers").log(Level.SEVERE, e.getMessage());
            e.getStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private ResponseEntity<CampaignInfluencerV1Entity> saveToCampaignInfluencer(InfluencerDemographicsMapper influencerDemographics, String influencerId, String platform, String campaignId) {
        Logger.getLogger("saveToCampaignInfluencer").log(Level.FINE, "Save to campaign influencer method called");
        try {
            CampaignInfluencerV1Entity campaignInfluencerV1Entity = new CampaignInfluencerV1Entity();
            CampaignInfluencerKey campaignInfluencerKey = new CampaignInfluencerKey();
            campaignInfluencerKey.setCampaignId(campaignId);
            campaignInfluencerKey.setInfluencerId(influencerId);
            Optional<CampaignInfluencerV1Entity> campaignInfluencerDetails = campaignInfluencerRepo.findByCampaignInfluencerId(campaignInfluencerKey);
            if (campaignInfluencerDetails.isPresent()) {
                return ResponseEntity.notFound().build();
            } else {
                campaignInfluencerV1Entity.setCampaignInfluencerId(campaignInfluencerKey);
                campaignInfluencerV1Entity.setInfluencerEmail(influencerDemographics.getEmail());
                campaignInfluencerV1Entity.setPlatform(influencerDemographics.getPlatform());
                campaignInfluencerV1Entity.setInfluencerAudienceCredibility(influencerDemographics.getCredibility().toString());
                campaignInfluencerV1Entity.setInfluencerEngagement(influencerDemographics.getEngagement().toString());
                campaignInfluencerV1Entity.setInfluencerEmail(influencerDemographics.getEmail());
                campaignInfluencerV1Entity.setProposalStatus(String.valueOf(ProposalStatus.INVITE_NOT_SENT));
                campaignInfluencerV1Entity.setInfluencerHandle(influencerDemographics.getInfluencerHandle());
                campaignInfluencerV1Entity.setInfluencerFollowers(influencerDemographics.getFollowers().toString());
                campaignInfluencerV1Entity.setInfluencerFirstName(influencerDemographics.getInfluencerName());
                campaignInfluencerV1Entity.setPlatform(platform.toLowerCase());

                return ResponseEntity.ok(campaignInfluencerRepo.save(campaignInfluencerV1Entity));
            }
        } catch (Exception e) {
            Logger.getLogger("saveToCampaignInfluencer").log(Level.SEVERE, e.getMessage());
            mailService.sendErrorMailDetail(e);
            return ResponseEntity.badRequest().build();
        }
    }

    private void saveToInfluencerOffer(BrandCampaignDetailsV1 brandCampaignDetailsV1Entity, String influencerId) {
        Logger.getLogger("saveToInfluencerOffer").log(Level.FINE, brandCampaignDetailsV1Entity.toString());
        try {
            List<CampaignOffersV1> campaignOfferV1Entity = campaignOffersRepositoryV1
                    .findByCampaignOfferId_CampaignId(brandCampaignDetailsV1Entity.getCampaignId().toString());
            CampaignInfluencerOfferV1Entity campaignInfluencerOfferV1Entity = new CampaignInfluencerOfferV1Entity();
            if (!campaignOfferV1Entity.isEmpty()) {
                for (int i = 0; i < campaignOfferV1Entity.size(); i++) {
                    campaignInfluencerOfferV1Entity.setCampaignOfferInfluencerId(new CampaignInfOfferKey(campaignOfferV1Entity.get(i).getCampaignOfferId().getCampaignId(), influencerId, campaignOfferV1Entity.get(i).getCampaignOfferId().getOfferType().toString()));
                    campaignInfluencerOfferV1Entity.setPaymentOffer(campaignOfferV1Entity.get(i).getPaymentOffer());
                    campaignInfluencerOfferV1Entity.setCommissionAffiliateCustomName(campaignOfferV1Entity.get(i).getCommissionAffiliateCustomName());
                    campaignInfluencerOfferV1Entity.setCommissionAffiliateDetails(campaignOfferV1Entity.get(i).getCommissionAffiliateDetails());
                    campaignInfluencerOfferV1Entity.setCommissionValue(campaignOfferV1Entity.get(i).getCommissionValue());
                    campaignInfluencerOfferV1Entity.setCommissionAffiliateType(campaignOfferV1Entity.get(i).getCommissionAffiliateType());
                    campaignInfluencerOfferV1Entity.setCommissionValueType(campaignOfferV1Entity.get(i).getCommissionValueType());
                    campaignInfluencerOfferV1Entity.setCommissionAffiliateTypeId(campaignOfferV1Entity.get(i).getCommissionAffiliateTypeId());
                    campaignInfluencerOfferV1Entity.setCommissionValueTypeId(campaignOfferV1Entity.get(i).getCommissionValueTypeId());
                    campaignInfOfferV1Repo.save(campaignInfluencerOfferV1Entity);
                }
            }
        } catch (Exception e) {
            mailService.sendErrorMailDetail(e);
            e.printStackTrace();
            Logger.getLogger("saveToInfluencerOffer").log(Level.SEVERE, e.getMessage());
        }
    }

    public void processUploadInfluencerToCampaign(Optional<BrandDetails> brandDetails, String influencerUserId, String platform, BrandCampaignDetailsV1 brandCampaignDetailsV1Entity, InfluencerDemographicsMapper influencerDemographics, UUID campaignId) {
        Logger.getLogger("processUploadInfluencerToCampaign").log(Level.FINE, "Process Upload influencer to campaign method called");
        BrandCampaignDetails brandCampaignDetails = saveUploadToBrandCampaignDetails(brandDetails, brandCampaignDetailsV1Entity,
                influencerDemographics);
        saveToBrandCampaignInfluencerHistory(brandCampaignDetails);
        ResponseEntity<CampaignInfluencerV1Entity> campaignInfluencerDetails = saveToCampaignInfluencer(influencerDemographics, influencerUserId, platform, campaignId.toString());
        if (campaignInfluencerDetails != null) {
            saveToInfluencerOffer(brandCampaignDetailsV1Entity, influencerUserId);
        }
    }

    private boolean validateUploadInfluencers(UploadInfluencersRequestModelNew request, UploadInfluencersResponseModel response) {
        Logger.getLogger("validateUploadInfluencers").log(Level.FINE, "validate upload influencers method called");
        if (!ResourceServer.isFreeBrand()) {
            if (request.getWorkbookSheets().size() == 1)
                return true;
            else {
                response.setUploadStatus(UPLOAD_ERROR);
                response.setResponseMessage("Only one sheet is allowed while uploading influencers");
                return false;
            }
        } else {
            response.setUploadStatus(UPLOAD_ERROR);
            response.setResponseMessage("Please subscribe for Higher plans to upload influencers into campaign "
                    + "or please contact help@smartfluence.io to upgrade your plan");
            return false;
        }
    }


    @Override
    public ResponseEntity<Object> getCampaignByInfluencer() {
        return super.getCampaignByInfluencer();
    }

    @Override
    public void truncateTemporaryContentTable() {
        super.truncateTemporaryContentTable();
    }

    @Override
    public List<MailTemplates> getMailTemplates() {
        return super.getMailTemplates();
    }

    @Override
    public Optional<LocationMaster> getLocationByName(String name) {
        return super.getLocationByName(name);
    }

    @Override
    public List<LocationMaster> getLocationUtilities(String name) {
        return super.getLocationUtilities(name);
    }

    @Override
    public Optional<IndustryMaster> getIndustryByName(String name) {
        return super.getIndustryByName(name);
    }

    @Override
    public List<IndustryMaster> getIndustryUtilities(String name) {
        return super.getIndustryUtilities(name);
    }

    @Override
    public Optional<BrandSearchHistory> getBrandSearchHistoryById(BrandSearchHistoryKey key) {
        return super.getBrandSearchHistoryById(key);
    }

    @Override
    public void saveBrandSearchHistory(BrandSearchHistory brandSearchHistory) {
        super.saveBrandSearchHistory(brandSearchHistory);
    }

    @Override
    public UUID saveBrandSearchHistory(UUID brandId, GetInfluencersRequestModel request, InfluencersDataResponse body) {
        return super.saveBrandSearchHistory(brandId, request, body);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}