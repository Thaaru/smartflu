package io.smartfluence.cassandra.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.constants.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("influencer_profile_details")
public class InfluencerProfileDetails implements Serializable {

	private static final long serialVersionUID = 28103925411147952L;

	@PrimaryKey("user_id")
	private UUID userId;

	@Column("address_1")
	private String address1;

	@Column("address_2")
	private String address2;

	@Column("city")
	private String city;

	@Column("country")
	private String country;

	@Column("dob")
	private String dob;

	@Column("gender")
	private String gender;

	@Column("paypal_email")
	private String paypalEmail;

	@Column("state")
	private String state;

	@Column("created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@Column("user_status")
	private UserStatus userStatus;
}