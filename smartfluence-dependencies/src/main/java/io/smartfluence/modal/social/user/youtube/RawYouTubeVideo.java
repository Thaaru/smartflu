package io.smartfluence.modal.social.user.youtube;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawYouTubeVideo implements Serializable {

	private static final long serialVersionUID = 4052449327150722220L;

	@JsonAlias("video_id")
	private String videoId;

	@JsonAlias("channel_id")
	private String channelId;

	@JsonAlias("title")
	private String title;

	@JsonAlias("views")
	private Long views;

	@JsonAlias("likes")
	private Long likes;

	@JsonAlias("dislikes")
	private Long dislikes;

	@JsonAlias("comments")
	private Long comments;

	@JsonAlias("genre")
	private String genre;

	@JsonAlias("thumbnail")
	private String thumbnail;

	@JsonAlias("description")
	private String description;
}