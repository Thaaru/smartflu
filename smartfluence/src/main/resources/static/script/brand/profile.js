var profileEditOpen = false;
var passwordEditOpen = false;

function enableProfileEdit() {
	toggleProfileEditClass();
	if (profileEditOpen == false) {
		profileEditOpen = true;
	} else {
		profileEditOpen = false;
	}
	if (passwordEditOpen == true) {
		toggleViewPasswordClass();
		passwordEditOpen = false;
	}
}

function toggleProfileEditClass() {
	$('.brandName').toggleClass('disabled');
	$('.brandName').toggleClass('disabled-grey');
	$('.firstName').toggleClass('disabled');
	$('.firstName').toggleClass('disabled-grey');
	$('.lastName').toggleClass('disabled');
	$('.lastName').toggleClass('disabled-grey');
	$('.industry').toggleClass('disabled');
	$('.industry').toggleClass('disabled-grey');
	$('.website').toggleClass('disabled');
	$('.website').toggleClass('disabled-grey');
	$('.email').toggleClass('disabled');
	$('.email').toggleClass('disabled-grey');
	$('#save').toggleClass('disabled');
}

/*
var mailValidatesuccess = true;

$('#email').on('blur', function() {
	var value = $('#email').val();
	validateEmail(value);
})

$('#email').on('focus', function() {
	$('#email').removeClass("border-danger");
	$('#emailInvalid').empty();
})

function validateEmail(email) {
	var invalidEmailMessage = 'Please enter valid email address';
	var reg = /^[a-zA-Z0-9]+([\-\.][a-zA-Z0-9]+)*\@[a-zA-Z0-9]+([\-\.][a-zA-Z0-9]+)*\.[a-zA-Z]{2,4}$/;
	if (reg.test(email) == false) {
		$('#email').addClass("border-danger");
		$('#emailInvalid').html(invalidEmailMessage); mailValidatesuccess = false;
	}
	else {
		$('#email').removeClass("border-danger"); $('#emailInvalid').empty();
		mailValidatesuccess = true;
	}
}

function saveProfile(obj) { var value =
	$('#email').val(); validateEmail(value);

	if (mailValidatesuccess) {
		var form = $(obj).ajaxSubmit();
		var xhr = form.data('jqxhr');
		$('#save').addClass('disabled');
		xhr.done(function(d, s, r) {
			$('#save').removeClass('disabled');
			SUCCESS("Profile Details saved successfully");
			toggleProfileEditClass();
		});
		xhr.fail(function(d, s, r) {
			$('#save').removeClass('disabled');
			if (d.responseText != null) {
				ERROR(d.responseText);
			} else {
				ERROR('Failed to save profile details');
			}
		});
	}
}
*/

function saveProfile(obj) {

	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');
	$('#save').addClass('disabled');
	xhr.done(function(d, s, r) {
		$('#save').removeClass('disabled');
		SUCCESS("Profile Details saved successfully");
		toggleProfileEditClass();
	});
	xhr.fail(function(d, s, r) {
		$('#save').removeClass('disabled');
		if (d.status == 406) {
			ERROR(d.responseText);
		} else {
			ERROR('Failed to save profile details');
		}
	});
}

function savePassword(obj) {
	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');

	xhr.done(function(d, s, r) {
		SUCCESS("Password changed successfully");
		$('#oldPassword').val('');
		$('#newPassword').val('');
		$('.password').toggleClass('expand-password');
		/* $(".profile-edit").click(); */
	});
	xhr.fail(function(x) {
		ERROR('Failed to change password');
	});
}

function viewPassword() {
	toggleViewPasswordClass();

	if (passwordEditOpen == false) {
		passwordEditOpen = true;
		console.log(passwordEditOpen);
	} else {
		passwordEditOpen = false;
		console.log(passwordEditOpen);
	}
	if (profileEditOpen == true) {
		toggleProfileEditClass();
		profileEditOpen = false;
	}
	window.scrollTo(0, document.body.scrollHeight);
}

function toggleViewPasswordClass() {
	$('.password').toggleClass('expand-password');
}

$(document).ready(function() {

	if (profileDetails.userStatus != 'FREE') {
		$('#connectAccountDiv').removeClass('d-none');
	}
	if(emailConnection != undefined) {
		$("#connectAccount").html('Connected');
	} else {
		$("#connectAccount").html('Connect');
	}

	var tokenState = getUrlParameter('state');

	if (tokenState == 'true') {
		$('#connectAccount').addClass("disabled");
		var tokenCode = getUrlParameter('code');
		hideURLParams();
		getAccessToken(tokenCode);
	}
});

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
		}
	}
};

function hideURLParams() {
	var hide = ['state','code'];
	for(var h in hide) {
		if (paramToHide(h)) {
			history.replaceState(null, document.getElementsByTagName("title")[0].innerHTML, window.location.pathname);
		}
	}
}

function paramToHide(name) {
	return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]);
}

$('#connectAccount').click(function() {
	if(emailConnection != undefined) {
		$("#removeEmail").modal('show');
	} else {
		$('#connectAccount').addClass("disabled");
		var userEmail = $('#email').val();

		$.get({
			url : window.location.origin + '/brand/get-settings/' + userEmail + '/get-tokenURL'
		}).done(function(tokenURL) {
			window.open(tokenURL, '_self');
		}).fail(function() {
			console.log("Failed to get URL");
		});
	}
});

function getAccessToken(code) {

	$.get({
		url : window.location.origin + '/brand/get-settings/' + code + '/get-accessToken'
	}).done(function(message) {
		console.log('Account has been linked successfully...!');
		toast(message);
		emailConnection = 'Connected';
		$("#connectAccount").html('Connected');
		$('#connectAccount').removeClass("disabled");
	}).fail(function() {
		console.log("Failed to link account");
		$('#connectAccount').removeClass("disabled");
	});
}

function removeEmail() {
	$("#removeEmail").modal('hide');
	$('#connectAccount').addClass("disabled");

	$.get({
		url : window.location.origin + '/brand/get-settings/remove-account'
	}).done(function(message) {
		console.log('Account has been removed successfully');
		toast(message);
		emailConnection = undefined;
		$("#connectAccount").html('Connect');
		$('#connectAccount').removeClass("disabled");
	}).fail(function() {
		console.log('Failed to get URL');
		toast('Failed to remove account, Please try after sometime');
		$("#connectAccount").html('Connected');
		$('#connectAccount').removeClass("disabled");
	});
}