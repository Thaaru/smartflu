import React, { useState, useEffect } from "react";
import { Collapse } from "reactstrap";
import { IoIosArrowForward ,IoIosArrowDown} from "react-icons/io";
import { NavItem } from "react-bootstrap";
import { PRIMARY_COLOR } from "../utils/string_constants";

function CollapsiblePanel({ children, ...props }) {
  const { data, collapse } = props;
  const [isCollapse, setIsCollapse] = useState(!collapse);
  const [iconData, setIcon] = useState(<IoIosArrowForward/>);
  const [iconStatus,setIconStatus] = useState(true);
  const toggle = () => {
    setIsCollapse(!isCollapse);
    // animate();
    if(iconStatus == true){
      setIconStatus(false);
      setIcon(<IoIosArrowDown/> )
    }else{
      setIconStatus(true);
      setIcon(<IoIosArrowForward/> )
    }

  };

  return (
    <>
    <div className="coll-panel" style={{backgroundColor:"#F8F4FF",border: `1px solid ${PRIMARY_COLOR}`}}>
      <button
        type="button"
        className="btn btn-outline-primary btn-lg btn-block"
        style={{"height":"50px","backgroundColor":"#F8F4FF","borderWidth":"0px","borderColor":null,"color":{PRIMARY_COLOR},outline:null,"textAlign":"left" ,boxShadow:"none"}}
        onClick={() => toggle()}
      >
        {
        <li class="normalText" style={{ "list-style-type": "none" }}>{iconData}{"    "}{data.platformIcon} {!data ? "No data Available" : `${data.platformName}   :   ${data.postTypeName}`}</li>
        }
        </button>
      <Collapse className="border text-left p-2" isOpen={isCollapse} >
        {children}
      </Collapse>
    </div>
    <br/>
    </>
  );
}

CollapsiblePanel.defaultProps = {
  children: "Add node as a child",
  title: "Collapsible Panel",
  collapse: true
};

export default CollapsiblePanel;
