package io.smartfluence.modal.brand.dashboard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PostDataModal {

	private String caption;

	private Integer noOfLikes;

	private Integer noOfComments;

	private Integer noOfViews;

	private String userName;
}