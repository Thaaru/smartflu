package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PostAnalyticsDataKey implements Serializable {

	private static final long serialVersionUID = -2065948226055698209L;

	public PostAnalyticsDataKey(PostAnalyticsDataKey key) {
		this.brandId = key.getBrandId();
		this.campaignId = key.getCampaignId();
		this.influencerId = key.getInfluencerId();
		this.postId = key.getPostId();
		this.postTimeHourIndex = key.getPostTimeHourIndex();
	}

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.PARTITIONED)
	private UUID brandId;

	@PrimaryKeyColumn(name = "campaign_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID campaignId;

	@PrimaryKeyColumn(name = "influencer_id", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private UUID influencerId;

	@PrimaryKeyColumn(name = "post_id", type = PrimaryKeyType.CLUSTERED, ordinal = 3)
	private UUID postId;

	@PrimaryKeyColumn(name = "post_time_hour_index", type = PrimaryKeyType.CLUSTERED, ordinal = 4)
	private Integer postTimeHourIndex;
}