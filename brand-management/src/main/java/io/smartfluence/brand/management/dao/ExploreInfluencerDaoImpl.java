package io.smartfluence.brand.management.dao;

import java.text.SimpleDateFormat;
import java.util.*;

import io.smartfluence.brand.management.entity.CampaignInfluencerKey;
import io.smartfluence.brand.management.entityv1.BrandInfluObj;
import io.smartfluence.brand.management.entityv1.CampaignInfluencerV1Entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Sets;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.mailbean.MailService;
import io.smartfluence.brand.management.repository.CampaignInfluencerRepo;
import io.smartfluence.brand.management.service.ReportsService;
import io.smartfluence.brand.management.util.SendMailUtil;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.TemporaryInfluencerContent;
import io.smartfluence.cassandra.entities.brand.BrandBidDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignHistory;
import io.smartfluence.cassandra.entities.brand.BrandCampaignInfluencerHistory;
import io.smartfluence.cassandra.entities.brand.BrandCreditAllowance;
import io.smartfluence.cassandra.entities.brand.BrandCreditHistory;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandMailTemplates;
import io.smartfluence.cassandra.entities.brand.BrandSearchHistory;
import io.smartfluence.cassandra.entities.brand.primary.BrandBidDetailsKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignDetailsKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignHistoryKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignInfluencerHistoryKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandSearchHistoryKey;
import io.smartfluence.cassandra.entities.instagram.InstagramContent;
import io.smartfluence.cassandra.entities.instagram.InstagramCountryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.instagram.InstagramIndustryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramStateDemographic;
import io.smartfluence.cassandra.entities.primary.ReportSearchHistoryKey;
import io.smartfluence.cassandra.entities.primary.TemporaryInfluencerContentKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.entities.tiktok.TikTokContent;
import io.smartfluence.cassandra.entities.tiktok.TikTokCountryDemographic;
import io.smartfluence.cassandra.entities.tiktok.TikTokDemographics;
import io.smartfluence.cassandra.entities.youtube.YouTubeContent;
import io.smartfluence.cassandra.entities.youtube.YouTubeCountryDemographic;
import io.smartfluence.cassandra.entities.youtube.YouTubeDemographics;
import io.smartfluence.cassandra.repository.brand.BrandBidDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignHistoryRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignInfluencerHistoryRepo;
import io.smartfluence.cassandra.repository.brand.BrandCreditAllowanceRepo;
import io.smartfluence.cassandra.repository.brand.BrandCreditHistoryRepo;
import io.smartfluence.cassandra.repository.brand.BrandCreditsRepo;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandMailTemplatesRepo;
import io.smartfluence.cassandra.repository.brand.BrandSearchHistoryRepo;
import io.smartfluence.cassandra.repository.InfluencerDetailsRepo;
import io.smartfluence.cassandra.repository.TemporaryInfluencerContentRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramContentRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramCountryDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramIndustryDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramStateDemographicRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokContentRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokCountryDemographicRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokDemographicRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeContentRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeCountryDemographicRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeDemographicRepo;
import io.smartfluence.constants.BidStatus;
import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.constants.CreditType;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.constants.ViewType;
import io.smartfluence.modal.brand.dashboard.MailTemplates;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.brand.viewinfluencer.BrandCampaigns;
import io.smartfluence.modal.social.search.response.InfluencersDataResponse;
import io.smartfluence.modal.validation.brand.explore.CampaignDetail;
import io.smartfluence.modal.validation.brand.explore.GetInfluencersRequestModel;
import io.smartfluence.modal.validation.brand.explore.SubmitBidModel;
import io.smartfluence.mysql.entities.BidStatusMapping;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.CampaignInfluencerMapping;
import io.smartfluence.mysql.entities.LocationMaster;
import io.smartfluence.mysql.entities.IndustryMaster;
import io.smartfluence.mysql.entities.MailDetails;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.BidStatusMappingRepo;
import io.smartfluence.mysql.repository.BrandCampaignDetailsMysqlRepo;
import io.smartfluence.mysql.repository.CampaignInfluencerMappingRepo;
import io.smartfluence.mysql.repository.LocationMasterRepo;
import io.smartfluence.mysql.repository.IndustryMasterRepo;
import io.smartfluence.mysql.repository.MailTemplateMasterRepo;
import io.smartfluence.mysql.repository.SocialIdMasterRepo;
import io.smartfluence.mysql.repository.UserAccountsRepo;
import io.smartfluence.brand.management.constants.ProposalStatus;
//@Repository
public class ExploreInfluencerDaoImpl implements ExploreInfluencerDao {

	private static final int CAMPAIGN_PER_INFLUENCER = 3;

	private static final int MAX_INFLUENCER_PER_CAMPAIGN = 30;

	@Autowired
	private MailService mailService;

	@Autowired
	private BrandBidDetailsRepo brandBidDetailsRepo;

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Autowired
	protected InfluencerDetailsRepo influencerDetailsRepo;

	@Autowired
	private SocialIdMasterRepo socialIdMasterRepo;

	@Autowired
	private BrandCreditAllowanceRepo brandCreditAllowanceRepo;

	@Autowired
	private InstagramDemographicRepo instagramDemographicRepo;

	@Autowired
	private InstagramCountryDemographicRepo instagramCountryDemographicRepo;

	@Autowired
	private InstagramStateDemographicRepo instagramStateDemographicRepo;

	@Autowired
	private InstagramIndustryDemographicRepo instagramIndustryDemographicRepo;

	@Autowired
	private YouTubeDemographicRepo youtubeDemographicRepo;

	@Autowired
	private YouTubeContentRepo youtubeContentRepo;

	@Autowired
	private YouTubeCountryDemographicRepo youtubeCountryDemographicRepo;

	@Autowired
	private TikTokDemographicRepo tiktokDemographicRepo;

	@Autowired
	private TikTokContentRepo tiktokContentRepo;

	@Autowired
	private TikTokCountryDemographicRepo tiktokCountryDemographicRepo;

	@Autowired
	private BrandCampaignInfluencerHistoryRepo brandCampaignInfluencerHistoryRepo;

	@Autowired
	private BrandCampaignDetailsRepo brandCampaignDetailsRepo;

	@Autowired
	private BrandCampaignHistoryRepo brandCampaignHistoryRepo;

	@Autowired
	private CampaignInfluencerMappingRepo campaignInfluencerMappingRepo;

	@Autowired
	private BrandCampaignDetailsMysqlRepo brandCampaignDetailsMysqlRepo;

	@Autowired
	private BidStatusMappingRepo bidStatusMappingRepo;

	@Autowired
	private ReportsService reportsService;

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private InstagramContentRepo instagramContentRepo;

	@Autowired
	private TemporaryInfluencerContentRepo temporaryInfluencerContentRepo;

	@Autowired
	private MailTemplateMasterRepo mailTemplateMasterRepo;

	@Autowired
	private BrandMailTemplatesRepo brandMailTemplatesRepo;

	@Autowired
	private LocationMasterRepo locationMasterRepo;

	@Autowired
	private IndustryMasterRepo industryMasterRepo;

	@Autowired
	private BrandCreditHistoryRepo brandCreditHistoryRepo;

	@Autowired
	private BrandCreditsRepo brandCreditsRepo;

	@Autowired
	private SendMailUtil sendMailUtil;

	@Autowired
	private BrandSearchHistoryRepo brandSearchHistoryRepo;

	@Autowired
    CampaignInfluencerRepo campaignInfluencerRepo;

	@Override
	public Optional<UserAccounts> getUserAccounts(String brandId) {
		return userAccountsRepo.findById(brandId);
	}

	@Override
	public Optional<InstagramDemographics> getInstagramDemographics(String influencerHandle) {
		return instagramDemographicRepo.findById(influencerHandle);
	}

	@Override
	public List<InstagramCountryDemographic> getAllInstagramCountryByHandle(String userName) {
		return instagramCountryDemographicRepo.findAllByKeyInfluencerHandle(userName);
	}

	@Override
	public List<InstagramIndustryDemographic> getAllInstagramIndustryByHandle(String userName) {
		return instagramIndustryDemographicRepo.findAllByKeyInfluencerHandle(userName);
	}

	@Override
	public List<InstagramStateDemographic> getAllInstagramStateByHandle(String userName) {
		return instagramStateDemographicRepo.findAllByKeyInfluencerHandle(userName);
	}

	@Override
	public Optional<YouTubeDemographics> getYouTubeDemographics(String influencerHandle) {
		return youtubeDemographicRepo.findById(influencerHandle);
	}

	@Override
	public List<YouTubeCountryDemographic> getAllYoutubeCountryByHandle(String userName) {
		return youtubeCountryDemographicRepo.findAllByKeyInfluencerHandle(userName);
	}

	@Override
	public Optional<TikTokDemographics> getTikTokDemographics(String influencerHandle) {
		return tiktokDemographicRepo.findById(influencerHandle);
	}

	@Override
	public List<TikTokCountryDemographic> getAllTiktokCountryByHandle(String userName) {
		return tiktokCountryDemographicRepo.findAllByKeyInfluencerHandle(userName);
	}

	@Override
	public Optional<CampaignInfluencerMapping> getCampaignInfluencerMapping(String campaignId, String influencerId) {
		return campaignInfluencerMappingRepo
				.findByBrandCampaignDetailsCampaignIdAndInfluencerId(campaignId, influencerId);
	}

	@Override
	public Optional<SocialIdMaster> getSocialIdMasterById(String userId) {
		return socialIdMasterRepo.findById(userId);
	}

	@Override
	public Optional<SocialIdMaster> getSocialIdMasterByHandleAndPlatform(String influencerHandle, Platforms platform) {
		return socialIdMasterRepo.findByInfluencerHandleAndPlatform(influencerHandle, platform);
	}

	@Override
	public Optional<SocialIdMaster> getSocialIdMasterByInfluencerId(String influencerId) {
		return socialIdMasterRepo.findByInfluencerId(influencerId);
	}

	@Override
	public void saveTemporaryInfluencerContent(TemporaryInfluencerContent temporaryInfluencerContent) {
		temporaryInfluencerContentRepo.save(temporaryInfluencerContent);
	}

	@Override
	public Optional<TemporaryInfluencerContent> getTemporaryInfluencerContent(String influencerId, boolean isProfilePic, int contentRank) {
		return temporaryInfluencerContentRepo.findById(new TemporaryInfluencerContentKey(influencerId, isProfilePic, contentRank));
	}

	@Override
	public void saveBrandCreditHistory(BrandCreditHistory brandCreditHistory) {
		brandCreditHistoryRepo.save(brandCreditHistory);
	}

	@Override
	public void saveBrandCredits(BrandCredits brandCredits) {
		brandCreditsRepo.save(brandCredits);
	}

	@Override
	public void saveBrandCreditAllowance(BrandCreditAllowance brandCreditAllowance) {
		brandCreditAllowanceRepo.save(brandCreditAllowance);
	}

	@Override
	public Optional<BrandCredits> getBrandCredits(UUID brandId) {
		return brandCreditsRepo.findById(brandId);
	}

	@Override
	public Optional<BrandDetails> getBrandDetails(UserDetailsKey userDetailsKey) {
		return brandDetailsRepo.findById(userDetailsKey);
	}

	@Override
	public Optional<BrandCreditAllowance> getBrandCreditAllowance(UUID brandId, UUID influencerId, CreditType creditType) {
		return brandCreditAllowanceRepo.findAllByKeyBrandIdAndKeyInfluencerIdAndKeyCreditType(brandId, influencerId, creditType);
	}

	@Override
	public void saveInstagramDemographics(InstagramDemographics instagramDemographics) {
		instagramDemographicRepo.save(instagramDemographics);
	}

	@Override
	public void saveInstagramContents(List<InstagramContent> instagramContent) {
		instagramContentRepo.saveAll(instagramContent);
	}

	@Override
	public void saveYoutubeDemographics(YouTubeDemographics youtubeDemographics) {
		youtubeDemographicRepo.save(youtubeDemographics);
	}

	@Override
	public void saveYoutubeContents(List<YouTubeContent> youtubeContent) {
		youtubeContentRepo.saveAll(youtubeContent);
	}

	@Override
	public void saveTiktokDemographics(TikTokDemographics tiktokDemographics) {
		tiktokDemographicRepo.save(tiktokDemographics);
	}

	@Override
	public void saveTiktokContents(List<TikTokContent> tiktokContent) {
		tiktokContentRepo.saveAll(tiktokContent);
	}

	private void saveReportHistory(SocialIdMaster socialIdMaster, InfluencerDemographicsMapper influencerDemographics, ViewType viewType, String platform) {
		Optional<UserAccounts> userAccounts = getUserAccounts(ResourceServer.getUserId().toString());

		reportsService.saveReportHistory(ReportSearchHistory.builder()
				.key(ReportSearchHistoryKey.builder().brandId(UUID.fromString(userAccounts.get().getUserId())).isNew(false).createdAt(new Date()).build())
				.influencerUsername(influencerDemographics.getInfluencerHandle()).brandName(userAccounts.get().getBrandName())
				.influencerDeepSocialId(StringUtils.isEmpty(socialIdMaster.getInfluencerId()) ? "" : socialIdMaster.getInfluencerId())
				.reportId(StringUtils.isEmpty(socialIdMaster.getInfluencerReportId()) ? "" : socialIdMaster.getInfluencerReportId())
				.viewType(viewType).platform(reportsService.getPlatform(platform)).build());
	}


	@Override
	public ResponseEntity<Object> submitBid(SubmitBidModel submitBidModel) {
		UUID brandId = ResourceServer.getUserId();
		UserDetailsKey brandUserDetailsKey = new UserDetailsKey(UserStatus.APPROVED, brandId);
		Optional<BrandDetails> optionalBrandDetails = getBrandDetails(brandUserDetailsKey);

		if (optionalBrandDetails.isPresent()) {
			UUID userId;
			Optional<SocialIdMaster> socialIdMaster = null;

			if (submitBidModel.getIsValid()) {
				userId = UUID.fromString(submitBidModel.getInfluencerId());
				socialIdMaster = getSocialIdMasterById(userId.toString());
			} else {
				socialIdMaster = getSocialIdMasterByInfluencerId(submitBidModel.getInfluencerId());
				if(socialIdMaster.isPresent())
					userId = UUID.fromString(socialIdMaster.get().getUserId());
				else {
					userId = reportsService.saveNewInfluencer(submitBidModel.getInfluencerId(), submitBidModel.getPlatform(), ViewType.BID);
					socialIdMaster = getSocialIdMasterById(userId.toString());
				}
			}
			if (socialIdMaster.isPresent()) {
				InfluencerDemographicsMapper influencerDemographics = reportsService.getInfluencerDemographics(socialIdMaster.get(),
						submitBidModel.getInfluencerId(), ViewType.BID, true);

				if (influencerDemographics.isSaveReportHistory())
					saveReportHistory(socialIdMaster.get(), influencerDemographics, ViewType.BID, submitBidModel.getPlatform());

				Optional<BrandCreditAllowance> brandCreditAllowance = getBrandCreditAllowance(brandId, userId, CreditType.BID);

				if (ResourceServer.isEnterpriseBrand() || brandCreditAllowance.isPresent()) {
					ResponseEntity<Object> responseEntity = null;
					CampaignDetail addToCampaign = CampaignDetail.builder()
							.campaignId(submitBidModel.getCampaignId())
							.campaignName(submitBidModel.getCampaignName()).build();
					if (addToCampaign.getCampaignId() != null) {
						Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetailsMysql = brandCampaignDetailsMysqlRepo
								.findByCampaignIdAndBrandId(addToCampaign.getCampaignId().toString(),
										brandId.toString());
						if (optionalBrandCampaignDetailsMysql.isPresent()) {
							addToCampaign.setCampaignName(optionalBrandCampaignDetailsMysql.get().getCampaignName());
							Optional<CampaignInfluencerMapping> optionalCampaignInfluencerMapping = getCampaignInfluencerMapping(
									addToCampaign.getCampaignId().toString(), userId.toString());
							if (!optionalCampaignInfluencerMapping.isPresent())
								responseEntity = addInfluencerToCampaign(optionalBrandDetails,
										optionalBrandCampaignDetailsMysql.get(), influencerDemographics);
							else
								responseEntity = ResponseEntity.status(HttpStatus.OK).build();
						} else
							responseEntity = createNewCampaign(optionalBrandDetails, addToCampaign, brandId, influencerDemographics);
					} else
						responseEntity = createNewCampaign(optionalBrandDetails, addToCampaign, brandId, influencerDemographics);
					if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
						BrandBidDetails brandBidDetails = saveBrandBidDetails(brandId, influencerDemographics,
								submitBidModel, optionalBrandDetails.get(), addToCampaign);
						saveBidStatusMapping(brandBidDetails);
						updateBidCount(brandBidDetails);
						submitBidModel.setCampaignId(addToCampaign.getCampaignId());

						Map<String, Object> data = new LinkedHashMap<>();

						if (StringUtils.isEmpty(brandBidDetails.getInfluencerMail())) {
							mailService.sendSubmitBidMailToAdmin(brandBidDetails);
							mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
							data.put("data",
									"Thank you for submitting your bid. "
											+ "A Smartfluence team member will reach out to the Influencer and will get back to you within 24 hours");
							return ResponseEntity.ok(data);
						} else
							return sendBIDMail(submitBidModel, brandId, brandBidDetails, data);
					} else
						return responseEntity;
				}
				return new ResponseEntity<>(submitBidModel, HttpStatus.UNAUTHORIZED);
			}
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.notFound().build();
	}

	private ResponseEntity<Object> sendBIDMail(SubmitBidModel submitBidModel, UUID brandId,
			BrandBidDetails brandBidDetails, Map<String, Object> data) {

		MailDetails mailDetails = MailDetails.builder()
				.mailId(UUID.randomUUID().toString()).brandId(brandId.toString()).brandEmail(brandBidDetails.getBrandEmail())
				.brandName(brandBidDetails.getBrandName()).createdAt(new Date()).influencerEmail(brandBidDetails.getInfluencerMail())
				.mailCc(submitBidModel.getMailCc()).isPromote(false).mailDescription(submitBidModel.getMailDescription())
				.mailSubject(submitBidModel.getMailSubject()).mailTemplateId(submitBidModel.getMailTemplateId().toString())
				.campaignId(submitBidModel.getCampaignId().toString()).influencerHandle(submitBidModel.getInfluencerHandle()).status(1)
				.mappingId(brandBidDetails.getKey().getBidId().toString()).build();

		//mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
		/*if (brandBidDetails.getPaymentType().equals("Product/Service")) {
			mailService.sendSubmitBidMailToInfluencerForProduct(brandBidDetails);
			mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
		} else if (brandBidDetails.getPaymentType().equals("Cash")) {
			mailService.sendSubmitBidMailToInfluencerForCash(brandBidDetails);
			mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
		} else if (brandBidDetails.getPaymentType().equals("Shoutout")) {
			mailService.sendSubmitBidMailToInfluencerForShoutout(brandBidDetails);
			mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
		} else if (brandBidDetails.getPaymentType().equals("Affiliate")) {
			mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
			mailService.sendSubmitBidMailToInfluencerForAffiliate(brandBidDetails);
		}*/

		sendMailUtil.addMailToQueue(mailDetails);
		submitBidMail();

		data.put("data",
				"Thank you for submitting your bid. Please check your email. In case you don't see an email, check your spam folder");
		return ResponseEntity.ok(data);
	}

	@Override
	public ResponseEntity<Object> sendProposal(SubmitBidModel submitBidModel) {
		UUID brandId = ResourceServer.getUserId();
		UserDetailsKey brandUserDetailsKey = new UserDetailsKey(UserStatus.APPROVED, brandId);
		Optional<BrandDetails> optionalBrandDetails = getBrandDetails(brandUserDetailsKey);

		if (optionalBrandDetails.isPresent()) {
			UUID userId;
			Optional<SocialIdMaster> socialIdMaster = null;

			if (submitBidModel.getIsValid()) {
				userId = UUID.fromString(submitBidModel.getInfluencerId());
				socialIdMaster = getSocialIdMasterById(userId.toString());
			} else {
				socialIdMaster = getSocialIdMasterByInfluencerId(submitBidModel.getInfluencerId());
				if(socialIdMaster.isPresent())
					userId = UUID.fromString(socialIdMaster.get().getUserId());
				else {
					userId = reportsService.saveNewInfluencer(submitBidModel.getInfluencerId(), submitBidModel.getPlatform(), ViewType.BID);
					socialIdMaster = getSocialIdMasterById(userId.toString());
				}
			}
			if (socialIdMaster.isPresent()) {
				InfluencerDemographicsMapper influencerDemographics = reportsService.getInfluencerDemographics(socialIdMaster.get(),
						submitBidModel.getInfluencerId(), ViewType.BID, true);

				if (influencerDemographics.isSaveReportHistory())
					saveReportHistory(socialIdMaster.get(), influencerDemographics, ViewType.BID, submitBidModel.getPlatform());

				Optional<BrandCreditAllowance> brandCreditAllowance = getBrandCreditAllowance(brandId, userId, CreditType.BID);

				if (ResourceServer.isEnterpriseBrand() || brandCreditAllowance.isPresent()) {
					ResponseEntity<Object> responseEntity = null;

					Map<String, Object> data = new LinkedHashMap<>();
					Optional<CampaignInfluencerV1Entity> campaignInfluencerDetails = campaignInfluencerRepo.findByCampaignInfluencerId(new CampaignInfluencerKey(submitBidModel.getCampaignId().toString(),
					submitBidModel.getInfluencerId().toString()));
					CampaignInfluencerV1Entity campaignInfluencerEntity = campaignInfluencerDetails.get();
					if (StringUtils.isEmpty(influencerDemographics.getEmail())) {
						mailService.sendProposalMailToAdmin(submitBidModel,optionalBrandDetails.get());//need to change
						mailService.sendProposalConfirmationMailToBrand(submitBidModel,optionalBrandDetails.get());//need to change
						data.put("data",
								"Thank you for submitting your bid. "
										+ "A Smartfluence team member will reach out to the Influencer and will get back to you within 24 hours");		
						campaignInfluencerEntity.setProposalStatus(String.valueOf(ProposalStatus.INVITE_SENT));
						campaignInfluencerRepo.save(campaignInfluencerEntity);
						return ResponseEntity.ok(data);
					} else
						campaignInfluencerEntity.setProposalStatus(String.valueOf(ProposalStatus.INVITE_SENT));
						campaignInfluencerRepo.save(campaignInfluencerEntity);
						return sendProposalMail(submitBidModel, brandId,data,influencerDemographics,optionalBrandDetails.get());//need to change
				}
				return new ResponseEntity<>(submitBidModel, HttpStatus.UNAUTHORIZED);
			}
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.notFound().build();
	}


	private ResponseEntity<Object> sendProposalMail(SubmitBidModel submitBidModel, UUID brandId,
											   Map data, InfluencerDemographicsMapper influencerDemographics ,BrandDetails brandDetails) {
		BrandInfluObj brandInfluObj=new BrandInfluObj();
		brandInfluObj.setCampaignId(submitBidModel.getCampaignId().toString());
		brandInfluObj.setInfluencerId(submitBidModel.getInfluencerId());
		String influencerURL = mailService.getInfluencerURL(brandInfluObj);
		MailDetails mailDetails = MailDetails.builder()
				.mailId(UUID.randomUUID().toString()).brandId(brandId.toString()).brandEmail(brandDetails.getEmail())
				.brandName(brandDetails.getBrandName()).createdAt(new Date()).influencerEmail(influencerDemographics.getEmail())
				.mailCc(submitBidModel.getMailCc()).isPromote(false).mailDescription(submitBidModel.getMailDescription())
				.mailSubject(submitBidModel.getMailSubject()).mailTemplateId(submitBidModel.getMailTemplateId().toString())
				.campaignId(submitBidModel.getCampaignId().toString()).influencerHandle(submitBidModel.getInfluencerHandle()).status(1)
				.influencerInviteLink(influencerURL).isNew(true).build();

		//mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
//		if (  brandBidDetails.getPaymentType().equals("Product/Service")) {
//			mailService.sendSubmitBidMailToInfluencerForProduct(brandBidDetails);
//			mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
//		} else if (brandBidDetails.getPaymentType().equals("Cash")) {
//			mailService.sendSubmitBidMailToInfluencerForCash(brandBidDetails);
//			mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
//		} else if (brandBidDetails.getPaymentType().equals("Shoutout")) {
//			mailService.sendSubmitBidMailToInfluencerForShoutout(brandBidDetails);
//			mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
//		} else if (brandBidDetails.getPaymentType().equals("Affiliate")) {
//			mailService.sendSubmitBidConfirmationMailToBrand(brandBidDetails);
//			mailService.sendSubmitBidMailToInfluencerForAffiliate(brandBidDetails);
//		}

		sendMailUtil.addMailToQueue(mailDetails);
		submitBidMail();

		data.put("data",
				"Thank you for submitting your bid. Please check your email. In case you don't see an email, check your spam folder");
		return ResponseEntity.ok(data);
	}

	private void submitBidMail() {
		Thread thread = new Thread(() -> sendMailUtil.sendMail());
		thread.start();
	}

	private void updateBidCount(BrandBidDetails brandBidDetails) {
		Optional<BrandCampaignDetails> optionalBrandCampaignDetails = brandCampaignDetailsRepo.findByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId
				(brandBidDetails.getKey().getBrandId(), brandBidDetails.getKey().getCampaignId(), brandBidDetails.getKey().getInfluencerId());
		if (optionalBrandCampaignDetails.isPresent()) {
			Optional<CampaignInfluencerMapping> optionalCampaignInfluencerMApping = getCampaignInfluencerMapping(
							brandBidDetails.getKey().getCampaignId().toString(),
							brandBidDetails.getKey().getInfluencerId().toString());
			if (optionalCampaignInfluencerMApping.isPresent()) {
				BrandCampaignDetails brandCampaignDetails = optionalBrandCampaignDetails.get();
				brandCampaignDetails.setBidCount(brandCampaignDetails.getBidCount() + 1);
				brandCampaignDetailsRepo.save(brandCampaignDetails);

				CampaignInfluencerMapping campaignInfluencerMapping = optionalCampaignInfluencerMApping.get();
				campaignInfluencerMapping.setBidCount(brandCampaignDetails.getBidCount());
				campaignInfluencerMappingRepo.save(campaignInfluencerMapping);
			}
		}
	}

	private void saveBidStatusMapping(BrandBidDetails brandBidDetails) {
		BidStatusMapping bidStatusMapping = new BidStatusMapping(brandBidDetails.getKey().getBidId().toString(),
				brandBidDetails.getBidStatus());
		bidStatusMappingRepo.save(bidStatusMapping);
	}

	private BrandBidDetails saveBrandBidDetails(UUID brandId, InfluencerDemographicsMapper influencerDemographics,
			SubmitBidModel submitBidModel, BrandDetails brandDetails, CampaignDetail addToCampaign) {
		UUID bidId = UUID.randomUUID();
		BrandBidDetailsKey brandBidDetailsKey = new BrandBidDetailsKey(brandId, addToCampaign.getCampaignId(),
				influencerDemographics.getUserId(), bidId, reportsService.getPlatform(submitBidModel.getPlatform()));
		BrandBidDetails brandBidDetails = new BrandBidDetails();
		brandBidDetails.setKey(brandBidDetailsKey);
		brandBidDetails.setBidStatus(BidStatus.BIDSENT);
		brandBidDetails.setBidAmount(submitBidModel.getBidAmount());//no bid amount
		brandBidDetails.setBrandEmail(brandDetails.getEmail());
		brandBidDetails.setBrandIndustry(brandDetails.getIndustry());
		brandBidDetails.setBrandInstagramHandle(brandDetails.getInstagramHandle());
		brandBidDetails.setBrandName(brandDetails.getBrandName());
		brandBidDetails.setBrandStatus(brandDetails.getKey().getUserStatus());
		brandBidDetails.setGeneralMessage(submitBidModel.getGeneralMessage());
		brandBidDetails.setIncludedTag(submitBidModel.getIncludedTag());
		brandBidDetails.setCreatedAt(new Date());
		brandBidDetails.setPaymentType(submitBidModel.getPaymentType());
		brandBidDetails.setCampaignName(addToCampaign.getCampaignName());
		brandBidDetails.setAffiliateType(submitBidModel.getAffiliateType());
		brandBidDetails.setInfluencerHandle(influencerDemographics.getInfluencerHandle());
		brandBidDetails.setInfluencerMail(influencerDemographics.getEmail());
		brandBidDetails.setPostDuration(submitBidModel.getPostDuration());
		brandBidDetails.setPostType(submitBidModel.getPostType());

		brandBidDetailsRepo.save(brandBidDetails);
		return brandBidDetails;
	}

	@Override
	public ResponseEntity<Object> addToCampaign(CampaignDetail addToCampaign) {
		UUID brandId = ResourceServer.getUserId();
		Optional<BrandDetails> brandDetails = getBrandDetails(new UserDetailsKey(UserStatus.APPROVED, brandId));
		ResponseEntity<Object> responseEntity = null;
		if (brandDetails.isPresent()) {
			UUID userId;
			Optional<SocialIdMaster> socialIdMaster = null;

			if (addToCampaign.getIsValid()) {
					userId = UUID.fromString(addToCampaign.getInfluencerId());
				socialIdMaster = getSocialIdMasterById(userId.toString());
			} else {
				socialIdMaster = getSocialIdMasterByInfluencerId(addToCampaign.getInfluencerId());
				if(socialIdMaster.isPresent())
					userId = UUID.fromString(socialIdMaster.get().getUserId());
				else {
					userId = reportsService.saveNewInfluencer(addToCampaign.getInfluencerId(), addToCampaign.getPlatform(), ViewType.CAMPAIGN);
					socialIdMaster = getSocialIdMasterById(userId.toString());
				}
			}
			if (socialIdMaster.isPresent()) {
				InfluencerDemographicsMapper influencerDemographics = reportsService.getInfluencerDemographics(socialIdMaster.get(),
						addToCampaign.getInfluencerId(), ViewType.CAMPAIGN, true);

				if (influencerDemographics.isSaveReportHistory())
					saveReportHistory(socialIdMaster.get(), influencerDemographics, ViewType.CAMPAIGN, addToCampaign.getPlatform());

				if (addToCampaign.getCampaignId() != null) {
					Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetailsMysql = brandCampaignDetailsMysqlRepo
							.findByCampaignIdAndBrandId(addToCampaign.getCampaignId().toString(), brandId.toString());

					if (optionalBrandCampaignDetailsMysql.isPresent()) {
						if (ResourceServer.isEnterpriseBrand() || optionalBrandCampaignDetailsMysql.get()
								.getInfluencerCount() < MAX_INFLUENCER_PER_CAMPAIGN) {
							Optional<CampaignInfluencerMapping> optionalCampaignInfluencerMapping = getCampaignInfluencerMapping(
											addToCampaign.getCampaignId().toString(), userId.toString());
							if (!optionalCampaignInfluencerMapping.isPresent())
								responseEntity = addInfluencerToCampaign(brandDetails, optionalBrandCampaignDetailsMysql.get(),
										influencerDemographics);
							else
								responseEntity = ResponseEntity.status(HttpStatus.CONFLICT)
										.body("Influencer already exists in the campaign.");
						} else
							responseEntity = ResponseEntity.status(HttpStatus.CONFLICT).body(
									"Not more than 30 Influencers can be added to a Campaign! Contact us for upgrading to the Enterprise plan.");
					} else
						responseEntity = createNewCampaign(brandDetails, addToCampaign, brandId, influencerDemographics);
				} else
					responseEntity = createNewCampaign(brandDetails, addToCampaign, brandId, influencerDemographics);
				return responseEntity;
			}
			return responseEntity = ResponseEntity.notFound().build();
		}
		return responseEntity = ResponseEntity.notFound().build();
	}


	private ResponseEntity<Object> createNewCampaign(Optional<BrandDetails> brandDetails, CampaignDetail addToCampaign,
			UUID brandId, InfluencerDemographicsMapper influencerDemographics) {
		Long brandCampaignsCount = brandCampaignDetailsMysqlRepo.countByBrandId(brandId.toString());

		if (ResourceServer.isEnterpriseBrand() || brandCampaignsCount < CAMPAIGN_PER_INFLUENCER) {
			Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetails = brandCampaignDetailsMysqlRepo
					.findByCampaignNameAndBrandId(addToCampaign.getCampaignName(), brandId.toString());

			if (optionalBrandCampaignDetails.isPresent())
				return ResponseEntity.status(HttpStatus.CONFLICT).body("This Campaign Name Already Exists");

			UUID campaignId = UUID.randomUUID();
			addToCampaign.setCampaignId(campaignId);

			BrandCampaignDetailsMysql brandCampaignDetailsMysql = new BrandCampaignDetailsMysql();
			saveCampaign(brandCampaignDetailsMysql, brandDetails, addToCampaign, brandId);
			saveBrandCampaignHistory(brandDetails, addToCampaign, brandId);
			return addInfluencerToCampaign(brandDetails, brandCampaignDetailsMysql, influencerDemographics);
		} else
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body("Not more than 3 Campaigns can be created! Contact us for upgrading to the Enterprise plan.");
	}

	private void saveCampaign(BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			Optional<BrandDetails> brandDetails, CampaignDetail addToCampaign, UUID brandId) {

		brandCampaignDetailsMysql.setBrandId(brandId.toString());
		brandCampaignDetailsMysql.setCampaignId(addToCampaign.getCampaignId().toString());
		brandCampaignDetailsMysql.setCampaignName(addToCampaign.getCampaignName());
		brandCampaignDetailsMysql.setCampaignStatus(CampaignStatus.NEW);
		brandCampaignDetailsMysql.setCreatedAt(new Date());
		brandCampaignDetailsMysql.setInfluencerCount(0);
		brandCampaignDetailsMysql.setModifiedAt(null);
		brandCampaignDetailsMysql.setIsPromote(false);
		brandCampaignDetailsMysqlRepo.save(brandCampaignDetailsMysql);
	}

	private void saveBrandCampaignHistory(Optional<BrandDetails> optionalBrandDetails, CampaignDetail addToCampaign,
			UUID brandId) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		String date = format.format(new Date());
		BrandDetails brandDetails = optionalBrandDetails.get();
		brandCampaignHistoryRepo.save(BrandCampaignHistory.builder().brandName(brandDetails.getBrandName())
				.campaignName(addToCampaign.getCampaignName()).createdAt(new Date())
				.key(new BrandCampaignHistoryKey(brandId, date, addToCampaign.getCampaignId(), new Date())).build());
	}

	private ResponseEntity<Object> addInfluencerToCampaign(Optional<BrandDetails> brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			InfluencerDemographicsMapper influencerDemographics) {

		processInfluencerToCampaign(brandDetails, brandCampaignDetailsMysql, influencerDemographics);

		return ResponseEntity.ok().build();
	}

	@Override
	public void processInfluencerToCampaign(Optional<BrandDetails> brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			InfluencerDemographicsMapper influencerDemographics) {
		BrandCampaignDetails brandCampaignDetails = saveToBrandCampaignDetails(brandDetails, brandCampaignDetailsMysql,
				influencerDemographics);
				saveToBrandCampaignInfluencerHistory(brandCampaignDetails);
		saveToCampaignInfluencerMapping(brandCampaignDetailsMysql, influencerDemographics.getUserId());

				updateCampaignMaster(brandCampaignDetails);
	}

	private BrandCampaignDetails saveToBrandCampaignDetails(Optional<BrandDetails> brandDetails,
			BrandCampaignDetailsMysql brandCampaignDetailsMysql, InfluencerDemographicsMapper influencerDemographics) {

		BrandCampaignDetails brandCampaignDetails = BrandCampaignDetails.builder()
				.brandName(brandDetails.get().getBrandName()).campaignName(brandCampaignDetailsMysql.getCampaignName())
				.createdAt(new Date())
				.influencerAudienceCredibilty(influencerDemographics.getCredibility().doubleValue())
				.influencerEngagement(influencerDemographics.getEngagement())
				.influencerFollowers(influencerDemographics.getFollowers())
				.influencerHandle(influencerDemographics.getInfluencerHandle()).bidCount(0)
				.key(BrandCampaignDetailsKey.builder().brandId(brandDetails.get().getKey().getUserId())
						.campaignId(UUID.fromString(brandCampaignDetailsMysql.getCampaignId()))
						.influencerId(influencerDemographics.getUserId()).build())
				.build();
		brandCampaignDetailsRepo.save(brandCampaignDetails);
		return brandCampaignDetails;
	}

	private void saveToBrandCampaignInfluencerHistory(BrandCampaignDetails brandCampaignDetails) {
		brandCampaignInfluencerHistoryRepo.save(BrandCampaignInfluencerHistory.builder()
				.brandName(brandCampaignDetails.getBrandName()).campaignName(brandCampaignDetails.getCampaignName())
				.createdAt(new Date())
				.influencerAudienceCredibilty(brandCampaignDetails.getInfluencerAudienceCredibilty())
				.influencerEngagement(brandCampaignDetails.getInfluencerEngagement())
				.influencerFollowers(brandCampaignDetails.getInfluencerFollowers())
				.influencerHandle(brandCampaignDetails.getInfluencerHandle())
				.key(BrandCampaignInfluencerHistoryKey.builder().brandId(brandCampaignDetails.getKey().getBrandId())
						.campaignId(brandCampaignDetails.getKey().getCampaignId())
						.influencerId(brandCampaignDetails.getKey().getInfluencerId()).build())
				.build());
	}

	private void saveToCampaignInfluencerMapping(BrandCampaignDetailsMysql brandCampaignDetailsMysql, UUID influencerId) {
		campaignInfluencerMappingRepo.save(CampaignInfluencerMapping.builder().brandCampaignDetails(brandCampaignDetailsMysql)
				.influencerId(influencerId.toString())
				.bidCount(0).build());
	}

	private void updateCampaignMaster(BrandCampaignDetails brandCampaignDetails) {
		Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetailsMysql = brandCampaignDetailsMysqlRepo
				.findByCampaignIdAndBrandId(brandCampaignDetails.getKey().getCampaignId().toString(),
						brandCampaignDetails.getKey().getBrandId().toString());

		if (optionalBrandCampaignDetailsMysql.isPresent()) {
			BrandCampaignDetailsMysql brandCampaignDetailsMysql = optionalBrandCampaignDetailsMysql.get();
			brandCampaignDetailsMysql.setInfluencerCount(brandCampaignDetailsMysql.getInfluencerCount() + 1);
			brandCampaignDetailsMysql.setCampaignStatus(CampaignStatus.INPROGRESS);
			brandCampaignDetailsMysqlRepo.save(brandCampaignDetailsMysql);
		}
	}

	@Override
	public ResponseEntity<Object> getCampaignByInfluencer() {
		UUID brandId = ResourceServer.getUserId();
		Set<BrandCampaigns> brandCampaigns = new HashSet<>();
		List<BrandCampaignDetailsMysql> brandCampaignDetails = brandCampaignDetailsMysqlRepo
				.findAllByBrandIdAndIsPromoteAndCampaignStatusIn(brandId.toString(), false, Sets.newHashSet(CampaignStatus.NEW,
						CampaignStatus.INPROGRESS, CampaignStatus.PAUSED));

		if (!brandCampaignDetails.isEmpty()) {
			for (BrandCampaignDetailsMysql brandCampaignDetail : brandCampaignDetails) {

				brandCampaigns.add(BrandCampaigns.builder().brandId(brandId)
						.campaignId(UUID.fromString(brandCampaignDetail.getCampaignId()))
						.campaignName(brandCampaignDetail.getCampaignName())
						.campaignStatus(brandCampaignDetail.getCampaignStatus())
						.influencerCount(brandCampaignDetail.getInfluencerCount()).build());
			}
			return ResponseEntity.ok(brandCampaigns);
		}
		return ResponseEntity.ok(brandCampaigns);
	}

	@Override
	public void truncateTemporaryContentTable() {
		temporaryInfluencerContentRepo.deleteAll();
	}

	@Override
	public List<MailTemplates> getMailTemplates() {
		UUID brandId = ResourceServer.getUserId();
		List<MailTemplates> mailTemplates = new LinkedList<>();
		Iterable<MailTemplateMaster> mailTemplateMaster = mailTemplateMasterRepo.findAllByIsHidden(false);
		List<BrandMailTemplates> brandMailTemplates = brandMailTemplatesRepo.findByKeyBrandId(brandId);
		for (MailTemplateMaster mail : mailTemplateMaster) {
			mailTemplates.add(MailTemplates.builder().id(String.valueOf(mail.getId())).isUserCreated(false)
					.mailBody(mail.getMailBody()).mailSubject(mail.getMailSubject()).paymentType(mail.getPaymentType())
					.subPaymentType(mail.getSubPaymentType()).templateName(mail.getTemplateName()).isNew(mail.getIsNew()).build());
		}
		for (BrandMailTemplates brandMailTemplate : brandMailTemplates) {
			mailTemplates.add(MailTemplates.builder().id(String.valueOf(brandMailTemplate.getTemplateId().toString()))
					.isUserCreated(true).mailBody(brandMailTemplate.getTemplateBody())
					.mailSubject(brandMailTemplate.getTemplateSubject())
					.templateName(brandMailTemplate.getKey().getTemplateName()).build());
		}
		return mailTemplates;
	}

	@Override
	public Optional<LocationMaster> getLocationByName(String name) {
		return locationMasterRepo.findByLocation(name);
	}

	@Override
	public List<LocationMaster> getLocationUtilities(String name) {
		List<LocationMaster> locationMaster = locationMasterRepo.findAllByLocationIgnoreCaseContainingAndStatus(name, 1);
		locationMaster.sort((LocationMaster a, LocationMaster b) -> a.getLocation().compareTo(b.getLocation()));

		return locationMaster;
	}

	@Override
	public Optional<IndustryMaster> getIndustryByName(String name) {
		return industryMasterRepo.findByIndustry(name);
	}

	@Override
	public List<IndustryMaster> getIndustryUtilities(String name) {
		List<IndustryMaster> industryMaster = industryMasterRepo.findAllByIndustryIgnoreCaseContainingAndStatus(name, 1);
		industryMaster.sort((IndustryMaster a, IndustryMaster b) -> a.getIndustry().compareTo(b.getIndustry()));


		return industryMaster;
	}

	@Override
	public Optional<BrandSearchHistory> getBrandSearchHistoryById(BrandSearchHistoryKey key) {
		return brandSearchHistoryRepo.findById(key);
	}

	@Override
	public void saveBrandSearchHistory(BrandSearchHistory brandSearchHistory) {
		brandSearchHistoryRepo.save(brandSearchHistory);
	}

	@Override
	public UUID saveBrandSearchHistory(UUID brandId, GetInfluencersRequestModel request, InfluencersDataResponse body) {
		UserAccounts userAccounts = getUserAccounts(brandId.toString()).get();
		BrandSearchHistory brandSearchHistory = BrandSearchHistory.builder().key(BrandSearchHistoryKey.builder().brandId(brandId)
				.searchId(UUID.randomUUID()).build()).brandName(userAccounts.getBrandName()).platform(reportsService.getPlatform(request.getPlatform()))
				.keyWords(request.getKeyword()).comparisonProfile(request.getComparisonProfile()).audienceIndustry(request.getAudienceBrandName())
				.audienceGender(request.getAudienceGender()).audienceLocation(request.getAudienceGeoName()).isVerified(request.getVerified())
				.audienceAge(ObjectUtils.isEmpty(request.getAudienceAge()) ? "" : request.getAudienceAge().toString()).bioText(request.getProfileBio())
				.followersRange(request.getFollowersMin() + "-" + request.getFollowersMax()).sponsoredPosts(request.getSponsoredPost())
				.engagementsRange(request.getEngagementMin() + "-" + request.getEngagementMax()).withContact(request.getWithContact())
				.influencerIndustry(StringUtils.isEmpty(request.getInfluencerBrandName()) ? "" : request.getInfluencerBrandName())
				.influencerLocation(request.getInfluencerGeoName()).influencerAge(request.getInfluencerAge()).createdAt(new Date())
				.influencerGender(request.getInfluencerGender()).totalResults(body.getTotal()).sortBy(request.getSortBy())
				.isInfluencerMapped(false).build();
		brandSearchHistoryRepo.save(brandSearchHistory);

		return brandSearchHistory.getKey().getSearchId();
	}
}