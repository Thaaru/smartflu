package io.smartfluence.constants;

public enum CampaignStatus {
	NEW, INPROGRESS, CLOSED ,ACTIVE ,INACTIVE,PAUSED
}