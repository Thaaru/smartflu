package io.smartfluence.brand.management.entityv1;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "brand_campaign_details_v1")
public class BrandCampaignDetailsV1Entity {

    @Id
//    @Column(name = "campaign_id")
    private String campaignId;
    @Column(name = "brand_id")
    private String brandId;
    @Column(name = "brand_description")
    private String brandDescription;
    @Column(name = "campaign_name")
    private String campaignName;
    @Column(name = "campaign_description")
    private String campaignDescription;
    @Column(name = "hashtags")
    private String hashTags;
    @Column(name = "max_product_count")
    private int maxProductCount;
    @Column(name = "restrictions")
    private String restrictions;
    @Column(name = "terms_id")
    private long termsId;

    @Column(name = " campaign_end_date")
    private LocalDateTime campaignEndDate;
    @Column(name = "campaign_status")
    private String campaignStatus;
    @Column(name = "influencer_count")
    private int influencerCount;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;

}
