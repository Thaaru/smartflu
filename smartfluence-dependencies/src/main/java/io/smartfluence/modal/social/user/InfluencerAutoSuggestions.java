package io.smartfluence.modal.social.user;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InfluencerAutoSuggestions implements Serializable {

	private static final long serialVersionUID = -5350794301703598027L;

	@JsonAlias("data")
	private List<InfluencerAutoSuggestionsData> data;
}