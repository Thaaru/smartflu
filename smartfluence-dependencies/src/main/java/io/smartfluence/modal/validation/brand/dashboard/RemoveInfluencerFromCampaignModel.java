package io.smartfluence.modal.validation.brand.dashboard;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class RemoveInfluencerFromCampaignModel {

	@NotNull(message = "Enter Influencer Id")
	private UUID influencerId;
}