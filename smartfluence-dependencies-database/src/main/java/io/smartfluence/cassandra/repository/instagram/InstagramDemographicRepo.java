package io.smartfluence.cassandra.repository.instagram;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;

@Repository
public interface InstagramDemographicRepo extends CassandraRepository<InstagramDemographics, String> {

}