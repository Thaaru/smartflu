package io.smartfluence.brand.management.entityv1;

import io.smartfluence.brand.management.util.JpaConvertorJson;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class WorkflowMaster {
    @Id
    public Long sno;
    public String status;


}
