package io.smartfluence.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("influencer")
public class InfluencerView {

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@GetMapping("/pub/campaign_details")
	public String influencerConnectView(Model model, Principal principal, HttpServletRequest header) {
		return "Influencer/Index";
	}

	@GetMapping("/home")
	public String influencerHomeConnectView(Model model, Principal principal, HttpServletRequest header) {
		return "Influencer/Index";
	}

	@GetMapping("/influencerlogin")
	public String influencerLoginConnectView(Model model, Principal principal, HttpServletRequest header) {
		return "Influencer/Index";
	}
}
