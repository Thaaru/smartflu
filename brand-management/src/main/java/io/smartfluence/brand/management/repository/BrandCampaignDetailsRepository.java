package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.BrandCampaignDetailsNew;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandCampaignDetailsRepository extends JpaRepository<BrandCampaignDetailsNew, String> {
    List<BrandCampaignDetailsNew> findByBrandId(String brandId);
    BrandCampaignDetailsNew findByCampaignId(String campaignId);
}
