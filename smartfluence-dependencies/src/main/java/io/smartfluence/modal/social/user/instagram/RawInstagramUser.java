package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawInstagramUser implements Serializable {

	private static final long serialVersionUID = 626159211071618768L;

	@JsonAlias("pk")
	private Long pk;

	@JsonAlias("username")
	private String userName;

	@JsonAlias("full_name")
	private String fullName;

	@JsonAlias("profile_pic_url")
	private String profilePicUrl;

	@JsonAlias("profile_pic_url_hd")
	private String profilePicHd;

	@JsonAlias("is_business")
	private boolean isBusiness;

	@JsonAlias("is_private")
	private boolean isPrivate;

	@JsonAlias("is_verified")
	private boolean isVerified;

	@JsonAlias("media_count")
	private Integer mediaCount;

	@JsonAlias("follower_count")
	private Integer followerCount;

	@JsonAlias("following_count")
	private Integer followingCount;

	@JsonAlias("biography")
	private String biography;

	@JsonAlias("external_url")
	private String externalUrl;

	@JsonAlias("has_highlight_reels")
	private boolean hasHighlightReels;

	@JsonAlias("category")
	private String category;
}