package io.smartfluence.stripe;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stripe.Stripe;
import com.stripe.exception.CardException;
import com.stripe.exception.StripeException;
import com.stripe.model.Card;
import com.stripe.model.Customer;
import com.stripe.model.CustomerCollection;
import com.stripe.model.ExternalAccount;
import com.stripe.model.ExternalAccountCollection;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceCollection;
import com.stripe.model.Plan;
import com.stripe.model.PlanCollection;
import com.stripe.model.Refund;
import com.stripe.model.Source;
import com.stripe.model.Subscription;
import com.stripe.model.SubscriptionItem;

import io.smartfluence.cassandra.entities.StripeCustomer;
import io.smartfluence.cassandra.entities.StripeSubscriptionHistory;
import io.smartfluence.cassandra.entities.primary.StripeCustomerKey;
import io.smartfluence.cassandra.entities.primary.StripeSubscriptionHistoryKey;
import io.smartfluence.cassandra.repository.StripeCustomerRepo;
import io.smartfluence.cassandra.repository.StripeSubscriptionHistoryRepo;
import io.smartfluence.constants.SubscriptionStatus;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.mysql.entities.StripeSubscriptionMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.StripeSubscriptionMasterRepo;
import io.smartfluence.mysql.repository.UserAccountsRepo;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class StripeManager {

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private StripeCustomerRepo stripeCustomerRepo;

	@Autowired
	private StripeSubscriptionMasterRepo stripeSubscriptionMasterRepo;

	@Autowired
	private StripeSubscriptionHistoryRepo stripeSubscriptionHistoryRepo;

	@Autowired
	private void setStripe(StripeConfiguration stripeConfiguration) {
		Stripe.apiKey = stripeConfiguration.getSecretKey();
	}

	public PlanCollection plans() {
		try {
			return Plan.list(null);
		} catch (StripeException e) {
			log.error("Plans Exception: {}", e);
			return null;
		}
	}

	public ExternalAccountCollection cards(UUID userId) {
		Customer customer = null;
		try {
			customer = getCustomer(userId);
			if (customer != null) {
				Map<String, Object> cardParams = new HashMap<String, Object>();
				cardParams.put("type", "card");
				return customer.getSources().list(cardParams);
			}
		} catch (StripeException e) {
			log.error("Cards Exception for user {}:{}: {}", userId, (customer == null ? customer : customer.getId()), e);
		}
		return null;
	}

	public Customer getCustomer(UUID userId) {
		Optional<UserAccounts> userOpt = userAccountsRepo.findById(userId.toString());

		if (userOpt.isPresent()) {
			UserAccounts userAccounts = userOpt.get();
			userAccounts.getEmail();
			Map<String, Object> params = new HashMap<>();
			params.put("email", userAccounts.getEmail());
			params.put("limit", 1);

			try {
				CustomerCollection customerCollection = Customer.list(params);
				if (!customerCollection.getData().isEmpty())
					return customerCollection.getData().get(0);
				else {
					params.remove("limit");
					Map<String, Object> metadata = new HashMap<>();
					metadata.put("userId", userId.toString());
					metadata.put("userType", userAccounts.getUserType().name());
					params.put("metadata", metadata);
					params.put("description", "Created on Smartfluence Platform");
					Customer customer = Customer.create(params);
					saveStripeCustomer(customer, userAccounts);
					return customer;
				}
			} catch (StripeException e) {
				log.error("Error while retrieving customer {} {}", userId, userAccounts.getEmail());
				throw new RuntimeException(e);
			}
		}
		return null;
	}

	private void saveStripeCustomer(Customer customer, UserAccounts userAccounts) {
		stripeCustomerRepo.save(StripeCustomer
				.builder().createdAt(new Date()).customerId(customer.getId()).key(StripeCustomerKey.builder()
						.userId(UUID.fromString(userAccounts.getUserId())).userType(userAccounts.getUserType()).build())
				.build());
	}

	public ExternalAccount addCard(UUID userId, String stripeToken, Boolean defaultSource) throws StripeException {
		Customer customer = getCustomer(userId);
		Source card = Source.retrieve(stripeToken);
		log.info(card);
		String fingerPrint = card.getTypeData().get("fingerprint");
		ExternalAccount externalAccount = null;

		for (ExternalAccount account : customer.getSources().list(null).autoPagingIterable()) {
			if (checkExistingCard(fingerPrint, account)) {
				account.getMetadata().put("exists", "true");
				return account;
			}
		}
		Map<String, Object> sourceParam = new HashMap<>();
		sourceParam.put("source", stripeToken);
		ExternalAccountCollection sources = customer.getSources();
		externalAccount = sources.create(sourceParam);

		if (defaultSource || sources.getData().isEmpty())
			changeDefaultSource(customer, externalAccount.getId());
		return externalAccount;
	}

	private boolean checkExistingCard(String fingerPrint, ExternalAccount account) {
		if (account instanceof Card) {
			Card cardAccount = (Card) account;

			if (fingerPrint.equals(cardAccount.getFingerprint())) {
				return true;
			}
		}
		if (account instanceof Source) {
			Source source = (Source) account;

			if (source.getTypeData().containsKey("fingerprint")
					&& source.getTypeData().get("fingerprint").equals(fingerPrint)) {
				return true;
			}
		}
		return false;
	}

	private Customer changeDefaultSource(Customer customer, String sourceId) throws StripeException {
		try {
			Map<String, Object> customerParam = new HashMap<>();
			customerParam.put("default_source", sourceId);
			return customer.update(customerParam);
		} catch (StripeException e) {
			log.error("Error changing default source {} for {}", sourceId, customer.getId());
			throw e;
		}
	}

	public String getDefaultSource(UUID userId) {
		Customer customer = getCustomer(userId);
		return customer.getDefaultSource();
	}

	public ExternalAccount removeCard(UUID userId, String cardId) throws StripeException {
		Customer customer = getCustomer(userId);

		if (customer.getSources().getTotalCount() > 1) {
			Source source = Source.retrieve(cardId);
			Subscription subscription = activeSubscription(userId);

			if (subscription != null && customer.getDefaultSource().equals(cardId))
				throw new CardException("Unsubscribe or cancel subscription to remove the default card", null, null, null, null, null, null, null);

			for (ExternalAccount account : customer.getSources().autoPagingIterable()) {
				if (!account.getId().equals(cardId)) {
					changeDefaultSource(customer, account.getId());
					break;
				}
			}
			return source.detach();
		}
		return null;
	}

	public Customer defaultCard(UUID userId, String cardId) throws StripeException {
		Customer customer = getCustomer(userId);
		return changeDefaultSource(customer, cardId);
	}

	public boolean hasSource(UUID userId) {
		Customer customer = getCustomer(userId);
		return !customer.getSources().getData().isEmpty();
	}

	public Subscription activeSubscription(UUID userId) {
		Customer customer = getCustomer(userId);

		if (customer.getSubscriptions().getData().isEmpty())
			return null;
		else
			return customer.getSubscriptions().getData().get(0);
	}

	public Subscription changeSubscription(UUID userId, String planId) throws StripeException {
		Customer customer = getCustomer(userId);

		if (customer.getSubscriptions().getData().isEmpty()) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("plan", planId);
			Map<String, Object> items = new HashMap<String, Object>();
			items.put("0", item);
			Map<String, Object> subParams = new HashMap<>();
			subParams.put("customer", customer.getId());
			subParams.put("items", items);
			Subscription subscription = Subscription.create(subParams);
			saveSubscription(subscription, false, userId);
			return subscription;
		} else {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("plan", planId);
			Subscription subscription = customer.getSubscriptions().getData().get(0);
			item.put("id", subscription.getSubscriptionItems().getData().get(0).getId());
			Map<String, Object> items = new HashMap<String, Object>();
			items.put("0", item);
			Map<String, Object> subParams = new HashMap<>();
			subParams.put("cancel_at_period_end", false);
			subParams.put("items", items);
			Subscription newSubscription = subscription.update(subParams);
			saveSubscription(newSubscription, true, userId);
			return newSubscription;
		}
	}

	private void saveSubscription(Subscription subscription, boolean deleteNeeded, UUID userId) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		String date = format.format(new Date());

		if (deleteNeeded) {
			Optional<StripeSubscriptionMaster> optionalSubscriptionMaster = stripeSubscriptionMasterRepo
					.findByCustomerId(subscription.getCustomer());
			if (optionalSubscriptionMaster.isPresent())
				stripeSubscriptionMasterRepo.delete(optionalSubscriptionMaster.get());
		}

		Optional<UserAccounts> optionalUserAccounts = userAccountsRepo.findById(userId.toString());
		UserAccounts userAccounts = optionalUserAccounts.get();
		SubscriptionItem subscriptionItem;

		if (subscription.getSubscriptionItems().getData().isEmpty())
			subscriptionItem = null;
		else
			subscriptionItem = subscription.getSubscriptionItems().getData().get(0);

		stripeSubscriptionMasterRepo.save(StripeSubscriptionMaster.builder().customerId(subscription.getCustomer())
				.endDate(new Date(subscription.getCurrentPeriodEnd() * 1000)).itemId(subscriptionItem.getId())
				.planId(subscriptionItem.getPlan().getId()).productId(subscriptionItem.getPlan().getProduct())
				.startDate(new Date(subscription.getCurrentPeriodStart() * 1000)).subscriptionId(subscription.getId())
				.userAccounts(userAccounts).build());

		Optional<StripeSubscriptionHistory> optionalStripeSubscriptionHistory = stripeSubscriptionHistoryRepo
				.findOneByKeyUserTypeAndKeyUserIdAndKeySubscriptionIdAndKeySubscriptionItemId(
						userAccounts.getUserType(), UUID.fromString(userAccounts.getUserId()), subscription.getId(),
						subscriptionItem.getId());

		if (optionalStripeSubscriptionHistory.isPresent()) {
			StripeSubscriptionHistory stripeSubscriptionHistory = optionalStripeSubscriptionHistory.get();
			stripeSubscriptionHistory.getKey().setModifiedAt(new Date());
			stripeSubscriptionHistory.setStatus(SubscriptionStatus.INACTIVE);
			stripeSubscriptionHistoryRepo.save(stripeSubscriptionHistory);
		}

		stripeSubscriptionHistoryRepo.save(StripeSubscriptionHistory.builder().brandName(userAccounts.getBrandName())
				.createdAt(new Date()).credits(subscription.getPlan().getTransformUsage().getDivideBy().intValue())
				.key(new StripeSubscriptionHistoryKey(userAccounts.getUserType(), userId, subscription.getId(),
						subscriptionItem.getId(), date, new Date()))
				.status(SubscriptionStatus.ACTIVE).subscriptionName(subscription.getPlan().getNickname())
				.renewable(true).resetCredit(true)
				.subscriptionDate(new Date(subscription.getCurrentPeriodStart() * 1000))
				.subscriptionType(SubscriptionType.STRIPE)
				.validity(subscription.getPlan().getIntervalCount().intValue())
				.validityType(subscription.getPlan().getInterval().toUpperCase()).build());
	}

	public Subscription cancelActiveSubscription(UUID userId, boolean isRefund) throws StripeException {
		Customer customer = getCustomer(userId);

		if (customer.getSubscriptions().getData().isEmpty())
			return null;
		else {
			Map<String, Object> params = new HashMap<>();
			Subscription subscription = customer.getSubscriptions().getData().get(0);

			if (isRefund) {
				Map<String, Object> invoiceParam = new HashMap<>();
				invoiceParam.put("subscription", subscription.getId());
				InvoiceCollection invoiceList = Invoice.list(invoiceParam);

				if (!invoiceList.getData().isEmpty()) {
					Map<String, Object> refundParam = new HashMap<>();
					refundParam.put("charge", invoiceList.getData().get(0).getCharge());
					Refund.create(refundParam);
				}
				subscription.cancel(params);
			} else {
				params.put("cancel_at_period_end", true);
				subscription.update(params);
			}
			return subscription;
		}
	}
}