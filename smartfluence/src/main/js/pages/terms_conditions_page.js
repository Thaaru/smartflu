import React, { useState } from "react";
import { toast } from "react-toastify";
import { ArrowButton } from "../components/arrow_button";
import { HeaderComponent } from "../components/header_component";
import ColoredLine from "../components/line_component";
import { loaderMethod } from "../components/loader";
import { PoweredBy } from "../components/powered_by";
import progressStatus from "../components/progressbar_status";
import { CProgressBar } from "../components/progress_bar";
import { updateCampaignDetailsApi } from "../services/api/campaign_details_api";
import JSONPretty from 'react-json-pretty';
import { ACCEPTED_MESSAGE, ERROR_MESSAGE, PROPOSAL_STATUS, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";

export default function TermsConditionPage({ data,  back, close, next }) {

    const termsData = data.termsConditionDetails;

 const [loaderComponent, setLoaderComponent] = useState(true);
    const loaderOpen = (status) => {
        if (status == true) {
            setLoaderComponent(status);
        } else {
            setLoaderComponent(status)
        }
    }
    const [submitStatus, setSubmitStatus] = useState(false);
    const submitStatusHandler = (event) => {
        if (event.target.checked == true) {
            data.termsConditionStatus = STEPS_ACCEPTED_STATUS;
            progressStatus(data);
        } else {
            data.termsConditionStatus = null;
            progressStatus(data);
        }
        setSubmitStatus(event.target.checked);
    }
    const submitHandler = async (data, loader) => {
        data.termsConditionStatus = STEPS_ACCEPTED_STATUS;
        data.proposalStatus = PROPOSAL_STATUS.INFLUENCER_ACCEPTED;
        loader(false);
        const result = await updateCampaignDetailsApi(data);
        if (result.code == 600) {
            toast.success(ACCEPTED_MESSAGE, { delay: 1000 })
            loader(true);
            const data = result.value;
            next(data);

        } else {
            toast.error(ERROR_MESSAGE, { delay: 1000 })
            loader(true);
            data.termsConditionStatus = null;
            data.proposalStatus = null;
            return null;
        }
    }

    return (
        <>
            <div class='content-wrapper'>
                <div>
                    <HeaderComponent data={data} close={close} headerName={"Terms & Conditions"} stepNo={6} />
                    <ColoredLine color={'secondaryColor'} />
                    <div class="col" style={{ "textAlign": "left" }}>
                        <div className="col-md">
                            <div className="termsConditions">
                                <div class="normalText">
                                    {termsData.type == "CUSTOM" ? `Download and view terms and conditions`
                                        : `You must agree to these content rights to participate.`}
                                </div>
                                {
                                    termsData.type == "CUSTOM" ?
                                        <div className="col-md">
                                            <button type="button" className="btn btn-primary btn-lg btn-block" style={{ "backgroundColor": "#2F008D" }} onClick={() => {window.open(termsData.termsConditionFilePath)}}>Click here to download</button>
                                        </div>
                                        : <div>
                                           <JSONPretty className="normalText"  data={termsData.termsCondition}></JSONPretty>                                         
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                    <div class="normalText">
                        <div class=" custom-control custom-checkbox my-2 mr-sm-4">
                            <input type="checkbox" class="custom-control-input" id="customControlInline" onChange={submitStatusHandler} />
                            <label class="custom-control-label" for="customControlInline">I accept these terms {`&`} conditions</label>
                        </div>
                    </div>
                    <div className="footer">
                        <ColoredLine color={'secondaryColor'} />
                        <div className="row justify-content-around" style={{ justifyContent: 'center', alignItems: 'center', }}>
                        <ArrowButton status={STEPS_ACCEPTED_STATUS} navigator="backward" buttonHandler={back}/>
                            <div class="col-6 ">
                                <button type="button" class="btn btn-outline-secondary acceptButton" disabled={data.paymentAcceptedStatus==STEPS_ACCEPTED_STATUS? submitStatus ? false : true : false} style={{ borderWidth: "2px", "borderColor":data.paymentAcceptedStatus==STEPS_ACCEPTED_STATUS? submitStatus ? "#2F008D" : "#C4C4C4":"#C4C4C4", color:data.paymentAcceptedStatus==STEPS_ACCEPTED_STATUS? submitStatus ? "#2F008D" : "white":"white", "backgroundColor":data.paymentAcceptedStatus==STEPS_ACCEPTED_STATUS? submitStatus ? null : "#C4C4C4":"#C4C4C4" }} onClick={() => {data.paymentAcceptedStatus==STEPS_ACCEPTED_STATUS? submitStatus ? submitHandler(data, loaderOpen) : null:null; }}>
                                    {loaderComponent == true ? "Submit Proposal" : loaderMethod()}
                                </button>
                            </div>
                        </div>
                        <CProgressBar data={data}/>
                        <PoweredBy/>
                    </div>
                </div>
            </div>
        </>
    );
}