package io.smartfluence.brand.management.constants;

public enum IsOfferAccepted {
    TRUE, FALSE
}
