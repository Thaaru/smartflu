package io.smartfluence.constants;

public enum DeepSocialDictionaryType {
	search(1);

	private int numVal;

	DeepSocialDictionaryType(int numVal) {
		this.numVal = numVal;
	}

	public int getNumVal() {
		return numVal;
	}
}