package io.smartfluence.modal.social.report;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportData implements Serializable {

	private static final long serialVersionUID = 7419854143750992278L;

	private boolean success;

	@JsonAlias("report_info")
	private ReportInfo reportInfo;

	@JsonAlias("user_profile")
	private UserProfile userProfile;

	@JsonAlias("audience_followers")
	private AudienceFollowers audienceFollowers;
}