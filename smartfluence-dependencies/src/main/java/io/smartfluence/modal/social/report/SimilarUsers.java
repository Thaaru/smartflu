package io.smartfluence.modal.social.report;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimilarUsers implements Serializable {

	private static final long serialVersionUID = -2806059706522278201L;

	@JsonAlias("user_id")
	private String userId;

	@JsonAlias("picture")
	private String profileImage;

	@JsonAlias("username")
	private String handle;

	@JsonAlias("followers")
	private Double followers;

	@JsonAlias("fullname")
	private String fullName;

	@JsonAlias("url")
	private String url;

	@JsonAlias("is_verified")
	private Boolean isVerified;

	@JsonAlias("engagements")
	private Double engagement;

	@JsonAlias("avg_likes")
	private Double avgLikes;

	@JsonAlias("avg_views")
	private Double avgViews;
}