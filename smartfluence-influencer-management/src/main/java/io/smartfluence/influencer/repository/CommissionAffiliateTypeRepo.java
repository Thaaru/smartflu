package io.smartfluence.influencer.repository;

import io.smartfluence.influencer.entities.CommissionAffiliateTypeMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommissionAffiliateTypeRepo extends JpaRepository<CommissionAffiliateTypeMasterEntity,Long> {
    CommissionAffiliateTypeMasterEntity findBycommAffiTypeId(Long commAffiTypeId);
}
