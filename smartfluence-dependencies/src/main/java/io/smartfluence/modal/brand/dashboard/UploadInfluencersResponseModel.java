package io.smartfluence.modal.brand.dashboard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UploadInfluencersResponseModel {

	private String uploadStatus;

	private String influencersNotAdded;

	private String influencersAdded;

	private String influencersNotFound;

	private String influencersPulling;

	private String responseMessage;
}