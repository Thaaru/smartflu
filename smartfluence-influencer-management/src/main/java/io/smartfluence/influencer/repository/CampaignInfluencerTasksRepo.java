package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.influencer.entities.CampaignInfluencerTaskId;
import io.smartfluence.influencer.entities.CampaignInfluencerTask;

import java.util.List;

@Repository
public interface CampaignInfluencerTasksRepo extends JpaRepository<CampaignInfluencerTask, CampaignInfluencerTaskId>{
    List<CampaignInfluencerTask> findByCitIdCampaignIdAndCitIdInfluencerId(String campaignId,String influencerId);
}
