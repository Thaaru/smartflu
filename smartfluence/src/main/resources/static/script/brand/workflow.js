$(document).ready(function (event) {
    var paramsString = window.location.search;
    var searchParams = new URLSearchParams(paramsString);
    if (searchParams.has("tabId")) {
        var selectedTab = document.querySelector(`#${searchParams.get("tabId")}`);
        var tab = new bootstrap.Tab(selectedTab);
        tab.show();
    }
});