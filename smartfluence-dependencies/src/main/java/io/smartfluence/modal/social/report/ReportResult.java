package io.smartfluence.modal.social.report;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportResult {

	private boolean is_stored;

	private String id;

	private UserProfile user_profile;
}