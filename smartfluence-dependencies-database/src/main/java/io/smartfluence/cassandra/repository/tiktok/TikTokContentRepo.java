package io.smartfluence.cassandra.repository.tiktok;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.primary.InfluencerContentKey;
import io.smartfluence.cassandra.entities.tiktok.TikTokContent;

@Repository
public interface TikTokContentRepo extends CassandraRepository<TikTokContent, InfluencerContentKey> {

	List<TikTokContent> findAllByKeyInfluencerHandle(String influencerHandle);
}