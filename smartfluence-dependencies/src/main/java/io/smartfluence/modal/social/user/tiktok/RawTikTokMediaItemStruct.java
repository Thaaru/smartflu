package io.smartfluence.modal.social.user.tiktok;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokMediaItemStruct {

	@JsonAlias("desc")
	private String description;

	@JsonAlias("author")
	private RawTikTokAuthor author;

	@JsonAlias("stats")
	private RawTikTokMediaStats stats;
}