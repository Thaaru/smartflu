package io.smartfluence.cassandra.entities.brand.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class BrandSubscriptionsKey implements Serializable {

	private static final long serialVersionUID = 8675143923039072531L;

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.PARTITIONED)
	private UUID brandId;

	@PrimaryKeyColumn(name = "active", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private boolean active;

	@PrimaryKeyColumn(name = "subscription_id", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private UUID subscriptionId;

	@PrimaryKeyColumn(name = "transaction_id", type = PrimaryKeyType.CLUSTERED, ordinal = 3)
	private UUID transactionId;

	public BrandSubscriptionsKey(BrandSubscriptionsKey key) {
		this.brandId = key.getBrandId();
		this.active = key.isActive();
		this.subscriptionId = key.getSubscriptionId();
		this.transactionId = key.getTransactionId();
	}
}