package io.smartfluence.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.InfluencerBulkMail;
import io.smartfluence.cassandra.entities.primary.InfluencerBulkMailKey;

@Repository
public interface InfluencerBulkMailRepo extends CassandraRepository<InfluencerBulkMail, InfluencerBulkMailKey> {

}