package io.smartfluence.brand.management.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BatchSchedulerConfiguration {

	@Bean
	public BatchScheduler batchScheduler() {
		return new BatchScheduler();
	}
}