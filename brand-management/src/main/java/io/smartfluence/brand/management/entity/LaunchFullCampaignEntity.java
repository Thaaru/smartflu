package io.smartfluence.brand.management.entity;

import io.smartfluence.brand.management.constants.CampaignStatusNew;
import io.smartfluence.brand.management.constants.DeletedStatus;
import io.smartfluence.brand.management.constants.IsNew;
import io.smartfluence.brand.management.constants.OfferType;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class LaunchFullCampaignEntity {

    private String campaignId;
    private String brandId;
    @NotBlank(message = "Brand Description should not be null")
    private String brandDescription;
    private String campaignName;
    private String campaignDescription;

    @Enumerated(EnumType.STRING)
    private CampaignStatusNew campaignStatusNew;

    @Enumerated(EnumType.STRING)
    private DeletedStatus deletedStatus;

    @Enumerated(EnumType.STRING)
    private OfferType offerType;

    @Enumerated(EnumType.STRING)
    private IsNew isNew;

    private String restrictions;
    private String currency;
    private List<CurrencyMaster> currencyMasters;
    private List<PostMaster> postMasters;
    private List<CommissionAffiliateTypeMaster> commissionAffiliateTypeMasters;
    private List<PlatformMaster> platformMasters;
    private LocalDateTime createdAt;
    private LocalDateTime deletedAt;
    private LocalDateTime modifiedAt;
    private LocalDateTime endDate;
    private String hashTags;
    private int maxProductCount;
    private double paymentOffer;
    private double commissionAffiliate;
    private String commissionAffiliateType;
    private String commissionAffiliateDetails;
    private String commissionAffiliateTypeName;
    private String commissionAffiliateCustomName;
    private Boolean isDefaultTermsConditions;
    private String platformName;
    private List<Products> productList;
    private List<CampaignTask> campaignTaskList;
    private long termId;
    private List<TermsAndConditions> termsAndConditions;


}
