package io.smartfluence.influencer.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignInfluencerOfferId implements Serializable{
    String campaignId;
    String influencerId;
    @Enumerated(EnumType.STRING)
    OffersType offerType;
}
