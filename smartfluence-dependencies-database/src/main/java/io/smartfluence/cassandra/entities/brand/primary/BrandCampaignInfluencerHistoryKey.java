package io.smartfluence.cassandra.entities.brand.primary;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import io.smartfluence.util.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class BrandCampaignInfluencerHistoryKey implements Serializable {

	private static final long serialVersionUID = -993775693514823099L;

	public BrandCampaignInfluencerHistoryKey(BrandCampaignInfluencerHistoryKey key) {
		this.brandId = key.getBrandId();
		this.campaignId = key.getCampaignId();
		this.influencerId = key.getInfluencerId();
	}

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.PARTITIONED, ordinal = 1)
	private UUID brandId;

	@Default
	@PrimaryKeyColumn(name = "modified_at", type = PrimaryKeyType.CLUSTERED, ordinal = 3)
	private Date modifiedAt=new Date();

	@Default
	@PrimaryKeyColumn(name = "trans_yr_month", type = PrimaryKeyType.PARTITIONED, ordinal = 2)
	private String transYrMonth=DateUtil.SDF_YYYY_MM.format(new Date());

	@PrimaryKeyColumn(name = "campaign_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID campaignId;

	@PrimaryKeyColumn(name = "influencer_id", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private UUID influencerId;
}