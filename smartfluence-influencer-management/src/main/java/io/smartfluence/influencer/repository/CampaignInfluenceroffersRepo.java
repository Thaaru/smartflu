package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.influencer.entities.CampaignInfluencerOfferId;
import io.smartfluence.influencer.entities.CampaignInfluencerOffers;

import java.util.List;

@Repository
public interface CampaignInfluenceroffersRepo extends JpaRepository<CampaignInfluencerOffers, CampaignInfluencerOfferId>{
    List<CampaignInfluencerOffers> findByCampaignInfluencerOfferTypeCampaignIdAndCampaignInfluencerOfferTypeInfluencerId(String campaignId,String influencerId);
}
