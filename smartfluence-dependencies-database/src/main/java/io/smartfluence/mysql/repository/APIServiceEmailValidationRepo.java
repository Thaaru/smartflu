package io.smartfluence.mysql.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.mysql.entities.APIServiceEmailValidation;
import io.smartfluence.mysql.entities.primary.APIServiceEmailValidationKey;

@Repository
public interface APIServiceEmailValidationRepo extends CrudRepository<APIServiceEmailValidation, APIServiceEmailValidationKey> {

	Optional<APIServiceEmailValidation> findByKeyEmailIdAndKeyService(String email, String service);

	@Modifying
	@Transactional
	@Query("update APIServiceEmailValidation u set u.dailyCount = 0 where u.dailyCount != 0")
	void updateDailyCount();
}