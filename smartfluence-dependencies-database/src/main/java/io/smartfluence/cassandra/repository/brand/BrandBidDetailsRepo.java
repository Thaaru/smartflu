package io.smartfluence.cassandra.repository.brand;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.brand.BrandBidDetails;
import io.smartfluence.cassandra.entities.brand.primary.BrandBidDetailsKey;

@Repository
public interface BrandBidDetailsRepo extends CassandraRepository<BrandBidDetails, BrandBidDetailsKey> {

	Set<BrandBidDetails> findAllByKeyBrandIdAndKeyCampaignId(UUID brandId, UUID campaignId);

	Optional<BrandBidDetails> findByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerIdAndKeyBidId(UUID brandId, UUID campaignId, UUID influencerId, UUID bidId);

	Long countByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(UUID brandId, UUID campaignId, UUID influencerId);

	List<BrandBidDetails> findAllByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(UUID brandId, UUID campaignId, UUID influencerId);
}