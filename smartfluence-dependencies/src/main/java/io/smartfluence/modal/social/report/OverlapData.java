package io.smartfluence.modal.social.report;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class OverlapData implements Serializable {

	private static final long serialVersionUID = -4361164997808921581L;

	private String profileName;

	@JsonAlias("user_id")
	private String userId;

	@JsonAlias("username")
	private String handle;

	private String profileImage;

	private byte[] profileImageUrl;

	private String description;

	@JsonAlias("followers")
	private Long followers;

	@JsonAlias("unique_percentage")
	private Double uniquePercentage;

	@JsonAlias("overlapping_percentage")
	private Double overlappingPercentage;

	private Long totalFollowers;

	private Long totalUniqueFollowers;
}