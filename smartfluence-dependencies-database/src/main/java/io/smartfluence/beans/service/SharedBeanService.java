package io.smartfluence.beans.service;

import org.springframework.http.ResponseEntity;

public interface SharedBeanService {

	public ResponseEntity<Object> getIndustryList();

	public ResponseEntity<Object> getLocationList();
}