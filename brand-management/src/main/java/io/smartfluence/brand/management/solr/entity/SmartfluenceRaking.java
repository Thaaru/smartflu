package io.smartfluence.brand.management.solr.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@SolrDocument(collection = "smartfluence_live")
public class SmartfluenceRaking {

	@Id
	@Indexed(name = "comparison_profile", type = "string")
	private String comparisonProfile;

	@Indexed(name = "name", type = "string")
	private String name;

	@Indexed(name="final_no_pricing",type="long")
	private Long finalNoPricing;

	@Indexed(name="final_with_pricing",type="long")
	private Long finalWithPricing;

	@Indexed(name="final_with_pricing_impressions",type="long")
	private Long finalWithPricingImpressions;
}