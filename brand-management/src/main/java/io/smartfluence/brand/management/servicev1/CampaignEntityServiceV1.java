package io.smartfluence.brand.management.servicev1;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.constants.CampaignStatusNew;
import io.smartfluence.brand.management.entityv1.*;
import io.smartfluence.brand.management.repository.CampaignInfOfferV1Repo;
import io.smartfluence.brand.management.repositoryv1.*;
import io.smartfluence.modal.brand.campaignv1.CampaignStatusEntityV1;
import io.smartfluence.modal.brand.campaignv1.IsActive;
import io.smartfluence.modal.brand.campaignv1.TermsTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

@Service
public class CampaignEntityServiceV1 {
    @Autowired
    BrandCampaignDetailsRepositoryV1 brandCampaignDetailsRepositoryV1;
    @Autowired
    CampaignProductsRepositoryV1 campaignProductsRepositoryV1;
    @Autowired
    CampaignTasksRepositoryV1 campaignTasksRepositoryV1;
    @Autowired
    TermsConditionRepositoryV1 termsConditionRepositoryV1;
    @Autowired
    CampaignOffersRepositoryV1 campaignOffersRepositoryV1;
    @Autowired
    CampaignInfOfferV1Repo  campaignInfOfferV1RepoV1;

    public BrandCampaignDetailsV1 getCampaignById(String campaignId){
        return brandCampaignDetailsRepositoryV1.findByCampaignId(campaignId);
    }

    public BrandCampaignDetailsV1 getCampaignByIdEager(String campaignId){
        return brandCampaignDetailsRepositoryV1.findByCampaignIdByEager(campaignId);
    }

    public List<BrandCampaignDetailsV1> getCampaignByCampaignNameAndBrandId(String campaignName,String brandId){
        return brandCampaignDetailsRepositoryV1.findByCampaignNameIgnoreCaseAndBrandId(campaignName,brandId);
    }

    public List<CampaignProductsV1> getProductsByCampaignId(String campaignId){
        return campaignProductsRepositoryV1.findByCampaignId(campaignId);
    }

    public List<CampaignTasksV1> getTasksByCampaignId(String campaignId){
        return campaignTasksRepositoryV1.findByCampaignId(campaignId);
    }

    public List<TermsConditionsV1> getTermsConditionsByTermIdAndType(long termId, TermsTypeEntity type){
        return  termsConditionRepositoryV1.findByTermIdOrType(termId,type);
    }

    public TermsConditionsV1 getTermsConditions(long termId, TermsTypeEntity type){
        return  termsConditionRepositoryV1.findByTermIdAndType(termId,type);
    }

    public List<TermsConditionsV1> getTermsConditionsByTypeAndIsActive(TermsTypeEntity termType, IsActive isActive){
        return  termsConditionRepositoryV1.findByTypeAndIsActive(termType,isActive);
    }

    public BrandCampaignDetailsV1 saveBrandCampaignDetails(BrandCampaignDetailsV1 brandCampaignDetailsV1){
        return brandCampaignDetailsRepositoryV1.save(brandCampaignDetailsV1);
    }

    public TermsConditionsV1 saveTermsCondition(TermsConditionsV1 termsConditionsV1){
        return termsConditionRepositoryV1.save(termsConditionsV1);
    }



    public void deleteProductsByProductId(long productId){
        campaignProductsRepositoryV1.deleteByProductId(productId);
    }

    public CampaignProductsV1 saveCampaignProducts(CampaignProductsV1 campaignProductsV1){
        return campaignProductsRepositoryV1.save(campaignProductsV1);
    }

    public List<CampaignOffersV1> getCampaignOffersByCampaignId(String campaignId){
        return campaignOffersRepositoryV1.findByCampaignOfferId_CampaignId(campaignId);
    }
    public List<CampaignInfluencerOfferV1Entity> getCampaignInfOffersByCampaignId(String campaignId){
        return campaignInfOfferV1RepoV1.findByCampaignOfferInfluencerId_CampaignId(campaignId);
    }
    public List<CampaignInfluencerOfferV1Entity> getCampaignInfOffersByCampaignIdAndInfluencerId(String campaignId, String influencrId){
        return campaignInfOfferV1RepoV1.findByCampaignOfferInfluencerId_CampaignIdAndCampaignOfferInfluencerId_InfluencerId(campaignId, influencrId);
    }


    public CampaignTasksV1 saveCampaignTasks(CampaignTasksV1 campaignTasksV1){
        return campaignTasksRepositoryV1.save(campaignTasksV1);
    }

    public void deleteCampaignTaskByTaskId(long taskId){
         campaignTasksRepositoryV1.deleteByTaskId(taskId);
    }

    public CampaignOffersV1 saveCampaignOffers(CampaignOffersV1 campaignOffersV1){
        return campaignOffersRepositoryV1.save(campaignOffersV1);
    }

    public void deleteOffersByCampaignOfferId(CampaignOfferId campaignOfferId){
        campaignOffersRepositoryV1.deleteByCampaignOfferId(campaignOfferId);
    }

    public List<BrandCampaignDetailsV1> getAllCampaignsByBrandId(String brandId){
        return brandCampaignDetailsRepositoryV1.findByBrandId(brandId);
    }

    public TermsConditionsV1 getTermsAndConditionByTermId(long termId){
        return termsConditionRepositoryV1.findByTermId(termId);
    }

    public void deleteByTermId(long termId){
        termsConditionRepositoryV1.deleteByTermId(termId);
    }

    public BrandCampaignDetailsV1 getCampaignByCampaignNameAndBrandIdAndNotCampaignId(String campaignName,
                                                                                      String brandId,String campaignId){
        return brandCampaignDetailsRepositoryV1.findByCampaignNameAndBrandIdAndCampaignIdNotAndCampaignStatusNot(
                campaignName,brandId,campaignId, CampaignStatusEntityV1.INACTIVE);
    }



 }
