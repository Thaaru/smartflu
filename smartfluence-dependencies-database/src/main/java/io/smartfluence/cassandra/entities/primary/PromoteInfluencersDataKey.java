package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PromoteInfluencersDataKey implements Serializable {

	private static final long serialVersionUID = 3329352361796533974L;

	public PromoteInfluencersDataKey(PromoteInfluencersDataKey key) {
		this.campaignId = key.getCampaignId();
		this.influencerDeepSocialId = key.getInfluencerDeepSocialId();
		this.promoteInfluencerId = key.getPromoteInfluencerId();
	}

	@PrimaryKeyColumn(name = "campaign_id", type = PrimaryKeyType.PARTITIONED)
	private UUID campaignId;

	@PrimaryKeyColumn(name = "influencer_deep_social_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private String influencerDeepSocialId;

	@PrimaryKeyColumn(name = "promote_influencer_id", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private UUID promoteInfluencerId;
}