var minH = $('.side-nav-wrapper').height();
var contentH = $('.side-nav-content').height();
if (contentH > minH) {
	minH = contentH;
}
$('.side-nav-wrapper, .tab-pane').css('min-height', `${minH}px`);
$('.side-nav-item').click(
	function () {
		$(this).parent().find('.side-nav-item').removeClass('active');
		$(this).addClass('active');
		$('.side-nav-content .tab-pane').removeClass('show active');
		$('.side-nav-content ' + $(this).find('a').attr('href')).addClass('show active');
	}
)

var gender;
var country;
var age;
var state;
var estPricePerInfluencer;
var costPerEngagement;
var costPerFollower;
var mSelect;
var mSelectCost;
var mSelectPost;
var ids
var costInfluencerJson = {};
var influencerJson = {};
var countries = {};
var states = {};
var interest = {};
var estPricePerInfluencerScroll;
var costPerEngagementScroll;
var costPerFollowerScroll;
var selectedCampaign = '';
var selectedInfluencer = [];
var selectedCampaignCost = '';
var selectedInfluencerCost = [];
var takeInfluencer = true;

$(document).ready(function () {
	var paramsString = window.location.search;
	var searchParams = new URLSearchParams(paramsString);
	console.log("tab id in analytics : ", searchParams.has("tabId"))

	if (searchParams.has("tabId")) {
		var selectedTab = document.querySelector(`#${searchParams.get("tabId")}`);
		var tab = new bootstrap.Tab(selectedTab);
		tab.show();
	}

	if (!showLoader) {
		$('.number-anim').numberAnimate();
	}
	$('#genderAgeLabel').click();
	$('#genderAgeLabel').parent().click();
	defaultGraph(3);

	mSelectCost = $('#influencerCost').mSelect({
		'selectLabel': 'Influencer',
		'selectPlaceHolder': 'Select Influencers'
	});
	mSelect = $('#influencer').mSelect({
		'selectLabel': 'Influencer',
		'selectPlaceHolder': 'Select Influencers'
	});
	mSelectPost = $('#influencerPost').mSelect({
		'selectLabel': 'Influencer',
		'selectPlaceHolder': 'Select Influencers'
	});
})

function defaultGraph(flag) {

	if ((flag & 1) == 1) {
		gender = {
			colors: ['#8ECEFD'],
			labels: ['No data'],
			data: [100]
		}

		age = {
			colors: ['#2f008d'],
			labels: ['No data'],
			data: []
		}

		engagement = {
			colors: '#2f008d',
			labels: ['No data'],
			data: [0]
		}

		country = {
			colors: '#2f008d',
			labels: ['No data'],
			data: [0]
		}

		state = {
			colors: '#2f008d',
			labels: ['No data'],
			data: [0]
		}

		buildChart('gender', 'doughnut');
		buildChart('age', 'bar');

		buildChartWithFormatting('country', 'horizontalBar');
		buildChartWithFormatting('state', 'horizontalBar');
		buildChartWithFormatting('engagement', 'horizontalBar');
	}

	if ((flag & 2) == 2) {
		estPricePerInfluencer = {
			colors: '#2f008d',
			labels: ['No data'],
			data: [0]
		}

		costPerEngagement = {
			colors: '#2f008d',
			labels: ['No data'],
			data: [0]
		}

		costPerFollower = {
			colors: '#2f008d',
			labels: ['No data'],
			data: [0]
		}

		buildChartWithScroll('estPricePerInfluencer', 'horizontalBar');
		buildChartWithScroll('costPerEngagement', 'horizontalBar');
		buildChartWithScroll('costPerFollower', 'horizontalBar');
	}
}

var chartJson = {}

function buildChartWithScroll(id, type, scroll) {
	var height = eval(id).data.length * 60;
	$('.chartAreaWrapper2').css("height", height + "px");
	if (!scroll) {

		if (id == "estPricePerInfluencer") {
			$('.chartAreaWrapper').css("max-height", " 220px");
		} else if (id == "costPerEngagement") {
			$('.chartAreaWrapper').css("max-height", " 220px");
		} else if (id == "costPerFollower") {
			$('.chartAreaWrapper').css("max-height", "220px");
		}
	}
	var baseData;

	var data = data = {
		labels: eval(id).labels,
		datasets: [{
			data: eval(id).data,
			backgroundColor: eval(id).colors
		}]
	};

	var config = {
		type: type,
		data: data,

		options: {
			maintainAspectRatio: scroll,
			scales: {
				xAxes: [{
					position: 'top',
					ticks: {
						beginAtZero: true,
						callback: function (value, index, values) {
							return '$' + value;
						}
					}
				}],
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}],
			},
			animation: {
				onComplete: function () {
					if (!scroll) {
						if (id == "estPricePerInfluencer" || id == "costPerEngagement" || id == "costPerFollower") {
							if (eval(id).data.length > 4) {
								var scale = window.devicePixelRatio;
								var sourceCanvas = chart.chart.canvas;
								var copyHeight = chart.scales['x-axis-0'].height;
								var copyWidth = chart.scales['x-axis-0'].width +
									chart.scales['y-axis-0'].width + 10;
								var targetCtx = document.getElementById(id + "Axes")
									.getContext('2d');
								targetCtx.scale(scale, scale);
								targetCtx.canvas.width = copyWidth * scale;
								targetCtx.canvas.height = copyHeight * scale;
								targetCtx.canvas.style.width = `${copyWidth}px`;
								targetCtx.canvas.style.height = `${copyHeight}px`;
								var axesTop = $('#' + id + 'Axes').siblings()[0].clientHeight;
								targetCtx.canvas.style.top = `-${axesTop}px`

								try {
									targetCtx.drawImage(sourceCanvas, 0, 0,
										(copyWidth * scale), copyHeight * scale, 0, 0,
										copyWidth * scale, copyHeight * scale);
								} catch (e) {
									console.log(e);
								}
							} else {
								$('#' + id + 'Axes').remove();
								$('.chartWrapper.' + id).append('<canvas id="' + id + 'Axes" height="0" width="300"></canvas>');
							}
						}
					}
				}
			},
			legend: {
				display: false
			},
			legendCallback: function (chart) {
				var text = [];
				var ds = chart.data.datasets[0];
				text.push('<ul class="chart-legend  row">');

				for (var i in ds.data) {
					if (Number(i) % 2 == 0) {

					}
					text.push('<li class="col-xl-12 col-lg-12 col-md-6 col-sm-4"><span class="chart-legend-icon" style="background-color:' +
						ds.backgroundColor[i] + '">&nbsp</span>');
					if (chart.data.labels[i]) {
						text.push('<span class="chart-legend-label">' +
							chart.data.labels[i] + '</span>');
					}
					text.push('</li>');
					if (Number(i) % 2 != 0) {

					}
				}
				text.push('</ul>');
				var content = text.join("");
				$('#' + id + '-legend').html(content);
				return content;
			}
		}
	};
	if (id == "estPricePerInfluencer" || id == "costPerEngagement" || id == "costPerFollower") {
		$('#' + id).remove();
		$('.chartAreaWrapper2.' + id).append('<canvas id="' + id + '" width="100%" height="100%">');
	}
	var canvas = document.getElementById(id);
	var ctx = canvas.getContext('2d');
	var chart = new Chart(ctx, config);
	chart.generateLegend();
	canvas.chart = chart;
}

function buildChart(id, type, scroll) {
	var baseData;

	var data = data = {
		labels: eval(id).labels,
		datasets: [{
			data: eval(id).data,
			backgroundColor: eval(id).colors
		}]
	};

	var config = {
		type: type,
		data: data,
		options: {
			maintainAspectRatio: scroll,

			xAxes: [{
				ticks: {
					beginAtZero: true
				}
			}],
			yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}],
			animation: {
				onComplete: function () {
					if (scroll) {
						var scale = window.devicePixelRatio;
						var sourceCanvas = chart.chart.canvas;
						var copyHeight = chart.scales['x-axis-0'].height;
						var copyWidth = chart.scales['x-axis-0'].width
							+ chart.scales['y-axis-0'].width + 10;
						var targetCtx = document.getElementById(id + "Axes")
							.getContext('2d');
						targetCtx.scale(scale, scale);
						targetCtx.canvas.width = copyWidth * scale;
						targetCtx.canvas.height = copyHeight * scale;
						targetCtx.canvas.style.width = `${copyWidth}px`;
						targetCtx.canvas.style.height = `${copyHeight}px`;
						targetCtx.drawImage(sourceCanvas, 0,
							sourceCanvas.height - (copyHeight * scale),
							copyWidth * scale, copyHeight * scale, 0, 0,
							copyWidth * scale, copyHeight * scale);
						var sourceCtx = sourceCanvas.getContext('2d');
						sourceCtx.clearRect(0,
							sourceCanvas.height - copyHeight, copyWidth
						* scale, copyHeight * scale);
					}
				}
			},
			legend: {
				display: false
			},
			legendCallback: function (chart) {
				var text = [];
				var ds = chart.data.datasets[0];
				text.push('<ul class="chart-legend  row">');
				for (var i in ds.data) {
					if (Number(i) % 2 == 0) {

					}
					text.push('<li class="col-xl-12 col-lg-12 col-md-6 col-sm-4"><span class="chart-legend-icon" style="background-color:'
						+ ds.backgroundColor[i] + '">&nbsp</span>');
					if (chart.data.labels[i]) {
						text.push('<span class="chart-legend-label">'
							+ chart.data.labels[i] + '</span>');
					}
					text.push('</li>');
					if (Number(i) % 2 != 0) {

					}
				}
				text.push('</ul>');
				var content = text.join("");
				$('#' + id + '-legend').html(content);
				return content;
			}
		}
	};
	var canvas = document.getElementById(id);
	if (canvas.chart) {
		canvas.chart.clear();
		canvas.chart.destroy();
	}
	var ctx = canvas.getContext('2d');
	var chart = new Chart(ctx, config);
	chart.generateLegend();
	canvas.chart = chart;
}

function buildChartWithFormatting(id, type, scroll) {
	var baseData;
	var canvasOld = document.getElementById(id)
		.getContext('2d').canvas;
	canvasOld.width = 0;
	canvasOld.height = 0;

	var data = data = {
		labels: eval(id).labels,
		datasets: [{
			data: eval(id).data,
			backgroundColor: eval(id).colors
		}]
	};

	var config = {
		type: type,
		data: data,
		options: {
			maintainAspectRatio: true,
			responsive: true,
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero: true,
						callback: function (value, index, values) {
							return (value / 1000) + 'k';
						}
					}
				}],
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			},
			animation: {
				onComplete: function () {
					if (scroll) {
						var scale = window.devicePixelRatio;
						var sourceCanvas = chart.chart.canvas;
						var copyHeight = chart.scales['x-axis-0'].height;
						var copyWidth = chart.scales['x-axis-0'].width +
							chart.scales['y-axis-0'].width + 10;
						var targetCtx = document.getElementById(id + "Axes")
							.getContext('2d');
						targetCtx.scale(scale, scale);
						targetCtx.canvas.width = copyWidth * scale;
						targetCtx.canvas.height = copyHeight * scale;
						targetCtx.canvas.style.width = `${copyWidth}px`;
						targetCtx.canvas.style.height = `${copyHeight}px`;
						targetCtx.drawImage(sourceCanvas, 0,
							sourceCanvas.height - (copyHeight * scale),
							copyWidth * scale, copyHeight * scale, 0, 0,
							copyWidth * scale, copyHeight * scale);
						var sourceCtx = sourceCanvas.getContext('2d');
						sourceCtx.clearRect(0,
							sourceCanvas.height - copyHeight, copyWidth *
						scale, copyHeight * scale);
					}
				}
			},
			legend: {
				display: false
			},
			legendCallback: function (chart) {
				var text = [];

				var ds = chart.data.datasets[0];
				text.push('<ul class="chart-legend  row">');

				for (var i in ds.data) {
					if (Number(i) % 2 == 0) {

					}
					text.push('<li class="col-xl-12 col-lg-12 col-md-6 col-sm-4"><span class="chart-legend-icon" style="background-color:' +
						ds.backgroundColor[i] + '">&nbsp</span>');
					if (chart.data.labels[i]) {
						text.push('<span class="chart-legend-label">' +
							chart.data.labels[i] + '</span>');
					}
					text.push('</li>');
					if (Number(i) % 2 != 0) {

					}
				}
				text.push('</ul>');
				var content = text.join("");
				$('#' + id + '-legend').html(content);
				return content;
			}
		}
	};
	var canvas = document.getElementById(id);
	if (canvas.chart) {
		canvas.chart.clear();
		canvas.chart.destroy();
	}
	var ctx = canvas.getContext('2d');
	var chart = new Chart(ctx, config);
	chart.generateLegend();
	canvas.chart = chart;
}

$('#campaignStatisticsLabel').on('click', function () {
	setTimeout(() => {
		$('#genderAgeLabel').click();
		$('#genderAgeLabel').parent().click();
	}, 10);
	if ($('#campaign').val().trim() == "Select a Campaign") {
		defaultGraph(1);
	} else if (!isEqual(selectedInfluencerCost, selectedInfluencer)) {
		takeInfluencer = false;
		$('#influencer')[0].unselect(ids);
		takeInfluencer = true;
		$('#influencer')[0].select(selectedInfluencerCost);

		if (selectedInfluencerCost == 0) {
			selectedInfluencer = selectedInfluencerCost;
		}
		processInfluencerDetailsForCampaignStatistics();
	} else if (selectedInfluencerCost == 0) {
		selectedInfluencer = selectedInfluencerCost;
	}
})

$('#campaign').on('change', function () {
	var selectPlatform = '<option value="instagram">Instagram</option>' +
		'<option value="youtube">YouTube</option>' +
		'<option value="tiktok">TikTok</option>';
	$("#campaign-platform").empty();
	$("#campaign-platform").append(selectPlatform);
	var campaignId = $('#campaign').val().trim();

	if (selectedCampaign != campaignId) {
		getInfluencerDetailsForCampaignStatistics(campaignId);
		selectedCampaign = campaignId;
		$('#campaignCost option[value=' + campaignId + ']').attr('selected', 'selected');
		$('#campaignCost').change();
	}
})

$('#campaign-platform').on('change', function () {
	var platform = $("#campaign-platform").val();

	var campaignId = $('#campaign').val().trim();
	processInfluencerDetailsForCampaignStatistics();

	if (platform == "youtube" || platform == "tiktok") {
		$("#analytic-intrest").addClass("d-none");
		$(".audience-states").addClass("d-none");
	} else if (platform == "instagram") {
		$("#analytic-intrest").removeClass("d-none");
		$(".audience-states").removeClass("d-none");
	}
});

var influncerDetailsForCampaignStatistics;

function getInfluencerDetailsForCampaignStatistics(campaignId) {
	$.get({
		url: window.location.origin + '/brand/campaign/' + campaignId + '/analytics',
	}).done(function (data) {
		influncerDetailsForCampaignStatistics = data;
		processInfluencerDetailsForCampaignStatistics();
	}).fail(function (data) {

	});
}

function processInfluencerDetailsForCampaignStatistics() {
	var data = influncerDetailsForCampaignStatistics;
	var content = '';
	ids = new Array();

	for (var i in data) {
		var influencer = data[i];

		if (influencer.platfrom == $('#campaign-platform').val()) {
			content += '<option value="' + influencer.influencerId + '">' + influencer.influencerHandle + '</option>';
			ids.push(influencer.influencerId);
			influencerJson[influencer.influencerId] = influencer;
			var influencerCountry = influencer.country;
			var influencerState = influencer.state;
			var influencerInterest = influencer.industry;
		}

		for (var countryName in influencerCountry) {
			var influencerDetail = {};
			var countryDetails = new Array();
			influencerDetail['influencerId'] = influencer.influencerId;
			influencerDetail['countryPercentage'] = influencerCountry[countryName];
			if (countryName in countries) {
				countryDetails = countries[countryName];
				countryDetails.push(influencerDetail);
			} else {
				countryDetails.push(influencerDetail);
			}
			countries[countryName] = countryDetails;
		}
		for (var stateName in influencerState) {
			var influencerStateDetail = {};
			var stateDetails = new Array();
			influencerStateDetail['influencerId'] = influencer.influencerId;
			influencerStateDetail['statePercentage'] = influencerState[stateName];
			if (stateName in states) {
				stateDetails = states[stateName];
				stateDetails.push(influencerStateDetail);
			} else {
				stateDetails.push(influencerStateDetail);
			}
			states[stateName] = stateDetails;
		}
		for (var interestName in influencerInterest) {
			var influencerInterestDetail = {};
			var interestDetails = new Array();
			influencerInterestDetail['influencerId'] = influencer.influencerId;
			influencerInterestDetail['interestPercentage'] = influencerInterest[interestName];
			if (interestName in interest) {
				interestDetails = interest[interestName];
				interestDetails.push(influencerInterestDetail);
			} else {
				interestDetails.push(influencerInterestDetail);
			}
			interest[interestName] = interestDetails;
		}
	}

	$('#influencer').html(content);
	mSelect.rebuild();
	$('.m-select-all-label.influencer').click();

	if (content == '') {
		defaultGraph(1);
		$('.potentialReach').html("-");
		$('.likes').html("-");
		$('.comments').html("-");
		$('.engagementRate').html("-");
		$('.engagementRatePercentage').addClass('d-none');
	}
}

$('#influencer').on('change', function () {
	var id = $('#influencer').val();
	viewAnalytics(id);
	if (takeInfluencer) {
		selectedInfluencer = id;
	}
})

function viewAnalytics(id) {
	var potentialReach = 0;
	var engagementRate = 0;
	var estimatedCost = 0;
	var costPerReach = 0;
	var costPerEngagement = 0;
	var male = 0;
	var female = 0;
	var age13To17 = 0;
	var age18To24 = 0;
	var age25To34 = 0;
	var age35To44 = 0;
	var age45To64 = 0;
	var age65Plus = 0;
	var likes = 0;
	var comments = 0;
	var calculatedJson = {};
	var calculatedStateJson = {};
	var calculatedInterestJson = {};

	for (var i in id) {
		var influencerDetails = influencerJson[id[i]];
		potentialReach = potentialReach +
			influencerDetails.followers;
		engagementRate = engagementRate +
			((influencerDetails.engagement / influencerDetails.followers) * 100);
		estimatedCost = estimatedCost +
			(influencerDetails.influencerPricing);
		costPerReach = costPerReach +
			(influencerDetails.followers / influencerDetails.influencerPricing);
		costPerEngagement = costPerEngagement +
			(influencerDetails.engagement / influencerDetails.influencerPricing);
		male = male + influencerDetails.gender.Male;
		female = female + influencerDetails.gender.Female;
		age13To17 = age13To17 +
			influencerDetails.ageRange["13-17"];
		age18To24 = age18To24 +
			influencerDetails.ageRange["18-24"];
		age25To34 = age25To34 +
			influencerDetails.ageRange["25-34"];
		age35To44 = age35To44 +
			influencerDetails.ageRange["35-44"];
		age45To64 = age45To64 +
			influencerDetails.ageRange["45-64"];
		age65Plus = age65Plus +
			influencerDetails.ageRange["65-Any"];
		likes = likes + influencerDetails.likes;
		comments = comments + influencerDetails.comments;
		calculatedJson['all'] = 0;
		calculatedStateJson['all'] = 0;
		calculatedInterestJson['all'] = 0;

		for (var countryName in countries) {
			var countryData = countries[countryName];

			for (var data in countryData) {
				var influencerData = countryData[data];

				if (id[i] == influencerData.influencerId) {
					if (!calculatedJson[countryName]) {
						calculatedJson[countryName] = [];
					}
					calculatedJson[countryName] = Number(calculatedJson[countryName]) +
						Number(influencerDetails.followers *
							influencerData.countryPercentage);
					calculatedJson['all'] += (influencerDetails.followers * influencerData.countryPercentage);
				}
			}
		}
		for (var stateName in states) {
			var stateData = states[stateName];

			for (var sdata in stateData) {
				var influencerSData = stateData[sdata];

				if (id[i] == influencerSData.influencerId) {
					if (!calculatedStateJson[stateName]) {
						calculatedStateJson[stateName] = [];
					}
					calculatedStateJson[stateName] = Number(calculatedStateJson[stateName]) +
						Number(influencerDetails.followers *
							influencerSData.statePercentage);
					calculatedStateJson['all'] += (influencerDetails.followers * influencerSData.statePercentage);
				}
			}
		}
		for (var interestName in interest) {
			var interestData = interest[interestName];

			for (var idata in interestData) {
				var influencerIData = interestData[idata];

				if (id[i] == influencerIData.influencerId) {
					if (!calculatedInterestJson[interestName]) {
						calculatedInterestJson[interestName] = [];
					}
					calculatedInterestJson[interestName] = Number(calculatedInterestJson[interestName]) +
						Number(influencerDetails.followers *
							influencerIData.interestPercentage);
					calculatedInterestJson['all'] += (influencerDetails.followers * influencerIData.interestPercentage);
				}
			}
		}
	}

	var noOfCountries = 5;
	var noOfStates = 5;
	var noOfInterest = 5;
	var calculatedArray = [];
	var calculatedStateArray = [];
	var calculatedInterestArray = [];

	for (var idx in calculatedJson) {
		var json = {};
		json[idx] = calculatedJson[idx];
		calculatedArray.push(json);
	}
	var calculatedFilterArray = calculatedArray.filter(word => !(Object.keys(word) == 'all'));
	calculatedFilterArray.sort(function (a, b) {
		return b[Object.keys(b)[0]] - a[Object.keys(a)[0]];
	});
	calculatedFilterArray.splice(noOfCountries, calculatedFilterArray.length);

	for (var sdx in calculatedStateJson) {
		var sJson = {};
		sJson[sdx] = calculatedStateJson[sdx];
		calculatedStateArray.push(sJson);
	}
	var calculatedStateFilterArray = calculatedStateArray.filter(word => !(Object.keys(word) == 'all'));
	calculatedStateFilterArray.sort(function (a, b) {
		return b[Object.keys(b)[0]] - a[Object.keys(a)[0]];
	});
	calculatedStateFilterArray.splice(noOfStates, calculatedStateFilterArray.length);

	for (var indx in calculatedInterestJson) {
		var iJson = {};
		iJson[indx] = calculatedInterestJson[indx];
		calculatedInterestArray.push(iJson);
	}
	var calculatedInterestFilterArray = calculatedInterestArray.filter(word => !(Object.keys(word) == 'all'));
	calculatedInterestFilterArray.sort(function (a, b) {
		return b[Object.keys(b)[0]] - a[Object.keys(a)[0]];
	});
	calculatedInterestFilterArray.splice(noOfInterest, calculatedInterestFilterArray.length);
	var influencersSelected = id.length;
	$('.potentialReach').html(potentialReach).numberAnimate(false);
	$('.likes').html(likes / id.length).numberAnimate(false);
	$('.comments').html(comments / id.length).numberAnimate(false);
	$('.engagementRate').html(engagementRate / influencersSelected).numberAnimate(false)
	$('.engagementRatePercentage').removeClass('d-none');

	$('#estimatedCost').html(estimatedCost).numberAnimate(false);
	$('#costPerReach').html(costPerReach).numberAnimate(false);
	$('#costPerEngagement').html(costPerEngagement).numberAnimate(false);

	gender = {
		colors: ['#8ECEFD', '#F7ABD5'],
		labels: ['Male %', 'Female %'],
		data: [((male / id.length) * 100).toFixed(2), ((female / id.length) * 100).toFixed(2)]
	}
	age = {
		colors: ['#2f008d', '#2f008d', '#2f008d', '#2f008d',
			'#2f008d', '#2f008d'
		],
		labels: ['13-17 %', '18-24 %', '25-34 %', '35-44 %', '45-64 %',
			'65+ %'
		],
		data: [((age13To17 / id.length) * 100).toFixed(2), ((age18To24 / id.length) * 100).toFixed(2), ((age25To34 / id.length) * 100).toFixed(2), ((age35To44 / id.length) * 100).toFixed(2),
		((age45To64 / id.length) * 100).toFixed(2), ((age65Plus / id.length) * 100).toFixed(2)
		]
	}

	country = {
		colors: '#2f008d',
		labels: [],
		data: []
	}
	state = {
		colors: '#2f008d',
		labels: [],
		data: []
	}
	engagement = {
		colors: '#2f008d',
		labels: [],
		data: []
	}
	if ((calculatedFilterArray.length) != 0) {
		for (var i = 0; i < noOfCountries; i++) {
			var countryName = Object.keys(calculatedFilterArray[i]);
			var countryValue = Object.values(calculatedFilterArray[i]);
			country.labels.push(countryName[0]);
			country.data.push(parseInt(countryValue[0]));
		}
		if ($('#campaign-platform').val() == "instagram" && (calculatedStateFilterArray.length) != 0 && (calculatedInterestFilterArray.length) != 0) {
			for (var i = 0; i < noOfStates; i++) {
				var stateName = Object.keys(calculatedStateFilterArray[i]);
				var stateValue = Object.values(calculatedStateFilterArray[i]);
				state.labels.push(stateName[0]);
				state.data.push(parseInt(stateValue[0]));
			}

			for (var i = 0; i < noOfInterest; i++) {
				var interestName = Object.keys(calculatedInterestFilterArray[i]);
				var interestValue = Object.values(calculatedInterestFilterArray[i]);
				engagement.labels.push(interestName[0]);
				engagement.data.push(parseInt(interestValue[0]));
			}
		}

		buildChart('gender', 'doughnut');
		buildChart('age', 'bar');
		buildChartWithFormatting('country', 'horizontalBar');
		buildChartWithFormatting('state', 'horizontalBar');
		buildChartWithFormatting('engagement', 'horizontalBar');
	} else {
		defaultGraph(1);
	}
}

$('#costStatisticsLabel').on('click', function () {
	setTimeout(() => {
		$('#estPricePerInfluencerNavLabel').click();
	}, 10);

	if ($('#campaignCost').val().trim() == "Select a Campaign") {
		defaultGraph(2);
	} else if (!isEqual(selectedInfluencer, selectedInfluencerCost)) {
		takeInfluencer = false;
		$('#influencerCost')[0].unselect(ids);
		takeInfluencer = true;
		$('#influencerCost')[0].select(selectedInfluencer);

		if (selectedInfluencer == 0) {
			selectedInfluencerCost = selectedInfluencer;
		}
		processInfluencerDetailsForCostStatistics();
	} else if (selectedInfluencer == 0) {
		selectedInfluencerCost = selectedInfluencer;
	}
})

$('#campaignCost').on('change', function () {
	var selectPlatform = '<option value="instagram">Instagram</option>' +
		'<option value="youtube">YouTube</option>' +
		'<option value="tiktok">TikTok</option>';
	$("#cost-platform").empty();
	$("#cost-platform").append(selectPlatform);
	var campaignId = $('#campaignCost').val().trim();

	if (selectedCampaignCost != campaignId) {
		getInfluencerDetailsForCostStatistics(campaignId);
		selectedCampaignCost = campaignId;
		$('#campaign option[value=' + campaignId + ']').attr('selected', 'selected');
		$('#campaign').change();
	}
})

$('#cost-platform').on('change', function () {
	var campaignId = $('#campaignCost').val().trim();
	processInfluencerDetailsForCostStatistics();
});

var influencerDetailsForCostStatistics;

function getInfluencerDetailsForCostStatistics(campaignId) {
	$.get({
		url: window.location.origin + '/brand/campaign/' + campaignId + '/analytics',
	}).done(function (data) {
		influencerDetailsForCostStatistics = data;
		processInfluencerDetailsForCostStatistics();
	}).fail(function (data) {

	});
}

function processInfluencerDetailsForCostStatistics() {
	var data = influencerDetailsForCostStatistics;
	var content = '';
	ids = new Array();

	for (var i in data) {
		var influencer = data[i];

		if (influencer.platfrom == $('#cost-platform').val()) {
			content += '<option value="' + influencer.influencerId + '">' + influencer.influencerHandle + '</option>';
			ids.push(influencer.influencerId);
			costInfluencerJson[influencer.influencerId] = influencer;
		}
	}

	$('#influencerCost').html(content);
	mSelectCost.rebuild();
	$('.m-select-all-label.influencerCost').click();

	if (content == '') {
		defaultGraph(2);
		$('.avgCostPerEngagement').html("-");
		$('.estCampaignCost').html("-");
		$('.avgCostPerFollower').html("-");
		$('.cost-dollar').addClass('d-none');
	}
}

$('#influencerCost').on('change', function () {
	var id = $('#influencerCost').val();
	viewCostAnalytics(id);
	if (takeInfluencer) {
		selectedInfluencerCost = id;
	}
})

function viewCostAnalytics(id) {
	var avgCostPerEngagement = 0;
	var campaignCost = 0;
	var avgCostPerFollower = 0;
	var calculatedEstPriceJson = {};
	var calculatedCostPerEngagementJson = {};
	var calculatedCostPerFollowerJson = {};
	var calculatedEstPriceArray = [];
	var calculatedCostPerEngagementArray = [];
	var calculatedCostPerFollowerArray = [];

	estPricePerInfluencer = {
		colors: '#2f008d',
		labels: [],
		data: []
	}

	costPerEngagement = {
		colors: '#2f008d',
		labels: [],
		data: []
	}

	costPerFollower = {
		colors: '#2f008d',
		labels: [],
		data: []
	}
	for (var i in id) {
		var influencerDetails = costInfluencerJson[id[i]];
		avgCostPerEngagement = avgCostPerEngagement + (influencerDetails.influencerPricing / influencerDetails.engagement);
		campaignCost = campaignCost + influencerDetails.influencerPricing;
		avgCostPerFollower = avgCostPerFollower + (influencerDetails.influencerPricing / influencerDetails.followers);

		if (!calculatedEstPriceJson[influencerDetails.influencerHandle]) {
			calculatedEstPriceJson[influencerDetails.influencerHandle] = [];
		}
		calculatedEstPriceJson[influencerDetails.influencerHandle] = influencerDetails.influencerPricing;
		if (!calculatedCostPerEngagementJson[influencerDetails.influencerHandle]) {
			calculatedCostPerEngagementJson[influencerDetails.influencerHandle] = [];
		}
		calculatedCostPerEngagementJson[influencerDetails.influencerHandle] = (influencerDetails.influencerPricing / influencerDetails.engagement).toFixed(2);
		if (!calculatedCostPerFollowerJson[influencerDetails.influencerHandle]) {
			calculatedCostPerFollowerJson[influencerDetails.influencerHandle] = [];
		}
		calculatedCostPerFollowerJson[influencerDetails.influencerHandle] = ((influencerDetails.influencerPricing / influencerDetails.followers) * 1000).toFixed(2);
	}
	for (var idx in calculatedEstPriceJson) {
		var json = {};
		json[idx] = calculatedEstPriceJson[idx];
		calculatedEstPriceArray.push(json);
	}
	calculatedEstPriceArray.sort(function (a, b) {
		return b[Object.keys(b)[0]] - a[Object.keys(a)[0]];
	});

	for (var idx in calculatedCostPerEngagementJson) {
		var json = {};
		json[idx] = calculatedCostPerEngagementJson[idx];
		calculatedCostPerEngagementArray.push(json);
	}
	calculatedCostPerEngagementArray.sort(function (a, b) {
		return b[Object.keys(b)[0]] - a[Object.keys(a)[0]];
	});

	for (var idx in calculatedCostPerFollowerJson) {
		var json = {};
		json[idx] = calculatedCostPerFollowerJson[idx];
		calculatedCostPerFollowerArray.push(json);
	}
	calculatedCostPerFollowerArray.sort(function (a, b) {
		return b[Object.keys(b)[0]] - a[Object.keys(a)[0]];
	});

	if ((calculatedEstPriceArray.length) != 0 && (calculatedCostPerEngagementArray.length) != 0 && (calculatedCostPerFollowerArray.length) != 0) {
		for (var i = 0; i < calculatedEstPriceArray.length; i++) {
			var influencerHandle = Object.keys(calculatedEstPriceArray[i]);
			var estPrice = Object.values(calculatedEstPriceArray[i]);
			estPricePerInfluencer.labels.push(influencerHandle[0]);
			estPricePerInfluencer.data.push(parseInt(estPrice[0]));
		}
		for (var i = 0; i < calculatedCostPerEngagementArray.length; i++) {
			var influencerHandle = Object.keys(calculatedCostPerEngagementArray[i]);
			var costPerEngage = Object.values(calculatedCostPerEngagementArray[i]);
			costPerEngagement.labels.push(influencerHandle[0]);
			costPerEngagement.data.push(costPerEngage[0]);
		}
		for (var i = 0; i < calculatedCostPerFollowerArray.length; i++) {
			var influencerHandle = Object.keys(calculatedCostPerFollowerArray[i]);
			var costPerFollow = Object.values(calculatedCostPerFollowerArray[i]);
			costPerFollower.labels.push(influencerHandle[0]);
			costPerFollower.data.push(costPerFollow[0]);
		}
	}
	$('.avgCostPerEngagement').html(avgCostPerEngagement / id.length).numberAnimate(false);
	$('.estCampaignCost').html(campaignCost).numberAnimate(false);
	$('.avgCostPerFollower').html((avgCostPerFollower / id.length) * 1000).numberAnimate(false);
	$('.cost-dollar').removeClass('d-none');
	var activeNavPanelCostAnalytics = document.getElementById("costAnalyticsNavPanel").getElementsByClassName("active")[0].id;
	if (activeNavPanelCostAnalytics == "estPricePerInfluencerNavLabel") {
		buildChartWithScroll('estPricePerInfluencer', 'horizontalBar');
	} else if (activeNavPanelCostAnalytics == "costPerEngagementNavLabel") {
		buildChartWithScroll('costPerEngagement', 'horizontalBar');
	} else if (activeNavPanelCostAnalytics == "costPerFollowerNavLabel") {
		buildChartWithScroll('costPerFollower', 'horizontalBar');
	}
}

$('#estPricePerInfluencerNavLabel').on('click', function () {
	if (estPricePerInfluencer != undefined && estPricePerInfluencer.labels[0] != 'No data') {
		buildChartWithScroll('estPricePerInfluencer', 'horizontalBar');
	}
})

$('#costPerEngagementNavLabel').on('click', function () {
	if (costPerEngagement != undefined && costPerEngagement.labels[0] != 'No data') {
		buildChartWithScroll('costPerEngagement', 'horizontalBar');
	}
})

$('#costPerFollowerNavLabel').on('click', function () {
	if (costPerFollower != undefined && costPerFollower.labels[0] != 'No data') {
		buildChartWithScroll('costPerFollower', 'horizontalBar');
	}
})

$(document).click(function (e) {
	var container = $(".m-select-options-container.active");

	if (!container.is(e.target) && container.has(e.target).length === 0 && container.hasClass('active')) {
		$(".m-select-options-container").removeClass('active');
	}
});

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

$('#postStatisticsLabel').on('click', function () {
	setTimeout(() => {
		$('#engagementNavLabel').click();
		createPostEngagementChart();
	}, 10);
})

$('#campaignPost').on('change', function () {
	var selectPlatform = '<option value="instagram">Instagram</option>' +
		'<option value="youtube">YouTube</option>' +
		'<option value="tiktok">TikTok</option>';
	$("#post-platform").empty();
	$("#post-platform").append(selectPlatform);
	var campaignId = $('#campaignPost').val().trim();
	getInfluencerDetailsForPostStatistics(campaignId);
})

$('#post-platform').on('change', function () {
	var platform = $("#post-platform").val();
	var campaignId = $('#campaignPost').val().trim();
	processInfluencerDetailsForPostStatistics();

	if (platform == "youtube" || platform == "tiktok") {
		$('#you-tik-total-views').removeClass("d-none");
		$('#postviews').removeClass("d-none");
		$('#totalViewsTab').addClass("d-none");
		if (platform == "youtube") {
			$('#totalViewsTab').removeClass("d-none");
		}
	} else if (platform == "instagram") {
		$('#you-tik-total-views').addClass("d-none");
		$('#postviews').addClass("d-none");
		$('#totalViewsTab').addClass("d-none");
	}
});

var postJsonData;
var viewChartBy = 'engagement';

function processPostChart(likes, comments, views, engagement, viewType) {
	viewChartBy = viewType;
	document.getElementById('postlikes').style.backgroundColor = likes;
	document.getElementById('postcomments').style.backgroundColor = comments;
	document.getElementById('postviews').style.backgroundColor = views;
	document.getElementById('postengagement').style.backgroundColor = engagement;
	createPostEngagementChart();
}

var influncerDetailsForPostStatistics;

function getInfluencerDetailsForPostStatistics(campaignId) {
	$.get({
		url: window.location.origin + '/brand/campaign/' + campaignId + '/postAnalytics',
	}).done(function (data) {
		influncerDetailsForPostStatistics = data;
		processInfluencerDetailsForPostStatistics();
	}).fail(function (data) {
		console.log(data);
	});
}

function processInfluencerDetailsForPostStatistics() {
	var data = influncerDetailsForPostStatistics;
	var influencers = {};
	postJsonData = data;
	var content = '';
	var postSelectedPlatform = '';
	document.getElementById('postlikes').disabled = !isEmpty(data) ? false : true;
	document.getElementById('postengagement').disabled = !isEmpty(data) ? false : true;
	document.getElementById('postcomments').disabled = !isEmpty(data) ? false : true;
	document.getElementById('postviews').disabled = !isEmpty(data) ? false : true;
	document.getElementById('postlikes').style.backgroundColor = (!isEmpty(data) && viewChartBy == 'likes') ? '#2d9994' : '#2f008d';
	document.getElementById('postcomments').style.backgroundColor = (!isEmpty(data) && viewChartBy == 'comments') ? '#2d9994' : '#2f008d';
	document.getElementById('postengagement').style.backgroundColor = (!isEmpty(data) && viewChartBy == 'engagement') ? '#2d9994' : '#2f008d';
	document.getElementById('postviews').style.backgroundColor = (!isEmpty(data) && viewChartBy == 'views') ? '#2d9994' : '#2f008d';

	if (!isEmpty(data)) {
		for (var i in data) {
			var influencer = data[i];

			if (influencer.platform == $('#post-platform').val()) {
				postSelectedPlatform = influencer.platform;
				influencers[influencer.influencerId] = influencer.influencerHandle;
			}
		}
		for (var k in influencers) {
			content += '<option value="' + k + '">' + influencers[k] + '</option>';
		}
		if (postSelectedPlatform == $('#post-platform').val()) {
			$('#influencerPost').html(content);
		} else {
			$('.m-select-all-label.influencerPost').click();
			$('#influencerPost').html('');
		}
		mSelectPost.rebuild();
		$('.m-select-all-label.influencerPost').click();
	} else {
		$('.m-select-all-label.influencerPost').click();
		$('#influencerPost').html('');
		mSelectPost.rebuild();
		emptyDataMessage = "Once you have added a post link, come back in an hour to track performance.";
		totalLikesChart = {};
		totalCommentsChart = {};
		totalEngagementChart = {};
		postCount = {};
		createPostEngagementChart();
	}
}

$('#influencerPost').on('change', function () {
	var ids = $('#influencerPost').val();
	if (ids.length == 0) {
		emptyDataMessage = "Please select an influencer";
	}
	var influencerDatas = [];
	var influencerLikes = {};
	var influencerComments = {};
	var influencerViews = {};
	var influencerPostNames = {};
	var influencerEngagement = {};
	var checkDate = new Set();
	totalLikes = 0;
	totalComments = 0;
	totalViews = 0;
	var likes = {};
	var comments = {};
	var views = {};
	postCount = {};

	for (var i = 0; i < ids.length; i++) {
		influencerDatas = influencerDatas.concat(postJsonData.filter(d => d.influencerId == ids[i]));
	}
	for (var i in influencerDatas) {
		var postNames = {};
		var likesChart = {};
		var commentsChart = {};
		var viewsChart = {};
		var engagementChart = {};
		var influencer = influencerDatas[i];
		var updatedDate = moment.utc((influencer["updatedData"])).local().format('YYYY-MM-DD HH:mm:ss');
		checkDate.add(updatedDate);

		postNames[updatedDate] = influencer["postName"];
		likesChart[updatedDate] = influencer["likes"];
		commentsChart[updatedDate] = influencer["comments"];
		viewsChart[updatedDate] = influencer["views"];
		engagementChart[updatedDate] = influencer["likes"] + influencer["comments"];

		if ((likes.hasOwnProperty(influencer["postId"]) && new Date(updatedDate) > new Date(Object.keys(likes[influencer["postId"]])))
			|| !likes.hasOwnProperty(influencer["postId"])) {
			var subLikes = {};
			subLikes[updatedDate] = influencer["likes"];
			likes[influencer["postId"]] = subLikes;
		}
		if ((comments.hasOwnProperty(influencer["postId"]) && new Date(updatedDate) > new Date(Object.keys(comments[influencer["postId"]])))
			|| !comments.hasOwnProperty(influencer["postId"])) {
			var subComments = {};
			subComments[updatedDate] = influencer["comments"];
			comments[influencer["postId"]] = subComments;
		}
		if ((views.hasOwnProperty(influencer["postId"]) && new Date(updatedDate) > new Date(Object.keys(views[influencer["postId"]])))
			|| !views.hasOwnProperty(influencer["postId"])) {
			var subViews = {};
			subViews[updatedDate] = influencer["views"];
			views[influencer["postId"]] = subViews;
		}

		if (!influencerPostNames[influencer["postId"]]) {
			influencerPostNames[influencer["postId"]] = postNames;
		} else {
			influencerPostNames[influencer["postId"]] = $.extend({}, postNames, influencerPostNames[influencer["postId"]]);
		}

		if (!influencerLikes[influencer["postId"]]) {
			influencerLikes[influencer["postId"]] = likesChart;
		} else {
			influencerLikes[influencer["postId"]] = $.extend({}, likesChart, influencerLikes[influencer["postId"]]);
		}

		if (!influencerComments[influencer["postId"]]) {
			influencerComments[influencer["postId"]] = commentsChart;
		} else {
			influencerComments[influencer["postId"]] = $.extend({}, commentsChart, influencerComments[influencer["postId"]]);
		}

		if (!influencerViews[influencer["postId"]]) {
			influencerViews[influencer["postId"]] = viewsChart;
		} else {
			influencerViews[influencer["postId"]] = $.extend({}, viewsChart, influencerViews[influencer["postId"]]);
		}

		if (!influencerEngagement[influencer["postId"]]) {
			influencerEngagement[influencer["postId"]] = engagementChart;
		} else {
			influencerEngagement[influencer["postId"]] = $.extend({}, engagementChart, influencerEngagement[influencer["postId"]]);
		}
	}
	for (var k in likes) {
		for (var l in likes[k]) {
			totalLikes += likes[k][l];
		}
	}
	for (var k in comments) {
		for (var l in comments[k]) {
			totalComments += comments[k][l];
		}
	}
	for (var k in views) {
		for (var l in views[k]) {
			totalViews += views[k][l];
		}
	}
	var dates = Array.from(checkDate).sort();
	totalLikesChart = {};
	totalCommentsChart = {};
	totalViewsChart = {};
	totalEngagementChart = {};

	dates = checkMissingDates(dates, dates[dates.length - 1]);
	totalLikesChart = engagementCalculator(influencerLikes, dates, influencerPostNames);
	totalCommentsChart = engagementCalculator(influencerComments, dates, influencerPostNames);
	totalViewsChart = engagementCalculator(influencerViews, dates, influencerPostNames);
	totalEngagementChart = engagementCalculator(influencerEngagement, dates, influencerPostNames);
	createPostEngagementChart();
})

var totalViews = 0;
var totalLikes = 0;
var totalComments = 0;
var totalLikesChart = {};
var totalCommentsChart = {};
var totalViewChart = {};
var totalEngagementChart = {};
var postCount = {};
var emptyDataMessage;

async function createPostEngagementChart() {
	var chartLayout;
	var postEngagementDate = [];
	var postEngagementData = [];
	var postCountData = [];
	var averageEngagement = 0;

	if (!isEmpty(totalLikesChart) && !isEmpty(totalCommentsChart) && !isEmpty(totalViewsChart) && !isEmpty(totalEngagementChart) && !isEmpty(postCount)) {
		var ordered = {};
		if (viewChartBy == 'likes') {
			Object.keys(totalLikesChart).sort().forEach(function (key) {
				ordered[key] = totalLikesChart[key];
			});
		}
		else if (viewChartBy == 'comments') {
			Object.keys(totalCommentsChart).sort().forEach(function (key) {
				ordered[key] = totalCommentsChart[key];
			});
		}
		else if (viewChartBy == 'views') {
			Object.keys(totalViewsChart).sort().forEach(function (key) {
				ordered[key] = totalViewsChart[key];
			});
		}
		else {
			Object.keys(totalEngagementChart).sort().forEach(function (key) {
				ordered[key] = totalEngagementChart[key];
			});
		}

		for (var a in ordered) {
			postEngagementDate.push(a);
			postEngagementData.push(ordered[a]);
			averageEngagement += ordered[a];
		}
		// sorting post count
		var orderedPostCount = {};
		Object.keys(postCount).sort().forEach(function (key) {
			orderedPostCount[key] = postCount[key];
		});
		for (var b in orderedPostCount) {
			postCountData.push('PostName : ' + orderedPostCount[b]);
		}
		// averageEngagement = averageEngagement/Object.keys(ordered).length;
		chartLayout = {
			/*
			 * title : { text : 'Total Engagement', x : 0.5, y : 0.97, },
			 */
			autosize: true,
			margin: {
				l: 50,
				r: 50,
				b: 30,
				t: 50,
			},
			xaxis: {
				type: 'date',
				hoverformat: '%Y-%m-%d %I:%M %p',
				tickformat: '%b %d,%Y',
				/*
				 * dtick : 'M1', tickvals : tickText, ticktext : tickVal
				 */
			},
			yaxis: {
				autorange: true,
				type: 'linear',
				tickformat: '.3s'
			}
		};
		if ($('#post-platform').val() == "youtube") {
			$('#averageEngagementPost').html(totalLikes + totalComments + totalViews).numberAnimate(false, 0);
		} else {
			$('#averageEngagementPost').html(totalLikes + totalComments).numberAnimate(false, 0);
		}
		$('#totalLikes').html(totalLikes).numberAnimate(false, 0);
		$('#totalComments').html(totalComments).numberAnimate(false, 0);
		$('#totalViews').html(totalViews).numberAnimate(false, 0);
	} else {
		var message = 'No data available. Please select campaign';
		if (!isEmpty(emptyDataMessage)) {
			message = emptyDataMessage;
		}
		$('#averageEngagementPost').html('-');
		$('#totalLikes').html('-');
		$('#totalComments').html('-');
		chartLayout = {
			/*
			 * title : { text :'Total Engagement', x : 0.5, y : 0.97, },
			 */
			autosize: true,
			margin: {
				l: 50,
				r: 50,
				b: 30,
				t: 50,
			},
			annotations: [
				{
					text: message,
					showarrow: false,
					font: {
						size: 18,
						color: '#00000040'
					},
				}],
			xaxis: {
				autotick: true,
				type: 'date',
				// tickformat : '%Y-%m-%d %I:%M %p',
				range: [moment(new Date().addHours(-4380)).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD')]
			},
			yaxis: {
				autorange: true,
				type: 'linear',
				tickformat: '.3s'
			}
		};
	}

	await sleep(300);
	var postStatisticsEngagement = document.getElementById('postStatisticsEngagement');
	Plotly.purge(postStatisticsEngagement);
	var ChartData = [{
		x: postEngagementDate,
		y: postEngagementData,
		type: 'scatter',
		mode: postEngagementData.length > 2 ? 'lines' : 'lines+markers',
		line: {
			color: '#007EBD',
			width: 1.3
		},
		text: postCountData,
	}];

	var options = {
		responsive: true,
		modeBarButtons: [[{
			name: 'Fullscreen',
			icon: Plotly.Icons.autoscale,
			click: function (elem) {
				toggleFullScreen(elem);
			}
		}, 'toImage', 'zoomIn2d', 'zoomOut2d', 'resetScale2d']]
	}
	Plotly.react(postStatisticsEngagement, ChartData, chartLayout,
		options);
}

function toggleFullScreen(elem) {
	document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement ? document.exitFullscreen ? document.exitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.msExitFullscreen && document.msExitFullscreen() : (element = elem, element.requestFullscreen ? element.requestFullscreen() : element.mozRequestFullScreen ? element.mozRequestFullScreen() : element.webkitRequestFullscreen ? element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT) : element.msRequestFullscreen && element.msRequestFullscreen())
}

function engagementCalculator(jsonObj, dateArr, jsonObjPostName) {
	var objKeys = Object.keys(jsonObj);
	var objPostNameKeys = Object.keys(jsonObjPostName);
	var outJson = {};
	postCount = {};

	for (var i = 0; i < objKeys.length; i++) {
		var objKey = objKeys[i];
		var objPostNameKey = objPostNameKeys[i];
		var post = jsonObj[objKey];
		var postName = jsonObjPostName[objPostNameKey];
		var dateKeys = dateArr;
		var firstDateFound = false;

		for (var j = 0; j < dateKeys.length; j++) {
			var dateKey = dateKeys[j];
			var data = post[dateKey];
			var jdx = j;

			if (data == undefined) {
				data = 0;
			} else {
				firstDateFound = true;
			}
			while (data <= 0 && jdx >= 0) {
				data = post[dateKeys[--jdx]];
				if (!data)
					data = 0;
			}
			if (!outJson[dateKey]) {
				outJson[dateKey] = 0;
			}
			outJson[dateKey] += data;

			if (firstDateFound && !postCount[dateKey]) {
				postCount[dateKey] = postName[dateKey];
			}
		}
	}

	for (var k in postCount) {
		var name;
		if (postCount[k] != undefined) {
			name = postCount[k]
		}
		if (postCount[k] == undefined) {
			postCount[k] = name;
		}
	}
	return outJson;
}

Date.prototype.addHours = function (h) {
	this.setHours(this.getHours() + h);
	return this;
}

function interpolateTheData(unorderedEngagement) {
	var engagement = {};
	Object.keys(unorderedEngagement).sort().forEach(function (key) {
		engagement[key] = unorderedEngagement[key];
	});
	var nextDate;
	var count = 0;
	var currentTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');

	for (var date in engagement) {
		count += 1;
		var dataToFind = [];
		var startDate = {};
		var endDate = {};
		var endDateFound = false;

		nextDate = moment(new Date(date).addHours(1)).format('YYYY-MM-DD HH:mm:ss');
		var nextData = engagement[nextDate];

		if (!nextData && count < Object.keys(engagement).length && (new Date(nextDate) < new Date(currentTime))) {
			startDate[date] = engagement[date];
			while (!endDateFound) {
				dataToFind.push(nextDate);
				nextDate = moment(new Date(nextDate).addHours(1)).format('YYYY-MM-DD HH:mm:ss');
				nextData = engagement[nextDate];

				if (nextData) {
					endDateFound = true;
					endDate[nextDate] = nextData;
				}
			}
			count = interpolation(dataToFind, startDate, endDate, engagement, count);
		}
	}
	return engagement;
}

function interpolation(dataToFind, startDate, endDate, engagement, count) {
	for (let i = 0; i < dataToFind.length; i++) {
		count = interpolate(startDate, endDate, engagement, count, dataToFind[i]);
	}

	return count;
}

function interpolate(startDate, endDate, engagement, count, unknownDate) {
	var unknownDateInTime = parseInt(new Date(unknownDate).getTime());
	var startValue = Object.values(startDate);
	var endValue = Object.values(endDate);
	var endDateInTime = parseInt(new Date(Object.keys(endDate)).getTime());
	var startDateInTime = parseInt(new Date(Object.keys(startDate)).getTime());
	var unknownData = Math.round((((endValue[0] - startValue[0]) * (unknownDateInTime - startDateInTime)) / (endDateInTime - startDateInTime)) + startValue[0]);
	engagement[moment(new Date(unknownDateInTime)).format('YYYY-MM-DD HH:mm:ss')] = unknownData;
	return count += 1;
}

function checkMissingDates(checkDate, endDate) {
	var missingDates = new Set();
	checkDate.forEach(function (date) {
		var nextDate = moment.utc((new Date(date).addHours(1))).local().format('YYYY-MM-DD HH:mm:ss');
		while (!checkDate.includes(nextDate) && nextDate < endDate) {
			missingDates.add(nextDate);
			nextDate = moment(new Date(nextDate).addHours(1)).format('YYYY-MM-DD HH:mm:ss');
		}
	});
	var s = Array.from(missingDates);
	return checkDate.concat(s).sort();
}

$(document).ready(function () {
	var campaignId = readCookie("campaignId");
	if (campaignId) {
		$('#campaign').val(campaignId).change();
		$('#campaignPost').val(campaignId).change();
		deleteCookie("campaignId");
	}
});

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function deleteCookie(name) {
	document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

$(window).on('beforeunload', function (e) {
	deleteCookie("campaignId");
});