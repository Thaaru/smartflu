package io.smartfluence.mysql.entities.influencer;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "platform_master")
public class PlatformMaster {
    @Id
//    @Column(name = "platform_id")
    private Long platformId;

//    @Column(name = "platform_name")
    private String platformName;
}
