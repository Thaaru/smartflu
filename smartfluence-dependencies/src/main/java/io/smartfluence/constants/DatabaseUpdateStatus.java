package io.smartfluence.constants;

public enum DatabaseUpdateStatus {
	INPROGRESS, COMPLETED
}