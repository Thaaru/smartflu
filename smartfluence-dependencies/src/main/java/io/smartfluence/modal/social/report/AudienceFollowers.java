package io.smartfluence.modal.social.report;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudienceFollowers implements Serializable {

	private static final long serialVersionUID = -8756285650706733248L;

	@JsonAlias("data")
	private AudienceFollowersData data;
}