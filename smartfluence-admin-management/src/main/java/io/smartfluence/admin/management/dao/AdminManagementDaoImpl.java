package io.smartfluence.admin.management.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.DatabaseUpdateHistory;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSubscriptions;
import io.smartfluence.cassandra.entities.instagram.InstagramContent;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.primary.SubscriptionMasterKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.repository.brand.BrandCreditsRepo;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandSubscriptionsRepo;
import io.smartfluence.cassandra.repository.DatabaseUpdateHistoryRepo;
import io.smartfluence.cassandra.repository.ReportSearchHistoryRepo;
import io.smartfluence.cassandra.repository.SubscriptionMasterRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramContentRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramDemographicRepo;
import io.smartfluence.constants.DatabaseUpdateStatus;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.UserAccountsRepo;

@Repository
public class AdminManagementDaoImpl implements AdminManagementDao {

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Autowired
	private SubscriptionMasterRepo subscriptionMasterRepo;

	@Autowired
	private BrandCreditsRepo brandCreditsRepo;

	@Autowired
	private BrandSubscriptionsRepo brandSubscriptionsRepo;

	@Autowired
	private DatabaseUpdateHistoryRepo databaseUpdateHistoryRepo;

	@Autowired
	private InstagramDemographicRepo instagramDemographicRepo;

	@Autowired
	private InstagramContentRepo instagramContentRepo;

	@Autowired
	private ReportSearchHistoryRepo reportSearchHistoryRepo;

	@Override
	public void saveBrandDetails(BrandDetails brandDetails) {
		brandDetailsRepo.save(brandDetails);
	}

	@Override
	public void deleteBrandDetails(BrandDetails brandDetails) {
		brandDetailsRepo.delete(brandDetails);
	}

	@Override
	public Optional<BrandDetails> getBrandDetails(UserDetailsKey key) {
		return brandDetailsRepo.findById(key);
	}

	@Override
	public Set<BrandDetails> getBrandDetailsByUserStatus(Set<UserStatus> userStatus) {
		return brandDetailsRepo.findAllByKeyUserStatusIn(userStatus);
	}

	@Override
	public Optional<BrandDetails> getBrandDetailsByUserStatusAndUserId(Set<UserStatus> userStatus, UUID userId) {
		return brandDetailsRepo.findByKeyUserStatusInAndKeyUserId(userStatus, userId);
	}

	@Override
	public void saveUserAccounts(UserAccounts userAccounts) {
		userAccountsRepo.save(userAccounts);
	}

	@Override
	public Optional<UserAccounts> getUserAccounts(String brandId) {
		return userAccountsRepo.findById(brandId);
	}

	@Override
	public SubscriptionMaster getSubscriptionMaster(UUID subscriptionId, SubscriptionType subscriptionType) {
		return subscriptionMasterRepo.findById(new SubscriptionMasterKey(subscriptionType, subscriptionId)).get();
	}

	@Override
	public List<SubscriptionMaster> getAllSubscriptionMaster() {
		return subscriptionMasterRepo.findAll();
	}

	@Override
	public void saveBrandSubscriptions(BrandSubscriptions brandSubscriptions) {
		brandSubscriptionsRepo.save(brandSubscriptions);
	}

	@Override
	public void deleteBrandSubscriptions(BrandSubscriptions brandSubscriptions) {
		brandSubscriptionsRepo.delete(brandSubscriptions);
	}

	@Override
	public Optional<BrandSubscriptions> getBrandSubscriptionsByBrandIdAndIsActive(UUID brandId) {
		return brandSubscriptionsRepo.findByKeyBrandIdAndKeyActiveIsTrue(brandId);
	}

	@Override
	public void saveBrandCredits(BrandCredits brandCredits) {
		brandCreditsRepo.save(brandCredits);
	}

	@Override
	public List<InstagramDemographics> getInfluencers() {
		return instagramDemographicRepo.findAll();
	}

	@Override
	public DatabaseUpdateHistory saveInDBHistory() {
		return databaseUpdateHistoryRepo.save(DatabaseUpdateHistory.builder().createdAt(new Date())
				.id(UUID.randomUUID()).startDate(new Date()).status(DatabaseUpdateStatus.INPROGRESS).build());
	}

	@Override
	public List<InstagramContent> getInstagramContent(String influencerHandle) {
		return instagramContentRepo.findAllByKeyInfluencerHandle(influencerHandle);
	}

	@Override
	public List<ReportSearchHistory> findAllBySearchId(UUID searchId) {
		return reportSearchHistoryRepo.findAllBySearchId(searchId);
	}

	@Override
	public void updateDB(List<InstagramDemographics> instagramDemographics, List<InstagramContent> contents, UUID updateId) {
		instagramDemographicRepo.saveAll(instagramDemographics);
		instagramContentRepo.saveAll(contents);
		Optional<DatabaseUpdateHistory> optionalDbUpdateHistory = databaseUpdateHistoryRepo.findById(updateId);
		if (optionalDbUpdateHistory.isPresent()) {
			DatabaseUpdateHistory databaseUpdateHistory = optionalDbUpdateHistory.get();
			databaseUpdateHistory.setEndDate(new Date());
			databaseUpdateHistory.setStatus(DatabaseUpdateStatus.COMPLETED);
			databaseUpdateHistoryRepo.save(databaseUpdateHistory);
		}
	}

	@Override
	public List<DatabaseUpdateHistory> getDBUpdateHistory() {
		return databaseUpdateHistoryRepo.findAll();
	}
}