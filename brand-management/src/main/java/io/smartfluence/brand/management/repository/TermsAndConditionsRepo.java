package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.TermsAndConditions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TermsAndConditionsRepo extends CrudRepository<TermsAndConditions, Long> {
    List<TermsAndConditions> findByTermId(long termId);
}
