package io.smartfluence.modal.social.report;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class NameAndWeight implements Serializable {

	private static final long serialVersionUID = 3922147772259122926L;

	@JsonAlias("name")
	private String name;

	@JsonAlias("weight")
	private Double weight;
}