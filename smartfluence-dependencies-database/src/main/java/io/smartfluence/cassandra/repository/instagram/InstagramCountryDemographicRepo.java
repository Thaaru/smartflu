package io.smartfluence.cassandra.repository.instagram;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.instagram.InstagramCountryDemographic;
import io.smartfluence.cassandra.entities.primary.InfluencerCountryKey;

@Repository
public interface InstagramCountryDemographicRepo extends CassandraRepository<InstagramCountryDemographic, InfluencerCountryKey> {

	List<InstagramCountryDemographic> findAllByKeyInfluencerHandle(String influencerHandle);

	List<InstagramCountryDemographic> findAllByKeyInfluencerHandleIn(List<String> influencerHandles);
}