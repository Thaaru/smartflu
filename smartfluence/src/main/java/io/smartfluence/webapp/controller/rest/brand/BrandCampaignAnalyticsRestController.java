package io.smartfluence.webapp.controller.rest.brand;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.webapp.proxy.BrandManagementServiceProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandCampaignAnalyticsRestController {

	@Autowired
	private BrandManagementServiceProxy brandManagementServiceProxy;

	@GetMapping("campaign/{campaignId}/analytics")
	public ResponseEntity<Object> getAnalyticsData(@PathVariable UUID campaignId) {
		log.info(campaignId);
		return brandManagementServiceProxy.getAnalyticsData(campaignId);
	}

	@GetMapping("campaign/{campaignId}/postAnalytics")
	public ResponseEntity<Object> getPostAnalyticsData(@PathVariable UUID campaignId) {
		log.info(campaignId);
		return brandManagementServiceProxy.getPostAnalyticsData(campaignId);
	}
}