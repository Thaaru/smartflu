package io.smartfluence.mysql.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.mysql.entities.IndustryMaster;

@Repository
public interface IndustryMasterRepo extends CrudRepository<IndustryMaster, Long> {

	Optional<IndustryMaster> findByIndustry(String industry);

	List<IndustryMaster> findAllByStatus(int status);

	List<IndustryMaster> findAllByIndustryIgnoreCaseContainingAndStatus(String name, int status);
}