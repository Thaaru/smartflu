package io.smartfluence.modal.validation.brand.promote;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.smartfluence.modal.validation.custom.Conditional;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Conditional
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PromoteCampaignRequestModel {

	private String campaignId;

	@NotBlank(message = "Please provide Campaign Name")
	private String campaignName;

	@NotBlank(message = "Please provide Brand Description")
	private String brandDescription;

	@NotBlank(message = "Please provide Campaign Description")
	private String campaignDescription;

	private String postType;

	private Double payment = 0.0;

	private String product;

	private Double productValue = 0.0;

	private String requirements;

	private String restrictions;

	@NotBlank(message = "Please select Active Until Date")
	private String activeUntil;

	@NotBlank(message = "Please provide Comparison profile")
	private String comparisonProfile;

	@NotBlank(message = "Please provide Platform")
	private String platform;

	@NotBlank(message = "Please provide FollowersRange")
	private String followersRange;

	private String engagementsRange;

	@NotBlank(message = "Please select a audience industry")
	private String audienceIndustry;

	private String influencerIndustry;

	@NotBlank(message = "Please provide audience location")
	private String audienceGeo;

	private String influencerGeo;

	private String audienceAge;

	private String influencerAge;

	private String audienceGender;

	private String influencerGender;

	@NotNull(message = "Please provide isEdit")
	private Boolean isEdit;

	private Date activeDate;
}