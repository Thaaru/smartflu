package io.smartfluence.util.mail;

class Info extends AbstractMailContent {

	private static final String SECTION_PREFIX = "<tr><td style=\"padding-top: 12px;padding-bottom: 12px;\"><span style=\"font-size: 17px;font-weight: bold;\">";

	private static final String SECTION_SUFFIX = "</span></td></tr>";

	private static final String ITEM_PREFIX = "<p style=\"margin: 0;font-family: Helvetica, Arial, sans-serif;\">";

	private static final String ITEM_SUFFIX = "</p>";

	@Override
	protected String getItemPrefix() {
		return ITEM_PREFIX;
	}

	@Override
	protected String getItemSuffix() {
		return ITEM_SUFFIX;
	}

	public Info(SmartMailBuilder smartMailBuilder) {
		this.setBuilder(smartMailBuilder);
	}

	@Override
	public StringBuilder content() {
		String content= super.content().toString();
		super.content().setLength(0);
		return super.content().append(SECTION_PREFIX).append(content).append(SECTION_SUFFIX);
	}
}