package io.smartfluence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.smartfluence.brand.management.mailbean.MailService;

@ControllerAdvice
public class CustomErrorHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MailService mailservice;

	@ExceptionHandler(NullPointerException.class)
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		mailservice.sendErrorMail(ex);
		return handleExceptionInternal(ex, "INTERNAL_SERVER_ERROR", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,
				request);
	}
}