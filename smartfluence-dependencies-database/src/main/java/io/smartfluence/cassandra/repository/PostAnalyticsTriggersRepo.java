package io.smartfluence.cassandra.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.PostAnalyticsTriggers;
import io.smartfluence.cassandra.entities.primary.PostAnalyticsTriggersKey;

public interface PostAnalyticsTriggersRepo	extends CassandraRepository<PostAnalyticsTriggers, PostAnalyticsTriggersKey> {

	List<PostAnalyticsTriggers> findAllByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(UUID brandId, UUID campaignId, UUID influencerId);

	List<PostAnalyticsTriggers> findAllByKeyBrandIdAndKeyCampaignId(UUID brandId, UUID campaignId);

	List<PostAnalyticsTriggers> findAllByIsTriggerEnd(Boolean isTriggerEnd);
}