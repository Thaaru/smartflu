package io.smartfluence.cassandra.repository.brand;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.brand.BrandPaymentDetails;

@Repository
public interface BrandPaymentDetailsRepo extends CassandraRepository<BrandPaymentDetails, UUID> {

}