package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.PlatformMasterV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlatformMasterRepositoryV1 extends JpaRepository<PlatformMasterV1,Long> {
    PlatformMasterV1 findByPlatformId(Long platformId);
}
