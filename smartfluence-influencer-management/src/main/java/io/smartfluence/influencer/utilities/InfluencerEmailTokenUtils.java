package io.smartfluence.influencer.utilities;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.smartfluence.influencer.entities.CampaignInfluencerId;
import io.smartfluence.influencer.repository.BrandCampaignDetailsRepo;
import io.smartfluence.influencer.repository.CampaignInfluencerDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

@Component
public class InfluencerEmailTokenUtils {

    @Autowired
    BrandCampaignDetailsRepo brandCampaignDetailsRepo;

    @Autowired
    CampaignInfluencerDetailsRepo campaignInfluencerDetailsRepo;




    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    //for retrieveing any information from token we will need the secret key
    public Claims getAllClaimsFromToken(String token) {

        return Jwts.parser().setSigningKey("3kODgDGTGsV1oc1yNBbxlKse").parseClaimsJws(token).getBody();
    }

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return new Date().before(expiration);
    }

    public CommonConstantsEnum validateToken(String token) {

        if (isTokenExpired(token)) {
            final Claims tokenData = getAllClaimsFromToken(token);
            String campaignId = (String) tokenData.get("campaignId");
            String influencerId = (String) tokenData.get("influencerId");
            CampaignInfluencerId campaignInfluencerId = new CampaignInfluencerId(campaignId, influencerId);

                if (campaignInfluencerDetailsRepo.findByCampaignInfluencerId(campaignInfluencerId) != null) {
                    return CommonConstantsEnum.SUCCESS;

                } else {
                    return CommonConstantsEnum.FAILURE;
                }
            } else {
                return CommonConstantsEnum.EXPIRED;
            }

    }
}
