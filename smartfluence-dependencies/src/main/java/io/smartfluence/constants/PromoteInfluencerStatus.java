package io.smartfluence.constants;

public enum PromoteInfluencerStatus {
	NEW, SENT, NO_CONTACT, PENDING, APPROVED, REJECTED, CONTENT_PENDING, CONTENT_APPROVED, REMOVED
}