import React from "react";
import { IoLogoInstagram, IoLogoYoutube } from "react-icons/io";

export const getPlatformAndIcon = data => {
    const tempList = [];
    if (data != null) {
        const deliverables = data.deliverables;
        const platFormDetailsList = data.platFormDetailsList;
        const platformPostTypeDetailsList = data.platformPostTypeDetailsList;
        for (let i = 0; i < deliverables.length; i++) {
            const tempObject = {};
            const platformId = deliverables[i].platformId;
            const postId = deliverables[i].postTypeId;
            tempObject["guidlines"] = deliverables[i].guildlines;
            for (let j = 0; j < platFormDetailsList.length; j++) {
                if (platformId == platFormDetailsList[j].platformId) {
                    tempObject["platformName"] = platFormDetailsList[j].platformName;
                    if (platFormDetailsList[j].platformName == "Instagram") {
                        tempObject["platformIcon"] = <IoLogoInstagram size={25} />;
                    } else if (platFormDetailsList[j].platformName == "Tiktok") {
                        tempObject["platformIcon"] = <IoLogoInstagram size={25} />;
                    } else {
                        tempObject["platformIcon"] = <IoLogoYoutube size={25} />;
                    }
                }
            }
            for (let k = 0; k < platformPostTypeDetailsList.length; k++) {
                if (postId == platformPostTypeDetailsList[k].postTypeId) {
                    if (platformPostTypeDetailsList[k].postTypeName == "Custom") {
                        for (let index = 0; index < deliverables.length; index++) {
                            if (platformPostTypeDetailsList[k].postTypeId == deliverables[index].postTypeId) {
                                tempObject["postTypeName"] = deliverables[index].postCustomName;
                            }
                        }

                    }

                    else
                        tempObject["postTypeName"] = platformPostTypeDetailsList[k].postTypeName;
                }
            }
            tempList.push(tempObject);
        }
    }
    return tempList;
}