package io.smartfluence;

import java.util.Map;
import java.util.UUID;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.context.request.RequestContextListener;

import io.smartfluence.constants.SecurityConstants;

@Configuration
@EnableResourceServer
@Order(SecurityProperties.BASIC_AUTH_ORDER - 1)
public class ResourceServer extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.httpBasic().disable()
		.authorizeRequests()
		.antMatchers("/influencer/pub/**").permitAll()
		.antMatchers("/admin/**").hasAuthority(SecurityConstants.ADMIN.getAuthority())
		.antMatchers("/brand/**").hasAuthority(SecurityConstants.BRAND.getAuthority())
		.antMatchers("/influencer/**").hasAuthority(SecurityConstants.INFLUENCER.getAuthority())
		.antMatchers("/static/**").permitAll();
	}

	public static UUID getUserId() {
		OAuth2Authentication authentication = getAuthentication();

		if (!(authentication.getAuthorities().contains(SecurityConstants.ADMIN)
		        || authentication.getAuthorities().contains(SecurityConstants.SMART_ADMIN))) {
			Object principalObj = authentication.getUserAuthentication().getDetails();

			if (principalObj instanceof Map<?, ?>) {
				@SuppressWarnings("unchecked")
				Map<String, Map<String, Object>> principal = (Map<String, Map<String, Object>>) principalObj;

				if (principal.containsKey("principal")) {
					Map<String, Object> userPrincipal = principal.get("principal");

					if (userPrincipal.containsKey("userId"))
						return UUID.fromString(userPrincipal.get("userId").toString());
				}
			}
			throw new InternalAuthenticationServiceException("No authentication object found");
		}
		return null;
	}

	private static OAuth2Authentication getAuthentication() {
		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();

		if (authentication == null)
			throw new InternalAuthenticationServiceException("No authentication object found");

		return authentication;
	}

	public static boolean isAdmin() {
		return getAuthentication().getAuthorities().contains(SecurityConstants.ADMIN);
	}

	@Bean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}
}
