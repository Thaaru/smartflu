package io.smartfluence.webapp.proxy;

import io.smartfluence.FeignConfiguration;
import io.smartfluence.mysql.entities.influencer.FullCampaignInfluencerDetails;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "smartfluence-influencer", configuration = FeignConfiguration.class)
@RibbonClient(name = "smartfluence-influencer")
public interface InfluencerManagementServiceProxy {

	@GetMapping("influencer/pub/campaign-influencer-details")
	public ResponseEntity<Object> getCampaignInfluencerDetails(@RequestParam("token") String emailTokenCode);

	@PutMapping("influencer/pub/campaign-influencer-details")
	public ResponseEntity<Object> updateCampaignInfluencerDetails(@RequestBody FullCampaignInfluencerDetails fullCampaignInfluencerDetails, @RequestParam("token") String emailTokenCode);

	@GetMapping("influencer/pub/states/{countryId}")
	public ResponseEntity<Object> getStatesDetails(@PathVariable Long countryId);


	@GetMapping("influencer/current-partnership")
	public ResponseEntity<Object> getcurrentpartnership();

	@GetMapping("influencer")
	public String getPubPage();

//	@PostMapping("influencer/social-connect")
//	public ResponseEntity<Object> saveInstagramHandle(@RequestParam String instagramHandle);
//
//	@PostMapping("influencer/profile")
//	public ResponseEntity<Object> saveProfile(@RequestParam String paypalemail, @RequestParam String address1,
//			@RequestParam String address2, @RequestParam String city, @RequestParam String country,
//			@RequestParam String dob, @RequestParam String gender, @RequestParam String state);
//
//	@GetMapping("influencer/profile")
//	public ResponseEntity<Object> getProfileDetails();
//
//	@PostMapping("influencer/influencer-preference")
//	public ResponseEntity<Object> updateInfluencerPreference(@RequestParam String description,
//			@RequestParam String industry, @RequestParam String location, @RequestParam Double instaStory2Hr,
//			@RequestParam Double instaStory24Hr, @RequestParam Double instaPost24Hr, @RequestParam Double instaPost1W,
//			@RequestParam Double instaPostPerm);
//
//	@GetMapping("influencer/influencer-preference")
//	public ResponseEntity<Object> getInfluencerPreference();
}