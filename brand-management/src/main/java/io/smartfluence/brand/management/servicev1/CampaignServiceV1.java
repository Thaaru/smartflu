package io.smartfluence.brand.management.servicev1;

import com.amazonaws.HttpMethod;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.constants.ProposalStatus;
import io.smartfluence.brand.management.dao.AddInfluencerDaoImpl;
import io.smartfluence.brand.management.entity.AddInfToCampResponse;
import io.smartfluence.brand.management.entity.CampaignInfluencerKey;
import io.smartfluence.brand.management.entity.OfferTypeResponse;
import io.smartfluence.brand.management.entity.RemoveInfluencerReq;
import io.smartfluence.brand.management.entityv1.CampaignInfluencerOfferV1Entity;
import io.smartfluence.brand.management.entityv1.CampaignInfluencerV1Entity;
import io.smartfluence.brand.management.entityv1.*;
import io.smartfluence.brand.management.mailbean.MailService;
import io.smartfluence.brand.management.repository.BrandCampaignDetailsV1Repo;
import io.smartfluence.brand.management.repository.CampaignInfOfferV1Repo;
import io.smartfluence.brand.management.repository.CampaignInfluencerRepo;
import io.smartfluence.brand.management.repositoryv1.BrandCampaignDetailsRepositoryV1;
import io.smartfluence.brand.management.service.DashboardService;
import io.smartfluence.brand.management.service.ReportsService;
import io.smartfluence.cassandra.entities.CampaignExportToCsvPath;
import io.smartfluence.cassandra.entities.PostAnalyticsTriggers;
import io.smartfluence.cassandra.entities.brand.BrandBidDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.repository.CampaignExportToCsvPathRepo;
import io.smartfluence.cassandra.repository.PostAnalyticsTriggersRepo;
import io.smartfluence.cassandra.repository.brand.BrandBidDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignDetailsRepo;
import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.brand.campaignv1.*;
import io.smartfluence.modal.brand.dashboard.BrandCampaignInfluencerList;
import io.smartfluence.modal.brand.dashboard.CampaignPostLinkModal;
import io.smartfluence.modal.brand.dashboard.InfluencerSnapshot;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.brand.viewinfluencer.BrandCampaigns;
import io.smartfluence.modal.utill.ResponseObj;
import io.smartfluence.modal.validation.brand.dashboard.SavePostLink;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.SocialIdMasterRepo;
import io.smartfluence.mysql.repository.UserAccountsRepo;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.*;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static io.smartfluence.brand.management.constants.CONSTANTFIELDS.*;
import static io.smartfluence.brand.management.constants.CONSTANTFIELDS.NOTACCEPTED;

@Log4j2
@Service
public class CampaignServiceV1 {

    // private static final String[] HEADERS = {"Influencer", "Followers",
    // "Engagement", "Audience Credibility",
    // "Estimated Pricing", "Bid Number", "Bid Status", "Post Type", "Post
    // duration", "Payment Type",
    // "Bid Amount / Affiliate Fee", "Submitted On", "Posted On", "Post Link",
    // "Email", "Platform"};

    private static final String[] HEADERS = {"Influencer", "Campaign", "Followers", "Engagement",
            "Audience Credibility", "Full Name",
            "Estimated Pricing", "Email", "Platform"};

    @Autowired
    CampaignEntityServiceV1 campaignEntityServiceV1;
    @Autowired
    MasterServiceV1 masterServiceV1;
    @Autowired
    Validator validator;
    @Autowired
    CampaignInfluencerRepo campaignInfluencerRepo;
    @Autowired
    CampaignInfOfferV1Repo campaignInfOfferV1Repo;
    @Autowired
    UserAccountsRepo userAccountsRepo;
    @Autowired
    MailService mailService;

    final static Logger LOG = Logger.getLogger(CampaignServiceV1.class.getName());

    @Autowired
    AddInfluencerDaoImpl addInfluencerDaoImpl;

    @Autowired
    private SocialIdMasterRepo socialIdMasterRepo;

    @Autowired
    private PostAnalyticsTriggersRepo postAnalyticsTriggersRepo;

    @Autowired
    private BrandCampaignDetailsRepo brandCampaignDetailsRepo;

    @Autowired
    private ReportsService reportsService;

    @Autowired
    private BrandBidDetailsRepo bidDetailsRepo;

    @Autowired
    private CampaignExportToCsvPathRepo campaignExportToCsvPathRepo;

    @Autowired
    private BrandCampaignDetailsRepositoryV1 brandCampaignDetailsRepositoryV1;

    @Autowired
    private BrandCampaignDetailsV1Repo brandCampaignDetailsV1Repo;

    @Autowired
    private StorageService storageService;

    @Autowired
    DashboardService dashboardService;

    @Autowired
    ProcedureClass procedureClass;

    @Transactional
    public ResponseEntity<CampaignEntityV1> saveCampaign(CampaignEntityV1 campaignEntityV1, MultipartFile file)
            throws IOException {
        LOG.log(Level.INFO, "save campaign***********");
        LOG.log(Level.INFO, "Inside the save campaign route the campaign entity=" + campaignEntityV1.toString()
                + "f or given brandId:-" + ResourceServer.getUserId().toString() + " time:-" + LocalDateTime.now());

        if (Objects.isNull(campaignEntityV1.getStatusType())) {
            LOG.log(Level.SEVERE, "Inside the campaign launch status type is null for given brandId:-" + ResourceServer.getUserId().toString()
                    + " time:-" + LocalDateTime.now());
            return getErrorResp("something wrong try again later");
        } else if (campaignEntityV1.getStatusType().equals(StatusType.LAUNCH)) {
            Set<ConstraintViolation<CampaignEntityV1>> violations = validator.validate(campaignEntityV1);
            if (!violations.isEmpty()) {
                LOG.log(Level.INFO, "Inside the campaign launch field validation error message:-" + violations.iterator().next().getMessage()
                        + "for given brandId:-"
                        + ResourceServer.getUserId().toString()
                        + "  time:-" + LocalDateTime.now());
                return getErrorResp(violations.iterator().next().getMessage());
            } else {
                List<PlatformMasterV1> platformMasterList = masterServiceV1.getPlatformMaster();
                List<PlatformPostTypeMasterV1> platformPostMasterList = masterServiceV1.getPlatformPostMaster();
                for (CampaignProductsEntityV1 campaignProducts :
                        campaignEntityV1.getProducts()) {
                    Set<ConstraintViolation<CampaignProductsEntityV1>> productViolation = validator.validate
                            (campaignProducts);
                    if (!productViolation.isEmpty()) {
                        LOG.log(Level.INFO, "Inside the campaign launch field validation error message:-" + productViolation.iterator().next().getMessage()
                                + "for given brandId:-"
                                + ResourceServer.getUserId().toString()
                                + "  time:-" + LocalDateTime.now());
                        return getErrorResp(productViolation.iterator().next().getMessage());
                    }
                    if (campaignEntityV1.getMaxProductCount() > campaignEntityV1.getProducts().size()) {
                        LOG.log(Level.INFO, "Inside the campaign launch field validation error message:-" +
                                "product count is greater than products"
                                + "for given brandId:-"
                                + ResourceServer.getUserId().toString()
                                + "  time:-" + LocalDateTime.now());
                        return getErrorResp("Max products count choose is greater than products");
                    }
                }
                for (CampaignTasksEntityV1 campaignTasks :
                        campaignEntityV1.getTasks()) {
                    Set<ConstraintViolation<CampaignTasksEntityV1>> taskViolation = validator.validate(campaignTasks);
                    if (!taskViolation.isEmpty())
                        return getErrorResp(taskViolation.iterator().next().getMessage());
                    if (Objects.isNull(campaignTasks.getPlatformId())) {
                        LOG.log(Level.INFO, "Inside the campaign launch field validation error message:-platformId is  null"
                                + "for given brandId:-"
                                + ResourceServer.getUserId().toString()
                                + "  time:-" + LocalDateTime.now());
                        return getErrorResp("Choose platform from the dropdown");
                    }

                    if (Objects.isNull(campaignTasks.getPostTypeId())) {
                        LOG.log(Level.INFO, "Inside the campaign launch field validation error message:-postTypeId is  null"
                                + "for given brandId:-"
                                + ResourceServer.getUserId().toString()
                                + "  time:-" + LocalDateTime.now());
                        return getErrorResp("Choose post type from the dropdown");
                    }

                    boolean platformIdValidation = platformMasterList.stream()
                            .anyMatch(platformMaster -> Objects.equals(platformMaster.getPlatformId(), campaignTasks.getPlatformId()));
                    boolean postIdValidation = platformPostMasterList.stream().anyMatch(platformPostTypeMaster -> Objects.equals(
                            platformPostTypeMaster.getPostTypeId(), campaignTasks.getPostTypeId()));
                    if (!platformIdValidation) {
                        LOG.log(Level.INFO, "Inside the campaign launch field validation error message:-Selected platformId doesn't match"
                                + "for given brandId:-"
                                + ResourceServer.getUserId().toString()
                                + "  time:-" + LocalDateTime.now());
                        return getErrorResp("invalid platform");
                    }
                    if (!postIdValidation) {
                        LOG.log(Level.INFO, "Inside the campaign launch field validation error message:-Selected platformId doesn't match"
                                + "for given brandId:-"
                                + ResourceServer.getUserId().toString()
                                + "  time:-" + LocalDateTime.now());
                        return getErrorResp("invalid post type");
                    }
                    boolean customNameValidation = platformPostMasterList.stream()
                            .anyMatch(platformPostType ->
                            {
                                if (Objects.equals(platformPostType.getPlatformId(), campaignTasks.getPlatformId())
                                        && Objects.equals(platformPostType.getPostTypeId(), campaignTasks.getPostTypeId())
                                        && Objects.equals(platformPostType.getPostTypeName(), "CUSTOM")) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                    if (customNameValidation) {
                        if (Objects.nonNull(campaignTasks.getPostCustomName())) {
                            if (Objects.equals(campaignTasks.getPostCustomName(), "")) {
                                return getErrorResp("task custom name is empty");
                            } else if (campaignTasks.getPostCustomName().length() < 0 ||
                                    campaignTasks.getPostCustomName().length() > 51) {
                                return getErrorResp("task custom name should be max 50 characters only");
                            }
                        }
                    }


                }
                Map<String, String> campaignOfferValidation = validateCampaignOffers(campaignEntityV1);
                if (!Objects.equals(campaignOfferValidation.get(CONDITION), ACCEPTED)) {
                    LOG.log(Level.INFO, "Inside the campaign launch field validation error message:-" + campaignOfferValidation.get(MESSAGE)
                            + "for given brandId:-"
                            + ResourceServer.getUserId().toString()
                            + "  time:-" + LocalDateTime.now());
                    return getErrorResp(campaignOfferValidation.get(MESSAGE));
                }


            }

        }


        if (campaignEntityV1.getStatusType().equals(StatusType.SAVE) ||
                campaignEntityV1.getStatusType().equals(StatusType.LAUNCH)) {
            BrandCampaignDetailsV1 brandCampaignDetailsV1 = null;
            if (Objects.nonNull(campaignEntityV1.getCampaignId())) {
                BrandCampaignDetailsV1 existCampaign = campaignEntityServiceV1
                        .getCampaignById(campaignEntityV1.getCampaignId());
                if (existCampaign.getCampaignStatus().equals(CampaignStatusEntityV1.ACTIVE)
                        || existCampaign.getCampaignStatus().equals(CampaignStatusEntityV1.INACTIVE)
                        || existCampaign.getCampaignStatus().equals(CampaignStatusEntityV1.CLOSED)) {
                    return getErrorResp("Can't edit campaign due to " + existCampaign.getCampaignStatus());
                } else {
                    campaignEntityV1 = saveCampaignEntity(campaignEntityV1, existCampaign, file);
                    if (Objects.nonNull(campaignEntityV1.getErrorObj())) {
                        return getErrorResp(campaignEntityV1.getErrorObj().get("campaignError"));
                    }
                }
            } else {
                campaignEntityV1 = saveCampaignEntity(campaignEntityV1, brandCampaignDetailsV1, file);
                if (Objects.nonNull(campaignEntityV1.getErrorObj())) {
                    return getErrorResp(campaignEntityV1.getErrorObj().get("campaignError"));
                }
            }
            if (Objects.nonNull(campaignEntityV1.getTempTermsId())) {
                campaignEntityServiceV1.deleteByTermId(campaignEntityV1.getTempTermsId());
            }
            if (Objects.isNull(campaignEntityV1.getErrorObj())) {
                campaignEntityV1.setCampaignId(campaignEntityV1.getCampaignId());
                if (Objects.nonNull(campaignEntityV1.getProducts())) {
                    campaignEntityV1 = saveCampaignProducts(campaignEntityV1);
                }
                if (Objects.nonNull(campaignEntityV1.getTasks())) {
                    campaignEntityV1 = saveCampaignTasks(campaignEntityV1);
                }
                if (Objects.nonNull(campaignEntityV1.getOffers())) {
                    campaignEntityV1 = saveCampaignOffer(campaignEntityV1);
                }

                campaignEntityV1 = getCampaignEntityByCampaignId(campaignEntityV1.getCampaignId()).getBody();
                return new ResponseEntity<>(campaignEntityV1, HttpStatus.OK);
            }
            return ResponseEntity.ok(campaignEntityV1);
        }
        return ResponseEntity.ok(campaignEntityV1);
    }

    public ResponseEntity<CampaignEntityV1> getErrorResp(String message) {
        CampaignEntityV1 campaignError = new CampaignEntityV1();
        Map errorsObj = new HashMap<String, String>();
        errorsObj.put("campaignError", message);
        campaignError.setErrorObj(errorsObj);
        return ResponseEntity.ok().body(campaignError);
    }

    public CampaignEntityV1 saveCampaignProducts(CampaignEntityV1 campaignEntityV1) {
        List<CampaignProductsV1> productList = campaignEntityServiceV1
                .getProductsByCampaignId(campaignEntityV1.getCampaignId());
        for (int i = 0; i < productList.size(); i++) {
            int count = 0;
            for (int j = 0; j < campaignEntityV1.getProducts().size(); j++) {
                if (!Objects.isNull(campaignEntityV1.getProducts().get(j).getProductId())) {
                    if (Objects.equals(productList.get(i).getProductId(),
                            campaignEntityV1.getProducts().get(j).getProductId())) {
                        count++;
                        CampaignProductsEntityV1 campaignProductsEntity = campaignEntityV1.getProducts().get(j);
                        saveProduct(campaignProductsEntity, campaignEntityV1.getCampaignId());
                    } else {
                        continue;
                    }
                }
            }
            if (count == 0) {
                campaignEntityServiceV1.deleteProductsByProductId(productList.get(i).getProductId());
            }
        }
        if (!Objects.isNull(campaignEntityV1.getProducts())) {
            campaignEntityV1.getProducts().stream()
                    .filter(campaignProductsEntityV1 -> Objects.isNull(campaignProductsEntityV1.getProductId()))
                    .forEach(campaignProductsEntityV1 -> {
                        if (Objects.isNull(campaignProductsEntityV1.getProductId())) {
                            saveProduct(campaignProductsEntityV1, campaignEntityV1.getCampaignId());
                        }
                    });
        }
        return campaignEntityV1;

    }

    public CampaignProductsV1 saveProduct(CampaignProductsEntityV1 campaignProductLatest, String campaignId) {
        CampaignProductsV1 campaignProductsV1 = new CampaignProductsV1();
        campaignProductsV1.setCampaignId(campaignId);
        if (!Objects.isNull(campaignProductLatest.getProductId())) {
            campaignProductsV1.setProductId(campaignProductLatest.getProductId());
        }
        campaignProductsV1.setProductName(campaignProductLatest.getProductName());
        campaignProductsV1.setProductLink(campaignProductLatest.getProductLink());
        return campaignEntityServiceV1.saveCampaignProducts(campaignProductsV1);
    }

    public CampaignOffersV1 saveOffers(CampaignOffersEntityV1 campaignOffersEntityV1, String campaignId) {
        CampaignOffersV1 campaignOffersV1 = new CampaignOffersV1();
        CampaignOfferId campaignOfferId = new CampaignOfferId();
        campaignOfferId.setCampaignId(campaignId);
        campaignOfferId.setOfferType(campaignOffersEntityV1.getOfferType());
        if (!Objects.isNull(campaignOffersEntityV1.getPaymentOffer())) {
            campaignOffersV1.setPaymentOffer(campaignOffersEntityV1.getPaymentOffer());
        } else {
            campaignOffersV1.setPaymentOffer(0);
        }
        if (!Objects.isNull(campaignOffersEntityV1.getCommissionValue())) {
            campaignOffersV1.setCommissionValue(campaignOffersEntityV1.getCommissionValue());
        } else {
            campaignOffersV1.setCommissionValue(0);
        }
        if (Objects.nonNull(campaignOffersEntityV1.getCommissionAffiliateTypeId())) {
            campaignOffersV1.setCommissionAffiliateTypeId(campaignOffersEntityV1.getCommissionAffiliateTypeId());
        }
        if (Objects.nonNull(campaignOffersEntityV1.getCommissionValueTypeId())) {
            campaignOffersV1.setCommissionValueTypeId(campaignOffersEntityV1.getCommissionValueTypeId());
        }
        campaignOffersV1.setCampaignOfferId(campaignOfferId);
        campaignOffersV1.setCommissionValueType(campaignOffersEntityV1.getCommissionValueType());
        campaignOffersV1.setCommissionAffiliateType(campaignOffersEntityV1.getCommissionAffiliateType());
        campaignOffersV1.setCommissionAffiliateCustomName(campaignOffersEntityV1.getCommissionAffiliateCustomName());
        campaignOffersV1.setCommissionAffiliateDetails(campaignOffersEntityV1.getCommissionAffiliateDetails());
        return campaignEntityServiceV1.saveCampaignOffers(campaignOffersV1);
    }

    public CampaignEntityV1 saveCampaignTasks(CampaignEntityV1 campaignEntityV1) {
        List<CampaignTasksV1> taskList = campaignEntityServiceV1.getTasksByCampaignId(campaignEntityV1.getCampaignId());
        for (int i = 0; i < taskList.size(); i++) {
            int count = 0;
            for (int j = 0; j < campaignEntityV1.getTasks().size(); j++) {
                if (!Objects.isNull(campaignEntityV1.getTasks().get(j).getTaskId())) {
                    if (Objects.equals(taskList.get(i).getTaskId(), campaignEntityV1.getTasks().get(j).getTaskId())) {
                        count++;
                        CampaignTasksEntityV1 campaignTasksEntity = campaignEntityV1.getTasks().get(j);
                        saveTasks(campaignTasksEntity, campaignEntityV1.getCampaignId());
                    } else {
                        continue;
                    }
                }
            }
            if (count == 0) {
                campaignEntityServiceV1.deleteCampaignTaskByTaskId(taskList.get(i).getTaskId());
            }
        }
        if (!Objects.isNull(campaignEntityV1.getTasks())) {
            campaignEntityV1.getTasks().stream()
                    .filter(campaignTasksEntityV1 -> Objects.isNull(campaignTasksEntityV1.getTaskId()))
                    .forEach(campaignTasksEntityV1 -> {
                        if (Objects.isNull(campaignTasksEntityV1.getTaskId())) {
                            saveTasks(campaignTasksEntityV1, campaignEntityV1.getCampaignId());
                        }
                    });
        }
        return campaignEntityV1;

    }

    public CampaignTasksV1 saveTasks(CampaignTasksEntityV1 campaignTasksEntityV1, String campaignId) {
        CampaignTasksV1 campaignTasksV1 = new CampaignTasksV1();
        campaignTasksV1.setCampaignId(campaignId);
        if (!Objects.isNull(campaignTasksEntityV1.getTaskId())) {
            campaignTasksV1.setTaskId(campaignTasksEntityV1.getTaskId());
        }
        campaignTasksV1.setPlatformId(campaignTasksEntityV1.getPlatformId());
        campaignTasksV1.setGuidelines(campaignTasksEntityV1.getGuidelines());
        campaignTasksV1.setPostTypeId(campaignTasksEntityV1.getPostTypeId());
        campaignTasksV1.setPostCustomName(campaignTasksEntityV1.getPostCustomName());
        return campaignEntityServiceV1.saveCampaignTasks(campaignTasksV1);
    }

    public CampaignEntityV1 saveCampaignEntity(CampaignEntityV1 campaignEntityV1, BrandCampaignDetailsV1 existCampaign,
                                               MultipartFile file) throws IOException {

        BrandCampaignDetailsV1 brandCampaignDetailsV1 = new BrandCampaignDetailsV1();
        if (campaignEntityV1.getStatusType() != null) {
            if (campaignEntityV1.getStatusType().equals(StatusType.SAVE)) {
                brandCampaignDetailsV1.setCampaignStatus(CampaignStatusEntityV1.INPROGRESS);
            } else if (campaignEntityV1.getStatusType().equals(StatusType.LAUNCH)) {
                brandCampaignDetailsV1.setCampaignStatus(CampaignStatusEntityV1.ACTIVE);
            } else {
                brandCampaignDetailsV1.setCampaignStatus(CampaignStatusEntityV1.INPROGRESS);
            }
        } else {
            brandCampaignDetailsV1.setCampaignStatus(CampaignStatusEntityV1.INPROGRESS);
        }

        brandCampaignDetailsV1.setBrandId(ResourceServer.getUserId().toString());
        brandCampaignDetailsV1.setCampaignName(campaignEntityV1.getCampaignName());
        brandCampaignDetailsV1.setCampaignDescription(campaignEntityV1.getCampaignDescription());
        brandCampaignDetailsV1.setBrandDescription(campaignEntityV1.getBrandDescription());
        brandCampaignDetailsV1.setMaxProductCount(campaignEntityV1.getMaxProductCount());
        brandCampaignDetailsV1.setHashTags(campaignEntityV1.getHashTags());
        brandCampaignDetailsV1.setRestrictions(campaignEntityV1.getRestrictions());
        if (Objects.isNull(campaignEntityV1.getMaxProductCount())) {
            brandCampaignDetailsV1.setMaxProductCount(0);
        } else {
            brandCampaignDetailsV1.setMaxProductCount(campaignEntityV1.getMaxProductCount());
        }
        campaignEntityV1 = setCampaignTermsAndCondition(campaignEntityV1, existCampaign, file);
        if (Objects.isNull(campaignEntityV1.getErrorObj())) {
            brandCampaignDetailsV1.setTermsId(campaignEntityV1.getTermId());
            if (Objects.isNull(campaignEntityV1.getCampaignId())) {
                if (Objects.nonNull(campaignEntityV1.getTempCampaignId())) {
                    brandCampaignDetailsV1.setCampaignId(campaignEntityV1.getTempCampaignId());
                } else {
                    brandCampaignDetailsV1.setCampaignId(UUID.randomUUID().toString());
                }
                campaignEntityV1.setCampaignId(brandCampaignDetailsV1.getCampaignId());
                brandCampaignDetailsV1.setCreatedAt(LocalDateTime.now());
            } else {
                brandCampaignDetailsV1.setCampaignId(campaignEntityV1.getCampaignId());
                brandCampaignDetailsV1.setModifiedAt(LocalDateTime.now());
                brandCampaignDetailsV1.setCreatedAt(existCampaign.getCreatedAt());
            }
            BrandCampaignDetailsV1 brandCampaignRel = campaignEntityServiceV1
                    .saveBrandCampaignDetails(brandCampaignDetailsV1);
            if (Objects.nonNull(brandCampaignRel)) {
                return campaignEntityV1;
            } else {
                LOG.log(Level.SEVERE, "Error while saving brand campaign details");
                campaignEntityV1.setErrorObj(
                        Collections.singletonMap("campaignError", "Something wrong please try again later"));
                return campaignEntityV1;
            }
        } else {
            return campaignEntityV1;
        }

        // if (Objects.equals(campaignEntityV1.getTermType(), "custom")) {
        // if (!validateTermsAndCondition(campaignEntityV1, file)) {
        // String fileLocation = storageService.uploadFile(file,
        // campaignEntityV1.getCampaignId());
        // if (campaignEntityV1.getTermId() != null) {
        // TermsConditionsV1 termsConditionsV1 =
        // campaignEntityServiceV1.getTermsConditions(campaignEntityV1.getTermId(),
        // TermsTypeEntity.CUSTOM);
        // if (termsConditionsV1 != null) {
        // termsConditionsV1.setTermsConditionFilePath(fileLocation);
        // termsConditionsV1.setFileName(file.getOriginalFilename());
        // TermsConditionsV1 termsCondRel =
        // campaignEntityServiceV1.saveTermsCondition(termsConditionsV1);
        // brandCampaignDetailsV1.setTermsId(termsCondRel.getTermId());
        // } else {
        // termsConditionsV1 = campaignEntityServiceV1.saveTermsCondition(new
        // TermsConditionsV1(
        // fileLocation, TermsTypeEntity.CUSTOM, IsActive.YES,
        // file.getOriginalFilename()));
        // brandCampaignDetailsV1.setTermsId(termsConditionsV1.getTermId());
        // }
        // } else {
        // TermsConditionsV1 termsConditionsV1 =
        // campaignEntityServiceV1.saveTermsCondition(new TermsConditionsV1(
        // fileLocation, TermsTypeEntity.CUSTOM, IsActive.YES,
        // file.getOriginalFilename()));
        // brandCampaignDetailsV1.setTermsId(termsConditionsV1.getTermId());
        // }
        // } else {
        // if (campaignEntityV1.getTermsCondition() != null) {
        // if (campaignEntityV1.getTermsCondition().getTermId() != null) {
        // brandCampaignDetailsV1.setTermsId(campaignEntityV1.getTermsCondition().getTermId());
        // } else {
        // brandCampaignDetailsV1 =
        // setStandardTermsAndCondition(brandCampaignDetailsV1);
        // }
        // } else {
        // brandCampaignDetailsV1 =
        // setStandardTermsAndCondition(brandCampaignDetailsV1);
        // }
        // }
        // } else if (Objects.equals(campaignEntityV1.getTermType(), "standard")) {
        // brandCampaignDetailsV1 =
        // setStandardTermsAndCondition(brandCampaignDetailsV1);
        //// List<TermsConditionsV1> termsConditionsByTypeAndIsActive =
        // campaignEntityServiceV1.getTermsConditionsByTypeAndIsActive(TermsTypeEntity.STANDARD,
        // IsActive.YES);
        //// List<TermsConditionsV1> standardTerm =
        // termsConditionsByTypeAndIsActive.stream().filter(termsConditionsV1 -> {
        //// return termsConditionsV1.getType().equals(TermsTypeEntity.STANDARD) &&
        // termsConditionsV1.getIsActive().equals(IsActive.YES);
        //// }).collect(Collectors.toList());
        //// if (!standardTerm.isEmpty()) {
        //// brandCampaignDetailsV1.setTermsId(standardTerm.get(0).getTermId());
        //// }
        // } else {
        // if (campaignEntityV1.getTermId() != null) {
        // brandCampaignDetailsV1.setTermsId(campaignEntityV1.getTermId());
        // } else {
        // brandCampaignDetailsV1 =
        // setStandardTermsAndCondition(brandCampaignDetailsV1);
        // }
        // }

    }

    public CampaignEntityV1 setCampaignTermsAndCondition(CampaignEntityV1 campaignEntityV1,
                                                         BrandCampaignDetailsV1 existCampaign,
                                                         MultipartFile file) {
        if (campaignEntityV1.getCampaignId() != null) {
            if (Objects.equals(campaignEntityV1.getTermType(), "standard")) {
                TermsConditionsV1 termsAndConditionByTermId = null;
                if (Objects.nonNull(existCampaign.getTermsId())) {
                    termsAndConditionByTermId = campaignEntityServiceV1
                            .getTermsAndConditionByTermId(existCampaign.getTermsId());
                }
                if (termsAndConditionByTermId != null) {
                    if (Objects.equals(termsAndConditionByTermId.getType().toString(), "CUSTOM")) {
                        campaignEntityV1.setTempTermsId(termsAndConditionByTermId.getTermId());
                        // campaignEntityServiceV1.deleteByTermId(termsAndConditionByTermId.getTermId());
                        return setCampaignTerms(campaignEntityV1);
                    } else {
                        return setCampaignTerms(campaignEntityV1);
                    }
                } else {
                    return setCampaignTerms(campaignEntityV1);
                }
            } else if (Objects.equals(campaignEntityV1.getTermType(),
                    "custom")) {
                TermsConditionsV1 termsAndConditionByTermId = null;
                if (Objects.nonNull(existCampaign.getTermsId())) {
                    termsAndConditionByTermId = campaignEntityServiceV1.getTermsConditions(existCampaign.getTermsId(),
                            TermsTypeEntity.CUSTOM);
                }
                if (termsAndConditionByTermId != null) {
                    if (Objects.equals(termsAndConditionByTermId.getType().toString(), "CUSTOM")
                            && Objects.isNull(file)
                            && Objects.isNull(termsAndConditionByTermId.getTermsConditionFilePath()) &&
                            Objects.equals(campaignEntityV1.getStatusType().toString(), "LAUNCH")) {
                        LOG.log(Level.SEVERE, "File shouldn't be empty");
                        campaignEntityV1
                                .setErrorObj(Collections.singletonMap("campaignError", "file shouldn't be empty"));
                        campaignEntityV1.setTermId(termsAndConditionByTermId.getTermId());
                        return campaignEntityV1;
                    } else if (Objects.equals(termsAndConditionByTermId.getType().toString(), "CUSTOM")
                            && Objects.isNull(file)
                            && Objects.isNull(termsAndConditionByTermId.getTermsConditionFilePath())
                            && (!Objects.equals(campaignEntityV1.getStatusType().toString(), "LAUNCH"))) {
                        campaignEntityV1.setTermId(termsAndConditionByTermId.getTermId());
                        return campaignEntityV1;
                    } else if (Objects.equals(termsAndConditionByTermId.getType().toString(), "CUSTOM")
                            && Objects.isNull(file)
                            && Objects.nonNull(termsAndConditionByTermId.getTermsConditionFilePath())) {
                        campaignEntityV1.setTermId(termsAndConditionByTermId.getTermId());
                        return campaignEntityV1;
                    } else if (Objects.equals(termsAndConditionByTermId.getType().toString(), "CUSTOM")
                            && Objects.nonNull(file)) {
                        String fileLocation = null;
                        if (Objects.isNull(campaignEntityV1.getCampaignId())) {
                            campaignEntityV1.setTempCampaignId(UUID.randomUUID().toString());
                            fileLocation = storageService.uploadFile(file, campaignEntityV1.getTempCampaignId());
                        } else {
                            fileLocation = storageService.uploadFile(file, campaignEntityV1.getCampaignId());
                        }

                        if (Objects.nonNull(fileLocation)) {
                            TermsConditionsV1 updatedTermsCond = updateFileAndNameInTermsAndCondition(
                                    termsAndConditionByTermId, file, fileLocation);
                            if (updatedTermsCond != null) {
                                campaignEntityV1.setTermId(updatedTermsCond.getTermId());
                                return campaignEntityV1;
                            } else {
                                LOG.log(Level.SEVERE, "Something wrong while updating custom terms and condition");
                                campaignEntityV1.setErrorObj(Collections.singletonMap("campaignError",
                                        "Something wrong please try again later"));
                                return campaignEntityV1;
                            }
                        } else {
                            LOG.log(Level.SEVERE, "Something wrong while uploading file in s3");
                            campaignEntityV1.setErrorObj(Collections.singletonMap("campaignError",
                                    "Something wrong please try again later"));
                            return campaignEntityV1;
                        }
                    }
                    // else {
                    // LOG.log(Level.SEVERE, "Something wrong");
                    // campaignEntityV1.setErrorObj(Collections.
                    // singletonMap("campaignError", "Something wrong please try again later"));
                    // return campaignEntityV1;
                    // }
                } else {
                    if ((!Objects.equals(campaignEntityV1.getStatusType().toString(), "LAUNCH"))
                            && (Objects.isNull(file))) {
                        return setCampaignTerms(campaignEntityV1);
                    } else if (Objects.equals(campaignEntityV1.getStatusType().toString(), "LAUNCH")
                            && Objects.isNull(file)) {

                        LOG.log(Level.SEVERE, "File shouldn't be empty");
                        campaignEntityV1
                                .setErrorObj(Collections.singletonMap("campaignError", "file shouldn't be empty"));
                        return campaignEntityV1;
                    } else if (Objects.nonNull(file)) {
                        return saveCustomFile(campaignEntityV1, file);
                    } else {
                        LOG.log(Level.SEVERE, "Something wrong");
                        campaignEntityV1.setErrorObj(
                                Collections.singletonMap("campaignError", "Something wrong please try again later"));
                        return campaignEntityV1;
                    }
                }
            }
        } else {
            if ((Objects.equals(campaignEntityV1.getTermType(), "standard") ||
                    Objects.equals(campaignEntityV1.getTermType(), "custom")) && Objects.isNull(file) &&
                    (!Objects.equals(campaignEntityV1.getStatusType().toString(), "LAUNCH"))) {
                Long standardTermId = getStandardTermId();
                campaignEntityV1.setTermId(standardTermId);
            } else if (Objects.equals(campaignEntityV1.getTermType(), "custom") && Objects.nonNull(file)) {
                return saveCustomFile(campaignEntityV1, file);
            } else {
                LOG.log(Level.SEVERE, "File shouldn't be empty");
                campaignEntityV1.setErrorObj(Collections.singletonMap("campaignError", "file shouldn't be empty"));
                return campaignEntityV1;
            }
        }

        return campaignEntityV1;
    }

    public CampaignEntityV1 saveCustomFile(CampaignEntityV1 campaignEntityV1, MultipartFile file) {
        String fileLocation = storageService.uploadFile(file, campaignEntityV1.getCampaignId());
        if (Objects.nonNull(fileLocation)) {
            TermsConditionsV1 termsConditionsV1 = new TermsConditionsV1();
            termsConditionsV1.setTermsConditionFilePath(fileLocation);
            termsConditionsV1.setType(TermsTypeEntity.CUSTOM);
            termsConditionsV1.setIsActive(IsActive.YES);
            termsConditionsV1.setFileName(file.getOriginalFilename());
            TermsConditionsV1 termsRel = campaignEntityServiceV1.saveTermsCondition(termsConditionsV1);
            if (Objects.nonNull(termsRel)) {
                campaignEntityV1.setTermId(termsRel.getTermId());
                return campaignEntityV1;
            } else {
                LOG.log(Level.SEVERE, "Something wrong while adding custom terms and condition");
                campaignEntityV1.setErrorObj(
                        Collections.singletonMap("campaignError", "Something wrong please try again later"));
                return campaignEntityV1;
            }
        } else {
            LOG.log(Level.SEVERE, "Something wrong while uploading file in s3");
            campaignEntityV1
                    .setErrorObj(Collections.singletonMap("campaignError", "Something wrong please try again later"));
            return campaignEntityV1;
        }
    }

    public CampaignEntityV1 setCampaignTerms(CampaignEntityV1 campaignEntityV1) {
        Long standardTermId = getStandardTermId();
        if (Objects.nonNull(standardTermId)) {
            campaignEntityV1.setTermId(standardTermId);
            return campaignEntityV1;
        } else {
            LOG.log(Level.SEVERE, "no standard terms and condition");
            campaignEntityV1.setErrorObj(Collections.singletonMap("campaignError", "Something wrong try again later"));
            return campaignEntityV1;
        }
    }

    public TermsConditionsV1 updateFileAndNameInTermsAndCondition(TermsConditionsV1 termsConditionsV1,
                                                                  MultipartFile file, String fileLocation) {
        termsConditionsV1.setTermsConditionFilePath(fileLocation);
        termsConditionsV1.setFileName(file.getOriginalFilename());
        termsConditionsV1.setType(TermsTypeEntity.CUSTOM);
        termsConditionsV1.setIsActive(IsActive.YES);
        return campaignEntityServiceV1.saveTermsCondition(termsConditionsV1);
    }

    public boolean validateTermsAndCondition(CampaignEntityV1 campaignEntityV1) {
        if (campaignEntityV1.getTermsCondition() == null) {
            return false;
        } else {
            if (campaignEntityV1.getTermsCondition().getTermId() == null) {
                return false;
            } else {
                return true;
            }
        }
    }

    public boolean validateTermsAndCondition(CampaignEntityV1 campaignEntityV1, MultipartFile file) {
        if (file == null) {
            if (campaignEntityV1.getTermsCondition() == null) {
                return false;
            } else {
                if (campaignEntityV1.getTermsCondition().getTermId() == null) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            if (file.isEmpty()) {
                return true;
            } else {
                return false;
            }
        }

    }

    public boolean validateFile(CampaignEntityV1 campaignEntityV1, MultipartFile file, String type) {
        if (Objects.equals(type, "custom")) {
            if (file == null) {
                return validateTermsAndCondition(campaignEntityV1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public BrandCampaignDetailsV1 setStandardTermsAndCondition(BrandCampaignDetailsV1 brandCampaignDetailsV1) {
        Long standardTermId = getStandardTermId();
        if (Objects.nonNull(standardTermId)) {
            brandCampaignDetailsV1.setTermsId(standardTermId);
        }
        return brandCampaignDetailsV1;
    }

    public Long getStandardTermId() {
        List<TermsConditionsV1> termsConditionsByTypeAndIsActive = campaignEntityServiceV1
                .getTermsConditionsByTypeAndIsActive(TermsTypeEntity.STANDARD, IsActive.YES);
        List<TermsConditionsV1> standardTerm = termsConditionsByTypeAndIsActive.stream().filter(termsConditionsV1 -> {
            return termsConditionsV1.getType().equals(TermsTypeEntity.STANDARD)
                    && termsConditionsV1.getIsActive().equals(IsActive.YES);
        }).collect(Collectors.toList());
        if (!standardTerm.isEmpty()) {
            return standardTerm.get(0).getTermId();
        } else {
            return null;
        }
    }

    public BrandCampaignDetailsV1 setTermsConditionId(CampaignEntityV1 campaignEntityV1,
                                                      BrandCampaignDetailsV1 brandCampaignDetailsV1) {
        if (Objects.isNull(campaignEntityV1.getTermsCondition())) {
            List<TermsConditionsV1> termsConditionsList = campaignEntityServiceV1
                    .getTermsConditionsByTypeAndIsActive(TermsTypeEntity.STANDARD, IsActive.YES);
            List<TermsConditionsV1> standardTerm = termsConditionsList.stream().filter(termsConditionsV1 -> {
                return termsConditionsV1.getType().equals(TermsTypeEntity.STANDARD)
                        && termsConditionsV1.getIsActive().equals(IsActive.YES);
            }).collect(Collectors.toList());
            if (!standardTerm.isEmpty()) {
                brandCampaignDetailsV1.setTermsId(standardTerm.get(0).getTermId());
            }
            return brandCampaignDetailsV1;
        } else {
            TermsConditionsV1 termsConditionsV1 = new TermsConditionsV1();
            termsConditionsV1.setTermsCondition(campaignEntityV1.getTermsCondition().getTermsCondition());
            termsConditionsV1.setIsActive(IsActive.YES);
            termsConditionsV1.setType(TermsTypeEntity.CUSTOM);
            TermsConditionsV1 termsConditionSaveObj = campaignEntityServiceV1.saveTermsCondition(termsConditionsV1);
            campaignEntityV1.setTermId(termsConditionSaveObj.getTermId());
            TermsConditionsEntityV1 termsConditionsEntityV1 = new TermsConditionsEntityV1();
            termsConditionsEntityV1.setTermId(termsConditionSaveObj.getTermId());
            termsConditionsEntityV1.setTermsCondition(termsConditionSaveObj.getTermsCondition());
            termsConditionsEntityV1.setTermsConditionFilePath(termsConditionSaveObj.getTermsConditionFilePath());
            termsConditionsEntityV1.setType(termsConditionSaveObj.getType());
            termsConditionsEntityV1.setIsActive(termsConditionSaveObj.getIsActive());
            campaignEntityV1.setTermsCondition(termsConditionsEntityV1);
            return brandCampaignDetailsV1;
        }
    }

    public ResponseEntity<CampaignEntityV1> getCampaignEntityByCampaignId(String campaignId) {

        BrandCampaignDetailsV1 brandCampaign = campaignEntityServiceV1.getCampaignById(campaignId);
        CampaignEntityV1 campaignEntityV1 = new CampaignEntityV1();
        if (brandCampaign != null) {

            campaignEntityV1 = setCampaignEntity(campaignEntityV1, brandCampaign);
            List<CampaignProductsV1> productList = campaignEntityServiceV1.getProductsByCampaignId(campaignId);
            campaignEntityV1 = setCampaignProducts(campaignEntityV1, productList);
            List<CampaignTasksV1> taskList = campaignEntityServiceV1.getTasksByCampaignId(campaignId);
            campaignEntityV1 = setCampaignTasks(campaignEntityV1, taskList);
            List<PlatformMasterV1> platformMasterList = masterServiceV1.getPlatformMaster();
            campaignEntityV1 = setPlatformMaster(campaignEntityV1, platformMasterList);
            List<PlatformPostTypeMasterV1> platformPostMasterList = masterServiceV1.getPlatformPostMaster();
            campaignEntityV1 = setPlatformPostMaster(campaignEntityV1, platformPostMasterList);
            List<CurrencyMasterV1> currencyMasterList = masterServiceV1.getCurrencyMaster();
            campaignEntityV1 = setCurrencyMaster(campaignEntityV1, currencyMasterList);

            List<CommissionAffiliateTypeMasterV1> commissionAffiliateTypeList = masterServiceV1
                    .getCommissionAffiliateTypeMaster();
            campaignEntityV1 = setCommissionAffilateTypeMaster(campaignEntityV1, commissionAffiliateTypeList);

            List<CampaignOffersV1> campaignOffersV1s = campaignEntityServiceV1
                    .getCampaignOffersByCampaignId(campaignEntityV1.getCampaignId());
            campaignEntityV1 = setCampaignOffers(campaignEntityV1, campaignOffersV1s);

            if (!Objects.isNull(campaignEntityV1.getTermId())) {
                TermsConditionsV1 termsAndConditionByTermId = campaignEntityServiceV1
                        .getTermsAndConditionByTermId(campaignEntityV1.getTermId());
                if (Objects.equals(termsAndConditionByTermId.getType().toString(), TermsTypeEntity.CUSTOM.toString())) {
                    String termsConditionFilePath = termsAndConditionByTermId.getTermsConditionFilePath();
                    String url = null;
                    if (Objects.nonNull(termsConditionFilePath)) {
                        url = storageService.generatePresignedUrl(termsAndConditionByTermId.getTermsConditionFilePath(),
                                HttpMethod.GET);
                    }
                    campaignEntityV1.setTermsCondition(new TermsConditionsEntityV1(
                            termsAndConditionByTermId.getTermId(), null, url, termsAndConditionByTermId.getType(),
                            termsAndConditionByTermId.getIsActive(), termsAndConditionByTermId.getFileName()));
                    campaignEntityV1.setTermType("custom");
                } else {
                    campaignEntityV1.setTermType("standard");
                }
            } else {
                List<TermsConditionsV1> termsConditionsList = campaignEntityServiceV1
                        .getTermsConditionsByTypeAndIsActive(TermsTypeEntity.STANDARD, IsActive.YES);
                if (!termsConditionsList.isEmpty()) {
                    campaignEntityV1.setTermsCondition(new TermsConditionsEntityV1());
                    campaignEntityV1.setTermType("standard");
                }
            }
            List<TermsConditionsV1> termsConditionsList = campaignEntityServiceV1
                    .getTermsConditionsByTypeAndIsActive(TermsTypeEntity.STANDARD, IsActive.YES);
            campaignEntityV1 = setTermsCondition(campaignEntityV1, termsConditionsList);
            return ResponseEntity.ok(campaignEntityV1);

        } else {
            return ResponseEntity.notFound().build();
        }
    }

    public CampaignEntityV1 setCampaignEntity(CampaignEntityV1 campaignEntity,
                                              BrandCampaignDetailsV1 brandEntity) {
        campaignEntity.setCampaignId(brandEntity.getCampaignId());
        campaignEntity.setCampaignName(brandEntity.getCampaignName());
        campaignEntity.setCampaignStatus(brandEntity.getCampaignStatus());
        campaignEntity.setBrandDescription(brandEntity.getBrandDescription());
        campaignEntity.setCampaignDescription(brandEntity.getCampaignDescription());
        campaignEntity.setHashTags(brandEntity.getHashTags());
        campaignEntity.setRestrictions(brandEntity.getRestrictions());
        campaignEntity.setCampaignStatus(campaignEntity.getCampaignStatus());
        if (Objects.isNull(brandEntity.getMaxProductCount())) {
            campaignEntity.setMaxProductCount(0);
        } else {
            campaignEntity.setMaxProductCount(brandEntity.getMaxProductCount());
        }
        campaignEntity.setTermId(brandEntity.getTermsId());
        // campaignEntityServiceV1.getTermsAndConditionByTermId(brandEntity.getTermsId());
        return campaignEntity;

    }

    public CampaignEntityV1 setCampaignProducts(CampaignEntityV1 campaignEntityV1,
                                                List<CampaignProductsV1> campaignProductsV1) {

        List<CampaignProductsEntityV1> campaignProductsEntityList = new LinkedList<>();
        campaignProductsV1.stream().forEach(products -> {
            CampaignProductsEntityV1 campaignProductsEntityV1 = new CampaignProductsEntityV1();
            campaignProductsEntityV1.setProductId(products.getProductId());
            campaignProductsEntityV1.setProductName(products.getProductName());
            campaignProductsEntityV1.setProductLink(products.getProductLink());
            campaignProductsEntityList.add(campaignProductsEntityV1);
        });
        campaignEntityV1.setProducts(campaignProductsEntityList);
        return campaignEntityV1;
    }

    public CampaignEntityV1 setCampaignOffers(CampaignEntityV1 campaignEntityV1,
                                              List<CampaignOffersV1> campaignOffersV1s) {
        List<CampaignOffersEntityV1> campaignOffersEntityV1List = new LinkedList<>();
        campaignOffersV1s.stream().forEach(campaignOffersV1 -> {
            CampaignOffersEntityV1 campaignOffersEntityV1 = new CampaignOffersEntityV1();
            campaignOffersEntityV1.setCampaignId(campaignOffersV1.getCampaignOfferId().getCampaignId());
            campaignOffersEntityV1.setOfferType(campaignOffersV1.getCampaignOfferId().getOfferType());
            if (!Objects.isNull(campaignOffersV1.getPaymentOffer())) {
                campaignOffersEntityV1.setPaymentOffer(campaignOffersV1.getPaymentOffer());
            }
            if (!Objects.isNull(campaignOffersV1.getCommissionValue())) {
                campaignOffersEntityV1.setCommissionValue(campaignOffersV1.getCommissionValue());
            }
            if (Objects.nonNull(campaignOffersV1.getCommissionValueTypeId())) {
                campaignOffersEntityV1.setCommissionValueTypeId(campaignOffersV1.getCommissionValueTypeId());
            }
            if (Objects.nonNull(campaignOffersV1.getCommissionAffiliateTypeId())) {
                campaignOffersEntityV1.setCommissionAffiliateTypeId(campaignOffersV1.getCommissionAffiliateTypeId());
            }
            campaignOffersEntityV1.setCommissionValueType(campaignOffersV1.getCommissionValueType());
            campaignOffersEntityV1.setCommissionAffiliateType(campaignOffersV1.getCommissionAffiliateType());
            campaignOffersEntityV1.setCommissionAffiliateDetails(campaignOffersV1.getCommissionAffiliateDetails());
            campaignOffersEntityV1
                    .setCommissionAffiliateCustomName(campaignOffersV1.getCommissionAffiliateCustomName());
            campaignOffersEntityV1List.add(campaignOffersEntityV1);
        });
        campaignEntityV1.setOffers(campaignOffersEntityV1List);
        return campaignEntityV1;
    }

    public CampaignEntityV1 setCampaignTasks(CampaignEntityV1 campaignEntityV1,
                                             List<CampaignTasksV1> campaignTasksV1s) {
        List<CampaignTasksEntityV1> campaignTasksEntityList = new LinkedList<>();
        List<PlatformMasterV1> platformMasterList = masterServiceV1.getPlatformMaster();
        campaignTasksV1s.stream().forEach(tasks -> {
            CampaignTasksEntityV1 campaignTasksEntityV1 = new CampaignTasksEntityV1();
            campaignTasksEntityV1.setPostTypeId(tasks.getPostTypeId());
            campaignTasksEntityV1.setPlatformId(tasks.getPlatformId());
            campaignTasksEntityV1.setGuidelines(tasks.getGuidelines());
            campaignTasksEntityV1.setPostCustomName(tasks.getPostCustomName());
            campaignTasksEntityV1.setTaskId(tasks.getTaskId());
            List<PlatformMasterV1> platformCampRel = platformMasterList.stream().filter(platformMasterV1 -> Objects.equals(platformMasterV1.getPlatformId(), tasks.getPlatformId()))
                    .collect(Collectors.toList());
            if (!platformCampRel.isEmpty()) {
                campaignTasksEntityV1.setPlatform_name(platformCampRel.get(0).getPlatformName());
            }
            campaignTasksEntityList.add(campaignTasksEntityV1);
        });
        campaignEntityV1.setTasks(campaignTasksEntityList);
        return campaignEntityV1;
    }

    public CampaignEntityV1 setPlatformMaster(CampaignEntityV1 campaignEntityV1,
                                              List<PlatformMasterV1> platformMasterV1s) {
        List<PlatformMasterEntityV1> platformMasterList = new LinkedList<>();
        platformMasterV1s.stream().forEach(platform -> {
            PlatformMasterEntityV1 platformMasterEntityV1 = new PlatformMasterEntityV1();
            platformMasterEntityV1.setPlatformId(platform.getPlatformId());
            platformMasterEntityV1.setPlatformName(platform.getPlatformName());
            platformMasterList.add(platformMasterEntityV1);
        });
        campaignEntityV1.setPlatformMaster(platformMasterList);
        return campaignEntityV1;
    }

    public CampaignEntityV1 setPlatformPostMaster(CampaignEntityV1 campaignEntityV1,
                                                  List<PlatformPostTypeMasterV1> platformPostTypeMasterV1s) {
        List<PlatformPostTypeEntityV1> platformPostMasterList = new LinkedList<>();
        platformPostTypeMasterV1s.stream().forEach(post -> {
            PlatformPostTypeEntityV1 platformPostMasterEntityV1 = new PlatformPostTypeEntityV1();
            platformPostMasterEntityV1.setPostTypeId(post.getPostTypeId());
            platformPostMasterEntityV1.setPlatformId(post.getPlatformId());
            platformPostMasterEntityV1.setPostTypeName(post.getPostTypeName());
            platformPostMasterList.add(platformPostMasterEntityV1);
        });
        campaignEntityV1.setPlatformPostMaster(platformPostMasterList);
        return campaignEntityV1;
    }

    public CampaignEntityV1 setTermsCondition(CampaignEntityV1 campaignEntityV1,
                                              List<TermsConditionsV1> termsConditionsV1s) {
        List<TermsConditionsEntityV1> termsConditionList = new LinkedList<>();
        // if(!termsConditionsV1s.isEmpty()){
        termsConditionsV1s.stream().forEach(terms -> {
            TermsConditionsEntityV1 termsConditionsEntityV1 = new TermsConditionsEntityV1();
            termsConditionsEntityV1.setTermId(terms.getTermId());
            termsConditionsEntityV1.setTermsCondition(terms.getTermsCondition());
            termsConditionsEntityV1.setTermsConditionFilePath(terms.getTermsConditionFilePath());
            termsConditionsEntityV1.setIsActive(terms.getIsActive());
            termsConditionsEntityV1.setType(terms.getType());

            termsConditionList.add(termsConditionsEntityV1);
        });
        campaignEntityV1.setTermsConditionMaster(termsConditionList);

        return campaignEntityV1;
    }

    public CampaignEntityV1 setCurrencyMaster(CampaignEntityV1 campaignEntityV1,
                                              List<CurrencyMasterV1> currencyMasterV1s) {
        List<CurrencyMasterEntityV1> currencyMasterEntityV1List = new LinkedList<>();
        currencyMasterV1s.stream().forEach(currencyMasterV1 -> {
            CurrencyMasterEntityV1 currencyMasterEntityV1 = new CurrencyMasterEntityV1();
            currencyMasterEntityV1.setCurrencyId(currencyMasterV1.getCurrencyId());
            currencyMasterEntityV1.setCurrencyValue(currencyMasterV1.getCurrency());
            currencyMasterEntityV1List.add(currencyMasterEntityV1);
        });
        campaignEntityV1.setCurrencyMaster(currencyMasterEntityV1List);
        return campaignEntityV1;
    }

    public CampaignEntityV1 setCommissionAffilateTypeMaster(CampaignEntityV1 campaignEntityV1,
                                                            List<CommissionAffiliateTypeMasterV1> commissionAffiliateTypeMasterV1s) {
        List<CommissionAffiliateMasterEntityV1> commissionAffiliateMasterEntityList = new LinkedList<>();
        commissionAffiliateTypeMasterV1s.stream().forEach(commissionAffiliateTypeMasterV1 -> {
            CommissionAffiliateMasterEntityV1 commissionAffiliateMasterEntityV1 = new CommissionAffiliateMasterEntityV1();
            commissionAffiliateMasterEntityV1.setCaId(commissionAffiliateTypeMasterV1.getCommAffiTypeId());
            commissionAffiliateMasterEntityV1.setCaValue(commissionAffiliateTypeMasterV1.getCommAffiName());
            commissionAffiliateMasterEntityList.add(commissionAffiliateMasterEntityV1);

        });
        campaignEntityV1.setCommissionAffiliateMaster(commissionAffiliateMasterEntityList);
        return campaignEntityV1;
    }

    public CampaignEntityV1 getCampaignEntityForm() {
        CampaignEntityV1 campaignEntityV1 = new CampaignEntityV1();

        List<PlatformMasterV1> platformMasterList = masterServiceV1.getPlatformMaster();
        campaignEntityV1 = setPlatformMaster(campaignEntityV1, platformMasterList);
        List<PlatformPostTypeMasterV1> platformPostMasterList = masterServiceV1.getPlatformPostMaster();
        campaignEntityV1 = setPlatformPostMaster(campaignEntityV1, platformPostMasterList);
        List<CurrencyMasterV1> currencyMasterList = masterServiceV1.getCurrencyMaster();
        campaignEntityV1 = setCurrencyMaster(campaignEntityV1, currencyMasterList);

        List<CommissionAffiliateTypeMasterV1> commissionAffiliateTypeList = masterServiceV1
                .getCommissionAffiliateTypeMaster();
        campaignEntityV1 = setCommissionAffilateTypeMaster(campaignEntityV1, commissionAffiliateTypeList);

        List<TermsConditionsV1> termsConditionsList = campaignEntityServiceV1
                .getTermsConditionsByTypeAndIsActive(TermsTypeEntity.STANDARD, IsActive.YES);
        campaignEntityV1 = setTermsCondition(campaignEntityV1, termsConditionsList);
        return campaignEntityV1;

    }

    public List<BrandCampaignDetailsV1Entity> getBrandCampaignDetailsNew(UUID brandId) {
        return brandCampaignDetailsV1Repo.findAllByBrandIdAndCampaignStatus(brandId.toString(), "ACTIVE");
    }

//    public List<BrandCampaignDetailsV1> getCampaignInfluencerAndCampaignStatus() {
//        UUID brandId = ResourceServer.getUserId();
//        List<BrandCampaignDetailsV1> finalList = new LinkedList<>();
//        List<BrandCampaignDetailsV1> brandCampaignDetailsListNew = new LinkedList<>();
//
//        List<BrandCampaignDetailsV1> brandCampaignDetailsV1List = brandCampaignDetailsRepositoryV1
//                .findAllByBrandIdAndCampaignStatus(brandId.toString(), CampaignStatusEntityV1.ACTIVE);
//        if (!brandCampaignDetailsV1List.isEmpty()) {
//            brandCampaignDetailsListNew = brandCampaignDetailsV1List.stream().map(brandCampaignDetailsV1 -> {
//                List<CampaignTasksV1> campaignTasksV1 = campaignEntityServiceV1
//                        .getTasksByCampaignId(brandCampaignDetailsV1.getCampaignId());
//                if (!campaignTasksV1.isEmpty()) {
//                    Long platformId = campaignTasksV1.get(0).getPlatformId();
//                    brandCampaignDetailsV1.setPlatformId(platformId);
//                    PlatformMasterV1 platform = masterServiceV1.getPlatformById(platformId);
//                    brandCampaignDetailsV1.setPlatformName(platform.getPlatformName());
//                }
//                return brandCampaignDetailsV1;
//            }).collect(Collectors.toList());
//        }
//
//        return brandCampaignDetailsListNew;
//    }

    public List<ActiveCampaignMapper> getCampaignInfluencerAndCampaignStatus() {
        List<ActiveCampaignMapper> allActiveCampaign;
        try {
            allActiveCampaign = procedureClass.getAllActiveCampaign();
        } catch (JsonProcessingException e) {
            mailService.sendErrorMailDetail(e);
            throw new RuntimeException(e);
        }

        return allActiveCampaign;
    }

    public ResponseEntity<Object> getNewCampaign() {
        Logger.getLogger("getNewCampaign").log(Level.FINE, "Get New campaign method called");
        try {
            UUID brandId = ResourceServer.getUserId();
            List<io.smartfluence.modal.brand.viewinfluencer.BrandCampaigns> brandCampaigns = new LinkedList<io.smartfluence.modal.brand.viewinfluencer.BrandCampaigns>();
            List<BrandCampaignDetailsV1Entity> brandCampaignDetails = getBrandCampaignDetailsNew(brandId);
            if (!brandCampaignDetails.isEmpty()) {
                for (BrandCampaignDetailsV1Entity brandCampaignDetail : brandCampaignDetails) {
                    // Date date = brandCampaignDetail.getCreatedAt();
                    // LocalDate localDate =
                    // date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    // Integer year = localDate.getYear();
                    // String month = localDate.getMonth().toString();
                    // Integer day = localDate.getDayOfMonth();
                    // String string = month.substring(0, 3);
                    // String createdAt = day.toString() + "-" + string + "-" +
                    // year.toString().substring(2);
                    brandCampaigns
                            .add(io.smartfluence.modal.brand.viewinfluencer.BrandCampaigns.builder().brandId(brandId)
                                    .campaignId(UUID.fromString(brandCampaignDetail.getCampaignId()))
                                    .campaignName(brandCampaignDetail.getCampaignName())
                                    .createdAt(brandCampaignDetail.getCreatedAt())
                                    // .campaignStatus(CampaignStatus.valueOf(brandCampaignDetail.getCampaignStatus())).stringCreatedAt(createdAt)
                                    // .createdAt(brandCampaignDetail.getCreatedAt()).isPromote(brandCampaignDetail.getIsPromote())
                                    .influencerCount(brandCampaignDetail.getInfluencerCount()).build());
                }
                List<io.smartfluence.modal.brand.viewinfluencer.BrandCampaigns> sortedBrandCampaigns = brandCampaigns
                        .stream()
                        .sorted(Comparator.comparing(BrandCampaigns::getCreatedAt).reversed())
                        .collect(Collectors.toList());
                return ResponseEntity.ok(sortedBrandCampaigns);
            }
            return ResponseEntity.ok(brandCampaigns);
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger("getNewCampaign").log(Level.SEVERE, e.getMessage());
            return ResponseEntity.badRequest().build();
        }
    }

    public List<CampaignInfluencerV1Entity> getAllInfCampaign(String campaignId) {
        return campaignInfluencerRepo.findAllByCampaignInfluencerId_CampaignId(campaignId);
    }

    public ResponseEntity<Object> getAllInfluencerByCampaignId(String campaignId) {
        Logger.getLogger("getAllInfluencerByCampaignId").log(Level.FINE, "Get All influencer method called");

        try {
            List<AddInfToCampResponse> addInfToCampResponses = new LinkedList<AddInfToCampResponse>();
            List<CampaignInfluencerV1Entity> campaignInfluencerV1Entities = getAllInfCampaign(campaignId);
            if (!campaignInfluencerV1Entities.isEmpty()) {

                List<String> influencerIds = new ArrayList();
                List<String> instagramHandles = new LinkedList<>();
                List<String> youtubeHandles = new LinkedList<>();
                List<String> tiktokHandles = new LinkedList<>();
                List<InfluencerDemographicsMapper> influencerDemographicsList = new ArrayList<>();
                List<CurrencyMasterV1> currencyMasterV1 = masterServiceV1.getCurrencyMaster();
                List<CommissionAffiliateTypeMasterV1> commissionAffiliateTypeMasterList = masterServiceV1.getCommissionAffiliateTypeMaster();
                for (CampaignInfluencerV1Entity campaignInfluencerV1Entity : campaignInfluencerV1Entities) {
                    influencerIds.add(campaignInfluencerV1Entity.getCampaignInfluencerId().getInfluencerId());
                }
                List<SocialIdMaster> socialIdMasterList = socialIdMasterRepo.findAllByUserIdIn(influencerIds);
                for (int i = 0; i < socialIdMasterList.size(); i++) {
                    InfluencerDemographicsMapper demographicsMapper = reportsService.getInfluencerDemographics(socialIdMasterList.get(i), null, null,
                            false);
                    influencerDemographicsList.add(demographicsMapper);
                }
                socialIdMasterList.parallelStream().filter(f -> f.getPlatform().equals(Platforms.INSTAGRAM))
                        .forEach(e -> instagramHandles.add(e.getInfluencerHandle()));
                socialIdMasterList.parallelStream().filter(f -> f.getPlatform().equals(Platforms.YOUTUBE))
                        .forEach(e -> youtubeHandles.add(e.getInfluencerHandle()));
                socialIdMasterList.parallelStream().filter(f -> f.getPlatform().equals(Platforms.TIKTOK))
                        .forEach(e -> tiktokHandles.add(e.getInfluencerHandle()));
                List<CampaignInfluencerOfferV1Entity> campaignOffersByCampaignId = campaignEntityServiceV1.getCampaignInfOffersByCampaignId(campaignId);
                List<CampaignProductsV1> campaignProductsV1List = campaignEntityServiceV1.getProductsByCampaignId(campaignId);
                for (CampaignInfluencerV1Entity campaignInfluencerV1Entity : campaignInfluencerV1Entities) {
                    OfferTypeResponse offerTypeResponses = new OfferTypeResponse();

                    List<InfluencerDemographicsMapper> demographicsMapper = influencerDemographicsList.stream().filter(influencerDemographicsMapper1 ->
                            Objects.equals(influencerDemographicsMapper1.getUserId().toString(), campaignInfluencerV1Entity.getCampaignInfluencerId().getInfluencerId().toString())).collect(Collectors.toList());

                    List<CampaignInfluencerOfferV1Entity> campaignOffersByCampaignIds = campaignOffersByCampaignId.stream().filter(campaignOffersByCampId ->
                            Objects.equals(campaignOffersByCampId.getCampaignOfferInfluencerId().getInfluencerId(), campaignInfluencerV1Entity.getCampaignInfluencerId().getInfluencerId())).collect(Collectors.toList());

                    if (!campaignOffersByCampaignIds.isEmpty()) {

                        List<CampaignInfluencerOfferV1Entity> campaignPaymentOffer = campaignOffersByCampaignIds.stream()
                                .filter(campaignOffersV1 -> Objects.equals(
                                        campaignOffersV1.getCampaignOfferInfluencerId().getOfferType(),
                                        OfferTypeEntityV1.PAYMENT.toString()))
                                .collect(Collectors.toList());
                        List<CampaignInfluencerOfferV1Entity> campaignCommissionOffer = campaignOffersByCampaignIds.stream()
                                .filter(campaignOffersV1 -> Objects.equals(
                                        campaignOffersV1.getCampaignOfferInfluencerId().getOfferType(),
                                        OfferTypeEntityV1.COMMISSION.toString()))
                                .collect(Collectors.toList());
                        List<CampaignInfluencerOfferV1Entity> campaignProductOffer = campaignOffersByCampaignIds.stream()
                                .filter(campaignOffersV1 -> Objects.equals(
                                        campaignOffersV1.getCampaignOfferInfluencerId().getOfferType(),
                                        OfferTypeEntityV1.PRODUCT.toString()))
                                .collect(Collectors.toList());

                        if (!campaignPaymentOffer.isEmpty()) {
                            for (int i = 0; i < campaignPaymentOffer.size(); i++) {
                                offerTypeResponses.setPaymentOffer(campaignPaymentOffer.get(i).getPaymentOffer());
                            }
                        }
                        if (!campaignCommissionOffer.isEmpty()) {
                            for (int i = 0; i < campaignCommissionOffer.size(); i++) {
                                Integer commissionValueTypeId = campaignCommissionOffer.get(i)
                                        .getCommissionValueTypeId();
                                List<CurrencyMasterV1> currencyMasterList = currencyMasterV1.stream()
                                        .filter(currencyMaster -> Objects.equals(commissionValueTypeId,
                                                currencyMaster.getCurrencyId()))
                                        .collect(Collectors.toList());
                                if (!currencyMasterList.isEmpty()) {
                                    offerTypeResponses.setCurrency((currencyMasterList.get(0).getCurrency()));
                                }
                                Integer commissionAffId = campaignCommissionOffer.get(i).getCommissionAffiliateTypeId();
                                List<CommissionAffiliateTypeMasterV1> commissionAffiliateTypeMaster = commissionAffiliateTypeMasterList.stream()
                                        .filter(commissionAffiliateTypeMasterV1 -> Objects.equals(commissionAffId,
                                                commissionAffiliateTypeMasterV1.getCommAffiTypeId()))
                                        .collect(Collectors.toList());
                                if (!commissionAffiliateTypeMaster.isEmpty()) {
                                    offerTypeResponses.setCommissionAffiliateTypeName(
                                            commissionAffiliateTypeMaster.get(0).getCommAffiName());
                                }
                                offerTypeResponses.setCommissionAffiliateType(
                                        campaignCommissionOffer.get(i).getCommissionAffiliateType());
                                offerTypeResponses.setCommissionAffTypeId(
                                        campaignCommissionOffer.get(i).getCommissionAffiliateTypeId());
                                offerTypeResponses.setCommissionValue(
                                        campaignCommissionOffer.get(i).getCommissionValue());
                                offerTypeResponses.setCommissionAffiliateCustomName(
                                        campaignCommissionOffer.get(i).getCommissionAffiliateCustomName());
                            }
                        }
                        if (!campaignProductOffer.isEmpty()) {
                            if (!campaignProductsV1List.isEmpty()) {
                                offerTypeResponses.setCampaignProductsV1List(campaignProductsV1List);
                            }
                        }
                    } else {
                        offerTypeResponses = null;
                    }
                    String followers = "";
                    String engagement = "";
                    String pricing = "";
                    if (!demographicsMapper.isEmpty()) {
                        followers = demographicsMapper.get(0).getFollowers().toString();
                        engagement = demographicsMapper.get(0).getEngagement().toString();
                        pricing = demographicsMapper.get(0).getPricing().toString();
                    } else {
                        mailService.sendErrorMailDetail(new Exception("demographicsMapper influencer not found : " + campaignInfluencerV1Entity.getCampaignInfluencerId().getInfluencerId()));
                    }
                    addInfToCampResponses.add(AddInfToCampResponse.builder()
                            .campaignId(campaignInfluencerV1Entity.getCampaignInfluencerId().getCampaignId())
                            .influencerId(campaignInfluencerV1Entity.getCampaignInfluencerId().getInfluencerId())
                            .platform(campaignInfluencerV1Entity.getPlatform())
                            .offerTypeResponse(offerTypeResponses)
                            .proposalStatus(campaignInfluencerV1Entity.getProposalStatus())
                            .influencerHandle(campaignInfluencerV1Entity.getInfluencerHandle())
                            .influencerEmail(campaignInfluencerV1Entity.getInfluencerEmail())
                            .influencerFollowers(followers)
                            .influencerEngagement(engagement)
                            .influencerPricing(pricing)
                            .build());
                }
            }
            return ResponseEntity.ok(addInfToCampResponses);
        } catch (Exception e) {
            e.printStackTrace();
            mailService.sendErrorMailDetail(e);
            Logger.getLogger("getAllInfluencerByCampaignId").log(Level.SEVERE, e.getMessage());
            return ResponseEntity.badRequest().build();
        }
    }


    public ResponseObj deleteCampaignById(String campaignId, String status) {
        BrandCampaignDetailsV1 existingCampaign = campaignEntityServiceV1.getCampaignById(campaignId);
        if (Objects.nonNull(existingCampaign)) {
            if (status.equals("DELETE")) {
                if (existingCampaign.getCampaignStatus().equals(CampaignStatusEntityV1.INPROGRESS)
                        || existingCampaign.getCampaignStatus().equals(CampaignStatusEntityV1.ACTIVE)) {
                    if (existingCampaign.getCampaignStatus().equals(CampaignStatusEntityV1.ACTIVE)) {
                        if (existingCampaign.getInfluencers().isEmpty()) {
                            existingCampaign.setCampaignStatus(CampaignStatusEntityV1.INACTIVE);
                        } else {
                            return new ResponseObj("653", null, Collections.singletonMap("message",
                                    "Cant Delete campaign,Influencer count=" + existingCampaign.getInfluencerCount()));
                        }
                    }
                    existingCampaign.setCampaignStatus(CampaignStatusEntityV1.INACTIVE);
                    BrandCampaignDetailsV1 brandCampaignDetailsV1 = campaignEntityServiceV1
                            .saveBrandCampaignDetails(existingCampaign);

                    if (brandCampaignDetailsV1.getCampaignStatus().equals(CampaignStatusEntityV1.INACTIVE)) {
                        return new ResponseObj("600", null, null);
                    } else {
                        return new ResponseObj("700", null,
                                Collections.singletonMap("message", "Something wrong please try again later"));
                    }
                } else {
                    return new ResponseObj("653", null, Collections.singletonMap("message",
                            "Cant Delete campaign,Campaign is in" + existingCampaign.getCampaignStatus() + " status"));
                }
            } else if (status.equals("END")) {
                if (existingCampaign.getCampaignStatus().equals(CampaignStatusEntityV1.ACTIVE)) {
                    int count = campaignInfluencerRepo.countByCampaignInfluencerId_CampaignId(campaignId);
                    if (count != 0) {
                        new ResponseObj("655", null,
                                Collections.singletonMap("message", "influencers are present in that campaign"));
                    }
                    existingCampaign.setCampaignStatus(CampaignStatusEntityV1.CLOSED);
                    BrandCampaignDetailsV1 brandCampaignDetailsV1 = campaignEntityServiceV1
                            .saveBrandCampaignDetails(existingCampaign);
                    if (brandCampaignDetailsV1.getCampaignStatus().equals(CampaignStatusEntityV1.CLOSED)) {
                        return new ResponseObj("600", null, null);
                    } else {
                        return new ResponseObj("700", null,
                                Collections.singletonMap("message", "Something wrong please try again later"));
                    }
                } else
                    return new ResponseObj("653", null, Collections.singletonMap("message",
                            "Can't End campaign,Campaign is in" + existingCampaign.getCampaignStatus() + " status"));
            } else {
                return new ResponseObj("654", null, Collections.singletonMap("message", "CampaignId is invalid"));
            }
        } else {
            return new ResponseObj("654", null, Collections.singletonMap("message", "CampaignId is invalid"));
        }
    }

    public ResponseObj getInfluencersByCampaignId(String campaignId, List<String> proposalStatus) {
        LOG.log(Level.INFO, campaignId, proposalStatus);
        try {
            BrandCampaignDetailsV1 campaignInfluencers = campaignEntityServiceV1.getCampaignByIdEager(campaignId);
            CampInfluRespObj campInfluRespObj = new CampInfluRespObj();
            InfluObj influObj = new InfluObj();
            if (campaignInfluencers != null) {
                if (campaignInfluencers.getInfluencers().isEmpty()) {
                    return new ResponseObj("700", Collections.singletonMap("message", "No Influencers present"));
                } else {
                    Set<InfluObj> influObjs = setInfluencersEntity(campaignInfluencers, proposalStatus);
                    if (influObjs.isEmpty()) {
                        return new ResponseObj("700", Collections.singletonMap("message", "No Influencers present"));
                    }
                    List<CurrencyMasterEntityV1> currencyMaster = masterServiceV1.getCurrencyMaster().stream()
                            .map(currencyMasterV1 -> new CurrencyMasterEntityV1(currencyMasterV1.getCurrencyId(),
                                    currencyMasterV1.getCurrency()))
                            .collect(Collectors.toList());
                    List<CommissionAffiliateMasterEntityV1> commissionAffiliateMaster = masterServiceV1
                            .getCommissionAffiliateTypeMaster().stream()
                            .map(commissionAffiliate -> new CommissionAffiliateMasterEntityV1(
                                    commissionAffiliate.getCommAffiTypeId(), commissionAffiliate.getCommAffiName()))
                            .collect(Collectors.toList());
                    campInfluRespObj.setInfluencersList(new ArrayList<>(influObjs));
                    campInfluRespObj.setCurrencyMaster(currencyMaster);
                    campInfluRespObj.setAffiliateMaster(commissionAffiliateMaster);
                    return new ResponseObj("600", campInfluRespObj);
                }
            } else {
                return new ResponseObj("701", null, Collections.singletonMap("message", "Invalid campaign id"));

            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.toString());
            LOG.log(Level.SEVERE, e.getMessage());
            mailService.sendErrorMailDetail(e);
            return new ResponseObj("500", null, Collections.singletonMap("message", e.getMessage()));
        }
    }

    public Set<InfluObj> setInfluencersEntity(BrandCampaignDetailsV1 brandCampaignDetailsV1,
                                              List<String> proposalStatus) {
        List<PlatformMasterV1> platformMasterList = masterServiceV1.getPlatformMaster();
        List<PlatformPostTypeMasterV1> platformPostMasterList = masterServiceV1.getPlatformPostMaster();
        List<InfluObj> result = brandCampaignDetailsV1.getInfluencers().stream().filter(campaignInfluencerV1Entity -> {
            return proposalStatus.contains(campaignInfluencerV1Entity.getProposalStatus().toString());
            // campaignInfluencerV1Entity.getProposalStatus().toString().equals(proposalStatus);
        }).map(campaignInfluencerV1Entity -> {
            InfluObj influObj = new InfluObj();
            influObj.setCampaignId(brandCampaignDetailsV1.getCampaignId());
            influObj.setInfluencerId(campaignInfluencerV1Entity.getCampaignInfluencerId().getInfluencerId());
            influObj.setInfluencerHandle(campaignInfluencerV1Entity.getInfluencerHandle());
            influObj.setFollowers(campaignInfluencerV1Entity.getInfluencerFollowers());
            influObj.setEngagementRate(campaignInfluencerV1Entity.getInfluencerEngagement());
            influObj.setPlatform(campaignInfluencerV1Entity.getPlatform());
            influObj.setProposalStatus(campaignInfluencerV1Entity.getProposalStatus());
            influObj.setPostUrl(campaignInfluencerV1Entity.getPostUrl());
            influObj.setTrackingId(campaignInfluencerV1Entity.getTrackingId());
            influObj.setPaymentMethod(campaignInfluencerV1Entity.getPaymentMethodType());
            influObj.setInfluencerEmail(campaignInfluencerV1Entity.getInfluencerEmail());
            influObj.setCampaignName(brandCampaignDetailsV1.getCampaignName());
            influObj.setPaymentEmailAddress(campaignInfluencerV1Entity.getPaymentEmailAddress());
            influObj.setCampaignStatus(brandCampaignDetailsV1.getCampaignStatus().toString());
            List<InfluOfferObj> influOfferList = campaignInfluencerV1Entity.getInfluencerOffers().stream()
                    .map(campaignInfluencerOfferV1Entity -> {
                        InfluOfferObj influOfferObj = new InfluOfferObj();
                        influOfferObj.setCampaignId(
                                campaignInfluencerOfferV1Entity.getCampaignOfferInfluencerId().getCampaignId());
                        influOfferObj.setInfluencerId(
                                campaignInfluencerOfferV1Entity.getCampaignOfferInfluencerId().getInfluencerId());
                        influOfferObj.setOfferType(OfferTypeEntityV1.valueOf(
                                campaignInfluencerOfferV1Entity.getCampaignOfferInfluencerId().getOfferType()
                                        .toString()));
                        if (Objects.equals(
                                campaignInfluencerOfferV1Entity.getCampaignOfferInfluencerId().getOfferType(),
                                "PAYMENT")) {
                            influOfferObj.setPaymentOffer(campaignInfluencerOfferV1Entity.getPaymentOffer());
                        } else if (Objects.equals(
                                campaignInfluencerOfferV1Entity.getCampaignOfferInfluencerId().getOfferType(),
                                "COMMISSION")) {
                            influOfferObj.setCommissionValueTypeId(
                                    campaignInfluencerOfferV1Entity.getCommissionValueTypeId());
                            influOfferObj.setCommissionAffiliateTypeId(
                                    campaignInfluencerOfferV1Entity.getCommissionAffiliateTypeId());
                            influOfferObj.setCommissionValue(campaignInfluencerOfferV1Entity.getCommissionValue());
                            influOfferObj.setCommissionAffiliateType(
                                    campaignInfluencerOfferV1Entity.getCommissionAffiliateType());
                            influOfferObj.setCommissionAffiliateCustomName(
                                    campaignInfluencerOfferV1Entity.getCommissionAffiliateCustomName());
                        }
                        return influOfferObj;
                    }).collect(Collectors.toList());
            influObj.setOffers(influOfferList);
            List<InfluAddressObj> influAddressObjs = campaignInfluencerV1Entity.getInfluencerAddresses().stream()
                    .map(campaignInfluencerAddress -> new InfluAddressObj(influObj.getCampaignId(),
                            campaignInfluencerAddress.getCampaignInfluencerAddressType().getInfluencerId(),
                            AddressTypeEntity.valueOf(campaignInfluencerAddress.getCampaignInfluencerAddressType()
                                    .getAddressType().toString()),
                            campaignInfluencerAddress.getFullName(), campaignInfluencerAddress.getAddressLine1(),
                            campaignInfluencerAddress.getAddressLine2(), campaignInfluencerAddress.getCountryId(),
                            campaignInfluencerAddress.getStateId(), campaignInfluencerAddress.getCityName(),
                            campaignInfluencerAddress.getZipcode(), campaignInfluencerAddress.getPhoneNumber()))
                    .collect(Collectors.toList());
            influObj.setAddress(influAddressObjs);
            List<InfluProductsObj> influProductsObjs = campaignInfluencerV1Entity.getInfluencerProducts().stream()
                    .map(campaignInfluencerProducts -> new InfluProductsObj(influObj.getCampaignId(),
                            campaignInfluencerProducts.getCipId().getInfluencerId(),
                            campaignInfluencerProducts.getCampaignProducts().getProductId(),
                            campaignInfluencerProducts.getCampaignProducts().getProductName(),
                            campaignInfluencerProducts.getCampaignProducts().getProductLink()))
                    .collect(Collectors.toList());
            influObj.setProducts(influProductsObjs);
            List<InfluTasksObj> influTasksObj = campaignInfluencerV1Entity.getInfluencerTasks().stream()
                    .map(campaignInfluencerTask -> new InfluTasksObj(influObj.getCampaignId(),
                            campaignInfluencerTask.getCitId().getInfluencerId(),
                            campaignInfluencerTask.getCampaignTasks().getTaskId(),
                            campaignInfluencerTask.getCampaignTasks().getPlatformId(),
                            campaignInfluencerTask.getCampaignTasks().getGuidelines(),
                            campaignInfluencerTask.getCampaignTasks().getPostTypeId(),
                            campaignInfluencerTask.getCampaignTasks().getPostCustomName()))
                    .collect(Collectors.toList());
            influObj.setTasks(influTasksObj);
            List<InfluPlatformMaster> influPlatformObj = platformMasterList.stream()
                    .map(platform -> new InfluPlatformMaster(platform.getPlatformId(),
                            platform.getPlatformName()))
                    .collect(Collectors.toList());
            influObj.setPlatformMasters(influPlatformObj);
            List<InfluPostTypeMaster> influPostObj = platformPostMasterList.stream()
                    .map(platformPost -> new InfluPostTypeMaster(platformPost.getPostTypeId(),
                            platformPost.getPostTypeName(), platformPost.getPlatformId()))
                    .collect(Collectors.toList());
            influObj.setPostTypeMasters(influPostObj);
            return influObj;
        }).collect(Collectors.toList());
        return new HashSet(result);

    }

    public Map<String, String> validateOffers(InfluObj influObj) {
        List<CurrencyMasterV1> currencyMaster = masterServiceV1.getCurrencyMaster();
        List<CommissionAffiliateTypeMasterV1> commissionAffiliateTypeMaster = masterServiceV1.getCommissionAffiliateTypeMaster();
        Map<String, String> value = new HashMap<>();
        if (!influObj.getOffers().isEmpty()) {
            if (influObj.getOffers().size() >= 3) {
                value.put(MESSAGE, MAXTWOOFFERS);
                value.put(CONDITION, NOTACCEPTED);
                return value;
            } else {
                List<String> offers = new LinkedList<>();
                for (int i = 0; i < influObj.getOffers().size(); i++) {
                    if (Objects.equals(influObj.getOffers().get(i).getOfferType().toString(), PAYMENTOFFER)) {
                        if (offers.contains(influObj.getOffers().get(i).getOfferType().toString())) {
                            value.put(MESSAGE, SAMEPAYMENTOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        } else if (offers.contains(PRODUCTOFFER)) {
                            value.put(MESSAGE, INVALIDPAYMENTOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                        }
                        if (Objects.nonNull(influObj.getOffers().get(i).getPaymentOffer())) {
                            if (influObj.getOffers().get(i).getPaymentOffer() <= 0) {
                                value.put(MESSAGE, INVALIDPAYMENTFIELD);
                                value.put(CONDITION, NOTACCEPTED);
                                return value;
                            }
                            offers.add(influObj.getOffers().get(i).getOfferType().toString());
                        } else {
                            value.put(MESSAGE, INVALIDPAYMENTFIELD);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        }
                    } else if (Objects.equals(influObj.getOffers().get(i).getOfferType().toString(), COMMISSIONOFFER)) {
                        if (offers.contains(influObj.getOffers().get(i).getOfferType().toString())) {
                            value.put(MESSAGE, SAMECOMMISSIONOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        }
                        if (Objects.nonNull(influObj.getOffers().get(i).getCommissionValue())) {
                            if (influObj.getOffers().get(i).getCommissionValue() <= 0) {
                                value.put(MESSAGE, INVALIDCOMMISSIONFIELD);
                                value.put(CONDITION, NOTACCEPTED);
                                return value;
                            }
                            if (Objects.nonNull(influObj.getOffers().get(i).getCommissionValueTypeId())
                                    && Objects.nonNull(influObj.getOffers().get(i).getCommissionAffiliateTypeId())) {
                                InfluOfferObj influOfferObj = influObj.getOffers().get(i);
                                boolean currencyValidate = currencyMaster.stream().anyMatch(currency -> Objects.equals(currency.getCurrencyId(), influOfferObj.getCommissionValueTypeId()));
                                boolean affiliateValidate = commissionAffiliateTypeMaster.stream().anyMatch(affiliate ->
                                        Objects.equals(affiliate.getCommAffiTypeId(), influOfferObj.getCommissionAffiliateTypeId()));
                                if (currencyValidate && affiliateValidate) {
                                    offers.add(influObj.getOffers().get(i).getOfferType().toString());
                                } else {
                                    value.put(MESSAGE, INVALIDCOMMISSIONFIELD);
                                    value.put(CONDITION, NOTACCEPTED);
                                    return value;
                                }
                            } else {
                                if (influObj.getOffers().get(i).getCommissionValue() <= 0) {
                                    value.put(MESSAGE, INVALIDCOMMISSIONFIELD);
                                    value.put(CONDITION, NOTACCEPTED);
                                    return value;
                                }
                            }

                        } else {
                            value.put(MESSAGE, INVALIDCOMMISSIONFIELD);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        }
                    } else if (Objects.equals(influObj.getOffers().get(i).getOfferType().toString(), PRODUCTOFFER)) {
                        if (offers.contains(influObj.getOffers().get(i).getOfferType().toString())) {
                            value.put(MESSAGE, SAMEPRODUCTOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        } else if (offers.contains(PAYMENTOFFER)) {
                            value.put(MESSAGE, INVALIDPRODUCTOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        }
                        offers.add(influObj.getOffers().get(i).getOfferType().toString());
                    } else {
                        value.put(MESSAGE, INVALIDOFFER);
                        value.put(CONDITION, NOTACCEPTED);
                        return value;
                    }
                }
                value.put(CONDITION, ACCEPTED);
                return value;

            }

        } else {
            value.put(CONDITION, NOTACCEPTED);
            return value;
        }
    }

    public Map<String, String> validateCampaignOffers(CampaignEntityV1 campaignEntity) {
        Map<String, String> value = new HashMap<>();
        List<CurrencyMasterV1> currencyMaster = masterServiceV1.getCurrencyMaster();
        List<CommissionAffiliateTypeMasterV1> commissionAffiliateTypeMaster = masterServiceV1.getCommissionAffiliateTypeMaster();

        if (!campaignEntity.getOffers().isEmpty()) {
            if (campaignEntity.getOffers().size() >= 3) {
                value.put(MESSAGE, MAXTWOOFFERS);
                value.put(CONDITION, NOTACCEPTED);
                return value;
            } else {
                List<String> offers = new LinkedList<>();
                for (int i = 0; i < campaignEntity.getOffers().size(); i++) {
                    if (Objects.equals(campaignEntity.getOffers().get(i).getOfferType().toString(), PAYMENTOFFER)) {
                        if (offers.contains(campaignEntity.getOffers().get(i).getOfferType().toString())) {
                            value.put(MESSAGE, SAMEPAYMENTOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        } else if (offers.contains(PRODUCTOFFER)) {
                            value.put(MESSAGE, INVALIDPAYMENTOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                        }
                        if (Objects.nonNull(campaignEntity.getOffers().get(i).getPaymentOffer())) {
                            if (campaignEntity.getOffers().get(i).getPaymentOffer() <= 0) {
                                value.put(MESSAGE, INVALIDPAYMENTFIELD);
                                value.put(CONDITION, NOTACCEPTED);
                                return value;
                            }
                            offers.add(campaignEntity.getOffers().get(i).getOfferType().toString());
                        } else {
                            value.put(MESSAGE, INVALIDPAYMENTFIELD);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        }
                    } else if (Objects.equals(campaignEntity.getOffers().get(i).getOfferType().toString(), COMMISSIONOFFER)) {
                        if (offers.contains(campaignEntity.getOffers().get(i).getOfferType().toString())) {
                            value.put(MESSAGE, SAMECOMMISSIONOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        }
                        if (Objects.nonNull(campaignEntity.getOffers().get(i).getCommissionValue())) {
                            if (campaignEntity.getOffers().get(i).getCommissionValue() <= 0) {
                                value.put(MESSAGE, INVALIDCOMMISSIONFIELD);
                                value.put(CONDITION, NOTACCEPTED);
                                return value;
                            }
                            if (Objects.nonNull(campaignEntity.getOffers().get(i).getCommissionValueTypeId())
                                    && Objects.nonNull(campaignEntity.getOffers().get(i).getCommissionAffiliateTypeId())) {
                                CampaignOffersEntityV1 campaignOffers = campaignEntity.getOffers().get(i);
                                boolean currencyValidate = currencyMaster.stream().anyMatch(currency -> Objects.equals(currency.getCurrencyId(), campaignOffers.getCommissionValueTypeId()));
                                boolean affiliateValidate = commissionAffiliateTypeMaster.stream().anyMatch(affiliate ->
                                        Objects.equals(affiliate.getCommAffiTypeId(), campaignOffers.getCommissionAffiliateTypeId()));
                                if (currencyValidate && affiliateValidate) {
                                    offers.add(campaignEntity.getOffers().get(i).getOfferType().toString());
                                } else {
                                    value.put(MESSAGE, INVALIDCOMMISSIONFIELD);
                                    value.put(CONDITION, NOTACCEPTED);
                                    return value;
                                }
                            } else {
                                if (campaignEntity.getOffers().get(i).getCommissionValue() <= 0) {
                                    value.put(MESSAGE, INVALIDCOMMISSIONFIELD);
                                    value.put(CONDITION, NOTACCEPTED);
                                    return value;
                                }
                            }

                        } else {
                            value.put(MESSAGE, INVALIDCOMMISSIONFIELD);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        }
                    } else if (Objects.equals(campaignEntity.getOffers().get(i).getOfferType().toString(), PRODUCTOFFER)) {
                        if (offers.contains(campaignEntity.getOffers().get(i).getOfferType().toString())) {
                            value.put(MESSAGE, SAMEPRODUCTOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        } else if (offers.contains(PAYMENTOFFER)) {
                            value.put(MESSAGE, INVALIDPRODUCTOFFER);
                            value.put(CONDITION, NOTACCEPTED);
                            return value;
                        }
                        offers.add(campaignEntity.getOffers().get(i).getOfferType().toString());
                    } else {
                        value.put(MESSAGE, INVALIDOFFER);
                        value.put(CONDITION, NOTACCEPTED);
                        return value;
                    }
                }
                value.put(CONDITION, ACCEPTED);
                return value;

            }

        } else {
            value.put(CONDITION, NOTACCEPTED);
            return value;
        }
    }

    @Transactional
    public ResponseObj updateInfluObj(InfluObj influObj) {
        try {
            Set<ConstraintViolation<InfluObj>> violations = validator.validate(influObj);
            if (!violations.isEmpty()) {
                return new ResponseObj("651", null, Collections.
                        singletonMap("message", "field validation error"));
            } else {
                boolean offerValidation = influObj.getOffers().stream()
                        .anyMatch(influOfferObj -> {
                            if (Objects.nonNull(influOfferObj.getOfferType())) {
                                if (influOfferObj.getOfferType().toString() == "") {
                                    return true;
                                } else {
                                    return false;
                                }
                            } else {
                                return true;
                            }

                        });
                if (offerValidation) {
                    return new ResponseObj("651", null, Collections.
                            singletonMap("message", "offertype shouldn't be empty"));
                } else {
                    String campaignStatus = brandCampaignDetailsRepositoryV1.findCampaignStatusByCampaignId(influObj.getCampaignId());
                    if (!Objects.equals(campaignStatus, ACTIVE)) {
                        return new ResponseObj("655", null,
                                Collections.singletonMap("message", "Campaign status is  " + campaignStatus + " ,So can't update"));
                    }
                    Map validateOffers = validateOffers(influObj);
                    if (validateOffers.get("cond").equals("accepted")) {
                        List<CampaignInfluencerOfferV1Entity> campaignInfluencerOfferUpdatedList = updateInfluencersoffers(influObj);
                        if (campaignInfluencerOfferUpdatedList.isEmpty()) {
                            return new ResponseObj("652", null, Collections.
                                    singletonMap("message", "Offer type shouldn't be empty"));
                        } else {
                            return new ResponseObj("600", null);
                        }
                    } else {
                        return new ResponseObj("651", null, validateOffers);
                    }
                }
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.toString());
            LOG.log(Level.SEVERE, e.getMessage());
            return new ResponseObj("500", null, Collections.singletonMap("message", e.getMessage()));
        }
    }

    private void deleteCampaignInfluencer(RemoveInfluencerReq removeInfluencerReq) {
        Logger.getLogger("deleteCampaignInfluencer").log(Level.FINE, removeInfluencerReq.toString());
        List<CampaignInfluencerV1Entity> optionalCampaignInfluencerV1 = campaignInfluencerRepo
                .findAllByCampaignInfluencerId_InfluencerIdIn(removeInfluencerReq.getInfluencerIds());
        if (!optionalCampaignInfluencerV1.isEmpty()) {
            campaignInfluencerRepo.deleteAllByCampaignInfluencerId_InfluencerIdInAndCampaignInfluencerId_CampaignId(
                    removeInfluencerReq.getInfluencerIds(), removeInfluencerReq.getCampaignId());
        } else {
            Logger.getLogger("deleteCampaignInfluencer").log(Level.SEVERE,
                    "Delete influencer is failed in campaign_influencer table");
        }
    }

    private boolean deleteCampaignInfOffer(RemoveInfluencerReq removeInfluencerReq) {
        Logger.getLogger("deleteCampaignInfOffer").log(Level.FINE, removeInfluencerReq.toString());
        List<CampaignInfluencerOfferV1Entity> optionalCampaignInfluencerOfferV1Entity = campaignInfOfferV1Repo
                .findAllByCampaignOfferInfluencerId_InfluencerIdInAndCampaignOfferInfluencerId_CampaignId(
                        removeInfluencerReq.getInfluencerIds(), removeInfluencerReq.getCampaignId());
        if (!optionalCampaignInfluencerOfferV1Entity.isEmpty()) {
            campaignInfOfferV1Repo.deleteAllByInfluencerIdInAndCampaignId(removeInfluencerReq.getInfluencerIds(),
                    removeInfluencerReq.getCampaignId());
            // campaignInfOfferV1Repo.deleteAllByCampaignOfferInfluencerId_InfluencerIdInAndCampaignOfferInfluencerId_CampaignId(removeInfluencerReq.getInfluencerIds(),
            // removeInfluencerReq.getCampaignId());
            return true;
        } else {
            Logger.getLogger("deleteCampaignInfOffer").log(Level.SEVERE,
                    "Delete influencer is failed in campaign_influencer_offer table");
            return false;
        }
    }

    @Transactional
    public ResponseEntity<Object> removeNewInfluencer(RemoveInfluencerReq removeInfluencerReq) {
        Logger.getLogger("removeNewInfluencer").log(Level.FINE, removeInfluencerReq.toString());
        try {
            if (!Objects.isNull(removeInfluencerReq)) {
                boolean isDeleted = deleteCampaignInfOffer(removeInfluencerReq);
                if (isDeleted) {
                    deleteCampaignInfluencer(removeInfluencerReq);
                }
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            Logger.getLogger("removeNewInfluencer").log(Level.SEVERE, e.getMessage());
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public ResponseEntity<Object> copyNewInfluencers(DashBoardModalNew dashBoardModal) {
        return addInfluencerDaoImpl.copyNewInfluencers(dashBoardModal);
    }

    public ResponseEntity<Object> uploadNewInfluencers(UploadInfluencersRequestModelNew request, UUID campaignId) {
        return addInfluencerDaoImpl.uploadNewInfluencers(request, campaignId);
    }

    public void generateCampaignPostLinkDetails(List<CampaignPostLinkModal> campaignPostLinkModal, UUID brandId,
                                                UUID campaignId) {
        List<PostAnalyticsTriggers> listCampaignPostLink = postAnalyticsTriggersRepo
                .findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId);

        if (!listCampaignPostLink.isEmpty()) {
            for (PostAnalyticsTriggers campaignPostLink : listCampaignPostLink) {
                Optional<SocialIdMaster> socialIdMaster = socialIdMasterRepo
                        .findById(campaignPostLink.getKey().getInfluencerId().toString());
                campaignPostLinkModal
                        .add(CampaignPostLinkModal.builder().influencerId(campaignPostLink.getKey().getInfluencerId())
                                .postName(campaignPostLink.getPostName()).postURL(campaignPostLink.getPostUrl())
                                .influencerHandle(campaignPostLink.getInfluencerHandle())
                                .influencerLink(getInfluencerProfileLink(socialIdMaster.get()))
                                .bidId(campaignPostLink.getKey().getBidId())
                                .postedOn(campaignPostLink.getPostDataTime()).build());
            }
        }
    }

    public void generateBrandCampaignInfluencerList1(UUID campaignId, UUID brandId,
                                                     List<BrandCampaignInfluencerList> brandBrandCampaignInfluencerList) {
        Set<BrandCampaignDetails> brandCampaignDetails = brandCampaignDetailsRepo
                .findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId);

        for (BrandCampaignDetails brandCampaignDetail : brandCampaignDetails) {
            Optional<SocialIdMaster> socialIdMaster = socialIdMasterRepo
                    .findById(brandCampaignDetail.getKey().getInfluencerId().toString());
            InfluencerDemographicsMapper demographicsMapper = reportsService
                    .getInfluencerDemographics(socialIdMaster.get(), null, null, false);

            brandBrandCampaignInfluencerList.add(BrandCampaignInfluencerList.builder()
                    .audienceCredibility(brandCampaignDetail.getInfluencerAudienceCredibilty())
                    .campaignId(campaignId)
                    .campaignName(brandCampaignDetail.getCampaignName())
                    .engagement(brandCampaignDetail.getInfluencerEngagement())
                    .followers(brandCampaignDetail.getInfluencerFollowers())
                    .influencerHandle(brandCampaignDetail.getInfluencerHandle())
                    .influencerId(brandCampaignDetail.getKey().getInfluencerId())
                    .influencerPricing(demographicsMapper.getPricing())
                    .platform(socialIdMaster.get().getPlatform().name().toLowerCase())
                    .bidCount(brandCampaignDetail.getBidCount())
                    .createdAt(brandCampaignDetail.getCreatedAt())
                    .influencerEmail(demographicsMapper.getEmail())
                    .influencerLink(getInfluencerProfileLink(socialIdMaster.get())).build());
        }
    }

    public void generateBrandCampaignInfluencerList(UUID campaignId, UUID brandId,
                                                    List<BrandCampaignInfluencerList> brandBrandCampaignInfluencerList) {
        List<BrandCampaignDetailsV1> brandCampaignDetail = brandCampaignDetailsRepositoryV1
                .findAllByBrandIdAndCampaignId(brandId.toString(), campaignId.toString());

        for (int i = 0; i < brandCampaignDetail.size(); i++) {
            for (int j = 0; j < brandCampaignDetail.get(i).getInfluencers().size(); j++) {
                Optional<SocialIdMaster> socialIdMaster = socialIdMasterRepo.findById(
                        brandCampaignDetail.get(i).getInfluencers().get(j).getCampaignInfluencerId().getInfluencerId());
                InfluencerDemographicsMapper demographicsMapper = reportsService
                        .getInfluencerDemographics(socialIdMaster.get(), null, null, false);
                brandBrandCampaignInfluencerList.add(BrandCampaignInfluencerList.builder()
                        .audienceCredibility(demographicsMapper.getCredibility())
                        .campaignId(campaignId)
                        .campaignName(brandCampaignDetail.get(i).getCampaignName())
                        .engagement(demographicsMapper.getEngagement())
                        .followers(demographicsMapper.getFollowers())
                        .influencerName(
                                demographicsMapper.getInfluencerName() != null ? demographicsMapper.getInfluencerName()
                                        : demographicsMapper.getInfluencerHandle())
                        .influencerHandle(demographicsMapper.getInfluencerHandle())
                        .influencerId(UUID.fromString(brandCampaignDetail.get(i).getInfluencers().get(j)
                                .getCampaignInfluencerId().getInfluencerId()))
                        .influencerPricing(demographicsMapper.getPricing())
                        .platform(socialIdMaster.get().getPlatform().name().toLowerCase())
                        .influencerEmail(demographicsMapper.getEmail())
                        .influencerLink(getInfluencerProfileLink(socialIdMaster.get())).build());
            }
        }
    }

    public SocialIdMaster getSocialId(Map<UUID, SocialIdMaster> influencerIdMap, BrandBidDetails bidDetails,
                                      UUID influencerId) {
        if (!influencerIdMap.keySet().contains(influencerId)) {
            Optional<SocialIdMaster> optionalSocialIdMaster = socialIdMasterRepo
                    .findById(bidDetails.getKey().getInfluencerId().toString());

            if (optionalSocialIdMaster.isPresent())
                influencerIdMap.put(influencerId, optionalSocialIdMaster.get());
            else
                influencerIdMap.put(influencerId, null);
        }
        SocialIdMaster socialIdMaster = influencerIdMap.get(influencerId);
        return socialIdMaster;
    }

    private String getInfluencerProfileLink(SocialIdMaster socialIdMaster) {
        if (socialIdMaster.getPlatform().equals(Platforms.INSTAGRAM))
            return "https://www.instagram.com/" + socialIdMaster.getInfluencerHandle().substring(1);
        if (socialIdMaster.getPlatform().equals(Platforms.YOUTUBE))
            return "https://www.youtube.com/channel/" + socialIdMaster.getInfluencerId();
        if (socialIdMaster.getPlatform().equals(Platforms.TIKTOK))
            return "https://www.tiktok.com/share/user/" + socialIdMaster.getInfluencerId();
        return null;
    }

    // public void generateInfluencerSnapshot(UUID campaignId, UUID brandId,
    // Set<InfluencerSnapshot> influencerSnapshots) {
    // Set<BrandBidDetails> brandBidDetails =
    // bidDetailsRepo.findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId);
    // Map<UUID, SocialIdMaster> influencerIdMap = new HashMap<>();
    // for (BrandBidDetails bidDetails : brandBidDetails) {
    // UUID influencerId = bidDetails.getKey().getInfluencerId();
    // if (ObjectUtils.isEmpty(getSocialId(influencerIdMap, bidDetails,
    // influencerId)))
    // continue;
    // String bidAmountName = bidDetails.getAffiliateType() != null ? "Affiliate
    // Fee" : "Bid Amount";
    // String bidAmount = bidDetails.getAffiliateType() != null &&
    // bidDetails.getAffiliateType().equals("%")
    // ? String.format("%.2f", bidDetails.getBidAmount()) + "%"
    // : "$" + String.format("%.2f", bidDetails.getBidAmount());
    //
    // InfluencerSnapshot influencerSnapshot =
    // InfluencerSnapshot.builder().bidAmount(bidAmount)
    // .bidDate(bidDetails.getCreatedAt()).influencerHandle(bidDetails.getInfluencerHandle())
    // .influencerName(bidDetails.getInfluencerHandle()).paymentType(bidDetails.getPaymentType())
    // .postDuration(StringUtils.isEmpty(bidDetails.getPostDuration()) ? "N/A"
    // : bidDetails.getPostDuration())
    // .postType(StringUtils.isEmpty(bidDetails.getPostType()) ? "N/A" :
    // bidDetails.getPostType())
    // .bidStatus(bidDetails.getBidStatus()).bidId(bidDetails.getKey().getBidId())
    // .influencerId(influencerId).affiliateType(bidDetails.getAffiliateType())
    // .bidAmountName(bidAmountName).isPostUrlPresent(bidDetails.getIsPostUrlPresent()).build();
    // influencerSnapshots.add(influencerSnapshot);
    // }
    // }

    public void generateInfluencerSnapshot(UUID campaignId, UUID brandId, Set<InfluencerSnapshot> influencerSnapshots) {
        Set<BrandBidDetails> brandBidDetails = bidDetailsRepo.findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId);
        Map<UUID, SocialIdMaster> influencerIdMap = new HashMap<>();
        for (BrandBidDetails bidDetails : brandBidDetails) {
            UUID influencerId = bidDetails.getKey().getInfluencerId();
            if (ObjectUtils.isEmpty(getSocialId(influencerIdMap, bidDetails, influencerId)))
                continue;
            InfluencerSnapshot influencerSnapshot = InfluencerSnapshot.builder()
                    .influencerHandle(bidDetails.getInfluencerHandle())
                    .influencerName(bidDetails.getInfluencerHandle())
                    .influencerId(influencerId).build();
            influencerSnapshots.add(influencerSnapshot);
        }
    }

    public ResponseEntity<String> exportCampaignCsvNew(UUID campaignId) {
        UUID brandId = ResourceServer.getUserId();
        List<BrandCampaignInfluencerList> brandCampaignInfluencerList = new LinkedList<>();
        Set<InfluencerSnapshot> influencerSnapshots = new HashSet<>();
        List<CampaignPostLinkModal> campaignPostLinkModals = new LinkedList<>();
        generateCampaignPostLinkDetails(campaignPostLinkModals, brandId, campaignId);
        generateBrandCampaignInfluencerList(campaignId, brandId, brandCampaignInfluencerList);
        generateInfluencerSnapshot(campaignId, brandId, influencerSnapshots);

        UUID csvName = UUID.randomUUID();
        File csvFile = new File(csvName.toString() + ".csv");
        try (FileWriter out = new FileWriter(csvFile);
             CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(HEADERS))) {
            for (BrandCampaignInfluencerList brandCampaignInfluencer : brandCampaignInfluencerList) {
                List<InfluencerSnapshot> filteredInfluencerSnapshots = influencerSnapshots.parallelStream()
                        .filter(e -> Objects.equals(e.getInfluencerHandle(),
                                brandCampaignInfluencer.getInfluencerHandle()))
                        .collect(Collectors.toList());
                if (filteredInfluencerSnapshots.isEmpty()) {
                    printer.printRecord(brandCampaignInfluencer.getInfluencerHandle(),
                            brandCampaignInfluencer.getCampaignName(),
                            brandCampaignInfluencer.getFollowers(),
                            brandCampaignInfluencer.getEngagement(),
                            brandCampaignInfluencer.getAudienceCredibility(),
                            brandCampaignInfluencer.getInfluencerName(),
                            brandCampaignInfluencer.getInfluencerPricing(),
                            brandCampaignInfluencer.getInfluencerEmail(),
                            brandCampaignInfluencer.getPlatform().toUpperCase());

                } else {
                    for (InfluencerSnapshot influencerSnapshot : filteredInfluencerSnapshots) {
                        printer.printRecord(influencerSnapshot.getInfluencerHandle(),
                                brandCampaignInfluencer.getCampaignName(),
                                brandCampaignInfluencer.getFollowers(),
                                brandCampaignInfluencer.getEngagement(),
                                String.format("%.2f", brandCampaignInfluencer.getAudienceCredibility() * 100) + "%",
                                influencerSnapshot.getInfluencerName(),
                                "$" + brandCampaignInfluencer.getInfluencerPricing(),
                                brandCampaignInfluencer.getInfluencerEmail(),
                                brandCampaignInfluencer.getPlatform().toUpperCase());
                    }
                }
            }
            printer.flush();
            campaignExportToCsvPathRepo.save(CampaignExportToCsvPath.builder().campaignId(campaignId)
                    .createdAt(new Date()).csvPath(csvFile.getAbsolutePath()).build());
            return ResponseEntity.ok(csvName.toString());
        } catch (IOException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    public ResponseEntity<byte[]> downloadCampaignCsvNew(UUID campaignId, UUID csvName) {
        Optional<BrandCampaignDetailsV1Entity> optionalBrandCampaignDetailsV1 = brandCampaignDetailsV1Repo
                .findByCampaignId(campaignId.toString());
        if (optionalBrandCampaignDetailsV1.isPresent()) {
            File file = new File(csvName + ".csv");
            if (file.exists()) {
                try {
                    byte[] csv = Files.readAllBytes(file.toPath());
                    Files.delete(file.toPath());
                    Optional<CampaignExportToCsvPath> optionalCampaignCsvPath = campaignExportToCsvPathRepo
                            .findById(campaignId);
                    if (optionalCampaignCsvPath.isPresent())
                        campaignExportToCsvPathRepo.delete(optionalCampaignCsvPath.get());

                    return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
                            .header(HttpHeaders.CONTENT_DISPOSITION,
                                    "attachment; filename=\"" + optionalBrandCampaignDetailsV1.get().getCampaignName()
                                            + ".csv\"")
                            .header(HttpHeaders.SET_COOKIE, "fileDownload=true; Path=/").body(csv);
                } catch (IOException e) {
                    e.getStackTrace();
                }
            }
        }
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<byte[]> downloadNewTemplate() {
        File file = new File("Upload_Influencer_Template" + ".csv");

        if (file.exists()) {
            try {
                byte[] csv = Files.readAllBytes(file.toPath());

                return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .header(HttpHeaders.CONTENT_DISPOSITION,
                                "attachment; filename=\"" + "Upload_Influencer_Template" + ".csv\"")
                        .header(HttpHeaders.SET_COOKIE, "fileDownload=true; Path=/").body(csv);
            } catch (IOException e) {
                e.getStackTrace();
            }
        }
        return ResponseEntity.notFound().build();
    }

    public List<CampaignInfluencerOfferV1Entity> updateInfluencersoffers(InfluObj influObj) {
        Optional<CampaignInfluencerV1Entity> campaignInfluencer = campaignInfluencerRepo
                .findByCampaignInfluencerId(new CampaignInfluencerKey(influObj.getCampaignId(),
                        influObj.getInfluencerId()));
        List<String> influencerOfferList = new LinkedList<>();
        List<CampaignInfluencerOfferV1Entity> resultInfluList = new LinkedList<>();
        if (campaignInfluencer.isPresent()) {
            CampaignInfluencerV1Entity campaignInfluencerV1Entity = campaignInfluencer.get();
            if (!campaignInfluencerV1Entity.getInfluencerOffers().isEmpty()) {
                for (int i = 0; i < campaignInfluencerV1Entity.getInfluencerOffers().size(); i++) {
                    int count = 0;
                    ArrayList<CampaignInfluencerOfferV1Entity> campaignInfluencerOffer = new ArrayList<>(campaignInfluencerV1Entity.getInfluencerOffers());
                    for (int j = 0; j < influObj.getOffers().size(); j++) {
                        if (Objects.equals(
                                campaignInfluencerOffer.get(i).getCampaignOfferInfluencerId()
                                        .getOfferType().toString(),
                                influObj.getOffers().get(j).getOfferType().toString())) {
                            count++;
                            InfluOfferObj influOfferObj = influObj.getOffers().get(j);
                            CampaignInfluencerOfferV1Entity campaignInfluencerOfferV1Entity = saveInfluencerOffers(
                                    influOfferObj,
                                    campaignInfluencerV1Entity.getCampaignInfluencerId().getCampaignId());
                            resultInfluList.add(campaignInfluencerOfferV1Entity);
                            influencerOfferList.add(influOfferObj.getOfferType().toString());
                        } else {
                            continue;
                        }
                    }
                    if (count == 0) {
                        campaignInfOfferV1Repo.deleteByCampaignIdAndInfluencerIdAndOfferType(influObj.getCampaignId(),
                                campaignInfluencerOffer.get(i).getCampaignOfferInfluencerId()
                                        .getInfluencerId(),
                                campaignInfluencerOffer.get(i).getCampaignOfferInfluencerId()
                                        .getOfferType());

                        // List<CampaignInfluencerOfferV1Entity> campaignInfluencerOfferV1Entities =
                        // campaignInfOfferV1Repo.deleteByCampaignOfferInfluencerId_CampaignIdAndCampaignOfferInfluencerId_InfluencerIdAndCampaignOfferInfluencerId_OfferType(influObj.getCampaignId(),
                        // campaignInfluencerV1Entity.getInfluencerOffers().get(i).getCampaignOfferInfluencerId().getInfluencerId(),
                        // campaignInfluencerV1Entity.getInfluencerOffers().get(i).getCampaignOfferInfluencerId().getOfferType());
                    }
                }
            }
            List<CampaignInfluencerOfferV1Entity> influObjRel = influObj.getOffers().stream().filter(influOfferObj -> {

                if (influencerOfferList.isEmpty()) {
                    return true;
                } else if ((!influencerOfferList.contains(influOfferObj.getOfferType().toString()))) {
                    return true;
                } else
                    return false;

            }).map(influOfferObj -> saveInfluencerOffers(influOfferObj,
                    campaignInfluencerV1Entity.getCampaignInfluencerId().getCampaignId())).collect(Collectors.toList());
            resultInfluList.addAll(influObjRel);
            return resultInfluList;
        }
        return resultInfluList;
    }

    public CampaignEntityV1 saveCampaignOffer(CampaignEntityV1 campaignEntityV1) {
        List<CampaignOffersV1> campaignOffersV1List = campaignEntityServiceV1
                .getCampaignOffersByCampaignId(campaignEntityV1.getCampaignId());
        List<OfferTypeEntityV1> offerList = new LinkedList<>();
        if (!campaignOffersV1List.isEmpty()) {
            for (int i = 0; i < campaignOffersV1List.size(); i++) {
                int count = 0;
                for (int j = 0; j < campaignEntityV1.getOffers().size(); j++) {
                    if (Objects.equals(campaignOffersV1List.get(i).getCampaignOfferId().getOfferType(),
                            campaignEntityV1.getOffers().get(j).getOfferType())) {
                        count++;
                        CampaignOffersEntityV1 campaignOffersEntity = campaignEntityV1.getOffers().get(j);
                        offerList.add(campaignOffersEntity.getOfferType());
                        saveOffers(campaignOffersEntity, campaignEntityV1.getCampaignId());
                    } else {
                        continue;
                    }
                }
                if (count == 0) {
                    campaignEntityServiceV1
                            .deleteOffersByCampaignOfferId(new CampaignOfferId(campaignEntityV1.getCampaignId(),
                                    campaignOffersV1List.get(i).getCampaignOfferId().getOfferType()));
                }
            }
        }

        campaignEntityV1.getOffers().stream().forEach(campaignOffersEntityV1 -> {
            if (!offerList.contains(campaignOffersEntityV1.getOfferType())) {
                saveOffers(campaignOffersEntityV1, campaignEntityV1.getCampaignId());
            }
        });
        return campaignEntityV1;
    }

    public CampaignInfluencerOfferV1Entity saveInfluencerOffers(InfluOfferObj influOfferObj, String campaignId) {
        CampaignInfluencerOfferV1Entity campaignInfluencerOfferV1Entity = new CampaignInfluencerOfferV1Entity();
        if (influOfferObj.getOfferType().equals(OfferTypeEntityV1.PAYMENT)) {
            campaignInfluencerOfferV1Entity.setCampaignOfferInfluencerId(new CampaignInfOfferKey(campaignId,
                    influOfferObj.getInfluencerId(), influOfferObj.getOfferType().toString()));
            campaignInfluencerOfferV1Entity.setPaymentOffer(influOfferObj.getPaymentOffer());
        } else if (influOfferObj.getOfferType().equals(OfferTypeEntityV1.COMMISSION)) {
            campaignInfluencerOfferV1Entity.setCampaignOfferInfluencerId(new CampaignInfOfferKey(campaignId,
                    influOfferObj.getInfluencerId(), influOfferObj.getOfferType().toString()));
            campaignInfluencerOfferV1Entity.setCommissionValue(influOfferObj.getCommissionValue());
            campaignInfluencerOfferV1Entity.setCommissionValueTypeId(influOfferObj.getCommissionValueTypeId());
            campaignInfluencerOfferV1Entity.setCommissionAffiliateTypeId(influOfferObj.getCommissionAffiliateTypeId());
            campaignInfluencerOfferV1Entity
                    .setCommissionAffiliateCustomName(influOfferObj.getCommissionAffiliateCustomName());
            campaignInfluencerOfferV1Entity
                    .setCommissionAffiliateDetails(influOfferObj.getCommissionAffiliateDetails());
        } else if (influOfferObj.getOfferType().equals(OfferTypeEntityV1.PRODUCT)) {
            campaignInfluencerOfferV1Entity.setCampaignOfferInfluencerId(new CampaignInfOfferKey(campaignId,
                    influOfferObj.getInfluencerId(), influOfferObj.getOfferType().toString()));
        }
        return campaignInfOfferV1Repo.save(campaignInfluencerOfferV1Entity);
    }

    public ResponseObj sendProposal(WorkflowReqObj workflowReqObj) {
        try {
            if (workflowReqObj.getCampaignId().equals("") || workflowReqObj.getCampaignId() == null ||
                    workflowReqObj.getInfluencerId().equals("") || workflowReqObj.getInfluencerId() == null) {
                return new ResponseObj("651", null, Collections.singletonMap("message", "field validation error"));
            }
            Optional<CampaignInfluencerV1Entity> campaignInfluencerDetails = campaignInfluencerRepo
                    .findByCampaignInfluencerId(new CampaignInfluencerKey(workflowReqObj.getCampaignId(),
                            workflowReqObj.getInfluencerId()));
            BrandInfluObj brandInfluObj = new BrandInfluObj();
            if (!campaignInfluencerDetails.isPresent()) {
                return new ResponseObj("654", null,
                        Collections.singletonMap("message", "CampaignId and influencerId is invalid"));
            } else {
                CampaignInfluencerV1Entity campaignInfluencerEntity = campaignInfluencerDetails.get();
                if (Objects.equals(campaignInfluencerEntity.getProposalStatus().toString(), "INVITE_NOT_SENT")) {
                    brandInfluObj.setCampaignId(campaignInfluencerEntity.getCampaignInfluencerId().getCampaignId());
                    brandInfluObj.setInfluencerId(campaignInfluencerEntity.getCampaignInfluencerId().getInfluencerId());
                    brandInfluObj.setInfluencerHandle(campaignInfluencerEntity.getInfluencerHandle());
                    brandInfluObj.setInfluencerFirstName(campaignInfluencerEntity.getInfluencerFirstName());
                    brandInfluObj.setInfluencerMail(campaignInfluencerEntity.getInfluencerEmail());
                    Optional<UserAccounts> userAccounts = userAccountsRepo
                            .findByUserId(ResourceServer.getUserId().toString());
                    if (userAccounts.isPresent()) {
                        UserAccounts userAccountsEntity = userAccounts.get();
                        brandInfluObj.setBrandName(userAccountsEntity.getBrandName());
                        brandInfluObj.setBrandEmail(userAccountsEntity.getEmail());
                        brandInfluObj.setBrandIndustry(userAccountsEntity.getIndustry());
                        brandInfluObj.setPostDuration("2H");
                        mailService.sendInviteMailToInfluencer(brandInfluObj);
                        campaignInfluencerEntity.setProposalStatus(String.valueOf(ProposalStatus.INVITE_SENT));
                        return new ResponseObj("600", campaignInfluencerRepo.save(campaignInfluencerEntity));

                    } else {
                        return new ResponseObj("654", null, Collections.singletonMap("message", "Invalid BrandId"));

                    }
                } else {
                    return new ResponseObj("655", null,
                            Collections.singletonMap("message",
                                    "Influencers in " + campaignInfluencerEntity.getProposalStatus()));

                }

            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.toString());
            LOG.log(Level.SEVERE, e.getMessage());
            return new ResponseObj("500", null, Collections.singletonMap("message", e.getMessage()));
        }
    }

    public ResponseObj setBrandApproval(CampaignInfluencerV1Entity campaignInfluencerEntity) {
        boolean productMatch = campaignInfluencerEntity.getInfluencerOffers().stream().anyMatch(
                offers -> Objects.equals(offers.getCampaignOfferInfluencerId().getOfferType().toString(), "PRODUCT"));
        if (productMatch) {
            campaignInfluencerEntity.setProposalStatus(PAID);
            return new ResponseObj("600", campaignInfluencerRepo.save(campaignInfluencerEntity));
        } else {
            campaignInfluencerEntity.setProposalStatus(NOT_PAID);
            return new ResponseObj("600", campaignInfluencerRepo.save(campaignInfluencerEntity));
        }

    }

    public boolean validateGivenString(String obj) {
        if (obj == null || Objects.equals(obj, "")) {
            return true;
        } else {
            return false;
        }
    }

    public ResponseObj brandApprovals(WorkflowReqObj workflowReqObj) {
        LOG.log(Level.INFO, workflowReqObj.toString());
        try {
            if (workflowReqObj.getCampaignId().equals("") || workflowReqObj.getCampaignId() == null ||
                    workflowReqObj.getInfluencerId().equals("") || workflowReqObj.getInfluencerId() == null ||
                    validateGivenString(workflowReqObj.getWorkflowStatus())
                    || validateGivenString(workflowReqObj.getStatus())) {
                return new ResponseObj("651", null, Collections.singletonMap("message", "field validation error"));
            }

            String campaignStatus = brandCampaignDetailsRepositoryV1.findCampaignStatusByCampaignId(workflowReqObj.getCampaignId());
            if (!Objects.equals(campaignStatus, ACTIVE)) {
                return new ResponseObj("655", null,
                        Collections.singletonMap("message", "Campaign status is  " + campaignStatus + " ,So can't update"));
            }
            Optional<CampaignInfluencerV1Entity> campaignInfluencerDetails = campaignInfluencerRepo
                    .findByCampaignInfluencerId(new CampaignInfluencerKey(workflowReqObj.getCampaignId(),
                            workflowReqObj.getInfluencerId()));

            if (!campaignInfluencerDetails.isPresent()) {
                return new ResponseObj("654", null,
                        Collections.singletonMap("message", "CampaignId and influencerId is invalid"));
            } else {
                CampaignInfluencerV1Entity campaignInfluencerEntity = campaignInfluencerDetails.get();

                switch (workflowReqObj.getWorkflowStatus()) {
                    case "BRAND_REVIEW":
                        if (Objects.equals(campaignInfluencerEntity.getProposalStatus(), INFLUENCER_ACCEPTED)) {
                            if (Objects.equals(workflowReqObj.getStatus(), ACCEPT)) {
                                return setBrandApproval(campaignInfluencerEntity);
                            } else {
                                campaignInfluencerEntity.setProposalStatus(BRAND_REJECTED);
                                return new ResponseObj("600", campaignInfluencerRepo.save(campaignInfluencerEntity));
                            }
                        } else if (Objects.equals(campaignInfluencerEntity.getProposalStatus(), BRAND_REJECTED)) {
                            if (Objects.equals(workflowReqObj.getStatus(), ACCEPT)) {
                                return setBrandApproval(campaignInfluencerEntity);
                            } else {
                                return new ResponseObj("655", null,
                                        Collections.singletonMap("message",
                                                "Influencer status already"
                                                        + campaignInfluencerEntity.getProposalStatus() + " " +
                                                        " so can't update"));
                            }
                        } else {
                            return new ResponseObj("655", null,
                                    Collections.singletonMap("message",
                                            "Influencer status already" + campaignInfluencerEntity.getProposalStatus()
                                                    + " " +
                                                    "so can't update"));
                        }
                    case "MAKE_PAYMENTS":
                        if (Objects.equals(campaignInfluencerEntity.getProposalStatus(), NOT_PAID)) {
                            if (Objects.equals(workflowReqObj.getStatus(), ACCEPT)) {

                                campaignInfluencerEntity.setProposalStatus(PAID);
                                return new ResponseObj("600", campaignInfluencerRepo.save(campaignInfluencerEntity));
                            } else {
                                return new ResponseObj("655", null,
                                        Collections.singletonMap("message",
                                                "Influencer status already"
                                                        + campaignInfluencerEntity.getProposalStatus() + " " +
                                                        "so can't update"));
                            }
                        } else {
                            return new ResponseObj("655", null,
                                    Collections.singletonMap("message",
                                            "Influencer status already" + campaignInfluencerEntity.getProposalStatus()
                                                    + " " +
                                                    "so can't update"));
                        }
                    case "FULFILL_PRODUCTS":
                        if (Objects.equals(campaignInfluencerEntity.getProposalStatus(), PAID)) {
                            if (validateGivenString(workflowReqObj.getTrackingId())) {
                                return new ResponseObj("651", null,
                                        Collections.singletonMap("message", "tracking id field is invalid"));
                            }
                            if (Objects.equals(workflowReqObj.getStatus(), ACCEPT)) {
                                campaignInfluencerEntity.setTrackingId(workflowReqObj.getTrackingId());
                                campaignInfluencerEntity.setProposalStatus(PRODUCT_FULFILLED);
                                return new ResponseObj("600", campaignInfluencerRepo.save(campaignInfluencerEntity));
                            } else {
                                campaignInfluencerEntity.setTrackingId(workflowReqObj.getTrackingId());
                                return new ResponseObj("601", campaignInfluencerRepo.save(campaignInfluencerEntity));
                            }
                        } else {
                            return new ResponseObj("655", null,
                                    Collections.singletonMap("message",
                                            "Influencer status already" + campaignInfluencerEntity.getProposalStatus()
                                                    + " " +
                                                    "so can't update"));
                        }
                    case "STATUS_UPDATE":
                        if (Objects.equals(campaignInfluencerEntity.getProposalStatus(), "PRODUCT_FULFILLED")) {
                            SavePostLink postLink = new SavePostLink();
                            postLink.setInfluencerId(UUID.fromString(workflowReqObj.getInfluencerId()));
                            postLink.setPostURL(workflowReqObj.getPostUrl());

                            if (Objects.equals(workflowReqObj.getStatus(), ACCEPT)) {
                                if (validateGivenString(workflowReqObj.getPostUrl())) {
                                    return new ResponseObj("651", null,
                                            Collections.singletonMap("message", "post url field is invalid"));
                                }
                                ResponseEntity<Object> objectResponseEntity = dashboardService.validatePostLinkNew(postLink);
                                if (objectResponseEntity.getStatusCode().value() != 200) {
                                    return new ResponseObj("651", null,
                                            Collections.singletonMap("message", "post url field is invalid"));
                                }
                                campaignInfluencerEntity.setPostUrl(workflowReqObj.getPostUrl());
                                campaignInfluencerEntity.setProposalStatus(TRACK_POST);
                                return new ResponseObj("600", campaignInfluencerRepo.save(campaignInfluencerEntity));
                            } else {
                                campaignInfluencerEntity.setPostUrl(workflowReqObj.getPostUrl());
                                return new ResponseObj("601", campaignInfluencerRepo.save(campaignInfluencerEntity));

                            }

                        } else {
                            return new ResponseObj("655", null,
                                    Collections.singletonMap("message",
                                            "Influencer status already" + campaignInfluencerEntity.getProposalStatus()
                                                    + " " +
                                                    "so can't update"));
                        }
                    default:
                        return new ResponseObj("655", null,
                                Collections.singletonMap("message", "Invalid workflow status"));

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.log(Level.SEVERE, e.getMessage());
            LOG.log(Level.SEVERE, e.toString());
            return new ResponseObj("654",
                    Collections.singletonMap("message", "Something wrong please try again later"));
        }
    }

    public ResponseObj deleteTermsCondition(String campaignId, long termId) {
        TermsConditionsV1 userTerm = campaignEntityServiceV1.getTermsConditions(termId, TermsTypeEntity.CUSTOM);
        if (userTerm != null) {
            TermsConditionsV1 updatedTerm = deleteFileAndSetFilePathAsNull(userTerm.getTermsConditionFilePath(),
                    userTerm);
            if (updatedTerm != null) {
                return new ResponseObj("600", updatedTerm, null);
            } else {
                return new ResponseObj("654", null, Collections.singletonMap("message", "something wrong try again later"));
            }
        } else {
            return new ResponseObj("655", null, Collections.singletonMap("message", "invalid termsId"));
        }

    }

    public TermsConditionsV1 deleteFileAndSetFilePathAsNull(String filePath, TermsConditionsV1 termsConditionsV1) {
        // String s3FileLocation = storageService.getFileName(campaignId);
        storageService.deleteFile(filePath);
        termsConditionsV1.setFileName(null);
        termsConditionsV1.setTermsConditionFilePath(null);
        return campaignEntityServiceV1.saveTermsCondition(termsConditionsV1);
    }

    public ResponseObj editCampaignName(String campaignId, String campaignName) {
        LOG.log(Level.INFO, "inside the edit campaign name campaignId=" + campaignId + " campaignName=" + campaignName);
        try {
            if (validateGivenString(campaignId) || validateGivenString(campaignName)) {
                return new ResponseObj("651", null, Collections.singletonMap("message", "field validation error"));
            } else {
                BrandCampaignDetailsV1 brandCampaign = campaignEntityServiceV1.getCampaignById(campaignId);
                if (Objects.isNull(brandCampaign)) {
                    return new ResponseObj("651", null, Collections.singletonMap("message", "No campaign Exists"));
                } else {
                    if (Objects.equals(brandCampaign.getCampaignStatus().toString(), ACTIVE)
                            || Objects.equals(brandCampaign.getCampaignStatus().toString(), CLOSED)
                            || Objects.equals(brandCampaign.getCampaignStatus().toString(), INACTIVE)) {
                        return new ResponseObj("655", null,
                                Collections.singletonMap("message", "Campaign status is "
                                        + brandCampaign.getCampaignStatus() + " so can't edit campaign name"));
                    } else {
                        BrandCampaignDetailsV1 existCampaign = campaignEntityServiceV1
                                .getCampaignByCampaignNameAndBrandIdAndNotCampaignId(campaignName,
                                        ResourceServer.getUserId().toString(), campaignId);
                        if (Objects.nonNull(existCampaign)) {
                            return new ResponseObj("655", null,
                                    Collections.singletonMap("message", "Campaign name already " +
                                            "exists with that brand"));
                        } else {
                            brandCampaign.setCampaignName(campaignName);
                            brandCampaign.setModifiedAt(LocalDateTime.now());
                            BrandCampaignDetailsV1 brandCampaignRel = brandCampaignDetailsRepositoryV1
                                    .save(brandCampaign);
                            if (brandCampaignRel == null) {
                                LOG.log(Level.SEVERE, "Something wrong while saving brandCampaignDetails");
                                return new ResponseObj("655", null,
                                        Collections.singletonMap("message", "something wrong try again later"));
                            } else {
                                return new ResponseObj("600", null, null);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.toString());
            LOG.log(Level.SEVERE, e.getMessage());
            return new ResponseObj("655", null,
                    Collections.singletonMap("message", e.getMessage()));
        }
    }

    public ResponseObj validateCampaignName(String campaignName, String campaignId) {
        if (Objects.isNull(campaignId)) {
            List<BrandCampaignDetailsV1> existBrandCampaign = campaignEntityServiceV1
                    .getCampaignByCampaignNameAndBrandId(campaignName, ResourceServer.getUserId().toString());
            if (existBrandCampaign.isEmpty()) {
                return new ResponseObj("600", null, null);
            } else {
                return new ResponseObj("655", null,
                        Collections.singletonMap("message", "Campaign name already exists with" +
                                " that brand"));

            }
        } else {
            BrandCampaignDetailsV1 existCampaign = campaignEntityServiceV1
                    .getCampaignByCampaignNameAndBrandIdAndNotCampaignId(campaignName,
                            ResourceServer.getUserId().toString(), campaignId);
            if (Objects.nonNull(existCampaign)) {
                return new ResponseObj("655", null,
                        Collections.singletonMap("message", "Campaign name already " +
                                "exists with that brand"));
            } else {
                return new ResponseObj("600", null, null);
            }

        }

    }

}
