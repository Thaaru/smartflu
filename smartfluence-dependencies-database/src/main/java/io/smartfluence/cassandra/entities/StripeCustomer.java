package io.smartfluence.cassandra.entities;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.StripeCustomerKey;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table("stripe_customer")
public class StripeCustomer implements Copyable {

	private static final long serialVersionUID = 5974860996741959910L;

	@PrimaryKey
	private StripeCustomerKey key;

	@Column("customer_id")
	private String customerId;

	@Column("created_at")
	private Date createdAt;
}