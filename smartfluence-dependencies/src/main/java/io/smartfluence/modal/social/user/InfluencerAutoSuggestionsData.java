package io.smartfluence.modal.social.user;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InfluencerAutoSuggestionsData implements Serializable {

	private static final long serialVersionUID = 3641745196655438112L;

	@JsonProperty("user_id")
	private String userId;

	@JsonProperty("username")
	private String userName;

	@JsonProperty("fullname")
	private String fullName;

	@JsonProperty("picture")
	private String profilePicture;

	@JsonProperty("followers")
	private Long followersCount;

	@JsonProperty("is_verified")
	private boolean isVerified;
}