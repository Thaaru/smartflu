package io.smartfluence.util;

import org.springframework.stereotype.Component;

@Component
public class EmailUtil {

	public Boolean emailValidation(String email) {

		/*if ((email.contains("gmail") || email.contains("yahoo") || email.contains("outlook") || email.contains("icloud")
				|| email.contains("hotmail") || email.contains("me.com") || email.contains("mail.ru")))
			return false;*/

		return (email.contains("@gmail.") || email.contains("@yahoo.") || email.contains("@outlook.") || email.contains("@icloud.")
				|| email.contains("@hotmail.") || email.contains("@me.") || email.contains("@mail.ru")) == false;
	}
}