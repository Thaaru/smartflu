package io.smartfluence.cassandra.entities;

import java.io.Serializable;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.SubscriptionMasterKey;
import io.smartfluence.constants.ValidityType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("subscription_master")
public class SubscriptionMaster implements Serializable {

	private static final long serialVersionUID = -1940731641644407271L;

	@PrimaryKey
	private SubscriptionMasterKey key;

	@Column("subscription_name")
	private String subscriptionName;

	@Column("credits")
	private Integer credits;

	@Column("validity")
	private Integer validity;

	@Column("validity_type")
	private ValidityType validityType;

	@Column("active")
	private boolean active;

	@Column("reset_credit")
	private boolean resetCredit;

	@Column("renewable")
	private boolean renewable;
}