package io.smartfluence.brand.management.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "brand_campaign_details_new")
public class BrandCampaignDetailsNew implements Serializable {

    private static final long serialVersionUID = -5186024765585348307L;
    @Id
    @Column(name = "campaign_id")
    private String campaignId;
    @Column(name = "brand_id")
    private String brandId;
    @Column(name = "brand_description")
    private String brandDescription;
    @Column(name = "campaign_name")
    private String campaignName;
    @Column(name = "campaign_description")
    private String campaignDescription;
    @Column(name = "campaign_status")
    private String campaignStatus;
    @Column(name = "deleted_status")
    private String deletedStatus;
    @Column(name = "offer_type")

    private String offerType;
    @Column(name="is_new")

    private String isNew;
    @Column(name = "restrictions")
    private String restrictions;
    @Column(name = "currency")
    private String currency;
    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;
    @Column(name="hashtags")
    private String hashTags;
    @Column(name="max_product_count")
    private int maxProductCount;
    @Column(name="payment_offer")
    private Double paymentOffer;
    @Column(name="commission_affiliate")
    private Double commissionAffiliate;
    @Column(name="commission_affiliate_type")
    private String commissionAffiliateType;
    @Column(name="commission_affiliate_details")
    private String commissionAffiliateDetails;

    @Column(name="commission_affiliate_custom_name")
    private String commissionAffiliateCustomName;

    @Column(name="terms_id")
    private long termsId;
    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name="influencer_count")
    private int influencerCount;



}