package io.smartfluence.webapp.controller.rest.admin;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.modal.validation.admin.ReportRequestModal;
import io.smartfluence.webapp.proxy.AdminManagementServiceProxy;

@RestController
@RequestMapping("admin")
public class AdminManagementController {

	@Autowired
	private AdminManagementServiceProxy adminManagementServiceProxy;

	@PostMapping("approve-brand")
	public ResponseEntity<Object> approveBrand(@RequestParam("brandId") String brandId, @RequestParam SubscriptionType subscriptionType,
			@RequestParam UUID subscriptionId) {
		return adminManagementServiceProxy.approveBrand(brandId, subscriptionType, subscriptionId);
	}

	@PostMapping("decline-brand")
	public ResponseEntity<Object> declineBrand(@RequestParam("brandId") String brandId) {
		return adminManagementServiceProxy.declineBrand(brandId);
	}

	@PostMapping("send-invitation")
	public ResponseEntity<Object> sendInvitation(@RequestParam("selectedUserId") String[] selectedUserId) {
		return adminManagementServiceProxy.sendInvitation(selectedUserId);
	}

	@PostMapping("update-db")
	public ResponseEntity<Object> updateDb() {
		return adminManagementServiceProxy.updateDb();
	}

	@GetMapping("check-db-update-status")
	public ResponseEntity<Object> checkDbUpdateStatus() {
		return adminManagementServiceProxy.checkDbUpdateStatus();
	}

	@GetMapping("export-data")
	public ResponseEntity<String> exportAdminData() {
		return adminManagementServiceProxy.exportAdminData();
	}

	@GetMapping("export-report-data")
	public ResponseEntity<Object> exportReportData(@Valid @ModelAttribute ReportRequestModal reportRequestModal, BindingResult bindingResult) {
		return adminManagementServiceProxy.exportReportData(reportRequestModal.getReportType(), reportRequestModal.getFromDate(),
				reportRequestModal.getToDate());
	}

	@GetMapping("download/{flag}/report")
	public ResponseEntity<byte[]> downloadReport(@PathVariable String flag) {
		return adminManagementServiceProxy.downloadReport(flag);
	}
}