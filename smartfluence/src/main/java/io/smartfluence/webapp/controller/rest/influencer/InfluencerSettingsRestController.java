package io.smartfluence.webapp.controller.rest.influencer;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("influencer")
public class InfluencerSettingsRestController {
//
//	@Autowired
//	private InfluencerManagementServiceProxy influencerManagementServiceProxy;
//
//	@PostMapping("social-connect")
//	public ResponseEntity<Object> saveInstagramHandle(@Validated @ModelAttribute SocialConnectModel socialConnectModel,
//			BindingResult bindingResult, RedirectAttributes attributes) {
//		if (!bindingResult.hasFieldErrors())
//			return influencerManagementServiceProxy.saveInstagramHandle(socialConnectModel.getInstagramHandle());
//
//		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
//
//	}
//
//	@PostMapping("profile")
//	public ResponseEntity<Object> saveProfile(InfluencerProfileModal influencerProfileModal, BindingResult bindingResult, RedirectAttributes attributes) {
//
//		if (!bindingResult.hasFieldErrors())
//			return influencerManagementServiceProxy.saveProfile(influencerProfileModal.getPaypalEmail(), influencerProfileModal.getAddress1(),
//					influencerProfileModal.getAddress2(), influencerProfileModal.getCity(), influencerProfileModal.getCountry(),
//					influencerProfileModal.getDob(), influencerProfileModal.getGender(), influencerProfileModal.getState());
//
//		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
//
//	}
//
//	@GetMapping("profile")
//	public ResponseEntity<Object> getProfileDetails() {
//		return influencerManagementServiceProxy.getProfileDetails();
//	}
//
//	@PostMapping("influencer-preference")
//	public ResponseEntity<Object> updateInfluencerPreference(@Validated @ModelAttribute InfluencerPreferenceModal influencerPreferenceModal,
//			BindingResult bindingResult, RedirectAttributes attributes) {
//
//		if (!bindingResult.hasFieldErrors())
//			return influencerManagementServiceProxy.updateInfluencerPreference(influencerPreferenceModal.getDescription(),
//					influencerPreferenceModal.getIndustry(), influencerPreferenceModal.getLocation(), influencerPreferenceModal.getInstaStory2Hr(),
//					influencerPreferenceModal.getInstaStory24Hr(), influencerPreferenceModal.getInstaPost24Hr(),
//					influencerPreferenceModal.getInstaPost1W(), influencerPreferenceModal.getInstaPostPerm());
//
//		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
//	}
//
//	@GetMapping("influencer-preference")
//	public ResponseEntity<Object> getInfluencerPreference() {
//		return influencerManagementServiceProxy.getInfluencerPreference();
//	}
}