package io.smartfluence.cassandra.repository;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.InfluencerProfileDetails;

@Repository
public interface InfluencerProfileDetailsRepo extends CassandraRepository<InfluencerProfileDetails, UUID> {

}