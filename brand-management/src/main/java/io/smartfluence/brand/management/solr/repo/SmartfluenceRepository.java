package io.smartfluence.brand.management.solr.repo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.brand.management.solr.entity.SmartfluenceRaking;

@Repository
public interface SmartfluenceRepository extends SolrCrudRepository<SmartfluenceRaking, String> {

	@Query(value = "*:*", filters = { "comparison_profile:?0", "?1:[0.5 TO *]" })
	List<SmartfluenceRaking> findByCustomFilter(String influencerHandle, String industry, Pageable page);
}