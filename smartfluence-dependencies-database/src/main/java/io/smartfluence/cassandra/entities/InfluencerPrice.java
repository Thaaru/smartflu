package io.smartfluence.cassandra.entities;

import java.io.Serializable;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.InfluencerPriceKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("influencer_pricedetails")
public class InfluencerPrice implements Serializable {

	private static final long serialVersionUID = -4937231924188163703L;

	@PrimaryKey
	private InfluencerPriceKey key;

	@Column("price")
	private Double price;
}