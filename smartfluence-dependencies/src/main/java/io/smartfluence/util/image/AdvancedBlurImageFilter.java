package io.smartfluence.util.image;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

class AdvancedBlurImageFilter implements ImageFilter {

	@Override
	public BufferedImage processImageBuffer(BufferedImage bufferedImage) {
		float[] matrix = new float[25];
		for (int i = 0; i < 25; i++)
			matrix[i] = 1.0f / 25.0f;
		BufferedImageOp blurFilter = new ConvolveOp(new Kernel(5, 5, matrix), ConvolveOp.EDGE_NO_OP, null);
		return blurFilter.filter(bufferedImage, null);
	}
}