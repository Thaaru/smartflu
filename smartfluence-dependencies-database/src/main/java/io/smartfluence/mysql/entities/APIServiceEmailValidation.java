package io.smartfluence.mysql.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.smartfluence.mysql.entities.primary.APIServiceEmailValidationKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "api_service_email_validation")
public class APIServiceEmailValidation implements Serializable {

	private static final long serialVersionUID = -1653158640403977196L;

	@EmbeddedId
	private APIServiceEmailValidationKey key;

	@Column(name = "daily_count")
	private int dailyCount;

	@Column(name = "total_count")
	private int totalCount;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "modified_at")
	private Date modifiedAt;
}