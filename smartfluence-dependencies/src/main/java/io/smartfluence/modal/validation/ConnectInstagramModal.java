package io.smartfluence.modal.validation;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ConnectInstagramModal {

	@NotBlank(message = "Please enter your instagram handle")
	@Pattern(regexp = "@([A-Za-z0-9_])+(\\.?([A-Za-z0-9_]))*", message = "Please enter your instagram handle")
	private String instagramHandle;
}