package io.smartfluence.cassandra.entities.instagram;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.instagram.primary.InstagramIndustryKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("instagram_industry_demographic")
public class InstagramIndustryDemographic {

	@PrimaryKey
	private InstagramIndustryKey key;

	@Column("value")
	private Double value;

	public Double getValue() {
		return value == null ? 0.0 : value;
	}
}