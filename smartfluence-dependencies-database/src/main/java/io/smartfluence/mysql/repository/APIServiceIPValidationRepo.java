package io.smartfluence.mysql.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.mysql.entities.APIServiceIPValidation;
import io.smartfluence.mysql.entities.primary.APIServiceIPValidationKey;

@Repository
public interface APIServiceIPValidationRepo extends CrudRepository<APIServiceIPValidation, APIServiceIPValidationKey> {

	Optional<APIServiceIPValidation> findByKeyIpAddressAndKeyService(String email, String service);

	@Modifying
	@Transactional
	@Query("update APIServiceIPValidation u set u.dailyCount = 0 where u.dailyCount != 0")
	void updateDailyCount();
}