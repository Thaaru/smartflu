package io.smartfluence.brand.management.repositoryv1;


import io.smartfluence.brand.management.entityv1.TermsConditionsV1;
import io.smartfluence.modal.brand.campaignv1.IsActive;
import io.smartfluence.modal.brand.campaignv1.TermsTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TermsConditionRepositoryV1 extends JpaRepository<TermsConditionsV1,Long> {
    List<TermsConditionsV1> findByTermIdOrType(long termId, TermsTypeEntity type);
    List<TermsConditionsV1> findByTypeAndIsActive(TermsTypeEntity type, IsActive isActive);
    TermsConditionsV1 findByTermId(long termId);
    TermsConditionsV1 findByTermIdAndType(long termId, TermsTypeEntity type);
    void deleteByTermId(long termId);
}
