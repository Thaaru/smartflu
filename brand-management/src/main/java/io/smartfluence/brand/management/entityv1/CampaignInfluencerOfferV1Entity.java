package io.smartfluence.brand.management.entityv1;

import io.smartfluence.brand.management.entity.CampaignInfluencerKey;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_influencer_offers_v1")
public class CampaignInfluencerOfferV1Entity {
    @EmbeddedId
    CampaignInfOfferKey campaignOfferInfluencerId;

    @Column(name = "payment_offer")
    private Double paymentOffer;
    @Column(name = "commission_value")
    private Double commissionValue;
    @Column(name = "commission_value_type")
    private String commissionValueType;
    @Column(name = "commission_affiliate_type")
    private String commissionAffiliateType;
    @Column(name = "commission_affiliate_custom_name")
    private String commissionAffiliateCustomName;
    @Column(name = "commission_affiliate_details")
    private String commissionAffiliateDetails;
    @Column(name = "commission_value_type_id")
    private Integer commissionValueTypeId;
    @Column(name = "commission_affiliate_type_id")
    private Integer commissionAffiliateTypeId;

}
