package io.smartfluence.cassandra.entities.brand.primary;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import io.smartfluence.util.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@AllArgsConstructor
public class BrandCampaignHistoryKey implements Serializable {

	private static final long serialVersionUID = -668207589859668676L;

	public BrandCampaignHistoryKey(BrandCampaignHistoryKey key) {
		this.brandId = key.getBrandId();
		this.campaignId = key.getCampaignId();
	}

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.PARTITIONED, ordinal = 1)
	private UUID brandId;

	@PrimaryKeyColumn(name = "trans_yr_month", type = PrimaryKeyType.PARTITIONED, ordinal = 2)
	private String transYrMonth;

	@PrimaryKeyColumn(name = "campaign_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID campaignId;

	@Default
	@PrimaryKeyColumn(name = "modified_at", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private Date modifiedAt = new Date();

	public String getTransYrMonth() {
		return DateUtil.SDF_YYYY_MM.format(modifiedAt);
	}
}