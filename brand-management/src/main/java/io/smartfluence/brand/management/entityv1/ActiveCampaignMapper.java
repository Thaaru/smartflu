package io.smartfluence.brand.management.entityv1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActiveCampaignMapper {
    public String campaign_id;
    public ArrayList<InfluencerMapper> influencers;
    public int platform_id;
    public String campaign_name;
    public String platform_name;
}