package io.smartfluence.modal.brand.viewinfluencer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerDemographicModel {

	private String influencerHandle;

	private byte[] profileImage;

	private String location;

	private Double brandRelevance;

	private Double audienceCheck;

	private Double engagement;
	
	private Double engagementRate;

	private Double followers;

	private String influencerId;

	private Boolean isValid;

	private String platform;

	private Double avgLikes;

	private Double avgComments;

	@Builder.Default
	private Double avgViews = 0.0;

	@Builder.Default
	private Double avgDisLikes = 0.0;

	@Builder.Default
	private List<TopInfluencerPost> topContents = new ArrayList<>();

	@Builder.Default
	private Map<String, Double> ageRanges = new LinkedHashMap<>(6);

	@Builder.Default
	private Map<String, Double> genders = new LinkedHashMap<>(3);

	@Builder.Default
	private Map<String, Double> topIndustries = new LinkedHashMap<>(3);

	@Builder.Default
	private Map<String, Double> topCountries = new LinkedHashMap<>(5);

	@Builder.Default
	private Map<String, Double> topStates = new LinkedHashMap<>(5);

	@Builder.Default
	private Map<String, Double> lastestEngagement = new LinkedHashMap<>(10);

	private Double lastPrice;

	private Boolean subscribed;

	private String deepSocialInfluencerId;

	@Builder.Default
	private String reportId = "";

	@Builder.Default
	private Boolean reportPresent = false;

	@Builder.Default
	private List<byte[]> images = new ArrayList<>();

	@Builder.Default
	private List<String> videos = new ArrayList<>();

	private Integer status;

	private String message;
}