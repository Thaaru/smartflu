package io.smartfluence.util.image;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

class TiltFilter implements ImageFilter {

	@Override
	public BufferedImage processImageBuffer(BufferedImage bufferedImage) {
		AffineTransform rot = AffineTransform.getRotateInstance(Math.toRadians(1), bufferedImage.getWidth() / 2.0,
				bufferedImage.getHeight() / 2.0);
		BufferedImageOp filter = new AffineTransformOp(rot, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		return filter.filter(bufferedImage, null);
	}
}