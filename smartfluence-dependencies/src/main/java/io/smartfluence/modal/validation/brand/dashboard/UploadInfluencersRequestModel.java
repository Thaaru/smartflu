package io.smartfluence.modal.validation.brand.dashboard;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UploadInfluencersRequestModel {

	@NotBlank(message = "Please Provide InfluencerHandles")
	private String influencerHandles;

	private UUID campaignId;

	private List<String> workbookSheets = new ArrayList<>();
}