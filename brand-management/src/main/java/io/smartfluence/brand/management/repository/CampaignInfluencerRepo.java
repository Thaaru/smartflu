package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.CampaignInfluencerKey;
import io.smartfluence.brand.management.entityv1.CampaignInfluencerV1Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CampaignInfluencerRepo extends JpaRepository<CampaignInfluencerV1Entity, CampaignInfluencerKey> {
    Optional<CampaignInfluencerV1Entity> findByCampaignInfluencerId(CampaignInfluencerKey campaignInfluencerId);

    Optional<CampaignInfluencerV1Entity> findAllByCampaignInfluencerId(CampaignInfluencerKey campaignInfluencerId);

    List<CampaignInfluencerV1Entity> findAllByCampaignInfluencerId_CampaignId(String campaignId);

//    @Query("Select count(u) from campaign_influencer_details_v1 u where u.campaign_id=?1")
    public Integer countByCampaignInfluencerId_CampaignId(String campaignId);
    @Override
    boolean existsById(CampaignInfluencerKey campaignInfluencerKey);
    void deleteAllByCampaignInfluencerId_InfluencerIdInAndCampaignInfluencerId_CampaignId(List<String> influencerId, String campaignId);

//    Optional<CampaignInfluencerV1Entity>  findByCampaignInfluencerId_InfluencerId(String influencerId);

    List<CampaignInfluencerV1Entity>  findAllByCampaignInfluencerId_InfluencerIdIn(List<String> influencerId);


}