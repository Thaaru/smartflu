import React, { useEffect, useState } from "react";
import { useMachine } from "@xstate/react";
import { influencerMachine } from "../influencer_machine";
import InfluencerLandingPage from "./influencer_landing_page";
import InfluencerWorkFlowPage from "./influencer_workflow_page";
import { getCampaignDetailsApi, getUserAccountApi } from "../services/api/campaign_details_api";
import OfferPage from "./offer_page";
import ProductPage from "./products_page";
import { productHandler } from "../components/productHandler.js";
import DeliverablesPage from "./deliverables_page";
import ShippingPage from "./shipping_page";
import PaymentPage from "./payment_page";
import TermsConditionPage from "./terms_conditions_page";
import CreateAccountScreen from "./create_and_verify_account_page";
import NotificationComponent from "../components/notification_component";
import CurrentPartnerShipPage from "./current_partnership_page";
import { ACCOUNT_STATUS, HOMEPATH, PASSWORD_STATUS, PROPOSAL_STATUS, SUCCESS_CODE } from "../utils/string_constants";



export default function MainPage() {
    const [current, send] = useMachine(influencerMachine);
    const [data, setData] = useState(null);
    const [urlLastName, setUrlLastName] = useState(null);
    const [code, setCode] = useState(null);
    const [booleanList, setBooleanList] = useState();

    useEffect(() => {
        const url = window.location.href;
        const lastElement = url.substring(url.lastIndexOf("/") + 1);
        if (lastElement != HOMEPATH) {
            getCampaignData();
        }else{
            setCode(SUCCESS_CODE);
        }
        setUrlLastName(lastElement);
        async function getCampaignData() {
            const responeData = await getCampaignDetailsApi();
            setCode(responeData.code);
            if (responeData.code == 600) {
                if (responeData.value != null) {
                    setData(responeData.value);
                    setBooleanList(productHandler(responeData.value))
                }               
            }
        }       

    }, []);


    return (
        <>
            {code != null ? code.toString() != SUCCESS_CODE ? <NotificationComponent code={code} /> :
            urlLastName==HOMEPATH?(
                <CurrentPartnerShipPage
                    key="partnership"
                    data={data}
                />
            ): 
                data.influencerAccountStatus == ACCOUNT_STATUS && data.influencerPasswordStatus == PASSWORD_STATUS && urlLastName != HOMEPATH && data.proposalStatus != PROPOSAL_STATUS.INVITE_SENT ?
                    (
                        <CreateAccountScreen
                            key="createAccount"                          
                            data={data}
                            code={code}
                            next={value => send("NEXT", { value })}
                        />
                    ) :
                    data.influencerAccountStatus == ACCOUNT_STATUS && data.influencerPasswordStatus == PASSWORD_STATUS && data.proposalStatus != PROPOSAL_STATUS.INVITE_SENT ?
                        (
                            <CurrentPartnerShipPage
                                key="partnership"
                                data={data}
                            />
                        ) :
                        data.proposalStatus == PROPOSAL_STATUS.INFLUENCER_ACCEPTED ?
                            (
                                <CreateAccountScreen
                                    key="createAccount"                                
                                    data={data}
                                    code={code}
                                    next={value => send("NEXT", { value })}
                                />
                            ) :
                            current.matches("getStarted") ? (
                                <InfluencerLandingPage
                                    key="getStarted"
                                    data={data}
                                    code={code}
                                    onSubmit={value => send("NEXT", { value })}
                                />
                            ) : current.matches("overview") ? (
                                <InfluencerWorkFlowPage
                                    key="overview"
                                    data={data}
                                    code={code}
                                    offer={value => send("OFFERS", { value })}
                                    product={value => send("PRODUCTS", { value })}
                                    deliverables={value => send("DELIVERABLES", { value })}
                                    shipping={value => send("SHIPPING", { value })}
                                    payment={value => send("PAYMENT", { value })}
                                    terms={value => send("TERMS", { value })}
                                    onSubmit={value => send("NEXT", { value })}
                                />
                            ) : current.matches("offers") ? (
                                <OfferPage
                                    key="offers"
                                    data={data}
                                    code={code}
                                    close={value => send("CLOSE", { value })}
                                    next={value => send("NEXT", { value })}
                                />
                            ) : current.matches("products") ? (
                                <ProductPage
                                    key="products"
                                    data={data}
                                    code={code}
                                    booleanData={booleanList}
                                    close={value => send("CLOSE", { value })}
                                    next={value => send("NEXT", { value })}
                                    back={value => send("BACK", { value })}
                                />
                            ) : current.matches("deliverables") ? (
                                <DeliverablesPage
                                    key="deliverables"
                                    data={data}
                                    code={code}
                                    close={value => send("CLOSE", { value })}
                                    next={value => send("NEXT", { value })}
                                    back={value => send("BACK", { value })}
                                />
                            ) : current.matches("shipping") ? (
                                <ShippingPage
                                    key="shipping"
                                    data={data}
                                    code={code}
                                    close={value => send("CLOSE", { value })}
                                    next={value => send("NEXT", { value })}
                                    back={value => send("BACK", { value })}
                                />
                            ) : current.matches("payment") ? (
                                <PaymentPage
                                    key="payment"
                                    data={data}
                                    code={code}
                                    close={value => send("CLOSE", { value })}
                                    next={value => send("NEXT", { value })}
                                    back={value => send("BACK", { value })}
                                />
                            ) : current.matches("terms") ? (
                                <TermsConditionPage
                                    key="terms"
                                    data={data}
                                    code={code}
                                    next={value => send("NEXT", { value })}
                                    close={value => send("CLOSE", { value })}
                                    back={value => send("BACK", { value })}
                                />
                            ) : current.matches("createAccount") ? (
                                <CreateAccountScreen
                                    key="createAccount"
                                    data={data}
                                    code={code}
                                    next={value => send("NEXT", { value })}
                                />
                            ) : current.matches("partnership") ? (
                                <CurrentPartnerShipPage
                                    key="partnership"
                                    data={data}
                                />
                            ) : (<></>) : <div className="loading" style={{ "alignContent": "center" }}>
                <div className='spinner-wrapper'>
                    <div class="heading mb-2">Please Wait ....</div>
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
            }
        </>
    );
}