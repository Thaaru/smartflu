package io.smartfluence.influencer.services;

import io.smartfluence.influencer.entities.CampaignTasks;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.entities.PlatformPostTypeMaster;
import io.smartfluence.influencer.repository.PlatformPostTypeMasterRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class PlatformPostTypeMasterService {

    @Autowired
    PlatformPostTypeMasterRepo platformPostTypeMasterRepo;

    public FullCampaignInfluencerDetails getPlatformPostTypeMasterService(List<CampaignTasks> campaignTaskList, FullCampaignInfluencerDetails fullCampaignInfluencerDetails){
        Logger.getLogger("getPlatformPostTypeMasterService").log(Level.FINE,"Getting the Platform PostType MasterService");
        List<PlatformPostTypeMaster> platformPostTypeMasterList = new ArrayList<>();
        campaignTaskList.forEach(campaignTasks -> {
            PlatformPostTypeMaster platformPostTypeMaster = platformPostTypeMasterRepo.findByPostTypeId(campaignTasks.getPostTypeId());
            if(platformPostTypeMaster!=null){
                platformPostTypeMasterList.add(platformPostTypeMaster);
            }
        });
        if(!platformPostTypeMasterList.isEmpty()){
            fullCampaignInfluencerDetails.setPlatformPostTypeDetailsList(platformPostTypeMasterList);
        }
        return fullCampaignInfluencerDetails;
    }

}
