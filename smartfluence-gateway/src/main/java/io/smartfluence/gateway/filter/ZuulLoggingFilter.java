package io.smartfluence.gateway.filter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class ZuulLoggingFilter extends ZuulFilter {

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
		log.info("Request - > {}\nURI -> {}\n{}", request, request.getRequestURI(),
				SecurityContextHolder.getContext().getAuthentication());
		return null;
	}

	@Override
	public String filterType() {
		return ZuulFilterType.PRE.value();
	}

	@Override
	public int filterOrder() {
		return 0;
	}
}