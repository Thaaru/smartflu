package io.smartfluence.influencer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import io.smartfluence.influencer.entities.CampaignTasks;

public interface CampaignTaskRepo extends JpaRepository<CampaignTasks,String>{
    List<CampaignTasks> getByCampaignId(String campaignId);
}
