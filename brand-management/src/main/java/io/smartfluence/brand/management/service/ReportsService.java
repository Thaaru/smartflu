package io.smartfluence.brand.management.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.http.ResponseEntity;

import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.constants.CreditType;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.ViewType;
import io.smartfluence.modal.brand.dashboard.BrandCampaignInfluencerList;
import io.smartfluence.modal.brand.dashboard.CampaignPostLinkModal;
import io.smartfluence.modal.brand.dashboard.InfluencerSnapshot;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.social.report.CodeAndWeight;
import io.smartfluence.modal.social.report.ReportData;
import io.smartfluence.modal.validation.brand.explore.SearchInfluencerRequestModel;
import io.smartfluence.mysql.entities.SocialIdMaster;

public interface ReportsService {

	ResponseEntity<Object> influencerAudienceDataReport(SearchInfluencerRequestModel searchInfluencerRequestModel);

	boolean checkBrandCredit(UUID brandId, String influencerId, CreditType creditType);

	ResponseEntity<byte[]> downloadReport(String influencerHandle, String reportId);

	ResponseEntity<String> exportCampaignCsv(UUID campaignId);

	ResponseEntity<byte[]> downloadCampaignCsv(UUID campaignId, UUID csvName);

	ReportData getReportData(String deepSocialInfluencerId, String platform, ViewType viewType, UUID searchId);

	ReportData createReport(String deepSocialInfluencerId, ViewType viewType, String platform, String brandId, UUID searchId);

	ResponseEntity<byte[]> downloadTemplate();

	InfluencerDemographicsMapper getInfluencerDemographics(SocialIdMaster socialId, String deepSocialId, ViewType viewType, boolean searchNew);

	UUID saveNewInfluencer(String deepsocialuserid, String platform, ViewType viewtype);

	String saveInfluencerReportData(ReportData reportData);

	Double getEstimatedPricing(Double engagementRate, Double followers, Double engagement, Double audienceCredibility);

	Map<String, Double> getGenderFromSocial(List<CodeAndWeight> audienceGenders);

	Map<String, Double> getAgeRangesFromSocial(List<CodeAndWeight> audienceAges);

	Object saveBrandCreditDetails(UUID userId, Optional<BrandCredits> optionalBrandCredits, CreditType creditType);

	void saveReportHistory(ReportSearchHistory reportSearchHistory);

	Platforms getPlatform(String platformType);

	void generateCampaignPostLinkDetails(List<CampaignPostLinkModal> campaignPostLinkModal, UUID brandId, UUID campaignId);

	void generateBrandCampaignInfluencerList(UUID campaignId, UUID brandId,	List<BrandCampaignInfluencerList> brandBrandCampaignInfluencerList);

	void generateInfluencerSnapshot(UUID campaignId, UUID brandId, Set<InfluencerSnapshot> influencerSnapshots);

	List<InfluencerDemographicsMapper> getAllInfluencerDemographics(List<String> instagramHandles, List<String> youtubeHandles, List<String> tiktokHandles);
}