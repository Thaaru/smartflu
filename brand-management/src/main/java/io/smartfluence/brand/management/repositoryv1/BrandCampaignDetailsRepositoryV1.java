package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.BrandCampaignDetailsV1;
import io.smartfluence.brand.management.entityv1.ManageCampaignMapperNew;
import io.smartfluence.modal.brand.campaignv1.CampaignStatusEntityV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BrandCampaignDetailsRepositoryV1 extends JpaRepository<BrandCampaignDetailsV1,String> {
    @Query("Select c from BrandCampaignDetailsV1 c  JOIN FETCH c.influencers b " +
            "LEFT JOIN FETCH b.influencerProducts inp LEFT JOIN FETCH b.influencerTasks int " +
            " LEFT JOIN FETCH b.influencerAddresses ina LEFT JOIN FETCH b.influencerOffers ino " +
            " LEFT JOIN FETCH b.socialIdMasterList ins " +
            "where  c.campaignId=:campaignId")
    BrandCampaignDetailsV1 findByCampaignIdByEager(String campaignId);
    BrandCampaignDetailsV1 findByCampaignId(String campaignId);
    List<BrandCampaignDetailsV1> findAllByBrandIdAndCampaignStatus(String brandId, CampaignStatusEntityV1 campaignStatus);
    List<BrandCampaignDetailsV1> findByBrandId(String brandId);
    List<BrandCampaignDetailsV1> findAllByBrandIdAndCampaignId(String brandId, String campaignId);
    Optional<BrandCampaignDetailsV1> findByCampaignIdAndBrandId(String campaignId, String brandId);
    List<BrandCampaignDetailsV1> findByCampaignNameIgnoreCaseAndBrandId(String campaignName,String brandId);
    @Query("delete from CampaignInfluencerOfferV1Entity u where u.campaignOfferInfluencerId.campaignId=?2 and u.campaignOfferInfluencerId.influencerId IN (?1) ")
    void deleteAllByInfluencerIdInAndCampaignId(List<String> influencerId, String campaignId);

        @Query("select u from BrandCampaignDetailsV1 u where u.campaignName=?1 and " +
            "u.brandId=?2 and u.campaignId!=?3 and u.campaignStatus!=?4")
    BrandCampaignDetailsV1 findByCampaignNameAndBrandIdAndCampaignIdNotAndCampaignStatusNot(
            String campaignName,String brandId,String campaignId,CampaignStatusEntityV1 campaignStatus);
    @Query(value = "CALL get_campaign_by_brandId(:brand_id);",nativeQuery = true)
    List<ManageCampaignMapperNew> findAllByBrandIdUsingProcedure(@Param("brand_id") String brandId);

    @Query("select c.campaignStatus from BrandCampaignDetailsV1 c where c.campaignId=?1")
    String findCampaignStatusByCampaignId(String campaignId);
}
