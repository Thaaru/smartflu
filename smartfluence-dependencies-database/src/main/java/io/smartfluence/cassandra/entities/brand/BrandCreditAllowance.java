package io.smartfluence.cassandra.entities.brand;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandCreditAllowanceKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_credit_allowance")
public class BrandCreditAllowance implements Serializable {

	private static final long serialVersionUID = 2571378644007835585L;

	@PrimaryKey
	private BrandCreditAllowanceKey key;

	@Column("trans_yr_month")
	private String transYrMonth;

	@Column("created_at")
	private Date createdAt;
}