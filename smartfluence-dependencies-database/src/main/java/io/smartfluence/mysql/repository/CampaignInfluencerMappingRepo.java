package io.smartfluence.mysql.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.mysql.entities.CampaignInfluencerMapping;

@Repository
public interface CampaignInfluencerMappingRepo extends CrudRepository<CampaignInfluencerMapping, Integer> {

	Optional<CampaignInfluencerMapping> findByBrandCampaignDetailsCampaignIdAndInfluencerId(String campaignId, String influencerId);
}