package io.smartfluence.mysql.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.mysql.entities.APIServiceMaster;

@Repository
public interface APIServiceMasterRepo extends CrudRepository<APIServiceMaster, Long> {

	Optional<APIServiceMaster> findByService(String service);
}