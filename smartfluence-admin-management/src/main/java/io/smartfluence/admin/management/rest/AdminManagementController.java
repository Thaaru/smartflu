package io.smartfluence.admin.management.rest;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.smartfluence.admin.management.service.AdminManagementService;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.modal.validation.admin.ReportRequestModal;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Controller
@RequestMapping("admin")
public class AdminManagementController {

	@Autowired
	private AdminManagementService adminManagementService;

	@GetMapping("brands")
	public ResponseEntity<Object> getBrandList() {
		return new ResponseEntity<>(adminManagementService.getBrandList(), HttpStatus.OK);
	}

	@GetMapping("subscriptions")
	public ResponseEntity<Object> getSubscriptions() {
		return new ResponseEntity<>(adminManagementService.getSubscriptions(), HttpStatus.OK);
	}

	@PostMapping("approve-brand")
	public ResponseEntity<Object> approveBrand(@RequestParam String brandId, @RequestParam SubscriptionType subscriptionType,
			@RequestParam UUID subscriptionId) {
		UUID brandUserId = UUID.fromString(brandId);

		adminManagementService.approveBrand(brandUserId, subscriptionType, subscriptionId);
		log.info("Approved brand {} with Subscription {}", brandId, subscriptionId);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("decline-brand")
	public ResponseEntity<Object> declineBrand(@RequestParam String brandId) {
		UUID brandUserId = UUID.fromString(brandId);

		adminManagementService.declineBrand(brandUserId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("send-invitation")
	public ResponseEntity<Object> sendInvitation(@RequestParam String[] selectedUserId) {
		adminManagementService.sendInvitation(selectedUserId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("db-histories")
	public ResponseEntity<Object> getDBUpdateHistory() {
		return new ResponseEntity<>(adminManagementService.getDBUpdateHistory(), HttpStatus.OK);
	}

	@PostMapping("update-db")
	public ResponseEntity<Object> updateDb() {
		return adminManagementService.updateDb();
	}

	@GetMapping("check-db-update-status")
	public ResponseEntity<Object> checkDbUpdateStatus() {
		return adminManagementService.checkDbUpdateStatus();
	}

	@GetMapping("export-data")
	public ResponseEntity<String> exportAdminData() {
		return adminManagementService.exportAdminData();
	}

	@GetMapping("export-report-data")
	public ResponseEntity<Object> exportReportData(@Validated @ModelAttribute ReportRequestModal reportRequestModal) {
		return adminManagementService.exportReportData(reportRequestModal);
	}

	@GetMapping("download/{flag}/report")
	public ResponseEntity<byte[]> downloadReport(@PathVariable String flag) {
		return adminManagementService.downloadReport(flag);
	}
}