import React from 'react';
import ResponseEntity from './response_entity';

export  function getResponseData(serverData){
    var data = serverData;
   
    var response;
    
   
       response = new ResponseEntity({
        code:data["code"],
        value:data["value"]==null?null:data["value"],
        error:data["error"]
    });
    return response;

} 