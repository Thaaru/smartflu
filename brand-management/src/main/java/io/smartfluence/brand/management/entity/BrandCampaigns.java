package io.smartfluence.brand.management.entity;



import io.smartfluence.constants.CampaignStatus;
import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class BrandCampaigns {

    private UUID brandId;

    private CampaignStatus campaignStatus;

    private UUID campaignId;

    private String campaignName;

    private String stringCreatedAt;

    private Date createdAt;

    private int influencerCount;

    private Boolean isPromote;

    private String platform;

    private String type;

    private String status;

    private List<String> platformList;


}
