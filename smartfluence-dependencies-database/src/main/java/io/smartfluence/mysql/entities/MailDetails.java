package io.smartfluence.mysql.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "mail_details")
public class MailDetails implements Serializable {

	private static final long serialVersionUID = 3778466382375464591L;

	@Id
	@Column(name = "mail_id")
	private String mailId;

	@Column(name = "brand_id")
	private String brandId;

	@Column(name = "brand_email")
	private String brandEmail;

	@Column(name = "brand_name")
	private String brandName;

	@Column(name = "influencer_handle")
	private String influencerHandle;

	@Column(name = "influencer_email")
	private String influencerEmail;

	@Column(name = "influencer_invite_link")
	private String influencerInviteLink;

	@Column(name = "mail_cc")
	private String mailCc;

	@Column(name = "mail_subject")
	private String mailSubject;

	@Column(name = "mail_description")
	private String mailDescription;

	@Column(name = "mail_template_id")
	private String mailTemplateId;

	@Column(name = "status")
	private Integer status;

	@Column(name = "mapping_id")
	private String mappingId;

	@Column(name = "campaign_id")
	private String campaignId;

	@Column(name = "campaign_name")
	private String campaignName;

	@Column(name = "campaign_description")
	private String campaignDescription;

	@Column(name = "product")
	private String product;

	@Column(name = "post_type")
	private String postType;

	@Column(name = "payment")
	private Double payment;

	@Column(name = "is_approval_mail")
	private Boolean isApprovalMail;

	@Column(name = "is_promote")
	private Boolean isPromote;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name="is_new")
	private Boolean isNew;
}