import React from "react";

export const loaderMethod = () => {
    return <div class="spinner-border" role="status" color="white">
        <span class="sr-only">Loading...</span>
    </div>
}