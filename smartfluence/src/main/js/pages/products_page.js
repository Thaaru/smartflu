import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { HeaderComponent } from "../components/header_component";
import { FooterComponent } from "../components/footer_component";
import ColoredLine from "../components/line_component";
import { updateCampaignDetailsApi } from "../services/api/campaign_details_api";
import { useMachine } from "@xstate/react";
import { influencerMachine } from "../influencer_machine";
import { ACCEPTED_MESSAGE, ERROR_MESSAGE, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";
import { toast } from "react-toastify";
export default function ProductPage({ data, code, back, next, close, booleanData }) {

    const [current, send] = useMachine(influencerMachine);
    var [booleanList, setBooleanList] = useState(booleanData);
   const [checked, setChecked] = useState([]);

    useEffect(() => {      
        handleBoolean(null);
        handleDisable(booleanList);
    })

    const [disable, setDisable] = useState([]);
    const handleDisable = (booleanList) => {
        const disable = [];
        for (let i = 0; i < booleanList.length; i++) {
            disable[i] = false;
        }
        if (submitStatus >= data.maxProductCount) {
            for (let i = 0; i < booleanList.length; i++) {
                disable[i] = !booleanList[i];
            }
        }
        setDisable(disable);
    }

   const [submitStatus, setSubmitStatus] = useState(0);
    const handleBoolean = (index, event) => {
        if (index != null)
            booleanList[index] = event.target.checked;
        setBooleanList(booleanList);
        checked.length = 0
        var updatedBooleanList = 0;
        booleanList.map((boolean) => {
            if (boolean == true) {
                updatedBooleanList++;
            }
        })
        setSubmitStatus(updatedBooleanList);
        return booleanList[index];
    };
    const submitHandler = async (data, loader) => {
        data.productsAcceptedStatus = STEPS_ACCEPTED_STATUS;
        const tempProducts = data.products;
        const tempProductList = [];
        data.products.map((item, index) => {
            const productData = item;
            const productAcceptedData = {};
            if (booleanList[index] == true) {
                productAcceptedData["campaignId"] = data.campaignId;
                productAcceptedData["influencerId"] = data.influencerId;
                productAcceptedData["productId"] = productData.productId;
                tempProductList.push(productAcceptedData);
            }
        });
        data.productsAcceptedList = tempProductList;
        loader(false);
        const result = await updateCampaignDetailsApi(data);
        if (result.code == 600) {
            toast.success(ACCEPTED_MESSAGE,{delay:1000})
            loader(true);
            next(result.value);
        } else {
            toast.error(ERROR_MESSAGE,{delay:1000})
            loader(true);
            data.productsAcceptedStatus = null;
            data.products = tempProducts;
            return null;
        }
    }
    return (
        <>
            {data == null ? <div className="text-center" style={{ "alignContent": "center" }}>
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div> :
                <div class='content-wrapper'>
                    <div>
                        <div >
                            <HeaderComponent data={data} close={close} headerName={"Product"} stepNo={2} />
                            <ColoredLine color={'secondaryColor'} />
                            <br />
                            <div className="col" style={{ "textAlign": "left" }}>
                                <div className='col-md'>
                                    <p className="normalText">
                                        Here are the products you will receive
                                    </p>
                                </div>
                                <div className='col-md' style={{ "textAlign": "center" }}>
                                    <p className="normalText">
                                        {
                                            data.maxProductCount!=null?data.maxProductCount >= 3?`Select all the products`: `Select only ${data.maxProductCount} products`: `Select only 2 products`
                                        }
                                        {/* Select only {data.maxProductCount != null ? data.maxProductCount : "2"} products */}
                                    </p>
                                </div>
                                <div className="col-md">                              
                                    {data.products.map((item, index) => {
                                       return (
                                        <div className="form-check " key={index}>
                                            <input className="form-check-input" type="checkbox" value={item.productName}
                                                checked={booleanList[index]} onChange={(event) => { handleBoolean(index, event); handleDisable(booleanList) }} id="flexCheckDefault" style={{ "height": "20px", "width": "20px", "borderRadius": "5px" }} />
                                            <label className="form-check-label" for="flexCheckDefault" style={{ "fontSize": "18px", marginLeft: "15px" }}>{item.productName}</label>
                                            <div className="col-md" style={{ "marginBottom": "20px","overflow":"hidden",textOverflow:"ellipsis",width:"100%",display:"block",whiteSpace:"nowrap"}}>
                                                <a className="primaryColor"  onClick={()=>{window.open(item.productLink)}} style={{ "textDecoration": "underline","cursor":"pointer" }}>{item.productLink}</a>
                                            </div>
                                        </div>
                                          );})}                                  
                                </div>
                                <div className="col-md" />
                            </div>
                            {
                                submitStatus > data.maxProductCount ?
                                    <div className="col-md" style={{ "textAlign": "center", color: "red" }}>
                                        Please select {data.maxProductCount} products only!
                                    </div> : null
                            }
                        </div>
                        <FooterComponent next={next} back={back} data={data} forwardStatus={STEPS_ACCEPTED_STATUS} backWardStatus={STEPS_ACCEPTED_STATUS} callBackfunction={submitStatus != data.maxProductCount ? null : submitHandler} submitStatus={submitStatus} previousScreenStatus={data.offerAcceptedStatus}/>
                    </div>
                </div>
            }
        </>
    );
}
