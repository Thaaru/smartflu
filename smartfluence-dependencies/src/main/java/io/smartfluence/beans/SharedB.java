package io.smartfluence.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SharedB {

	static final int MIN_LOG_ROUNDS = 4;

	static final int MAX_LOG_ROUNDS = 31;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(MIN_LOG_ROUNDS);
	}

	@Bean
	public TaskScheduler taskScheduler() {
		return new ConcurrentTaskScheduler();
	}
}