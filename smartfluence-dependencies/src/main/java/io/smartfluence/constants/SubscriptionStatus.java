package io.smartfluence.constants;

public enum SubscriptionStatus {
	ACTIVE, INACTIVE, SCHEDULED
}