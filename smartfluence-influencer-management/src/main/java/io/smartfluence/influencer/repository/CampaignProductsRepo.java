package io.smartfluence.influencer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.influencer.entities.CampaignProducts;
@Repository
public interface CampaignProductsRepo extends JpaRepository<CampaignProducts,String>{
    List<CampaignProducts> getByCampaignId(String campaignId);
}
