package io.smartfluence.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.TemporaryInfluencerContent;
import io.smartfluence.cassandra.entities.primary.TemporaryInfluencerContentKey;

public interface TemporaryInfluencerContentRepo extends CassandraRepository<TemporaryInfluencerContent, TemporaryInfluencerContentKey> {

}