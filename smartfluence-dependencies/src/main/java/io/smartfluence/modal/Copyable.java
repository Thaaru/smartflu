package io.smartfluence.modal;

import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface Copyable extends Serializable {

	static final ObjectMapper MAPPER = new ObjectMapper();

	default Object copy() throws CloneNotSupportedException {
		Object copy;
		try {
			copy = Copyable.MAPPER.readValue(Copyable.MAPPER.writeValueAsString(this), this.getClass());
		} catch (IOException e1) {
			throw new CloneNotSupportedException(e1.getMessage());
		}
		return copy;
	}
}