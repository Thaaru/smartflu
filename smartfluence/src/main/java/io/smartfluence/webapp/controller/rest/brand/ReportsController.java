package io.smartfluence.webapp.controller.rest.brand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.modal.validation.brand.explore.SearchInfluencerRequestModel;
import io.smartfluence.webapp.proxy.BrandManagementServiceProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class ReportsController {

	@Autowired
	private BrandManagementServiceProxy brandProxy;

	@GetMapping("reports/audience-data")
	public ResponseEntity<Object> influencerAudienceDataReport(@Validated @ModelAttribute SearchInfluencerRequestModel searchInfluencerRequestModel,
			BindingResult bindingResult) {
		log.info(searchInfluencerRequestModel);

		if (!bindingResult.hasFieldErrors())
			return brandProxy.influencerAudienceDataReport(searchInfluencerRequestModel.getInfluencerId(),searchInfluencerRequestModel.getInfluencerHandle(),
					searchInfluencerRequestModel.getDeepSocialInfluencerId(), searchInfluencerRequestModel.getReportId(), 
					searchInfluencerRequestModel.getReportPresent(), searchInfluencerRequestModel.getPlatform());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("reports/{influencerHandle}/audience-data/{reportId}")
	public ResponseEntity<byte[]> downloadReport(@PathVariable String influencerHandle, @PathVariable String reportId) {
		log.info("AD Report {} download requested  for {}", reportId, influencerHandle);
		return brandProxy.downloadReport(influencerHandle, reportId);
	}
}