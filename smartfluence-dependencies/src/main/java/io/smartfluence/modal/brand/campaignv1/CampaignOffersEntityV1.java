package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignOffersEntityV1 {
    private String campaignId;

    private OfferTypeEntityV1 offerType;

    private Double paymentOffer;

    private Double commissionValue;

    private String commissionValueType;

    private String commissionAffiliateType;

    private String commissionAffiliateCustomName;

    private String commissionAffiliateDetails;

    private Integer commissionValueTypeId;

    private Integer commissionAffiliateTypeId;
}
