package io.smartfluence.modal.social.search.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchAccount implements Serializable {

	private static final long serialVersionUID = 6405309742706137084L;

	@JsonAlias("user_profile")
	private SearchUserProfile user;

	@JsonAlias("audience_source")
	private String audienceSource;
}