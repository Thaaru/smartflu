package io.smartfluence.util.mail;

abstract class AbstractMailContent implements MailContent {

	protected final StringBuilder contentBuilder = new StringBuilder();
	
	private static final String DEFAULT_PRE_SUF="";

	private SmartMailBuilder builder;

	SmartMailBuilder getBuilder() {
		return builder;
	}

	protected void setBuilder(SmartMailBuilder builder) {
		this.builder = builder;
	}

	@Override
	public MailContent reset() {
		contentBuilder.setLength(0);
		return this;
	}

	@Override
	public MailContent appendContent(String content) {
		contentBuilder.append(getItemPrefix() + content + getItemSuffix());
		return this;
	}

	protected String getItemPrefix() {
		return DEFAULT_PRE_SUF;
	}

	protected String getItemSuffix() {
		return DEFAULT_PRE_SUF;
	}

	@Override
	public MailContent setValue(String param, String value) {
		int idx;
		while ((idx = contentBuilder.indexOf(':' + param + ':')) != -1) {
			contentBuilder.replace(idx, param.length() + 2, value);
		}
		return this;
	}

	@Override
	public MailContent setLink(String param, String value, String address) {
		int idx;
		while ((idx = contentBuilder.indexOf(':' + param + ':')) != -1) {
			String linkValue = "<a style=\"text-decoration: none;mix-blend-mode: difference;color: #009e83;font-family: Helvetica, Arial, sans-serif;\" target=\"_blank\" href=\""
					+ address + "\">" + value + "</a>";
			contentBuilder.replace(idx, idx+param.length() + 2, linkValue);
		}
		return this;
	}

	@Override
	public StringBuilder content() {
		return contentBuilder;
	}

	@Override
	public SmartMailBuilder and() {
		return getBuilder();
	}
}