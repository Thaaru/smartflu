package io.smartfluence.mysql.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "stripe_customer")
public class StripeCustomer implements Copyable{

	private static final long serialVersionUID = 5974860996741959910L;

	@Id
	@Column(name="user_id")
	private String userId;

	@Column(name="customer_id")
	private String customerId;
}