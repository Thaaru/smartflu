package io.smartfluence.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.InfluencerBidDetails;
import io.smartfluence.cassandra.entities.primary.InfluencerBidDetailsKey;

@Repository
public interface InfluencerBidDetailsRepo extends CassandraRepository<InfluencerBidDetails, InfluencerBidDetailsKey> {

}