package io.smartfluence.mysql.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.mysql.entities.MailDetails;

@Repository
public interface MailDetailsRepo extends CrudRepository<MailDetails, String> {

	@Modifying
	@Transactional
	void deleteAllByCampaignId(String campaignId);

	List<MailDetails> findAllByCampaignId(String campaignId);
}