package io.smartfluence.brand.management.mailbean;

public class Constants {
    public static String secretKey="secretKey";
    public static String campaignId="campaignId";
    public static String influencerId="influencerId";
    public static String Expiration="exp";
    public static String HOUR="HOUR";
    public static String MINUTE="MINUTE";
    public static String SECOND="SECOND";
    public static String DATE="DATE";
    public static String MONTH="MONTH";
    public static String YEAR="YEAR";

}
