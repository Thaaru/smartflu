package io.smartfluence.cassandra.entities.youtube;

import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("youtube_demographics")
public class YouTubeDemographics {

	@PrimaryKey("influencer_handle")
	private String influencerHandle;

	@Column("user_id")
	private UUID userId;

	@Column("url")
	private String url;

	@Column("profile_image")
	private String profileImage;

	@Column("email")
	private String email;

	@Column("male")
	private Double male;

	@Column("female")
	private Double female;

	@Column("age_13_17")
	private Double age13To17;

	@Column("age_18_24")
	private Double age18To24;

	@Column("age_25_34")
	private Double age25To34;

	@Column("age_35_44")
	private Double age35To44;

	@Column("age_45_64")
	private Double age45To64;

	@Column("age_65_")
	private Double age65ToAny;

	@Column("engagement")
	private Double engagement;
	
	@Column("engagement_rate")
	private Double engagementRate;

	@Column("followers")
	private Double followers;

	@Column("credibility")
	private Double credibility;

	@Column("pricing")
	private Double pricing;

	@Column("comments")
	private Double comments;

	@Column("likes")
	private Double likes;

	@Column("dis_likes")
	private Double disLikes;

	@Column("views")
	private Double views;

	@Column("influencer_name")
	private String influencerName;

	public Double getMale() {
		return male == null ? 0 : male;
	}

	public Double getFemale() {
		return female == null ? 0 : female;
	}

	public Double getAge13To17() {
		return age13To17 == null ? 0 : age13To17;
	}

	public Double getAge18To24() {
		return age18To24 == null ? 0 : age18To24;
	}

	public Double getAge25To34() {
		return age25To34 == null ? 0 : age25To34;
	}

	public Double getAge35To44() {
		return age35To44 == null ? 0 : age35To44;
	}

	public Double getAge45To64() {
		return age45To64 == null ? 0 : age45To64;
	}

	public Double getAge65ToAny() {
		return age65ToAny == null ? 0 : age65ToAny;
	}

	public Double getEngagement() {
		return engagement == null ? 0 : engagement;
	}

	public Double getFollowers() {
		return followers == null ? 0 : followers;
	}

	public Double getCredibility() {
		return credibility == null ? 0 : credibility;
	}
}