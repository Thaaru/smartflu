package io.smartfluence.cassandra.repository.youtube;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.youtube.YouTubeDemographics;

@Repository
public interface YouTubeDemographicRepo extends CassandraRepository<YouTubeDemographics, String> {

	@AllowFiltering
	Optional<YouTubeDemographics> findByUserId(UUID userId);
}