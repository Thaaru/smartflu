var deleteCampId = "";
var rowsSelected = [];
var editor;
var campaignDT;

//$.fn.dataTable.moment('DD-MM-YYYY');

const keyEvent = (event) => {
	var cId = event.target.id.replace("cinput_", "");
	var td = $(`td#cname_${cId}`);
    var prevVal = td.attr('data-cname');
    var { value } = event.target;
	if(!value){
		toast("Campaign Name Required","error")
	}
    else if (prevVal != value && value.trim() != "") {
        if (event.key === "Enter" && value) {
            callAPI({cId: cId, value: value}, td);
        }
		callAPI({cId: cId, value: value}, td);
    }
}

const callAPI = (data, td) => {
    $.ajax({
        type        : 'PUT',
        url         : `/brand/view-campaign-v1/update-campaign-name?campaignId=${data.cId}&campaignName=${data.value}`,
        // data        : JSON.stringify(data),
        // contentType : "application/json; charset=utf-8",
        // traditional : true,
        success: function (response, statusMsg, xhr) {
            if (response.code == 600) {
				td.attr('data-cname', data.value).empty().html(data.value);
                toast("Campaign Name Updated Successfully");
            } else {
                toast("Error occured", 'error');
            }
        },
		error : function(xhr) {
			
            toast("Error occured", 'error');
		}
    });
}

$(document).ready(function() {
	$(document).on('focusout', '.cname-class', function() {
		$(".cname-class").each(function() {
			$(this).empty().html($(this).attr('data-cname'));
		});
	});

    $(document).on( 'click', '#table_campaign > tbody td .row-edit', function (e) {
		var trId = $(this).closest('tr').attr('id');
		var td = $(`td#cname_${trId}`);
		var innerText = td.attr('data-cname');
		td.html(`<input class="form-control cname-input" id='cinput_${trId}' onfocusout="return keyEvent(event);" />`).find(".cname-input").val(innerText).focus();
    });

	campaignDT = $('#table_campaign').DataTable({
		dom: "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-7 pl-0'><'col-sm-12 col-md-3'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
		
		columnDefs: [
			{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					return '<input type="checkbox" disabled>';
				}
			},
			{
				className: 'control',
				targets: [0, 7],
				orderable: false
			}
		],
		select: {
			style: 'multi'
		},
		order: [
			[1, 'asc']
		],
		'rowCallback': function(row, data, dataIndex) {
			// Get row ID
			var rowId = data.DT_RowId;
			// If row ID is in the list of selected row IDs
			if ($.inArray(rowId, rowsSelected) !== -1) {
				$(row).find('input[type="checkbox"]').prop(
					'checked', true);
				$(row).addClass('selected');
			}
		}
	});

	campaignDT.on('draw', function() {
		// Update state of "Select all" control
		updateDataTableSelectAllCtrl(campaignDT);
	});
});

function updateDataTableSelectAllCtrl(table) {
	var $table = table.table().node();
	var $chkbox_all = $('tbody input[type="checkbox"]', $table);
	var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
	var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

	// If none of the checkboxes are checked
	if ($chkbox_checked.length === 0) {
		chkbox_select_all.checked = false;
		if ('indeterminate' in chkbox_select_all) {
			chkbox_select_all.indeterminate = false;
		}
		// If all of the checkboxes are checked
	} else if ($chkbox_checked.length === $chkbox_all.length) {
		chkbox_select_all.checked = true;
		if ('indeterminate' in chkbox_select_all) {
			chkbox_select_all.indeterminate = false;
		}
		// If some of the checkboxes are checked
	} else {
		chkbox_select_all.checked = true;
		if ('indeterminate' in chkbox_select_all) {
			chkbox_select_all.indeterminate = true;
		}
	}
	if (rowsSelected.length != 0) {
		$("#bulkMail").removeClass("disabled");
		$('#copyInfluencers').parent().removeClass('disabled');
		$('#copyInfluencers').css("color", "rgb(47, 0, 141)");
		$('#copyInfluencers').css("cursor", "pointer");
	} else {
		$('#bulkMail').addClass('disabled');
		$('#copyInfluencers').parent().addClass('disabled');
		$('#copyInfluencers').css("color", "rgb(227, 226, 226)");
		$('#copyInfluencers').css("cursor", "");
	}
}

$('#copyInfluencers').parent().addClass('disabled');
$('#copyInfluencers').css("color", "rgb(227, 226, 226)");
$('#copyInfluencers').css("cursor", "");

var paymentTypeInsta = '<option value="" hidden="true">Select payment type</option>'+
					'<option value="Cash">Cash</option>'+
					'<option value="Product/Service">Product/Service</option>'+
					'<option value="Shoutout">Shoutout</option>'+
					'<option value="Affiliate">Affiliate</option>';

var paymentTypeYT = '<option value="" hidden="true">Select payment type</option>'+
					'<option value="Cash">Cash</option>'+
					'<option value="Product/Service">Product/Service</option>'+
					'<option value="Affiliate">Affiliate</option>';

var postTypeInsta = '<option value="" hidden="true">Select post type</option>'+
					'<option value="Image">Image</option>'+
					'<option value="Video">Video</option>'+
					'<option value="IGTV">IGTV</option>'+
					'<option value="Story">Story</option>';

var postTypeYoutube = '<option value="" hidden="true">Select post type</option>'+
					'<option value="IntegratedVideo">Integrated Video</option>'+
					'<option value="DedicatedVideo">Dedicated Video</option>'+
					'<option value="PostRoll">Post Roll</option>'+
					'<option value="Intro">Intro</option>'+
					'<option value="Shoutout">Shout Out</option>'+
					'<option value="Other">Other</option>';

var postTypeTiktok = '<option value="" hidden="true">Select post type</option>'+
					'<option value="SponsoredPost">Sponsored Post</option>';

var postDurationInsta = '<option value="" hidden="true">Select post duration</option>'+
					'<option id="2H" class="story" value="2H" hidden="hidden">2 Hours</option>'+
					'<option id="6H" class="story" value="6H" hidden="hidden">6 Hours</option>'+
					'<option id="12H" class="story" value="12H" hidden="hidden">12 Hours</option>'+
					'<option id="24H" class="story image video" value="24H" hidden="hidden">24 Hours</option>'+
					'<option id="1W" class="image video" value="1W" hidden="hidden">1 Week</option>'+
					'<option id="P" class="igtv image video" value="P" hidden="hidden">Permanent</option>';

// Handle click on checkbox
//$('#table_campaign tbody').on('click', 'input[type="checkbox"]', function(e) {
//	var $row = $(this).closest('tr');
//	// Get row data
//	var data = campaignDT.row($row).data();
//	// Get row ID
//	var rowId = data.DT_RowId;
//	// Determine whether row ID is in the list of selected row IDs
//	var index = $.inArray(rowId, rowsSelected);
//	// If checkbox is checked and row ID is not in list of selected row IDs
//	if (this.checked && index === -1) {
//		rowsSelected.push(rowId);
//		// Otherwise, if checkbox is not checked and row ID is in list of
//		// selected
//		// row IDs
//	} else if (!this.checked && index !== -1) {
//		rowsSelected.splice(index, 1);
//	}
//	if (this.checked) {
//		$row.addClass('selected');
//	} else {
//		$row.removeClass('selected');
//	}
//	// Update state of "Select all" control
//	updateDataTableSelectAllCtrl(campaignDT);
//	// Prevent click event from propagating to parent
//	e.stopPropagation();
//});

$('#campaignName').on('keyup', function() {
	var campaignName = $('#campaignName').val();

	if (campaignName.indexOf("'") != -1 && campaignName.indexOf('"') != -1) {
		$('#campaignNameError').text("(\') and (\") Together Not Used As Campaign Name");
		$('.md-form input[type=text]:focus:not([readonly])').css({"border-bottom":"1px solid #dc3545","box-shadow":"0 1px 0 0 #dc3545"});
		$('.save-campaign').addClass("disabled");
		$('.save-campaign').css("margin-top","-2.5rem");
	} else {
		$('#campaignNameError').text("");
		$('.md-form input[type=text]:focus:not([readonly])').css({"border-bottom":"","box-shadow":""});
		$('.save-campaign').removeClass("disabled");
		$('.save-campaign').css("margin-top","");
	}
});

// Handle click on table cells with checkboxes
$('#table_campaign').on('click', 'tbody td, thead th:first-child', function(e) {
//	$(this).parent().find('input[type="checkbox"]').trigger('click');
});

// Handle click on "Select all" control
//$('thead input[name="select_all"]', campaignDT.table().container()).on(
//	'click',
//	function(e) {
//		if (this.checked) {
//			$('tbody input[type="checkbox"]:not(:checked)',
//					campaignDT.table().container()).trigger('click');
//		} else {
//			$('tbody input[type="checkbox"]:checked',
//					campaignDT.table().container()).trigger('click');
//		}
//		// Prevent click event from propagating to parent
//		e.stopPropagation();
//	});

// Handle table draw event


$(document).ready(function() {
	$("#table_campaign").on("click", ".deleteMCampaign", function(event){
		event.stopPropagation();
		event.stopImmediatePropagation();
		let campId = $(event.target).closest('span').attr("id").split("_")[0];
		deleteCampId = campId;
		$('#deleteCampaignModal').modal('show'); 
	 });

	// $(".deleteMCampaign").on('click', function(event){
	// 	event.stopPropagation();
	// 	event.stopImmediatePropagation();
	// 	let campId = $(event.target).closest('span').attr("id").split("_")[0];
	// 	deleteCampId = campId;
	// 	$('#deleteCampaignModal').modal('show'); 
		
	// });

	// $('#deleteCampaignModal').on('show.bs.modal', function(e) {
	// 	var campId = $(e.relatedTarget).data('camp-id');
	// 	$("#dltCampSpnId").val(campId);
	// 	// $(e.currentTarget).find('input[name="bookId"]').val(campId);
	// });

	$('#deleteCampaignModal').on('hidden.bs.modal', function () {
		deleteCampId = "";
	});

	// var deleteCampaignMessage = 'Are you sure that you want to delete this Campaign? This will delete all influencers in the campaign..!';
	// if (profileDetails.userStatus != 'FREE') {
	// 	$('#copyInfluencerBtn').removeClass('d-none');
	// 	$('#deleteCampaignBtn').removeClass('d-none');
	// 	$('#openUploadInfluencersModal').removeClass('d-none');
	// }
	// if (influencerCount <= 0 || influencerCount == null) {
	// 	deleteCampaignMessage = deleteCampaignMessage.substring(0, deleteCampaignMessage.lastIndexOf('?') + 1);
	// 	$('#openExportToCsvModal').parent().addClass('disabled');
	// 	$('#openExportToCsvModal').css("color", "rgb(227, 226, 226)");
	// 	$('#openExportToCsvModal').css("cursor", "");
	// 	$('#viewAnalytics').parent().addClass('disabled');
	// 	$('#viewAnalytics').css("color", "rgb(227, 226, 226)");
	// 	$('#viewAnalytics').css("cursor", "");
	// }

	// $('#deleteCampaignMessage').html(deleteCampaignMessage);
});

function deleteCampaignOnModal() {

	var request = $.ajax({
				url:`/brand/delete-campaign?id=${deleteCampId}&status=DELETE`,
				type: "DELETE",
			}).done(function (data) {
				if(data?.code == 600){
					toast("Your Campaign was Deleted Successfully");
					$("#deleteCampaignModal").modal('hide');
					window.location.href = "/brand/campaign"
				}else {
		 			// {"code":"654","data":null,"errors":{"message":"Campaign not in creation"}}
					if(data?.errors?.message){
						toast(data.errors.message);
					}else{
						toast("Something went wrong, Please try again");
					}  
				}
				
				// $("#campaignForm").trigger("reset");
			}).fail(function (jqXHR, textStatus) {
				// alert("Request failed: ", jqXHR);
				toast("Request Failed "+textStatus,"error")
			});
}

$('.expand-campaign-bid').click(function() {
	var tr = $(this).closest('tr');
	var row = campaignDT.row(tr);

	if (row.child.isShown()) {
		// This row is already open - close it
		row.child.hide();
		tr.removeClass('shown');
	} else {
		// Open this row
		var content = bindBidForCampaign($(this).attr(
				'data-handle'));
		if (content) {
			row.child(content).show();
			row.child().css({
				background : 'rgba(181, 181, 181, 0.25)'
			})
			tr.addClass('shown');
			$('.carousel').carousel();
			$('.carousel').carousel('pause');
			$('.carousel-control-prev').click(function() {
				row.child().find('.carousel').carousel('prev');
			});
			$('.carousel-control-next').click(function() {
				row.child().find('.carousel').carousel('next');
			});
		} else {
			row.child('<div class="text-center w-100">No bid submitted</div>')
					.show();
			row.child().css({
				background : 'rgba(181, 181, 181, 0.25)'
			})
			tr.addClass('shown');
		}
	}
})

$('#bulkMailTemplate').change(function() {
	$('#bulkMailTemplateInvalid').addClass('d-none');
})

function sendBulkMail() {
	var selectedTemplateId = $('#bulkMailTemplate').val();
	var campaignId = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);

	if (selectedTemplateId == '' || selectedTemplateId == undefined) {
		$('#bulkMailTemplateInvalid').text('Select Mail Template');
		$('#bulkMailTemplateInvalid').removeClass('d-none');
		return;
	}
	$('#sendBulk').addClass('disabled');
	$.post({
		url : window.location.origin + "/brand/view-campaign/send-bulk-mail",
		traditional : true,
		data : {
			campaignId		: campaignId
			,influencerIds	: rowsSelected
			,templateId		: selectedTemplateId
		},
	}).done(function(data) {
		toast("Your E-mail Was Sent Successfully");
		$('#sendBulk').removeClass('disabled');
		$('#table_campaign tbody tr').removeClass('selected');
		$('.dt-body-center input').prop('checked', false);
		$('#bulkMail').addClass('disabled');
		rowsSelected = [];
		$(".close").click();
	}).fail(function(data) {
		$('#sendBulk').removeClass('disabled');
		toast('Failed to Send Mail', 'error');
		$('#bulkMail').addClass('disabled');
		rowsSelected = [];
	});
}

$('#bulkMail').addClass('disabled');

var clickedStatus;
var openedInfluencerJson = {};

function bindBidForCampaign(d) {
	var content = '<div class="col-sm row child-table-campaign w-100" >';
	content += '<div id="corousel-' + d
			+ '" class="carousel slide w-100" data-ride="carousel">';

	content += '<div class="carousel-inner row w-100 mx-auto">';
	var i = 0;
	for (var x in bids) {

		var bid = bids[x];
		if (bid.influencerHandle != d) {
			continue;
		}
		// bid.isPostUrlPresent=false;
		if (!openedInfluencerJson[bid.influencerHandle]) {
			openedInfluencerJson[bid.influencerHandle] = {};
		}
		if (!openedInfluencerJson[bid.influencerHandle][bid.bidId]) {
			openedInfluencerJson[bid.influencerHandle][bid.bidId] = bid;
		} else {
			bid = openedInfluencerJson[bid.influencerHandle][bid.bidId];
		}

		if ((i % 3) == 0) {
			content += '<div  class="carousel-item row '
					+ (i == 0 ? 'active' : '') + '" >'
		}

		var postData;
		for (var y in campaignPostLinkDetails) {

			if (campaignPostLinkDetails[y].bidId == bid.bidId) {
				postData = campaignPostLinkDetails[y];
			}
		}

		content += '<div class="d-inline-block px-2 py-3 col-sm-4"> <div class="card " >'
				+ '<div class="card-body" >'
				+ '<table class="w-100"><thead>'
				+ '<tr class="w-100 text-primary"><th class="w-50">Status</th><th class="w-50 text-center">'
				+ '<select id="status-'
				+ bid.bidId
				+ '" data-bid="'
				+ bid.bidId
				+ '" data-influencerId="'
				+ bid.influencerId
				+ '" data-influencer="'
				+ bid.influencerHandle
				+ '"  name="status" onclick="clickedStatus=this.value;"  onchange="saveStatus(this)">'
				+ '<option value="" hidden>Select status</option>'
				+ '<option value="BIDSENT" '
				+ ('BIDSENT' == bid.bidStatus ? 'selected' : '')
				+ '>Bid	Sent</option>'
				+ '<option value="EMAILFOLLOWUP" '
				+ ('EMAILFOLLOWUP' == bid.bidStatus ? 'selected' : '')
				+ '>Email Follow-up</option>'
				+ '<option value="PAIDINFLUENCER" '
				+ ('PAIDINFLUENCER' == bid.bidStatus ? 'selected' : '')
				+ '>Paid Influencer</option>'
				+ '<option value="POSTED" '
				+ ('POSTED' == bid.bidStatus ? 'selected' : '')
				+ '>Posted</option>'
				+ '<option value="NOTACTIVE" '
				+ ('NOTACTIVE' == bid.bidStatus ? 'selected' : '')
				+ '>Not Active</option>'
				+ '</select></th></tr></thead>'
				+ '<tbody class="w-100">'
				+ '<tr class="w-100"><td class="w-50">Post type</td><td class="w-50 text-center">'
				+ getPostType(bid.postType)
				+ '</td></tr>';
				if (platformCard == "instagram") {
							content += '<tr class="w-100"><td class="w-50">Post duration</td><td class="w-50 text-center">'
									+ bid.postDuration
									+ '</td></tr>';
				}
				content += '<tr class="w-100"><td class="w-50">Payment type</td><td class="w-50 text-center">'
				+ bid.paymentType
				+ '</td></tr>'
				+'<tr class="d-none"><td><input id="platform-' + bid.influencerName.slice(1) + '" value="' + platformCard + '"></td></tr>'
				+ '<tr class="w-100"><td class="w-50">'
				+ bid.bidAmountName
				+ '</td><td class="w-50 text-center">'
				+ bid.bidAmount
				+ '</td></tr>'
				+ '<tr class="w-100"><td class="w-50">Submitted on</td><td class="w-50 text-center">'
				+ moment(bid.bidDateTime).format('MMM DD, YYYY HH:mm')
				+ '</td></tr>'
				+ '<tr class="w-100"><td class="w-50">'
				+ (bid.isPostUrlPresent ? 'Post Details' : 'Track Post')
				+ '</td><td class="w-50 text-center">'
				+ (bid.isPostUrlPresent ? ('<div style="position: relative;height: 24px;"><span style="overflow: hidden;white-space:nowrap;text-overflow:ellipsis;width: 80%;display:inline-block;position: relative;top: 3px;" title="'
						+ postData.postName
						+ '">'
						+ postData.postName
						+ '</span>'
						+ '<a style="position: relative;float: right;color: #2f008d;font-size: 1.4rem;height: 10px !important;top: -4px;" href="'
						+ postData.postURL + '" target="_blank"><i class="fas fa-link"></i></a></div>')
						: '<div style="position: relative;height: 24px;"><span id="addPost" data-influencerId="'
								+ bid.influencerId
								+ '" data-bidId="'
								+ bid.bidId
								+ '" onclick="openAddPostModal(this)" class="'+bid.bidDateTime+'" style="cursor: pointer;block-size: 1.3rem;" title="post tracking"><img id="addPostIcon" src="/static/img/plus-square.svg" style="width: 1.2rem;"></span></div>')
				+ '</td></tr>' + '</tbody></table>'

				+ '</div></div></div>'
		if ((i % 3) == 2 || Number(x) == bids.length - 1) {
			content += '</div>';
		}
		i++;
	}
	if (i == 0) {
		return false;
	}

	content += '</div></div><a class="carousel-control-prev" href="#corousel-'
			+ d + '" role="button" data-slide="prev">'
			+ '<i class="fas fa-chevron-left"></i>'
			+ '<span class="sr-only">Previous</span>' + '</a>'
			+ '<a class="carousel-control-next" href="#corousel-' + d
			+ '" role="button" data-slide="next">'
			+ '<i class="fas fa-chevron-right"></i>'
			+ '<span class="sr-only">Next</span>' + '</a></div>'
	return content;
}

function createNewCampaign() {
	$('.new-campaign').removeClass('scale-down-center');
	$('.new-campaign').addClass('scale-up-center')
	$('.new-campaign').removeClass('d-none');
}

$(document).scroll(function() {
	if ($(this).scrollTop() > 150) {
		$('.dashboard-title').addClass('min-dashboard-title');
	} else {
		$('.dashboard-title').removeClass('min-dashboard-title');
	}
});

function closeNewCampaign() {
	$('.new-campaign').removeClass('scale-up-center');
	$('.new-campaign').addClass('scale-down-center');
	var delayInMilliseconds = 200; // 1 second

	setTimeout(function() {
		$('.new-campaign').addClass('d-none');
	}, delayInMilliseconds);
}

function formCallBack(obj) {
	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');

	$('.save-campaign').addClass('disabled');
	xhr.done(function(d, s, r) {
		toast('Campaign saved successfully');
		location.reload();
	});
	xhr.fail(function(d, s, r) {
		$('.save-campaign').removeClass('disabled');
		if (!d.responseText == '') {
			toast(d.responseText, 'error');
		} else {
			toast('Failed to save campaign', 'error');
		}
	});
}

function saveStatus(obj) {
	$(obj).prop('disabled', true);

	var status = $('#' + obj.id).val();
	$(obj).val(clickedStatus);
	var bidId = $(obj).attr('data-bid');
	var influencerId = $(obj).attr('data-influencerId');
	var influencerHandle = $(obj).attr('data-influencer');

	$.post(
			{
				url : window.location.origin + window.location.pathname
						+ '/save-status',
				data : {
					status			: status
					,bidId			: bidId
					,influencerId	: influencerId
				},
			}).done(function(data) {
		openedInfluencerJson[influencerHandle][bidId].bidStatus = status;
		$(obj).prop('disabled', false);
		toast("Status saved successfully");

		$(obj).val(status)
	}).fail(function() {
		$(obj).prop('disabled', false);
		toast('Failed to save status', 'error');
	});
}

function setLocalInfluencerId(influencerId) {
	influencerID = influencerId;
}

function removeInfluencer() {
	$('#removeInfluencer').modal('hide');
	$.post({
		url : window.location.origin + window.location.pathname + '/remove-influencer',
		data : {
			influencerId	: influencerID
		},
	}).done(function(data) {
		SUCCESS("Influencer removed successfully");
		location.reload();
	}).fail(function() {
		ERROR('Failed to Remove Influencer');
	});
}

var influencerID;
var influencerPricing;
var influencerHandle;

function setInfluencerId(influencerId, pricing, influencerName, platform) {

	$("#paymentType").empty();
	$("#postType").empty();
	$("#postDuration").empty();
	$('#payment-non-cash').hide();
	influencerID = influencerId;
	influencerPricing = pricing;
	influencerHandle = influencerName;
	$('#estimatedPrice').text('$' + Number(influencerPricing).toFixed(2));
	resetForms();
	$('#platform-submit-bid').val(platform);

	if (platform == "instagram") {
		$("#paymentType").append(paymentTypeInsta);
		$("#postType").append(postTypeInsta);
		$("#postDuration").append(postDurationInsta);
		$('#postDuration').removeClass('disabled');
		$('.yt').removeClass('col-md-4');
		$('.yt').addClass('col-md-3');
		$('#bidPostDuration').addClass("col-md-3 md-form pl-0 d-inline-block");
		$('#bidPostDuration').removeClass("d-none");
		$('#pricing').removeClass('d-none');
	}
	if (platform == "youtube" || platform == "tiktok") {
		$("#paymentType").append(paymentTypeYT);
		$('#postDuration').empty();
		$('#postDuration').addClass('disabled');
		$('#bidPostDuration').removeClass("col-md-3 md-form pl-0 d-inline-block");
		$('#bidPostDuration').addClass("d-none");
		$('.yt').removeClass('col-md-3');
		$('.yt').addClass('col-md-4');
		$('#pricing').addClass('d-none');

		if (platform == "youtube") {
			$("#postType").append(postTypeYoutube);
		} else {
			$("#postType").append(postTypeTiktok);
		}
	}
}

$('#postType').change(function(event) {
	$('#postType').removeClass("border-danger");
	$('#postTypeInvalid').empty();

	if ($('#postType').val() == "Story") {
		$('#bid').removeClass("border-danger");
		$('#bidAmountInvalid').empty();
	}
});

$('#postDuration').change(function(event) {
	$('#postDuration').removeClass("border-danger");
	$('#postDurationInvalid').empty();

	if ($('#postDuration').val() == "24H" || $('#postDuration').val() == "1W") {
		$('#bid').removeClass("border-danger");
		$('#bidAmountInvalid').empty();
	}
});

$('#paymentTypeModal')
.change(
	function() {

		if (paymentType != undefined || paymentType != '') {
			$('#paymentTypeModalInvalid').addClass('d-none');
		}

		if ($('#paymentTypeModal').val() == 'Affiliate') {
			$('#trackPaymentMode').removeClass("d-none");
			$('#trackPaymentMode').addClass("col-md-12 md-form p-0 d-inline-block");

			$('#trackAffiliatePercentage').removeClass("d-none");
			$('#trackAffiliatePercentage').addClass("col-md-12 md-form p-0 d-inline-block");

			// this is also follow
			$('#trackBidAmount').removeClass("col-md-12 md-form p-0 d-inline-block");
			$('#trackBidAmount').addClass("d-none");
			// track post type
			$('#trackPostType').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostType').addClass("d-none");
			// track post duration
			$('#trackPostDuration').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostDuration').addClass("d-none");
		} else {
			$('#trackPaymentMode').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPaymentMode').addClass("d-none");

			$('#trackAffiliatePercentage').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackAffiliatePercentage').addClass("d-none");

			$('#trackAffiliateFee').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackAffiliateFee').addClass("d-none");

			// fallow this
			$('#trackBidAmount').addClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackBidAmount').removeClass("d-none");
			// track post type
			$('#trackPostType').addClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostType').removeClass("d-none");
			// track post duration
			$('#trackPostDuration').addClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostDuration').removeClass("d-none");
		}
		if ($("#platformModal").val() != 'instagram') {
			$('#trackPostDuration').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostDuration').addClass("d-none");
		}
	});

$('#trackPaymentMode')
.change(
	function() {
		if ($('#paymentModeModal').val() == '% of Sales') {
			$('#trackAffiliatePercentage').removeClass("d-none");
			$('#trackAffiliatePercentage').addClass("col-sm-12 md-form pl-0 d-inline-block");

			$('#trackAffiliateFee').removeClass("col-sm- md-form pl-0 d-inline-block");
			$('#trackAffiliateFee').addClass("d-none");
		}else{
			$('#trackAffiliateFee').removeClass("d-none");
			$('#trackAffiliateFee').addClass("col-sm-12 md-form pl-0 d-inline-block");

			$('#trackAffiliatePercentage').removeClass("col-sm- md-form pl-0 d-inline-block");
			$('#trackAffiliatePercentage').addClass("d-none");
		}
	});

var trackInfluencerId;
var trackPostName;

$("#postTrackingSubmit").click(function() {
	var postTrackInfluencerId = trackInfluencerId;
	var postName = $("#postNameModal").val();
	var paymentType = $("#paymentTypeModal").val();
	var postType = $("#postTypeModal").val();
	var postDuration = $("#postDurationModal").val();
	var paymentMode = $("#paymentModeModal").val();
	var affiliateFee = $("#affiliateFeeModal").val();
	var affiliatePercentage = $("#affiliatePercentageModal").val();
	var bidAmount = $("#bidAmountModal").val()
	var postUrl = $("#postUrlModal").val();
	var bidId = null;
	var isNewOne = isNewTrack;
	var platform = $("#platformModal").val();

	if (postTrackInfluencerId == undefined || postTrackInfluencerId == '') {
		postTrackInfluencerId = addPostInfluencerId;
	}

	if (!isNewOne) {
		bidId = addPostBidId;
	}

	var campaignId = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);

	if (postName == undefined || postName == '') {
		$('#postNameModalInvalid').text('Enter Post Name');
		$('#postNameModalInvalid').removeClass('d-none');
		return;
	}

	if (paymentType == undefined || paymentType == '') {
		$('#paymentTypeModalInvalid').text('Select Payment Type');
		$('#paymentTypeModalInvalid').removeClass('d-none');
		return;
	}

	if (paymentType == "Affiliate") {
		postType = null;
		postDuration = null;

		if (paymentMode == undefined || paymentMode == '') {
			$('#paymentModeModalInvalid').text('Select Payment Mode');
			$('#paymentModeModalInvalid').removeClass('d-none');
			return;
		}
		if (paymentMode == "$ per Sale" && (affiliateFee == undefined || affiliateFee == '')) {
			$('#affiliateFeeModalInvalid').text('Enter Fee');
			$('#affiliateFeeModalInvalid').removeClass('d-none');
			return;
		}
		// percentage
		if (paymentMode == "% of Sales" && (affiliatePercentage == undefined || affiliatePercentage == '')) {
			$('#affiliatePercentageModalInvalid').text('Enter Percentage');
			$('#affiliatePercentageModalInvalid').removeClass('d-none');
			return;
		}
		if (paymentType == "Affiliate" && paymentMode == "$ per Sale") {
			bidAmount = affiliateFee;
			paymentMode = "$";
		} else if (paymentType == "Affiliate" && paymentMode == "% of Sales") {
			bidAmount = affiliatePercentage;
			paymentMode = "%";
		}
	} else {
		paymentMode = null;

		if (postType == undefined || postType == '') {
			$('#postTypeModalInvalid').text('Select Post Type');
			$('#postTypeModalInvalid').removeClass('d-none');
			return;
		}
		if (platform == 'instagram' && (postDuration == undefined || postDuration == '')) {
			$('#postDurationModalInvalid').text('Select Post Duration');
			$('#postDurationModalInvalid').removeClass('d-none');
			return;
		}
		if (postDuration == "2 hours") {
			postDuration = "2H";
		}
		else if (postDuration == "6 hour") {
			postDuration = "6H";
		}
		else if (postDuration == "12 hour") {
			postDuration = "12H";
		}
		else if (postDuration == "24 hour") {
			postDuration = "24H";
		}
		else if (postDuration == "1 week") {
			postDuration = "1W";
		}
		else if (postDuration == "Permanent") {
			postDuration = "P";
		}
		if (bidAmount == undefined || bidAmount == '') {
			$('#bidAmountModalInvalid').text('Enter Bid Amount');
			$('#bidAmountModalInvalid').removeClass('d-none');
			return;
		}
	}

	if (postUrl == undefined || postUrl == '') {
		$('#postUrlModalInvalid').text('Enter Post Url');
		$('#postUrlModalInvalid').removeClass('d-none');
		return;
	}

	$.post({
		url : window.location.origin + window.location.pathname + "/save-post-link",
		data : {
			influencerId	: postTrackInfluencerId
			,paymentType	: paymentType
			,paymentMode	: paymentMode
			,postType		: postType
			,postDuration	: postDuration
			,bidId			: bidId
			,bidAmount		: bidAmount
			,postURL		: postUrl
			,postName		: postName
			,isNew			: isNewOne
			,campaignId		: campaignId
			,platform		: platform
		},
	}).done(function(data) {
		$('#postTrackingModal').modal('hide');

		toast('Post has been added successfully for tracking');
		$(".close").click();

		var delayInMilliseconds = 2000;

		setTimeout(function() {
			location.reload();
		}, delayInMilliseconds);
	}).fail(function(data) {
		$('#postTrackingModal').modal('hide');
		if (data.status == 400) {
			toast('Falied to add post, please try again later.', 'error');
		} else if (data.status != 500) {
			toast(data.responseText, 'error');
		} else {
			toast('Falied to add post, please try again later.', 'error');
		}
	});
});

$('#paymentType')
	.change(
		function(event) {
			$('#paymentType').removeClass("border-danger");
			$('#bid').removeClass("border-danger");
			$('#bidAmountInvalid').empty();
			$('#paymentTypeInvalid').empty();

			if ($('#paymentType').val() == 'Product/Service'
					|| $('#paymentType').val() == 'Shoutout') {
				$('.not-affiliate').show();
				$('.not-affiliate').addClass('d-inline-block');
				$('.affiliate').addClass('col-md-3');
				$('.affiliate').removeClass('col-md-4');
				$('#payment-non-cash')
						.html(
								"Please include the cash equivalent value of the product, service or shoutout you are providing in the bid amount.")
						.show();
				$('#affiliateSuffixDiv').addClass('d-none');
				$('#bidAmountLabel').html('Bid Amount in $');
			} else if ($('#paymentType').val() == 'Affiliate') {
				$('#bid').val(0);
				$('.not-affiliate').hide();
				$('.not-affiliate').removeClass('d-inline-block');
				$('.affiliate').removeClass('col-md-3');
				$('.affiliate').addClass('col-md-4');
				$('#payment-non-cash')
						.html(
								"Smartfluence will recommend an affiliate post schedule for the influencer. Please select whether you will be paying as a % of sales or $ per sale.")
						.show();
				$('#affiliateSuffixDiv').removeClass('d-none');
				$('#bidAmountLabel').html('Affiliate %');
			} else {
				$('.not-affiliate').show();
				$('.not-affiliate').addClass('d-inline-block');
				$('.affiliate').addClass('col-md-3');
				$('.affiliate').removeClass('col-md-4');
				$('#payment-non-cash').hide();
				$('#affiliateSuffixDiv').addClass('d-none');
				$('#bidAmountLabel').html('Bid Amount in $');
			}
			$('#mailTemplate option').attr('hidden', 'hidden');
			$('#mailTemplate .true').removeAttr('hidden');

			if ($('#paymentType').val() == 'Affiliate') {
				if ($('#affiliateType').val() == '%') {
					$('#mailTemplate .Affiliate.percentage')
							.removeAttr('hidden');
					$('#mailTemplate .Affiliate.percentage').prop(
							'selected', true);
				} else if ($('#affiliateType').val() == '$') {
					$('#mailTemplate .Affiliate.fee').removeAttr(
							'hidden');
					$('#mailTemplate .Affiliate.fee').prop('selected',
							true);
				}
			} else if ($('#paymentType').val() == 'Product/Service') {
				$('#mailTemplate .Product').removeAttr('hidden');
				$('#mailTemplate .Product').prop('selected', true);
			} else {
				$('#mailTemplate '
						+ (this.value.trim() == '' ? '' : '.'
								+ this.value.trim().replace(
										/ /g, '_')))
						.removeAttr('hidden');
				$('#mailTemplate '
						+ (this.value.trim() == '' ? '' : '.'
								+ this.value.trim().replace(
										/ /g, '_'))).prop(
						'selected', true);
			}
			$('#mailTemplate').val('').change();
			$('#mailTextArea').val('');
			$('#mailSubject').val('');
			if ($("#platform-submit-bid").val() != 'instagram') {
				$('#bidPostDuration').removeClass('d-inline-block');
				$('.yt').removeClass('col-md-3');
				$('.yt').addClass('col-md-4');
			}
		});

$('#affiliateType')
	.change(
		function(event) {
			var affiliateType = $('#affiliateType').val();

			if (affiliateType == "%") {
				$('#bidAmountLabel').html('Affiliate %');
			} else if (affiliateType == "$") {
				$('#bidAmountLabel').html('Affiliate Fee');
			}
			$('#bid').removeClass("border-danger");
			$('#bidAmountInvalid').empty();
			$('#mailTemplate option').attr('hidden', 'hidden');
			$('#mailTemplate .true').removeAttr('hidden');

			if ($('#paymentType').val() == 'Affiliate') {
				if ($('#affiliateType').val() == '%') {
					$('#mailTemplate .Affiliate.percentage')
							.removeAttr('hidden');
				} else if ($('#affiliateType').val() == '$') {
					$('#mailTemplate .Affiliate.fee').removeAttr(
							'hidden');
				}
			} else if ($('#paymentType').val() == 'Product/Service') {
				$('#mailTemplate .Product').removeAttr('hidden');
			} else {
				$('#mailTemplate '
						+ (this.value.trim() == '' ? '' : '.'
								+ this.value.trim().replace(
										/ /g, '_')))
						.removeAttr('hidden');
			}
			$('#mailTemplate').val('').change();
			$('#mailTextArea').val('');
			$('#mailSubject').val('');
		});

$('#bid')
	.on(
		'input',
		function() {
			var value = $('#bid').val();

		 if ($("#platform-submit-bid").val() == "instagram") {
				if ($('#postType').val() == ""
						|| ($('#postType').val() == "Image" && ($(
								'#postDuration').val() == "24H" || $(
								'#postDuration').val() == "1W"))) {

					if ($('#paymentType').val() == "Affiliate") {
						var affiliateType = $('#affiliateType').val();

						if (affiliateType == "%"
								&& (value <= 0 || value > 100)) {
							$('#bid').addClass("border-danger");
							$('#bidAmountInvalid').html(
									"Enter a valid percentage");
							bidBoolean = false;
						} else if (affiliateType == "$" && (value <= 0)) {
							$('#bid').addClass("border-danger");
							$('#bidAmountInvalid')
									.html("Enter a valid fee");
							bidBoolean = false;
						} else {
							$('#bid').removeClass("border-danger");
							$('#bidAmountInvalid').empty();
						}
					} else {
						var value = $('#bid').val();
						var bid = document.getElementById("bid");
						bid.setCustomValidity('Enter the bid amount');
						if (value == "" || Number(value) <= 0) {
							$('#bid').addClass("border-danger");
							$('#bidAmountInvalid').html(
									bid.validationMessage);
						} else {
							$('#bid').removeClass("border-danger");
							$('#bidAmountInvalid').empty();
						}
					}
				} else {
					if ($('#paymentType').val() == "Affiliate") {
						var affiliateType = $('#affiliateType').val();
						if (affiliateType == "%"
								&& (value <= 0 || value > 100)) {
							$('#bid').addClass("border-danger");
							$('#bidAmountInvalid').html(
									"Enter a valid percentage");
							bidBoolean = false;
						} else if (affiliateType == "$" && (value <= 0)) {
							$('#bid').addClass("border-danger");
							$('#bidAmountInvalid')
									.html("Enter a valid fee");
							bidBoolean = false;
						} else {
							$('#bid').removeClass("border-danger");
							$('#bidAmountInvalid').empty();
						}
					} else {
						var value = $('#bid').val();
						var bid = document.getElementById("bid");
						var minimumAmount = (influencerPricing * 0.75)
								.toFixed(0);
						bid.setCustomValidity('Your Bid should be greater than 0$');
						if (Number(value) > 0) {
							$('#bid').removeClass("border-danger");
							$('#bidAmountInvalid').empty();
						} else {
							$('#bid').addClass("border-danger");
							$('#bidAmountInvalid').html(
									bid.validationMessage);
						}
					}
				}
			}
		})

function submitBid() {
	var postTypeBoolean = true;
	var postDurationBoolean = true;
	var bidBoolean = true;
	var paymentTypeBoolean = true;
	var campaignBoolean = true;

	var postType = document.getElementById("postType");
	var postDuration = document.getElementById("postDuration");
	var paymentType = $('#paymentType').val();
	var bidAmount = $('#bid').val();
	var affiliateType = $('#affiliateType').val();

	var mailTemplate = $("#mailTemplate").val();
	var mailDescription = $('#mailTextArea').val();
	var mailSubject = $('#mailSubject').val();
	var mailCc = $('#mailCc').val();
	mailCc = mailCc.split(' ').join(',');
	var paymentTypeMessage = 'Select Payment Type';
	var platform = $('#platform-submit-bid').val();

	if (paymentType == '') {
		$('#paymentType').addClass("border-danger");
		$('#paymentTypeInvalid').html(paymentTypeMessage);
		paymentTypeBoolean = false;
	} else if (paymentType == "Affiliate") {
		var affiliateType = $('#affiliateType').val();
		if (affiliateType == "%" && (bidAmount <= 0 || bidAmount > 100)) {
			$('#bid').addClass("border-danger");
			$('#bidAmountInvalid').html("Enter a valid percentage");
			bidBoolean = false;
		} else if (affiliateType == "$" && (bidAmount <= 0)) {
			$('#bid').addClass("border-danger");
			$('#bidAmountInvalid').html("Enter a valid fee");
			bidBoolean = false;
		}
	}

	if (paymentType != "Affiliate") {
		affiliateType = null;
		postType.setCustomValidity('Select Post Type');
		if (postType.value == '') {
			$('#postType').addClass("border-danger");
			$('#postTypeInvalid').html(postType.validationMessage);
			postTypeBoolean = false;
		}

		if (platform == 'instagram') {
			postDuration.setCustomValidity('Select Post Duration');
			if (postDuration.value == '') {
				$('#postDuration').addClass("border-danger");
				$('#postDurationInvalid').html(postDuration.validationMessage);
				postDurationBoolean = false;
			}
		}

		if ($('#postType').val() == "Story"
				|| ($('#postType').val() == "Image" && ($('#postDuration')
						.val() == "24H" || $('#postDuration').val() == "1W"))) {
			var bid = document.getElementById("bid");
			bid.setCustomValidity('Enter the bid amount');
			if (bidAmount == "" || Number(bidAmount) <= 0) {
				$('#bid').addClass("border-danger");
				$('#bidAmountInvalid').html(bid.validationMessage);
				bidBoolean = false;
			}
		} else {
			var bid = document.getElementById("bid");
			var minimumAmount = (influencerPricing * 0.75).toFixed(0);
			bid.setCustomValidity('Your Bid should be greater than 0$');
			if (Number(bidAmount) > 0) {
				bidBoolean = true;
			} else {
				$('#bid').addClass("border-danger");
				$('#bidAmountInvalid').html(bid.validationMessage);
				bidBoolean = false;
			}
		}
	}

	if (postTypeBoolean && postDurationBoolean && bidBoolean
			&& paymentTypeBoolean && campaignBoolean) {
		$('.cancel').addClass('disabled');
		$('.submit-bid').addClass('disabled');
		$('#submitBid').addClass('disabled');
		var postTypeVal = $('#postType').val();
		var paymentTypeVal = $('#paymentType').val();
		var postDurationVal = platform == 'instagram' ? $('#postDuration').val() : '';

		if (paymentTypeVal == 'Affiliate') {
			postTypeVal = null;
			postDurationVal = null;
		}

		$.post({
				url : window.location.origin + '/brand/view-influencer/submit-bid',
				data : {
					postType					: postTypeVal
					,postDuration				: postDurationVal
					,paymentType				: paymentTypeVal
					,bidAmount					: (parseFloat($('#bid').val())).toFixed(2)
					,affiliateType				: affiliateType
					,campaignId					: window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1)
					,isValid					: true
					,mailTemplateId				: mailTemplate
					,mailDescription			: mailDescription
					,mailSubject				: mailSubject
					,mailCc						: mailCc
					,influencerId				: influencerID
					,influencerHandle			: influencerHandle
					,reportId					: ''
					,reportPresent				: false
					,platform					: platform
				},
		}).done(function(data) {
			$('#SubmitBid').modal('hide');
			$('#submitBid').removeClass('disabled');
			toast(data.data);
			$(".close").click();

			var delayInMilliseconds = 2000;

			setTimeout(function() {
				location.reload();
			}, delayInMilliseconds);
		}).fail(function() {
			$('.cancel').removeClass('disabled');
			$('#submitBid').removeClass('disabled');
			$('.submit-bid').removeClass('disabled');
			$('#SubmitBid').modal('hide');
			toast('Failed to submit bid', 'error');
		});
	}
}

$('#postTypeModal').on('change', function() {
	if (paymentType != undefined || paymentType != '')
	{
		$('#postTypeModalInvalid').addClass('d-none');
	}
	var postType = $('#postTypeModal').val();

	if (postType.length > 0) {
		$('#postDuration').val('');
		$('#postDuration option').attr('hidden', 'hidden');
		$('.' + postType.toLowerCase()).removeAttr('hidden');
	}
})

$('#postDurationModal').on('change',function() {
	if ($('#postDurationModal').val() != undefined || $('#postDurationModal').val() != '')
	{
		$('#postDurationModalInvalid').addClass('d-none');
	}
})

$('#postType').on('change', function() {
	var postType = $('#postType').val();

	if (postType != null && postType != '') {
		$('#postDuration').val('');
		$('#postDuration option').attr('hidden', 'hidden');
		$('.' + postType.toLowerCase()).removeAttr('hidden');
	}
})

$('#postTypeModal').on('change', function() {
	var postType = $('#postTypeModal').val();

	if (postType.length > 0) {
		$('#postDurationModal').val('');
		$('#postDurationModal option').attr('hidden', 'hidden');
		$('.' + postType.toLowerCase()).removeAttr('hidden');
	}
})

function resetFormSubmitBid() {
	$('#invalidCampaign').html('');
	$('.submitBid-tab-label').removeClass('skip');
	$('#payment-non-cash').hide();
	$('.text-danger').html('');
	$('.border-danger').removeClass('border-danger');
	$('#SubmitBid input,#SubmitBid select').val('').change();
	$('#mailTextArea').val('');
	resetForms();
}

var lastSelectedInfluencer;

$('#influencer').on('change', function() {
	var influencerId = $('#influencer').val();
	if (influencerId) {
		$('#bidPostAnalytics').val('');
		$('.' + lastSelectedInfluencer).attr("hidden", "true");
		$('.' + influencerId).removeAttr("hidden");
		lastSelectedInfluencer = influencerId;
		$('#influencerInvalid').addClass('d-none');
	}
})

var addPostInfluencerId;
var addPostBidId;
var isNewTrack;
var cardCount = 0;

function trackPostModal(InfluencerId, InfluencerHandle, platform) {

	trackInfluencerId = InfluencerId;
	trackPostName = InfluencerHandle;
	isNewTrack = true;
	cardCount = 0;

	$("#paymentTypeModal").empty();
	$("#postTypeModal").empty();
	$("#postDurationModal").empty();

	if (platform == "instagram") {
		$("#paymentTypeModal").append(paymentTypeInsta);
		$("#postTypeModal").append(postTypeInsta);
		$("#postDurationModal").append(postDurationInsta);
		$('#postDurationModal').removeClass('disabled');
		$('.yt').removeClass('col-md-4');
		$('.yt').addClass('col-md-3');
		$('#trackPostDuration').addClass("col-md-12 md-form pl-0 d-inline-block");
		$('#trackPostDuration').removeClass("d-none");
	}
	if (platform == "youtube" || platform == "tiktok") {
		$("#paymentTypeModal").append(paymentTypeYT);
		$('#postDurationModal').empty();
		$('#postDurationModal').addClass('disabled');
		$('#trackPostDuration').removeClass("col-md-12 md-form pl-0 d-inline-block");
		$('#trackPostDuration').addClass("d-none");
		$('.yt').removeClass('col-md-3');
		$('.yt').addClass('col-md-4');

		if (platform == "youtube") {
			$("#postTypeModal").append(postTypeYoutube);
		} else {
			$("#postTypeModal").append(postTypeTiktok);
		}
	}

	for(i = 0; i < bids.length; i++) {
		if (trackPostName == bids[i].influencerHandle) {
			cardCount++;
		}
	}

	$('#postNameModal').val(trackPostName + "-" + (cardCount + 1));
	$('#platformModal').val(platform);
}

var platformCard;

function openPostModal(platform) {
	$('#platformModal').val(platform);
	platformCard = platform;
	$("#paymentTypeModal").empty();
	$("#postTypeModal").empty();
	$("#postDurationModal").empty();

	if(platform == "instagram") {
		$("#paymentTypeModal").append(paymentTypeInsta);
		$("#postTypeModal").append(postTypeInsta);
		$("#postDurationModal").append(postDurationInsta);
		$('#postDurationModal').removeClass('disabled');
		$('#trackPostDuration').addClass("col-md-12 md-form pl-0 d-inline-block");
		$('#trackPostDuration').removeClass("d-none");
	}
	if(platform == "youtube" || platform == "tiktok") {
		$("#paymentTypeModal").append(paymentTypeYT);
		$('#postDurationModal').empty();
		$('#postDurationModal').addClass('disabled');
		$('#trackPostDuration').removeClass("col-md-12 md-form pl-0 d-inline-block");
		$('#trackPostDuration').addClass("d-none");
		
		if(platform == "youtube") {
			$("#postTypeModal").append(postTypeYoutube);
		} else {
			$("#postTypeModal").append(postTypeTiktok);
		}
	}
}

function openAddPostModal(element) {
	addPostInfluencerId = $(element).attr("data-influencerId");
	addPostBidId = $(element).attr("data-bidId");
	var bidtime = $(element).attr("class");

	var postName;
	isNewTrack = false;

	$.get({
		url : window.location.origin + window.location.pathname + '/get-influencer-bid-count',
		data : {
			influencerId	: addPostInfluencerId
			,bidId			: addPostBidId
		},
	}).done(function(data) {
		postName = data.postName;
		var result = bids.filter(obj => {
			return (obj.influencerName === postName.slice(0,-2) && obj.bidDateTime == bidtime)
		})

		$('#postNameModal').val(postName);
		$('#paymentTypeModal').val(result[0].paymentType);

		var getplatform = "#platform-" + result[0].influencerName.slice(1);
		$('#platformModal').val($(getplatform).val());

		if (result[0].paymentType == "Affiliate") {

			$('#trackPostType').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostType').addClass("d-none");

			$('#trackPostDuration').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostDuration').addClass("d-none");

			$('#trackBidAmount').removeClass("col-md-12 md-form p-0 d-inline-block");
			$('#trackBidAmount').addClass("d-none");

			$('#trackPaymentMode').removeClass("d-none");
			$('#trackPaymentMode').addClass("col-md-12 md-form p-0 d-inline-block");

			if (result[0].affiliateType == "%") {
				$('#paymentModeModal').val("% of Sales");

				$('#trackAffiliateFee').removeClass("col-sm- md-form pl-0 d-inline-block");
				$('#trackAffiliateFee').addClass("d-none");

				$('#trackAffiliatePercentage').removeClass("d-none");
				$('#trackAffiliatePercentage').addClass("col-md-12 md-form p-0 d-inline-block");

				$('#affiliatePercentageModal').val(result[0].bidAmount.slice(0,-1));
			} else {
				$('#paymentModeModal').val("$ per Sale");

				$('#trackAffiliatePercentage').removeClass("col-sm- md-form pl-0 d-inline-block");
				$('#trackAffiliatePercentage').addClass("d-none");

				$('#trackAffiliateFee').removeClass("d-none");
				$('#trackAffiliateFee').addClass("col-sm-12 md-form pl-0 d-inline-block");

				$('#affiliateFeeModal').val(result[0].bidAmount.slice(1));
			}
		} else {

			$('#trackAffiliateFee').removeClass("col-sm- md-form pl-0 d-inline-block");
			$('#trackAffiliateFee').addClass("d-none");

			$('#trackAffiliatePercentage').removeClass("col-sm- md-form pl-0 d-inline-block");
			$('#trackAffiliatePercentage').addClass("d-none");

			$('#trackPaymentMode').removeClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPaymentMode').addClass("d-none");

			// fallow this
			$('#trackBidAmount').addClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackBidAmount').removeClass("d-none");
			// track post type
			$('#trackPostType').addClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostType').removeClass("d-none");
			// track post duration
			$('#trackPostDuration').addClass("col-sm-12 md-form p-0 d-inline-block");
			$('#trackPostDuration').removeClass("d-none");
			$('#postTypeModal').val(result[0].postType);

			var postType = result[0].postType;
			// removing attribute
			if (postType.length > 0) {
				$('#postDurationModal').val('');
				$('#postDurationModal option').attr('hidden', 'hidden');
				$('.' + postType.toLowerCase()).removeAttr('hidden');
			}
			if ($(getplatform).val() == "instagram") {
				$('#trackPostDuration').addClass("col-sm-12 md-form p-0 d-inline-block");
				$('#trackPostDuration').removeClass("d-none");
			} else {
				$('#trackPostDuration').removeClass("col-sm-12 md-form p-0 d-inline-block");
				$('#trackPostDuration').addClass("d-none");
			}
			$('#postDurationModal').val(result[0].postDuration);
			$('#bidAmountModal').val(result[0].bidAmount.slice(1));
		}
		$('#postTrackingModal').modal('show');
	}).fail(function(data) {
		postName = "Post " + new Date().getTime();
		$('#postName').val(postName);
		$('#postTrackingModal').modal('show');
	});
}

$('#bidPostAnalytics').on('change', function() {
	$('#bidInvalid').addClass('d-none');
})

$('#postNameModal').on('input', function() {
	$('#postNameModalInvalid').addClass('d-none');
})

// const instaRegex = /(https?:\/\/)?www.instagram.com\/p\/[A-Za-z0-9\-\_]{5,}/i
const instaRegex = /instagram.com\/p\/[A-Za-z0-9\-\_]{5,}/i
$('#postUrl').on('input', function() {
	var postUrl = $('#postUrl').val();
	if (postUrl) {
		var validatedPostUrl = instaRegex.exec(postUrl);
		if (validatedPostUrl != null && validatedPostUrl[0]) {
			validatedPostUrl = "https://www." + validatedPostUrl;
			var postURI = new URL(validatedPostUrl);
			$('#postUrl').val(postURI.origin + postURI.pathname);
			$('#postURLInvalid').addClass('d-none');
		} else {
			$('#postURLInvalid').removeClass('d-none');
			$('#postURLInvalid').html('Enter a valid post URL');
		}
	}
})

$("#openUploadInfluencersModal").click(function(event) {
	event.preventDefault();
	$('#uploadInfluencersModal').modal('show');
});

function downloadTemplate() {
	window.open('/brand/view-campaign/download-template/upload-influencers');
	$('#downloadTemplate').addClass('d-none');
	$('#uploadTemplate').removeClass('d-none');
}

$("#browseBtn").click(function() {
	$("#uploadInfluencers").click();
});

$("#uploadErrorBtn").click(function() {
	$('#uploadInfluencersModal').modal('hide');
	window.location.reload();
});

$("#uploadInfluencers").change(function(event) {
	var f = event.target.files[0];

	if (f) {
		var data;
		var r = new FileReader();
		r.readAsBinaryString(f);
		r.onload = e => {
			var result = {};
			var workbook = XLSX.read(e.target.result, {
				type: 'binary'
			});
			workbook.SheetNames.forEach(sheetName => {
				var row = XLSX.utils.make_json(workbook.Sheets[sheetName]);
				if (row.length) result[sheetName] = row;

				data = JSON.stringify(result, 2, 2);
			});

			uploadInfluencersToCampaign(data, workbook.SheetNames);
		}
		$("#uploadInfluencers")[0].value = '';
	} 
});

function uploadInfluencersToCampaign(data, sheets) {
	resetUploadButtons(1);

	$.post({
		url : window.location.origin + window.location.pathname + "/upload-bulk-influencers",
		data : {
			influencerHandles	: data
			,workbookSheets		: sheets.toString()
		},
	}).done(function(response) {
		$("#uploadInfluencers")[0].value = '';

		if (response.influencersNotAdded != null) {
			$("#uploadTemplate").addClass('d-none');
			$("#alreadyAddedError").removeClass('d-none');
			$("#uploadError").removeClass('d-none');
			$("#influencersNotAdded").html(response.influencersNotAdded);
		}
		if (response.influencersNotFound != null) {
			$("#uploadTemplate").addClass('d-none');
			$("#notFoundError").removeClass('d-none');
			$("#uploadError").removeClass('d-none');
			$("#influencersNotFound").html(response.influencersNotFound);
		}
		if (response.influencersPulling != null) {
			$("#uploadTemplate").addClass('d-none');
			$("#tryAgainError").removeClass('d-none');
			$("#uploadError").removeClass('d-none');
			$("#influencersPulling").html(response.influencersPulling);
		} else {
			resetUploadButtons(2);
		}

		if (response.uploadStatus == 'SUCCESS') {
			SUCCESS(response.responseMessage);
		} else {
			ERROR(response.responseMessage);
		}

		if (response.influencersNotAdded == null && response.influencersNotFound == null && response.uploadStatus != 'ERROR') {
			window.location.reload();
		}
	}).fail(function(response) {
		$("#uploadInfluencers")[0].value = '';
		resetUploadButtons(2);
		ERROR("Failed, Please upload valid template");
	});
}

function resetUploadButtons(flag) {
	if (flag == 1) {
		$('#browseBtn').addClass('d-none');
		$("#uploadingBtn").removeClass('d-none');
	}
	else if (flag == 2) {
		$('#browseBtn').removeClass('d-none');
		$("#uploadingBtn").addClass('d-none');
	}
}

var csvName;
function openExportToCsvModal() {
	$('#exportToCsvModal').modal('show');
	$('#fileReadyMessage').removeClass('d-none');
	$('#reportMessageLoader').removeClass('d-none');
	$('#downloadCsv').addClass('d-none');
	exportToCsv();
	return false;
}

function exportToCsv() {
	$('#openExportToCsvModal').addClass('disabled');
	$.get(
		{
			url : window.location.origin + window.location.pathname
					+ '/export-campaign-csv',
		}).done(function(data) {
			csvName = data;
			$('#fileReadyMessage').addClass('d-none');
			$('#reportMessageLoader').addClass('d-none');
			$('#downloadCsv').removeClass('d-none');
			$('#openExportToCsvModal').removeClass('disabled');
		}).fail(function(data) {
			$('#exportToCsvModal').modal('hide');
			$('#openExportToCsvModal').removeClass('disabled');
			toast('Failed to download', 'error');
		});
}

function downloadCSV() {
	window.open(window.location.origin + window.location.pathname
			+ '/download-campaign-csv/' + csvName);
}

function resetExportToCsvModal() {
	$('#openExportToCsvModal').removeClass('disabled');
	$('#fileReadyMessage').removeClass('d-none');
	$('#reportMessageLoader').removeClass('d-none');
	$('#downloadCsv').addClass('d-none');
}

function openSearchInfluencerPage() {
	window.open(window.location.origin + "/brand/explore-influencers");
	return false;
}

function openAnalyticsPage() {
	window.open(window.location.origin + "/brand/analytics");
	var campaignId = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
	if (campaignId) {
		var date = new Date();
		date.setTime(date.getTime() + (10 * 1000));
		var expires = "; expires=" + date.toGMTString();
		document.cookie = "campaignId=" + campaignId + expires + ";path=/";
	}
	return false;
}

function validateSubmitBid() {
	$('#step3Body').html('')
	var postTypeBoolean = true;
	var postDurationBoolean = true;
	var bidBoolean = true;
	var paymentTypeBoolean = true;
	var campaignBoolean = true;
	var postType = document.getElementById("postType");
	var postDuration = document.getElementById("postDuration");
	var paymentType = $('#paymentType').val();
	var bidAmount = $('#bid').val();
	var affiliateType = $('#affiliateType').val();
	var paymentTypeMessage = 'Select Payment Type';
	var platform = $('#platform-submit-bid').val();

	if (paymentType == '') {
		$('#paymentType').addClass("border-danger");
		$('#paymentTypeInvalid').html(paymentTypeMessage);
		paymentTypeBoolean = false;
	} else if (paymentType == "Affiliate") {
		var affiliateType = $('#affiliateType').val();
		if (affiliateType == "%" && (bidAmount <= 0 || bidAmount > 100)) {
			$('#bid').addClass("border-danger");
			$('#bidAmountInvalid').html("Enter a valid percentage");
			bidBoolean = false;
		} else if (affiliateType == "$" && (bidAmount <= 0)) {
			$('#bid').addClass("border-danger");
			$('#bidAmountInvalid').html("Enter a valid fee");
			bidBoolean = false;
		}
	}
	if (paymentType != "Affiliate") {
		affiliateType = null;
		postType.setCustomValidity('Select Post Type');

		if (postType.value == '') {
			$('#postType').addClass("border-danger");
			$('#postTypeInvalid').html(postType.validationMessage);
			postTypeBoolean = false;
		}
		if (platform == 'instagram') {
			postDuration.setCustomValidity('Select Post Duration');
			if (postDuration.value == '') {
				$('#postDuration').addClass("border-danger");
				$('#postDurationInvalid').html(postDuration.validationMessage);
				postDurationBoolean = false;
			}
		}

		if ($('#postType').val() == "Story"
				|| ($('#postType').val() == "Image" && ($('#postDuration')
						.val() == "24H" || $('#postDuration').val() == "1W"))) {
			var bid = document.getElementById("bid");
			bid.setCustomValidity('Enter the bid amount');
			if (bidAmount == "" || Number(bidAmount) <= 0) {
				$('#bid').addClass("border-danger");
				$('#bidAmountInvalid').html(bid.validationMessage);
				bidBoolean = false;
			}
		} else {
			var bid = document.getElementById("bid");
			var minimumAmount = (influencerPricing * 0.75).toFixed(0);
			bid.setCustomValidity('Your Bid should be greater than 0$');
			if (Number(bidAmount) > 0) {
				bidBoolean = true;
			} else {
				$('#bid').addClass("border-danger");
				$('#bidAmountInvalid').html(bid.validationMessage);
				bidBoolean = false;
			}
		}
	}

	if (postTypeBoolean && postDurationBoolean && bidBoolean
			&& paymentTypeBoolean && campaignBoolean) {
		$('#step3Body').html(
				'A bid will be submitted with the following details:');
		$('.submitBidTable').removeClass('d-none');

		if (paymentType == "Affiliate") {
			var affilateType = $('#affiliateType').val();
			var affilateMode;
			$('#postTypeTableLabel').addClass('d-none');
			$('#postDurationTableLabel').addClass('d-none');
			$('#paymentModeTableLabel').removeClass('d-none');
			$('#postTypeTable').addClass('d-none');
			$('#postDurationTable').addClass('d-none');
			$('#paymentModeTable').removeClass('d-none');

			if (affilateType == '$') {
				$('#bidAmountTableLabel').text('Affiliate Fee');
				affilateMode = '$ per Sale';
				$('#bidAmountTable').html('$' + $("#bid").val());
			} else if (affilateType == '%') {
				$('#bidAmountTableLabel').text('Affiliate %');
				affilateMode = '% of Sales';
				$('#bidAmountTable').html($("#bid").val() + '%');
			}
			$('#paymentModeTable').html(affilateMode);
		} else {
			$('#bidAmountTable').html('$' + $("#bid").val());
			$('#postTypeTableLabel').removeClass('d-none');
			$('#paymentModeTableLabel').addClass('d-none');
			$('#postTypeTable').removeClass('d-none');
			$('#paymentModeTable').addClass('d-none');
			$('#bidAmountTableLabel').text('Bid Amount');

			if (platform != 'instagram') {
				$('#postDurationTableLabel').addClass('d-none');
				$('#postDurationTable').addClass('d-none');
			} else {
				$('#postDurationTableLabel').removeClass('d-none');
				$('#postDurationTable').removeClass('d-none');
			}
		}

		$('#postTypeTable').html($("#postType option:selected").text());
		$('#postDurationTable').html($("#postDuration option:selected").text());
		$('#paymentTypeTable').html($("#paymentType option:selected").text());
		$('.addToCampaignMesage').addClass('d-none');
		$('#submitBid').removeClass('d-none');
		$('#addToCampaign').addClass('d-none');
		sendEvent('#SubmitBid', 2);
		$('#mailCc').val(profileDetails.email);
		$('#mailTemplate .true').removeAttr('hidden');

		if ($('#paymentType').val() == 'Affiliate') {
			if ($('#affiliateType').val() == '%') {
				$('#mailTemplate .Affiliate.percentage').removeAttr('hidden');
				$('#mailTemplate .Affiliate.percentage').prop('selected', true);
				$('#mailTemplate .Affiliate.fee').change();
			} else if ($('#affiliateType').val() == '$') {
				$('#mailTemplate .Affiliate.fee').removeAttr('hidden');
				$('#mailTemplate .Affiliate.fee').prop('selected', true);
				$('#mailTemplate .Affiliate.fee').change();
			}
		} else if ($('#paymentType').val() == 'Product/Service') {
			$('#mailTemplate .Product').removeAttr('hidden');
			$('#mailTemplate .Product').prop('selected', true);
			$('#mailTemplate .Affiliate.fee').change();
		} else {
			$(
					'#mailTemplate '
							+ ($('#paymentType').val().trim() == '' ? '' : '.'
									+ $('#paymentType').val().trim().replace(
											/ /g, '_'))).removeAttr('hidden');
			$(
					'#mailTemplate '
							+ ($('#paymentType').val().trim() == '' ? '' : '.'
									+ $('#paymentType').val().trim().replace(
											/ /g, '_'))).prop('selected', true);
			$('#mailTemplate .Affiliate.fee').change();
		}
	}
}

$(function() {
	$("#mailTemplate")
	.change(
		function() {
			$('#mailTemplateInvalid').text('');

			if ($("#mailTemplate").val() != ''
					&& $("#mailTemplate").val() != undefined
					&& $("#mailTemplate").val() != null) {
				var element = $(this).find('option:selected');
				var mailDescription = element.attr("data-value");
				var mailSubject = element.attr("data-subject");

				var mailPlatfrom = getPlatform($('#platform-submit-bid').val());
				mailDescription = mailDescription.split("<Platform>")
						.join(mailPlatfrom);
				mailSubject = mailSubject.split("<Brand Name>")
						.join(profileDetails.brandName);
				mailSubject = mailSubject.split("<Influencer Handle>")
						.join(influencerHandle);
				mailSubject = mailSubject.split("<Estimated Pricing>")
						.join('$' + Number(influencerPricing).toFixed(2));
				mailDescription = mailDescription.split(
						"<Influencer Handle>").join(
						influencerHandle);
				mailDescription = mailDescription.split(
						"<Brand Name>").join(
						profileDetails.brandName);
				mailDescription = mailDescription.split(
						"<Bid Amount>").join($('#bid').val());
				mailDescription = mailDescription.split(
						"<Post Type>").join(getPostType($('#postType').val()));
				if ($('#platform-submit-bid').val() == 'instagram') {
					var postDuration = getPostDuration($('#postDuration').val());
					mailDescription = mailDescription.split(
							"<Post Duration>").join(postDuration);
					var affiliateInstaText = '\nTo maximize your potential earnings, we recommend you to do the following:\n\n'
												+ '\tkeep your unique affiliate link in your bio (direct your audience to your bio)\n'
												+ '\tcreate 1 feed post\n'
												+ '\tcreate 2 stories every week\n';
					mailDescription = mailDescription.split("<Affiliate Instagram>").join(affiliateInstaText);
				} else {
					mailDescription = mailDescription.split("<Post Duration>")
						.join('');
					mailDescription = mailDescription.split("<Affiliate Instagram>").join('');
				}
				mailDescription = mailDescription.split(
						"<Affilitate %>").join($('#bid').val());
				mailDescription = mailDescription.split(
						"<Affiliate Fee>").join($('#bid').val());
				mailDescription = mailDescription.split(
						"<Estimated Pricing>").join('$' + Number(influencerPricing).toFixed(2));
				mailDescription = mailDescription.split("<apostrophe>")
						.join("'");
				$('#mailTextArea').val(mailDescription);
				$('#mailSubject').val(mailSubject);
			}
		});
});

function getPlatform(platform) {
	switch (platform) {
	case "instagram":
		platform = "Instagram";
		break;
	case "youtube":
		platform = "YouTube";
		break;
	case "tiktok":
		platform = "TikTok";
		break;
	default:
		break;
	}
	return platform;
}

function getPostType(postType) {
	switch (postType) {
	case "IntegratedVideo":
		postType = "Integrated Video";
		break;
	case "DedicatedVideo":
		postType = "Dedicated Video";
		break;
	case "PostRoll":
		postType = "Post Roll";
		break;
	case "Shoutout":
		postType = "Shout Out";
		break;
	case "SponsoredPost":
		postType = "Sponsored Post";
		break;
	default:
		break;
	}
	return postType;
}

function getPostDuration(postDuration) {
	switch (postDuration) {
	case "2H":
		postDuration = "2 hours";
		break;
	case "6H":
		postDuration = "6 hour";
		break;
	case "12H":
		postDuration = "12 hour";
		break;
	case "24H":
		postDuration = "24 hour";
		break;
	case "1W":
		postDuration = "1 week";
		break;
	case "P":
		postDuration = "permanent";
		break;
	default:
		break;
	}
	return postDuration;
}

function validateMailContent() {
	var mailTemplate = $("#mailTemplate").val();
	var mailDescription = $('#mailTextArea').val();
	var mailSubject = $('#mailSubject').val();

	if (mailTemplate == '' || mailTemplate == undefined) {
		$('#mailTemplateInvalid').text('Select Mail Template');
		return;
	}
	if (mailDescription == '' || mailDescription == undefined) {
		$('#mailTextAreaInvalid').text('Enter Mail Content');
		return;
	}
	if (mailSubject == '' || mailSubject == undefined) {
		$('#mailSubjectInvalid').text('Enter Subject');
		return;
	}
	sendEvent('#SubmitBid', 3);
}
$('#mailTextArea').on('input', function() {
	$('#mailTextAreaInvalid').text('');
});
$('#mailSubject').on('input', function() {
	$('#mailSubjectInvalid').text('');
});

function viewMailEditPage(title, id) {
	$('#mailTemplateTable').addClass('d-none');
	$('#createNewTemplate').addClass('d-none');
	$('#editTemplate').removeClass('d-none');
	$('#editTemplate').addClass('slide-left');
	var isNew = true;

	if (id != null) {
		for (var template in mailTemplates) {
			if (mailTemplates[template].id == id) {
				$('#templateTextArea').html('');
				$("#templateName").val(mailTemplates[template].templateName);
				$('#templateTextArea').val(mailTemplates[template].mailBody);
				$('#templateSubject').val(mailTemplates[template].mailSubject);
				$('#someid').attr('name', 'value');
				isNew = false;
				break;
			}
		}
	}
	$('#saveTemplate').attr('onclick',
			"saveTemplate('" + id + "'," + isNew + ")");
	$('#mailTemplatesLabel').html(title);
}

function viewTemplatePage() {
	$('#editTemplate').addClass('d-none');
	$('#createNewTemplate').removeClass('d-none');
	$('#mailTemplateTable').removeClass('d-none');
	$('#mailTemplateTable').addClass('slide-right');
	$('#mailTemplatesLabel').html('Mail Templates');
	$('.text-danger').html('');
	$('#mailTemplateModal input').val('').change();
	$('#variables').val('variables');
	$('#templateTextArea').val('');
}

function saveTemplate(id, isNew) {
	var templateName = $("#templateName").val().trim();
	var templateDescription = $('#templateTextArea').val().trim();
	var templateSubject = $('#templateSubject').val().trim();

	if (templateName == '' || templateName == undefined) {
		$('#templateNameInvalid').text('Enter Mail Template Name');
		$('#templateNameInvalid').removeClass('d-none');
		return;
	}
	if (templateSubject == '' || templateSubject == undefined) {
		$('#templateSubjectInvalid').text('Enter Subject');
		$('#templateSubjectInvalid').removeClass('d-none');
		return;
	}
	if (templateDescription == '' || templateDescription == undefined) {
		$('#templateTextAreaInvalid').text('Enter Mail Content');
		$('#templateTextAreaInvalid').removeClass('d-none');
		return;
	}

	$.post({
		url : window.location.origin + '/brand/view-campaign/save-mail-template',
		data : {
			templateName		: templateName
			,templateId			: id
			,templateSubject	: templateSubject
			,templateBody		: templateDescription
			,isNew				: isNew
		},
	})
	.done(function(data) {
		toast("Mail Template saved successfully");
		$('.' + data.templateId).remove();
		$('#mailTemplate').append($('<option>', {
			value : data.templateId,
			text : data.key.templateName,
			'data-value' : data.templateBody,
			'data-subject' : data.templateSubject,
			'data-isusercreated' : 'true',
			'class' : 'true ' + data.templateId
		}));
		$
		.get(
				{
					url : window.location.origin
							+ '/brand/get-mail-templates',
				})
		.done(function(data) {
			$('#table_mailTemplate tbody')
					.html('');
			$('#dropdownTemplates select')
					.html(
							'<option value="" hidden="true">Mail Template</option>');
			mailTemplates = [];

			for (var k in data) {
				var mailData = new Object();
				if (data[k].isUserCreated) {
					appendMailTemplate(data[k]);
					mailData.isUserCreated = true;
				} else {
					mailData.isUserCreated = false;
				}
				mailData.id = data[k].id;
				mailData.mailBody = data[k].mailBody;
				mailData.mailSubject = data[k].mailSubject;
				mailData.paymentType = null;
				mailData.subPaymentType = null;
				mailData.templateName = data[k].templateName;
				mailTemplates.push(mailData);
			}
		}).fail(function(data) {
			window.location.reload();
		});
		var delayInMilliseconds = 2000;
		setTimeout(function() {
			viewTemplatePage();
		}, delayInMilliseconds);
	}).fail(function(data) {
		if (data.status == 409) {
			toast(data.responseText, 'error');
		} else {
			toast('Failed to add post', 'error');
		}
	});
}

function resetMailTemplate() {
	$('.text-danger').html('');
	$('#mailTemplateModal input').val('').change();
	$('#variables').val('variables');
	$('#templateTextArea').val('');
	resetForms();
	viewTemplatePage();
	$('#mailTemplateTable').removeClass('slide-right');
	$('#editTemplate').removeClass('slide-left');
}

$('#templateTextArea').on('input', function() {
	$('#templateTextAreaInvalid').text('');
});
$('#templateSubject').on('input', function() {
	$('#templateSubjectInvalid').text('');
});
$('#templateName').on('input', function() {
	$('#templateNameInvalid').text('');
});

// postTrack
$('#bidAmountModal').on('input', function() {
	$('#bidAmountModalInvalid').text('');
});

$('#postUrlModal').on('input', function() {
	$('#postUrlModalInvalid').text('');
});

$('#affiliatePercentageModal').on('input', function() {
	$('#affiliatePercentageModalInvalid').text('');
});

$('#affiliateFeeModal').on('input', function() {
	$('#affiliateFeeModalInvalid').text('');
});

function appendMailTemplate(data) {
	$('#table_mailTemplate tbody')
			.append(
					'<tr><td style="text-align: start;">'
							+ data.templateName
							+ '</td><td style="text-align: start;"><div class="row"><div style="margin: auto;"><a id="'
							+ data.id
							+ '" class="btn-primary btn-sm waves-effect">Edit</a></div></div></td></tr>');
	$('#' + data.id).attr('onclick',
			'viewMailEditPage("' + data.templateName + '","' + data.id + '")');

	$('#dropdownTemplates select').append(
			'<option value="' + data.id + '">' + data.templateName
					+ '</option>');
}

var fieldFlag = '';
$('#templateSubject').click(function() {
	fieldFlag = 'subject';
});

$('#templateTextArea').click(function() {
	fieldFlag = 'content';
});

$('#variables').on('change', function() {
	if ($('#variables').val() != "variables") {
		insertAtCursor(fieldFlag, $('#variables').val());
	}
	$('#variables').val('variables');
});

function insertAtCursor(field, myValue) {
	var myField;
	if (field == 'subject') {
		myField = $('#templateSubject')[0];
	} else if (field == 'content') {
		myField = $('#templateTextArea')[0];
	}
	// IE support
	if (document.selection) {
		myField.focus();
		sel = document.selection.createRange();
		sel.text = myValue;
	}
	// MOZILLA and others
	else if (myField.selectionStart || myField.selectionStart == '0') {
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos) + myValue
				+ myField.value.substring(endPos, myField.value.length);
		myField.selectionStart = startPos + myValue.length;
		myField.selectionEnd = startPos + myValue.length;
	} else {
		myField.value += myValue;
	}
}

function resetModal() {
	$('.text-danger').html('');
	$('#bulkMailModal select').val('').change();
	$('#copyInfluencersModal select').val('').change();
}

function deleteCampaign() {
	var campaignId = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);

	$.post({
		url : window.location.origin + '/brand/view-campaign/' + campaignId + '/delete-campaign',
	})
	.done(function(data) {
		if (data.status == 200) {
			SUCCESS(data.message);
			window.location.replace(window.location.origin + '/brand/campaign');
		} else {
			ERROR(data.message);
		}
	}).fail(function(data) {
		ERROR('Campaign not found');
		window.location.replace(window.location.origin + '/brand/campaign');
	});
}





function copyToCampaign() {
	var selectedCampaignId = $('#campaignId').val();

	if (selectedCampaignId == '' || selectedCampaignId == undefined) {
		$('#campaignIdInvalid').text('Select Campaign Name');
		$('#campaignIdInvalid').removeClass('d-none');
		return;
	}
	$('#copyInfluencersConfirm').addClass('disabled');
	$.post({
		url : window.location.origin + "/brand/view-campaign/copy-influencers",
		traditional : true,
		data : {
			campaignId		: selectedCampaignId
			,influencerIds	: rowsSelected
		},
	}).done(function(data) {
		if (data.status == 200) {
			SUCCESS(data.message);
		} else {
			ERROR(data.message);
		}
		$('#copyInfluencersConfirm').removeClass('disabled');
		$('#table_campaign tbody tr').removeClass('selected');
		$('.dt-body-center input').prop('checked', false);
		resetCopyButton();
		rowsSelected = [];
		$(".close").click();
	}).fail(function(data) {
		$('#copyInfluencersConfirm').removeClass('disabled');
		ERROR('Failed to copy influencers');
		resetCopyButton();
		rowsSelected = [];
	});
}

function resetCopyButton() {
	$('#copyInfluencers').parent().addClass('disabled');
	$('#copyInfluencers').css("color", "rgb(227, 226, 226)");
	$('#copyInfluencers').css("cursor", "");
}

function changeCampaignName() {
	$('#campaignName').removeAttr('disabled');
	$('#editCampaignName').addClass('d-none');
	$('#saveCampaignName').removeClass('d-none');
}

function saveCampaignName() {
	var campaignName = $('#campaignName').val();
	var selectedCampaignId = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);

	if (campaignName == '' || campaignName == undefined || campaignName == null) {
		ERROR('Enter valid Campaign name');
		return;
	}
	$.post({
		url : window.location.origin + "/brand/view-campaign/edit-campaign-name",
		traditional : true,
		data : {
			campaignId		: selectedCampaignId
			,campaignName	: campaignName
		},
	}).done(function(data) {
		if (data.status == 200) {
			SUCCESS(data.message);
		} else {
			ERROR(data.message);
			$('#campaignName').val(campaignNameOriginal);
		}
		$('#campaignName').prop("disabled", true);
		$('#editCampaignName').removeClass('d-none');
		$('#saveCampaignName').addClass('d-none');
	}).fail(function(data) {
		ERROR('Failed to change CampaignName');
	});
}