package io.smartfluence.brand.management.mailbean;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class TokenUtil {
    @Autowired
    private Environment env;
    @Autowired
    DateUtil dateUtil;
    final static Logger LOG = Logger.getLogger(TokenUtil.class.getName());
    public String getJWTToken(String campaignId,String influencerId) {
        LOG.log(Level.INFO,env.getProperty("secretkey"));

        return Jwts
                .builder()
                .claim(Constants.campaignId,campaignId)
                .claim(Constants.influencerId,influencerId)
                .claim(Constants.Expiration,dateUtil.getDate())
                .signWith(SignatureAlgorithm.HS256,
                        "3kODgDGTGsV1oc1yNBbxlKse").compact();

    }
}
