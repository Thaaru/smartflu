package io.smartfluence.brand.management.service;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.nylas.RequestFailedException;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.dao.SettingsDao;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandPaymentDetails;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.modal.nylas.response.AccessTokenResponse;
import io.smartfluence.modal.validation.ChangePasswordModal;
import io.smartfluence.modal.validation.brand.settings.PaymentModal;
import io.smartfluence.modal.validation.brand.settings.ProfileModel;
import io.smartfluence.modal.validation.brand.settings.SocialConnectModel;
import io.smartfluence.modal.validation.brand.settings.PaymentModal.PaymentModalBuilder;
import io.smartfluence.mysql.entities.BrandEmailConnection;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.util.NylasUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
@Service
public class SettingsServiceImpl implements SettingsService {

	@Autowired
	private SettingsDao settingsDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private NylasUtil nylasUtil;

	static final Logger LOG = Logger.getLogger(SettingsServiceImpl.class.getName());

	@Override
	public ResponseEntity<Object> getBrandSocialConnect() {
		UserDetailsKey detailsKey = new UserDetailsKey(UserStatus.APPROVED, ResourceServer.getUserId());
		Optional<BrandDetails> brandOptional = settingsDao.getBrandDetailsById(detailsKey);
		SocialConnectModel connectModel = new SocialConnectModel();

		if (brandOptional.isPresent()) {
			connectModel.setInstagramHandle(brandOptional.get().getInstagramHandle());
			return new ResponseEntity<>(connectModel, HttpStatus.OK);
		}
		return new ResponseEntity<>(connectModel, HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Object> updateBrandSocialConnect(SocialConnectModel socialConnectModel) {
		UserDetailsKey detailsKey = new UserDetailsKey(UserStatus.APPROVED, ResourceServer.getUserId());
		Optional<BrandDetails> brandOptional = settingsDao.getBrandDetailsById(detailsKey);

		if (brandOptional.isPresent()) {
			BrandDetails brandDetails = brandOptional.get();
			brandDetails.setInstagramHandle(socialConnectModel.getInstagramHandle());
			settingsDao.saveBrandDetails(brandDetails);
		}
		return new ResponseEntity<>(socialConnectModel, HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Object> getBrandPaymentPreference() {
		PaymentModalBuilder paymentModelBuilder = PaymentModal.builder();
		Optional<BrandPaymentDetails> brandPaymentOptional = settingsDao.getBrandPaymentDetailsById(ResourceServer.getUserId());
		PaymentModal model;

		if (brandPaymentOptional.isPresent()) {
			BrandPaymentDetails paymentDetails = brandPaymentOptional.get();
			paymentModelBuilder.billingAddress(paymentDetails.getBillingAddress()).billingCity(paymentDetails.getBillingCity())
					.billingCountry(paymentDetails.getBillingCountry()).billingFullName(paymentDetails.getBillingFullName())
					.billingZipCode(paymentDetails.getBillingZipCode()).cardHolderName(paymentDetails.getCardHolderName())
					.creditCardNumber(paymentDetails.getCreditCardNumber()).cvv(paymentDetails.getCvv())
					.expiryDate(paymentDetails.getExpiryDate()).mailBill(paymentDetails.isMailBill())
					.mailingAddress(paymentDetails.getMailingAddress()).mailingCity(paymentDetails.getMailingCity())
					.mailingCountry(paymentDetails.getMailingCountry()).mailingFullName(paymentDetails.getMailingFullName())
					.mailingZipCode(paymentDetails.getMailingZipCode());
			model = paymentModelBuilder.build();
			return new ResponseEntity<>(model, HttpStatus.OK);
		}
		model = paymentModelBuilder.build();
		return new ResponseEntity<>(model, HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Object> updateBrandPaymentPreference(PaymentModal paymentModel) {
		BrandPaymentDetails brandPaymentDetails = BrandPaymentDetails.builder().userId(ResourceServer.getUserId())
				.billingAddress(paymentModel.getBillingAddress()).billingCity(paymentModel.getBillingCity())
				.billingCountry(paymentModel.getBillingCountry()).billingFullName(paymentModel.getBillingFullName())
				.billingZipCode(paymentModel.getBillingZipCode()).cardHolderName(paymentModel.getCardHolderName())
				.creditCardNumber(paymentModel.getCreditCardNumber()).cvv(paymentModel.getCvv())
				.expiryDate(paymentModel.getExpiryDate()).mailBill(paymentModel.isMailBill())
				.mailingAddress(paymentModel.getMailingAddress()).mailingCity(paymentModel.getMailingCity())
				.mailingCountry(paymentModel.getMailingCountry()).mailingFullName(paymentModel.getMailingFullName())
				.mailingZipCode(paymentModel.getMailingZipCode()).build();
		settingsDao.saveBrandPaymentDetails(brandPaymentDetails);
		return new ResponseEntity<>(paymentModel, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getProfileDetails() {
		UUID brandId = ResourceServer.getUserId();
		Optional<UserAccounts> optionalUserAccounts = settingsDao.getUserAccountsById(brandId.toString());
		UserAccounts userAccountsBuilder = null;

		if (optionalUserAccounts.isPresent()) {
			UserAccounts userAccounts = optionalUserAccounts.get();
			userAccountsBuilder = UserAccounts.builder().brandName(userAccounts.getBrandName()).email(userAccounts.getEmail())
					.industry(userAccounts.getIndustry()).firstName(userAccounts.getFirstName()).lastName(userAccounts.getLastName())
					.website(userAccounts.getWebsite()).userStatus(getUserStatus()).build();
		}
		return ResponseEntity.ok(userAccountsBuilder);
	}

	private UserStatus getUserStatus() {
		if (ResourceServer.isFreeBrand())
			return UserStatus.FREE;

		else if (ResourceServer.isPremiumBrand())
			return UserStatus.PREMIUM;

		return UserStatus.APPROVED;
	}

	@Override
	public ResponseEntity<Object> getEmailConnectionSettings() {
		Optional<BrandEmailConnection> optionalBrandEmailConnection = settingsDao.getEmailConnection(ResourceServer.getUserId().toString());
		return optionalBrandEmailConnection.isPresent() ? ResponseEntity.ok(optionalBrandEmailConnection.get()) : ResponseEntity.ok(null);
	}

	@Override
	public ResponseEntity<Object> updateProfileDetails(ProfileModel profileModel) {
		UUID brandId = ResourceServer.getUserId();
		Optional<UserAccounts> optionalUserAccounts = settingsDao.getUserAccountsById(brandId.toString());

		if (optionalUserAccounts.isPresent()) {
			UserAccounts userAccounts = optionalUserAccounts.get();
			Optional<BrandDetails> optionalBrandDetails = settingsDao.
					getBrandDetailsById(new UserDetailsKey(userAccounts.getUserStatus(), brandId));

			if (optionalBrandDetails.isPresent()) {
				Optional<UserAccounts> optionalMailValidation = settingsDao.
						getUserAccountsByEmailAndUserIdNotEqual(profileModel.getEmail(), brandId.toString());

				if (!optionalMailValidation.isPresent()) {
					BrandDetails brandDetails = optionalBrandDetails.get();
					userAccounts.setBrandName(profileModel.getBrandName());
					userAccounts.setFirstName(profileModel.getFirstName());
					userAccounts.setLastName(profileModel.getLastName());
					userAccounts.setIndustry(profileModel.getIndustry());
					userAccounts.setWebsite(profileModel.getWebsite());
					userAccounts.setEmail(profileModel.getEmail());
					settingsDao.saveUserAccounts(userAccounts);
					brandDetails.setBrandName(userAccounts.getBrandName());
					brandDetails.setIndustry(userAccounts.getIndustry());
					brandDetails.setWebsite(userAccounts.getWebsite());
					brandDetails.setEmail(userAccounts.getEmail());
					settingsDao.saveBrandDetails(brandDetails);
					return ResponseEntity.ok(profileModel);
				} else
					return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(
							"The email address you entered is already in use on another Smartfluence account. Please try with another email address.");
			}
		}
		return ResponseEntity.notFound().build();
	}

	@Override
	public ResponseEntity<Object> changePassword(ChangePasswordModal changePasswordModal) {
		UUID brandId = ResourceServer.getUserId();
		Optional<UserAccounts> optionalUserAccounts = settingsDao.getUserAccountsById(brandId.toString());
		if (optionalUserAccounts.isPresent()) {
			UserAccounts userAccounts = optionalUserAccounts.get();
			if (passwordEncoder.matches(changePasswordModal.getOldPassword(), userAccounts.getPassword())) {
				userAccounts.setPassword(passwordEncoder.encode(changePasswordModal.getNewPassword()));
				userAccounts.setModifiedAt(new Date());
				settingsDao.saveUserAccounts(userAccounts);
				return ResponseEntity.ok(changePasswordModal);
			}
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.notFound().build();
	}

	@Override
	public ResponseEntity<String> getTokenURL(String userEmail) {
		return !ResourceServer.isFreeBrand() ? new ResponseEntity<>(nylasUtil.getTokenUrl(userEmail), HttpStatus.OK)
				: new ResponseEntity<>("Please subscribe for Higher plans to Connect account.", HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<String> getAccessToken(String code) {
		try {
			UUID brandId = ResourceServer.getUserId();
			AccessTokenResponse response = new AccessTokenResponse();
			nylasUtil.getAccessToken(code, response);
			nylasUtil.upgradeAccount(response.getAccountId());

			if (!StringUtils.isEmpty(response.getAccessToken()))
				settingsDao.saveEmailConnection(BrandEmailConnection.builder().brandId(brandId.toString()).brandName(response.getAccountName())
						.email(response.getAccountEmailAddesss()).accessToken(response.getAccessToken()).accountId(response.getAccountId())
						.accountProvider(response.getAccountProvider()).syncState(response.getAccountSyncState()).createdAt(new Date())
						.status(1).build());
		} catch (IOException | RequestFailedException e) {
			//e.getStackTrace();
			LOG.log(Level.SEVERE,e.toString());
			LOG.log(Level.SEVERE,e.getMessage());
		}

		return new ResponseEntity<>("Thank you for linking your account. Your account is successfully connected.", HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> removeAccount() {
		try {
			UUID brandId = ResourceServer.getUserId();
			Optional<BrandEmailConnection> optionalBrandEmailConnection = settingsDao.getEmailConnection(brandId.toString());

			if (optionalBrandEmailConnection.isPresent()) {
				BrandEmailConnection brandEmailConnection = optionalBrandEmailConnection.get();
				nylasUtil.removeAccount(brandEmailConnection.getAccountId());

				brandEmailConnection.setAccessToken("");
				brandEmailConnection.setSyncState("removed");
				brandEmailConnection.setModifiedAt(new Date());
				brandEmailConnection.setStatus(1073741824);
				settingsDao.saveEmailConnection(brandEmailConnection);
			}
		} catch (IOException | RequestFailedException e) {
			//e.getStackTrace();
			LOG.log(Level.SEVERE,e.toString());
			LOG.log(Level.SEVERE,e.getMessage());
		}

		return new ResponseEntity<>("Your account has been removed successfully.", HttpStatus.OK);
	}
}