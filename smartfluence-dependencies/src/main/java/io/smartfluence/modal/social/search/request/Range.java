package io.smartfluence.modal.social.search.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Range {

	@JsonProperty("left_number")
	private Integer minValue;

	@JsonProperty("right_number")
	private Integer maxValue;
}