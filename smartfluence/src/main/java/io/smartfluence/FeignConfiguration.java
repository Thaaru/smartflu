package io.smartfluence;

import java.nio.charset.Charset;
import java.util.ArrayList;
import feign.Logger;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.json.BasicJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.StreamUtils;

import feign.Response;
import feign.codec.ErrorDecoder;
import io.smartfluence.exception.HttpClientErrorException;
import io.smartfluence.exception.HttpServerErrorException;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class FeignConfiguration extends FeignClientsConfiguration {

	private final ErrorDecoder defaultErrorDecoder = new ErrorDecoder.Default();

	JsonParser parser = new BasicJsonParser();

	@Bean
	public ErrorDecoder feignErrorDecoder() {
		return (String methodKey, Response response) -> {
			HttpHeaders responseHeaders = new HttpHeaders();
			response.headers().entrySet().stream().forEach(entry -> {
				if (!entry.getKey().equalsIgnoreCase(HttpHeaders.TRANSFER_ENCODING))
					responseHeaders.put(entry.getKey(), new ArrayList<>(entry.getValue()));
			});

			HttpStatus statusCode = HttpStatus.valueOf(response.status());
			String statusText = null;
			byte[] responseBody;
			try {
				String responseString = StreamUtils.copyToString(response.body().asInputStream(),
						Charset.forName("UTF-8"));
				statusText = getStatusText( responseString);
				responseBody = responseString.getBytes();
			} catch (Exception e) {
				log.error("Error while hitting microservice {}",e);
				throw new HttpClientErrorException(statusCode, statusText, responseHeaders, null, response);
			}

			if (statusCode.is4xxClientError())
				return new HttpClientErrorException(statusCode, statusText, responseHeaders, responseBody, response);

			if (statusCode.is5xxServerError())
				return new HttpServerErrorException(statusCode, statusText, responseHeaders, responseBody, null);

			return defaultErrorDecoder.decode(methodKey, response);
		};
	}

	private String getStatusText(String responseString) {
		try {
			return (String) parser.parseMap(responseString).get("message");
		} catch (Exception e) {
			log.error("Error while hitting microservice {}",e);
		}
		return null;
	}
	@Autowired
	private ObjectFactory<HttpMessageConverters> messageConverters;

	@Bean
	@Primary
	@Scope("prototype")
	public Encoder multipartFormEncoder() {

//TODO return new SpringFormEncoder(new SpringEncoder(messageConverters));// The source code does not support file array upload
		return new SpringFormEncoder(new SpringEncoder(messageConverters));
	}

	@Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }


}