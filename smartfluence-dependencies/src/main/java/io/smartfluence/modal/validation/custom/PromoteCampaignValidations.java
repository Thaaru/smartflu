package io.smartfluence.modal.validation.custom;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.ObjectUtils;

import io.smartfluence.modal.validation.brand.promote.PromoteCampaignRequestModel;

public class PromoteCampaignValidations implements ConstraintValidator<Conditional, PromoteCampaignRequestModel> {

	@Override
	public boolean isValid(PromoteCampaignRequestModel value, ConstraintValidatorContext context) {
		if (ObjectUtils.isEmpty(value.getProduct()))
			return false;
		if (!value.getProduct().equals("PRODUCT_ONLY")) {
			if (value.getProduct().equals("YES") && ObjectUtils.isEmpty(value.getProductValue()))
				return false;
			if (ObjectUtils.isEmpty(value.getPostType()))
				return false;
			if (ObjectUtils.isEmpty(value.getPayment()))
				return false;
		}
		return true;
	}

	@Override
	public void initialize(Conditional constraintAnnotation) {

	}
}