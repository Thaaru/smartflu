package io.smartfluence.cassandra.entities;

import java.io.Serializable;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.InfluencerBidDetailsKey;
import io.smartfluence.constants.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("influencer_bid_details")
public class InfluencerBidDetails implements Serializable {

	private static final long serialVersionUID = 1375380471131623644L;

	@PrimaryKey
	private InfluencerBidDetailsKey key;

	@Column("bid_amount")
	private Double bidAmount;

	@Column("brand_email")
	private String brandEmail;

	@Column("brand_industry")
	private String brandIndustry;

	@Column("brand_instagram_handle")
	private String brandInstagramHandle;

	@Column("brand_name")
	private String brandName;

	@Column("brand_status")
	private UserStatus brandStatus;

	@Column("created_at")
	private String createdAt;

	@Column("general_message")
	private String generalMessage;

	@Column("included_tag")
	private String includedTag;

	@Column("influencer_email")
	private String influencerMail;

	@Column("influencer_first_name")
	private String influencerFirstName;

	@Column("influencer_industry")
	private String influencerIndustry;

	@Column("influencer_handle")
	private String influencerHandle;

	@Column("influencer_last_name")
	private String influencerLastName;

	@Column("influencer_status")
	private UserStatus influencerStatus;

	@Column("post_duration")
	private String postDuration;

	@Column("post_type")
	private String postType;
}