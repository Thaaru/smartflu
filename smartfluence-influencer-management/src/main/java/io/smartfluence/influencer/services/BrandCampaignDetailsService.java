package io.smartfluence.influencer.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.smartfluence.influencer.entities.BrandCampaignDetails;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.repository.BrandCampaignDetailsRepo;

import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class BrandCampaignDetailsService {

    @Autowired
    BrandCampaignDetailsRepo brandCampaignDetailsRepo;

    public BrandCampaignDetails getBrandCampaignDetails(String campaignId){
        Logger.getLogger("getBrandCampaignDetails").log(Level.FINE,"Getting the Brand Campaign details");
        return brandCampaignDetailsRepo.findByCampaignId(campaignId);
    }

    public FullCampaignInfluencerDetails setBrandCampaignDetailsInInflu(BrandCampaignDetails brandCampaignDetailsNew,
                                                                        String campaignId, String influencerId) {
        Logger.getLogger("setBrandCampaignDetailsInInflu").log(Level.FINE,"Setting the Brand Campaign details");
        FullCampaignInfluencerDetails fullCampaignInfluencerDetails = new FullCampaignInfluencerDetails();
        fullCampaignInfluencerDetails.setCampaignId(campaignId);
        fullCampaignInfluencerDetails.setInfluencerId(influencerId);

        fullCampaignInfluencerDetails.setCampaignDescription(brandCampaignDetailsNew.getCampaignDescription());
        fullCampaignInfluencerDetails.setBrandId(brandCampaignDetailsNew.getBrandId());
        fullCampaignInfluencerDetails.setTermsId(brandCampaignDetailsNew.getTermsId());
        fullCampaignInfluencerDetails.setBrandDescription(brandCampaignDetailsNew.getBrandDescription());
        fullCampaignInfluencerDetails.setHashtags(brandCampaignDetailsNew.getHashTags());
        fullCampaignInfluencerDetails.setMaxProductCount(brandCampaignDetailsNew.getMaxProductCount());
       return fullCampaignInfluencerDetails;
    }

}
