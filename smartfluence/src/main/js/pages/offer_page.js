import React from "react";
import ColoredLine from "../components/line_component";
import { HeaderComponent } from "../components/header_component";
import { FooterComponent } from "../components/footer_component";
import { updateCampaignDetailsApi } from "../services/api/campaign_details_api";
import { ACCEPTED_MESSAGE, ERROR_MESSAGE, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";
import { toast } from "react-toastify";
const OfferPage = ({ data, next, close }) => {

    const submitHandler = async (data, loader) => {
        data.offerAcceptedStatus = STEPS_ACCEPTED_STATUS;
        loader(false);
        const result = await updateCampaignDetailsApi(data);
        if (result.code == 600) {
            toast.success(ACCEPTED_MESSAGE, { delay: 1000 })
            loader(true);
            data = result.value;
            next(data);
        } else {
            loader(true);
            toast.error(ERROR_MESSAGE, { delay: 1000 })
            data.offerAcceptedStatus = null;
            return null;
        }
    }

    return (
        <div class='content-wrapper' >
            <div>
                <div >
                    <HeaderComponent data={data} close={close} headerName={"Offer"} stepNo={1} />
                    <ColoredLine color={'secondaryColor'} />

                    <div>
                        {
                            <div className="col-md justify-content-center">
                                {data.offers && data.offers.map((offer) =>
                                    <li className="normalText" style={{ "listStyleType": "none" }}>
                                        {offer.campaignInfluencerOfferType.offerType == "PAYMENT" ?
                                            <div class="col-lg">
                                                <div className="row justify-content-around" style={{ backgroundColor: "#F8F4FF", "height": "80px", alignContent: "center" }}>
                                                    <div className="col-4 primaryColor">
                                                        Payment
                                                    </div>
                                                    <div className="col-6 primaryColor">
                                                        ${offer.paymentOffer} Cash
                                                    </div>
                                                </div>
                                            </div> : offer.campaignInfluencerOfferType.offerType == "PRODUCT" ?
                                                <div class="col-lg">
                                                    <div className="row justify-content-around" style={{ backgroundColor: "#F8F4FF", "height": "80px", alignContent: "center" }}>
                                                        <div className="col-6 primaryColor">
                                                            Product Only
                                                        </div>
                                                        <div className="col-4 primaryColor">
                                                        </div>
                                                    </div>
                                                </div> : null
                                        }
                                        <br />



                                    </li>)}

                                {data.offers && data.offers.map((offer) =>
                                    <li className="normalText" style={{ "listStyleType": "none" }}>

                                        {
                                            offer.campaignInfluencerOfferType.offerType == "COMMISSION" ?
                                                offer.commissionValue != null && offer.commissionValueType != null && offer.commissionAffiliateType != null ?

                                                    <div class="col-lg " style={{ "textAlign": "left" }}>

                                                        <p class="heading">Commision</p>

                                                        <p class="normalText">{`${offer.commissionValue}${offer.commissionValueType} per ${offer.commissionAffiliateType=="Custom"?offer.commissionAffiliateCustomName:offer.commissionAffiliateType}`}</p>

                                                        {/* <p class="normalText">{offer.commissionAffiliateDetails != null ? offer.commissionAffiliateDetails : null}</p> */}

                                                    </div> : null : null
                                        }

                                    </li>)}

                            </div>

                        }
                    </div>
                </div>
                <div style={{ marginTop: 'auto' }}>
                    <div class="normalText" style={{ "textAlign": "center" }}>See available products on the next page help</div>
                    <FooterComponent next={next} data={data} callBackfunction={submitHandler} acceptedStatus={data.offerAcceptedStatus} forwardStatus={STEPS_ACCEPTED_STATUS} previousScreenStatus={STEPS_ACCEPTED_STATUS} />
                </div>

            </div>
        </div>

    );
}

export default OfferPage;