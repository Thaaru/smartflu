package io.smartfluence.brand.management.entity;


import io.smartfluence.brand.management.entityv1.OfferTypeV1;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignInfluencerOfferKey implements Serializable {

    private String campaignId;

    private String influencerId;

    @Enumerated(EnumType.STRING)
    private OfferTypeV1 offerType;
}
