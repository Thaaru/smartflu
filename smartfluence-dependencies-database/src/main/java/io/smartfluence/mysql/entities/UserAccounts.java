package io.smartfluence.mysql.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import io.smartfluence.constants.InviteStatus;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.constants.UserType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_accounts")
public class UserAccounts implements Serializable {

	private static final long serialVersionUID = 6985837998359023491L;

	public UserAccounts(UserAccounts userAccounts) {
		this.userId = userAccounts.getUserId();
		this.brandName = userAccounts.getBrandName();
		this.firstName = userAccounts.getFirstName();
		this.lastName = userAccounts.getLastName();
		this.industry = userAccounts.getIndustry();
		this.email = userAccounts.getEmail();
		this.password = userAccounts.getPassword();
		this.userType = userAccounts.getUserType();
		this.userStatus = userAccounts.getUserStatus();
		this.verificationCode = userAccounts.getVerificationCode();
		this.timeZone = userAccounts.getTimeZone();
		this.inviteId = userAccounts.getInviteId();
		this.inviteStatus = userAccounts.getInviteStatus();
	}

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "user_id", columnDefinition = "VARCHAR(255)")
	private String userId;

	@Column(name = "brand_name")
	private String brandName;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "industry")
	private String industry;

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private String password;

	@Column(name = "invite_id")
	private String inviteId;

	@Enumerated(EnumType.STRING)
	@Column(name = "invite_status")
	private InviteStatus inviteStatus;

	@Column(name = "user_type")
	@Enumerated(EnumType.STRING)
	private UserType userType;

	@Enumerated(EnumType.STRING)
	@Column(name = "user_status")
	private UserStatus userStatus;

	@Column(name = "verification_code")
	private String verificationCode;

	@Column(name = "website")
	private String website;

	@Column(name = "time_zone")
	private String timeZone;

	@Column(name = "modified_at")
	@LastModifiedDate
	private Date modifiedAt;

	@CreatedDate
	@Column(name = "created_at")
	private Date createdAt;

	@Transient
	public boolean isBrand() {
		return userType != null && userType.equals(UserType.BRAND);
	}

	@Transient
	public boolean isInfluencer() {
		return userType != null && userType.equals(UserType.INFLUENCER);
	}

	public InviteStatus getInviteStatus() {
		if (this.inviteStatus == null) {
			return InviteStatus.UNINVITED;
		}
		return this.inviteStatus;
	}
}