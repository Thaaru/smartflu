package io.smartfluence.webapp.controller.rest.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor


@Table(name = "platform_master_new")
public class PlatformMaster implements Serializable {

    @Id
    @Column(name = "platform_id")
    private Long platformId;

    @Column(name = "platform_name")
    private String platformName;

}
