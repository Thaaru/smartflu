package io.smartfluence.brand.management.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.constants.IsNew;
import io.smartfluence.brand.management.dao.DashboardDao;
import io.smartfluence.brand.management.entity.BrandCampaigns;
import io.smartfluence.brand.management.entityv1.ManageCampaignMapperNew;
import io.smartfluence.brand.management.repository.CampaignInfluencerRepo;
import io.smartfluence.brand.management.repositoryv1.BrandCampaignDetailsRepositoryV1;
import io.smartfluence.brand.management.servicev1.CampaignEntityServiceV1;
import io.smartfluence.brand.management.servicev1.CampaignServiceV1;
import io.smartfluence.brand.management.servicev1.MasterServiceV1;

import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.modal.brand.campaignv1.CampaignStatusEntityV1;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class DashBoardServiceNew {

    @Autowired
    DashboardServiceImpl dashboardServiceImpl;
    @Autowired
    CampaignInfluencerRepo campaignInfluencerRepo;
    @Autowired
    private DashboardDao dashboardDao;
    @Autowired
    CampaignEntityServiceV1 campaignEntityServiceV1;
    @Autowired
    MasterServiceV1 masterServiceV1;
    @Autowired
    BrandCampaignDetailsRepositoryV1 brandCampaignDetailsRepositoryV1;


    final static Logger LOG = Logger.getLogger(DashBoardServiceNew.class.getName());

    public List<BrandCampaigns> getListOfCampaigns(){
        LOG.log(Level.INFO,"inside the list of campaigns for the brand api");
        List<BrandCampaigns> oldCampaignList = getOldCampaigns(false);
        List<BrandCampaigns> newCampaignList= getNewCampaigns();
        newCampaignList.addAll(oldCampaignList);
        return newCampaignList;
    }

    public List<BrandCampaigns> getNewCampaigns(){
        List<ManageCampaignMapperNew> allCampaignsByBrandIdUsingProcedure =brandCampaignDetailsRepositoryV1.findAllByBrandIdUsingProcedure(ResourceServer.getUserId().toString());
       return allCampaignsByBrandIdUsingProcedure.stream()
                .filter(campaignmapper ->
                        !Objects.equals(campaignmapper.getCampaign_status(), CampaignStatusEntityV1.INACTIVE.toString()))
                .map(campaignMapper -> {
                    LocalDate localDate = campaignMapper.getCreated_at().toLocalDate();
                    BrandCampaigns brandCampaigns = new BrandCampaigns();
                    brandCampaigns.setBrandId(ResourceServer.getUserId());
                    brandCampaigns.setCampaignId(UUID.fromString(campaignMapper.getCampaign_id()));
                    brandCampaigns.setCampaignName(campaignMapper.getCampaign_name());
                    brandCampaigns.setInfluencerCount(campaignMapper.getInfluencer_count());
                    brandCampaigns.setStatus(IsNew.newcampaign.toString().toUpperCase());
                    brandCampaigns.setStringCreatedAt(localDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
                    brandCampaigns.setCampaignStatus(CampaignStatus.valueOf(campaignMapper.getCampaign_status()));
                    brandCampaigns.setPlatformList(campaignMapper.getPlatform());
                    return brandCampaigns;
                }).collect(Collectors.toList());
    }


    public List<BrandCampaigns> getOldCampaigns(boolean isAllCampaign) {
        UUID brandId = ResourceServer.getUserId();
        List<BrandCampaigns> brandCampaigns = new LinkedList<BrandCampaigns>();
        List<BrandCampaignDetailsMysql> brandCampaignDetails = dashboardDao.getBrandCampaignDetailsMysql(brandId, isAllCampaign);

        if (!brandCampaignDetails.isEmpty()) {
            for (BrandCampaignDetailsMysql brandCampaignDetail : brandCampaignDetails) {
                Date date = brandCampaignDetail.getCreatedAt();
                LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                brandCampaigns.add(BrandCampaigns.builder().brandId(brandId)
                        .campaignId(UUID.fromString(brandCampaignDetail.getCampaignId()))
                        .campaignName(brandCampaignDetail.getCampaignName())
                        .campaignStatus(CampaignStatus.valueOf((brandCampaignDetail.getCampaignStatus().toString().equals("NEW")||
                                brandCampaignDetail.getCampaignStatus().toString().equals("INPROGRESS"))?"ACTIVE":
                                brandCampaignDetail.getCampaignStatus().toString())).stringCreatedAt(localDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")))
                        .createdAt(brandCampaignDetail.getCreatedAt()).isPromote(brandCampaignDetail.getIsPromote())
                        .influencerCount(brandCampaignDetail.getInfluencerCount()).
                        status(IsNew.legacycampaign.toString().toUpperCase()).platformList(new LinkedList<>()).
                        build());
            }
            return brandCampaigns;
        }
        return brandCampaigns;
    }
}
