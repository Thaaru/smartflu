package io.smartfluence.cassandra.entities.brand;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignInfluencerHistoryKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_campaign_influencer_history")
public class BrandCampaignInfluencerHistory implements Serializable {

	private static final long serialVersionUID = 4423636053287541315L;

	public BrandCampaignInfluencerHistory(BrandCampaignInfluencerHistory brandCampaignInfluencerHistory) {
		this.key = brandCampaignInfluencerHistory.getKey();
		this.brandName = brandCampaignInfluencerHistory.getBrandName();
		this.createdAt = brandCampaignInfluencerHistory.getCreatedAt();
		this.influencerAudienceCredibilty = brandCampaignInfluencerHistory.getInfluencerAudienceCredibilty();
		this.influencerEngagement = brandCampaignInfluencerHistory.getInfluencerEngagement();
		this.influencerFirstName = brandCampaignInfluencerHistory.getInfluencerFirstName();
		this.influencerFollowers = brandCampaignInfluencerHistory.getInfluencerFollowers();
		this.influencerLastName = brandCampaignInfluencerHistory.getInfluencerLastName();
		this.campaignName = brandCampaignInfluencerHistory.getCampaignName();
	}

	@PrimaryKey
	private BrandCampaignInfluencerHistoryKey key;

	@Column("brand_name")
	private String brandName;

	@Column("campaign_name")
	private String campaignName;

	@Column("created_at")
	private Date createdAt;

	@Column("influencer_audience_credibility")
	private Double influencerAudienceCredibilty;

	@Column("influencer_engagement")
	private Double influencerEngagement;

	@Column("influencer_first_name")
	private String influencerFirstName;

	@Column("influencer_followers")
	private Double influencerFollowers;

	@Column("influencer_last_name")
	private String influencerLastName;

	@Column("influencer_handle")
	private String influencerHandle;
}