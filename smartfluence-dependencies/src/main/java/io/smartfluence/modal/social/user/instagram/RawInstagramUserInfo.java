package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawInstagramUserInfo implements Serializable {

	private static final long serialVersionUID = 3426301587267699096L;

	@JsonAlias("status")
	private String status;

	@JsonAlias("user")
	private RawInstagramUser user;
}