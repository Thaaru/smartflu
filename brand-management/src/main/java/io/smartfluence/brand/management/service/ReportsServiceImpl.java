package io.smartfluence.brand.management.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.collect.Maps;

import feign.FeignException;
import io.smartfluence.ResourceServer;
import io.smartfluence.cassandra.entities.CampaignExportToCsvPath;
import io.smartfluence.cassandra.entities.PostAnalyticsTriggers;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.brand.BrandBidDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.entities.brand.BrandCreditAllowance;
import io.smartfluence.cassandra.entities.brand.BrandCreditHistory;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.primary.BrandCreditAllowanceKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandCreditHistoryKey;
import io.smartfluence.cassandra.entities.instagram.InstagramContent;
import io.smartfluence.cassandra.entities.instagram.InstagramCountryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.instagram.InstagramIndustryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramStateDemographic;
import io.smartfluence.cassandra.entities.instagram.primary.InstagramIndustryKey;
import io.smartfluence.cassandra.entities.instagram.primary.InstagramStateKey;
import io.smartfluence.cassandra.entities.primary.InfluencerContentKey;
import io.smartfluence.cassandra.entities.primary.InfluencerCountryKey;
import io.smartfluence.cassandra.entities.primary.ReportSearchHistoryKey;
import io.smartfluence.cassandra.entities.tiktok.TikTokContent;
import io.smartfluence.cassandra.entities.tiktok.TikTokCountryDemographic;
import io.smartfluence.cassandra.entities.tiktok.TikTokDemographics;
import io.smartfluence.cassandra.entities.youtube.YouTubeContent;
import io.smartfluence.cassandra.entities.youtube.YouTubeCountryDemographic;
import io.smartfluence.cassandra.entities.youtube.YouTubeDemographics;
import io.smartfluence.cassandra.repository.brand.BrandBidDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandCreditAllowanceRepo;
import io.smartfluence.cassandra.repository.brand.BrandCreditHistoryRepo;
import io.smartfluence.cassandra.repository.brand.BrandCreditsRepo;
import io.smartfluence.cassandra.repository.CampaignExportToCsvPathRepo;
import io.smartfluence.cassandra.repository.PostAnalyticsTriggersRepo;
import io.smartfluence.cassandra.repository.ReportSearchHistoryRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramContentRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramCountryDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramIndustryDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramStateDemographicRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokContentRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokCountryDemographicRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokDemographicRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeContentRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeCountryDemographicRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeDemographicRepo;
import io.smartfluence.constants.BidStatus;
import io.smartfluence.constants.CreditType;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.ViewType;
import io.smartfluence.modal.brand.dashboard.BrandCampaignInfluencerList;
import io.smartfluence.modal.brand.dashboard.CampaignPostLinkModal;
import io.smartfluence.modal.brand.dashboard.InfluencerSnapshot;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.social.report.CodeAndWeight;
import io.smartfluence.modal.social.report.Contacts;
import io.smartfluence.modal.social.report.NameAndWeight;
import io.smartfluence.modal.social.report.ReportData;
import io.smartfluence.modal.social.report.ReportResult;
import io.smartfluence.modal.social.report.TopPosts;
import io.smartfluence.modal.validation.brand.explore.SearchInfluencerRequestModel;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.CostPerEngagementMaster;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.BrandCampaignDetailsMysqlRepo;
import io.smartfluence.mysql.repository.CostPerEngagementMasterRepo;
import io.smartfluence.mysql.repository.SocialIdMasterRepo;
import io.smartfluence.mysql.repository.UserAccountsRepo;
import io.smartfluence.proxy.social.SocialDataProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Configuration
public class ReportsServiceImpl implements ReportsService {

	private static final String END_DATE = "endDate";

	private static final String START_DATE = "startDate";

	private static final String[] HEADERS = { "Influencer", "Followers", "Engagement", "Audience Credibility",
			"Estimated Pricing", "Bid Number", "Bid Status", "Post Type", "Post duration", "Payment Type",
			"Bid Amount / Affiliate Fee", "Submitted On", "Posted On", "Post Link", "Email" , "Platform"};

	private static final double PPF = 0.01;

	private static final String AGE_65 = "65+";

	private static final String AGE_45_64 = "45-64";

	private static final String AGE_35_44 = "35-44";

	private static final String AGE_25_34 = "25-34";

	private static final String AGE_18_24 = "18-24";

	private static final String AGE_13_17 = "13-17";

	private static final String GENDER_FEMALE = "Female";

	private static final String GENDER_MALE = "Male";

	private static final String INSTAGRAM = "instagram";

	private static final String YOUTUBE = "youtube";

	private static final String TIKTOK = "tiktok";

	@Autowired
	private SocialDataProxy socialDataProxy;

	@Autowired
	private ReportSearchHistoryRepo reportSearchHistoryRepo;

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private BrandCampaignDetailsMysqlRepo brandCampaignDetailsMysqlRepo;

	@Autowired
	private CampaignExportToCsvPathRepo campaignExportToCsvPathRepo;

	@Autowired
	private SocialIdMasterRepo socialIdMasterRepo;

	@Autowired
	private InstagramDemographicRepo instagramDemographicRepo;

	@Autowired
	private InstagramIndustryDemographicRepo instagramIndustryDemographicRepo;

	@Autowired
	private InstagramStateDemographicRepo instagramStateDemographicRepo;

	@Autowired
	private InstagramCountryDemographicRepo instagramCountryDemographicRepo;

	@Autowired
	private InstagramContentRepo instagramContentRepo;

	@Autowired
	private YouTubeDemographicRepo youtubeDemographicRepo;

	@Autowired
	private YouTubeCountryDemographicRepo youtubeCountryDemographicRepo;

	@Autowired
	private YouTubeContentRepo youtubeContentRepo;

	@Autowired
	private TikTokDemographicRepo tiktokDemographicRepo;

	@Autowired
	private TikTokCountryDemographicRepo tiktokCountryDemographicRepo;

	@Autowired
	private TikTokContentRepo tiktokContentRepo;

	@Autowired
	private CostPerEngagementMasterRepo costPerEngagementMasterRepo;

	@Autowired
	private BrandCreditsRepo brandCreditsRepo;

	@Autowired
	private BrandCreditAllowanceRepo brandCreditAllowanceRepo;

	@Autowired
	private BrandCreditHistoryRepo brandCreditHistoryRepo;

	@Autowired
	private PostAnalyticsTriggersRepo postAnalyticsTriggersRepo;

	@Autowired
	private BrandCampaignDetailsRepo brandCampaignDetailsRepo;

	@Autowired
	private BrandBidDetailsRepo bidDetailsRepo;

	@Value("${upload.influencers.template}")
	private String templateName;

	@Override
	public ResponseEntity<Object> influencerAudienceDataReport(SearchInfluencerRequestModel request) {
		try {
			UUID brandId = ResourceServer.getUserId();
			if (checkBrandCredit(brandId, request.getInfluencerId(), CreditType.DOWNLOAD_REPORT))
				return getReportData(request, brandId, null, false);
			else {
				Optional<BrandCredits> brandCredits = brandCreditsRepo.findById(brandId);
				if (brandCredits.isPresent() && brandCredits.get().getCredits() > 0)
					return getReportData(request, brandId, brandCredits, true);
				else {
					if (ResourceServer.isFreeBrand())
						return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You have run out of free credits! "
								+ "Please contact help@smartfluence.io to increase your limit.");
					else
						return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You have run out of credits! "
								+ "Please contact help@smartfluence.io to increase your limit.");
				}
			}
		} catch (FeignException fe) {
			return ResponseEntity.status(fe.status()).body(fe.getMessage());
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

	@Override
	public boolean checkBrandCredit(UUID brandId, String influencerId, CreditType creditType) {
		if (StringUtils.isEmpty(influencerId))
			return false;

		return brandCreditAllowanceRepo.
				findAllByKeyBrandIdAndKeyInfluencerIdAndKeyCreditType(brandId, UUID.fromString(influencerId), creditType).isPresent();
	}

	private ResponseEntity<Object> getReportData(SearchInfluencerRequestModel request, UUID brandId, Optional<BrandCredits> brandCredits,
			boolean saveCredits) {
		Optional<UserAccounts> userAccounts = userAccountsRepo.findById(brandId.toString());

		if (userAccounts.isPresent()) {
			String influencerSFId = request.getInfluencerId(); 
			String reportInfluencerId = StringUtils.isEmpty(request.getDeepSocialInfluencerId())
					? request.getInfluencerHandle() : request.getDeepSocialInfluencerId();
			String reportId = request.getReportPresent() ? request.getReportId() : "";

			if(!request.getReportPresent()) {
				ReportData reportData = getReportData(reportInfluencerId, request.getPlatform(), ViewType.DOWNLOAD, null);
				reportId = reportData.getReportInfo().getReportId();

				influencerSFId = saveInfluencerReportData(reportData);
			} else {
				saveReportHistory(ReportSearchHistory.builder().key(ReportSearchHistoryKey.builder().brandId(UUID.fromString(userAccounts.get().getUserId()))
						.isNew(false).createdAt(new Date()).build()).influencerUsername(request.getInfluencerHandle())
						.influencerDeepSocialId(request.getDeepSocialInfluencerId()).reportId(reportId).brandName(userAccounts.get().getBrandName())
						.viewType(ViewType.DOWNLOAD).platform(getPlatform(request.getPlatform())).build());
			}

			if(saveCredits)
				saveBrandCreditDetails(UUID.fromString(influencerSFId), brandCredits, CreditType.DOWNLOAD_REPORT);

			return formDownloadReportUrl(request, reportId);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	private ResponseEntity<Object> formDownloadReportUrl(SearchInfluencerRequestModel request, String reportId) {
		log.info("Report Id is {} ", reportId);
		if (StringUtils.isEmpty(reportId))
			return ResponseEntity.noContent().build();

		Map<String, URI> map = buildReportUriMap(request.getInfluencerHandle(), reportId);
		log.info("Report URL is {} ", map.get("report_url"));
		return ResponseEntity.status(HttpStatus.CREATED).location(map.get("report_url")).body(map);
	}

	private Map<String, URI> buildReportUriMap(String influencerHandle, String reportId) {
		URI reportUri = ServletUriComponentsBuilder.fromCurrentContextPath().port(-1)
				.pathSegment("brand", "reports", influencerHandle, "audience-data", reportId).build().toUri();
		log.info("Report URI generated {}", reportUri);
		Map<String, URI> map = new HashMap<>();
		map.put("report_url", reportUri);
		return map;
	}

	@Override
	public InfluencerDemographicsMapper getInfluencerDemographics(SocialIdMaster socialId, String deepSocialId, ViewType viewType, boolean searchNew) {
		InfluencerDemographicsMapper demographicsMapper = new InfluencerDemographicsMapper();
		demographicsMapper.setSaveReportHistory(true);

		if (socialId.getPlatform().equals(Platforms.INSTAGRAM))
			getInstagramDemographics(socialId, deepSocialId, viewType, demographicsMapper, searchNew);
		else if (socialId.getPlatform().equals(Platforms.YOUTUBE))
			getYoutubeDemographics(socialId, deepSocialId, viewType, demographicsMapper, searchNew);
		else if (socialId.getPlatform().equals(Platforms.TIKTOK))
			getTiktokDemographics(socialId, deepSocialId, viewType, demographicsMapper, searchNew);
		return demographicsMapper;
	}

	private void getInstagramDemographics(SocialIdMaster socialId, String deepSocialId, ViewType viewType,
			InfluencerDemographicsMapper demographicsMapper, boolean searchNew) {
		ModelMapper modelMapper = new ModelMapper();
		Optional<InstagramDemographics> instagramDemographics = instagramDemographicRepo.findById(socialId.getInfluencerHandle());		
		if (!instagramDemographics.isPresent() && searchNew) {
			demographicsMapper.setSaveReportHistory(false);
			saveNewInfluencer(deepSocialId, socialId.getPlatform().name(), viewType);
			instagramDemographics = instagramDemographicRepo.findById(socialId.getInfluencerHandle());
		}
		modelMapper.map(instagramDemographics.get(), demographicsMapper);
		demographicsMapper.setPricing(instagramDemographics.get().getInstagramPricing());
	}

	private void getYoutubeDemographics(SocialIdMaster socialId, String deepSocialId, ViewType viewType,
			InfluencerDemographicsMapper demographicsMapper, boolean searchNew) {
		ModelMapper modelMapper = new ModelMapper();
		Optional<YouTubeDemographics> youtubeDemographics = youtubeDemographicRepo.findById(socialId.getInfluencerHandle());

		if(!youtubeDemographics.isPresent())
			youtubeDemographics = youtubeDemographicRepo.findByUserId(UUID.fromString(socialId.getUserId()));

		if (!youtubeDemographics.isPresent() && searchNew) {
			demographicsMapper.setSaveReportHistory(false);
			saveNewInfluencer(deepSocialId, socialId.getPlatform().name(), viewType);
			youtubeDemographics = youtubeDemographicRepo.findById(socialId.getInfluencerHandle());
		}
		modelMapper.map(youtubeDemographics.get(), demographicsMapper);
	}

	private void getTiktokDemographics(SocialIdMaster socialId, String deepSocialId, ViewType viewType,
			InfluencerDemographicsMapper demographicsMapper, boolean searchNew) {
		ModelMapper modelMapper = new ModelMapper();
		Optional<TikTokDemographics> tiktokDemographics = tiktokDemographicRepo.findById(socialId.getInfluencerHandle());

		if (!tiktokDemographics.isPresent())
			tiktokDemographics = tiktokDemographicRepo.findByUserId(UUID.fromString(socialId.getUserId()));

		if (!tiktokDemographics.isPresent() && searchNew) {
			demographicsMapper.setSaveReportHistory(false);
			saveNewInfluencer(deepSocialId, socialId.getPlatform().name(), viewType);
			tiktokDemographics = tiktokDemographicRepo.findById(socialId.getInfluencerHandle());
		}
		modelMapper.map(tiktokDemographics.get(), demographicsMapper);
	}

	@Override
	public List<InfluencerDemographicsMapper> getAllInfluencerDemographics(List<String> instagramHandles, List<String> youtubeHandles,
			List<String> tiktokHandles) {
		ModelMapper modelMapper = new ModelMapper();
		List<InfluencerDemographicsMapper> influencerDemographicsList = new LinkedList<InfluencerDemographicsMapper>();

		if (!instagramHandles.isEmpty())
			getInstagramDemographics(instagramHandles, modelMapper, influencerDemographicsList);

		if (!youtubeHandles.isEmpty())
			getYoutubeDemographics(youtubeHandles, modelMapper, influencerDemographicsList);

		if (!tiktokHandles.isEmpty())
			getTiktokDemographics(tiktokHandles, modelMapper, influencerDemographicsList);

		return influencerDemographicsList;
	}

	private void getInstagramDemographics(List<String> instagramHandles, ModelMapper modelMapper,
			List<InfluencerDemographicsMapper> influencerDemographicsList) {
		instagramDemographicRepo.findAllById(instagramHandles).forEach(data -> {
			InfluencerDemographicsMapper influencerDemographicsMapper = new InfluencerDemographicsMapper();
			modelMapper.map(data, influencerDemographicsMapper);
			influencerDemographicsMapper.setPlatform(Platforms.INSTAGRAM.name().toLowerCase());
			influencerDemographicsMapper.setPricing(data.getInstagramPricing());
			influencerDemographicsList.add(influencerDemographicsMapper);
		});
	}

	private void getYoutubeDemographics(List<String> youtubeHandles, ModelMapper modelMapper,
			List<InfluencerDemographicsMapper> influencerDemographicsList) {
		youtubeDemographicRepo.findAllById(youtubeHandles).forEach(data -> {
			InfluencerDemographicsMapper influencerDemographicsMapper = new InfluencerDemographicsMapper();
			modelMapper.map(data, influencerDemographicsMapper);
			influencerDemographicsMapper.setPlatform(Platforms.YOUTUBE.name().toLowerCase());
			influencerDemographicsList.add(influencerDemographicsMapper);
		});
	}

	private void getTiktokDemographics(List<String> tiktokHandles, ModelMapper modelMapper,
			List<InfluencerDemographicsMapper> influencerDemographicsList) {
		tiktokDemographicRepo.findAllById(tiktokHandles).forEach(data -> {
			InfluencerDemographicsMapper influencerDemographicsMapper = new InfluencerDemographicsMapper();
			modelMapper.map(data, influencerDemographicsMapper);
			influencerDemographicsMapper.setPlatform(Platforms.TIKTOK.name().toLowerCase());
			influencerDemographicsList.add(influencerDemographicsMapper);
		});
	}

	@Override
	public UUID saveNewInfluencer(String deepsocialuserid, String platform, ViewType viewtype) {
		log.info("deep social data="+deepsocialuserid+" platform="+platform+" viewtype="+viewtype);
		return UUID.fromString(saveInfluencerReportData(getReportData(deepsocialuserid, platform.toLowerCase(), viewtype, null)));
	}

	@Override
	public String saveInfluencerReportData(ReportData reportData) {

		UUID influencerId = saveSocialIdMaster(reportData);
		if (reportData.getUserProfile().getPlatformType().equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			saveInstagramData(reportData, influencerId);
		if (reportData.getUserProfile().getPlatformType().equalsIgnoreCase(Platforms.YOUTUBE.name()))
			saveYoutubeData(reportData, influencerId);
		if (reportData.getUserProfile().getPlatformType().equalsIgnoreCase(Platforms.TIKTOK.name()))
			saveTiktokData(reportData, influencerId);

		return influencerId.toString();
	}

	private UUID saveSocialIdMaster(ReportData reportData) {
		log.info("report data="+reportData.toString());
		SocialIdMaster socialIdMaster = new SocialIdMaster();
		String handle = "@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
				? reportData.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase() : reportData.getUserProfile().getHandle());
		Optional<SocialIdMaster> optionalSocialIdMaster = reportData.getUserProfile().getPlatformType().equalsIgnoreCase(Platforms.INSTAGRAM.name())
				? socialIdMasterRepo.findByInfluencerHandleAndPlatform(handle, getPlatform(reportData.getUserProfile().getPlatformType()))
						: socialIdMasterRepo.findByInfluencerId(reportData.getUserProfile().getUserId());

		if (!optionalSocialIdMaster.isPresent()) {
			Boolean instaDataAvailable = false;
			if (reportData.getUserProfile().getPlatformType().equalsIgnoreCase(Platforms.INSTAGRAM.name())) {
				optionalSocialIdMaster = socialIdMasterRepo.findByInfluencerId(reportData.getUserProfile().getUserId());

				if (optionalSocialIdMaster.isPresent()) {
					instaDataAvailable = true;
					socialIdMaster = optionalSocialIdMaster.get();
					socialIdMaster.setModifiedAt(new Date());
				}
			}

			if (!instaDataAvailable) {
				socialIdMaster.setUserId(UUID.randomUUID().toString());
				socialIdMaster.setPlatform(getPlatform(reportData.getUserProfile().getPlatformType()));
				socialIdMaster.setCreatedAt(new Date());
			}
		}
		else {
			socialIdMaster = optionalSocialIdMaster.get();
			socialIdMaster.setModifiedAt(new Date());
		}

		socialIdMaster.setInfluencerId(reportData.getUserProfile().getUserId());
		socialIdMaster.setInfluencerReportId(reportData.getReportInfo().getReportId());
		socialIdMaster.setInfluencerHandle(handle);
		SocialIdMaster socialDataRel = socialIdMasterRepo.save(socialIdMaster);

		return UUID.fromString(socialDataRel.getUserId());
	}

	private void saveInstagramData(ReportData reportData, UUID influencerId) {
		saveInstagramDemographics(reportData, influencerId);
		saveInstagramIndustryDemographic(reportData);
		saveInstagramStateDemographic(reportData);
		saveInstagramCountryDemographic(reportData);
		saveInstagramContent(reportData);
	}

	private void saveYoutubeData(ReportData reportData, UUID influencerId) {
		saveYoutubeDemographics(reportData, influencerId);
		saveYoutubeCountryDemographic(reportData);
		saveYoutubeContent(reportData);
	}

	private void saveTiktokData(ReportData reportData, UUID influencerId) {
		saveTiktokDemographics(reportData, influencerId);
		saveTiktokCountryDemographic(reportData);
		saveTiktokContent(reportData);
	}

	private void saveInstagramDemographics(ReportData reportData, UUID influencerId) {
		Map<String, Double> ageRange = getAgeRangesFromSocial(
				reportData.getAudienceFollowers().getData().getAudienceAge());
		Map<String, Double> gender = getGenderFromSocial(
				reportData.getAudienceFollowers().getData().getAudienceGender());
		InstagramDemographics instagramDemographics = InstagramDemographics.builder().age13To17(ageRange.get(AGE_13_17))
				.age18To24(ageRange.get(AGE_18_24)).age25To34(ageRange.get(AGE_25_34))
				.age35To44(ageRange.get(AGE_35_44)).age45To64(ageRange.get(AGE_45_64)).age65ToAny(ageRange.get(AGE_65))
				.comments(reportData.getUserProfile().getAvgComments())
				.credibility(reportData.getAudienceFollowers().getData().getAudienceCredibility())
				.engagement(reportData.getUserProfile().getEngagement()).female(gender.get(GENDER_FEMALE))
				.engagementRate(reportData.getUserProfile().getEngagementRate())
				.followers(reportData.getUserProfile().getFollowers()).email(getEmail(reportData.getUserProfile().getContacts()))
				.influencerHandle("@" + reportData.getUserProfile().getHandle()).url(reportData.getUserProfile().getUrl())
				.likes(reportData.getUserProfile().getAvgLikes()).male(gender.get(GENDER_MALE))
				.instagramPricing(getEstimatedPricing(reportData.getUserProfile().getEngagementRate(),
						reportData.getUserProfile().getFollowers(), reportData.getUserProfile().getEngagement(),
						reportData.getAudienceFollowers().getData().getAudienceCredibility()))
				.influencerName(reportData.getUserProfile().getFullName())
				.profileImage(reportData.getUserProfile().getProfileImage()).userId(influencerId).build();

		instagramDemographicRepo.save(instagramDemographics);
	}

	private void saveInstagramIndustryDemographic(ReportData reportData) {
		Map<String, Double> topIndustries = getTopIndustriesFromSocial(reportData.getAudienceFollowers().getData().getAudienceInterests());
		List<InstagramIndustryDemographic> industryDemographics = new LinkedList<>();
		topIndustries.forEach((k, v) -> {
			industryDemographics.add(InstagramIndustryDemographic.builder()
					.key(new InstagramIndustryKey("@" + reportData.getUserProfile().getHandle(), k)).value(v).build());
		});
		instagramIndustryDemographicRepo.saveAll(industryDemographics);
	}

	private void saveInstagramStateDemographic(ReportData reportData) {
		Map<String, Double> topStates = getTopStatesFromSocial(reportData.getAudienceFollowers().getData().getAudienceGeo().getStates());
		List<InstagramStateDemographic> stateDemographics = new LinkedList<>();
		topStates.forEach((k, v) -> {
			stateDemographics.add(InstagramStateDemographic.builder()
					.key(new InstagramStateKey("@" + reportData.getUserProfile().getHandle(), k)).value(v).build());
		});
		instagramStateDemographicRepo.saveAll(stateDemographics);
	}

	private void saveInstagramCountryDemographic(ReportData reportData) {
		Map<String, Double> topCountries = getTopCountriesFromSocial(reportData.getAudienceFollowers().getData().getAudienceGeo().getCountries());
		List<InstagramCountryDemographic> countryDemographics = new LinkedList<>();
		topCountries.forEach((k, v) -> {
			countryDemographics.add(InstagramCountryDemographic.builder()
					.key(new InfluencerCountryKey("@" + reportData.getUserProfile().getHandle(), k)).value(v).build());
		});
		instagramCountryDemographicRepo.saveAll(countryDemographics);
	}

	private void saveInstagramContent(ReportData reportData) {
		List<InstagramContent> instagramContents = new LinkedList<>();
		Integer i = 1;

		for (TopPosts topPost : reportData.getUserProfile().getTopPosts()) {
			instagramContents.add(InstagramContent.builder()
					.key(new InfluencerContentKey("@" + reportData.getUserProfile().getHandle(), i))
				.value(topPost.getImage()).url(topPost.getUrl()).build());
			i++;
		}
		instagramContentRepo.saveAll(instagramContents);
	}

	private void saveYoutubeDemographics(ReportData reportData, UUID influencerId) {
		Map<String, Double> ageRange = getAgeRangesFromSocial(
				reportData.getAudienceFollowers().getData().getAudienceAge());
		Map<String, Double> gender = getGenderFromSocial(
				reportData.getAudienceFollowers().getData().getAudienceGender());
		YouTubeDemographics youtubeDemographics = YouTubeDemographics.builder().age13To17(ageRange.get(AGE_13_17))
				.age18To24(ageRange.get(AGE_18_24)).age25To34(ageRange.get(AGE_25_34))
				.age35To44(ageRange.get(AGE_35_44)).age45To64(ageRange.get(AGE_45_64)).age65ToAny(ageRange.get(AGE_65))
				.comments(reportData.getUserProfile().getAvgComments()).credibility(reportData.getAudienceFollowers().getData().getAudienceCredibility())
				.engagement(reportData.getUserProfile().getEngagement()).female(gender.get(GENDER_FEMALE))
				.engagementRate(reportData.getUserProfile().getEngagementRate())
				.followers(reportData.getUserProfile().getFollowers()).email(getEmail(reportData.getUserProfile().getContacts()))
				.influencerHandle("@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
						? reportData.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase()
								: reportData.getUserProfile().getHandle())).views(reportData.getUserProfile().getAvgViews())
				.likes(reportData.getUserProfile().getAvgLikes()).disLikes(reportData.getUserProfile().getAvgDislikes()).male(gender.get(GENDER_MALE))
				.pricing(getEstimatedPricing(reportData.getUserProfile().getEngagementRate(),
						reportData.getUserProfile().getFollowers(), reportData.getUserProfile().getEngagement(),
						reportData.getAudienceFollowers().getData().getAudienceCredibility())).url(reportData.getUserProfile().getUrl())
				.influencerName(reportData.getUserProfile().getFullName())
				.profileImage(reportData.getUserProfile().getProfileImage()).userId(influencerId).build();
		
		youtubeDemographicRepo.save(youtubeDemographics);
	}

	private void saveYoutubeCountryDemographic(ReportData reportData) {
		Map<String, Double> topCountries = getTopCountriesFromSocial(reportData.getAudienceFollowers().getData().getAudienceGeo().getCountries());
		List<YouTubeCountryDemographic> countryDemographics = new LinkedList<>();
		topCountries.forEach((k, v) -> {
			countryDemographics.add(YouTubeCountryDemographic.builder()
					.key(new InfluencerCountryKey("@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
							? reportData.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase()
									: reportData.getUserProfile().getHandle()), k)).value(v).build());
		});
		youtubeCountryDemographicRepo.saveAll(countryDemographics);
	}

	private void saveYoutubeContent(ReportData reportData) {
		List<YouTubeContent> youtubeContents = new LinkedList<>();
		Integer i = 1;

		for (TopPosts topPost : reportData.getUserProfile().getTopPosts()) {
			youtubeContents.add(YouTubeContent.builder()
					.key(new InfluencerContentKey("@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
							? reportData.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase()
									: reportData.getUserProfile().getHandle()), i)).value(topPost.getThumbnail()).url(topPost.getUrl()).build());
			i++;
		}
		youtubeContentRepo.saveAll(youtubeContents);
	}

	private void saveTiktokDemographics(ReportData reportData, UUID influencerId) {
		Map<String, Double> ageRange = getAgeRangesFromSocial(
				reportData.getAudienceFollowers().getData().getAudienceAge());
		Map<String, Double> gender = getGenderFromSocial(
				reportData.getAudienceFollowers().getData().getAudienceGender());
		TikTokDemographics tiktokDemographics = TikTokDemographics.builder().age13To17(ageRange.get(AGE_13_17))
				.age18To24(ageRange.get(AGE_18_24)).age25To34(ageRange.get(AGE_25_34))
				.age35To44(ageRange.get(AGE_35_44)).age45To64(ageRange.get(AGE_45_64)).age65ToAny(ageRange.get(AGE_65))
				.comments(reportData.getUserProfile().getAvgComments()).credibility(reportData.getAudienceFollowers().getData().getAudienceCredibility())
				.engagement(reportData.getUserProfile().getEngagement()).female(gender.get(GENDER_FEMALE))
				.engagementRate(reportData.getUserProfile().getEngagementRate())
				.followers(reportData.getUserProfile().getFollowers()).email(getEmail(reportData.getUserProfile().getContacts()))
				.influencerHandle("@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
						? reportData.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase()
								: reportData.getUserProfile().getHandle())).views(reportData.getUserProfile().getAvgViews())
				.likes(reportData.getUserProfile().getAvgLikes()).male(gender.get(GENDER_MALE))
				.pricing(getEstimatedPricing(reportData.getUserProfile().getEngagementRate(),
						reportData.getUserProfile().getFollowers(), reportData.getUserProfile().getEngagement(),
						reportData.getAudienceFollowers().getData().getAudienceCredibility())).url(reportData.getUserProfile().getUrl())
				.influencerName(reportData.getUserProfile().getFullName())
				.profileImage(reportData.getUserProfile().getProfileImage()).userId(influencerId).build();
		
		tiktokDemographicRepo.save(tiktokDemographics);
	}

	private void saveTiktokCountryDemographic(ReportData reportData) {
		Map<String, Double> topCountries = getTopCountriesFromSocial(reportData.getAudienceFollowers().getData().getAudienceGeo().getCountries());
		List<TikTokCountryDemographic> countryDemographics = new LinkedList<>();
		topCountries.forEach((k, v) -> {
			countryDemographics.add(TikTokCountryDemographic.builder()
					.key(new InfluencerCountryKey("@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
							? reportData.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase()
									: reportData.getUserProfile().getHandle()), k)).value(v).build());
		});
		tiktokCountryDemographicRepo.saveAll(countryDemographics);
	}

	private void saveTiktokContent(ReportData reportData) {
		List<TikTokContent> tiktokContents = new LinkedList<>();
		Integer i = 1;

		for (TopPosts topPost : reportData.getUserProfile().getTopPosts()) {
			tiktokContents.add(TikTokContent.builder()
					.key(new InfluencerContentKey("@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
							? reportData.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase()
									: reportData.getUserProfile().getHandle()), i)).value(topPost.getThumbnail()).url(topPost.getUrl()).build());
			i++;
		}
		tiktokContentRepo.saveAll(tiktokContents);
	}

	private Map<String, Double> getTopCountriesFromSocial(List<NameAndWeight> countries) {
		Map<String, Double> topCountries = Maps.newLinkedHashMap();
		for (NameAndWeight country : countries) {
			topCountries.put(country.getName(), country.getWeight());
		}
		return topCountries;
	}

	private Map<String, Double> getTopStatesFromSocial(List<NameAndWeight> states) {
		Map<String, Double> topCountries = Maps.newLinkedHashMap();
		for (NameAndWeight country : states) {
			topCountries.put(country.getName(), country.getWeight());
		}
		return topCountries;
	}

	private Map<String, Double> getTopIndustriesFromSocial(List<NameAndWeight> audienceInterests) {
		Map<String, Double> topIndustries = Maps.newLinkedHashMap();
		for (NameAndWeight audienceInterest : audienceInterests) {
			topIndustries.put(audienceInterest.getName(), audienceInterest.getWeight());
		}
		return topIndustries;
	}

	@Override
	public Double getEstimatedPricing(Double engagementRate, Double followers, Double engagement, Double audienceCredibility) {
		Optional<CostPerEngagementMaster> engagementMaster = costPerEngagementMasterRepo
				.findByStartRangeAndEndRange(engagementRate);

		if (engagementMaster.isPresent()) {
			Double firstParam = followers * PPF * audienceCredibility;
			Double secondParam = engagement * engagementMaster.get().getCostPerEngagement();
			Double estPricing = (firstParam + secondParam) / 2;
			return estPricing;
		}
		else
			return followers * PPF * audienceCredibility;
	}

	@Override
	public Map<String, Double> getGenderFromSocial(List<CodeAndWeight> audienceGenders) {
		Map<String, Double> genders = Maps.newLinkedHashMap();
		for (CodeAndWeight audienceGender : audienceGenders) {
			switch (audienceGender.getCode()) {
			case "MALE":
				genders.put(GENDER_MALE, audienceGender.getWeight());
				break;
			case "FEMALE":
				genders.put(GENDER_FEMALE, audienceGender.getWeight());
				break;
			default:
				break;
			}
		}
		return genders;
	}

	@Override
	public Map<String, Double> getAgeRangesFromSocial(List<CodeAndWeight> audienceAges) {
		Map<String, Double> ageRanges = Maps.newLinkedHashMap();
		for (CodeAndWeight audienceAge : audienceAges) {
			switch (audienceAge.getCode()) {
			case AGE_13_17:
				ageRanges.put(AGE_13_17, audienceAge.getWeight());
				break;
			case AGE_18_24:
				ageRanges.put(AGE_18_24, audienceAge.getWeight());
				break;
			case AGE_25_34:
				ageRanges.put(AGE_25_34, audienceAge.getWeight());
				break;
			case AGE_35_44:
				ageRanges.put(AGE_35_44, audienceAge.getWeight());
				break;
			case AGE_45_64:
				ageRanges.put(AGE_45_64, audienceAge.getWeight());
				break;
			case AGE_65:
				ageRanges.put(AGE_65, audienceAge.getWeight());
				break;
			default:
				break;
			}
		}
		return ageRanges;
	}

	private String getEmail(List<Contacts> contacts) {
		if (!Objects.isNull(contacts)) {
			for (Contacts contact : contacts) {
				if (contact.getType().equalsIgnoreCase("email"))
					return contact.getValue();
			}
		}
		return null;
	}

	@Override
	public Object saveBrandCreditDetails(UUID userId, Optional<BrandCredits> optionalBrandCredits, CreditType creditType) {
		BrandCreditHistory brandCreditHistory = saveBrandCreditHistory(optionalBrandCredits.get(), creditType);
		saveBrandInfluencerBidAllowanceMap(brandCreditHistory, userId, creditType);
		return saveBrandCredits(optionalBrandCredits);
	}

	private BrandCreditHistory saveBrandCreditHistory(BrandCredits brandCredits, CreditType creditType) {
		UUID brandId = ResourceServer.getUserId();
		UUID referenceId = UUID.randomUUID();
		UUID creditId = UUID.randomUUID();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
		Date date = new Date();

		BrandCreditHistory brandCreditHistory = new BrandCreditHistory();
		BrandCreditHistoryKey brandCreditHistoryKey = new BrandCreditHistoryKey(brandId, dateFormat.format(date),
				creditId, date);
		brandCreditHistory.setKey(brandCreditHistoryKey);
		brandCreditHistory.setCreditSpent(1);
		brandCreditHistory.setCreditType(creditType);
		brandCreditHistory.setReferenceId(referenceId);
		brandCreditHistoryRepo.save(brandCreditHistory);

		return brandCreditHistory;
	}

	private void saveBrandInfluencerBidAllowanceMap(BrandCreditHistory brandCreditHistory, UUID userId, CreditType creditType) {
		BrandCreditAllowance brandCreditAllowance = BrandCreditAllowance.builder()
				.key(BrandCreditAllowanceKey.builder().brandId(brandCreditHistory.getKey().getBrandId())
						.influencerId(userId).creditType(creditType)
						.creditId(brandCreditHistory.getKey().getCreditId()).build())
				.transYrMonth(brandCreditHistory.getKey().getTransYrMonth()).createdAt(new Date()).build();

		brandCreditAllowanceRepo.save(brandCreditAllowance);
	}

	private BrandCredits saveBrandCredits(Optional<BrandCredits> optionalBrandCredits) {
		BrandCredits brandCredits = optionalBrandCredits.get();
		Integer balance = optionalBrandCredits.get().getCredits();
		Integer newBalance = balance - 1;
		brandCredits.setModifiedAt(new Date());
		brandCredits.setCredits(newBalance);
		brandCreditsRepo.save(brandCredits);

		return brandCredits;
	}

	@Override
	public ReportData getReportData(String influencer, String platform, ViewType viewType, UUID searchId) {
		ReportData reportData = getReportIdIfUserExists(influencer, viewType, platform, searchId);

		if (reportData == null) {
			if ((reportData = createReport(influencer, viewType, platform, null, searchId)) == null)
				return null;
		}
		return reportData;
	}

	private ReportData getReportIdIfUserExists(String deepSocialInfluencerId, ViewType viewType, String platform, UUID searchId) {
		ReportData reportData = null;
		Map<String, ArrayList<ReportResult>> reportResult = socialDataProxy.getReportsList(40, platform, deepSocialInfluencerId);

		if (reportResult.containsKey("results") && reportResult.get("results") != null) {
			ArrayList<ReportResult> results = reportResult.get("results");
			for (ReportResult result : results) {
				if (result.getUser_profile().getUserId().equals(deepSocialInfluencerId)
						|| result.getUser_profile().getFullName().equals(deepSocialInfluencerId)
						|| result.getUser_profile().getHandle().equals(deepSocialInfluencerId)) {
					reportData = getReport(result.getId(), viewType, searchId);

					break;
				}
			}
		}
		return reportData;
	}

	private ReportData getReport(String reportId, ViewType viewType, UUID searchId) {
		ReportData reportData = socialDataProxy.getReport(reportId, "json");

		Optional<UserAccounts> userAccounts = userAccountsRepo.findById(ResourceServer.getUserId().toString());
		if (userAccounts.isPresent())
			saveReportSearchHistory(reportData, userAccounts.get(), viewType, false, searchId);

		return reportData;
	}

	@Override
	public ReportData createReport(String deepSocialInfluencerId, ViewType viewType, String platform, String brandId, UUID searchId) {
		log.info("Report not found for {}. Creating new Report Request for {} ", deepSocialInfluencerId);
		ReportData reportResult = socialDataProxy.createNewReport(deepSocialInfluencerId, 0, 1, true, platform);
		if (reportResult.isSuccess() && !Objects.isNull(reportResult.getReportInfo())
				&& !Objects.isNull(reportResult.getReportInfo().getReportId())) {

			String userId = StringUtils.isEmpty(brandId) ? ResourceServer.getUserId().toString() : brandId;
			Optional<UserAccounts> userAccounts = userAccountsRepo.findById(userId);
			if (userAccounts.isPresent())
				saveReportSearchHistory(reportResult, userAccounts.get(), viewType, true, searchId);

			log.info("Report {} generated for {}", reportResult.getReportInfo().getReportId(), reportResult.getUserProfile().getUserId());
			return reportResult;
		} else
			return null;
	}

	private void saveReportSearchHistory(ReportData reportData, UserAccounts userAccounts, ViewType viewType, Boolean isNew, UUID searchId) {
		ReportSearchHistory reportSearchHistory = ReportSearchHistory.builder().key(ReportSearchHistoryKey.builder()

				.brandId(UUID.fromString(userAccounts.getUserId())).isNew(isNew).createdAt(new Date()).build())
				.influencerName(userAccounts.getFirstName())
				.influencerUsername(StringUtils.isEmpty(reportData.getUserProfile().getHandle()) ? reportData.getUserProfile().getFullName().replace(" ", "")
						: reportData.getUserProfile().getHandle()).influencerDeepSocialId(reportData.getUserProfile().getUserId())
				.platform(getPlatform(reportData.getUserProfile().getPlatformType())).brandName(userAccounts.getBrandName())
				.reportId(reportData.getReportInfo().getReportId()).viewType(viewType).searchId(searchId).build();

		saveReportHistory(reportSearchHistory);
	}

	@Override
	public void saveReportHistory(ReportSearchHistory reportSearchHistory) {
		reportSearchHistory.setInfluencerUsername(reportSearchHistory.getInfluencerUsername().startsWith("@")
				? reportSearchHistory.getInfluencerUsername() : "@" + reportSearchHistory.getInfluencerUsername().toLowerCase());
		reportSearchHistoryRepo.save(reportSearchHistory);
	}

	@Override
	public Platforms getPlatform(String platform) {
		switch (platform) {
		case INSTAGRAM:
			return Platforms.INSTAGRAM;
		case YOUTUBE:
			return Platforms.YOUTUBE;
		case TIKTOK:
			return Platforms.TIKTOK;
		}
		return null;
	}

	@SuppressWarnings("unused")
	private Map<String, Date> findStartAndEndDate() throws ParseException {
		Map<String, Date> startAndEndDate = new HashMap<>();
		Calendar calendar = Calendar.getInstance();
		startAndEndDate.put(END_DATE, calendar.getTime());
		Integer day = calendar.get(Calendar.DAY_OF_WEEK);
		switch (day) {
		case Calendar.MONDAY:
			calendar.add(Calendar.DAY_OF_MONTH, 0);
			break;
		case Calendar.TUESDAY:
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			break;
		case Calendar.WEDNESDAY:
			calendar.add(Calendar.DAY_OF_MONTH, -2);
			break;
		case Calendar.THURSDAY:
			calendar.add(Calendar.DAY_OF_MONTH, -3);
			break;
		case Calendar.FRIDAY:
			calendar.add(Calendar.DAY_OF_MONTH, -4);
			break;
		case Calendar.SATURDAY:
			calendar.add(Calendar.DAY_OF_MONTH, -5);
			break;
		case Calendar.SUNDAY:
			calendar.add(Calendar.DAY_OF_MONTH, -6);
			break;
		}
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		startAndEndDate.put(START_DATE, calendar.getTime());
		return startAndEndDate;
	}

	@Override
	public ResponseEntity<byte[]> downloadReport(String influencerHandle, String reportId) {
		String username = influencerHandle.startsWith("@") ? influencerHandle.substring(1) : influencerHandle;
		log.info("Downloading report pdf for {} - {}, downloaded by {}", reportId, username, ResourceServer.getUserId());
		byte[] body = socialDataProxy.downloadReport(reportId, "pdf");

		return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + username + "_audience_report.pdf\"")
				.header(HttpHeaders.SET_COOKIE, "fileDownload=true; Path=/").body(body);
	}

	@Override
	public ResponseEntity<String> exportCampaignCsv(UUID campaignId) {
		UUID brandId = ResourceServer.getUserId();
		List<BrandCampaignInfluencerList> brandCampaignInfluencerList = new LinkedList<>();
		Set<InfluencerSnapshot> influencerSnapshots = new HashSet<>();
		List<CampaignPostLinkModal> campaignPostLinkModals = new LinkedList<>();
		generateCampaignPostLinkDetails(campaignPostLinkModals, brandId, campaignId);
		generateBrandCampaignInfluencerList(campaignId, brandId, brandCampaignInfluencerList);
		generateInfluencerSnapshot(campaignId, brandId, influencerSnapshots);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
		UUID csvName = UUID.randomUUID();
		File csvFile = new File(csvName.toString() + ".csv");
		try (FileWriter out = new FileWriter(csvFile);
				CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(HEADERS))) {
			log.info("CSV File generated at:{}", csvFile.getPath());
			for (BrandCampaignInfluencerList brandCampaignInfluencer : brandCampaignInfluencerList) {
				List<InfluencerSnapshot> filteredInfluencerSnapshots = influencerSnapshots.parallelStream()
						.filter(e -> e.getInfluencerHandle().equals(brandCampaignInfluencer.getInfluencerHandle()))
						.sorted(Comparator.comparing(InfluencerSnapshot::getBidDate)).collect(Collectors.toList());
				if (filteredInfluencerSnapshots.isEmpty()) {
					printer.printRecord(brandCampaignInfluencer.getInfluencerHandle(), brandCampaignInfluencer.getFollowers(),
							brandCampaignInfluencer.getEngagement(), brandCampaignInfluencer.getAudienceCredibility(),
							brandCampaignInfluencer.getInfluencerPricing(), "0", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A",
							brandCampaignInfluencer.getInfluencerEmail(), brandCampaignInfluencer.getPlatform().toUpperCase());
				} else {
					Integer bidCount = 0;
					for (InfluencerSnapshot influencerSnapshot : filteredInfluencerSnapshots) {
						Boolean isPostPresent = false;
						String postedOn = "";
						String postedLink = null;
						for (CampaignPostLinkModal campaignPostLinkModal : campaignPostLinkModals) {
							if (campaignPostLinkModal.getBidId().equals(influencerSnapshot.getBidId())) {
								isPostPresent = true;
								postedLink = campaignPostLinkModal.getPostURL();
								postedOn = campaignPostLinkModal.getPostedOn() == null ? null
										: simpleDateFormat.format(campaignPostLinkModal.getPostedOn());
								break;
							}
						}
						printer.printRecord(influencerSnapshot.getInfluencerHandle(),
								brandCampaignInfluencer.getFollowers(), brandCampaignInfluencer.getEngagement(),
								String.format("%.2f", brandCampaignInfluencer.getAudienceCredibility() * 100) + "%",
								"$" + brandCampaignInfluencer.getInfluencerPricing(), bidCount += 1,
								getBidStatus(influencerSnapshot.getBidStatus()), influencerSnapshot.getPostType(),
								influencerSnapshot.getPostDuration(), influencerSnapshot.getPaymentType(),
								influencerSnapshot.getBidAmount(), simpleDateFormat.format(influencerSnapshot.getBidDateTime()),
								isPostPresent && postedOn != null ? postedOn : "N/A", isPostPresent ? postedLink : "N/A",
								brandCampaignInfluencer.getInfluencerEmail(), brandCampaignInfluencer.getPlatform().toUpperCase());
					}
				}
			}
			printer.flush();
			log.info("File path is {}", csvFile.getAbsolutePath());
			campaignExportToCsvPathRepo.save(CampaignExportToCsvPath.builder().campaignId(campaignId)
					.createdAt(new Date()).csvPath(csvFile.getAbsolutePath()).build());
			return ResponseEntity.ok(csvName.toString());
		} catch (IOException e) {
			log.error("Error occurred due to {}", e);
			return ResponseEntity.badRequest().build();
		}
	}

	private String getBidStatus(BidStatus bidStatus) {
		switch (bidStatus) {
		case NEW:
			return "New";
		case EMAILFOLLOWUP:
			return "Email Follow-up";
		case BIDSENT:
			return "Bid Sent";
		case NOTACTIVE:
			return "Not Active";
		case PAIDINFLUENCER:
			return "Paid Influencer";
		case POSTED:
			return "Posted";
		default:
			return "New";
		}
	}

	@Override
	public void generateCampaignPostLinkDetails(List<CampaignPostLinkModal> campaignPostLinkModal, UUID brandId, UUID campaignId) {
		List<PostAnalyticsTriggers> listCampaignPostLink = postAnalyticsTriggersRepo.findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId);

		if (!listCampaignPostLink.isEmpty()) {
			for (PostAnalyticsTriggers campaignPostLink : listCampaignPostLink) {
				Optional<SocialIdMaster> socialIdMaster = socialIdMasterRepo.findById(campaignPostLink.getKey().getInfluencerId().toString());
				campaignPostLinkModal
						.add(CampaignPostLinkModal.builder().influencerId(campaignPostLink.getKey().getInfluencerId())
								.postName(campaignPostLink.getPostName()).postURL(campaignPostLink.getPostUrl())
								.influencerHandle(campaignPostLink.getInfluencerHandle()).influencerLink(getInfluencerProfileLink(socialIdMaster.get()))
								.bidId(campaignPostLink.getKey().getBidId()).postedOn(campaignPostLink.getPostDataTime()).build());
			}
		}
	}

	@Override
	public void generateBrandCampaignInfluencerList(UUID campaignId, UUID brandId, List<BrandCampaignInfluencerList> brandBrandCampaignInfluencerList) {
		Set<BrandCampaignDetails> brandCampaignDetails = brandCampaignDetailsRepo.findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId);

		for (BrandCampaignDetails brandCampaignDetail : brandCampaignDetails) {
			Optional<SocialIdMaster> socialIdMaster = socialIdMasterRepo.findById(brandCampaignDetail.getKey().getInfluencerId().toString());
			InfluencerDemographicsMapper demographicsMapper = getInfluencerDemographics(socialIdMaster.get(), null, null, false);

			brandBrandCampaignInfluencerList.add(BrandCampaignInfluencerList.builder()
					.audienceCredibility(brandCampaignDetail.getInfluencerAudienceCredibilty())
					.campaignId(campaignId)
					.campaignName(brandCampaignDetail.getCampaignName())
					.engagement(brandCampaignDetail.getInfluencerEngagement())
					.followers(brandCampaignDetail.getInfluencerFollowers())
					.influencerHandle(brandCampaignDetail.getInfluencerHandle())
					.influencerId(brandCampaignDetail.getKey().getInfluencerId())
					.influencerPricing(demographicsMapper.getPricing())
					.platform(socialIdMaster.get().getPlatform().name().toLowerCase())
					.bidCount(brandCampaignDetail.getBidCount())
					.createdAt(brandCampaignDetail.getCreatedAt())
					.influencerEmail(demographicsMapper.getEmail())
					.influencerLink(getInfluencerProfileLink(socialIdMaster.get())).build());
		}
	}

	private String getInfluencerProfileLink(SocialIdMaster socialIdMaster) {
		if (socialIdMaster.getPlatform().equals(Platforms.INSTAGRAM))
			return "https://www.instagram.com/" + socialIdMaster.getInfluencerHandle().substring(1);
		if (socialIdMaster.getPlatform().equals(Platforms.YOUTUBE))
			return "https://www.youtube.com/channel/" + socialIdMaster.getInfluencerId();
		if (socialIdMaster.getPlatform().equals(Platforms.TIKTOK))
			return "https://www.tiktok.com/share/user/" + socialIdMaster.getInfluencerId();
		return null;
	}

	@Override
	public void generateInfluencerSnapshot(UUID campaignId, UUID brandId, Set<InfluencerSnapshot> influencerSnapshots) {
		Set<BrandBidDetails> brandBidDetails = bidDetailsRepo.findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId);
		Map<UUID, SocialIdMaster> influencerIdMap = new HashMap<>();
		for (BrandBidDetails bidDetails : brandBidDetails) {
			UUID influencerId = bidDetails.getKey().getInfluencerId();

			if (ObjectUtils.isEmpty(getSocialId(influencerIdMap, bidDetails, influencerId)))
				continue;

			String bidAmountName = bidDetails.getAffiliateType() != null ? "Affiliate Fee" : "Bid Amount";
			String bidAmount = bidDetails.getAffiliateType() != null && bidDetails.getAffiliateType().equals("%")
					? String.format("%.2f", bidDetails.getBidAmount()) + "%"
					: "$" + String.format("%.2f", bidDetails.getBidAmount());

			InfluencerSnapshot influencerSnapshot = InfluencerSnapshot.builder().bidAmount(bidAmount)
					.bidDate(bidDetails.getCreatedAt()).influencerHandle(bidDetails.getInfluencerHandle())
					.influencerName(bidDetails.getInfluencerHandle()).paymentType(bidDetails.getPaymentType())
					.postDuration(StringUtils.isEmpty(bidDetails.getPostDuration()) ? "N/A"
							: bidDetails.getPostDuration())
					.postType(StringUtils.isEmpty(bidDetails.getPostType()) ? "N/A" : bidDetails.getPostType())
					.bidStatus(bidDetails.getBidStatus()).bidId(bidDetails.getKey().getBidId())
					.influencerId(influencerId).affiliateType(bidDetails.getAffiliateType())
					.bidAmountName(bidAmountName).isPostUrlPresent(bidDetails.getIsPostUrlPresent()).build();
			influencerSnapshots.add(influencerSnapshot);
		}
	}

	public SocialIdMaster getSocialId(Map<UUID, SocialIdMaster> influencerIdMap, BrandBidDetails bidDetails, UUID influencerId) {
		if (!influencerIdMap.keySet().contains(influencerId)) {
			Optional<SocialIdMaster> optionalSocialIdMaster = socialIdMasterRepo.findById(bidDetails.getKey().getInfluencerId().toString());

			if (optionalSocialIdMaster.isPresent())
				influencerIdMap.put(influencerId, optionalSocialIdMaster.get());
			else
				influencerIdMap.put(influencerId, null);
		}
		SocialIdMaster socialIdMaster = influencerIdMap.get(influencerId);
		return socialIdMaster;
	}

	@Override
	public ResponseEntity<byte[]> downloadCampaignCsv(UUID campaignId, UUID csvName) {
		Optional<BrandCampaignDetailsMysql> opBrandCampaignDetailsMysql = brandCampaignDetailsMysqlRepo
				.findByCampaignId(campaignId.toString());
		if (opBrandCampaignDetailsMysql.isPresent()) {
			File file = new File(csvName + ".csv");
			if (file.exists()) {
				try {
					byte[] csv = Files.readAllBytes(file.toPath());
					Files.delete(file.toPath());
					Optional<CampaignExportToCsvPath> optionalCampaignCsvPath = campaignExportToCsvPathRepo
							.findById(campaignId);
					if (optionalCampaignCsvPath.isPresent())
						campaignExportToCsvPathRepo.delete(optionalCampaignCsvPath.get());

					return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
							.header(HttpHeaders.CONTENT_DISPOSITION,
									"attachment; filename=\"" + opBrandCampaignDetailsMysql.get().getCampaignName()
											+ ".csv\"")
							.header(HttpHeaders.SET_COOKIE, "fileDownload=true; Path=/").body(csv);
				} catch (IOException e) {
					e.getStackTrace();
				}
			}
		}
		return ResponseEntity.notFound().build();
	}

	@Override
	public ResponseEntity<byte[]> downloadTemplate() {
		File file = new File(templateName + ".csv");

		if (file.exists()) {
			try {
				byte[] csv = Files.readAllBytes(file.toPath());

				return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
						.header(HttpHeaders.CONTENT_DISPOSITION,
								"attachment; filename=\"" + templateName	+ ".csv\"")
						.header(HttpHeaders.SET_COOKIE, "fileDownload=true; Path=/").body(csv);
			} catch (IOException e) {
				e.getStackTrace();
			}
		}
		return ResponseEntity.notFound().build();
	}

	public void deleteCampaignCsv() {
		List<CampaignExportToCsvPath> campaignExportToCsvPaths = campaignExportToCsvPathRepo.findAll();
		List<CampaignExportToCsvPath> campaigns = new LinkedList<>();
		Calendar currentTime = Calendar.getInstance();
		campaignExportToCsvPaths.forEach(e -> {
			Calendar createdAt = Calendar.getInstance();
			createdAt.setTime(e.getCreatedAt());

			if (currentTime.getTimeInMillis() - createdAt.getTimeInMillis() > 1800000) {
				try {
					Files.deleteIfExists(Paths.get(e.getCsvPath()));
					campaigns.add(e);
				} catch (IOException e1) {
					e1.getStackTrace();
				}
			}
			campaignExportToCsvPathRepo.deleteAll(campaigns);
		});
	}
}