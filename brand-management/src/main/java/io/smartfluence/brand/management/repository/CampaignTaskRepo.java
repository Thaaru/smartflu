package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.CampaignTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CampaignTaskRepo extends JpaRepository<CampaignTask, Long> {
    List<CampaignTask> findByCampaignId(String campaignId);
}