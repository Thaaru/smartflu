package io.smartfluence.mysql.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import io.smartfluence.constants.Platforms;
import io.smartfluence.mysql.entities.primary.InfluencerDataQueueKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "influencer_data_queue")
public class InfluencerDataQueue implements Serializable {

	private static final long serialVersionUID = 2049818702849379274L;

	@EmbeddedId
	private InfluencerDataQueueKey key;

	@Column(name = "campaign_id")
	private String campaignId;

	@Column(name = "brand_name")
	private String brandName;

	@Column(name = "brand_email")
	private String brandEmail;

	@Column(name = "influencer_handle")
	private String influencerHandle;

	@Column(name = "platform")
	@Enumerated(EnumType.STRING)
	private Platforms platform;

	@Column(name = "search_id")
	private String searchId;
}