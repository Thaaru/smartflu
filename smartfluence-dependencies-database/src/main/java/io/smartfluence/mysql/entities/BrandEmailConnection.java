package io.smartfluence.mysql.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "brand_email_connection")
public class BrandEmailConnection implements Serializable {

	private static final long serialVersionUID = 3564978141000500399L;

	@Id
	@Column(name = "brand_id")
	private String brandId;

	@Column(name = "brand_name")
	private String brandName;

	@Column(name = "email")
	private String email;

	@Column(name = "access_token")
	private String accessToken;

	@Column(name = "account_id")
	private String accountId;

	@Column(name = "account_provider")
	private String accountProvider;

	@Column(name = "sync_state")
	private String syncState;

	@CreatedDate
	@Column(name = "created_at")
	private Date createdAt;

	@LastModifiedDate
	@Column(name = "modified_at")
	private Date modifiedAt;

	@Column(name = "status")
	private int status;
}