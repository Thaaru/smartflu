package io.smartfluence.gateway.filter;

public enum ZuulFilterType {

	PRE, POST, ROUTE, ERROR, STATIC;

	public String value() {
		return this.name().toLowerCase();
	}
}