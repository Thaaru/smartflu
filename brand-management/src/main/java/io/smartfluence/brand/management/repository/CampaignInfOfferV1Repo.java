package io.smartfluence.brand.management.repository;


import io.smartfluence.brand.management.entityv1.CampaignInfOfferKey;
import io.smartfluence.brand.management.entityv1.CampaignInfluencerOfferV1Entity;
import io.smartfluence.brand.management.entityv1.CampaignOffersV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CampaignInfOfferV1Repo extends JpaRepository<CampaignInfluencerOfferV1Entity, CampaignInfOfferKey> {

    List<CampaignInfluencerOfferV1Entity> findAllByCampaignOfferInfluencerId_InfluencerIdInAndCampaignOfferInfluencerId_CampaignId(List<String> influencerId, String campaignId);

    @Modifying
    @Query("delete from CampaignInfluencerOfferV1Entity u where u.campaignOfferInfluencerId.campaignId=?2 and u.campaignOfferInfluencerId.influencerId IN (?1) ")
    void deleteAllByInfluencerIdInAndCampaignId(List<String> influencerId, String campaignId);

    @Modifying
    @Query("delete from CampaignInfluencerOfferV1Entity u where u.campaignOfferInfluencerId.campaignId=?1 and u.campaignOfferInfluencerId.influencerId=?2 and u.campaignOfferInfluencerId.offerType=?3")
    void deleteByCampaignIdAndInfluencerIdAndOfferType(String campaignId,String influencerId,String offerType);

    List<CampaignInfluencerOfferV1Entity> findByCampaignOfferInfluencerId_CampaignId(String campaignId);
    List<CampaignInfluencerOfferV1Entity> findByCampaignOfferInfluencerId_CampaignIdAndCampaignOfferInfluencerId_InfluencerId(String campaignId, String influencerId);
}
