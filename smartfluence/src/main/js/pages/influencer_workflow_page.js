import React, { useState } from "react";
import LongText from "../components/long_short_text_component";
import progressStatus from "../components/progressbar_status";
import { updateCampaignDetailsApi } from "../services/api/campaign_details_api";
import { ACCEPTED_MESSAGE, BBB_COLOR, ERROR_MESSAGE, PRIMARY_COLOR, PROPOSAL_STATUS, SMARTFLUENCE_LOGO, STEPS_ACCEPTED_STATUS, SUBMIT_PROPOSAL_BUTTON } from "../utils/string_constants";
import {CardName,CardVLine} from "../components/workflow_cards";
import { PoweredBy } from "../components/powered_by";
import { CProgressBar } from "../components/progress_bar";
import { loaderMethod } from "../components/loader";
import { toast } from "react-toastify";
export default function InfluencerWorkFlowPage({ data, onSubmit, offer, product, deliverables, shipping, payment, terms }) {

    const [loaderComponent, setLoaderComponent] = useState(true);

    const loaderOpen = (status) => {
        if (status == true) {
            setLoaderComponent(status);
        } else {
            setLoaderComponent(status)
        }
    }

    const submitHandler = async (loader) => {
        data.termsConditionStatus = STEPS_ACCEPTED_STATUS;
        data.proposalStatus = PROPOSAL_STATUS.INFLUENCER_ACCEPTED;
        loader(false);
        const result = await updateCampaignDetailsApi(data);
        if (result.code == 600) {
            toast.success(ACCEPTED_MESSAGE, { delay: 1000 })
            loader(true);
            data = result.value
            onSubmit(data);
        } else {
            toast.error(ERROR_MESSAGE, { delay: 1000 })
            loader(true);
            data.termsConditionStatus = null;
            data.proposalStatus = null;
            return null;
        }
    }
    
    return (
        <>
            <div class='content-wrapper'>
                <div>
                    <div>
                        <div className="col">
                            <div className="col-md" >
                                <p className="heading">Overview</p>
                            </div>
                            <div className="col-md" style={{ "textAlign": "left" }}>
                                <LongText content={!data ? "loading...." : data.brandDescription} limit={200} />
                            </div>
                        </div>
                        <div className="col" style={{ "textAlign": "left" }}>
                            <CardName acceptedStatus={data.offerAcceptedStatus} buttonHandler={offer} cardName=" Offers"/>
                            <CardVLine/>
                            <CardName acceptedStatus={data.productsAcceptedStatus} buttonHandler={product} cardName=" Products"/>
                            <CardVLine/>
                            <CardName acceptedStatus={data.deliverablesAcceptedStatus} buttonHandler={deliverables} cardName=" Deliverables"/>
                            <CardVLine/>
                            <CardName acceptedStatus={data.shippingAcceptedStatus} buttonHandler={shipping} cardName=" Shipping Information"/>
                            <CardVLine/>
                            <CardName acceptedStatus={data.paymentAcceptedStatus} buttonHandler={payment} cardName=" Payment Information"/>
                            <CardVLine/>
                            <CardName acceptedStatus={data.termsConditionStatus} buttonHandler={terms} cardName=" Terms & Conditions"/>
                        </div>
                    </div>
                    <div class="footer">
                        <CProgressBar data={data} />
                        <br />
                        <div className="col-md">
                            <button type="button" className="btn btn-primary btn-lg btn-block" style={{ backgroundColor: progressStatus(data) == 100 ? PRIMARY_COLOR : BBB_COLOR, borderBlockColor: progressStatus(data) == 100 ? PRIMARY_COLOR : BBB_COLOR }} disabled={progressStatus(data) == 100 ? false : true} onClick={progressStatus(data) == 100 ? () => { submitHandler(loaderOpen) } : null}>
                                {loaderComponent == true ? SUBMIT_PROPOSAL_BUTTON : loaderMethod()}
                            </button>
                        </div>
                        <PoweredBy/>
                    </div>
                </div>
            </div>
        </>
    );
}
