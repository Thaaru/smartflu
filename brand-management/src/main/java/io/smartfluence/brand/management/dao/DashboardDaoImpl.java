package io.smartfluence.brand.management.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Sets;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.service.ReportsService;
import io.smartfluence.cassandra.entities.BulkMailHistory;
import io.smartfluence.cassandra.entities.CampaignPostLinkDetails;
import io.smartfluence.cassandra.entities.InfluencerBulkMail;
import io.smartfluence.cassandra.entities.PostAnalyticsData;
import io.smartfluence.cassandra.entities.PostAnalyticsTriggers;
import io.smartfluence.cassandra.entities.brand.BrandBidDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignHistory;
import io.smartfluence.cassandra.entities.brand.BrandCampaignInfluencerHistory;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandMailTemplates;
import io.smartfluence.cassandra.entities.brand.primary.BrandBidDetailsKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignHistoryKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignInfluencerHistoryKey;
import io.smartfluence.cassandra.entities.brand.primary.BrandMailTemplatesKey;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.primary.PostAnalyticsTriggersKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.repository.brand.BrandBidDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignHistoryRepo;
import io.smartfluence.cassandra.repository.brand.BrandCampaignInfluencerHistoryRepo;
import io.smartfluence.cassandra.repository.brand.BrandCreditsRepo;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandMailTemplatesRepo;
import io.smartfluence.cassandra.repository.BulkMailHistoryRepo;
import io.smartfluence.cassandra.repository.CampaignPostLinkDetailsRepo;
import io.smartfluence.cassandra.repository.InfluencerBulkMailRepo;
import io.smartfluence.cassandra.repository.PostAnalyticsDataRepo;
import io.smartfluence.cassandra.repository.PostAnalyticsTriggersRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramDemographicRepo;
import io.smartfluence.constants.BidStatus;
import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.validation.brand.dashboard.MailTemplateModal;
import io.smartfluence.modal.validation.brand.dashboard.SaveCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SavePostLink;
import io.smartfluence.modal.validation.brand.dashboard.SaveStatusModel;
import io.smartfluence.mysql.entities.BidStatusMapping;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.CampaignInfluencerMapping;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.BidStatusMappingRepo;
import io.smartfluence.mysql.repository.BrandCampaignDetailsMysqlRepo;
import io.smartfluence.mysql.repository.CampaignInfluencerMappingRepo;
import io.smartfluence.mysql.repository.MailDetailsRepo;
import io.smartfluence.mysql.repository.SocialIdMasterRepo;
import io.smartfluence.mysql.repository.UserAccountsRepo;

@Repository
public class DashboardDaoImpl implements DashboardDao {

	private static final int CAMPAIGN_PER_INFLUENCER = 3;

	@Autowired
	private ReportsService reportsService;

	@Autowired
	private SocialIdMasterRepo socialIdMasterRepo;

	@Autowired
	private InstagramDemographicRepo instagramDemographicRepo;

	@Autowired
	private BrandBidDetailsRepo bidDetailsRepo;

	@Autowired
	private BrandCreditsRepo brandCreditsRepo;

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Autowired
	private BrandCampaignHistoryRepo brandCampaignHistoryRepo;

	@Autowired
	private BrandCampaignDetailsRepo brandCampaignDetailsRepo;

	@Autowired
	private BrandCampaignDetailsMysqlRepo brandCampaignDetailsMysqlRepo;

	@Autowired
	private CampaignInfluencerMappingRepo campaignInfluencerMappingRepo;

	@Autowired
	private BrandCampaignInfluencerHistoryRepo brandCampaignInfluencerHistoryRepo;

	@Autowired
	private BidStatusMappingRepo bidStatusMappingRepo;

	@Autowired
	private PostAnalyticsTriggersRepo postAnalyticsTriggersRepo;

	@Autowired
	private CampaignPostLinkDetailsRepo campaignPostLinkDetailsRepo;

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private PostAnalyticsDataRepo postAnalyticsDataRepo;

	@Autowired
	private BrandMailTemplatesRepo brandMailTemplatesRepo;

	@Autowired
	private BulkMailHistoryRepo bulkMailHistoryRepo;

	@Autowired
	private InfluencerBulkMailRepo influencerBulkMailRepo;

	@Autowired
	private MailDetailsRepo mailDetailsRepo;

	@Override
	public Optional<BrandCredits> getBrandCreditsById(UUID brandId) {
		return brandCreditsRepo.findById(brandId);
	}

	@Override
	public List<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysql(UUID brandId, boolean isAllCampaign) {
		Set<CampaignStatus> campaignStatus = Sets.newHashSet(CampaignStatus.NEW, CampaignStatus.INPROGRESS, CampaignStatus.PAUSED);
		return isAllCampaign ? brandCampaignDetailsMysqlRepo.findAllByBrandIdAndCampaignStatusIn(brandId.toString(), campaignStatus)
				: brandCampaignDetailsMysqlRepo.findAllByBrandIdAndIsPromoteAndCampaignStatusIn(brandId.toString(), isAllCampaign, campaignStatus);
	}

	@Override
	public Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByCampaignId(String campaignId) {
		return brandCampaignDetailsMysqlRepo.findByCampaignId(campaignId);
	}

	@Override
	public Optional<CampaignPostLinkDetails> getPostDetails(String postURL) {
		return campaignPostLinkDetailsRepo.findById(postURL);
	}

	@Override
	public Optional<SocialIdMaster> getSocialIdMasterById(String influencerId) {
		return socialIdMasterRepo.findById(influencerId);
	}

	@Override
	public Optional<SocialIdMaster> getSocialIdMasterByHandleAndPlatform(String influencerHandle, Platforms platform) {
		return socialIdMasterRepo.findByInfluencerHandleAndPlatform(influencerHandle, platform);
	}

	@Override
	public List<BrandBidDetails> getBrandBidDetailsByKey(UUID brandId, UUID campaignId, UUID influencerId) {
		return bidDetailsRepo.findAllByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(brandId, campaignId, influencerId);
	}

	@Override
	public void saveBrandMailTemplates(BrandMailTemplates brandMailTemplates) {
		brandMailTemplatesRepo.save(brandMailTemplates);
	}

	@Override
	public Optional<BrandCampaignDetails> getBrandCampaignDetailsByBrandIdAndCampaignIdAndInfluencerId(UUID brandId, UUID campaignId, UUID influencerId) {
		return brandCampaignDetailsRepo.findByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(brandId, campaignId, influencerId);
	}

	private void saveCampaignPostLinkDetails(CampaignPostLinkDetails campaignPostLinkDetails) {
		campaignPostLinkDetailsRepo.save(campaignPostLinkDetails);
	}

	@Override
	public Optional<BrandMailTemplates> getBrandMailTemplatesByName(String templateName) {
		BrandMailTemplatesKey brandMailTemplatesKey = BrandMailTemplatesKey.builder()
				.brandId(ResourceServer.getUserId()).templateName(templateName).build();
		return brandMailTemplatesRepo.findById(brandMailTemplatesKey);
	}

	@Override
	public ResponseEntity<Object> saveCampaign(SaveCampaignModel saveCampaignModel) {
		UUID brandId = ResourceServer.getUserId();
		Optional<BrandDetails> brandDetails = getBrandDetails(new UserDetailsKey(UserStatus.APPROVED, brandId));

		if (brandDetails.isPresent()) {
			Long brandCampaignsCount = brandCampaignDetailsMysqlRepo.countByBrandId(brandId.toString());

			if (ResourceServer.isEnterpriseBrand() || brandCampaignsCount < CAMPAIGN_PER_INFLUENCER) {
				Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetails =
						getBrandCampaignDetailsByCampaignNameAndBrandId(saveCampaignModel.getCampaignName(), brandId.toString());

				if (optionalBrandCampaignDetails.isPresent())
					return ResponseEntity.status(HttpStatus.CONFLICT).body("This Campaign Name Already Exists");

				BrandCampaignDetailsMysql brandCampaignDetailsMysql = saveCampaign(brandDetails, saveCampaignModel,
						brandId);
				saveBrandCampaignHistory(brandCampaignDetailsMysql, brandDetails);
				return ResponseEntity.ok(saveCampaignModel);
			} else
				return ResponseEntity.status(HttpStatus.CONFLICT).body(
						"Not more than 3 Campaigns can be created! Contact us for upgrading to the Enterprise plan.");
		}
		return ResponseEntity.notFound().build();
	}

	private BrandCampaignDetailsMysql saveCampaign(Optional<BrandDetails> brandDetails,
			SaveCampaignModel saveCampaignModel, UUID brandId) {
		UUID campaignId = UUID.randomUUID();

		BrandCampaignDetailsMysql brandCampaignDetailsMysql = BrandCampaignDetailsMysql.builder()
				.brandId(brandId.toString()).campaignId(campaignId.toString())
				.campaignName(saveCampaignModel.getCampaignName()).campaignStatus(CampaignStatus.NEW)
				.createdAt(new Date()).influencerCount(0).isPromote(false).build();

		saveBrandCampaignDetailsMysql(brandCampaignDetailsMysql);
		return brandCampaignDetailsMysql;
	}

	private void saveBrandCampaignHistory(BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			Optional<BrandDetails> brandDetails) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		String date = format.format(new Date());
		brandCampaignHistoryRepo.save(BrandCampaignHistory.builder().brandName(brandDetails.get().getBrandName())
				.campaignName(brandCampaignDetailsMysql.getCampaignName()).createdAt(new Date())
				.key(BrandCampaignHistoryKey.builder().brandId(UUID.fromString(brandCampaignDetailsMysql.getBrandId()))
						.campaignId(UUID.fromString(brandCampaignDetailsMysql.getCampaignId())).transYrMonth(date)
						.modifiedAt(new Date()).build())
				.build());
	}

	@Override
	public ResponseEntity<Object> saveStatus(SaveStatusModel saveStatusModel, UUID campaignId) {
		UUID brandId = ResourceServer.getUserId();
		Optional<BidStatusMapping> optionalBidStatusMapping = bidStatusMappingRepo
				.findById(saveStatusModel.getBidId().toString());
		Optional<BrandBidDetails> optionalBrandBidDetails = bidDetailsRepo
				.findByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerIdAndKeyBidId(brandId, campaignId,
						saveStatusModel.getInfluencerId(), saveStatusModel.getBidId());
		if (optionalBidStatusMapping.isPresent()) {
			if (optionalBrandBidDetails.isPresent()) {
				BidStatusMapping bidStatusMapping = optionalBidStatusMapping.get();
				bidStatusMapping.setBidStatus(saveStatusModel.getStatus());
				bidStatusMappingRepo.save(bidStatusMapping);
				BrandBidDetails brandBidDetails = optionalBrandBidDetails.get();
				brandBidDetails.setBidStatus(bidStatusMapping.getBidStatus());
				bidDetailsRepo.save(brandBidDetails);
				return ResponseEntity.ok(saveStatusModel);
			} else
				return ResponseEntity.notFound().build();
		} else
			return ResponseEntity.notFound().build();
	}

	@Override
	public boolean removeInfluencerFromCampaign(UUID campaignId, UUID influencerId, boolean notFound) {
		UUID brandId = ResourceServer.getUserId();

		notFound = deleteBrandCampaignDetailsMysql(campaignId, notFound);
		notFound = deleteCampaignInfluencerMapping(campaignId, influencerId, notFound);
		notFound = deleteBrandCampaignDetails(campaignId, influencerId, notFound, brandId);
		deletePostAnalyticsData(campaignId, influencerId, brandId);
		deletePostAnalyticsTriggers(campaignId, influencerId, brandId);
		deleteBrandBidDetails(campaignId, influencerId, brandId);

		return notFound;
	}

	private boolean deleteBrandCampaignDetailsMysql(UUID campaignId, boolean notFound) {
		Optional<BrandCampaignDetailsMysql> optionalBrandCampaign = brandCampaignDetailsMysqlRepo
				.findById(campaignId.toString());
		if (optionalBrandCampaign.isPresent()) {
			BrandCampaignDetailsMysql brandCampaignMaster = optionalBrandCampaign.get();
			brandCampaignMaster.reduceInfluencer();
			saveBrandCampaignDetailsMysql(brandCampaignMaster);
		} else
			notFound = true;
		return notFound;
	}

	private boolean deleteCampaignInfluencerMapping(UUID campaignId, UUID influencerId, boolean notFound) {
		Optional<CampaignInfluencerMapping> campaignInfluencerMappings = getBrandCampaignDetailsByCampaignIdAndInfluencerId
				(campaignId.toString(), influencerId.toString());
		if (campaignInfluencerMappings.isPresent())
			campaignInfluencerMappingRepo.delete(campaignInfluencerMappings.get());
		else
			notFound = true;
		return notFound;
	}

	private boolean deleteBrandCampaignDetails(UUID campaignId, UUID influencerId, boolean notFound, UUID brandId) {
		Optional<BrandCampaignDetails> brandCampaignDetailsOptional = 
				getBrandCampaignDetailsByBrandIdAndCampaignIdAndInfluencerId(brandId, campaignId, influencerId);
		if (brandCampaignDetailsOptional.isPresent()) {
			BrandCampaignDetails brandCampaignDetails = brandCampaignDetailsOptional.get();
			brandCampaignDetailsRepo.delete(brandCampaignDetails);
			saveHistory(brandCampaignDetails);
		} else
			notFound = true;
		return notFound;
	}

	private void deletePostAnalyticsData(UUID campaignId, UUID influencerId, UUID brandId) {
		List<PostAnalyticsData> postAnalyticsDatas = postAnalyticsDataRepo
				.findAllByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(brandId, campaignId, influencerId);
		if (!postAnalyticsDatas.isEmpty()) {
			postAnalyticsDatas.parallelStream().forEach(e -> {
				postAnalyticsDataRepo.delete(e);
			});
		}
	}

	private void deletePostAnalyticsTriggers(UUID campaignId, UUID influencerId, UUID brandId) {
		List<PostAnalyticsTriggers> postAnalyticsTriggers = postAnalyticsTriggersRepo
				.findAllByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(brandId, campaignId, influencerId);
		if (!postAnalyticsTriggers.isEmpty()) {
			postAnalyticsTriggers.parallelStream().forEach(e -> {
				campaignPostLinkDetailsRepo.deleteById(e.getPostUrl());
				postAnalyticsTriggersRepo.delete(e);
			});
		}
	}

	private void deleteBrandBidDetails(UUID campaignId, UUID influencerId, UUID brandId) {
		List<BrandBidDetails> brandBidDetailsList = bidDetailsRepo
				.findAllByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(brandId, campaignId, influencerId);
		if (!brandBidDetailsList.isEmpty()) {
			for (BrandBidDetails brandBidDetails : brandBidDetailsList) {
				bidDetailsRepo.delete(brandBidDetails);
			}
		}
	}

	private void saveHistory(BrandCampaignDetails brandCampaignDetails) {
		BrandCampaignInfluencerHistory brandCampaignInfluencerHistory = BrandCampaignInfluencerHistory.builder()
				.brandName(brandCampaignDetails.getBrandName()).campaignName(brandCampaignDetails.getCampaignName())
				.createdAt(brandCampaignDetails.getCreatedAt())
				.key(BrandCampaignInfluencerHistoryKey.builder().brandId(brandCampaignDetails.getKey().getBrandId())
						.campaignId(brandCampaignDetails.getKey().getCampaignId())
						.influencerId(brandCampaignDetails.getKey().getInfluencerId()).build())
				.influencerAudienceCredibilty(brandCampaignDetails.getInfluencerAudienceCredibilty())
				.influencerEngagement(brandCampaignDetails.getInfluencerEngagement())
				.influencerFirstName(brandCampaignDetails.getInfluencerFirstName())
				.influencerFollowers(brandCampaignDetails.getInfluencerFollowers())
				.influencerLastName(brandCampaignDetails.getInfluencerLastName())
				.influencerHandle(brandCampaignDetails.getInfluencerHandle()).build();
		saveBrandCampaignInfluencerHistory(brandCampaignInfluencerHistory);
	}

	@Override
	public ResponseEntity<Object> savePostLink(SavePostLink savePostLink, SocialIdMaster socialIdMaster) {
		return savePostLink(savePostLink, ResourceServer.getUserId(), socialIdMaster) ? ResponseEntity.ok().build()
				: ResponseEntity.status(HttpStatus.NOT_FOUND).body("The link is invalid. Please provide a valid link!");
	}

	@Override
	public ResponseEntity<Object> savePostLinkNew(SavePostLink savePostLink, SocialIdMaster socialIdMaster) {
		return savePostLinkNew(savePostLink, ResourceServer.getUserId(), socialIdMaster) ? ResponseEntity.ok().build()
				: ResponseEntity.status(HttpStatus.NOT_FOUND).body("The link is invalid. Please provide a valid link!");
	}

	private Boolean savePostLinkNew(SavePostLink savePostLink, UUID brandId, SocialIdMaster socialIdMaster) {
		Boolean flag = false;
		Optional<UserAccounts> userAccounts = userAccountsRepo.findById(brandId.toString());
		UserDetailsKey userDetailsKey = new UserDetailsKey(UserStatus.APPROVED, brandId);
		Optional<BrandDetails> brandDetails = getBrandDetails(userDetailsKey);

		if (userAccounts.isPresent()) {
			UUID postId = UUID.randomUUID();
			savePostLink.setBidId(savePostLink.getIsNew() ? UUID.randomUUID() : savePostLink.getBidId());

			postAnalyticsTriggersRepo.save(PostAnalyticsTriggers.builder()
					.createdAt(new Date()).brandName(userAccounts.get().getBrandName())
					.key(PostAnalyticsTriggersKey.builder().brandId(brandId).campaignId(savePostLink.getCampaignId())
							.influencerId(savePostLink.getInfluencerId()).bidId(savePostLink.getBidId()).build())
					.postUrl(savePostLink.getPostURL()).postName(savePostLink.getPostName()).postId(postId).platform(socialIdMaster.getPlatform())
					.postTimeHourIndex(0).isTriggerEnd(false).influencerHandle(socialIdMaster.getInfluencerHandle()).build());

			saveCampaignPostLinkDetails(CampaignPostLinkDetails.builder().brandId(brandId).campaignId(savePostLink.getCampaignId())
					.createdAt(new Date()).influencerId(savePostLink.getInfluencerId())
					.postUrl(savePostLink.getPostURL()).postName(savePostLink.getPostName()).postId(postId)
					.influencerHandle(socialIdMaster.getInfluencerHandle()).build());

			bidStatusMappingRepo.save(BidStatusMapping.builder().bidId(savePostLink.getBidId().toString())
					.bidStatus(savePostLink.getIsNew() ? BidStatus.NEW : BidStatus.POSTED).build());

			if (brandDetails.isPresent())
				flag = saveBrandBidDetails(savePostLink, brandDetails.get(), socialIdMaster);
		}
		return flag;
	}

	private Boolean savePostLink(SavePostLink savePostLink, UUID brandId, SocialIdMaster socialIdMaster) {
		Boolean flag = false;
		Optional<UserAccounts> userAccounts = userAccountsRepo.findById(brandId.toString());
		UserDetailsKey userDetailsKey = new UserDetailsKey(UserStatus.APPROVED, brandId);
		Optional<BrandDetails> brandDetails = getBrandDetails(userDetailsKey);

		if (userAccounts.isPresent()) {
			UUID postId = UUID.randomUUID();
			savePostLink.setBidId(savePostLink.getIsNew() ? UUID.randomUUID() : savePostLink.getBidId());

			postAnalyticsTriggersRepo.save(PostAnalyticsTriggers.builder()
					.createdAt(new Date()).brandName(userAccounts.get().getBrandName())
					.key(PostAnalyticsTriggersKey.builder().brandId(brandId).campaignId(savePostLink.getCampaignId())
							.influencerId(savePostLink.getInfluencerId()).bidId(savePostLink.getBidId()).build())
					.postUrl(savePostLink.getPostURL()).postName(savePostLink.getPostName()).postId(postId).platform(socialIdMaster.getPlatform())
					.postTimeHourIndex(0).isTriggerEnd(false).influencerHandle(socialIdMaster.getInfluencerHandle()).build());

			saveCampaignPostLinkDetails(CampaignPostLinkDetails.builder().brandId(brandId).campaignId(savePostLink.getCampaignId())
							.createdAt(new Date()).influencerId(savePostLink.getInfluencerId())
							.postUrl(savePostLink.getPostURL()).postName(savePostLink.getPostName()).postId(postId)
							.influencerHandle(socialIdMaster.getInfluencerHandle()).build());

			bidStatusMappingRepo.save(BidStatusMapping.builder().bidId(savePostLink.getBidId().toString())
					.bidStatus(savePostLink.getIsNew() ? BidStatus.NEW : BidStatus.POSTED).build());

			if (brandDetails.isPresent())
				flag = saveBrandBidDetails(savePostLink, brandDetails.get(), socialIdMaster);
		}
		return flag;
	}

	private Boolean saveBrandBidDetails(SavePostLink savePostLink, BrandDetails brandDetails, SocialIdMaster socialIdMaster) {
		Optional<BrandCampaignDetailsMysql> brandCampaignDetails = brandCampaignDetailsMysqlRepo
				.findById(savePostLink.getCampaignId().toString());

		if (brandCampaignDetails.isPresent()) {
			InfluencerDemographicsMapper influencerDemographics = reportsService.getInfluencerDemographics(socialIdMaster, null, null, false);

			BrandBidDetails brandBidDetails = getBrandBidDetails(savePostLink, brandDetails,
					brandCampaignDetails.get(), influencerDemographics);

			brandBidDetails.setBidAmount(savePostLink.getBidAmount());
			brandBidDetails.setIsPostUrlPresent(true);
			brandBidDetails.setAffiliateType(
					savePostLink.getPaymentMode().isEmpty() ? null : savePostLink.getPaymentMode());
			brandBidDetails.setPostDuration(
					savePostLink.getPostDuration().isEmpty() ? null : savePostLink.getPostDuration());
			brandBidDetails.setPaymentType(savePostLink.getPaymentType());
			brandBidDetails.setPostType(savePostLink.getPostType().isEmpty() ? null : savePostLink.getPostType());

			bidDetailsRepo.save(brandBidDetails);

			if (savePostLink.getIsNew())
				updateBidCount(brandBidDetails);
		}

		return true;
	}

	private BrandBidDetails getBrandBidDetails(SavePostLink savePostLink, BrandDetails brandDetails,
			BrandCampaignDetailsMysql brandCampaignDetails, InfluencerDemographicsMapper influencerDemographics) {

		BrandBidDetails brandBidDetails = new BrandBidDetails();
		BrandBidDetailsKey brandBidDetailsKey = new BrandBidDetailsKey(brandDetails.getKey().getUserId(),
				savePostLink.getCampaignId(), savePostLink.getInfluencerId(), savePostLink.getBidId(),
				reportsService.getPlatform(savePostLink.getPlatform()));

		if (!savePostLink.getIsNew()) {
			brandBidDetails = bidDetailsRepo.findById(brandBidDetailsKey).get();
			brandBidDetails.setModifiedAt(new Date());
		} else {
			brandBidDetails.setKey(brandBidDetailsKey);
			brandBidDetails.setCampaignName(brandCampaignDetails.getCampaignName());
			brandBidDetails.setBrandEmail(brandDetails.getEmail());
			brandBidDetails.setBrandIndustry(brandDetails.getIndustry());
			brandBidDetails.setBrandInstagramHandle(brandDetails.getInstagramHandle());
			brandBidDetails.setBrandName(brandDetails.getBrandName());
			brandBidDetails.setBrandStatus(brandDetails.getKey().getUserStatus());
			brandBidDetails.setInfluencerMail(influencerDemographics.getEmail());
			brandBidDetails.setInfluencerHandle(influencerDemographics.getInfluencerHandle());
			brandBidDetails.setBidStatus(BidStatus.NEW);
			brandBidDetails.setCreatedAt(new Date());
		}
		return brandBidDetails;
	}

	public Boolean updateBidCount(BrandBidDetails brandBidDetails) {
		Boolean flag = false;
		Optional<BrandCampaignDetails> optionalBrandCampaignDetails = getBrandCampaignDetailsByBrandIdAndCampaignIdAndInfluencerId
				(brandBidDetails.getKey().getBrandId(), brandBidDetails.getKey().getCampaignId(), brandBidDetails.getKey().getInfluencerId());

		if (optionalBrandCampaignDetails.isPresent()) {
			Optional<CampaignInfluencerMapping> optionalCampaignInfluencerMapping = getBrandCampaignDetailsByCampaignIdAndInfluencerId
					(brandBidDetails.getKey().getCampaignId().toString(),
							brandBidDetails.getKey().getInfluencerId().toString());

			if (optionalCampaignInfluencerMapping.isPresent()) {
				flag = true;
				BrandCampaignDetails brandCampaignDetails = optionalBrandCampaignDetails.get();
				brandCampaignDetails.setBidCount(brandCampaignDetails.getBidCount() + 1);
				saveBrandCampaignDetails(brandCampaignDetails);

				CampaignInfluencerMapping campaignInfluencerMapping = optionalCampaignInfluencerMapping.get();
				campaignInfluencerMapping.setBidCount(brandCampaignDetails.getBidCount());
				saveCampaignInfluencerMapping(campaignInfluencerMapping);
			}
		}
		return flag;
	}

	@Override
	public ResponseEntity<Object> updateMailTemplate(MailTemplateModal mailTemplateModal, Date createdAt) {
		BrandMailTemplates brandMailTemplates = BrandMailTemplates.builder()
				.key(BrandMailTemplatesKey.builder().templateName(mailTemplateModal.getTemplateName())
						.brandId(ResourceServer.getUserId()).build())
				.templateId(UUID.fromString(mailTemplateModal.getTemplateId()))
				.templateBody(mailTemplateModal.getTemplateBody())
				.templateSubject(mailTemplateModal.getTemplateSubject()).createdAt(createdAt).modifiedAt(new Date())
				.build();
		brandMailTemplatesRepo.save(brandMailTemplates);
		return ResponseEntity.ok(brandMailTemplates);
	}

	@Override
	public List<BrandMailTemplates> getAllBrandMailTemplates() {
		UUID brandId = ResourceServer.getUserId();
		return brandMailTemplatesRepo.findByKeyBrandId(brandId);
	}

	@Override
	public void deleteMailTemplate(BrandMailTemplatesKey brandMailTemplatesKey) {
		brandMailTemplatesRepo.deleteById(brandMailTemplatesKey);
	}

	@Override
	public List<InfluencerDemographicsMapper> getAllInfluencerDemographics(List<String> influencerIds) {
		List<SocialIdMaster> socialIdMaster = socialIdMasterRepo.findAllByUserIdIn(influencerIds);
		return getAllSocialIdMasterData(socialIdMaster);
	}

	private List<InfluencerDemographicsMapper> getAllSocialIdMasterData(List<SocialIdMaster> socialIdMaster) {
		List<String> instagramHandles = new LinkedList<>();
		List<String> youtubeHandles = new LinkedList<>();
		List<String> tiktokHandles = new LinkedList<>();

		socialIdMaster.parallelStream().filter(f -> f.getPlatform().equals(Platforms.INSTAGRAM))
				.forEach(e -> instagramHandles.add(e.getInfluencerHandle()));
		socialIdMaster.parallelStream().filter(f -> f.getPlatform().equals(Platforms.YOUTUBE))
				.forEach(e -> youtubeHandles.add(e.getInfluencerHandle()));
		socialIdMaster.parallelStream().filter(f -> f.getPlatform().equals(Platforms.TIKTOK))
				.forEach(e -> tiktokHandles.add(e.getInfluencerHandle()));

		return reportsService.getAllInfluencerDemographics(instagramHandles, youtubeHandles, tiktokHandles);
	}

	@Override
	public UserAccounts getUserAccounts() {
		return userAccountsRepo.findById(ResourceServer.getUserId().toString()).get();
	}

	@Override
	public Optional<BrandDetails> getBrandDetails(UserDetailsKey key) {
		return brandDetailsRepo.findById(key);
	}

	@Override
	public Optional<InstagramDemographics> getInstagramDemographics(String instagramHandle) {
		return instagramDemographicRepo.findById(instagramHandle);
	}

	@Override
	public void saveBrandCampaignDetailsMysql(BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		brandCampaignDetailsMysqlRepo.save(brandCampaignDetailsMysql);
	}

	@Override
	public Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsByCampaignIdAndBrandId(String campaignId, String brandId) {
		return brandCampaignDetailsMysqlRepo.findByCampaignIdAndBrandId(campaignId, brandId);
	}

	@Override
	public Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsByCampaignNameAndBrandId(String campaignName, String brandId) {
		return brandCampaignDetailsMysqlRepo.findByCampaignNameAndBrandId(campaignName, brandId);
	}

	@Override
	public Optional<CampaignInfluencerMapping> getBrandCampaignDetailsByCampaignIdAndInfluencerId(String campaignId, String influencerId) {
		return campaignInfluencerMappingRepo.findByBrandCampaignDetailsCampaignIdAndInfluencerId(campaignId, influencerId);
	}

	@Override
	public void saveBrandCampaignDetails(BrandCampaignDetails brandCampaignDetails) {
		brandCampaignDetailsRepo.save(brandCampaignDetails);
	}

	@Override
	public void saveBrandCampaignInfluencerHistory(BrandCampaignInfluencerHistory brandCampaignInfluencerHistory) {
		brandCampaignInfluencerHistoryRepo.save(brandCampaignInfluencerHistory);
	}

	@Override
	public void saveCampaignInfluencerMapping(CampaignInfluencerMapping campaignInfluencerMapping) {
		campaignInfluencerMappingRepo.save(campaignInfluencerMapping);
	}

	@Override
	public void saveBulkMailHistory(BulkMailHistory bulkMailHistory) {
		bulkMailHistoryRepo.save(bulkMailHistory);
	}

	@Override
	public void saveInfluencerBulkMail(InfluencerBulkMail influencerBulkMail) {
		influencerBulkMailRepo.save(influencerBulkMail);
	}

	@Override
	public Set<BrandCampaignDetails> getBrandCampaignDetailsByBrandIdAndCampaignId(UUID brandId, UUID campaignId) {
		return brandCampaignDetailsRepo.findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId); 
	}

	@Override
	public void deleteBrandCampaignDetailsMySqlByCampaignId(String campaignId) {
		mailDetailsRepo.deleteAllByCampaignId(campaignId); 
	}
}