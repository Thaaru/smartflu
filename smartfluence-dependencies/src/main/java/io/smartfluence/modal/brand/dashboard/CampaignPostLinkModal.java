package io.smartfluence.modal.brand.dashboard;

import java.util.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CampaignPostLinkModal {

	private UUID influencerId;

	private String postName;

	private String postURL;

	private String influencerHandle;

	private UUID bidId;

	private Date postedOn;

	private String influencerLink;
}