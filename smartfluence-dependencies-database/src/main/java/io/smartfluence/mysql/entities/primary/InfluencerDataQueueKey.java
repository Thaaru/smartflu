package io.smartfluence.mysql.entities.primary;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import io.smartfluence.constants.ViewType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@Embeddable
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerDataQueueKey implements Serializable {

	private static final long serialVersionUID = -4702552027211772656L;

	@Column(name = "brand_id")
	private String brandId;

	@Column(name = "influencer")
	private String influencer;

	@Column(name = "view_type")
	@Enumerated(EnumType.STRING)
	private ViewType viewType;
}