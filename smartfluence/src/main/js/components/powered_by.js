import React from "react";
import { SMARTFLUENCE_LOGO } from "../utils/string_constants";

export const PoweredBy = () => {
    return <div className="col-md" style={{ "textAlign": "end" }}>
        <li style={{ "listStyleType": "none" }}>Powered by <img src={SMARTFLUENCE_LOGO} height="25px" /></li>
    </div>
}