package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CampaignNameIndexKey implements Serializable {

	private static final long serialVersionUID = 909034011179888776L;

	public CampaignNameIndexKey(CampaignNameIndexKey key) {
		this.campaignName = key.getCampaignName();
		this.brandId = key.getBrandId();
	}

	@PrimaryKeyColumn(name = "campaign_name", type = PrimaryKeyType.PARTITIONED)
	private String campaignName;

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID brandId;
}
