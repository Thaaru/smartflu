package io.smartfluence.cassandra.entities;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;

import io.smartfluence.cassandra.entities.primary.PostAnalyticsCommentsDataKey;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
//@Table("post_analytics_comments_data")
public class PostAnalyticsCommentsData implements Copyable {

	private static final long serialVersionUID = -8848198542445167970L;

	public PostAnalyticsCommentsData(PostAnalyticsCommentsData postAnalyticsCommentsData) {
		this.updatedDate = postAnalyticsCommentsData.getUpdatedDate();
		this.key = new PostAnalyticsCommentsDataKey(postAnalyticsCommentsData.getKey());
		this.commentLikes = postAnalyticsCommentsData.getCommentLikes();
		this.commentMasterId = postAnalyticsCommentsData.getCommentMasterId();
		this.commentor = postAnalyticsCommentsData.getCommentor();
		this.commentText = postAnalyticsCommentsData.getCommentText();
		this.commentTime = postAnalyticsCommentsData.getCommentTime();
		this.postUrl = postAnalyticsCommentsData.getPostUrl();
	}

	@PrimaryKey
	private PostAnalyticsCommentsDataKey key;

	@Column("comment_master_id")
	private String commentMasterId;

	@Column("post_url")
	private String postUrl;

	@Column("comment_text")
	private String commentText;

	@Column("commentor")
	private String commentor;

	@Column("comment_likes")
	private Integer commentLikes;

	@Column("comment_time")
	private Date commentTime;

	@Column("updated_date")
	private Date updatedDate;

	@Column("influencer_handle")
	private String influencerHandle;

	@Column("post_name")
	private String postName;
}