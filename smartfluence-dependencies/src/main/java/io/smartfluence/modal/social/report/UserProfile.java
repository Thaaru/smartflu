package io.smartfluence.modal.social.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfile implements Serializable {

	private static final long serialVersionUID = 4272346070842736936L;

	@JsonAlias("type")
	private String platformType;

	@JsonAlias("user_id")
	private String userId;

	@JsonAlias("username")
	private String handle = "";

	@JsonAlias("url")
	private String url = "";

	@JsonAlias("picture")
	private String profileImage = "";

	@JsonAlias("fullname")
	private String fullName = "";

	@JsonAlias("description")
	private String description = "";

	@JsonAlias("is_verified")
	private Boolean isVerified;

	@JsonAlias("is_business")
	private Boolean isBusiness;

	@JsonAlias("is_hidden")
	private Boolean isHidden;

	@JsonAlias("followers")
	private Double followers = 0.0;

	@JsonAlias("engagements")
	private Double engagement = 0.0;

	@JsonAlias("engagement_rate")
	private Double engagementRate = 0.0;

	@JsonAlias("top_posts")
	private List<TopPosts> topPosts = new ArrayList<>();

	@JsonAlias("recent_posts")
	private List<TopPosts> recentPosts = new ArrayList<>();

	@JsonAlias("avg_likes")
	private Double avgLikes = 0.0;

	@JsonAlias("avg_comments")
	private Double avgComments = 0.0;

	@JsonAlias("avg_views")
	private Double avgViews = 0.0;

	@JsonAlias("avg_dislikes")
	private Double avgDislikes = 0.0;

	@JsonAlias("contacts")
	private List<Contacts> contacts = new ArrayList<>();

	@JsonAlias("similar_users")
	private List<SimilarUsers> similarUsers = new ArrayList<>();
}