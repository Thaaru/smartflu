package io.smartfluence.brand.management.rest;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.brand.management.service.CampaignAnalyticsService;

@RestController
@RequestMapping("brand")
public class BrandCampaignAnalyticsRestController {

	@Autowired
	private CampaignAnalyticsService campaignAnalyticsService;

	@GetMapping("campaign/{campaignId}/analytics")
	public ResponseEntity<Object> getAnalyticsData(@PathVariable UUID campaignId) {
		return campaignAnalyticsService.getAnalyticsData(campaignId);
	}

	@GetMapping("campaign/{campaignId}/postAnalytics")
	public ResponseEntity<Object> getPostAnalyticsData(@PathVariable UUID campaignId) {
		return campaignAnalyticsService.getPostAnalyticsData(campaignId);
	}
}