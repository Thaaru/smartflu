package io.smartfluence.modal.validation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DownloadReportModal {

	@NotBlank(message = "Enter the report id")
	private String reportId;

	@NotBlank(message = "Enter the influencer handle")
	private String influencerHandle;

	@NotBlank(message = "Enter the Brand name")
	private String brandName;

	@Email(message = "Enter your valid email")
	@NotBlank(message = "Enter your valid email")
	private String brandEmail;

	@NotBlank(message = "Enter Platform")
	private String platform;

	private String fileName;

	private byte[] file;
}