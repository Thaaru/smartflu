package io.smartfluence.modal.social.report;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TopPosts {

	@JsonAlias("image")
	private String image;

	@JsonAlias("thumbnail")
	private String thumbnail;

	@JsonAlias("link")
	private String url;
}