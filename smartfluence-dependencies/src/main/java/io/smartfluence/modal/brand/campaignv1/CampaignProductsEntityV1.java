package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignProductsEntityV1 {
   private Long productId;
   @NotEmpty(message = "product name should not be empty")
   @Size(min = 1,max = 51,message = "product name should be  max 50 characters")
   private String   productName;
   @NotEmpty(message = "product link should not be empty")
   @Size(min = 1,max = 251,message = "product link should be max 250 characters")
   private String   productLink;
}
