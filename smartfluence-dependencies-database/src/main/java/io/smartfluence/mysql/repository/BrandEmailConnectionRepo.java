package io.smartfluence.mysql.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.mysql.entities.BrandEmailConnection;

@Repository
public interface BrandEmailConnectionRepo extends CrudRepository<BrandEmailConnection, String> {

	Optional<BrandEmailConnection> findByBrandIdAndStatus(String brandId, int status);
}