package io.smartfluence.modal.validation.brand.explore;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SearchInfluencerRequestModel {

	private String influencerId;

	private String deepSocialInfluencerId;

	private String reportId;

	@NotNull(message = "Please provide influencerHandle")
	private String influencerHandle;

	private Boolean influencerPresent;

	private Boolean reportPresent;

	@NotBlank(message = "Please select a platform")
	private String platform;
}