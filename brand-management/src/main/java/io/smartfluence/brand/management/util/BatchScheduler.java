package io.smartfluence.brand.management.util;

import java.util.Date;
import java.util.UUID;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.ScheduleBuilder;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@EnableScheduling
public class BatchScheduler {

	@Autowired
	private SchedulerFactoryBean schedulerFactory;

	/**
	 * @see {@link #schedule(Class, Date, JobDataMap)}
	 * @param job
	 * @throws SchedulerException
	 */
	public void schedule(Class<? extends Job> job) throws SchedulerException {
		schedule(job, new Date(), (JobDataMap) null);
	}

	/**
	 * @see {@link #schedule(Class, Date, JobDataMap)}
	 * @param job
	 * @param jobDataMap
	 * @throws SchedulerException
	 */
	public void schedule(Class<? extends Job> job, JobDataMap jobDataMap) throws SchedulerException {
		schedule(job, new Date(), jobDataMap);
	}

	/**
	 * @see {@link #schedule(Class, Date, JobDataMap)}
	 * @param job
	 * @param triggerTime
	 * @throws SchedulerException
	 */
	public void schedule(Class<? extends Job> job, Date triggerTime) throws SchedulerException {
		schedule(job, triggerTime, (JobDataMap) null);
	}

	/**
	 * Job scheduling with Trigger time. Triggers once
	 * 
	 * @param job
	 * @param triggerTime
	 * @param jobDataMap
	 * @throws SchedulerException
	 */
	public void schedule(Class<? extends Job> job, Date triggerTime, JobDataMap jobDataMap) throws SchedulerException {
		JobKey jobKey = jobKey(job);
		JobDetail jobDetail = jobDetail(job, jobKey, jobDataMap);
		ScheduleBuilder<? extends Trigger> schedBuilder = simpleScheduler();
		Trigger trigger = trigger(schedBuilder, triggerTime, (Date) null, jobDetail);
		schedulerFactory.getScheduler().scheduleJob(jobDetail, trigger);

	}

	/**
	 * @see {@link #schedule(Class, String, Date, Date, JobDataMap)}
	 * @param job
	 * @param cronExpression
	 * @throws SchedulerException
	 */
	public void schedule(Class<? extends Job> job, String cronExpression) throws SchedulerException {
		schedule(job, cronExpression, (JobDataMap) null);
	}

	/**
	 * @see {@link #schedule(Class, String, Date, Date, JobDataMap)}
	 * @param job
	 * @param cronExpression
	 * @param jobDataMap
	 * @throws SchedulerException
	 */
	public void schedule(Class<? extends Job> job, String cronExpression, JobDataMap jobDataMap)
			throws SchedulerException {
		schedule(job, cronExpression, new Date(), (Date) null, jobDataMap);

	}

	public void schedule(Class<? extends Job> job, String cronExpression, Date startTime) throws SchedulerException {
		schedule(job, cronExpression, startTime, (Date) null, (JobDataMap) null);

	}

	/**
	 * @see {@link #schedule(Class, String, Date, Date, JobDataMap)}
	 * @param job
	 * @param cronExpression
	 * @param startTime
	 * @param endTime
	 * @throws SchedulerException
	 */
	public void schedule(Class<? extends Job> job, String cronExpression, Date startTime, Date endTime)
			throws SchedulerException {
		schedule(job, cronExpression, startTime, endTime, (JobDataMap) null);

	}

	/**
	 * Triggers based on start or end time for the given cron expression <br>
	 * Setting start date as null will set date as now<br>
	 * Setting the end date will have no end date
	 * 
	 * @param job
	 * @param cronExpression
	 * @param startTime
	 * @param endTime
	 * @param jobDataMap
	 * @throws SchedulerException
	 */
	public void schedule(Class<? extends Job> job, String cronExpression, Date startTime, Date endTime,
			JobDataMap jobDataMap) throws SchedulerException {
		JobKey jobKey = jobKey(job);
		JobDetail jobDetail = jobDetail(job, jobKey, jobDataMap);
		ScheduleBuilder<? extends Trigger> schedBuilder = cronScheduler(cronExpression);
		Trigger trigger = trigger(schedBuilder, startTime, endTime, jobDetail);
		schedulerFactory.getScheduler().scheduleJob(jobDetail, trigger);

	}

	/***********************************/
	/** Quartz utility functions below **/
	/***********************************/

	private ScheduleBuilder<? extends Trigger> cronScheduler(String cronExpression) {
		ScheduleBuilder<? extends Trigger> schedBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
		return schedBuilder;
	}

	private ScheduleBuilder<? extends Trigger> simpleScheduler() {
		ScheduleBuilder<? extends Trigger> schedBuilder = SimpleScheduleBuilder.repeatSecondlyForTotalCount(1);
		return schedBuilder;
	}

	private Trigger trigger(ScheduleBuilder<? extends Trigger> schedBuilder, Date startTime, Date endTime,
			JobDetail jobDetail) {
		Trigger trigger = TriggerBuilder.newTrigger().forJob(jobDetail)
				.withIdentity(TriggerKey.triggerKey(jobDetail.getKey().getName() + "_TRIGGER",
						jobDetail.getKey().getGroup() + "_TRIGGER"))
				.withSchedule(schedBuilder).startAt(startTime).endAt(endTime).build();
		return trigger;
	}

	private String uniqueName(String name) {
		StringBuilder builder = new StringBuilder();
		boolean first = true;
		char[] charArray = name.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			char c = charArray[i];
			if (first) {
				builder.append(Character.toUpperCase(c));
				first = false;
				continue;
			}
			if (Character.isUpperCase(c)) {
				builder.append('_');
			}
			builder.append(Character.toUpperCase(c));

		}
		builder.append('_');
		builder.append(UUID.randomUUID());
		return builder.toString();
	}

	private JobKey jobKey(Class<? extends Job> job) {
		String jobName = uniqueName(job.getSimpleName());
		return JobKey.jobKey(jobName, job.getName());
	}

	private JobDetail jobDetail(Class<? extends Job> job, JobKey jobKey, JobDataMap jobDataMap) {
		if (jobDataMap == null) {
			jobDataMap = new JobDataMap();
		}
		JobDetail jobDetail = JobBuilder.newJob(job).withIdentity(jobKey).setJobData(jobDataMap).build();
		return jobDetail;
	}
}