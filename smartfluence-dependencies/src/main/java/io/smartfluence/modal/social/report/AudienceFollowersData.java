package io.smartfluence.modal.social.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudienceFollowersData implements Serializable {

	private static final long serialVersionUID = 8347606169938354711L;

	@JsonAlias("audience_credibility")
	private Double audienceCredibility = 1.0;

	@JsonAlias("audience_genders")
	private List<CodeAndWeight> audienceGender = new ArrayList<>();

	@JsonAlias("audience_ages")
	private List<CodeAndWeight> audienceAge = new ArrayList<>();

	@JsonAlias("audience_interests")
	private List<NameAndWeight> audienceInterests = new ArrayList<>();

	@JsonAlias("audience_geo")
	private AudienceGeo audienceGeo = new AudienceGeo();

	@JsonAlias("audience_lookalikes")
	private List<AudienceLookalikes> audienceLookalikes = new ArrayList<>();
}