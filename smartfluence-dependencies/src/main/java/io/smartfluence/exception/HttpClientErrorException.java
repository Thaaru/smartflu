package io.smartfluence.exception;

import java.nio.charset.Charset;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import feign.Request;
import feign.Response;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class HttpClientErrorException extends org.springframework.web.client.HttpClientErrorException {

	private static final long serialVersionUID = -3627585486251871390L;

	public static final Charset UTF8 = Charset.forName("UTF-8");

	private final Request request;

	private final Response response;

	public HttpClientErrorException(HttpStatus statusCode, String statusText, HttpHeaders responseHeaders,
			byte[] responseBody, Response response) {
		super(statusCode, statusText, responseHeaders, responseBody, UTF8);
		this.request = response.request();
		this.response = response;
	}
}