package io.smartfluence.modal.brand.exploreinfluencer;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class GetInfluencersResponseModel {

	private String influencerId;

	private String influencerHandle;

	private String url;

	private Integer accountType;

	private Double followers;

	private Double engagements;

	private Double engagementScore;

	private String profileImage;

	private byte[] profileImageUrl;

	private Double audienceCheck;

	private Long totalResult;

	private Double pricing;

	private Double avgViews;

	private UUID searchId;

	private String fullName;
}