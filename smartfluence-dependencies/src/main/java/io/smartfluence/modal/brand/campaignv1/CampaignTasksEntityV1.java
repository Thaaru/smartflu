package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignTasksEntityV1 {
    private Long taskId;
    private Long platformId;
    private String    platform_name;
    @NotEmpty
    @Size(min = 1,max = 2500,message = "guidelines should be  max 2500 characters")
    private String guidelines;
    private long   postTypeId;
    private String postTypeName;
    private String         postCustomName;
}
