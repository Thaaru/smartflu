package io.smartfluence.webapp.proxy;

import java.util.UUID;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.smartfluence.constants.SubscriptionType;

@FeignClient(name = "smartfluence-admin-management")
@RibbonClient(name = "smartfluence-admin-management")
public interface AdminManagementServiceProxy {

	@GetMapping("admin/brands")
	public ResponseEntity<Object> getBrandList();

	@GetMapping("admin/subscriptions")
	public ResponseEntity<Object> getSubscriptions();

	@PostMapping("admin/approve-brand")
	public ResponseEntity<Object> approveBrand(@RequestParam(required = false) String brandId,
			@RequestParam(required = false) SubscriptionType subscriptionType, @RequestParam(required = false) UUID subscriptionId);

	@PostMapping("admin/decline-brand")
	public ResponseEntity<Object> declineBrand(@RequestParam(required = false) String brandId);

	@PostMapping("admin/send-invitation")
	public ResponseEntity<Object> sendInvitation(@RequestParam(required = false) String[] selectedUserId);

	@PostMapping("admin/update-db")
	public ResponseEntity<Object> updateDb();

	@GetMapping("admin/db-histories")
	public ResponseEntity<Object> getDBUpdateHistory();

	@GetMapping("admin/check-db-update-status")
	public ResponseEntity<Object> checkDbUpdateStatus();

	@GetMapping("admin/export-data")
	public ResponseEntity<String> exportAdminData();

	@GetMapping("admin/export-report-data")
	public ResponseEntity<Object> exportReportData(@RequestParam String reportType, @RequestParam String fromDate, @RequestParam String toDate);

	@GetMapping("admin/download/{flag}/report")
	public ResponseEntity<byte[]> downloadReport(@PathVariable String flag);
}