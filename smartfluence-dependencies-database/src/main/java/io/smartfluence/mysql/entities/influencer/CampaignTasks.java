package io.smartfluence.mysql.entities.influencer;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_tasks_v1")
public class CampaignTasks {
    @Id
//    @Column(name = "task_id")
    private Long taskId;

//    @Column(name = "campaign_id")
    private String campaignId;

//    @Column(name = "platform_id")
    private int platformId;

//    @Column(name = "guidelines")
    private String guidelines;

//    @Column(name = "post_type_id")
    private Long postTypeId;

//    @Column(name="post_custom_name")
    private String postCustomName;
}
