package io.smartfluence.influencer.services;

import io.smartfluence.influencer.ResourceServer;
import io.smartfluence.influencer.entities.*;
import io.smartfluence.influencer.repository.BrandCampaignDetailsRepo;
import io.smartfluence.influencer.repository.CampaignInfluencerDetailsRepo;
import io.smartfluence.influencer.repository.UserAccountRepo;
import io.smartfluence.influencer.utilities.CommonConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class CurrentPartnershipService {

    @Autowired
    CampaignInfluencerDetailsRepo campaignInfluencerDetailsRepo;

    @Autowired
    BrandCampaignDetailsRepo brandCampaignDetailsRepo;

    @Autowired
    UserAccountRepo userAccountRepo;

    public CustomResponseEntity getCurrentPartnershipDetails(){
        Logger.getLogger("getCurrentPartnershipDetails").log(Level.FINE,"getting the getCurrentPartnershipDetails");
        CustomResponseEntity customResponseEntity = new CustomResponseEntity();
        try {
            UUID userId = ResourceServer.getUserId();
            UserDetails userDetails = userAccountRepo.findByUserId(userId.toString());
            if(userDetails!=null){
            CurrentPartnershipEntity currentPartnershipEntity = new CurrentPartnershipEntity();
            currentPartnershipEntity.setUserEmail(userDetails.getEmail());
                currentPartnershipEntity.setUserName(userDetails.getBrandName());
                currentPartnershipEntity.setUserId(userDetails.getUserId());
            List<CampaignInfluencerDetails> campaignInfluencerDetailsList = campaignInfluencerDetailsRepo.findAllByInfluencerEmail(userDetails.email);
            List<CurrentPartnershipCampainDetails> campainDetailsList = new ArrayList<>();
            if(!campaignInfluencerDetailsList.isEmpty()){
                
                campaignInfluencerDetailsList.forEach(campaignInfluencerDetails -> {
                    BrandCampaignDetails brandCampaignDetails = brandCampaignDetailsRepo.findByCampaignId(campaignInfluencerDetails.getCampaignInfluencerId().getCampaignId());
                    if(brandCampaignDetails!=null && campaignInfluencerDetails.getProposalStatus()!=ProposalStatus.INVITE_NOT_SENT){
                      campainDetailsList.add(new CurrentPartnershipCampainDetails(brandCampaignDetails.getCampaignId(), brandCampaignDetails.getCampaignName(),campaignInfluencerDetails.getProposalStatus()));
                    }
                });

            }
                currentPartnershipEntity.setCampainDetailsList(campainDetailsList);
                customResponseEntity.setCode(CommonConstants.SUCCESS_CODE);
                customResponseEntity.setValue(currentPartnershipEntity);
                return customResponseEntity;
        }else{
            customResponseEntity.setCode(CommonConstants.NO_DATA_CODE);
            customResponseEntity.setError("No data available for this influencer");
           return customResponseEntity; 
        }
        }catch(Exception e){
            Logger.getLogger("getCurrentPartnershipDetails").log(Level.SEVERE,e.getMessage());
            customResponseEntity.setCode(CommonConstants.ERROR_CODE);
            customResponseEntity.setError(e.getMessage());
            return customResponseEntity;
        }


    }



}
