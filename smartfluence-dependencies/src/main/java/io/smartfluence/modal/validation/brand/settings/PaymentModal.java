package io.smartfluence.modal.validation.brand.settings;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.CreditCardNumber;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PaymentModal {

	@NotBlank(message = "Enter your full name for mailing")
	private String mailingFullName;

	@NotBlank(message = "Enter your address for mailing")
	private String mailingAddress;

	@NotBlank(message = "Enter your city for mailing")
	private String mailingCity;

	@NotBlank(message = "Enter your zipcode for mailing")
	private String mailingZipCode;

	@NotBlank(message = "Enter your country for mailing")
	private String mailingCountry;

	private boolean mailBill;

	private String billingFullName;

	private String billingAddress;

	private String billingCity;

	private String billingZipCode;

	private String billingCountry;

	@CreditCardNumber(message = "Enter a valid credit card number")
	private String creditCardNumber;

	@NotBlank(message = "Enter a card holder name")
	private String cardHolderName;

	@NotBlank(message = "Enter expiry date")
	private String expiryDate;

	@NotBlank(message = "Enter a valid CVV")
	@Pattern(regexp = "[0-9]{3}", message = "Enter a valid CVV")
	private String cvv;

	public String getBillingFullName() {
		if (mailBill)
			return mailingFullName;
		return billingFullName;
	}

	public String getBillingAddress() {
		if (mailBill)
			return mailingAddress;
		return billingAddress;
	}

	public String getBillingCity() {
		if (mailBill)
			return mailingCity;
		return billingCity;
	}

	public String getBillingZipCode() {
		if (mailBill)
			return mailingZipCode;
		return billingZipCode;
	}

	public String getBillingCountry() {
		if (mailBill)
			return mailingCountry;
		return billingCountry;
	}
}