package io.smartfluence.cassandra.repository.brand;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.brand.BrandSearchHistory;
import io.smartfluence.cassandra.entities.brand.primary.BrandSearchHistoryKey;

@Repository
public interface BrandSearchHistoryRepo extends CassandraRepository<BrandSearchHistory, BrandSearchHistoryKey> {

}