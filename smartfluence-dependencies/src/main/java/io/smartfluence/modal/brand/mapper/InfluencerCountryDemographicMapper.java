package io.smartfluence.modal.brand.mapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerCountryDemographicMapper {

	private InfluencerCountryKeyMapper key;

	private Double value;

	public Double getValue() {
		return value == null ? 0.0 : value;
	}
}