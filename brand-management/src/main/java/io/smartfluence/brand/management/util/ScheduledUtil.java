package io.smartfluence.brand.management.util;

import java.net.URISyntaxException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import io.smartfluence.brand.management.dao.ExploreInfluencerDao;
import io.smartfluence.brand.management.service.CampaignAnalyticsService;
import io.smartfluence.brand.management.service.PromoteCampaignService;
import io.smartfluence.brand.management.service.ReportsServiceImpl;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Configuration
@EnableScheduling
public class ScheduledUtil {

	@Autowired
	private ReportsServiceImpl reportsServiceImpl;

	@Autowired
	private ExploreInfluencerDao exploreInfluencerDao;

	@Autowired
	private CampaignAnalyticsService campaignAnalyticsService;

	@Autowired
	private PromoteCampaignService promoteCampaignService;

	@Scheduled(cron = "0 0/30 * 1/1 * ?")
	private void insightReportSchedule() throws URISyntaxException {
		log.info("Scheduler for insights crawler {}", new Date());
		reportsServiceImpl.deleteCampaignCsv();
	}

	@Scheduled(cron = "0 1 0 * * ?")
	private void truncateTemporaryContentTable() {
		log.info("Deleting Temporary Content Table");
		exploreInfluencerDao.truncateTemporaryContentTable();
	}

	@Scheduled(cron = "${post.statistics.cron}")
	private void getPostData() {
		log.info("Cron Running for Getting Post Data from DeepSocial ");
		campaignAnalyticsService.getPostData();
	}

	@Scheduled(cron = "${promote.campaign.cron}")
	private void executePromoteCampaigns() {
		log.info("Cron Running for executing Promote Campaigns");
		promoteCampaignService.executePromoteCampaignsCron();
	}
}