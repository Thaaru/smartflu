package io.smartfluence.brand.management.entityv1;

import lombok.*;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class DashBoardModalNew {

	private UUID campaignId;

	private String campaignName;

	private List<String> influencerIds;

}