package io.smartfluence.cassandra.entities.brand;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignDetailsKey;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_campaign_details")
public class BrandCampaignDetails implements Copyable {

	private static final long serialVersionUID = -1586573954645770089L;

	public BrandCampaignDetails(BrandCampaignDetails brandCampaignDetails) {
		this.key = brandCampaignDetails.getKey();
		this.brandName = brandCampaignDetails.getBrandName();
		this.createdAt = brandCampaignDetails.getCreatedAt();
		this.influencerAudienceCredibilty = brandCampaignDetails.getInfluencerAudienceCredibilty();
		this.influencerEngagement = brandCampaignDetails.getInfluencerEngagement();
		this.influencerFirstName = brandCampaignDetails.getInfluencerFirstName();
		this.influencerFollowers = brandCampaignDetails.getInfluencerFollowers();
		this.influencerLastName = brandCampaignDetails.getInfluencerLastName();
		this.modifiedAt = brandCampaignDetails.getModifiedAt();
		this.campaignName = brandCampaignDetails.getCampaignName();
	}

	@PrimaryKey
	private BrandCampaignDetailsKey key;

	@Column("brand_name")
	private String brandName;

	@Column("campaign_name")
	private String campaignName;

	@Column("influencer_audience_credibility")
	private Double influencerAudienceCredibilty;

	@Column("influencer_engagement")
	private Double influencerEngagement;

	@Column("influencer_first_name")
	private String influencerFirstName;

	@Column("influencer_followers")
	private Double influencerFollowers;

	@Column("influencer_last_name")
	private String influencerLastName;

	@Column("created_at")
	private Date createdAt;

	@Column("modified_at")
	private Date modifiedAt;

	@Column("influencer_handle")
	private String influencerHandle;

	@Column("bid_count")
	private Integer bidCount;
	
	public Integer getBidCount() {
		return bidCount == null ? 0 : bidCount;
	}
}