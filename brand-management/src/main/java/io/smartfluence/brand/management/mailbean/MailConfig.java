package io.smartfluence.brand.management.mailbean;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import io.smartfluence.brand.management.config.NoReplyProperties;
import io.smartfluence.brand.management.config.PartnershipProperties;

@Configuration
public class MailConfig {

	@Autowired
	private NoReplyProperties noReplyProperties;

	@Autowired
	private PartnershipProperties partnershipProperties;

	@Bean
	public JavaMailSenderImpl javaMailSenderImplPartner() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		Properties props = mailSender.getJavaMailProperties();
		props.putAll(partnershipProperties.getProperties());
		mailSender.setHost(partnershipProperties.getHost());
		mailSender.setPort(noReplyProperties.getPort());
		mailSender.setUsername(partnershipProperties.getUsername());
		mailSender.setPassword(partnershipProperties.getPassword());
		return mailSender;
	}

	@Bean
	public JavaMailSenderImpl javaMailSenderImplNoReply() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		Properties props = mailSender.getJavaMailProperties();
		props.putAll(noReplyProperties.getProperties());
		mailSender.setHost(noReplyProperties.getHost());
		mailSender.setPort(noReplyProperties.getPort());
		mailSender.setUsername(noReplyProperties.getUsername());
		mailSender.setPassword(noReplyProperties.getPassword());
		return mailSender;
	}
}