package io.smartfluence.modal.google.recaptcha;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VerifyRecaptchaResponse implements Serializable {

	private static final long serialVersionUID = -4226970313092296864L;

	@JsonAlias("success")
	private Boolean success;

	@JsonAlias("challenge_ts")
	private String createdDate;

	@JsonAlias("hostname")
	private String hostname;

	@JsonAlias("error-codes")
	private String[] errorCodes;
}