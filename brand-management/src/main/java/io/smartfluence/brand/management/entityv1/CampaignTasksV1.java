package io.smartfluence.brand.management.entityv1;

import lombok.*;

import javax.persistence.*;
import java.util.List;


@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_tasks_v1")
public class CampaignTasksV1 {
    @Id
//    @Column(name = "task_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long taskId;

//    @Column(name = "campaign_id")
    private String campaignId;

//    @Column(name = "platform_id")
    private Long platformId;

//    @Column(name = "guidelines")
    private String guidelines;

//    @Column(name = "post_type_id")
    private Long postTypeId;

//    @Column(name="post_custom_name")
    private String postCustomName;

}
