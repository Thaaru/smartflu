package io.smartfluence.brand.management.constants;

public enum IsShippingAccepted {
    TRUE, FALSE
}
