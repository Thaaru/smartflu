package io.smartfluence.beans.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import io.smartfluence.mysql.entities.IndustryMaster;
import io.smartfluence.mysql.entities.LocationMaster;
import io.smartfluence.mysql.repository.IndustryMasterRepo;
import io.smartfluence.mysql.repository.LocationMasterRepo;

@Service
public class SharedBeanServiceImpl implements SharedBeanService {

	@Autowired
	private IndustryMasterRepo industryMasterRepo;

	@Autowired
	private LocationMasterRepo locationMasterRepo;

	@Override
	public ResponseEntity<Object> getIndustryList() {
		List<String> industries = new LinkedList<String>();
		List<IndustryMaster> industryMaster = industryMasterRepo.findAllByStatus(1);
		industryMaster.parallelStream().forEach(e -> industries.add(e.getIndustry()));
		industries.sort((String a, String b) -> a.compareTo(b));
		return new ResponseEntity<>(industries, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getLocationList() {
		List<String> locations = new LinkedList<String>();
		List<LocationMaster> locationMaster = locationMasterRepo.findAllByStatus(1);
		locationMaster.parallelStream().forEach(e -> locations.add(e.getLocation()));
		locations.sort((String a, String b) -> a.compareTo(b));
		return new ResponseEntity<>(locations, HttpStatus.OK);
	}
}