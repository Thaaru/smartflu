var rows_Selected = [];
var origInfluencersList = [];
var cwfinfluencersList = [];
var currencyMaster = [];
var affiliateMaster = [];
var rowsSelected = [];
var campaignId;
var campaignWFIDT;

$(document).ready(function () {
    $.fn.dataTable.ext.errMode = 'none';
    campaignId = window.location.href.split("/")[5];
    init_inviteTable(campaignId);
 if(campaignStatusFromCampaignWF == "ENDED" ){
    campaignWFIDT.on('user-select', function (e, dt, type, cell, originalEvent) {
        //if ( originalEvent.target.nodeName.toLowerCase() === 'img' ) {
        e.preventDefault();
        //}
    });
}
});

const getCurrencyValue = (currencyId) => {
    return currencyMaster.find(x => x.currencyId == currencyId)?.currencyValue;
}

const getAffiliateValue = (commissionAffiliateTypeId) => {
    return affiliateMaster.find(x => x.caId == commissionAffiliateTypeId)?.caValue;
}

function isNumber(n) { return !isNaN(parseFloat(n)) && !isNaN(n - 0) }

const keyEvent = (event) => {
    var prevVal = event.target.getAttribute("data-prev-val");
    var dataType = event.target.getAttribute("data-data-type");
    var { value } = event.target;
    if (dataType === "number") {
        value = value?.replace(/[^0-9\.]/g, '');
        event.target.value = value;
        var charC = (event.which) ? event.which : event.keyCode;
        if (charC == 46) {
            if (!value.indexOf('.') === -1) return false;
        } else {
            if (charC > 31 && (charC < 48 || charC > 57)) return false;
        }
    }

    if ((prevVal != value) || (value == "")) {
        let type = 'payment';
        event.target.value = value;
        var keyType = event.target.getAttribute("data-key-type");
        var influencerId = event.target.id.replace(`${keyType}_`, '');
        var influencerIndex = cwfinfluencersList.findIndex(x => x.influencerId == influencerId);
        if (keyType === 'offer' && !isNaN(value)) {
            $(`#po_${influencerId}`).prop('checked', false);
            cwfinfluencersList[influencerIndex]['offers'] = cwfinfluencersList[influencerIndex]['offers'].filter(x => x.offerType != "PRODUCT");
            var paymentOfferIndex = cwfinfluencersList[influencerIndex]['offers'].findIndex(x => x.offerType == "PAYMENT");
            if (paymentOfferIndex === -1) {
                cwfinfluencersList[influencerIndex]['offers'].push({
                    campaignId: cwfinfluencersList[influencerIndex]['campaignId'],
                    influencerId: cwfinfluencersList[influencerIndex]['influencerId'],
                    offerType: "PAYMENT",
                    paymentOffer: value,
                    commissionValue: null,
                    commissionValueType: null,
                    commissionAffiliateType: null,
                    commissionAffiliateCustomName: null,
                    commissionAffiliateDetails: null,
                    commissionValueTypeId: null,
                    commissionAffiliateTypeId: null
                });
            }
            else {
                cwfinfluencersList[influencerIndex]['offers'][paymentOfferIndex]['paymentOffer'] = value;
            }
        } else if (keyType === 'ccn' || keyType === 'affiliate') {
            console.log(`ccn value inside  : keyType${keyType} `)
            type = "commission";
            var commissionOfferIndex = cwfinfluencersList[influencerIndex]['offers'].findIndex(x => x.offerType == "COMMISSION");
            pushCommissionObject(influencerIndex, commissionOfferIndex, `${keyType === 'ccn' ? 'commissionAffiliateCustomName' : 'commissionValue'}`, value);
        }

        if (event.key === "Enter" || event.type === "focusout") {
            event.target.setAttribute("data-prev-val", value);
            callAPI(cwfinfluencersList[influencerIndex], type);
        }
    }
}

$(document).on('change', '.productOnlyCheckbox', function (event) {
    $('.inviteCheckbox').prop("checked", false);
    var influencerId = event.target.id.replace(`po_`, '');
    var influencerIndex = cwfinfluencersList.findIndex(x => x.influencerId == influencerId);
    if (event.target.checked) {
        $(`#offer_${influencerId}`).val('--');
        cwfinfluencersList[influencerIndex]['offers'] = cwfinfluencersList[influencerIndex]['offers'].filter(x => x.offerType != "PAYMENT");
        var productOfferIndex = cwfinfluencersList[influencerIndex]['offers'].findIndex(x => x.offerType == "PRODUCT");
        var productOffer = {
            campaignId: cwfinfluencersList[influencerIndex]['campaignId'],
            influencerId: cwfinfluencersList[influencerIndex]['influencerId'],
            offerType: "PRODUCT",
            paymentOffer: null,
            commissionValue: null,
            commissionValueType: null,
            commissionAffiliateType: null,
            commissionAffiliateCustomName: null,
            commissionAffiliateDetails: null,
            commissionValueTypeId: null,
            commissionAffiliateTypeId: null
        };
        if (productOfferIndex === -1) {
            cwfinfluencersList[influencerIndex]['offers'].push(productOffer);
        }
        else {
            cwfinfluencersList[influencerIndex]['offers'][productOfferIndex] = productOffer;
        }
    } else {
        cwfinfluencersList[influencerIndex]['offers'] = cwfinfluencersList[influencerIndex]['offers'].filter(x => x.offerType != "PRODUCT");
    }
    callAPI(cwfinfluencersList[influencerIndex], 'product');
    event.stopPropagation();
});

const pushCommissionObject = (influencerIndex, commissionOfferIndex, key, value) => {
    console.log("pushcommssion object called ",commissionOfferIndex);
    if (commissionOfferIndex === -1) {
        let defaultComObj = {
            "campaignId": cwfinfluencersList[influencerIndex]['campaignId'],
            "influencerId": cwfinfluencersList[influencerIndex]['influencerId'],
            "offerType": "COMMISSION",
            "paymentOffer": null,
            "commissionValue": null,
            "commissionValueType": null,
            "commissionAffiliateType": null,
            "commissionAffiliateCustomName": "",
            "commissionAffiliateDetails": null,
            "commissionValueTypeId": null,
            "commissionAffiliateTypeId": null
        };
        defaultComObj[key] = value;
        cwfinfluencersList[influencerIndex]['offers'].push(defaultComObj);
        console.log("defaultComObj[key] if: ", defaultComObj[key] )
        console.log("push commission object if: ", cwfinfluencersList[influencerIndex]['offers'])
    }
    else {
        cwfinfluencersList[influencerIndex]['offers'][commissionOfferIndex][key] = value;
        console.log("push commission object else: ", cwfinfluencersList[influencerIndex]['offers'])
    }
   
}

$(document).on('change', '.commissionCustomType', function (event) {
    var influencerId = event.target.id.replace(`cct_`, '');
    var isCustom = (event.target.value === "Custom");
    $(`#ccn_${influencerId}`).css({ display: isCustom ? 'block' : 'none' });
    var influencerIndex = cwfinfluencersList.findIndex(x => x.influencerId == influencerId);
    var commissionOfferIndex = cwfinfluencersList[influencerIndex]['offers'].findIndex(x => x.offerType == "COMMISSION");
    if (!isCustom) {
        pushCommissionObject(influencerIndex, commissionOfferIndex, 'commissionAffiliateCustomName', null);
    }
    var commissionAffiliateType = affiliateMaster.find(x => x.caValue === event.target.value);
    var commissionAffiliateTypeId = commissionAffiliateType ? commissionAffiliateType.caId : null;
    pushCommissionObject(influencerIndex, commissionOfferIndex, 'commissionAffiliateTypeId', commissionAffiliateTypeId);
    callAPI(cwfinfluencersList[influencerIndex], 'commission');
});

$(document).on('change', '.commissionCurrencyMasterType', function (event) {
    var influencerId = event.target.id.replace(`ccmt_`, '');
    var influencerIndex = cwfinfluencersList.findIndex(x => x.influencerId == influencerId);
    var commissionOfferIndex = cwfinfluencersList[influencerIndex]['offers'].findIndex(x => x.offerType == "COMMISSION");
    var commissionValueType = currencyMaster.find(x => x.currencyValue === event.target.value);
    var commissionValueTypeId = commissionValueType ? commissionValueType.currencyId : null;
    pushCommissionObject(influencerIndex, commissionOfferIndex, 'commissionValueTypeId', commissionValueTypeId);
    callAPI(cwfinfluencersList[influencerIndex], 'commission');
});

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const checkCommissionAvailable = (data, type) => {
    let offers = data['offers'];

    let commissionObjIndex = offers.findIndex(x => x.offerType === 'COMMISSION');
    let paymentObjIndex = offers.findIndex(x => x.offerType === 'PAYMENT');
    let productObjIndex = offers.findIndex(x => x.offerType === 'PRODUCT');

    let result = {
        success: true,
        data: data
    }

    if (type == 'commission' && commissionObjIndex != -1) {
        console.log(offers[commissionObjIndex]);
        console.log(isNumber(offers[commissionObjIndex]['commissionValue']));
        console.log(!getCurrencyValue(offers[commissionObjIndex]['commissionValueTypeId']));
        console.log(!getAffiliateValue(offers[commissionObjIndex]['commissionAffiliateTypeId']));
        
        if (!isNumber(offers[commissionObjIndex]['commissionValue']) && 
            !getCurrencyValue(offers[commissionObjIndex]['commissionValueTypeId']) && 
            !getAffiliateValue(offers[commissionObjIndex]['commissionAffiliateTypeId'])) {
            
            data['offers'] = offers.filter(x => x.offerType !== 'COMMISSION');
            
            result = {
                success: true,
                message: "",
                data: data
            }
        }
        else if (!isNumber(offers[commissionObjIndex]['commissionValue']) || 
            !getCurrencyValue(offers[commissionObjIndex]['commissionValueTypeId']) || 
            !getAffiliateValue(offers[commissionObjIndex]['commissionAffiliateTypeId'])) {
            result = {
                success: false,
                message: "Please Add Affiliate Value Details"
            }
        }
    }


    if (type == 'payment' && paymentObjIndex != -1) {
        if (isNaN(offers[paymentObjIndex]['paymentOffer'])) {
            result = {
                success: false,
                message: "Please Provide a Payment Offer Amount"
            }
        }
    }

    if (type == 'product') {
        if (productObjIndex == -1) {
            result = {
                success: false,
                message: "Please Provide a Payment Offer Amount"
            }
        }
    }

    return result;
}

const callAPI = (data, type) => {
    let validateData = checkCommissionAvailable(data, type);

    if (!validateData.success) {
        return toast(validateData.message, 'error');
    }

    data = validateData.data;

    $.ajax({
        type: 'PUT',
        url: '/brand/view-campaign-v1/view-influencers/updateoffer',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function (data, statusMsg, xhr) {
            if (data.code == 600) {
                toast(`${capitalizeFirstLetter(type)} ${['payment', 'commission'].includes(type) ? ' Amount' : ''} Updated Successfully`);
                campaignWFIDT.ajax.reload(null, false);
            } else {
                toast(data.errors.message, 'error');
            }
        },
        error: function (xhr) {
            
            toast("Error occured", 'error');
        }
    });
}

var influencerHandle = "",
    paymentType = "",
    platform = "",
    postType = "",
    postDuration = "",
    bidAmount = "",
    affiliateType = "",
    influencerPricing = "",
    ginfluencerId = "";

const resetMailTemplateGlobalVars = () => {
    influencerHandle = paymentType = platform = postType = "";
    influencerPricing = postDuration = bidAmount = affiliateType = ginfluencerId = "";
}
$('#mailTemplateNewModal').on('hidden.bs.modal', function () {
    
    resetMailTemplateGlobalVars();
});

const showMailTemplate = () => {
    // $('#bid').removeClass("border-danger");
    $('#bidAmountInvalid').empty();
    $('#paymentTypeInvalid').empty();
   
    if (['Product/Service', 'Shoutout'].includes(paymentType)) {
        $('.not-affiliate').show();
        $('.not-affiliate').addClass('d-inline-block');
        $('.affiliate').addClass('col-md-3');
        $('.affiliate').removeClass('col-md-4');
        $('#payment-non-cash').html("Please include the cash equivalent value of the product, service or shoutout you are providing in the bid amount.").show();
        $('#affiliateSuffixDiv').addClass('d-none');
        $('#bidAmountLabel').html('Bid Amount in $');
    } else if (paymentType == 'Affiliate') {
        // $('#bid').val(0);
        bidAmount = 0;
        $('.not-affiliate').hide();
        $('.not-affiliate').removeClass('d-inline-block');
        $('.affiliate').removeClass('col-md-3');
        $('.affiliate').addClass('col-md-4');
        $('#payment-non-cash').html("Smartfluence will recommend an affiliate post schedule for the influencer. Please select whether you will be paying as a % of sales or $ per sale.").show();
        $('#affiliateSuffixDiv').removeClass('d-none');
        $('#bidAmountLabel').html('Affiliate %');
    } else {
        $('.not-affiliate').show();
        $('.not-affiliate').addClass('d-inline-block');
        $('.affiliate').addClass('col-md-3');
        $('.affiliate').removeClass('col-md-4');
        $('#payment-non-cash').hide();
        $('#affiliateSuffixDiv').addClass('d-none');
        $('#bidAmountLabel').html('Bid Amount in $');
    }
    $('#mailTemplate option').attr('hidden', 'hidden');
    $('#mailTemplate .true').removeAttr('hidden');
    if (paymentType == 'Affiliate') {
        if (affiliateType == '%') {
            $('#mailTemplate .Affiliate.percentage').removeAttr('hidden');
            $('#mailTemplate .Affiliate.percentage').prop('selected', true);
        } else if (affiliateType == '$') {
            $('#mailTemplate .Affiliate.fee').removeAttr('hidden');
            $('#mailTemplate .Affiliate.fee').prop('selected', true);
        }
    } else if (paymentType == 'Product/Service') {
        $('#mailTemplate .Product').removeAttr('hidden');
        $('#mailTemplate .Product').prop('selected', true);
    } else {
        $('#mailTemplate ' + (paymentType == '' ? '' : '.' + paymentType.replace(/ /g, '_'))).removeAttr('hidden');
        $('#mailTemplate ' + (paymentType == '' ? '' : '.' + paymentType.replace(/ /g, '_'))).prop('selected', true);
    }
    // $('#mailTemplate').val('').change();
    // $('#mailTextArea').val('');
    // $('#mailSubject').val('');
    if (platform != 'instagram') {
        $('#bidPostDuration').removeClass('d-inline-block');
        $('.yt').removeClass('col-md-3');
        $('.yt').addClass('col-md-4');
    }

    validateSubmitBid();
}

const validateSubmitBid = () => {
    $('#step3Body').html('')
    var postTypeBoolean = true;
    var postDurationBoolean = true;
    var bidBoolean = true;
    var paymentTypeBoolean = true;
    var campaignBoolean = true;
    // var paymentType = $('#paymentType').val();
    // var postType = document.getElementById("postType");
    // var postDuration = document.getElementById("postDuration");
    // var bidAmount = $('#bid').val();
    // var affiliateType = $('#affiliateType').val();
    var paymentTypeMessage = 'Select Payment Type';
    // var platform = $('#platform-submit-bid').val();

    if (paymentType == '') {
        // $('#paymentType').addClass("border-danger");
        // $('#paymentTypeInvalid').html(paymentTypeMessage);
        // paymentTypeBoolean = false;
    } else if (paymentType == "Affiliate") {
        var affiliateType = affiliateType;
        if (affiliateType == "%" && (bidAmount <= 0 || bidAmount > 100)) {
            // $('#bid').addClass("border-danger");
            // $('#bidAmountInvalid').html("Enter a valid percentage");
            bidBoolean = false;
        } else if (affiliateType == "$" && (bidAmount <= 0)) {
            // $('#bid').addClass("border-danger");
            // $('#bidAmountInvalid').html("Enter a valid fee");
            bidBoolean = false;
        }
    }
    if (paymentType != "Affiliate") {
        affiliateType = null;
        // postType.setCustomValidity('Select Post Type');

        if (postType == '') {
            // $('#postType').addClass("border-danger");
            // $('#postTypeInvalid').html(postType.validationMessage);
            postTypeBoolean = false;
        }
        if (platform == 'instagram') {
            // postDuration.setCustomValidity('Select Post Duration');
            if (postDuration == '') {
                // $('#postDuration').addClass("border-danger");
                // $('#postDurationInvalid').html(postDuration.validationMessage);
                postDurationBoolean = false;
            }
        }

        if (postType == "Story" || (postType == "Image" && (postDuration == "24H" || postDuration == "1W"))) {
            // var bid = document.getElementById("bid");
            // bid.setCustomValidity('Enter the bid amount');
            if (bidAmount == "" || Number(bidAmount) <= 0) {
                // $('#bid').addClass("border-danger");
                // $('#bidAmountInvalid').html(bid.validationMessage);
                bidBoolean = false;
            }
        } else {
            // var bid = document.getElementById("bid");
            // var minimumAmount = (influencerPricing * 0.75).toFixed(0);
            // bid.setCustomValidity('Your Bid should be greater than 0$');
            if (Number(bidAmount) > 0) {
                bidBoolean = true;
            } else {
                // $('#bid').addClass("border-danger");
                // $('#bidAmountInvalid').html(bid.validationMessage);
                bidBoolean = false;
            }
        }
    }

    if (postTypeBoolean && postDurationBoolean && bidBoolean && paymentTypeBoolean && campaignBoolean) {
        //$('#step3Body').html('A bid will be submitted with the following details:');
        $('.submitBidTable').removeClass('d-none');

        if (paymentType == "Affiliate") {
            // var affilateType = $('#affiliateType').val();
            var affilateMode;
            $('#postTypeTableLabel').addClass('d-none');
            $('#postDurationTableLabel').addClass('d-none');
            $('#paymentModeTableLabel').removeClass('d-none');
            $('#postTypeTable').addClass('d-none');
            $('#postDurationTable').addClass('d-none');
            $('#paymentModeTable').removeClass('d-none');

            if (affilateType == '$') {
                $('#bidAmountTableLabel').text('Affiliate Fee');
                affilateMode = '$ per Sale';
                $('#bidAmountTable').html('$' + bidAmount);
            } else if (affilateType == '%') {
                $('#bidAmountTableLabel').text('Affiliate %');
                affilateMode = '% of Sales';
                $('#bidAmountTable').html(bidAmount + '%');
            }
            $('#paymentModeTable').html(affilateMode);
        } else {
            $('#bidAmountTable').html('$' + bidAmount);
            $('#postTypeTableLabel').removeClass('d-none');
            $('#paymentModeTableLabel').addClass('d-none');
            $('#postTypeTable').removeClass('d-none');
            $('#paymentModeTable').addClass('d-none');
            $('#bidAmountTableLabel').text('Bid Amount');

            if (platform != 'instagram') {
                $('#postDurationTableLabel').addClass('d-none');
                $('#postDurationTable').addClass('d-none');
            } else {
                $('#postDurationTableLabel').removeClass('d-none');
                $('#postDurationTable').removeClass('d-none');
            }
        }

        $('#postTypeTable').html($("#postType option:selected").text());
        $('#postDurationTable').html($("#postDuration option:selected").text());
        $('#paymentTypeTable').html($("#paymentType option:selected").text());
        $('.addToCampaignMesage').addClass('d-none');
        $('#submitBid').removeClass('d-none');
        $('#addToCampaign').addClass('d-none');
        // sendEvent('#SubmitBid', 2);
        $('#mailCc').val(profileDetails.email);
        $('#mailTemplate .true').removeAttr('hidden');

        if (paymentType == 'Affiliate') {
            if (affiliateType == '%') {
                $('#mailTemplate .Affiliate.percentage').removeAttr('hidden');
                $('#mailTemplate .Affiliate.percentage').prop('selected', true);
                $('#mailTemplate .Affiliate.fee').change();
            } else if (affiliateType == '$') {
                $('#mailTemplate .Affiliate.fee').removeAttr('hidden');
                $('#mailTemplate .Affiliate.fee').prop('selected', true);
                $('#mailTemplate .Affiliate.fee').change();
            }
        } else if (paymentType == 'Product/Service') {
            $('#mailTemplate .Product').removeAttr('hidden');
            $('#mailTemplate .Product').prop('selected', true);
            $('#mailTemplate .Affiliate.fee').change();
        } else {
            $(
                '#mailTemplate '
                + (paymentType.trim() == '' ? '' : '.'
                    + paymentType.trim().replace(
                        / /g, '_'))).removeAttr('hidden');
            $(
                '#mailTemplate '
                + (paymentType.trim() == '' ? '' : '.'
                    + paymentType.trim().replace(
                        / /g, '_'))).prop('selected', true);
            $('#mailTemplate .Affiliate.fee').change();
        }
    }
}

const sendInvite = (campaignId, influencerId, offerType) => {
    let infDetails = origInfluencersList.find(x => x.campaignId === campaignId && x.influencerId === influencerId);
    
    let validateData = checkCommissionAvailable(infDetails, 'commission');
    if (!validateData.success) {
        return toast(validateData.message, 'error');
    }

    if (infDetails) {
        offerType = offerType.split(",");
    
        if (offerType.includes("PRODUCT")) {
            if (offerType.includes("COMMISSION")) {
                paymentType = "Product-Commission"
            } else {
                paymentType = "Product-Only"
            }
        } else if (offerType.includes("PAYMENT")) {
            if (offerType.includes("COMMISSION")) {
                paymentType = "Payment-Commission"
            } else {
                paymentType = "Payment"
            }
        }
       
        // paymentType => Cash, Product/Service, Shoutout, Affiliate
        // affiliateType => %,   $
        // platform => instagram, tiktok, youtube
        //payment types

        influencerHandle = infDetails.influencerHandle;
        platform = infDetails.platform;
        postType = "Image";
        postDuration = "2H";
        bidAmount = "100";
        affiliateType = "";
        influencerPricing = "1000";
        ginfluencerId = influencerId;
        showMailTemplate();
        $('#mailTemplateNewModal').modal('show');
        // $.ajax({
        //     type: 'POST',
        //     url: '/brand/view-campaign-v1/view-influencers/sendproposal',
        //     data: JSON.stringify({ campaignId, influencerId }),
        //     contentType: "application/json; charset=utf-8",
        //     traditional: true,
        //     success: function (data, statusMsg, xhr) {
        //         console.log({ data, statusMsg, xhr });
        //         if (data.code == 600) {
        //             toast("Invited successfully");
        //             var someTabTriggerEl = document.querySelector('#reviewProposal-tab');
        //             var tab = new bootstrap.Tab(someTabTriggerEl);
        //             tab.show();
        //             campaignWFIDT.ajax.reload(null, false);
        //         } else {
        //             toast("Error occured", 'error');
        //         }
        //     },
        //     error: function (xhr) {
        //         console.log({ xhr });
        //         toast("Error occured", 'error');
        //     }
        // });
    }
}

//table_campaign_invite
function init_inviteTable(campaignId) {
    campaignWFIDT = $('#table_campaign_invite').DataTable({
        dom: "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-7 btn-explore pl-0 d-flex justify-content-end'><'col-sm-12 col-md-3'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",

        ajax: {
            url: `/brand/view-campaign-v1/view-influencers?id=${campaignId}&proposalStatus=INVITE_NOT_SENT`,
            dataSrc: function (response) {
               
                if (response?.code == 600) {
                    currencyMaster = response.data.currencyMaster;
                    affiliateMaster = response.data.affiliateMaster;
                    cwfinfluencersList = origInfluencersList = response.data.influencersList;
                    return cwfinfluencersList;
                }else{
                return [];
                }
            }
        },
        autoWidth: false,
        rowId: 'influencerId',
        columnDefs: [
            {
                targets: 0,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return '<input name="influencers_cbs" type="checkbox" class="inviteCheckbox">';
                }
            },
            {
                targets: 1,
                width: "10%",
                className: "text-left",
                data: "influencerHandle",
                render: function (data, type, row, meta) {
                    return `<a style=" color: #2f008d" href="/brand/view-influencer/${row.influencerId}?isValid=true&platform=${row.platform}"
                   target="_blank">${data}</a>`
                }
            },
            {
                targets: 2,
                width: "10%",
                data: "platform",
                className: "text-left",
                render: function (data, type, full, meta) {
                    //tiktok
                    if (data == "tiktok" || data == "TikTok") {
                        //` <img style="height: 3vh"; src="/static/svgs/brands/tiktok.svg"></img>`
                        return `<a style=" color: #2f008d; font-size: 1.2rem;"><img style="width: 18px; height: 18px;" src="/static/svgs/brands/tiktok.svg"></img></a>`;
                    }
                    else {
                        return `<a style=" color: #2f008d; font-size: 1.2rem;"><i class="fab fa-${data}"></i></a>`;
                    }

                }
            },
            {
                targets: 3,
                width: "10%",
                className: "text-left",
                data: "followers",
                render: function (data, row) {
                    const kFormatter = (num) => {
                        return Math.abs(num) > 999 ? Math.sign(num) * ((Math.abs(num) / 1000).toFixed(2)) + 'k' : Math.sign(num) * Math.abs(num)
                    }
                    return kFormatter(data)
                }
            },
            {
                targets: 4,
                width: "10%",
                className: "text-left",
                data: "engagementRate",
                render: function (data, row) {
                    const kFormatter = (num) => {
                        return Math.abs(num) > 999 ? Math.sign(num) * ((Math.abs(num) / 1000).toFixed(2)) + 'k' : Math.sign(num) * Math.abs(num)
                    }
                    return kFormatter(data)
                }
            },
            {
                targets: 5,
                data: "offers",
                className: "text-left",
                width: "10%",
                render: function (data, type, row, meta) {
                    var offerAmount = "--";
                    if (typeof data === 'object' && data?.length > 0) {
                        var pO = data.find(x => x.offerType === "PAYMENT");
                        offerAmount = (pO && typeof pO?.paymentOffer === "number") ? pO.paymentOffer : "--";
                    }

                    return `
                        <input 
                            style="pointer-events: ${campaignStatusFromCampaignWF == "ENDED" ? 'none' : 'default'}" 
                            type="text" 
                            class='w-75' 
                            value='${offerAmount}' 
                            data-data-type="number" 
                            data-key-type='offer' 
                            data-prev-val='${offerAmount}' 
                            id='offer_${row.influencerId}' 
                            onchange="return keyEvent(event);" onkeyup="return keyEvent(event);" onfocusout="return keyEvent(event);" 
                        />
                    `;
                }
            },
            {
                targets: 6,
                data: "offers",
                className: "text-center",
                width: "25%",
                render: function (data, type, row, meta) {
                    var cO = data.find(x => x.offerType === "COMMISSION");
                    var commissionAmount = "--"; var commissionAffiliateCustomName = "";
                    var currencyMasterValue; var affiliateMasterValue;
                    if (cO) {

                        if (typeof data === 'object' && data?.length > 0) {
                            commissionAmount = (cO && typeof cO?.commissionValue === "number") ? cO.commissionValue : "--";
                            currencyMasterValue = cO ? getCurrencyValue(cO.commissionValueTypeId) : currencyMasterValue;
                            affiliateMasterValue = cO ? getAffiliateValue(cO.commissionAffiliateTypeId) : affiliateMasterValue;
                            commissionAffiliateCustomName = (cO && typeof cO?.commissionAffiliateCustomName === "string") ? cO.commissionAffiliateCustomName : "";
                        }
                    }

                    return `
                        <div style="display: flex; pointer-events: ${campaignStatusFromCampaignWF == "ENDED" ? 'none' : 'default'}">
                            <input type="text" class='w-75' value='${commissionAmount}' data-data-type="number" data-key-type='affiliate' data-prev-val='${commissionAmount}' id='affiliate_${row.influencerId}' onchange="return keyEvent(event);" onkeyup="return keyEvent(event);" onfocusout="return keyEvent(event);" />
                            <select class='w-75 commissionCurrencyMasterType' id='ccmt_${row.influencerId}'>
                                <option>--</option>    
                                <option ${currencyMasterValue == '%' ? 'selected' : ''} value="%">%</option>
                                <option ${currencyMasterValue == '$' ? 'selected' : ''} value="$">$</option>
                            </select>
                            <span class='mx-2'>per</span>
                            <select class='w-75 commissionCustomType' id='cct_${row.influencerId}'>
                                <option>--</option>
                                <option ${affiliateMasterValue == 'Click' ? 'selected' : ''} value="Click">Click</option>
                                <option ${affiliateMasterValue == 'Sale' ? 'selected' : ''} value="Sale">Sale</option>
                                <option ${affiliateMasterValue == 'Custom' ? 'selected' : ''} value="Custom">Custom</option>
                            </select>
                            <input type="text" class='w-75' style="display: ${affiliateMasterValue == 'Custom' ? 'block' : 'none'};" value='${commissionAffiliateCustomName}' data-data-type="string" data-key-type='ccn' data-prev-val='${commissionAffiliateCustomName}' id='ccn_${row.influencerId}' onchange="return keyEvent(event);" onkeyup="return keyEvent(event);" onfocusout="return keyEvent(event);" />
                        </div>
                    `;

                }
            },
            {
                targets: 7,
                searchable: false,
                orderable: false,
                data: "offers",
                className: "text-center",
                render: function (data, type, row, meta) {
                    var productAvail = false;
                    if (typeof data === 'object' && data?.length > 0) {
                        productAvail = data.find(x => x.offerType === "PRODUCT") ? true : false;
                    }
                    return `
                        <input 
                            ${campaignStatusFromCampaignWF == "ENDED" ? 'disabled' : ''} 
                            type="checkbox" 
                            class="productOnlyCheckbox" 
                            id='po_${row.influencerId}' 
                            ${productAvail ? 'checked' : ''}
                        >
                    `;
                }
            },
            {
                targets: 8,
                className: "text-left",
                data: "influencerHandle",
                render: function (data, type, row, meta) {
                    return `<a style="color: #2f008d">Invite Not Sent</a>`;
                }
            },
            {
                targets: 9,
                className: "text-center",
                data: "influencerHandle",
                render: function (data, type, row, meta) {
                    if (campaignStatusFromCampaignWF == "ENDED") {
                        return `
                        <div class="dropdown">
                            <span class="dot-icon disabled"><i class="fa fa-ellipsis-v"></i></span>
                        </div>
                    `;
                    } else {
                        var offerType = Array.from(new Set(row.offers.map(item => item.offerType)))
                        return `
                        <div class="dropdown">
                            <span class="dot-icon"><i class="fa fa-ellipsis-v"></i></span>
                            <ul class="dropdown-content">
                                <li id='send_${row.influencerId}' onclick="return sendInvite('${row.campaignId}', '${row.influencerId}', '${offerType}')">Send</li>
                            </ul>
                        </div>
                    `;
                    }
                }
            },
            {
                className: 'control text-center',
                targets: [0, 2, 5, 6, 7, 9],
                orderable: false
            }
        ],
        select: {
            style: 'multi'
        },
        order: [
            [1, 'asc']
        ],
        drawCallback: inviteDrawcallback,
        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            // var rowId = data.DT_RowId;
            var rowId = data.influencerId
            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_Selected) !== -1) {
                $(row).find('.inviteCheckbox').prop('checked', true);
                $(row).addClass('selected');
            }
        }
    }).on('error.dt', function (e, settings, techNote, message) {
        // console.log('error occured');
        $('table#table_campaign_invite > tbody > tr > td.dataTables_empty').html("Error occured");
    });
}

function inviteDrawcallback() {
    if (campaignStatusFromCampaignWF == "ENDED") {
        $('.btn-explore')
            .html(
                '<span class="disabled style="float:right">'
                + '	<a  style="right: 8.5%; href="#mailTemplateModal" id="viewMailTemplate" data-toggle="modal" class="btn btn-primary btn-sm waves-effect" style="">Mail Templates</a>'
                + '</span>'

                + '<span  style="float:right">'
                + '<a  class="d-none" style="right: 5.5%; href="#bulkMailModal" id="bulkMail" data-toggle="modal" class="btn btn-primary btn-sm waves-effect">Bulk Email</a>'
                + '</span>'
            );
    } else {
        $('.btn-explore')
            .html(
                '<span  >'
                + '	<a  href="#mailTemplateModal" id="viewMailTemplate" data-toggle="modal" class="btn btn-primary btn-sm waves-effect" style="">Mail Templates</a>'
                + '</span>'

                + '<span >'
                + '<a  href="#bulkMailModal" id="bulkMail" data-toggle="modal" class="btn d-none btn-primary btn-sm waves-effect">Bulk Email</a>'
                + '</span>'
            );
    }
    $('[date-value]').each(
        function () {
            var text = moment(
                Number($(this).attr('date-value')))
                .format('MMMM DD, YYYY HH:mm ')
                + moment.tz(moment.tz.guess())
                    .zoneAbbr();
            $(this).text(text);
        });
}

$('#table_campaign_invite tbody').on('click', '.inviteCheckbox', function (e) {
    // debugger;
    var $row = $(this).closest('tr');
    // Get row data
    var data = campaignWFIDT.row($row).data();
    // Get row ID
    // var rowId = data.DT_RowId;
    var rowId = data.influencerId
    // Determine whether row ID is in the list of selected row IDs
    var index = $.inArray(rowId, rows_Selected);
    // If checkbox is checked and row ID is not in list of selected row IDs
    if (this.checked && index === -1) {
        rows_Selected.push(rowId);
        // Otherwise, if checkbox is not checked and row ID is in list of
        // selected
        // row IDs
    } else if (!this.checked && index !== -1) {
        rows_Selected.splice(index, 1);
    }
    if (this.checked) {
        $row.addClass('selected');
    } else {
        $row.removeClass('selected');
    }
    // Update state of "Select all" control
    // updateDataTableSelectAllCtrl(campaignWFIDT);
    // Prevent click event from propagating to parent
    e.stopPropagation();
});

// $(document).on('click', '.removeInfluencer', function (event) {
//     let selectedId = event.currentTarget.id;
//     rowsSelected = [];
//     rowsSelected.push(selectedId);
//     $('.inviteCheckbox').prop('checked', false);
// });

// Handle click on table cells with checkboxes

$(document).ready(function () {
    $(document).on('click', 'thead input[name="select_all"]', function(e) {
        $(this).parent().find('input[name="influencers_cbs"]').trigger('click');
    });
    
    $('thead input[name="select_all"]', campaignWFIDT.table().container()).on(
        'click',
        function(e) {
           
            if (this.checked) {
                $('tbody input[name="influencers_cbs"]:not(:checked)',
                campaignWFIDT.table().container()).trigger('click');
            } else {
                $('tbody input[name="influencers_cbs"]:checked',
                campaignWFIDT.table().container()).trigger('click');
            }
            // Prevent click event from propagating to parent
            e.stopPropagation();
        });
});

$(document).on('change', 'input[name="influencers_cbs"]', function (e) {
   
    var $row = $(this).closest('tr');
    // Get row data
    var data = campaignWFIDT.row($row).data();
    // Get row ID
    // var rowId = data.DT_RowId;
    var rowId = data.influencerId
    // Determine whether row ID is in the list of selected row IDs
    var index = $.inArray(rowId, rowsSelected);
    // If checkbox is checked and row ID is not in list of selected row IDs
    if (this.checked && index === -1) {
        rowsSelected.push(rowId);
        // Otherwise, if checkbox is not checked and row ID is in list of
        // selected
        // row IDs
    } else if (!this.checked && index !== -1) {
        rowsSelected.splice(index, 1);
    }
    if (this.checked) {
        $row.addClass('selected');
    } else {
        $row.removeClass('selected');
    }
    // Update state of "Select all" control
    updateDataTableSelectAllCtrl(campaignWFIDT);
    // Prevent click event from propagating to parent
    e.stopPropagation();
});

function updateDataTableSelectAllCtrl(table) {

   

    var $table = table.table().node();
    var $chkbox_all = $('tbody input[name="influencers_cbs"]', $table);
    var $chkbox_checked = $('tbody input[name="influencers_cbs"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }
        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }
        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
    if (rowsSelected.length != 0) {
        $("#bulkMail").removeClass("d-none");
        $('#deleteCampaignBtn').removeClass("d-none");
        $('#copyInfluencers').parent().removeClass('disabled');
        $('#copyInfluencers').css("color", "rgb(47, 0, 141)");
        $('#copyInfluencers').css("cursor", "pointer");
    } else {
        $('#bulkMail').addClass('d-none ');
        $('#deleteCampaignBtn').addClass("d-none");
        $('#copyInfluencers').parent().addClass('disabled');
        $('#copyInfluencers').css("color", "rgb(227, 226, 226)");
        $('#copyInfluencers').css("cursor", "");
    }
}

function removeNewInfluencer() {
    //jQuery.noConflict();
    var settings = {
        "url": "/brand/remove-influencer",
        "method": "DELETE",
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({ "influencerIds": rowsSelected, "campaignId": campaignId }),
    };

    $.ajax(settings).done(function (data) {
        toast(" Influencer Deleted Successfully");
        campaignWFIDT.ajax.reload(null, false);
        $('#removeInfluencer').modal('hide');
    }).fail(function (jqXHR, textStatus) {
        toast("Request Failed " + textStatus, "error");
        $('#removeInfluencer').modal('hide');
    });
}

//email template


function validateMailContent() {
    var mailTemplate = $("#mailTemplate").val();
    var mailDescription = $('#mailTextArea').val();
    var mailSubject = $('#mailSubject').val();

    if (mailTemplate == '' || mailTemplate == undefined) {
        $('#mailTemplateInvalid').text('Select Mail Template');
        return;
    }
    if (mailDescription == '' || mailDescription == undefined) {
        $('#mailTextAreaInvalid').text('Enter Mail Content');
        return;
    }
    if (mailSubject == '' || mailSubject == undefined) {
        $('#mailSubjectInvalid').text('Enter Subject');
        return;
    }
    sendEvent('#SubmitBid', 3);
}

$('#mailTextArea').on('input', function () {
    $('#mailTextAreaInvalid').text('');
});
$('#mailSubject').on('input', function () {
    $('#mailSubjectInvalid').text('');
});

function viewMailEditPage(title, id) {
    $('#mailTemplateTable').addClass('d-none');
    $('#createNewTemplate').addClass('d-none');
    $('#editTemplate').removeClass('d-none');
    $('#editTemplate').addClass('slide-left');
    var isNew = true;

    if (id != null) {
        for (var template in mailTemplates) {
            if (mailTemplates[template].id == id) {
                $('#templateTextArea').html('');
                $("#templateName").val(mailTemplates[template].templateName);
                $('#templateTextArea').val(mailTemplates[template].mailBody);
                $('#templateSubject').val(mailTemplates[template].mailSubject);
                $('#someid').attr('name', 'value');
                isNew = false;
                break;
            }
        }
    }
    $('#saveTemplate').attr('onclick',
        "saveTemplate('" + id + "'," + isNew + ")");
    $('#mailTemplatesLabel').html(title);
}

function viewTemplatePage() {
    $('#editTemplate').addClass('d-none');
    $('#createNewTemplate').removeClass('d-none');
    $('#mailTemplateTable').removeClass('d-none');
    $('#mailTemplateTable').addClass('slide-right');
    $('#mailTemplatesLabel').html('Mail Templates');
    $('.text-danger').html('');
    $('#mailTemplateModal input').val('').change();
    $('#variables').val('variables');
    $('#templateTextArea').val('');
}

function saveTemplate(id, isNew) {
    var templateName = $("#templateName").val().trim();
    var templateDescription = $('#templateTextArea').val().trim();
    var templateSubject = $('#templateSubject').val().trim();

    if (templateName == '' || templateName == undefined) {
        $('#templateNameInvalid').text('Enter Mail Template Name');
        $('#templateNameInvalid').removeClass('d-none');
        return;
    }
    if (templateSubject == '' || templateSubject == undefined) {
        $('#templateSubjectInvalid').text('Enter Subject');
        $('#templateSubjectInvalid').removeClass('d-none');
        return;
    }
    if (templateDescription == '' || templateDescription == undefined) {
        $('#templateTextAreaInvalid').text('Enter Mail Content');
        $('#templateTextAreaInvalid').removeClass('d-none');
        return;
    }

    $.post({
        url: window.location.origin + '/brand/view-campaign/save-mail-template',
        data: {
            templateName: templateName
            , templateId: id
            , templateSubject: templateSubject
            , templateBody: templateDescription
            , isNew: isNew
        },
    })
        .done(function (data) {
            toast("Mail Template saved successfully");
            $('.' + data.templateId).remove();
            $('#mailTemplate').append($('<option>', {
                value: data.templateId,
                text: data.key.templateName,
                'data-value': data.templateBody,
                'data-subject': data.templateSubject,
                'data-isusercreated': 'true',
                'class': 'true ' + data.templateId
            }));
            $
                .get(
                    {
                        url: window.location.origin
                            + '/brand/get-mail-templates',
                    })
                .done(function (data) {
                    $('#table_mailTemplate tbody')
                        .html('');
                    $('#dropdownTemplates select')
                        .html(
                            '<option value="" hidden="true">Mail Template</option>');
                    mailTemplates = [];

                    for (var k in data) {
                        var mailData = new Object();
                        if (data[k].isUserCreated) {
                            appendMailTemplate(data[k]);
                            mailData.isUserCreated = true;
                        } else {
                            mailData.isUserCreated = false;
                        }
                        mailData.id = data[k].id;
                        mailData.mailBody = data[k].mailBody;
                        mailData.mailSubject = data[k].mailSubject;
                        mailData.paymentType = null;
                        mailData.subPaymentType = null;
                        mailData.templateName = data[k].templateName;
                        mailTemplates.push(mailData);
                    }
                }).fail(function (data) {
                    window.location.reload();
                });
            var delayInMilliseconds = 2000;
            setTimeout(function () {
                viewTemplatePage();
            }, delayInMilliseconds);
        }).fail(function (data) {
            if (data.status == 409) {
                toast(data.responseText, 'error');
            } else {
                toast('Failed to add post', 'error');
            }
        });
}

function resetMailTemplate() {
    $('.text-danger').html('');
    $('#mailTemplateModal input').val('').change();
    $('#variables').val('variables');
    $('#templateTextArea').val('');
    resetForms();
    viewTemplatePage();
    $('#mailTemplateTable').removeClass('slide-right');
    $('#editTemplate').removeClass('slide-left');
}

$('#templateTextArea').on('input', function () {
    $('#templateTextAreaInvalid').text('');
});

$('#templateSubject').on('input', function () {
    $('#templateSubjectInvalid').text('');
});
$('#templateName').on('input', function () {
    $('#templateNameInvalid').text('');
});

function appendMailTemplate(data) {
    $('#table_mailTemplate tbody')
        .append(
            '<tr><td style="text-align: start;">'
            + data.templateName
            + '</td><td style="text-align: start;"><div class="row"><div style="margin: auto;"><a id="'
            + data.id
            + '" class="btn-primary btn-sm waves-effect">Edit</a></div></div></td></tr>');
    $('#' + data.id).attr('onclick',
        'viewMailEditPage("' + data.templateName + '","' + data.id + '")');

    $('#dropdownTemplates select').append(
        '<option value="' + data.id + '">' + data.templateName
        + '</option>');
}


var fieldFlag = '';
$('#templateSubject').click(function () {
    fieldFlag = 'subject';
});

$('#templateTextArea').click(function () {
    fieldFlag = 'content';
});

$('#variables').on('change', function () {
    if ($('#variables').val() != "variables") {
        insertAtCursor(fieldFlag, $('#variables').val());
    }
    $('#variables').val('variables');
});

function insertAtCursor(field, myValue) {
    var myField;
    if (field == 'subject') {
        myField = $('#templateSubject')[0];
    } else if (field == 'content') {
        myField = $('#templateTextArea')[0];
    }
    // IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    // MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos) + myValue
            + myField.value.substring(endPos, myField.value.length);
        myField.selectionStart = startPos + myValue.length;
        myField.selectionEnd = startPos + myValue.length;
    } else {
        myField.value += myValue;
    }
}

function resetModal() {
    $('.text-danger').html('');
    $('#bulkMailModal select').val('').change();
    $('#copyInfluencersModal select').val('').change();
}

//bulk mail
function sendBulkMail() {
    var selectedTemplateId = $('#bulkMailTemplate').val();
    var campaignId = window.location.href.split("/")[5];

    if (selectedTemplateId == '' || selectedTemplateId == undefined) {
        $('#bulkMailTemplateInvalid').text('Select Mail Template');
        $('#bulkMailTemplateInvalid').removeClass('d-none');
        return;
    }
    $('#sendBulk').addClass('disabled');
    $.post({
        url: window.location.origin + "/brand/view-campaign/send-bulk-mail-new",
        traditional: true,
        data: {
            campaignId: campaignId
            , influencerIds: rowsSelected
            , templateId: selectedTemplateId
        },
    }).done(function (data) {
        campaignWFIDT.ajax.reload(null, false);
        campaignWFRPDT.ajax.reload(null, false);
        var someTabTriggerEl = document.querySelector('#reviewProposal-tab');
        var tab = new bootstrap.Tab(someTabTriggerEl);
        tab.show();
        $('#mailTemplateNewModal').modal('hide');
        toast("Your E-mail was Sent Successfully");
        $('#sendBulk').removeClass('disabled');
        $('#table_general tbody tr').removeClass('selected');
        $('.dt-body-center input').prop('checked', false);
        $('#bulkMail').addClass('disabled');
        rowsSelected = [];
        $(".close").click();
    }).fail(function (data) {
        $('#sendBulk').removeClass('disabled');
        toast('Failed to Send Mail', 'error');
        $('#bulkMail').addClass('disabled');
        rowsSelected = [];
    });
}

function submitBid() {
    var postTypeBoolean = true;
    var postDurationBoolean = true;
    var bidBoolean = true;
    var paymentTypeBoolean = true;
    var campaignBoolean = true;

    // var postType = document.getElementById("postType");
    // var postDuration = document.getElementById("postDuration");
    // var paymentType = $('#paymentType').val();
    // var bidAmount = $('#bid').val();
    // var affiliateType = $('#affiliateType').val();

    var mailTemplate = $("#mailTemplate").val();
    var mailDescription = $('#mailTextArea').val();
    var mailSubject = $('#mailSubject').val();
    var mailCc = $('#mailCc').val();
    mailCc = mailCc.split(' ').join(',');
    var paymentTypeMessage = 'Select Payment Type';
    // var platform = $('#platform-submit-bid').val();

    if (paymentType == '') {
        // $('#paymentType').addClass("border-danger");
        // $('#paymentTypeInvalid').html(paymentTypeMessage);
        paymentTypeBoolean = false;
    } else if (paymentType == "Affiliate") {
        // var affiliateType = $('#affiliateType').val();
        if (affiliateType == "%" && (bidAmount <= 0 || bidAmount > 100)) {
            // $('#bid').addClass("border-danger");
            // $('#bidAmountInvalid').html("Enter a valid percentage");
            bidBoolean = false;
        } else if (affiliateType == "$" && (bidAmount <= 0)) {
            // $('#bid').addClass("border-danger");
            // $('#bidAmountInvalid').html("Enter a valid fee");
            bidBoolean = false;
        }
    }

    if (paymentType != "Affiliate") {
        affiliateType = null;
        // postType.setCustomValidity('Select Post Type');
        if (postType == '') {
            // $('#postType').addClass("border-danger");
            // $('#postTypeInvalid').html(postType.validationMessage);
            postTypeBoolean = false;
        }

        if (platform == 'instagram') {
            // postDuration.setCustomValidity('Select Post Duration');
            if (postDuration == '') {
                // $('#postDuration').addClass("border-danger");
                // $('#postDurationInvalid').html(postDuration.validationMessage);
                postDurationBoolean = false;
            }
        }

        if (postType == "Story" || (postType == "Image" && (postDuration == "24H" || postDuration == "1W"))) {
            // var bid = document.getElementById("bid");
            // bid.setCustomValidity('Enter the bid amount');
            if (bidAmount == "" || Number(bidAmount) <= 0) {
                // $('#bid').addClass("border-danger");
                // $('#bidAmountInvalid').html(bid.validationMessage);
                bidBoolean = false;
            }
        } else {
            // var bid = document.getElementById("bid");
            // var minimumAmount = (influencerPricing * 0.75).toFixed(0);
            // bid.setCustomValidity('Your Bid should be greater than 0$');
            if (Number(bidAmount) > 0) {
                bidBoolean = true;
            } else {
                // $('#bid').addClass("border-danger");
                // $('#bidAmountInvalid').html(bid.validationMessage);
                bidBoolean = false;
            }
        }
    }

    if (postTypeBoolean && postDurationBoolean && bidBoolean && paymentTypeBoolean && campaignBoolean) {
        $('.cancel').addClass('disabled');
        $('.submit-bid').addClass('disabled');
        $('#submitBid').addClass('disabled');
        var postTypeVal = postType;//$('#postType').val();
        var paymentTypeVal = paymentType;//$('#paymentType').val();
        var postDurationVal = platform == 'instagram' ? postDuration : '';

        if (paymentTypeVal == 'Affiliate') {
            postTypeVal = null;
            postDurationVal = null;
        }

        $.post({
            url: window.location.origin + '/brand/view-influencer/send-proposal',
            data: {
                postType: postTypeVal
                , postDuration: postDurationVal
                , paymentType: paymentTypeVal
                , bidAmount: (parseFloat(bidAmount)).toFixed(2)
                , affiliateType: affiliateType
                , campaignId: window.location.href.split("/")[5]
                , isValid: true
                , mailTemplateId: mailTemplate
                , mailDescription: mailDescription
                , mailSubject: mailSubject
                , mailCc: mailCc
                , influencerId: ginfluencerId
                , influencerHandle: influencerHandle
                , reportId: ''
                , reportPresent: false
                , platform: platform
            },
        }).done(function (data) {
            campaignWFIDT.ajax.reload(null, false);
            campaignWFRPDT.ajax.reload(null, false);
            var someTabTriggerEl = document.querySelector('#reviewProposal-tab');
                var tab = new bootstrap.Tab(someTabTriggerEl);
                tab.show();
            $('#mailTemplateNewModal').modal('hide');
            // $('#SubmitBid').modal('hide');
            // $('#submitBid').removeClass('disabled');
            toast(data.data);
            $(".close").click();

            // var delayInMilliseconds = 2000;

            // setTimeout(function() {
            // location.reload();
            // }, delayInMilliseconds);
        }).fail(function (error, xhr) {
            // $('.cancel').removeClass('disabled');
            // $('#submitBid').removeClass('disabled');
            // $('.submit-bid').removeClass('disabled');
            // $('#SubmitBid').modal('hide');
            toast('Choose a mail template. Failed to submit bid.', 'error');
        });
    }
}

$('#bulkMailTemplate').change(function () {
    $('#bulkMailTemplateInvalid').addClass('d-none');
})

$(function () {
    $("#mailTemplate")
        .change(
            function () {
                $('#mailTemplateInvalid').text('');

                if ($("#mailTemplate").val() != ''
                    && $("#mailTemplate").val() != undefined
                    && $("#mailTemplate").val() != null) {
                    var element = $(this).find('option:selected');
                    var mailDescription = element.attr("data-value");
                    var mailSubject = element.attr("data-subject");

                    var mailPlatfrom = getPlatform();
                    mailDescription = mailDescription.split("<Platform>")
                        .join(mailPlatfrom);
                    mailSubject = mailSubject.split("<Brand Name>")
                        .join(profileDetails.brandName);
                    mailSubject = mailSubject.split("<Influencer Handle>")
                        .join(influencerHandle);
                    mailSubject = mailSubject.split("<Estimated Pricing>")
                        .join('$' + Number(influencerPricing).toFixed(2));

                    //********************** NEED TO CHANGE START ****************************** */
                    mailSubject = mailSubject.split(
                        "<Full Name>").join(
                            influencerHandle);
                    //*********************** NEED TO CHANGE END ***************************** */

                    mailDescription = mailDescription.split(
                        "<Influencer Handle>").join(
                            influencerHandle);

                    //********************** NEED TO CHANGE START ****************************** */
                    mailDescription = mailDescription.split(
                        "<Full Name>").join(
                            influencerHandle);
                    //************************ NEED TO CHANGE END **************************** */

                    mailDescription = mailDescription.split(
                        "<Brand Name>").join(
                            profileDetails.brandName);
                    mailDescription = mailDescription.split(
                        "<Bid Amount>").join(bidAmount);
                    mailDescription = mailDescription.split(
                        "<Post Type>").join(getPostType());
                    if (platform == 'instagram') {
                        var postDuration = getPostDuration();
                        mailDescription = mailDescription.split(
                            "<Post Duration>").join(postDuration);
                        var affiliateInstaText = '\nTo maximize your potential earnings, we recommend you to do the following:\n\n'
                            + '\tkeep your unique affiliate link in your bio (direct your audience to your bio)\n'
                            + '\tcreate 1 feed post\n'
                            + '\tcreate 2 stories every week\n';
                        mailDescription = mailDescription.split("<Affiliate Instagram>").join(affiliateInstaText);
                    } else {
                        mailDescription = mailDescription.split("<Post Duration>")
                            .join('');
                        mailDescription = mailDescription.split("<Affiliate Instagram>").join('');
                    }
                    mailDescription = mailDescription.split(
                        "<Affilitate %>").join(bidAmount);
                    mailDescription = mailDescription.split(
                        "<Affiliate Fee>").join(bidAmount);
                    mailDescription = mailDescription.split(
                        "<Estimated Pricing>").join('$' + Number(influencerPricing).toFixed(2));
                    mailDescription = mailDescription.split("<apostrophe>")
                        .join("'");
                    $('#mailTextArea').val(mailDescription);
                    $('#mailSubject').val(mailSubject);
                }
            });
});

function getPlatform() {
    let setPlatform;
    switch (platform) {
        case "instagram":
            setPlatform = "Instagram";
            break;
        case "youtube":
            setPlatform = "YouTube";
            break;
        case "tiktok":
            setPlatform = "TikTok";
            break;
        default:
            break;
    }
    return setPlatform;
}

function getPostType() {
    let setPostType;
    switch (postType) {
        case "IntegratedVideo":
            setPostType = "Integrated Video";
            break;
        case "DedicatedVideo":
            setPostType = "Dedicated Video";
            break;
        case "PostRoll":
            setPostType = "Post Roll";
            break;
        case "Shoutout":
            setPostType = "Shout Out";
            break;
        case "SponsoredPost":
            setPostType = "Sponsored Post";
            break;
        default:
            break;
    }
    return setPostType;
}

function getPostDuration() {
    let setpostDuration;
    switch (postDuration) {
        case "2H":
            setpostDuration = "2 hours";
            break;
        case "6H":
            setpostDuration = "6 hour";
            break;
        case "12H":
            setpostDuration = "12 hour";
            break;
        case "24H":
            setpostDuration = "24 hour";
            break;
        case "1W":
            setpostDuration = "1 week";
            break;
        case "P":
            setpostDuration = "permanent";
            break;
        default:
            break;
    }
    return setpostDuration;
}

// postTrack
$('#bidAmountModal').on('input', function () {
    $('#bidAmountModalInvalid').text('');
});

$('#postUrlModal').on('input', function () {
    $('#postUrlModalInvalid').text('');
});

$('#affiliatePercentageModal').on('input', function () {
    $('#affiliatePercentageModalInvalid').text('');
});

$('#affiliateFeeModal').on('input', function () {
    $('#affiliateFeeModalInvalid').text('');
});
