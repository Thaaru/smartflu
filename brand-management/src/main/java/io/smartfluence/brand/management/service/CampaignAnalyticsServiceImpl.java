package io.smartfluence.brand.management.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.dao.CampaignAnalyticsDao;
import io.smartfluence.cassandra.entities.PostAnalyticsData;
import io.smartfluence.cassandra.entities.PostAnalyticsTriggers;
import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.brand.campaign.analytics.PostAnalyticsModal;
import io.smartfluence.modal.brand.dashboard.PostDataModal;
import io.smartfluence.modal.brand.dashboard.PostDataModal.PostDataModalBuilder;
import io.smartfluence.modal.social.user.instagram.RawInstagramMediaInfo;
import io.smartfluence.modal.social.user.tiktok.RawTikTokMediaInfo;
import io.smartfluence.modal.social.user.youtube.RawYouTubeVideoInfo;
import io.smartfluence.proxy.social.SocialDataProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class CampaignAnalyticsServiceImpl implements CampaignAnalyticsService {

	@Autowired
	private CampaignAnalyticsDao campaignAnalyticsDao;

	@Autowired
	private SocialDataProxy socialDataProxy;

	@Override
	public ResponseEntity<Object> getAnalyticsData(UUID campaignId) {
		return campaignAnalyticsDao.getAnalyticsData(campaignId);
	}

	@Override
	public ResponseEntity<Object> getPostAnalyticsData(UUID campaignId) {
		UUID brandId = ResourceServer.getUserId();
		List<PostAnalyticsModal> listAnalyticsModal = new LinkedList<>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<PostAnalyticsData> analyticsPostDatas = campaignAnalyticsDao.getPostAnalyticsDataByBrandIdAndCampaignId(brandId, campaignId);

		if (!analyticsPostDatas.isEmpty()) {
			for (PostAnalyticsData analyticsPostData : analyticsPostDatas) {
				PostAnalyticsModal analyticsModal = new PostAnalyticsModal();
				analyticsModal.setLikes(analyticsPostData.getNoOfLikes());
				analyticsModal.setComments(analyticsPostData.getNoOfComments());
				analyticsModal.setViews(analyticsPostData.getNoOfViews());
				analyticsModal.setEngagement(analyticsModal.getLikes() + analyticsModal.getComments() + analyticsModal.getViews());
				analyticsModal.setInfluencerId(analyticsPostData.getKey().getInfluencerId());
				analyticsModal.setInfluencerHandle(analyticsPostData.getInfluencerHandle());
				analyticsModal.setUpdatedData(format.format(analyticsPostData.getUpdatedDate()));
				analyticsModal.setInfluencerHandle(analyticsPostData.getInfluencerHandle());
				analyticsModal.setBrandId(analyticsPostData.getKey().getBrandId());
				analyticsModal.setPostId(analyticsPostData.getKey().getPostId());
				analyticsModal.setPostName(analyticsPostData.getPostName());
				analyticsModal.setPlatform(analyticsPostData.getPlatform().name().toLowerCase());
				listAnalyticsModal.add(analyticsModal);
			}
		}
		return ResponseEntity.ok(listAnalyticsModal);
	}

	@Override
	public void getPostData() {
		List<PostAnalyticsTriggers> listPostAnalyticsTriggers = campaignAnalyticsDao.getPostAnalyticsTriggers(false);

		try {
			if(!StringUtils.isEmpty(listPostAnalyticsTriggers))
				processPostData(listPostAnalyticsTriggers);
		} catch (Exception e) {
			log.error("failed to update and insert the post data " + e.getStackTrace());
		}
	}

	private void processPostData(List<PostAnalyticsTriggers> listPostAnalyticsTriggers) {
		Date updatedDate = new Date();

		listPostAnalyticsTriggers.forEach(data -> {
			data.setPostTimeHourIndex(data.getPostTimeHourIndex() + 1);

			PostDataModal postDataModal = getpostData(data);

			if(!ObjectUtils.isEmpty(postDataModal)) {
				campaignAnalyticsDao.updatePostAnalyticsTriggers(data);
				campaignAnalyticsDao.insertPostData(data, postDataModal, updatedDate);
			}
		});
	}

	private PostDataModal getpostData(PostAnalyticsTriggers postAnalyticsTriggers) {
		PostDataModalBuilder postDataModal = PostDataModal.builder();

		if (postAnalyticsTriggers.getPlatform().equals(Platforms.INSTAGRAM))
			getInstagramPostData(postAnalyticsTriggers.getPostUrl(), postDataModal);
		else if (postAnalyticsTriggers.getPlatform().equals(Platforms.YOUTUBE))
			getYoutubePostURL(postAnalyticsTriggers.getPostUrl(), postDataModal);
		else if (postAnalyticsTriggers.getPlatform().equals(Platforms.TIKTOK))
			getTiktokPostURL(postAnalyticsTriggers.getPostUrl(), postDataModal);

		return postDataModal.build();
	}

	private void getInstagramPostData(String postURL, PostDataModalBuilder postDataModal) {
		ResponseEntity<RawInstagramMediaInfo> response = socialDataProxy.getInstagramMediaInfo(postURL.substring(postURL.lastIndexOf("/") + 1));

		if (response.getStatusCode().is2xxSuccessful()) {
			postDataModal.userName(response.getBody().getItems().get(0).getUser().getUserName())
				.caption(response.getBody().getItems().get(0).getCaption() != null ? response.getBody().getItems().get(0).getCaption().getText() : null)
				.noOfLikes(response.getBody().getItems().get(0).getLikeCount().intValue())
				.noOfComments(response.getBody().getItems().get(0).getCommentCount().intValue())
				.noOfViews(0).build();
		}
	}

	private void getYoutubePostURL(String postURL, PostDataModalBuilder postDataModal) {
		ResponseEntity<RawYouTubeVideoInfo> response = socialDataProxy.getYoutubeVideoInfo(postURL);

		if (response.getStatusCode().is2xxSuccessful()) {
			postDataModal.userName(response.getBody().getVideo().getChannelId())
					.caption(response.getBody().getVideo().getTitle() != null ? response.getBody().getVideo().getTitle() : null)
					.noOfLikes(response.getBody().getVideo().getLikes().intValue())
					.noOfComments(response.getBody().getVideo().getComments().intValue())
					.noOfViews(response.getBody().getVideo().getViews().intValue()).build();
		}
	}

	private void getTiktokPostURL(String postURL, PostDataModalBuilder postDataModal) {
		ResponseEntity<RawTikTokMediaInfo> response = socialDataProxy.getTiktokMediaInfo(postURL);

		if (response.getStatusCode().is2xxSuccessful()) {
			postDataModal.userName(response.getBody().getMedia().getItemInfo().getItemStruct().getAuthor().getUniqueName())
					.caption(response.getBody().getMedia().getItemInfo().getItemStruct().getDescription() != null
								? response.getBody().getMedia().getItemInfo().getItemStruct().getDescription() : null)
					.noOfLikes(response.getBody().getMedia().getItemInfo().getItemStruct().getStats().getLikes().intValue())
					.noOfComments(response.getBody().getMedia().getItemInfo().getItemStruct().getStats().getComments().intValue())
					.noOfViews(response.getBody().getMedia().getItemInfo().getItemStruct().getStats().getViews().intValue()).build();
		}
	}
}