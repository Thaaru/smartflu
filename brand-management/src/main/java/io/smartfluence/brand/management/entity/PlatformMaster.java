package io.smartfluence.brand.management.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "platform_master_new")
public class PlatformMaster {
    @Id
    @Column(name = "platform_id")
    private Long platformId;

    @Column(name = "platform_name")
    private String platformName;

}
