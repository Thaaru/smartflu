package io.smartfluence.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.BulkMailHistory;
import io.smartfluence.cassandra.entities.primary.BulkMailHistoryKey;

@Repository
public interface BulkMailHistoryRepo extends CassandraRepository<BulkMailHistory, BulkMailHistoryKey> {

}