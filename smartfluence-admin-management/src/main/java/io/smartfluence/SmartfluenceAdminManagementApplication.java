package io.smartfluence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.QueryLogger;

import io.smartfluence.beans.WebContainer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
@EntityScan("io.smartfluence.mysql.entities")
@EnableJpaRepositories("io.smartfluence.mysql.repository")
@EnableCassandraRepositories("io.smartfluence.cassandra.repository")
@Configuration
public class SmartfluenceAdminManagementApplication {

	static final int MIN_LOG_ROUNDS = 4;
	static final int MAX_LOG_ROUNDS = 31;

	public static void main(String[] args) {
		SpringApplication.run(SmartfluenceAdminManagementApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(prefix="smartfluence",name="use-ssl")
	public WebContainer webContainer() {
		return new WebContainer();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(MIN_LOG_ROUNDS);
	}

	@Bean
	public QueryLogger queryLogger(Cluster cluster) {
		QueryLogger queryLogger = QueryLogger.builder().build();
		cluster.register(queryLogger);
		return queryLogger;
	}
}