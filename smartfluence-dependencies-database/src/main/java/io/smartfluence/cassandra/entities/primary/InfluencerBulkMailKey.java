package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerBulkMailKey implements Serializable {

	private static final long serialVersionUID = 6666012364104509497L;

	public InfluencerBulkMailKey(InfluencerBulkMailKey key) {
		this.bulkMailId = key.getBulkMailId();
		this.influencerBulkMailId = key.getInfluencerBulkMailId();
	}

	@PrimaryKeyColumn(name = "bulk_mail_id", type = PrimaryKeyType.PARTITIONED)
	private UUID bulkMailId;

	@PrimaryKeyColumn(name = "influencer_bulk_mail_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID influencerBulkMailId;
}