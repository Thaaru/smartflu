import React from "react";

export const productHandler = data => {
    const products = [];
    {
        if (data != null) {
            if (data.products != null && data.productsAcceptedList!=null) {
                for (let i = 0; i < data.products.length; i++) {
                    products.push(false);
                }
                data.productsAcceptedList.map((productsAccepted, index1) => {
                    data.products.map((product, index2) => {
                        if (productsAccepted.productId == product.productId) {
                            products[index2] = true;
                        }
                    })
                })
            }
        }
        return products;
    }
}