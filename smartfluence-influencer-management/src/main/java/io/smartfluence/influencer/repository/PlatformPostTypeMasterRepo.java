package io.smartfluence.influencer.repository;

import io.smartfluence.influencer.entities.PlatformPostTypeMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlatformPostTypeMasterRepo extends JpaRepository<PlatformPostTypeMaster,Long> {
    PlatformPostTypeMaster findByPostTypeId(Long PostTypeId);
}
