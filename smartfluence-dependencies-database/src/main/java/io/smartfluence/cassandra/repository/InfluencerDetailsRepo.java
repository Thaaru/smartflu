package io.smartfluence.cassandra.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.InfluencerDetails;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.UserStatus;

@Repository
public interface InfluencerDetailsRepo extends CassandraRepository<InfluencerDetails, UserDetailsKey> {

	List<BrandDetails> findAllByKeyUserStatusIn(Set<UserStatus> userStatus);
}