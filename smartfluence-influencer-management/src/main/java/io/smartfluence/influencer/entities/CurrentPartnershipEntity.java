package io.smartfluence.influencer.entities;

import lombok.*;
import java.util.List;


@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CurrentPartnershipEntity {

    String userId;
    List<CurrentPartnershipCampainDetails> campainDetailsList;
    String userEmail;
    String userName;

}
