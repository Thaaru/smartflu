package io.smartfluence.cassandra.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.MailHistoryKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("mail_history")
public class MailHistory implements Serializable {

	private static final long serialVersionUID = -2719197978411398258L;

	@PrimaryKey
	private MailHistoryKey mailHistoryKey;

	@Column("brand_name")
	private String brandName;

	@Column("mail_body")
	private String mailBody;

	@Column("mail_subject")
	private String mailSubject;

	@Column("mail_cc")
	private String mailCc;

	@Column("mail_from")
	private String mailFrom;

	@Column("mail_to")
	private String mailTo;

	@Column("mail_template_id")
	private UUID mailTemplateId;

	@LastModifiedDate
	@Column("modified_at")
	private Date modifiedAt;

	@Column("status")
	private int status;

	@Indexed
	@Column("mapping_id")
	private UUID mappingId;
}