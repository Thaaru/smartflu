getProfileDetails();
$(document).ready(function() {
	$('#dob').inputmask();
});
function submitProfileDetails(obj) {
	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');
	xhr.done(function(d, s, r) {
		SUCCESS('Profile details saved')
	});
	xhr.fail(function(x, s, t) {
		ERROR('Failed to save profile details');
	});
}

function getProfileDetails() {
	$.get({
		url : '/influencer/profile'
	}).done(function(d, s, r) {
		setProfileDetailsForm(d);
	})
}

function setProfileDetailsForm(d) {
	for ( var k in d) {
		var elem = $('[name="' + k + '"]');
		elem.val(d[k]).change();
		elem.parent().children('label').addClass('active');
	}
}