package io.smartfluence.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class InvalidUserException extends RuntimeException {

	private static final long serialVersionUID = -349396815576980274L;

	public InvalidUserException(String message) {
		super(message);
	}
}