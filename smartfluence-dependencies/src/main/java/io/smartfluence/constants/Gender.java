package io.smartfluence.constants;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Gender {
	MALE("MALE", 0), FEMALE("FEMALE", 0);

	@JsonProperty("code")
	private String code;

	@JsonProperty("weight")
	private Integer weight;

	private Gender(String code, Integer weight) {
		this.code = code;
		this.weight = weight;
	}

	private void setCode(String code) {
		this.code = code;
	}

	private void setWeight(Integer weight) {
		this.weight = weight;
	}

	@JsonValue
	private String getCode() {
		return code;
	}

	@JsonValue
	private Integer getWeight() {
		return weight;
	}
}