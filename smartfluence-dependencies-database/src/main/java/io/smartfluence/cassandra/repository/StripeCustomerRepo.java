package io.smartfluence.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.StripeCustomer;
import io.smartfluence.cassandra.entities.primary.StripeCustomerKey;

public interface StripeCustomerRepo extends CassandraRepository<StripeCustomer, StripeCustomerKey> {

}