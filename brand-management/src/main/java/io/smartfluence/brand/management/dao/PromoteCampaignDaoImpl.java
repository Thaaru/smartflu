package io.smartfluence.brand.management.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.PromoteInfluencersData;
import io.smartfluence.cassandra.entities.PromoteTargetingDetails;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.cassandra.repository.PromoteInfluencersDataRepo;
import io.smartfluence.cassandra.repository.PromoteTargetingDetailsRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramDemographicRepo;
import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.constants.Platforms;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.repository.BrandCampaignDetailsMysqlRepo;
import io.smartfluence.mysql.repository.MailTemplateMasterRepo;
import io.smartfluence.mysql.repository.SocialIdMasterRepo;

@Repository
public class PromoteCampaignDaoImpl implements PromoteCampaignDao {

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Autowired
	private BrandCampaignDetailsMysqlRepo brandCampaignDetailsMysqlRepo;

	@Autowired
	private PromoteTargetingDetailsRepo promoteTargetingDetailsRepo;

	@Autowired
	private PromoteInfluencersDataRepo promoteInfluencersDataRepo;

	@Autowired
	private SocialIdMasterRepo socialIdMasterRepo;

	@Autowired
	private InstagramDemographicRepo instagramDemographicRepo;

	@Autowired
	private MailTemplateMasterRepo mailTemplateMasterRepo;

	@Override
	public Optional<BrandDetails> getBrandDetails(UserDetailsKey key) {
		return brandDetailsRepo.findById(key);
	}

	@Override
	public void saveBrandCampaignDetailsMysql(BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		brandCampaignDetailsMysqlRepo.save(brandCampaignDetailsMysql);
	}

	@Override
	public Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByCampaignId(String campaignId) {
		return brandCampaignDetailsMysqlRepo.findByCampaignId(campaignId);
	}

	@Override
	public Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByCampaignNameAndBrandId(String campaignName, String brandId) {
		return brandCampaignDetailsMysqlRepo.findByCampaignNameAndBrandId(campaignName, brandId);
	}

	@Override
	public List<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByBrandIdAndIsPromote(String brandId, Boolean isPromote) {
		return brandCampaignDetailsMysqlRepo.findAllByBrandIdAndIsPromote(brandId, isPromote);
	}

	@Override
	public List<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlCampaignStatusAndIsPromote(CampaignStatus campaignStatus, Boolean isPromote) {
		return brandCampaignDetailsMysqlRepo.findAllByCampaignStatusAndIsPromote(campaignStatus, isPromote);
	}

	@Override
	public Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlCampaignIdAndBrandId(String campaignId, String brandId) {
		return brandCampaignDetailsMysqlRepo.findByCampaignIdAndBrandId(campaignId, brandId);
	}

	@Override
	public void savePromoteTargetingDetails(PromoteTargetingDetails promoteTargetingDetails) {
		promoteTargetingDetailsRepo.save(promoteTargetingDetails);
	}

	@Override
	public Optional<PromoteTargetingDetails> getPromoteTargetingDetailsByCampaignId(UUID campaignId) {
		return promoteTargetingDetailsRepo.findById(campaignId);
	}

	@Override
	public void updatePromoteInfluencerData(PromoteInfluencersData promoteInfluencersData) {
		promoteInfluencersDataRepo.save(promoteInfluencersData);
	}

	@Override
	public void saveAllPromoteInfluencersData(List<PromoteInfluencersData> promoteInfluencersData) {
		promoteInfluencersDataRepo.saveAll(promoteInfluencersData);
	}

	@Override
	public Optional<PromoteInfluencersData> getPromoteInfluencersDataByCampaignIdAndInfluencerDeepSocialId(UUID campaignId, String influencerDeepSocialId) {
		return promoteInfluencersDataRepo.findByKeyCampaignIdAndKeyInfluencerDeepSocialId(campaignId, influencerDeepSocialId);
	}

	@Override
	public List<PromoteInfluencersData> getPromoteInfluencersDataByCampaignId(UUID campaignId) {
		return promoteInfluencersDataRepo.findAllByKeyCampaignId(campaignId);
	}

	@Override
	public Optional<SocialIdMaster> getSocialIdMasterById(String influencerId) {
		return socialIdMasterRepo.findById(influencerId);
	}

	@Override
	public Optional<SocialIdMaster> getSocialIdMasterByHandleAndPlatform(String influencerHandle, Platforms platform) {
		return socialIdMasterRepo.findByInfluencerHandleAndPlatform(influencerHandle, platform);
	}

	@Override
	public Optional<InstagramDemographics> getInstagramDemographics(String instagramHandle) {
		return instagramDemographicRepo.findById(instagramHandle);
	}

	@Override
	public Optional<MailTemplateMaster> getMailTemplateMasterByTemplateName(String name) {
		return mailTemplateMasterRepo.findByTemplateName(name);
	}
}