package io.smartfluence.proxy.social;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClientProperties.FeignClientConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.RequestInterceptor;

@Configuration
@EnableFeignClients
public class SocialDataConfiguration extends FeignClientConfiguration {

	@Bean
	public RequestInterceptor apiKeyInterceptor() {
		return requestTemplate -> {
			requestTemplate.query("api_token", "2emk2snzmps0gwg");
		};
	}
}