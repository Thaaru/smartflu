package io.smartfluence.influencer.services;

import com.amazonaws.HttpMethod;
import io.smartfluence.influencer.entities.TermsConditions;
import io.smartfluence.influencer.entities.TermsType;
import io.smartfluence.influencer.repository.TermsRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class TermsConditionService {

    @Autowired
    TermsRepo termsRepo;

    @Autowired
    StorageService storageService;

    public TermsConditions getTermsConditionsDetails(Long termId){
        Logger.getLogger("getTermsConditionsDetails").log(Level.FINE,"Getting the Terms & Conditions Details");
        TermsConditions termsConditions = termsRepo.findByTermId(termId);
        if(termsConditions.getType().equals(TermsType.CUSTOM)){
            termsConditions.setTermsConditionFilePath(storageService.generatePresignedUrl(termsConditions.getTermsConditionFilePath(), HttpMethod.GET));
        }
        return termsConditions;
    }

}
