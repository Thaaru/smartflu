import React from "react";
import { updateCampaignDetailsApi } from "../services/api/campaign_details_api";

export default async function submit(data,loader){

        loader(false);
        const result = await updateCampaignDetailsApi(data);
        if (result.code == 600) {
            loader(true);
            return 
        } else {
            loader(true);
            return null;
        }

}