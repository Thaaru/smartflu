getAboutYouDetails();

function getAboutYouDetails(){
	$.get({
		url : '/influencer/influencer-preference'
	}).done(function(d, s, r) {
		setAboutYouDetailsForm(d);
	})
}

function submitAboutYou(obj) {

	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');
	xhr.done(function(d, s, r) {
		SUCCESS('Details saved successfully')
	});
	xhr.fail(function(x, s, t) {
		ERROR('Failed to save details');
	});
}

function setAboutYouDetailsForm(d) {
	for ( var k in d) {
		var elem = $('[name="' + k + '"]');
		elem.val(d[k]).change();
		elem.parent().children('label').addClass('active');
	}
}