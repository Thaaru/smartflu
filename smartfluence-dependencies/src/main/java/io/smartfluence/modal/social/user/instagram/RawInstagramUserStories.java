package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawInstagramUserStories implements Serializable {

	private static final long serialVersionUID = -4643432096637523050L;

	@JsonAlias("status")
	private String status;

	@JsonAlias("reel")
	private StoriesReel reel;
}