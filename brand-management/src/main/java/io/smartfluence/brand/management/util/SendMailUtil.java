package io.smartfluence.brand.management.util;

import java.time.Instant;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.quartz.JobDataMap;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import io.smartfluence.brand.management.jobs.InfluencerDataQueueJob;
import io.smartfluence.brand.management.jobs.SendMailJob;
import io.smartfluence.mysql.entities.InfluencerDataQueue;
import io.smartfluence.mysql.entities.MailDetails;
import io.smartfluence.mysql.repository.InfluencerDataQueueRepo;
import io.smartfluence.mysql.repository.MailDetailsRepo;

@Component
@Configuration
public class SendMailUtil {

	@Autowired
	private BatchScheduler batchScheduler;

	@Autowired
	private MailDetailsRepo mailDetailsRepo;

	@Autowired
	private InfluencerDataQueueRepo influencerDataQueueRepo;

	@Value("${bulk.mail.config}")
	private int mailDelay;

	private static Calendar lastSentMailTime = null;

	private Queue<MailDetails> mailQueue = new LinkedList<>();

	public void addMailToQueue(MailDetails mailDetails) {
		mailQueue.add(mailDetails);
	}

	public void addMailToQueue(List<MailDetails> mailDetails) {
		mailQueue.addAll(mailDetails);
	}

	public void sendMail() {
		if (!mailQueue.isEmpty())
			scheduleMailJob();
	}

	@EventListener(ApplicationReadyEvent.class)
	public void scheduleFailedMailJob() {
		getMailDetails().forEach(data -> {
			createMailQueueJob(data);
		});
	}

	private void scheduleMailJob() {
		while(!mailQueue.isEmpty()) {
			createMailQueueJob((MailDetails) mailQueue.poll());
		}
	}

	private void createMailQueueJob(MailDetails mailDetails) {
		JobDataMap dataMap = new JobDataMap();
		dataMap.put("mailDetailsData", mailDetails);
		Calendar calendar = getScheduleTime();
		calendar.add(Calendar.MINUTE, mailDelay);
		lastSentMailTime = calendar;
		try {
			saveMailDetails(mailDetails);
			batchScheduler.schedule(SendMailJob.class, calendar.getTime(), dataMap);
		} catch (SchedulerException ex) {
			ex.getStackTrace();
		}
	}

	public void scheduleInfluencerReportMailJob(InfluencerDataQueue influencerDataQueue) {
		createInfluencerReportMailJob(influencerDataQueue);
	}

	private void createInfluencerReportMailJob(InfluencerDataQueue influencerDataQueue) {
		JobDataMap dataMap = new JobDataMap();
		dataMap.put("influencerDataQueue", influencerDataQueue);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, mailDelay);
		try {
			influencerDataQueueRepo.save(influencerDataQueue);
			batchScheduler.schedule(InfluencerDataQueueJob.class, calendar.getTime(), dataMap);
		} catch (SchedulerException ex) {
			ex.getStackTrace();
		}
	}

	private Calendar getScheduleTime() {
		if (lastSentMailTime == null || lastSentMailTime.toInstant().isBefore(Instant.now()))
			return Calendar.getInstance();
		
		return lastSentMailTime;
	}

	private void saveMailDetails(MailDetails mailDetails) {
		mailDetailsRepo.save(mailDetails);
	}

	private Iterable<MailDetails> getMailDetails() {
		return mailDetailsRepo.findAll();
	}
}