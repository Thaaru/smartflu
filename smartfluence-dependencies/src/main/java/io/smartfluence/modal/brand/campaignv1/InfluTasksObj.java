package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InfluTasksObj {
    private String campaignId;
    private String influencerId;
    private long campaignTaskId;
    private Long platformId;
    private String guidelines;
    private Long postTypeId;
    private String postCustomName;
}
