package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlatformPostTypeEntityV1 {
    private Long postTypeId;
    private String postTypeName;
    private int platformId;
}
