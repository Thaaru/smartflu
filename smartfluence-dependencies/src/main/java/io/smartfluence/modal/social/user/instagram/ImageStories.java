package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageStories implements Serializable {

	private static final long serialVersionUID = 737154309468934387L;

	@JsonAlias("candidates")
	private List<Candidates> candidates;
}