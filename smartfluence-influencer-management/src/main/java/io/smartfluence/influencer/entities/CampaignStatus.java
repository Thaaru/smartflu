package io.smartfluence.influencer.entities;

public enum CampaignStatus {
    INPROGRESS,ACTIVE,INACTIVE,CLOSED
}
