package io.smartfluence.influencer.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_accounts")
public class UserDetails {
    @Id
    public String userId;
    private String brandName;
    public String email;
    private String firstName;
    private String industry;
    private String lastName;
    private String password;
    private String userStatus;
    private String userType;
    private String verificationCode;
    private String website;
    private String inviteId;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private String timeZone;
    private String inviteStatus;

}
