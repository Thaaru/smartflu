package io.smartfluence.brand.management.entity;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class RemoveInfluencerReq {

        @NotNull(message = "Enter Influencer Id")
        private List<String> influencerIds;

        @NotNull(message = "Enter campaign Id")
        private String campaignId;
}
