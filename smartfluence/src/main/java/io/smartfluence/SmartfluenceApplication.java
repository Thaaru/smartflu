package io.smartfluence;

import java.io.IOException;
import java.security.Principal;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.google.common.collect.Sets;

import io.smartfluence.beans.WebContainer;
import io.smartfluence.constants.SecurityConstants;

@RestController
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
@EntityScan({ "io.smartfluence.mysql.entities", "io.smartfluence.cassandra.entities" })
@EnableJpaRepositories("io.smartfluence.mysql.repository")
@EnableCassandraRepositories("io.smartfluence.cassandra.repository")
@EnableFeignClients(value = "io.smartfluence.webapp.proxy", defaultConfiguration = FeignConfiguration.class)
public class SmartfluenceApplication {
	@Autowired
	Environment env;

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	public static void main(String[] args) {
		SpringApplication.run(SmartfluenceApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(prefix="smartfluence",name="use-ssl")
	public WebContainer webContainer() {
		return new WebContainer();	
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(Locale.US);
		localeResolver.setDefaultTimeZone(TimeZone.getTimeZone("UTC"));
		return localeResolver;
	}

	@ResponseBody
	@GetMapping("/user/me")
	public Principal user(HttpServletRequest request, HttpServletResponse response, Principal principal) throws IOException {
		OAuth2Authentication authentication = (OAuth2Authentication) principal;
		if (authentication != null) {
			if (authentication.getAuthorities().contains(SecurityConstants.ADMIN))
				redirectStrategy.sendRedirect(request, response, ServletUriComponentsBuilder.fromCurrentContextPath()
								.host(eurekaInstanceConfigBean.getHostname()).port(-1).path("/admin/brand-management").toUriString());
			else if (authentication.getAuthorities().contains(SecurityConstants.BRAND))
				redirectStrategy.sendRedirect(request, response, ServletUriComponentsBuilder.fromCurrentContextPath()
						.host(eurekaInstanceConfigBean.getHostname()).port(-1).path("/brand/campaign").toUriString());
			else if (authentication.getAuthorities().contains(SecurityConstants.INFLUENCER))
				redirectStrategy.sendRedirect(request, response, ServletUriComponentsBuilder.fromCurrentContextPath()
						.host(eurekaInstanceConfigBean.getHostname()).port(-1).path("/influencer/home").toUriString());
		} else
			redirectStrategy.sendRedirect(request, response, ServletUriComponentsBuilder.fromCurrentContextPath()
					.host(eurekaInstanceConfigBean.getHostname()).port(-1).path("/login").toUriString());

		return principal;
	}

	@Autowired
	public void setAdditionalDialect(SpringTemplateEngine springTemplateEngine) {
		springTemplateEngine.setAdditionalDialects(Sets.newHashSet(new SpringSecurityDialect()));
	}


}