package io.smartfluence.cassandra.entities;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Table("influencer_details")
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerDetails implements Serializable {

	public InfluencerDetails(InfluencerDetails influencerDetails) {
		this.key = new UserDetailsKey(influencerDetails.getKey());
		this.firstName = influencerDetails.getFirstName();
		this.lastName = influencerDetails.getLastName();
		this.industry = influencerDetails.getIndustry();
		this.email = influencerDetails.getEmail();
		this.instagramHandle = influencerDetails.getInstagramHandle();
		this.timeZone=influencerDetails.getTimeZone(); 
	}

	private static final long serialVersionUID = 7149367397344233449L;

	@PrimaryKey
	private UserDetailsKey key;

	@Column("first_name")
	private String firstName;

	@Column("last_name")
	private String lastName;

	@Column("industry")
	private String industry;

	@Column("email")
	private String email;

	@Column("instagram_handle")
	private String instagramHandle;
	
	@Column("time_zone")
	private String timeZone;
	
	@Column("modified_at")
	@LastModifiedDate
	private Date modifiedAt;

	@Column("created_at")
	@CreatedDate
	private Date createdAt;
}