package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import io.smartfluence.constants.SubscriptionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SubscriptionMasterKey implements Serializable {

	private static final long serialVersionUID = -3048895794668439628L;

	@PrimaryKeyColumn(name = "subscription_type", type = PrimaryKeyType.PARTITIONED)
	private SubscriptionType subscriptionType;

	@PrimaryKeyColumn(name = "subscription_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID subscriptionId;
}