package io.smartfluence.cassandra.entities.brand;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandCreditHistoryKey;
import io.smartfluence.constants.CreditType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_credit_history")
public class BrandCreditHistory implements Serializable {

	private static final long serialVersionUID = 5213110848854396744L;

	public BrandCreditHistory(BrandCreditHistory brandCreditHistory) {
		this.key = new BrandCreditHistoryKey(brandCreditHistory.getKey());
		this.creditSpent=brandCreditHistory.getCreditSpent();
		this.creditType=brandCreditHistory.getCreditType();
		this.referenceId=brandCreditHistory.getReferenceId();
	}

	@PrimaryKey
	private BrandCreditHistoryKey key;

	@Column("credit_spent")
	private int creditSpent;

	@Column("credit_type")
	@Enumerated(EnumType.STRING)
	private CreditType creditType;

	@Column("reference_id")
	private UUID referenceId;
}