package io.smartfluence.cassandra.entities.brand;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_payment_details")
public class BrandPaymentDetails implements Serializable {

	private static final long serialVersionUID = -4937231924188163703L;

	public BrandPaymentDetails(BrandPaymentDetails brandPaymentDetails) {
		this.userId = brandPaymentDetails.getUserId();
		this.mailingFullName = brandPaymentDetails.getMailingFullName();
		this.mailingAddress = brandPaymentDetails.getMailingAddress();
		this.mailingCity = brandPaymentDetails.getMailingCity();
		this.mailingZipCode = brandPaymentDetails.getMailingZipCode();
		this.mailingCountry = brandPaymentDetails.getMailingCountry();
		this.mailBill = brandPaymentDetails.isMailBill();
		this.billingFullName = brandPaymentDetails.getBillingFullName();
		this.billingAddress = brandPaymentDetails.getBillingAddress();
		this.billingCity = brandPaymentDetails.getBillingCity();
		this.billingZipCode = brandPaymentDetails.getBillingZipCode();
		this.billingCountry = brandPaymentDetails.getBillingCountry();
		this.creditCardNumber = brandPaymentDetails.getCreditCardNumber();
		this.cardHolderName = brandPaymentDetails.getCardHolderName();
		this.expiryDate = brandPaymentDetails.getExpiryDate();
		this.cvv = brandPaymentDetails.getCvv();
	}

	@PrimaryKey("user_id")
	private UUID userId;

	@Column("mailing_full_name")
	private String mailingFullName;

	@Column("mailing_address")
	private String mailingAddress;

	@Column("mailing_city")
	private String mailingCity;

	@Column("mailing_zip_code")
	private String mailingZipCode;

	@Column("mailing_country")
	private String mailingCountry;

	@Column("mailing_bill")
	private boolean mailBill;

	@Column("billing_full_name")
	private String billingFullName;

	@Column("billing_address")
	private String billingAddress;

	@Column("billing_city")
	private String billingCity;

	@Column("billing_zip_code")
	private String billingZipCode;

	@Column("billing_country")
	private String billingCountry;

	@Column("credit_card_number")
	private String creditCardNumber;

	@Column("card_holder_name")
	private String cardHolderName;

	@Column("expiry_date")
	private String expiryDate;

	@Column("cvv")
	private String cvv;

	@LastModifiedDate
	@Column("modified_at")
	private Date modifiedAt;

	@Column("created_at")
	private Date createdAt;
}