import React from "react";
import { IoIosArrowDropleft, IoIosArrowDropright } from "react-icons/io";
import { PRIMARY_COLOR, SECONDARY_COLOR, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";

export const ArrowButton = ({ status, navigator,buttonHandler }) => {
    switch (navigator) {
        case "backward":
            return <div className="col-2" >
                <IoIosArrowDropleft size={30} color={status == STEPS_ACCEPTED_STATUS ? PRIMARY_COLOR : SECONDARY_COLOR} onClick={status == STEPS_ACCEPTED_STATUS ? buttonHandler : null} />
            </div>

        case "forward":
            return <div className="col-2 " >
                <IoIosArrowDropright size={30} color={status == STEPS_ACCEPTED_STATUS ? PRIMARY_COLOR : SECONDARY_COLOR} onClick={status == STEPS_ACCEPTED_STATUS ? buttonHandler : null} />
            </div>
    }
}