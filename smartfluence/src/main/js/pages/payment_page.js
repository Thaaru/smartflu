import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { HeaderComponent } from "../components/header_component";
import ColoredLine from "../components/line_component";
import { FooterComponent } from "../components/footer_component";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import PhoneInput, { isValidPhoneNumber, parsePhoneNumber } from 'react-phone-number-input/input'
import 'react-phone-number-input/style.css'
import { getStateListApi, updateCampaignDetailsApi } from "../services/api/campaign_details_api";
import { getStateDetails } from "../utils/http_urls";
import { ProgressBar } from "react-bootstrap";
import { IoIosArrowDropleft, IoIosArrowDropright } from "react-icons/io";
import progressStatus from "../components/progressbar_status";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { DETAILS_ACCEPTED_MESSAGE, ERROR_MESSAGE, SMARTFLUENCE_LOGO, SP_ARROWBUTTON_MESSAGE, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";
import { PoweredBy } from "../components/powered_by";
import { CProgressBar } from "../components/progress_bar";
import { loaderMethod } from "../components/loader";
import { ArrowButton } from "../components/arrow_button";

export default function PaymentPage({ data, code, next, back, close }) {
    const [backButtonController, setBackButtonController] = useState(0);
    const [frontButtonController, setFrontButtonController] = useState(0);
    const paymentAddresss = data.paymentAddress;
    const shippingAddress = data.shippingAddress;
    const [stateList, setStateList] = useState(null);
    useEffect(() => {
        const paymentAddress = data.paymentAddress;
        if (paymentAddress != null) {
            setValue("fullName",  paymentAddress != null ? paymentAddress.fullName : "");
            setValue("addressLine1",  paymentAddress != null ? paymentAddress.addressLine1 : "");
            setValue("addressLine2", paymentAddress != null ? paymentAddress.addressLine2 : "");
            setValue("cityName", paymentAddress != null ? paymentAddress.cityName : "");
            setValue("zipcode",paymentAddress != null ? paymentAddress.zipcode : "");
            setValue("country",paymentAddress != null ? paymentAddress.countryId : "");
            setValue("state", paymentAddress != null ? paymentAddress.stateId : "");
            setValue("phoneNumber", paymentAddress != null ? paymentAddress.phoneNumber : "");
            getStateList(paymentAddress.countryId, setStateList);
        }
    }, [])
    const regExs = {
        "US": /^([0-9]{5})(?:[-\s]*([0-9]{4}))?$/,
        "other": /^[ABCEGHJ-NPRSTVXY][0-9][ABCEGHJ-NPRSTV-Z] [0-9][ABCEGHJ-NPRSTV-Z][0-9]$/,
    }
    const schema = yup.object().shape({
        fullName: yup.string().max(50).required("FullName is required").matches("^[a-zA-Z ]*$", "FullName must be alphabets only"),
        addressLine1: yup.string().required("AddressLine is required").matches("^[a-zA-Z0-9#]+(?:[\w --,/]*[a-zA-Z0-9]+)*$", "Address should contains only alphaNumeric and (- , / #)"),
        addressLine2: yup.string().notRequired().nullable().when("addressLine2", {
            is: value => value?.length,
            then: rule => rule.matches("^[a-zA-Z0-9#]+(?:[\w --,/]*[a-zA-Z0-9]+)*$", "Address should contains only alphaNumeric and (- , / #)"),
        }),
        cityName: yup.string().max(20).required("CityName is required").matches("^[A-Za-z]+[A-Za-z ]*$", "City must be alphabets only"),
        zipcode: yup.string().required("Zip Code is required").test("validZipcode", "Zip Code is invalid", function (value) {
            let countryId = this.parent.country;
            let countryList = data.countryList;
            let country = countryList.find(x => x.countryId == countryId)?.countryName;
            let isValidZipcode = (regExs[country] ? regExs[country] : regExs['other']).test(value);
            return isValidZipcode;
        }),
        phoneNumber: yup.string().required("Phone Number is required").test("validPhonenumber", "Phone Number is invalid", function (value) {
            if (value != null && value != "") {
                const phoneNumber = parsePhoneNumber(value);      
                if (phoneNumber != null && phoneNumber != '') {
                    if (phoneNumber.country == "US" || phoneNumber.country == "CA") {
                        return isValidPhoneNumber(value, phoneNumber.country);
                    } 
                }
            }
            return false;
        }),
        country: yup.string().required("Please select country"),
        state: yup.string().required("Please select state"),
        email: yup.string().email().required("Email is required").matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$","Email is invalid"),
        paymentMethodType: yup.string().required("Please choose payment method type")
    },
        [
            ['addressLine2', 'addressLine2'],
        ]
    );

    const { register,
        control,
        watch,
        setValue,
        handleSubmit,
        getValues,
        clearErrors,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });
    const onSubmit = async (event) => {
        var paymentAddress = {};
        paymentAddress["campaignInfluencerAddressType"] = null;
        paymentAddress["fullName"] = event.fullName;
        paymentAddress["addressLine1"] = event.addressLine1;
        paymentAddress["addressLine2"] = event.addressLine2;
        paymentAddress["countryId"] = event.country;
        paymentAddress["cityName"] = event.cityName;
        paymentAddress["zipcode"] = event.zipcode;
        paymentAddress["phoneNumber"] = event.phoneNumber;
        paymentAddress["stateId"] = event.state;
        data.paymentAddress = paymentAddress;
        data.paymentMethodType = event.paymentMethodType;
        data.paymentEmailAddress = event.email;
        data.paymentAcceptedStatus = STEPS_ACCEPTED_STATUS;
        loaderOpen(false);
        const result = await updateCampaignDetailsApi(data);
        if (result.code == 600) {
            toast.success(DETAILS_ACCEPTED_MESSAGE, { delay: 1000 })
            loaderOpen(true);
            next(result.value);

        } else {
            loaderOpen(true);
            toast.error(ERROR_MESSAGE, { delay: 1000 })
            data.paymentAcceptedStatus = null;
            return null;
        }
    }

    const backButtonHandler = () => {
        if (backButtonController == 0) {
            const fieldData = getValues();
            if (fieldData.email != "" && fieldData.paymentMethodType != "" && fieldData.fullName != "" && fieldData.addressLine1 != "" && fieldData.country != "" && fieldData.cityName != "" && fieldData.zipcode != "" && fieldData.phoneNumber != "" && fieldData.state != "") {
                back();
            } else {
                setBackButtonController(1);
                toast.info(SP_ARROWBUTTON_MESSAGE);
            }
        } else {
            back();
        }
    }
    const frontButtonHandler = () => {
        if (frontButtonController == 0) {
            const fieldData = getValues();
            if (fieldData.email != "" && fieldData.paymentMethodType != "" && fieldData.fullName != "" && fieldData.addressLine1 != "" && fieldData.country != "" && fieldData.cityName != "" && fieldData.zipcode != "" && fieldData.phoneNumber != "" && fieldData.state != "") {
                next();
            } else {
                setFrontButtonController(1);
                toast.info(SP_ARROWBUTTON_MESSAGE);
            }
        } else {
            next();
        }
    }
    const handleChange = event => {
        if(event.target.value!=null){
            clearErrors("country")
            if(getValues("zipcode")!=null){
                clearErrors("zipcode")
            }
        }
        getStateList(event.target.value);
    }
    const [paymentMethod, setPaymentMethod] = useState("PAYPAL");
    const paymentHandler = event => {
        setPaymentMethod(event.target.value);
    }
    const getStateList = async (countryId) => {
        if (countryId != null) {
            const responeData = await getStateListApi(countryId);
            const stateList = responeData.value;
            setStateList(stateList);
        }
    }
    const [addressChange, setAddressChange] = useState(false);
    const addressChangeHandler = (event) => {
        setAddressChange(event.target.checked);
        if (event.target.checked == true) {
            getStateList(shippingAddress.countryId);
        }

        setValue("fullName", event.target.checked ? shippingAddress.fullName : (paymentAddresss != null ? paymentAddresss.fullName : ""));
        setValue("addressLine1", event.target.checked ? shippingAddress.addressLine1 : (paymentAddresss != null ? paymentAddresss.addressLine1 : ""));
        setValue("addressLine2", event.target.checked ? shippingAddress.addressLine2 : (paymentAddresss != null ? paymentAddresss.addressLine2 : ""));
        setValue("cityName", event.target.checked ? shippingAddress.cityName : (paymentAddresss != null ? paymentAddresss.cityName : ""));
        setValue("zipcode", event.target.checked ? shippingAddress.zipcode : (paymentAddresss != null ? paymentAddresss.zipcode : ""));
        setValue("country", event.target.checked ? shippingAddress.countryId : (paymentAddresss != null ? paymentAddresss.countryId : ""));
        setValue("state", event.target.checked ? shippingAddress.stateId : (paymentAddresss != null ? paymentAddresss.stateId : ""));
        setValue("phoneNumber", event.target.checked ? shippingAddress.phoneNumber : (paymentAddresss != null ? paymentAddresss.phoneNumber : ""));

    }
    const [loaderComponent, setLoaderComponent] = useState(true);
    // const loaderMethod = () => {
    //     return <div class="spinner-border" role="status" color="white">
    //         <span class="sr-only">Loading...</span>
    //     </div>
    // }
    const loaderOpen = (status) => {
        if (status == true) {
            setLoaderComponent(status);
        } else {
            setLoaderComponent(status)
        }
    }

    return (
        <>
            <div className="content-wrapper">
                <form >
                    <div>
                        <HeaderComponent data={data} close={close} headerName={"Payment Information"} stepNo={5} />
                        <ColoredLine color={'secondaryColor'} />
                        <div className="col-lg forScroll" style={{ "textAlign": "left" }}>
                            <div class=" custom-control custom-checkbox my-2 mr-sm-4" style={{ "paddingLeft": "2.5rem" }}>
                                <input type="checkbox" class="custom-control-input" id="customControlInline" disabled={shippingAddress != null ? false : true} onChange={addressChangeHandler} />
                                <label class="custom-control-label" for="customControlInline">Same as Shipping address</label>
                            </div>
                            {/* <br/> */}
                            <div class="form-group col-lg">
                                <label for="country">Choose Payment Method</label>
                                <select class="form-control"  {...register("paymentMethodType")} defaultValue={data.paymentMethodType} onChange={paymentHandler}>
                                    <option
                                        key="PAYPAL"
                                        value="PAYPAL"
                                    >
                                        PAYPAL
                                    </option>
                                    <option
                                        key="OTHER"
                                        value="OTHER"
                                    >
                                        OTHER PAYMENT{`(${data.brandEmail})`}
                                    </option>
                                </select>
                                <p style={{ "color": "red" }}>{errors.paymentMethodType?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="email">{paymentMethod == "PAYPAL" ? "Paypal Email Address / Email" : "Email Address"}</label>
                                <input class="form-control" {...register("email")} defaultValue={data.paymentEmailAddress} />
                                <p style={{ "color": "red" }}>{errors.email?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="fullName">Full Name</label>
                                <input class="form-control" {...register("fullName")} defaultValue={addressChange ? shippingAddress.fullName : paymentAddresss != null ? paymentAddresss.fullName : null} error={errors.fullName ? true : false} disabled={addressChange ? true : false} />
                                <p style={{ "color": "red" }}>{errors.fullName?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="phoneNumber">Phone Number</label>
                                <PhoneInput
                                    initialValueFormat="national"
                                    className="form-control"
                                    disabled={addressChange ? true : false}
                                    value={addressChange ? shippingAddress.phoneNumber : paymentAddresss != null ? paymentAddresss.phoneNumber : null}
                                    countries={["CA", "US"]}
                                    onChange={(value) => {
                                        setValue('phoneNumber', value);
                                        clearErrors("phoneNumber")
                                    }}
                                />
                                <p style={{ "color": "red" }}>{errors.phoneNumber?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="country">Country</label>
                                <select class="form-control"  {...register("country")} onChange={handleChange} defaultValue={addressChange ? shippingAddress.countryId : paymentAddresss != null ? paymentAddresss.countryId : null} disabled={addressChange ? true : false} error={errors.country ? true : false}>
                                    <option > </option>
                                    {data.countryList && data.countryList.map((option) => (
                                        <option
                                            key={option.countryId}
                                            value={option.countryId}
                                        >
                                            {option.countryName}
                                        </option>
                                    ))}
                                </select>
                                <p style={{ "color": "red" }}>{errors.country?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="addressLine1">Address Line 1</label>
                                <input type="text"
                                    class="form-control"
                                    id="addressLine1"
                                    disabled={addressChange ? true : false}
                                    {...register("addressLine1")}

                                    defaultValue={addressChange ? shippingAddress.addressLine1 : paymentAddresss != null ? paymentAddresss.addressLine1 : null}
                                    error={errors.addressLine1 ? true : false} />
                                <p style={{ "color": "red" }}>{errors.addressLine1?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="addressLine2">Address Line 2 (Optional)</label>
                                <input type="text"
                                    class="form-control"
                                    id="addressLine2"
                                    disabled={addressChange ? true : false}
                                    {...register("addressLine2")}
                                    defaultValue={addressChange ? shippingAddress.addressLine2 : paymentAddresss != null ? paymentAddresss.addressLine2 : null} />
                            </div>
                            <div class="form-group col-lg">
                                <label for="cityName">City</label>
                                <input type="text"
                                    class="form-control"
                                    id="cityName"
                                    disabled={addressChange ? true : false}
                                    {...register("cityName")}
                                    defaultValue={addressChange ? shippingAddress.cityName : paymentAddresss != null ? paymentAddresss.cityName : null}
                                    error={errors.cityName ? true : false} />
                                <p style={{ "color": "red" }}>{errors.cityName?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="state">State / Province</label>
                                {
                                    !stateList ? <input type="text"
                                        class="form-control"
                                        disabled="true"
                                    /> :
                                        <select class="form-control" id="state"{...register("state")} defaultValue={addressChange ? shippingAddress.stateId : paymentAddresss != null ? paymentAddresss.stateId : null} disabled={addressChange ? true : false} error={errors.state ? true : false}>
                                            {stateList && stateList.map((option) => (
                                                <option
                                                    key={option.stateId}
                                                    value={option.stateId}
                                                >
                                                    {option.stateName}
                                                </option>
                                            ))}
                                        </select>
                                }
                                <p style={{ "color": "red" }}>{errors.state?.message}</p>
                            </div>
                            <div class="form-group col-lg">
                                <label for="zipcode">Zip Code</label>
                                <input type="text"
                                    class="form-control"
                                    id="zipcode"
                                    disabled={addressChange ? true : false}
                                    {...register("zipcode")}
                                    defaultValue={addressChange ? shippingAddress.zipcode : paymentAddresss != null ? paymentAddresss.zipcode : null}
                                    error={errors.zipcode ? true : false} />
                                <p style={{ "color": "red" }}>{errors.zipcode?.message}</p>
                            </div>
                        </div>
                        <div className="footer">
                            <ColoredLine color={'secondaryColor'} />
                            <div className="row " style={{ justifyContent: 'center', alignItems: "center" }}>
                            <ArrowButton status={STEPS_ACCEPTED_STATUS} navigator="backward" buttonHandler={backButtonHandler}/>
                                <div class="col-6 ">
                                    <button type="button" class="btn btn-outline-secondary acceptButton" style={{ borderWidth: "2px", "borderColor": "#2F008D", color: "#2F008D", }} onClick={handleSubmit(onSubmit)}>
                                        {loaderComponent == true ? "Save & Accept" : loaderMethod()}
                                    </button>
                                </div>
                                <ArrowButton status={STEPS_ACCEPTED_STATUS} navigator="forward" buttonHandler={frontButtonHandler}/>
                            </div>
                            <CProgressBar data={data} />
                            <PoweredBy />
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}