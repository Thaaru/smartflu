package io.smartfluence.mysql.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.UpdateTimestamp;

import io.smartfluence.constants.CampaignStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "brand_campaign_details", indexes = { @Index(columnList = "brand_id", unique = false),
		@Index(columnList = "campaign_name", unique = false), @Index(columnList = "campaign_status", unique = false) })
@NamedEntityGraph(name = "BrandCampaignDetailsMysql.eagerAll", attributeNodes = @NamedAttributeNode("campaignInfluencerMappings"))
public class BrandCampaignDetailsMysql implements Serializable {

	private static final long serialVersionUID = -5186024765585348307L;

	@Id
	@Column(name = "campaign_id")
	private String campaignId;

	@Column(name = "brand_id")
	private String brandId;

	@Column(name = "brand_description")
	private String brandDescription;

	@Column(name = "campaign_name")
	private String campaignName;

	@Column(name = "campaign_description")
	private String campaignDescription;

	@Enumerated(EnumType.STRING)
	@Column(name = "campaign_status")
	private CampaignStatus campaignStatus;

	@Column(name = "influencer_count")
	private Integer influencerCount;

	@Column(name = "post_type")
	private String postType;

	@Column(name = "payment")
	private Double payment;

	@Column(name = "product")
	private String product;

	@Column(name = "product_value")
	private Double productValue;

	@Column(name = "requirements")
	private String requirements;

	@Column(name = "restrictions")
	private String restrictions;

	@Column(name = "next_trigger")
	private Date nextTrigger;

	@Column(name = "active_until")
	private Date activeUntil;

	@Column(name = "custom_list_id")
	private String customListId;

	@Default
	@Column(name = "is_promote")
	private Boolean isPromote = false;

	@Column(name = "created_at")
	private Date createdAt;

	@UpdateTimestamp
	@Column(name = "modified_at")
	private Date modifiedAt;

	@Default
	@EqualsAndHashCode.Exclude
	@OneToMany(mappedBy = "brandCampaignDetails", fetch = FetchType.EAGER)
	private Set<CampaignInfluencerMapping> campaignInfluencerMappings = new HashSet<>();

	@Transient
	public void reduceInfluencer() {
		if (influencerCount > 0) {
			--influencerCount;
		}
	}
}