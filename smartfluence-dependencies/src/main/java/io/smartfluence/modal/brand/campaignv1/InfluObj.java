package io.smartfluence.modal.brand.campaignv1;

import lombok.Data;
import java.util.List;

@Data
public class InfluObj {

    private String campaignId;

    private String influencerId;

    private String influencerHandle;

    private String platform;

    private String proposalStatus;

    private String campaignName;

    private String engagementRate;
    private String followers;

    private List<InfluOfferObj> offers;
    private List<InfluAddressObj> address;
    private List<InfluProductsObj> products;
    private List<InfluTasksObj> tasks;
    private List<InfluPlatformMaster> platformMasters;
    private List<InfluPostTypeMaster> postTypeMasters;
    private String trackingId;
    private String postUrl;
    private String paymentMethod;
    private String influencerEmail;
    private String paymentEmailAddress;
    private String campaignStatus;

}
