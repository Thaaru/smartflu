package io.smartfluence.modal.social.report;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudienceLookalikes implements Serializable {

	private static final long serialVersionUID = 7295230847390965209L;

	@JsonAlias("user_id")
	private String userId;

	@JsonAlias("username")
	private String handle;

	@JsonAlias("picture")
	private String profileImage;

	@JsonAlias("followers")
	private Double followers;

	@JsonAlias("fullname")
	private String fullName;

	@JsonAlias("url")
	private String url;

	@JsonAlias("is_verified")
	private Boolean isVerified;

	@JsonAlias("engagements")
	private Double engagement;
}