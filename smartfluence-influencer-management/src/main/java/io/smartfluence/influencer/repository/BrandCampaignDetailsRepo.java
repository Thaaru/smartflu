package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.influencer.entities.BrandCampaignDetails;

@Repository
public interface BrandCampaignDetailsRepo extends JpaRepository<BrandCampaignDetails,String>{
    BrandCampaignDetails findByCampaignId(String campaignId);
}
