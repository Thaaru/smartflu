package io.smartfluence.modal.validation.brand.explore;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.smartfluence.modal.validation.custom.Conditional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@Conditional
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class GetInfluencersRequestModel {

	private String audienceBrandName;

	private String audienceGeoName;

	private String audienceGender;

	private List<String> audienceAge;

	private String comparisonProfile;

	private String influencerBrandName;

	private String influencerGeoName;

	private String influencerGender;

	private String influencerAge;

	private Integer followersMin;

	private Integer followersMax;

	private Integer engagementMin;

	private Integer engagementMax;

	private String keyword;

	private String profileBio;

	private String hashTag;

	private String withContact;

	private String customListId;

	private String customListAction;

	private Boolean verified;

	private Boolean sponsoredPost;

	@NotBlank(message = "Please select a sortBy")
	private String sortBy;

	@NotBlank(message = "Please select a platform")
	private String platform;

	@NotNull(message = "Please select a limit")
	private Integer limitCount;

	@NotNull(message = "Please select a skipCount")
	private Integer skipCount;

	private Boolean isNewSearch;
}