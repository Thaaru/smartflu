package io.smartfluence.modal.brand.campaignv1;

public enum CampaignStatusEntityV1 {
    INPROGRESS,ACTIVE,INACTIVE,CLOSED
}
