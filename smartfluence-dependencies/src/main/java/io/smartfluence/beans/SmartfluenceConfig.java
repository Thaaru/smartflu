package io.smartfluence.beans;

import java.net.URI;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "smartfluence")
public class SmartfluenceConfig {

	private boolean useSsl = false;

	private URI inviteUrl;

	private String dbUpdatedCompletedMail;
}