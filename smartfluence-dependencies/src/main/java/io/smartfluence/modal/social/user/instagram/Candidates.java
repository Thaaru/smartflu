package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Candidates implements Serializable {

	private static final long serialVersionUID = 3996569865558851347L;

	@JsonAlias("width")
	private Integer width;

	@JsonAlias("height")
	private Integer height;

	@JsonAlias("url")
	private String imageUrl;
}