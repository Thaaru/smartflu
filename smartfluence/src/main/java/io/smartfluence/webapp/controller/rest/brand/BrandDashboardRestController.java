package io.smartfluence.webapp.controller.rest.brand;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.smartfluence.modal.validation.brand.dashboard.DashBoardModal;
import io.smartfluence.modal.validation.brand.dashboard.GetInfluencerBidCountModal;
import io.smartfluence.modal.validation.brand.dashboard.MailTemplateModal;
import io.smartfluence.modal.validation.brand.dashboard.RemoveInfluencerFromCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SaveCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SavePostLink;
import io.smartfluence.modal.validation.brand.dashboard.SaveStatusModel;
import io.smartfluence.modal.validation.brand.dashboard.UploadInfluencersRequestModel;
import io.smartfluence.webapp.proxy.BrandManagementServiceProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandDashboardRestController {

	@Autowired
	private BrandManagementServiceProxy brandManagementServiceProxy;

	@PostMapping("save-campaign")
	public ResponseEntity<Object> saveCampaign(@Validated @ModelAttribute SaveCampaignModel saveCampaignModel, BindingResult bindingResult) {
		log.info(saveCampaignModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.saveCampaign(saveCampaignModel.getCampaignName().trim());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-campaign/{campaignId}/save-status")
	public ResponseEntity<Object> saveStatus(@Validated @ModelAttribute SaveStatusModel saveStatusModel,
			BindingResult bindingResult, @PathVariable UUID campaignId) {
		log.info(saveStatusModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.saveStatus(saveStatusModel.getStatus(), saveStatusModel.getBidId(),
					saveStatusModel.getInfluencerId(), campaignId);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-campaign/{campaignId}/save-post-link")
	public ResponseEntity<Object> savePostLink(@Validated @ModelAttribute SavePostLink savePostLink, BindingResult bindingResult,
			@PathVariable UUID campaignId) {
		log.info(savePostLink);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.savePostLink(savePostLink.getInfluencerId(), savePostLink.getPaymentType(), savePostLink.getPaymentMode(),
					savePostLink.getPostType(), savePostLink.getPostDuration(), savePostLink.getBidId(), savePostLink.getBidAmount(), savePostLink.getPostURL(),
					savePostLink.getPostName(), savePostLink.getIsNew(), campaignId, savePostLink.getPlatform());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-campaign/{campaignId}/remove-influencer")
	public ResponseEntity<Object> removeInfluencer(@Validated @ModelAttribute RemoveInfluencerFromCampaignModel removeInfluencerFromCampaignModel,
			BindingResult bindingResult, @PathVariable UUID campaignId) {
		log.info(removeInfluencerFromCampaignModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.removeInfluencer(removeInfluencerFromCampaignModel.getInfluencerId(), campaignId);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("get-campaign")
	public ResponseEntity<Object> getCampaign() {
		return brandManagementServiceProxy.getCampaign(false);
	}

	@GetMapping(path = "view-campaign/{campaignId}/export-campaign-csv")
	public ResponseEntity<String> exportCampaignCsv(@PathVariable UUID campaignId) {
		return brandManagementServiceProxy.exportCampaignCsv(campaignId);
	}

	@GetMapping(path = "view-campaign/{campaignId}/download-campaign-csv/{csvName}")
	public ResponseEntity<byte[]> downloadCampaignCsv(@PathVariable UUID campaignId, @PathVariable UUID csvName) {
		return brandManagementServiceProxy.downloadCampaignCsv(campaignId, csvName);
	}

	@GetMapping("view-campaign/{campaignId}/get-influencer-bid-count")
	public ResponseEntity<Object> getInfluencerBidCount(@Validated @ModelAttribute GetInfluencerBidCountModal campaignBidCountModal,
			BindingResult bindingResult, @PathVariable UUID campaignId) {
		log.info(campaignBidCountModal);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.getInfluencerBidCount(campaignBidCountModal.getInfluencerId(), campaignBidCountModal.getBidId(), campaignId);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-campaign/save-mail-template")
	public ResponseEntity<Object> saveMailTemplate(@Validated @ModelAttribute MailTemplateModal mailTemplateModal, BindingResult bindingResult) {
		log.info(mailTemplateModal);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.saveMailTemplate(mailTemplateModal.getTemplateId(), mailTemplateModal.getTemplateBody(),
					mailTemplateModal.getTemplateName(), mailTemplateModal.getTemplateSubject(), mailTemplateModal.getIsNew());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-campaign/send-bulk-mail")
	public ResponseEntity<Object> sendBulkMail(@Validated @ModelAttribute DashBoardModal dashBoardModal, BindingResult bindingResult) {
		log.info(dashBoardModal);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.sendBulkMail(dashBoardModal.getCampaignId(), dashBoardModal.getInfluencerIds(),
					dashBoardModal.getTemplateId());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-campaign/send-bulk-mail-new")
	public ResponseEntity<Object> sendBulkMailNew(@Validated @ModelAttribute DashBoardModal dashBoardModal, BindingResult bindingResult) {
		log.info(dashBoardModal);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.sendBulkMailNew(dashBoardModal.getCampaignId(), dashBoardModal.getInfluencerIds(),
					dashBoardModal.getTemplateId());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-campaign/{campaignId}/upload-bulk-influencers")
	public ResponseEntity<Object> uploadInfluencers(@Validated @ModelAttribute UploadInfluencersRequestModel uploadInfluencersModel,
			BindingResult bindingResult, @PathVariable UUID campaignId) {
		log.info(uploadInfluencersModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.uploadInfluencers(uploadInfluencersModel.getInfluencerHandles(),
					uploadInfluencersModel.getWorkbookSheets(), campaignId);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("view-campaign/download-template/upload-influencers")
	public ResponseEntity<byte[]> downloadTemplate() {
		return brandManagementServiceProxy.downloadTemplate();
	}

	@PostMapping("view-campaign/{campaignId}/delete-campaign")
	public ResponseEntity<Object> deleteCampaign(@PathVariable UUID campaignId) {
		return brandManagementServiceProxy.deleteCampaign(campaignId);
	}

	@PostMapping("view-campaign/copy-influencers")
	public ResponseEntity<Object> copyInfluencers(@Validated @ModelAttribute DashBoardModal dashBoardModal, BindingResult bindingResult) {
		log.info("Request for copying influencers = {}", dashBoardModal);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.copyInfluencers(dashBoardModal.getCampaignId(), dashBoardModal.getInfluencerIds());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("view-campaign/edit-campaign-name")
	public ResponseEntity<Object> editCampaignName(@Validated @ModelAttribute DashBoardModal dashBoardModal, BindingResult bindingResult) {
		log.info("Request for changing campaignName = {}", dashBoardModal);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.editCampaignName(dashBoardModal.getCampaignId(), dashBoardModal.getCampaignName().trim());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

//	@DeleteMapping("view-campaign/delete-campaign")
//	public ResponseEntity<Object> deleteCampaign(@PathVariable String campaignId){
//		brandManagementServiceProxy.deleteCampaign()
//	}
}