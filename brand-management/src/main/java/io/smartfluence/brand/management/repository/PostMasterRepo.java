package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.PostMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostMasterRepo extends JpaRepository<PostMaster, Long> {
}
