var rowsSelected = [];
var campaignWFMPDT;
// A $( document ).ready() block.
$(document).ready(function () {
    $.fn.dataTable.ext.errMode = 'none';
    campaignId = window.location.href.split("/")[5];
    var newCampaignId = campaignId.split('?')
    init_makePaymentsTable(newCampaignId[0]);

    $(".makePaymentAction").click(function () {
        alert("Handler for .change() called.");
    });
if(campaignStatusFromCampaignWF == "ENDED"){
    campaignWFMPDT.on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
        //if ( originalEvent.target.nodeName.toLowerCase() === 'img' ) {
            e.preventDefault();
        //}
    } );
}
});

var influencersList_mp = [];
var currencyMaster = [];
var affiliateMaster = [];
var platformMasters = [];
var postTypeMasters = [];

//table_campaign_makepayments
function init_makePaymentsTable(campaignId){
    // console.log("campaign iD of makepayment : ",campaignId);
     campaignWFMPDT = $('#table_campaign_makepayments').DataTable({
        dom: "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-7 pl-0'><'col-sm-12 col-md-3'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
            ajax: {
                url: `/brand/view-campaign-v1/view-influencers?proposalStatus=NOT_PAID&id=${campaignId}`,
                dataSrc: function (response) {
                    // console.log("makepaymentList Data RESPONSE: ", response.data)
                    if (response?.code == 600) {
                        // console.log("influencerlist : ", response.data)
                        currencyMaster = response.data.currencyMaster;
                        affiliateMaster = response.data.affiliateMaster;
                        influencersList_mp = response.data.influencersList;
                        if (influencersList_mp?.length > 0) {
                            platformMasters = influencersList_mp[0].platformMasters;
                            postTypeMasters = influencersList_mp[0].postTypeMasters;
                            return influencersList_mp;
                        }
                        return [];
                    }
                    return [];
                }
            },
            autoWidth: false,
            columnDefs: [
                {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        //meta.settings.aoData[0]._aData.influencerId
                        // console.log("checkbox render", data)
                        return '<input disabled type="checkbox" class="inviteCheckbox">';
                    }
                },
                {
                    targets: 1,
                    width: "15%",
                    className: "text-left",
                    data: "influencerHandle",
                    render:function (data, type, row, meta) {
                       return `<a style=" color: #2f008d" href="/brand/view-influencer/${row.influencerId}?isValid=true&platform=${row.platform}"
                       target="_blank">${data}</a>`
                    }
                },
                {
                    targets: 2,
                    width: "18%",
                    data: "offers",
                    className: "text-left",
                    render: function (data, type, row, meta) {
                        var payment = data.find(x => x.offerType === 'PAYMENT');
                        var commission = data.find(x => x.offerType === 'COMMISSION');
                        var product = data.find(x => x.offerType === 'PRODUCT');
                        // payment = { paymentOffer: 10.05 }
                        // commission = { commissionValue: 13.05 }
                        var result = '';
                        if (payment && typeof payment?.paymentOffer === "number") {
                            result += `<span>$ ${payment?.paymentOffer} Cash</span><br>`;
                        }
                        if (product) {
                            result += `<span>Product Only</span>`;
                        }
                        if (commission && typeof commission?.commissionValue === "number") {
                            var affilateTypeValue = affiliateMaster.find(x => x.caId === commission?.commissionAffiliateTypeId);
                            var commissionTypeValue = currencyMaster.find(x => x.currencyId === commission?.commissionValueTypeId);
                            // console.log("PERCENTAGE/CLICK CHECK : ", affilateTypeValue);
                            if (affilateTypeValue.caValue == "Custom") {
                                result += `<span>${commission?.commissionValue}${commissionTypeValue.currencyValue} Per ${commission?.commissionAffiliateCustomName}</span><br>`;
                            } else {
                                result += `<span>${commission?.commissionValue}${commissionTypeValue.currencyValue} Per ${affilateTypeValue.caValue}</span><br>`;
                            }
                        }
                       
                        return result;
                    }
                },
                {
                    targets: 3,
                    width: "20%",
                    className: "text-left",
                    data:"paymentEmailAddress"
                },
                {
                    targets: 4,
                    data: "paymentMethod",
                    className: "text-left",
                    width: "21%",
                },
                {
                    targets: 5,
                    data: "proposalStatus",
                    className: "text-left",
                    width: "18%",
                    render: function (data, type, row, meta) {
                       if(data == "NOT_PAID"){
                        return `<a style="color:#2f008d;">Payment Pending</a>`
                       }
                    }
                },
               
                {
                    targets: 6,
                    className: "text-center",
                    render: function (data, type, row, meta) {
                       
                        var influencerId = row.influencerId; 
                        var campaignId = row.campaignId;
                        var proposalStatus = row.proposalStatus;
                        if (campaignStatusFromCampaignWF == "ENDED") {
                        return `
                        <div class="dropdown">
                        <span class="dot-icon disabled"><i class="fa fa-ellipsis-v"></i></span>
                    </div>
     `;
                        }else{
                            return `
         <div class="dropdown makePaymentAction" id=${meta.settings.aoData[0]._aData.influencerId}>
             <span class="dot-icon"><i class="fa fa-ellipsis-v"></i></span>
             <ul class="dropdown-content">
                 <li id="accept" onclick="makePaymentApproval('accept','${campaignId}','${influencerId}')">Mark As Paid</li>
             </ul>
         </div>
     `;
                        }
                    }
                },
                {
                    className: 'control text-center',
                    targets: [0, 6],
                    orderable: false
                }
            ],
        select: {
            style: 'multi'
        },
        order: [
            [1, 'asc']
        ],
        'rowCallback': function(row, data, dataIndex) {
            // Get row ID
            var rowId = data.DT_RowId;
            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rowsSelected) !== -1) {
                $(row).find('input[type="checkbox"]').prop(
                    'checked', true);
                $(row).addClass('selected');
            }
        }
    });

    
    $('#table_campaign_makepayments tbody').on('click', 'input[type="checkbox"]', function(e) {
        var $row = $(this).closest('tr');
        // Get row data
        var data = campaignWFMPDT.row($row).data();
        // Get row ID
        var rowId = data.DT_RowId;
        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rowsSelected);
        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rowsSelected.push(rowId);
            // Otherwise, if checkbox is not checked and row ID is in list of
            // selected
            // row IDs
        } else if (!this.checked && index !== -1) {
            rowsSelected.splice(index, 1);
        }
        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(campaignWFMPDT);
        // Prevent click event from propagating to parent
        e.stopPropagation();
    });
}



//accept/reject
function makePaymentApproval(status, campaignid, influencerId) {

    var requestMakePaymentData = {
        campaignId: campaignid,
        influencerId: influencerId,
        status: status,
        workflowStatus: "MAKE_PAYMENTS"
    }
    
     $.ajax({
        url: "/brand/view-campaign-v1/view-influencers/brandapproval",
        type: "PUT",
        data: JSON.stringify(requestMakePaymentData),
        dataType: "json",
        contentType: "application/json",
    }).done(function (data) {
        // data = JSON.parse(data);
        if (data.code == "600" ) {
            campaignWFFPDT.ajax.reload( null, false );
            campaignWFMPDT.ajax.reload( null, false );
            var someTabTriggerEl = document.querySelector('#fulfillProducts-tab');
            var tab = new bootstrap.Tab(someTabTriggerEl);
            tab.show();
            toast("Marked as Paid")
        }else{
         toast(data.errors.message, "error");
        }

        //window.location.href = "/brand/view-campaign-v1/"+msg.campaignId;
        // $("#campaignForm").trigger("reset");
    }).fail(function (jqXHR, textStatus) {
        toast("Request Failed " + textStatus, "error")
    });
}