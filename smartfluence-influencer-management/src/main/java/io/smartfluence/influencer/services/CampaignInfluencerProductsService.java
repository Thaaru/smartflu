package io.smartfluence.influencer.services;


import io.smartfluence.influencer.entities.CampaignInfluencerProductId;
import io.smartfluence.influencer.entities.CampaignInfluencerProducts;
import io.smartfluence.influencer.entities.CampaignProducts;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.repository.CampaignInfluencerProductsRepo;
import io.smartfluence.influencer.repository.CampaignProductsRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class CampaignInfluencerProductsService {
    @Autowired
    CampaignProductsRepo campaignProductsRepo;
    @Autowired
    CampaignInfluencerProductsRepo campaignInfluencerProductsRepo;


    public FullCampaignInfluencerDetails getInfluencerProducts(String campaignId, String influencerId, FullCampaignInfluencerDetails fullCampaignInfluencerDetails){
        Logger.getLogger("getInfluencerProducts").log(Level.FINE,"getting the InfluencerProducts");
        List<CampaignProducts> products = campaignProductsRepo.getByCampaignId(campaignId);
                    if (!products.isEmpty()) {
                        fullCampaignInfluencerDetails.setProducts(products);
                        List<CampaignInfluencerProductId> tempInfluencerProductsList = new LinkedList<>();
                        List<CampaignInfluencerProducts> campaignInfluencerProductsList = campaignInfluencerProductsRepo.findByCipIdCampaignIdAndCipIdInfluencerId(campaignId,influencerId);
                        campaignInfluencerProductsList.forEach(campaignInfluencerProducts ->
                            tempInfluencerProductsList.add(campaignInfluencerProducts.getCipId())
                        );
                        fullCampaignInfluencerDetails.setProductsAcceptedList(tempInfluencerProductsList);
    }
        return fullCampaignInfluencerDetails;
}


    public void updateCampaignInfluencerProductService(FullCampaignInfluencerDetails fullCampaignInfluencerDetails) {
        Logger.getLogger("updateCampaignInfluencerProductService").log(Level.FINE,"getting the updateCampaignInfluencerProduct");
        if(fullCampaignInfluencerDetails.getProductsAcceptedList() != null){
            List<CampaignInfluencerProducts> campaignInfluencerProductsList = campaignInfluencerProductsRepo
                    .findByCipIdCampaignIdAndCipIdInfluencerId(fullCampaignInfluencerDetails.getCampaignId(), fullCampaignInfluencerDetails.getInfluencerId());
            campaignInfluencerProductsRepo.deleteAll(campaignInfluencerProductsList);
            fullCampaignInfluencerDetails.getProductsAcceptedList().stream().forEach(infProducts1 ->
                campaignInfluencerProductsRepo.save(new CampaignInfluencerProducts(infProducts1))
            );
        }
    }



}
