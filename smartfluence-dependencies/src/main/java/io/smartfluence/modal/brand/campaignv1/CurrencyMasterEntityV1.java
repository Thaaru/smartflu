package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyMasterEntityV1 {
    private int currencyId;
    private  String  currencyValue;
}
