package io.smartfluence.modal.validation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerLookalikeModal {

	@NotBlank(message = "Please select a comparisonProfile")
	private String comparisonProfile;

	@NotBlank(message = "Please select a industry")
	private String industry;

	@NotNull(message = "Please select a Min followers")
	private Integer followersMin;

	@NotNull(message = "Please select a Max followers")
	private Integer followersMax;

	@Email(message = "Enter valid business email")
	@NotBlank(message = "Enter valid business email")
	private String email;

	@NotBlank(message = "Please select a platform")
	private String platform;

	@NotBlank(message = "Please Enter Mandatory field(s)")
	private String grecaptcha;
}