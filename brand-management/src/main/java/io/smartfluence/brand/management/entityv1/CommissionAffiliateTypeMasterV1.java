package io.smartfluence.brand.management.entityv1;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "commission_affiliate_type_master")
public class CommissionAffiliateTypeMasterV1 implements Serializable {
    @Id
    @Column(name = "comm_affi_type_id")
    private Integer commAffiTypeId;

    @Column(name = "comm_affi_name")
    private String commAffiName;

}
