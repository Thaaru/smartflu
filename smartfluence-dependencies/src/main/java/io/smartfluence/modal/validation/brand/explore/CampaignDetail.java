package io.smartfluence.modal.validation.brand.explore;

import java.util.UUID;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CampaignDetail {

	@NotBlank(message = "Enter campaign name")
	private String campaignName;

	private UUID campaignId;

	@NotBlank(message = "Please provide influencerId")
	private String influencerId;

	private String reportId;

	@NotBlank(message = "Please provide influencerHandle")
	private String influencerHandle;

	private String platform;

	private boolean reportPresent;

	private boolean isValid;

	public boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public boolean getReportPresent() {
		return reportPresent;
	}

	public void setReportPresent(boolean reportPresent) {
		this.reportPresent = reportPresent;
	}
}