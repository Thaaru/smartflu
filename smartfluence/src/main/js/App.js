import React from 'react';
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import './index.css';
import './App.css';
import { createRoot } from "react-dom/client";
import MainPage from './pages/main_page';
import { ToastContainer } from 'react-toastify';

const container = document.getElementById("app");
const root = createRoot(container);
root.render(
  <div className='App' >
    <MainPage/>

    <ToastContainer />
  </div>
);