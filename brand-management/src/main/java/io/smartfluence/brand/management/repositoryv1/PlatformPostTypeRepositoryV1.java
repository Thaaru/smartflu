package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.PlatformPostTypeMasterV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlatformPostTypeRepositoryV1 extends JpaRepository<PlatformPostTypeMasterV1,Long> {
}
