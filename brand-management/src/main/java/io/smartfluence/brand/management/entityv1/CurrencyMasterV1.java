package io.smartfluence.brand.management.entityv1;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "currency_master")
public class CurrencyMasterV1 implements Serializable {
    @Id
    @Column(name = "currency_Id")
    private Integer currencyId;

    @Column(name = "currency")
    private String currency;
}
