package io.smartfluence.webapp.controller.rest.entity;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UploadInfluencersRequestModelNew {

	@NotBlank(message = "Please Provide InfluencerHandles")
	private String influencerHandles;

	private UUID campaignId;

	private List<String> workbookSheets = new ArrayList<>();
}