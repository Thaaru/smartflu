package io.smartfluence.webapp.controller.rest.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_influencer_offers_v1")


public class CampaignInfOfferV1Entity {

    @Id
    @Column(name = "campaign_id")
    private String campaignId;
    @Column(name = "influencer_id")
    private String influencerId;
    @Column(name = "offer_type")
    private String offerType;
    @Column(name = "payment_offer")
    private Double paymentOffer;
    @Column(name = "commission_value")
    private Double commissionValue;
    @Column(name = "commission_value_type")
    private String commissionValueType;
    @Column(name = "commission_affiliate_type")
    private String commissionAffiliateType;
    @Column(name = "commission_affiliate_custom_name")
    private String commissionAffiliateCustomName;
    @Column(name = "commission_affiliate_details")
    private String commissionAffiliateDetails;

}
