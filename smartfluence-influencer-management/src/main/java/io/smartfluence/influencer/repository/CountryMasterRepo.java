package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.smartfluence.influencer.entities.CountryMaster;
import org.springframework.stereotype.Repository;


@Repository
public interface CountryMasterRepo extends JpaRepository<CountryMaster,Long>{
CountryMaster findCountryByCountryId(Long countryId);
}
