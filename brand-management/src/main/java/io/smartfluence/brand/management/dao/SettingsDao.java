package io.smartfluence.brand.management.dao;

import java.util.Optional;
import java.util.UUID;

import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandPaymentDetails;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.mysql.entities.BrandEmailConnection;
import io.smartfluence.mysql.entities.UserAccounts;

public interface SettingsDao {

	void saveBrandDetails(BrandDetails brandDetails);

	Optional<BrandDetails> getBrandDetailsById(UserDetailsKey key);

	void saveBrandPaymentDetails(BrandPaymentDetails brandPaymentDetails);

	Optional<BrandPaymentDetails> getBrandPaymentDetailsById(UUID userId);

	void saveUserAccounts(UserAccounts userAccounts);

	Optional<UserAccounts> getUserAccountsById(String userId);

	Optional<UserAccounts> getUserAccountsByEmailAndUserIdNotEqual(String email, String userId);

	default Optional<BrandEmailConnection> getEmailConnection(String brandId) {
		return null;
	}

	default void saveEmailConnection(BrandEmailConnection brandEmailConnection) {

	}

	default void removeEmailConnection(String brandId) {

	}
}