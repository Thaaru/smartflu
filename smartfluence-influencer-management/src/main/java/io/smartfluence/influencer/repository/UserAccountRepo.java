package io.smartfluence.influencer.repository;

import io.smartfluence.influencer.entities.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepo extends JpaRepository<UserDetails,String> {
    UserDetails findByUserId(String userId);
    UserDetails findByEmail(String email);
}
