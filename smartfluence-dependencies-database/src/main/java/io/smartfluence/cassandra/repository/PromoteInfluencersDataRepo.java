package io.smartfluence.cassandra.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.PromoteInfluencersData;
import io.smartfluence.cassandra.entities.primary.PromoteInfluencersDataKey;

@Repository
public interface PromoteInfluencersDataRepo extends CassandraRepository<PromoteInfluencersData, PromoteInfluencersDataKey> {

	List<PromoteInfluencersData> findAllByKeyCampaignId(UUID campaignId);

	Optional<PromoteInfluencersData> findByKeyCampaignIdAndKeyInfluencerDeepSocialId(UUID campaignId, String influencerDeepSocialId);
}