package io.smartfluence.util.image;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.LookupOp;
import java.awt.image.ShortLookupTable;

class InvertFilter implements ImageFilter {

	@Override
	public BufferedImage processImageBuffer(BufferedImage bufferedImage) {
		short[] invertArray = new short[256];

		for (int counter = 0; counter < 256; counter++)
			invertArray[counter] = (short) (255 - counter);

		BufferedImageOp invertFilter = new LookupOp(new ShortLookupTable(0, invertArray), null);
		return invertFilter.filter(bufferedImage, null);
	}
}