package io.smartfluence.modal.brand.dashboard;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CampaignDetails {

	private String campaignName;

	private Integer influencerCount;
}