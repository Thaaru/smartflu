package io.smartfluence.mysql.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import io.smartfluence.mysql.entities.StripeSubscriptionMaster;

public interface StripeSubscriptionMasterRepo extends CrudRepository<StripeSubscriptionMaster, Integer> {

	Optional<StripeSubscriptionMaster> findByCustomerId(String customer);
}