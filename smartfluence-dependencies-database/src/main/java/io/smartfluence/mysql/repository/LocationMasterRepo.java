package io.smartfluence.mysql.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.mysql.entities.LocationMaster;

@Repository
public interface LocationMasterRepo extends CrudRepository<LocationMaster, Long> {

	Optional<LocationMaster> findByLocation(String location);

	List<LocationMaster> findAllByStatus(int status);

	List<LocationMaster> findAllByLocationIgnoreCaseContainingAndStatus(String name, int status);
}