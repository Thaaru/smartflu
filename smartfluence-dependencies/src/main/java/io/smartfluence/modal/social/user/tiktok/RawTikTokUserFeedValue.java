package io.smartfluence.modal.social.user.tiktok;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokUserFeedValue implements Serializable {

	private static final long serialVersionUID = 8954263006617521812L;

	@JsonAlias("items")
	private List<RawTikTokUserFeedItems> items;
}