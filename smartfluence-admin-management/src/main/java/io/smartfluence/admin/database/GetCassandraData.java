package io.smartfluence.admin.database;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ResultSet;

@Component
@Configuration
public class GetCassandraData {

	@Value("${spring.data.cassandra.username}")
	private String userName;

	@Value("${spring.data.cassandra.password}")
	private String password;

	@Value("${spring.data.cassandra.keyspace-name}")
	private String keySpace;

	@Value("${spring.data.cassandra.contact-points}")
	private String contactPoints;

	public ResultSet getData(CassandraDatabaseConnector db, String queryString) {
		try {
			db.connectDataBase(userName, password, keySpace, contactPoints, 9042);

			BoundStatement bs = db.getSession().prepare(queryString).bind();
			return db.getSession().execute(bs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void closeConnection(CassandraDatabaseConnector db) {
		db.close();
	}
}