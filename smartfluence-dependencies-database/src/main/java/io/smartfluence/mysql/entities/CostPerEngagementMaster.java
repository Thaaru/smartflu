package io.smartfluence.mysql.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cost_per_engagement_master")
public class CostPerEngagementMaster implements Serializable {

	private static final long serialVersionUID = 1434730516628694887L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "start_range")
	private Double startRange;

	@Column(name = "end_range")
	private Double endRange;

	@Column(name = "cost_per_engagement")
	private Double costPerEngagement;

	@Column(name = "created_at")
	private Date createdAt;
}