package io.smartfluence.brand.management.service;

import java.util.List;
import java.util.UUID;

import io.smartfluence.modal.validation.brand.explore.*;
import org.springframework.http.ResponseEntity;

import io.smartfluence.modal.brand.exploreinfluencer.GetInfluencersResponseModel;

public interface ExploreInfluencerService {

	ResponseEntity<Object> getInfluencerDemographicDetails(String influencerId, boolean isValid, String platform, UUID searchId);

	ResponseEntity<Object> getInfluencers(GetInfluencersRequestModel getInfluencersRequestModel);

	ResponseEntity<byte[]> viewInfluencerProfilePic(String userId);

	ResponseEntity<Object> viewInfluencerProfileName(UUID userId);

	ResponseEntity<Object> submitBid(SubmitBidModel submitBidModel);

	ResponseEntity<Object> sendProposal(SubmitBidModel submitBidModel);

	ResponseEntity<byte[]> viewInfluencerContentImage(String userId, int contentId, String platform);

	ResponseEntity<Object> addToCampaign(CampaignDetail addToCampaign);

//	ResponseEntity<Object> addInfluencerToNewCampaign(List<CampaignInfluencerReqEntity> campaignInfluencerReqEntity);

	ResponseEntity<Object> getCampaignByInfluencer();

	ResponseEntity<Object> checkInfluencer(CampaignDetail campaignDetail, String influencerHandle);

	ResponseEntity<Object> searchInfluencer(SearchInfluencerRequestModel searchInfluencerRequestModel);

	ResponseEntity<Object> getAutoSuggestions(String influencerHandle, String platform);

	ResponseEntity<Object> getUtilities(String name, String data);

	ResponseEntity<Object> getMailTemplates();

	void processSearchInfluencersRequest(GetInfluencersRequestModel request, List<GetInfluencersResponseModel> response, boolean savePicture);

	ResponseEntity<Object> viewInfluencerInstagramStories(String userId, Integer type);

	ResponseEntity<Object> getInfluencerChartData(String userName, String platform);
}