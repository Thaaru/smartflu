var toast;
var hideToast;
var hideToastOnClose;

function toastSuccess(message){
	toast(message,'success');
}
function toastError(message){
	toast(message,'error');
}

function toastInfo(message){
	toast(message,'info');
}

function toastWarning(message){
	toast(message,'warning');
}


var toastElements = document.getElementsByClassName('toast-container');
var toastElement;
var toastMessage;
var toastClose;


var timeoutThread;
toast = function(message, level) {
	
	hideToast();
	removeColor();
	toastElement.classList.add('active');
	toastElement.classList.add('toast-message-' + level);
	toastMessage.innerText = message.trim();
	var noOfWords = toastMessage.innerText.split(' ').length;
	var timeout = ((noOfWords * 250) + 1500);
	console.log(timeout / 1000);
	clearTimeout(timeoutThread);
	timeoutThread = setTimeout(hideToast, timeout);
}

hideToast = function() {
	toastElement.classList.remove('active');

}

hideToastOnClose=function(){
	hideToast();
	toastElement.classList.add('toast-force-close');
}
if (toastElements.length == 0) {
	// toast container
	toastElement = document.createElement('div');
	toastElement.classList.add('toast-container');

	// toast Message
	toastMessage = document.createElement('span');
	toastMessage.classList.add('toast-message-container');
	toastMessage.innerText = 'Toast Message';
	toastElement.appendChild(toastMessage);

	// toast close
	toastClose = document.createElement('span');
	toastClose.classList.add('toast-message-close');
	toastClose.onclick = hideToastOnClose;
	toastClose.innerHTML = '<svg viewBox="0 0 20 20" width="1em" height="1em"> <path fill="white" stroke-width="0.8px" stroke="white" d="M10.185,1.417c-4.741,0-8.583,3.842-8.583,8.583c0,4.74,3.842,8.582,8.583,8.582S18.768,14.74,18.768,10C18.768,5.259,14.926,1.417,10.185,1.417 M10.185,17.68c-4.235,0-7.679-3.445-7.679-7.68c0-4.235,3.444-7.679,7.679-7.679S17.864,5.765,17.864,10C17.864,14.234,14.42,17.68,10.185,17.68 M10.824,10l2.842-2.844c0.178-0.176,0.178-0.46,0-0.637c-0.177-0.178-0.461-0.178-0.637,0l-2.844,2.841L7.341,6.52c-0.176-0.178-0.46-0.178-0.637,0c-0.178,0.176-0.178,0.461,0,0.637L9.546,10l-2.841,2.844c-0.178,0.176-0.178,0.461,0,0.637c0.178,0.178,0.459,0.178,0.637,0l2.844-2.841l2.844,2.841c0.178,0.178,0.459,0.178,0.637,0c0.178-0.176,0.178-0.461,0-0.637L10.824,10z"></path> </svg>';
	toastElement.appendChild(toastClose);

	document.getElementsByTagName("BODY")[0].appendChild(toastElement);

}

function removeColor() {
	toastElement.classList.remove('toast-message-success');
	toastElement.classList.remove('toast-message-error');
	toastElement.classList.remove('toast-message-info');
	toastElement.classList.remove('toast-message-warning');
	toastElement.classList.remove('toast-force-close');
}