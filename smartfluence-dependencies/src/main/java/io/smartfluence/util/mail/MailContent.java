package io.smartfluence.util.mail;

public interface MailContent {

	MailContent reset();

	MailContent appendContent(String content);

	MailContent setValue(String param, String value);

	MailContent setLink(String param, String value,String address);

	StringBuilder content();

	SmartMailBuilder and();
}