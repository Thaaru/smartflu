package io.smartfluence.util.mail;

class Salutation extends AbstractMailContent {

	private static final String SECTION_PREFIX = "<tr><td style=\"padding-top: 32px;\"><span style=\"padding-top: 16px;padding-bottom: 16px;font-size: 24px;color: #17a78e; font-weight: bold;font-family: Helvetica, Arial, sans-serif;\">";

	private static final String SECTION_SUFFIX = "</span></td></tr>";

	public Salutation(SmartMailBuilder smartMailBuilder) {
		this.setBuilder(smartMailBuilder);
	}

	@Override
	public StringBuilder content() {
		String content= super.content().toString();
		super.content().setLength(0);
		return super.content().append(SECTION_PREFIX).append(content).append(SECTION_SUFFIX);
	}
}