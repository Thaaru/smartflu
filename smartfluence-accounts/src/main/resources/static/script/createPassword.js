function formCallBack(obj) {
	if (obj.id == 'resetpasswordForm')
		createPassword(obj);
}

var password = document.getElementById("password"), confirm_password = document
		.getElementById("confirmPassword");

function createPassword(obj) {
	var verificationCode = getParam('verificationCode');
	if (password.value != confirm_password.value) {
		ERROR("Passwords Don't Match");
	} else {
		var form = $(obj).ajaxSubmit({

			beforeSubmit : function(arr, $form, options) {
				arr.push({
					name : 'verificationCode',
					value : verificationCode
				});
			}
		});
		var xhr = form.data('jqxhr');
		xhr.done(function(d, s, r) {
			resetForms();
			window.location.href=window.location.origin+'/create-password-successfully';
		});
		xhr.fail(function(x) {
			ERROR('Our server is facing a technical issue. Please try again after a while');
		});
	}
}
password.onchange = createPassword();
confirm_password.onkeyup = createPassword();