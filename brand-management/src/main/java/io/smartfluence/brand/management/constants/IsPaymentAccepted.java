package io.smartfluence.brand.management.constants;

public enum IsPaymentAccepted {
    TRUE, FALSE
}
