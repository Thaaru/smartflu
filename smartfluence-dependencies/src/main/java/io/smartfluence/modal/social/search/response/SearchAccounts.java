package io.smartfluence.modal.social.search.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchAccounts implements Serializable {

	private static final long serialVersionUID = 4565355623937435726L;

	@JsonAlias("account")
	private SearchAccount account;

	@JsonAlias("match")
	private Match match;
}