package io.smartfluence.modal.validation.brand.explore;

import java.util.UUID;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SubmitBidModel {
	@NotBlank(message = "Select a payment type")
	private String paymentType;

	private String postType;

	private String postDuration;

	private String includedTag;

	private String generalMessage;

	private String affiliateType;

	@NotNull(message = "Please enter a bid amount")
	@DecimalMin(value = "0.00", message = "")
	private Double bidAmount;

	@NotBlank(message = "Please enter platform")
	private String platform;

	private String campaignName;

	private UUID mailTemplateId;

	private String mailDescription;

	private String mailSubject;

	private String mailCc;

	private UUID campaignId;

	@NotBlank(message = "Please provide influencerId")
	private String influencerId;

	private String reportId;

	@NotBlank(message = "Please provide influencerHandle")
	private String influencerHandle;

	private boolean reportPresent;

	private boolean isValid;

	public boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public boolean getReportPresent() {
		return reportPresent;
	}

	public void setReportPresent(boolean reportPresent) {
		this.reportPresent = reportPresent;
	}
}