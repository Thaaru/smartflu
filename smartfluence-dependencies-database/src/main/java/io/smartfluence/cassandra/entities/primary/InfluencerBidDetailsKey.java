package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import io.smartfluence.constants.Platforms;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerBidDetailsKey implements Serializable {

	private static final long serialVersionUID = 7365431281239948743L;

	public InfluencerBidDetailsKey(InfluencerBidDetailsKey key) {
		this.influencerId = key.getInfluencerId();
		this.platform = key.getPlatform();
		this.brandId = key.getBrandId();
		this.bidId = key.getBidId();
	}

	@PrimaryKeyColumn(name = "influencer_id", type = PrimaryKeyType.PARTITIONED)
	private UUID influencerId;

	@PrimaryKeyColumn(name = "platform", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private Platforms platform;

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private UUID brandId;

	@PrimaryKeyColumn(name = "bid_id", type = PrimaryKeyType.CLUSTERED, ordinal = 3)
	private UUID bidId;
}