package io.smartfluence.util.image;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

class BrightUpFilter implements ImageFilter {

	@Override
	public BufferedImage processImageBuffer(BufferedImage bufferedImage) {
		float[] data = new float[] { 0, 0, 0, 0, 2, 0, 0, 0, 0 };
		BufferedImageOp filter = new ConvolveOp(new Kernel(3, 3, data));
		return filter.filter(bufferedImage, null);
	}
}