package io.smartfluence.mysql.entities.influencer;

public class CustomResponseEntity {
    String code;
    Object value;
    String error;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "CustomResponseEntity{" +
                "code='" + code + '\'' +
                ", value=" + value +
                ", error='" + error + '\'' +
                '}';
    } 
}
