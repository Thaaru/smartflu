package io.smartfluence.modal.brand.campaignv1;

import lombok.Data;

@Data
public class InfluAddressObj {
    private String campaignId;
    private String influencerId;
    private AddressTypeEntity addressType;
    private String fullName;
    private String addressLine1;
    private String addressLine2;
    private Long countryId;
    private Long stateId;
    private String cityName;
    private String zipcode;
    private String phoneNumber;

    public InfluAddressObj(String campaignId, String influencerId, AddressTypeEntity addressType,
                           String fullName, String addressLine1, String addressLine2, Long countryId, Long stateId,
                           String cityName, String zipcode, String phoneNumber) {
        this.campaignId=campaignId;
        this.influencerId=influencerId;
        this.addressType=addressType;
        this.fullName=fullName;
        this.addressLine1=addressLine1;
        this.addressLine2=addressLine2;
        this.countryId=countryId;
        this.stateId=stateId;
        this.cityName=cityName;
        this.zipcode=zipcode;
        this.phoneNumber=phoneNumber;
    }
}
