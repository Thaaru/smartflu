package io.smartfluence.mysql.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.Builder.Default;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "mail_template_master")
public class MailTemplateMaster implements Serializable {

	private static final long serialVersionUID = -5526691490493582921L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", columnDefinition = "VARCHAR(255)")
	private String id;

	@Column(name = "mail_body")
	private String mailBody;

	@Column(name = "mail_subject")
	private String mailSubject;

	@Column(name = "name")
	private String templateName;

	@Column(name = "payment_type")
	private String paymentType;

	@Column(name = "sub_payment_type")
	private String subPaymentType;

	@Default
	@Column(name = "is_hidden")
	private Boolean isHidden = false;

	@Column(name = "is_new")
	private Boolean isNew;
}