import React, { useState } from "react";
import { toast } from "react-toastify";
import ColoredLine from "../components/line_component";
import { loaderMethod } from "../components/loader";
import { PoweredBy } from "../components/powered_by";
import { CProgressBar } from "../components/progress_bar";
import { createInfluencerAccountApi } from "../services/api/campaign_details_api";
import { login } from "../utils/http_urls";
import { ACCOUNT_STATUS, CREATED_ACCOUNT_MESSAGE, ERROR_MESSAGE, LOGIN_ERROR_MESSAGE, PASSWORD_SET_MESSAGE, PASSWORD_STATUS, SMARTFLUENCE_LOGO, SMARTFLUENCE_NAME_LOGO } from "../utils/string_constants";

export default function CreateAccountScreen({ data,}) {
    const [loaderComponent, setLoaderComponent] = useState(true);
    const [screenChange, setScreenChange] = useState(true);
    const loaderOpen = (status) => {
        if (status == true) {
            setLoaderComponent(status);
        } else {
            setLoaderComponent(status)
        }
    }
    const submitHandler = async (data, loader) => {
        if (data.influencerAccountStatus != ACCOUNT_STATUS) {
            loader(false);
            const result = await createInfluencerAccountApi(data.influencerId).then((result) => {
                loader(true);
                if (result.hasOwnProperty("code")) {
                    if (result.code.toString() == "604" || result.code.toString() == "603") {
                        toast.error(ERROR_MESSAGE, { delay: 1000 })
                    }
                } else {
                    setScreenChange(false);
                }
            }).catch((e) => {
                loader(true);
                toast.error(ERROR_MESSAGE, { delay: 1000 })
            });
        } else {
            toast.error(CREATED_ACCOUNT_MESSAGE)
        }

    }

    const loginHandler = () => {
        if (data.influencerAccountStatus == ACCOUNT_STATUS) {
            if (data.influencerPasswordStatus == PASSWORD_STATUS) {
                window.location.assign(login());
            } else {
                toast.error(PASSWORD_SET_MESSAGE)
            }
        } else {
            toast.error(LOGIN_ERROR_MESSAGE)
        }
    }

    return (
        <>
            {screenChange == true ?
                <div class='content-wrapper'>
                    <div>
                        <div>
                            <div className="col-lg" style={{ "textAlign": "left" }}>
                                <div className="col-md normalText">
                                    Finalize Your Proposal
                                </div>
                                <div className="col-md heading">
                                    Create An Account
                                </div>
                            </div>
                            <ColoredLine color={'secondaryColor'} />
                        </div>
                        <div class="col" style={{ "textAlign": "left", "justifyContent": "space-between" }}>
                            <div className="col-md" style={{ "lineHeight": "4" }}>
                                Thank you for submitting your Proposal !
                            </div>
                            <div className="col-md normalText">
                                To keep track of your progress and get directly notified when the brand responds to your proposal. Please make an account.
                            </div>
                            <div className="col-md normalText">
                                Username
                            </div>
                            <div className="col-md">
                                <input type="text"
                                    class="form-control"
                                    id="influencerEmail"
                                    disabled={true}
                                    value={data.influencerEmail}
                                />
                            </div>
                        </div>
                        <div className="footer">
                            <ColoredLine color={'secondaryColor'} />
                            <div className="row " style={{ justifyContent: 'center', alignItems: "center", padding: "0 24px" }}>
                                <div class="col-6">
                                    <button type="button" class="btn btn-outline-secondary acceptButton" style={{ borderWidth: "2px", "borderColor": "#2F008D", color: "#2F008D", }} onClick={() => { submitHandler(data, loaderOpen) }}>
                                        {loaderComponent == true ? "Create Account" : loaderMethod()}
                                    </button>
                                </div>
                                <div class="col-6 ">
                                    <button type="button" class="btn btn-secondary " style={{ borderWidth: "2px", "borderColor": "#2F008D", backgroundColor: "#2F008D", height: "100%", width: "100%", padding: "10px" }} onClick={loginHandler}>
                                        Log In
                                    </button>
                                </div>
                            </div>
                            <CProgressBar data={data}/>
                            <PoweredBy/>
                        </div>
                    </div>
                </div> : <div class='content-wrapper'>
                    <div>
                        <div>
                            <div className="col-lg" style={{ "textAlign": "center" }}>
                                <div className="row" style={{ "justifyContent": "center", "justifyItems": "center" }}>
                                    <div><img src={SMARTFLUENCE_LOGO} height="30px" /></div>
                                    <div><img src={SMARTFLUENCE_NAME_LOGO} height="30px" /></div>
                                </div>
                            </div>
                            <ColoredLine color={'secondaryColor'} />
                        </div>
                        <div class="col" style={{ "textAlign": "left", "justifyContent": "space-between" }}>
                            <div className="col-md normalText" >
                                Congratulations! Your account has been created. Please verify your account via the confirmation email that was sent to you.
                            </div>
                            <div className="col-md normalText">
                                {data.brandName} has received your proposal and are promptly reviewing it. If you need to get in contact with them directly, please email them directly through the email below.
                            </div>
                        </div>
                        <div class="col" style={{ "textAlign": "center", "justifyContent": "space-between" }}>
                            <div className="col-md normalText" >
                                {data.brandName}
                            </div>
                            <div className="col-md normalText">
                                {data.brandEmail}
                            </div>
                        </div>
                        <div className="footer">
                            <ColoredLine color={'secondaryColor'} />
                            <CProgressBar data={data}/>
                            <PoweredBy/>
                        </div>
                    </div>
                </div>
            }
        </>
    );
}
