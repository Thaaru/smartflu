package io.smartfluence.influencer.entities;

import java.io.Serializable;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignInfluencerProductId implements Serializable {

    @Column(name = "campaign_id")
    private String campaignId;
    @Column(name = "influencer_id")
    private String influencerId;
    @Column(name = "product_id")
    private int productId;
}
