package io.smartfluence.webapp.controller.rest.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_influencer_details_v1")
public class CampaignInfluencerV1Entity {

    @Id
    @Column(name = "campaign_id")
    private String campaignId;
    @Column(name = "influencer_id")
    private String influencerId;
    @Column(name = "influencer_email")
    private String influencerEmail;
    @Column(name = "platform")
    private String platform;

    @Column(name = "proposal_status")
    private String proposalStatus;
    @Column(name = " is_offer_accepted")
    private String isOfferAccepted;
    @Column(name = "offer_accepted_at")
    private LocalDateTime OfferAcceptedAt;
    @Column(name = " is_products_accepted")
    private String isProductsAccepted;
    @Column(name = "products_accepted_at")
    private LocalDateTime productsAcceptedAt;
    @Column(name = "is_deliverables_accepted")
    private String isDeliverablesAccepted;
    @Column(name = "deliverables_accepted_at")
    private LocalDateTime deliverablesAcceptedAt;
    @Column(name = " is_shipping_accepted")
    private String isShippingAccepted;
    @Column(name = "shipping_accepted_at")
    private LocalDateTime shippingAcceptedAt;
    @Column(name = "is_payment_accepted")
    private String isPaymentAccepted;
    @Column(name = "payment_accepted_at")
    private LocalDateTime paymentAcceptedAt;
    @Column(name = " is_terms_accepted")
    private String isTermsAccepted;
    @Column(name = "terms_accepted_at")
    private LocalDateTime termsAcceptedAt;
    @Column(name = "payment_method_type")
    private String paymentMethodType;
    @Column(name = "payment_email_address")
    private String paymentEmailAddress;
    @Column(name = "campaign_paid_at")
    private LocalDateTime campaignPaidAt;

   @Column(name = "influencer_handle")
   private String influencerHandle;
   @Column(name = "influencer_audience_credibility")
   private String influencerAudienceCredibility;
   @Column(name = "influencer_engagement")
   private String influencerEngagement;
   @Column(name = "influencer_followers")
   private String influencerFollowers;
   @Column(name = "influencer_first_name")
   private String influencerFirstName;
   @Column(name = "influencer_last_name")
   private String influencerLastName;

}
