var rowsSelected = [];
var campaignWFFPDT;


$(document).ready(function () {
    $.fn.dataTable.ext.errMode = 'none';
    campaignId = window.location.href.split("/")[5];
    init_fulFillTable(campaignId);

    $(".fulFillProductAction").click(function () {
        // console.log($(this))
        alert("Handler for .change() called.");
    });

    campaignWFFPDT.on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
        //if ( originalEvent.target.nodeName.toLowerCase() === 'img' ) {
            e.preventDefault();
        //}
    } );
});

var influencersList_ff= [];
var currencyMaster = [];
var affiliateMaster = [];
var platformMasters = [];
var postTypeMasters = [];

//table_campaign_fulfill
function init_fulFillTable(campaignId) {
     campaignWFFPDT = $('#table_campaign_fulfill').DataTable({
        dom: "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-7 pl-0'><'col-sm-12 col-md-3'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
        ajax: {
            url: `/brand/view-campaign-v1/view-influencers?proposalStatus=PAID&id=${campaignId}`,
            dataSrc: function (response) {
                // console.log("FULFILL PRODUCT DATA RESPONSE: ", response.data)
                if (response?.code == 600) {
                    // console.log("influencerlist : ", response.data)
                    currencyMaster = response.data.currencyMaster;
                    affiliateMaster = response.data.affiliateMaster;
                    influencersList_ff = response.data.influencersList;
                    if (influencersList_ff?.length > 0) {
                        platformMasters = influencersList_ff[0].platformMasters;
                        postTypeMasters = influencersList_ff[0].postTypeMasters;
                        return influencersList_ff;
                    }
                    return [];
                }
                return [];
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                targets: 0,
                width: "15%",
                className: "text-center",
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    //meta.settings.aoData[0]._aData.influencerId
                    // console.log("checkbox render", data)
                    return '<input disabled type="checkbox" class="inviteCheckbox">';
                }
            },
            {
                targets: 1,
                width: "18%",
                className: "text-left",
                data: "influencerHandle",
                render:function (data, type, row, meta) {
                   return `<a style=" color: #2f008d" href="/brand/view-influencer/${row.influencerId}?isValid=true&platform=${row.platform}"
                   target="_blank">${data}</a>`
                }
            },
            {
                targets: 2,
                width: "18%",
                className: "text-left",
                data: "products",
                render: function (data, type, row, meta) {
                    
                    // var productLink = data.map(x => x.productLink);
                    var result = '';
                    if (campaignStatusFromCampaignWF == "ENDED") {
                        result += `<a style=" color: #2f008d" class="disabled"   style="color:blue; pointer-events:none;">View Selected<br>Products</a>`;
                    } else {
                        result += `<a style=" color: #2f008d" onClick=viewSelectedProductsFf("${row.influencerId}") style="color:blue;pointer-events:none;">View Selected<br>Products</a>`;
                    }
                    return result
                }
            },
            {
                targets: 3,
                width: "17%",
                className: "text-left",
                data: "address",
                render: function (data, type, row, meta) {
                    var address = data.find(x => x.addressType === 'SHIPPING');

                    var result = '';
                   if(address){
                    if (address.addressLine1) {
                        result += `<span> ${address?.addressLine1}</span><br>`;
                    }if(address.addressLine2){
                        result += `<span>${address?.addressLine2}</span><br>`
                    }if(address.cityName){
                        result += `<span>${address?.cityName}</span>`
                    }
                   }else{
                       result = "-"
                   }
                    return result;
                }

            },
            {
                targets: 4,
                className: "text-left",
                width: "19%",
                render: function (data, type, row, meta) {
                    var influencerId = row.influencerId;
                    var campaignId = row.campaignId;
                    if (campaignStatusFromCampaignWF == "ENDED") {
                    return `
                 <div>  <input max=50 disabled  type="text"  id='tracking_${row.influencerId}'/><span style="background-color:red" class="errorMsg" id="errortracking_${row.influencerId}></span><div>`;
                    //onfocusout="fulFillProductApproval('reject','${campaignId}','${influencerId}','#tracking_${influencerId}','#errortracking_${influencerId}')"
                }else{
                    return `
                    <div>  <input class="w-75" max=50  placeholder="Tracking ID" type="text"  id='tracking_${row.influencerId}'/><span style="background-color:red" class="errorMsg" id="errortracking_${row.influencerId}></span><div>`;
                }
            }
            },
            {
                targets: 5,
                data: "proposalStatus",
                className: "text-left",
                width: "20%",
                render: function (data, type, row, meta) {
                    if (data == "PAID") {   
                        return `<a style="color: #2f008d";>Product Unfulfilled</a>`;
                    }
                }
            },


            {
                targets: 6,
                className: "text-center",
                render: function (data, type, row, meta) {

                    var influencerId = row.influencerId;
                    var campaignId = row.campaignId;
                    var proposalStatus = row.proposalStatus;
                    if (campaignStatusFromCampaignWF == "ENDED") {
                    return `
                    <div class="dropdown">
                    <span class="dot-icon disabled"><i class="fa fa-ellipsis-v"></i></span>
                </div>
     `;}else{
        return `
        <div class="dropdown fulFillProductAction" id=${meta.settings.aoData[0]._aData.influencerId}>
            <span class="dot-icon"><i class="fa fa-ellipsis-v"></i></span>
            <ul class="dropdown-content">
                <li id="accept" onclick="fulFillProductApproval('accept','${campaignId}','${influencerId}','#tracking_${influencerId}','#errortracking_${influencerId}')">Mark As Fulfilled</li>
            </ul>
        </div>
    `;
     }
                }
            },
            {
                className: 'control ',
                targets: [0, 2,4,6],
                orderable: false
            }
        ],
        select: {
            style: 'multi'
        },
        order: [
            [1, 'asc']
        ],
        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            var rowId = data.DT_RowId;
            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rowsSelected) !== -1) {
                $(row).find('input[type="checkbox"]').prop(
                    'checked', true);
                $(row).addClass('selected');
            }
        }
    });


    $('#table_campaign_fulfill tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');
        // Get row data
        var data = campaignWFFPDT.row($row).data();
        // Get row ID
        var rowId = data.DT_RowId;
        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rowsSelected);
        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rowsSelected.push(rowId);
            // Otherwise, if checkbox is not checked and row ID is in list of
            // selected
            // row IDs
        } else if (!this.checked && index !== -1) {
            rowsSelected.splice(index, 1);
        }
        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(campaignWFFPDT);
        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

}



function viewSelectedProductsFf(selectedInfluencer){
    let selectedProducts = influencersList_ff.find(x => x.influencerId == selectedInfluencer)?.products;
    var productHtml = ""
    $("#productPopup").empty()
    if (!selectedProducts || selectedProducts?.length ==0) {
        $("#productPopup").html("No products found")
    } else {
        selectedProducts.map((x, index) => {
            // console.log("product details : ", x.productName, " : ", x.productLink);
            productHtml += `<div style="display:flex;">
        <input type="checkbox" checked  style="visibility:visible;left: 13px;width:18px;height:18px"/>
        <span id="productNameModal_${index}"  style="padding-left:24px; font-weight:400; line-height:1; transform: translateY(-2px);">
        <span style='font-size:18px; text-transform:capitalize;'>${x.productName}</span>
        <br/><br> <a style='white-space: pre-wrap;word-break: break-all;' href="${x.productLink}"> <u>${x.productLink}</u></a></span></div>`
        $("#productPopup").html(productHtml)
            // $(`#productNameModal`).text();
            // $(`#productLinkModal`).text();
            // $(`#productLinkModal`).attr("href", x.productLink);
        });
    }
    $("#viewProducts").modal("show");


}


function fulFillProductApproval(status, campaignId, influencerId, trackingId, errortrackingId) {

    if ($(trackingId).val() == "") {
        toast("Tracking is Required", "error");
        return
    } else {
        var trackingdataValue = $(trackingId).val()
        var requestFulfillProductData = {
            campaignId: campaignId,
            influencerId: influencerId,
            status: status,
            workflowStatus: "FULFILL_PRODUCTS",
            trackingId: trackingdataValue
        }

        $.ajax({
            url: "/brand/view-campaign-v1/view-influencers/brandapproval",
            type: "PUT",
            data: JSON.stringify(requestFulfillProductData),
            dataType: "json",
            contentType: "application/json",
        }).done(function (data) {
            // data = JSON.parse(data);
            if (data.code == "600") {
                campaignWFFPDT.ajax.reload(null, false);
                campaignWFSTDT.ajax.reload(null, false);
                var someTabTriggerEl = document.querySelector('#statistics-tab');
                var tab = new bootstrap.Tab(someTabTriggerEl);
                tab.show();
                toast("Marked as Fulfilled");
            } else if (data.code == "601") {
                toast("tracking ID added successfully");
                return
            }
                 else {
                     toast(data.errors.message);
                 }

            //window.location.href = "/brand/view-campaign-v1/"+msg.campaignId;
            // $("#campaignForm").trigger("reset");
        }).fail(function (jqXHR, textStatus) {
            toast("Request Failed " + textStatus, "error")
        });
    }
}

