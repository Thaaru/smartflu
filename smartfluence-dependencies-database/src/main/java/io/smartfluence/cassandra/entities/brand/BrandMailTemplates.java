package io.smartfluence.cassandra.entities.brand;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandMailTemplatesKey;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_mail_templates")
public class BrandMailTemplates implements Copyable {

	private static final long serialVersionUID = -2134048326715969400L;

	public BrandMailTemplates(BrandMailTemplates brandMailTemplates) {
		this.key = new BrandMailTemplatesKey(brandMailTemplates.getKey());
		this.createdAt = brandMailTemplates.getCreatedAt();
		this.templateSubject = brandMailTemplates.getTemplateSubject();
		this.templateBody = brandMailTemplates.getTemplateBody();
		this.templateId = brandMailTemplates.getTemplateId();
	}

	@PrimaryKey
	private BrandMailTemplatesKey key;

	@Column("template_subject")
	private String templateSubject;

	@Column("template_id")
	private UUID templateId;

	@Column("template_body")
	private String templateBody;

	@Column("created_at")
	private Date createdAt;

	@Column("modified_at")
	private Date modifiedAt;
}