package io.smartfluence.mysql.entities;

import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_influencer_details_v1")
public class CampaignInfluencerDetails {
    @EmbeddedId
    private CampaignInfluencerId campaignInfluencerId;
    private String influencerEmail;
    private String platform;
    private String paymentEmailAddress;
    private String influencerFirstName;
    private String influencerLastName;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isOfferAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isProductAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isDeliverablesAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isShippingAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isPaymentAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isTermsAccepted;
    @Enumerated(EnumType.STRING)
    private ProposalStatus proposalStatus;
    @Enumerated(EnumType.STRING)
    private PaymentMethodType paymentMethodType;
    public LocalDateTime productsAcceptedAt;
    public LocalDateTime offerAcceptedAt;
    public LocalDateTime deliverablesAcceptedAt;
    public LocalDateTime shippingAcceptedAt;
    public LocalDateTime paymentAcceptedAt;
    public LocalDateTime termsAcceptedAt;
    public LocalDateTime campaignPaidAt;

}
