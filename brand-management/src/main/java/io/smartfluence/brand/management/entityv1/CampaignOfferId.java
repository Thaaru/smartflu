package io.smartfluence.brand.management.entityv1;

import io.smartfluence.modal.brand.campaignv1.OfferTypeEntityV1;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignOfferId implements Serializable {
    @Column(name = "campaign_id")
    private String campaignId;
    @Enumerated(EnumType.STRING)
    @Column(name = "offer_type")
    private OfferTypeEntityV1 offerType;
}
