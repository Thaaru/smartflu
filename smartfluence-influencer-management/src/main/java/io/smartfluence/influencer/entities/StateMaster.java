package io.smartfluence.influencer.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "state_master")
@Entity
public class StateMaster {
    @Id
    public Long stateId;
    private String stateName;
    private Long countryId;
}



