package io.smartfluence.brand.management.servicev1;

import io.smartfluence.brand.management.entityv1.*;
import io.smartfluence.brand.management.repositoryv1.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MasterServiceV1 {

    @Autowired
    PlatformMasterRepositoryV1 platformMasterRepositoryV1;
    @Autowired
    PlatformPostTypeRepositoryV1 platformPostTypeRepositoryV1;
    @Autowired
    CurrencyMasterRepositoryV1 currencyMasterRepositoryV1;
    @Autowired
    CommissionAffiliateTypeRepositoryV1 commissionAffiliateTypeRepositoryV1;
    @Autowired
    WorkFlowMasterRepo workFlowMasterRepo;

    public List<PlatformMasterV1> getPlatformMaster(){
        return platformMasterRepositoryV1.findAll();
    }

    public List<PlatformPostTypeMasterV1> getPlatformPostMaster(){
        return platformPostTypeRepositoryV1.findAll();
    }

    public List<CurrencyMasterV1> getCurrencyMaster(){
        return currencyMasterRepositoryV1.findAll();
    }

    public List<CommissionAffiliateTypeMasterV1> getCommissionAffiliateTypeMaster(){
        return commissionAffiliateTypeRepositoryV1.findAll();
    }

    public PlatformMasterV1 getPlatformById(Long platformId){
        return  platformMasterRepositoryV1.findByPlatformId(platformId);
    }

    public CommissionAffiliateTypeMasterV1 getCommAffMasterByCommAffId(int commAffId){
        return commissionAffiliateTypeRepositoryV1.findByCommAffiTypeId(commAffId);
    }

    public CurrencyMasterV1 getCurrencyById(int currId){
        return currencyMasterRepositoryV1.findByCurrencyId(currId);
    }

    public List<WorkflowMaster> getAllWorkflowMaster(){
        return workFlowMasterRepo.findAll();
    }


}
