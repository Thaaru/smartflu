import React from "react";
import { STEPS_ACCEPTED_STATUS } from "../utils/string_constants";

const progressStatus = (data) => {
if (data.offerAcceptedStatus == STEPS_ACCEPTED_STATUS) {
        if ( data.productsAcceptedStatus == STEPS_ACCEPTED_STATUS) {
            if ( data.deliverablesAcceptedStatus == STEPS_ACCEPTED_STATUS) {
                if ( data.shippingAcceptedStatus == STEPS_ACCEPTED_STATUS) {
                    if ( data.paymentAcceptedStatus == STEPS_ACCEPTED_STATUS) {
                        if ( data.termsConditionStatus == STEPS_ACCEPTED_STATUS) {
                             return 100;
                        } else return 85;
                    } else return 68;
                } else return 51;
            } else return 34;
        } else return 17;
    } else return 0;

}
export default progressStatus;






