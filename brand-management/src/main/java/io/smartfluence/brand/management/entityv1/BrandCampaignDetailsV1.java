package io.smartfluence.brand.management.entityv1;


import io.smartfluence.modal.brand.campaignv1.CampaignStatusEntityV1;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "brand_campaign_details_v1")
public class BrandCampaignDetailsV1 implements Serializable {
    @Id
//    @Column(name = "campaign_id")
    private String campaignId;
    @Column(name = "brand_id")
    private String brandId;
    @Column(name = "brand_description")
    private String brandDescription;
    @Column(name = "campaign_name")
    private String campaignName;
    @Column(name = "campaign_description")
    private String campaignDescription;
    @Column(name = "campaign_status")
    @Enumerated(EnumType.STRING)
    private CampaignStatusEntityV1 campaignStatus;
    @Column(name="hashtags")
    private String hashTags;
    @Column(name = "restrictions")
    private String restrictions;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;
    @Column(name="max_product_count")
    private Integer maxProductCount;
    @Column(name="terms_id")
    private Long termsId;
    @Column(name = "campaign_end_date")
    private LocalDateTime endDate;
    @Column(name = "influencer_count")
    private int influencerCount;
    @Transient
    private String platformName;
    @Transient
    private  Long platformId;


    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "campaignId",insertable = false,updatable = false  )
    private List<CampaignInfluencerV1Entity> influencers;

//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "campaignId",insertable = false,updatable = false )
//    private List<CampaignTasksV1>  campaignTasks;



    @Override
    public String toString() {
        return "BrandCampaignDetailsV1{" +
                "campaignId='" + campaignId + '\'' +
                ", brandId='" + brandId + '\'' +
                ", brandDescription='" + brandDescription + '\'' +
                ", campaignName='" + campaignName + '\'' +
                ", campaignDescription='" + campaignDescription + '\'' +
                ", campaignStatus=" + campaignStatus +
                ", hashTags='" + hashTags + '\'' +
                ", restrictions='" + restrictions + '\'' +
                ", createdAt=" + createdAt +
                ", modifiedAt=" + modifiedAt +
                ", maxProductCount=" + maxProductCount +
                ", termsId=" + termsId +
                ", endDate=" + endDate +
                ", influencerCount=" + influencerCount +
                ", influencers=" + influencers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrandCampaignDetailsV1 that = (BrandCampaignDetailsV1) o;
        return influencerCount == that.influencerCount && Objects.equals(campaignId, that.campaignId) && Objects.equals(brandId, that.brandId) && Objects.equals(brandDescription, that.brandDescription) && Objects.equals(campaignName, that.campaignName) && Objects.equals(campaignDescription, that.campaignDescription) && campaignStatus == that.campaignStatus && Objects.equals(hashTags, that.hashTags) && Objects.equals(restrictions, that.restrictions) && Objects.equals(createdAt, that.createdAt) && Objects.equals(modifiedAt, that.modifiedAt) && Objects.equals(maxProductCount, that.maxProductCount) && Objects.equals(termsId, that.termsId) && Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(campaignId, brandId, brandDescription, campaignName, campaignDescription, campaignStatus, hashTags, restrictions, createdAt, modifiedAt, maxProductCount, termsId, endDate, influencerCount);
    }
}
