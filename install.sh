echo Hello

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
export _CD="$PWD"

echo "Select an option:"
echo "   0) smartfluence-parent"
echo "   1) smartfluence-dependencies"
echo "   2) smartfluence-dependencies-database"
echo "   3) smartfluence-discovery"
echo "   4) smartfluence-accounts"
echo "   5) smartfluence-admin-management"
echo "   6) brand-management"
echo "   7) smartfluence"
echo "   8) smartfluence-gateway"
echo "   9) smartfluence-influencer-management"
echo "   *) All"

read -p "Option: " option

case "$option" in
	0)
		echo
		echo Building smartfluence-parent
		cd ${_CD}/smartfluence-parent/
		mvn  clean install -Dmaven.test.skip=true
	;;
	1)
		echo
		echo Building smartfluence-dependencies
		cd ${_CD}/smartfluence-dependencies/
		mvn  clean install -Dmaven.test.skip=true
	;;
	2)
		echo
		echo Building smartfluence-dependencies-database
		cd ${_CD}/smartfluence-dependencies-database/
		mvn  clean install -Dmaven.test.skip=true

	;;
	3)
		echo
		echo Building smartfluence-discovery
		cd ${_CD}/smartfluence-discovery/
		mvn  clean install -Dmaven.test.skip=true
	;;
	4)
		echo
		echo Building smartfluence-accounts
		cd ${_CD}/smartfluence-accounts/
		mvn  clean install -Dmaven.test.skip=true
	;;
	5)
		echo
		echo Building smartfluence-admin-management
		cd ${_CD}/smartfluence-admin-management/
		mvn  clean install -Dmaven.test.skip=true
	;;
	6)
		echo
		echo Building brand-management
		cd ${_CD}/brand-management/
		mvn  clean install -Dmaven.test.skip=true
	;;
	7)
		echo
		echo Building smartfluence
		cd ${_CD}/smartfluence/
		mvn  clean install -Dmaven.test.skip=true
	;;
	8)
		echo
		echo Building smartfluence-gateway
		cd ${_CD}/smartfluence-gateway/
		mvn  clean install -Dmaven.test.skip=true
	;;
	9)
		echo
		echo Building smartfluence-influencer-management
		cd ${_CD}/smartfluence-influencer-management/
		mvn  clean install -Dmaven.test.skip=true
	;;
	*)
		echo
		echo "Build All Modules"
		echo Building smartfluence-dependencies
		cd ${_CD}/smartfluence-dependencies/
		mvn  clean install -Dmaven.test.skip=true

		echo Building smartfluence-dependencies-database
		cd ${_CD}/smartfluence-dependencies-database/
		mvn  clean install -Dmaven.test.skip=true

		echo Building smartfluence-discovery
		cd ${_CD}/smartfluence-discovery/
		mvn  clean install -Dmaven.test.skip=true

		echo Building smartfluence-accounts
		cd ${_CD}/smartfluence-accounts/
		mvn  clean install -Dmaven.test.skip=true

		echo Building smartfluence-admin-management
		cd ${_CD}/smartfluence-admin-management/
		mvn  clean install -Dmaven.test.skip=true

		echo Building brand-management
		cd ${_CD}/brand-management/
		mvn  clean install -Dmaven.test.skip=true

		echo Building smartfluence-influencer-management
		cd ${_CD}/smartfluence-influencer-management/
		mvn  clean install -Dmaven.test.skip=true

		echo Building smartfluence
		cd ${_CD}/smartfluence/
		mvn  clean install -Dmaven.test.skip=true

		echo Building smartfluence-gateway
		cd ${_CD}/smartfluence-gateway/
		mvn  clean install -Dmaven.test.skip=true
	;;
esac

cd ${_CD}/
echo Build successful..!!
