package io.smartfluence.cassandra.repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.primary.ReportSearchHistoryKey;

@Repository
public interface ReportSearchHistoryRepo extends CassandraRepository<ReportSearchHistory, ReportSearchHistoryKey> {

	@Query("select count(brand_id) from report_search_history WHERE brand_id=:brandId AND is_new IN (false, true) AND created_at>=:from AND created_at<=:to")
	Long countByCreatedAt(@Param("brandId") UUID brandId, @Param("from") Date from, @Param("to") Date to);

	List<ReportSearchHistory> findAllBySearchId(UUID searchId);
}