package io.smartfluence.brand.management.entityv1;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "campaign_influencer_products_v1")
public class CampaignInfluencerProducts {
    @EmbeddedId
    CampaignInfluencerProductId cipId;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "productId",insertable = false,updatable = false)
    CampaignProductsV1 campaignProducts;

}
