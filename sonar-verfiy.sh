echo Hello

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/
export _CD="$PWD"

sonar_verify () {
	mvn clean verify sonar:sonar \
	  -Dsonar.projectKey=$1 \
	  -Dsonar.host.url=http://34.226.134.237:8888 \
	  -Dsonar.login="admin" \
	  -Dsonar.password="admin@1234" \
	  -Dmaven.test.skip=true
}

echo "Select an option:"
echo "   0) smartfluence-parent"
echo "   1) smartfluence-dependencies"
echo "   2) smartfluence-dependencies-database"
echo "   3) smartfluence-discovery"
echo "   4) smartfluence-accounts"
echo "   5) smartfluence-admin-management"
echo "   6) brand-management"
echo "   7) smartfluence"
echo "   8) smartfluence-gateway"
echo "   9) smartfluence-influencer-management"
echo "   *) All"

read -p "Option: " option

case "$option" in
	0)
		echo
		echo Building smartfluence-parent
		cd ${_CD}/smartfluence-parent/
		mvn  clean install -Dmaven.test.skip=true
	;;
	1)
		echo
		echo Sonar Verifying - smartfluence-dependencies
		cd ${_CD}/smartfluence-dependencies/
		sonar_verify smartfluence-dependencies

	;;
	2)
		echo
		echo Sonar Verifying - smartfluence-dependencies-database
		cd ${_CD}/smartfluence-dependencies-database/
		sonar_verify smartfluence-dependencies-database

	;;
	3)
		echo
		echo Sonar Verifying - smartfluence-discovery
		cd ${_CD}/smartfluence-discovery/
		sonar_verify discovery
	;;
	4)
		echo
		echo Sonar Verifying - smartfluence-accounts
		cd ${_CD}/smartfluence-accounts/
		sonar_verify accounts
	;;
	5)
		echo
		echo Sonar Verifying - smartfluence-admin-management
		cd ${_CD}/smartfluence-admin-management/
		sonar_verify admin-management
	;;
	6)
		echo
		echo Sonar Verifying - brand-management
		cd ${_CD}/brand-management/
		sonar_verify brand-management
	;;
	7)
		echo
		echo Sonar Verifying - smartfluence
		cd ${_CD}/smartfluence/
		sonar_verify webapp
	;;
	8)
		echo
		echo Sonar Verifying - smartfluence-gateway
		cd ${_CD}/smartfluence-gateway/
		sonar_verify gateway
	;;
	9)
		echo
		echo Sonar Verifying - smartfluence-influencer-management
		cd ${_CD}/smartfluence-influencer-management/
		sonar_verify influencer-management
	;;
	*)
		echo
		echo Sonar Verifying - smartfluence-dependencies
		cd ${_CD}/smartfluence-dependencies/
		sonar_verify smartfluence-dependencies

		echo
		echo Sonar Verifying - smartfluence-dependencies-database
		cd ${_CD}/smartfluence-dependencies-database/
		sonar_verify smartfluence-dependencies-database

		echo
		echo Sonar Verifying - smartfluence-discovery
		cd ${_CD}/smartfluence-discovery/
		sonar_verify discovery

		echo
		echo Sonar Verifying - smartfluence-accounts
		cd ${_CD}/smartfluence-accounts/
		sonar_verify accounts

		echo
		echo Sonar Verifying - smartfluence-admin-management
		cd ${_CD}/smartfluence-admin-management/
		sonar_verify admin-management

		echo
		echo Sonar Verifying - brand-management
		cd ${_CD}/brand-management/
		sonar_verify brand-management

		echo
		echo Sonar Verifying - smartfluence
		cd ${_CD}/smartfluence/
		sonar_verify webapp

		echo
		echo Sonar Verifying - smartfluence-gateway
		cd ${_CD}/smartfluence-gateway/
		sonar_verify gateway

		echo
		echo Sonar Verifying - smartfluence-influencer-management
		cd ${_CD}/smartfluence-influencer-management/
		sonar_verify influencer-management
	;;
esac

cd ${_CD}/
echo Verified successful..!!
