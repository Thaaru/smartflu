import { useLocation } from "react-router-dom";
import { Machine, assign } from "xstate";
import { getCampaignDetailsApi } from "./services/api/campaign_details_api";
import submit from "./utils/submit_call";

export const influencerMachine = Machine({
  id: "influencer",
  context: {
    stack: [],
  },
  initial: "getStarted",
  states: {

    
    
    

    getStarted: {
      on: {
        NEXT: {
          target: "overview",
          actions: assign({ getStarted: (_, e) => e.value })
        }
      }
    },
    overview: {
      on: {
        OFFERS:{
          target: "offers",
          actions: assign({ overview: (_, e) => e.value })
        },
        PRODUCTS:{
          target: "products",
          actions: assign({ overview: (_, e) => e.value })
        },
        DELIVERABLES:{
          target: "deliverables",
          actions: assign({ overview: (_, e) => e.value })
        },
        SHIPPING:"shipping",
        PAYMENT:"payment",
        TERMS:"terms",
        NEXT: {
          target: "createAccount",
          actions: assign({ overview: (_, e) => e.value })
        }
      }
    },

    loading:{},

    offers: {
      on: {
        NEXT: {
          target: "products",
          actions: assign({ offers: (_, e) => e.value })
        },
        CLOSE: {
          target: "overview",
          actions: assign({ offers: (_, e) => e.value })
        }
      }
    },

    products: {
      on: {
        NEXT: {
          target: "deliverables",
          actions: assign({ products: (_, e) => e.value })
        },
        BACK: {
          target: "offers",
          actions: assign({ products: (_, e) => e.value })
        },
        CLOSE: {
            target: "overview",
            actions: assign({ products: (_, e) => e.value })
        }
      }
    },
    deliverables: {
      on: {
        NEXT: {
          target: "shipping",
          actions: assign({ deliverables: (_, e) => e.value })
        },
        BACK: {
          target: "products",
          actions: assign({ deliverables: (_, e) => e.value })
        },
        CLOSE: {
            target: "overview",
            actions: assign({ deliverables: (_, e) => e.value })
        }
      }
    },
    shipping: {
      on: {
        NEXT: {
          target: "payment",
          actions: assign({ shipping: (_, e) => e.value })
        },
        BACK: {
          target: "deliverables",
          actions: assign({ shipping: (_, e) => e.value })
        },
        CLOSE: {
            target: "overview",
            actions: assign({ shipping: (_, e) => e.value })
          
        }
      }
    },
    payment: {
      on: {
        NEXT: {
          target: "terms",
          actions: assign({ payment: (_, e) => e.value })
        },
        BACK: {
          target: "shipping",
          actions: assign({ payment: (_, e) => e.value })
        },
        CLOSE: {
            target: "overview",
            actions: assign({ payment: (_, e) => e.value })
          
        }
      }
    },
    terms: {
      on: {
        NEXT: {
          target: "createAccount",
          actions: assign({ terms: (_, e) => e.value })
        },
        BACK: {
          target: "payment",
          actions: assign({ terms: (_, e) => e.value })
        },
        CLOSE: {
            target: "overview",
            actions: assign({ terms: (_, e) => e.value })
        }
      }
    },

    createAccount: {
      on: {
        NEXT: {
          target: "verifyEmail",
          actions: assign({ createAccount: (_, e) => e.value })
        },
        
      }
    },

    verifyEmail:{

    },

    partnership:{
      
    }
    
    
  }
});






