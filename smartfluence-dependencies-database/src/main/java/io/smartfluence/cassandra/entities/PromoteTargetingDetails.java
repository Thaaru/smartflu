package io.smartfluence.cassandra.entities;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("promote_targeting_details")
public class PromoteTargetingDetails implements Copyable {

	private static final long serialVersionUID = -2787846945207532662L;

	@PrimaryKey("campaign_id")
	private UUID campaignId;

	@Column("platfrom")
	private Platforms platform;

	@Column("comparison_profile")
	private String comparisonProfile;

	@Column("followers_range")
	private String followersRange;

	@Column("engagements_range")
	private String engagementsRange;

	@Column("audience_industry")
	private String audienceIndustry;

	@Column("influencer_industry")
	private String influencerIndustry;

	@Column("audience_geo")
	private String audienceGeo;

	@Column("influencer_geo")
	private String influencerGeo;

	@Column("audience_age")
	private String audienceAge;

	@Column("influencer_age")
	private String influencerAge;

	@Column("audience_gender")
	private String audienceGender;

	@Column("influencer_gender")
	private String influencerGender;

	@Column("additional_info")
	private Integer additionalInfo;

	@Indexed
	@Column("created_at")
	private Date createdAt;

	@Column("modified_at")
	private Date modifiedAt;
}