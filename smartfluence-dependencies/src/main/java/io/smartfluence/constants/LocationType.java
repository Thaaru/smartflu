package io.smartfluence.constants;

public enum LocationType {
	COUNTRY, STATE, CITY
}