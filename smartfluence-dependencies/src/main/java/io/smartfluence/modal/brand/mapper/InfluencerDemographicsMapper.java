package io.smartfluence.modal.brand.mapper;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerDemographicsMapper {

	private String influencerHandle;

	private UUID userId;

	private String url;

	private String profileImage;

	private String email;

	private Double male;

	private Double female;

	private Double age13To17;

	private Double age18To24;

	private Double age25To34;

	private Double age35To44;

	private Double age45To64;

	private Double age65ToAny;

	private Double engagement;
	
	private Double engagementRate;

	private Double followers;

	private Double credibility;

	private Double pricing;

	private Double likes;

	private Double comments;

	private String platform;

	private String influencerName;

	@Default
	private Double views = 0.0;

	@Default
	private Double disLikes = 0.0;

	@Default
	private boolean saveReportHistory = true;

	public Double getMale() {
		return male == null ? 0 : male;
	}

	public Double getFemale() {
		return female == null ? 0 : female;
	}

	public Double getAge13To17() {
		return age13To17 == null ? 0 : age13To17;
	}

	public Double getAge18To24() {
		return age18To24 == null ? 0 : age18To24;
	}

	public Double getAge25To34() {
		return age25To34 == null ? 0 : age25To34;
	}

	public Double getAge35To44() {
		return age35To44 == null ? 0 : age35To44;
	}

	public Double getAge45To64() {
		return age45To64 == null ? 0 : age45To64;
	}

	public Double getAge65ToAny() {
		return age65ToAny == null ? 0 : age65ToAny;
	}

	public Double getEngagement() {
		return engagement == null ? 0 : engagement;
	}

	public Double getFollowers() {
		return followers == null ? 0 : followers;
	}

	public Double getCredibility() {
		return credibility == null ? 0 : credibility;
	}
}