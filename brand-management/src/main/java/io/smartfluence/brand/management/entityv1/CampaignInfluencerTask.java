package io.smartfluence.brand.management.entityv1;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "campaign_influencer_tasks_v1")
public class CampaignInfluencerTask {
    @EmbeddedId
    CampaignInfluencerTaskId citId;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "campaignTaskId" ,insertable = false,updatable = false)
    CampaignTasksV1 campaignTasks;

}
