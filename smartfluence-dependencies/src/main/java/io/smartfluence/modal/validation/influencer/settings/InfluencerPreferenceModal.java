package io.smartfluence.modal.validation.influencer.settings;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerPreferenceModal {

	@NotBlank(message = "Write Something about yourself")
	private String description;

	@NotBlank(message = "Please select Industry")
	private String industry;

	@NotBlank(message = "Please Select the Location")
	private String location;

	@NotNull
	@DecimalMin(message = "Please enter the price", value = "0.00")
	private Double instaStory2Hr;

	@NotNull
	@DecimalMin(message = "Please enter the price", value = "0.00")
	private Double instaStory24Hr;

	@NotNull
	@DecimalMin(message = "Please enter the price", value = "0.00")
	private Double instaPost24Hr;

	@NotNull
	@DecimalMin(message = "Please enter the price", value = "0.00")
	private Double instaPost1W;

	@NotNull
	@DecimalMin(message = "Please enter the price", value = "0.00")
	private Double instaPostPerm;
}