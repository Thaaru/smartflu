package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerPriceKey implements Serializable {

	private static final long serialVersionUID = -152632305585671937L;

	public InfluencerPriceKey(InfluencerPriceKey key) {
		this.userId = key.getUserId();
		this.platform = key.getPlatform();
		this.type = key.getType();
		this.duration = key.getDuration();
	}

	@PrimaryKeyColumn(name = "user_id", type = PrimaryKeyType.PARTITIONED)
	private UUID userId;

	@PrimaryKeyColumn(name = "platform", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private String platform;

	@PrimaryKeyColumn(name = "type", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private String type;

	@PrimaryKeyColumn(name = "duration", type = PrimaryKeyType.CLUSTERED, ordinal = 3)
	private String duration;

}
