package io.smartfluence.modal.social.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudienceGeo implements Serializable {

	private static final long serialVersionUID = -2385891740175472106L;

	@JsonAlias("countries")
	private List<NameAndWeight> countries = new ArrayList<>();

	@JsonAlias("states")
	private List<NameAndWeight> states = new ArrayList<>();
}