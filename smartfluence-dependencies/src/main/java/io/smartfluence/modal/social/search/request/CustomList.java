package io.smartfluence.modal.social.search.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomList {

	@JsonProperty("id")
	private Integer id;

	@JsonProperty("action")
	private String action;
}