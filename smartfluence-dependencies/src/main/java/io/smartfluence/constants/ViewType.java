package io.smartfluence.constants;

public enum ViewType {
	SEARCH, VIEW, DOWNLOAD, CAMPAIGN, BID, PROMOTE, UPLOAD
}