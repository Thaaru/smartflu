package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.CampaignTasksV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CampaignTasksRepositoryV1 extends JpaRepository<CampaignTasksV1,Long> {
    List<CampaignTasksV1> findByCampaignId(String campaignId);
    void deleteByTaskId(long taskId);
}
