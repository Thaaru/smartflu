package io.smartfluence.brand.management.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.http.ResponseEntity;

import io.smartfluence.cassandra.entities.TemporaryInfluencerContent;
import io.smartfluence.cassandra.entities.brand.BrandCreditAllowance;
import io.smartfluence.cassandra.entities.brand.BrandCreditHistory;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSearchHistory;
import io.smartfluence.cassandra.entities.brand.primary.BrandSearchHistoryKey;
import io.smartfluence.cassandra.entities.instagram.InstagramContent;
import io.smartfluence.cassandra.entities.instagram.InstagramCountryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.instagram.InstagramIndustryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramStateDemographic;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.entities.tiktok.TikTokContent;
import io.smartfluence.cassandra.entities.tiktok.TikTokCountryDemographic;
import io.smartfluence.cassandra.entities.tiktok.TikTokDemographics;
import io.smartfluence.cassandra.entities.youtube.YouTubeContent;
import io.smartfluence.cassandra.entities.youtube.YouTubeCountryDemographic;
import io.smartfluence.cassandra.entities.youtube.YouTubeDemographics;
import io.smartfluence.constants.CreditType;
import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.brand.dashboard.MailTemplates;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.social.search.response.InfluencersDataResponse;
import io.smartfluence.modal.validation.brand.explore.CampaignDetail;
import io.smartfluence.modal.validation.brand.explore.GetInfluencersRequestModel;
import io.smartfluence.modal.validation.brand.explore.SubmitBidModel;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.CampaignInfluencerMapping;
import io.smartfluence.mysql.entities.LocationMaster;
import io.smartfluence.mysql.entities.IndustryMaster;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;

public interface ExploreInfluencerDao {

	Optional<UserAccounts> getUserAccounts(String brandId);

	Optional<InstagramDemographics> getInstagramDemographics(String influencerHandle);

	Optional<CampaignInfluencerMapping> getCampaignInfluencerMapping(String campaignId,	String influencerId);

	Optional<SocialIdMaster> getSocialIdMasterById(String userId);

	Optional<SocialIdMaster> getSocialIdMasterByHandleAndPlatform(String influencerHandle, Platforms platform);

	Optional<SocialIdMaster> getSocialIdMasterByInfluencerId(String influencerId);

	ResponseEntity<Object> submitBid(SubmitBidModel submitBidModel);

	ResponseEntity<Object> sendProposal(SubmitBidModel submitBidModel);

	ResponseEntity<Object> addToCampaign(CampaignDetail addToCampaign);


	ResponseEntity<Object> getCampaignByInfluencer();

	void truncateTemporaryContentTable();

	List<MailTemplates> getMailTemplates();

	default void saveTemporaryInfluencerContent(TemporaryInfluencerContent temporaryInfluencerContent) {

	}

	default Optional<TemporaryInfluencerContent> getTemporaryInfluencerContent(String influencerId, boolean isProfilePic, int contentRank) {
		return null;
	}

	default void saveBrandCredits(BrandCredits brandCredits) {

	}

	default void saveBrandCreditHistory(BrandCreditHistory BrandCreditHistory) {

	}

	default void saveBrandCreditAllowance(BrandCreditAllowance brandCreditAllowance) {

	}

	default Optional<BrandCredits> getBrandCredits(UUID brandId) {
		return null;
	}

	default Optional<BrandDetails> getBrandDetails(UserDetailsKey userDetailsKey) {
		return null;
	}

	default Optional<BrandCreditAllowance> getBrandCreditAllowance(UUID brandId, UUID influencerId, CreditType creditType) {
		return null;
	}

	default void processInfluencerToCampaign(Optional<BrandDetails> brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			InfluencerDemographicsMapper influencerDemographics) {
	}

	default void saveInstagramDemographics(InstagramDemographics instagramDemographics) {

	}

	default void saveInstagramContents(List<InstagramContent> instagramContent) {

	}

	default List<InstagramCountryDemographic> getAllInstagramCountryByHandle(String userName) {
		return null;
	}

	default List<InstagramIndustryDemographic> getAllInstagramIndustryByHandle(String userName) {
		return null;
	}

	default List<InstagramStateDemographic> getAllInstagramStateByHandle(String userName) {
		return null;
	}

	default Optional<YouTubeDemographics> getYouTubeDemographics(String influencerHandle) {
		return null;
	}

	default List<YouTubeCountryDemographic> getAllYoutubeCountryByHandle(String userName) {
		return null;
	}

	default Optional<TikTokDemographics> getTikTokDemographics(String influencerHandle) {
		return null;
	}

	default List<TikTokCountryDemographic> getAllTiktokCountryByHandle(String userName) {
		return null;
	}

	default void saveYoutubeDemographics(YouTubeDemographics youtubeDemographics) {

	}

	default void saveYoutubeContents(List<YouTubeContent> youtubeContent) {

	}

	default void saveTiktokDemographics(TikTokDemographics tiktokDemographics) {

	}

	default void saveTiktokContents(List<TikTokContent> tiktokContent) {

	}

	default Optional<BrandSearchHistory> getBrandSearchHistoryById(BrandSearchHistoryKey key) {
		return null;
	}

	default void saveBrandSearchHistory(BrandSearchHistory brandSearchHistory) {

	}

	default UUID saveBrandSearchHistory(UUID brandId, GetInfluencersRequestModel request, InfluencersDataResponse body) {
		return null;
	}

	default Optional<LocationMaster> getLocationByName(String name) {
		return null;
	}

	default List<LocationMaster> getLocationUtilities(String name) {
		return null;
	}

	default List<IndustryMaster> getIndustryUtilities(String name) {
		return null;
	}

	default Optional<IndustryMaster> getIndustryByName(String name) {
		return null;
	}
}