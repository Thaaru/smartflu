@ECHO off
@echo Hello
SETLOCAL

set _CD=%CD%
cls
:start
ECHO.

ECHO SMARTFLUENCE
ECHO 0 smartfluence-parent
ECHO 1 smartfluence-dependencies
ECHO 2 dependency-database
ECHO 3 discovery
ECHO 4 accounts
ECHO 5 admin
ECHO 6 brand
ECHO 7 webapp
ECHO 8 gateway
ECHO 9 influencer
ECHO * all

set choice=
set /p choice=Please Select one of the above options :
if not '%choice%'=='' set choice=%choice:~0,1%

ECHO '%choice%'
if '%choice%'=='0' goto smartfluence-parent
if '%choice%'=='1' goto smartfluence-dependencies
if '%choice%'=='2' goto dependency-database
if '%choice%'=='3' goto discovery
if '%choice%'=='4' goto accounts
if '%choice%'=='5' goto admin
if '%choice%'=='6' goto brand
if '%choice%'=='7' goto webapp
if '%choice%'=='8' goto gateway
if '%choice%'=='9' goto influencer
if '%choice%'=='*' goto all

ECHO "%choice%" is not valid, try again
ECHO.
goto end

:smartfluence-parent
@echo Building smartfluence-parent
cd %_CD%\smartfluence-parent\
call mvnw  clean install -Dmaven.test.skip=true
goto end

:smartfluence-dependencies
@echo Building smartfluence-dependencies
cd %_CD%\smartfluence-dependencies\
call mvnw  clean install -Dmaven.test.skip=true
goto end

:dependency-database
@echo Building smartfluence-dependencies-database
cd %_CD%\smartfluence-dependencies-database\
call mvnw  clean install -Dmaven.test.skip=true
goto end

:discovery
@echo Building smartfluence-discovery
cd %_CD%\smartfluence-discovery\
call mvnw  clean install -Dmaven.test.skip=true
goto end

:accounts
@echo Building smartfluence-accounts
cd %_CD%\smartfluence-accounts\
call mvnw  clean install -Dmaven.test.skip=true
goto end

:admin
@echo Building smartfluence-admin-management
cd %_CD%\smartfluence-admin-management\
call mvnw  clean install -Dmaven.test.skip=true
goto end

:brand
@echo Building brand-management
cd %_CD%\brand-management\
call mvnw  clean install -Dmaven.test.skip=true
goto end 

:webapp
@echo Building webapp
cd %_CD%\smartfluence\
call mvnw  clean install -Dmaven.test.skip=true
goto end 

:influencer
@echo Building smartfluence-influencer
cd %_CD%\smartfluence-influencer-management\
call mvnw  clean install -Dmaven.test.skip=true
goto end

:gateway
@echo Building smartfluence-gateway
cd %_CD%\smartfluence-gateway\
call mvnw  clean install -Dmaven.test.skip=true
goto end

:all
@echo Building All modules

@echo Building smartfluence-dependencies
cd %_CD%\smartfluence-dependencies\
call mvnw  clean install -Dmaven.test.skip=true

@echo Building smartfluence-dependencies-database
cd %_CD%\smartfluence-dependencies-database\
call mvnw  clean install -Dmaven.test.skip=true

@echo Building smartfluence-discovery
cd %_CD%\smartfluence-discovery\
call mvnw  clean install -Dmaven.test.skip=true

@echo Building smartfluence-accounts
cd %_CD%\smartfluence-accounts\
call mvnw  clean install -Dmaven.test.skip=true

@echo Building smartfluence-admin-management
cd %_CD%\smartfluence-admin-management\
call mvnw  clean install -Dmaven.test.skip=true

@echo Building brand-management
cd %_CD%\brand-management\
call mvnw  clean install -Dmaven.test.skip=true

@echo Building webapp
cd %_CD%\smartfluence\
call mvnw  clean install -Dmaven.test.skip=true

@echo Building smartfluence-gateway
cd %_CD%\smartfluence-gateway\
call mvnw  clean install -Dmaven.test.skip=true

goto end

cd %_CD%\
@echo Build successful..!!
:end
pause