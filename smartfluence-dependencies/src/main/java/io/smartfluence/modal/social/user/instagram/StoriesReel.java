package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StoriesReel implements Serializable{

	private static final long serialVersionUID = 6628289153902679970L;

	@JsonAlias("latest_reel_media")
	private Long latestReelMedia;

	@JsonAlias("expiring_at")
	private Long expiringAt;

	@JsonAlias("media_count")
	private Integer mediaCount;

	@JsonAlias("items")
	private List<StoriesItems> items;
}