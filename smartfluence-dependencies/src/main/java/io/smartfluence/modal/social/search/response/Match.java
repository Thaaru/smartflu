package io.smartfluence.modal.social.search.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Match implements Serializable {

	private static final long serialVersionUID = 8673564278030891828L;

	@JsonAlias("audience_likers")
	private AudienceLikers audienceLikers;
}