package io.smartfluence.brand.management.service;

import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.modal.validation.brand.dashboard.DashBoardModal;
import io.smartfluence.modal.validation.brand.dashboard.GetInfluencerBidCountModal;
import io.smartfluence.modal.validation.brand.dashboard.MailTemplateModal;
import io.smartfluence.modal.validation.brand.dashboard.RemoveInfluencerFromCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SaveCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SavePostLink;
import io.smartfluence.modal.validation.brand.dashboard.SaveStatusModel;
import io.smartfluence.modal.validation.brand.dashboard.UploadInfluencersRequestModel;

public interface DashboardService {

	ResponseEntity<Object> getInfluencerSnapshot(UUID campaignId);

	ResponseEntity<Object> getCreditBalance();

	ResponseEntity<Object> saveCampaign(SaveCampaignModel saveCampaignModel);

	ResponseEntity<Object> getCampaign(boolean isAllCampaign);

	ResponseEntity<Object> getInfluencerByCampaign(UUID campaignId);

	ResponseEntity<Object> saveStatus(SaveStatusModel saveStatusModel, UUID campaignId);

	@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
	ResponseEntity<Object> removeInfluencer(RemoveInfluencerFromCampaignModel removeInfluencerFromCampaignModel, UUID campaignId);

	ResponseEntity<Object> getCampaignName(UUID campaignId);

	ResponseEntity<Object> savePostLink(SavePostLink savePostLink);

	ResponseEntity<Object> validatePostLinkNew(SavePostLink savePostLink);

	ResponseEntity<Object> getCampaignPostLinkDetails(UUID campaignId);

	ResponseEntity<Object> getInfluencerBidCount(GetInfluencerBidCountModal campaignBidCountModal, UUID campaignId);

	ResponseEntity<Object> saveMailTemplate(MailTemplateModal mailTemplateModal);

	ResponseEntity<Object> sendBulkMail(DashBoardModal dashBoardModal);

	ResponseEntity<Object> sendBulkMailNew(DashBoardModal dashBoardModal);

	ResponseEntity<Object> uploadInfluencers(UploadInfluencersRequestModel uploadInfluencersModel, UUID campaignId);

	ResponseEntity<Object> deleteCampaign(UUID campaignId);

	ResponseEntity<Object> copyInfluencers(DashBoardModal dashBoardModal);

	ResponseEntity<Object> editCampaignName(DashBoardModal dashBoardModal);
}