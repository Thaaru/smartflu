package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.CurrencyMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyMasterRepo extends JpaRepository<CurrencyMaster, Long> {
}
