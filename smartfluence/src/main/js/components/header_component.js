import React from "react";
import { BsXCircleFill } from 'react-icons/bs'

export const HeaderComponent = ({data,headerName,stepNo,close}) =>{
    
    return (
                <div  style={{ "textAlign": "left" }}>
                    <div className="col-lg" style={{"marginBottom":"10px"}}>
                        <div className="row">
                            <div className="col normalText" style={{ "fontWeight": "inherit" }}>Step {stepNo} of 6</div>
                            <div className="ml-auto mr-3"  ><BsXCircleFill size={25} onClick={close}/></div>
                        </div>
                    </div>
                    <div class = "col-lg">
                    <p className="heading">{headerName}</p>
                    </div>
                </div>
    );
}