//package io.smartfluence.webapp.controller.rest.entity;
//
//import lombok.*;
//
//import javax.validation.constraints.NotBlank;
//import java.util.UUID;
//
//@Getter
//@Setter
//@Builder
//@ToString
//@EqualsAndHashCode
//@NoArgsConstructor
//@AllArgsConstructor
//public class CampaignInfluencerReqEntity {
//    @NotBlank(message = "Enter campaign name")
//    private String campaignName;
//
//    private UUID campaignId;
//
//    @NotBlank(message = "Please provide influencerId")
//    private String influencerId;
//
//    private String reportId;
//    private String influencerEmail;
//
//
//    @NotBlank(message = "Please provide influencerHandle")
//    private String influencerHandle;
//
//    private String platform;
//
//    private boolean reportPresent;
//
//    private boolean isValid;
//
//    public boolean getIsValid() {
//        return isValid;
//    }
//
//    public void setIsValid(boolean isValid) {
//        this.isValid = isValid;
//    }
//
//    public boolean getReportPresent() {
//        return reportPresent;
//    }
//
//    public void setReportPresent(boolean reportPresent) {
//        this.reportPresent = reportPresent;
//    }
//
//}