package io.smartfluence.webapp.controller.rest.brand;

import io.smartfluence.modal.brand.campaignv1.CampaignEntityV1;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

//@ControllerAdvice
//public class FileErrorController  extends ResponseEntityExceptionHandler {
//    @ExceptionHandler(MultipartException.class)
//    @ResponseBody
//    ResponseEntity<CampaignEntityV1> handleFileException(HttpServletRequest request, Throwable ex) {
//        //return your json insted this string.
//     return   getErrorResp("File shouldn't exceed 10 MB");
//    }
//
//    public ResponseEntity<CampaignEntityV1> getErrorResp(String message){
//        CampaignEntityV1 campaignError = new CampaignEntityV1();
//        Map errorsObj = new HashMap<String, String>();
//        errorsObj.put("campaignError", message);
//        campaignError.setErrorObj(errorsObj);
//        return ResponseEntity.badRequest().body(campaignError);
//    }
//}
