package io.smartfluence.constants;

public enum UserType {
	SMART_ADMIN, ADMIN, BRAND, INFLUENCER
}