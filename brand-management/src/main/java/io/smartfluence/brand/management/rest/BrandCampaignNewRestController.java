package io.smartfluence.brand.management.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import feign.Headers;
import io.smartfluence.brand.management.entity.*;
import io.smartfluence.brand.management.mailbean.MailService;
import io.smartfluence.brand.management.service.CreateCampaignService;
import io.smartfluence.brand.management.servicev1.CampaignEntityServiceV1;
import io.smartfluence.brand.management.servicev1.CampaignServiceV1;
import io.smartfluence.brand.management.servicev1.StorageService;
import io.smartfluence.modal.brand.campaignv1.CampaignEntityV1;
import io.smartfluence.modal.brand.campaignv1.InfluObj;
import io.smartfluence.modal.brand.campaignv1.WorkflowReqObj;
import io.smartfluence.modal.utill.ResponseObj;
import io.smartfluence.brand.management.entityv1.DashBoardModalNew;
import io.smartfluence.brand.management.entityv1.UploadInfluencersRequestModelNew;
import io.smartfluence.modal.validation.brand.explore.CampaignInfluencerReqEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("brand")
public class BrandCampaignNewRestController {

    @Autowired
    private CreateCampaignService createCampaignService;

    @Autowired
    private CampaignServiceV1 campaignServiceV1;

    @Autowired
    private CampaignEntityServiceV1 campaignEntityServiceV1;

    @Autowired
    StorageService storageService;

    @Autowired
    MailService mailService;

    final static Logger LOG = Logger.getLogger(BrandCampaignNewRestController.class.getName());


    @GetMapping("view-campaign-v1/{id}")
    public CampaignEntityV1 getCampaignEntityByCampaignId(@PathVariable String id) {
        return campaignServiceV1.getCampaignEntityByCampaignId(id).getBody();
    }


    @GetMapping("new-campaign")
    public CampaignEntityV1 getCampaignForm() {
        return campaignServiceV1.getCampaignEntityForm();
    }

    @DeleteMapping("delete-campaign")
    public ResponseObj deleteCampaignById(@RequestParam("id") String campaignId,
                                          @RequestParam("status") String status) {
        return campaignServiceV1.deleteCampaignById(campaignId, status);
    }


    @PostMapping("saveNewCampaign")
    public ResponseEntity<Object> saveCampaignNew(@RequestBody FullCampaignDetailEntity fullCampaignDetailEntity, BindingResult bindingResult) {
        if (!bindingResult.hasFieldErrors())
            return new ResponseEntity<>(createCampaignService.saveNewCampaign(fullCampaignDetailEntity), HttpStatus.OK);

        return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);

    }

    @GetMapping("/getCampaigsnById/{campaignId}")
    public ResponseEntity<Object> getCampaignById(@PathVariable("campaignId") UUID campaignId) {
        return createCampaignService.getCampaignById(campaignId);

    }

    @PostMapping("launchNewCampaign")
    public ResponseEntity<Object> launchCampaign(@Validated @RequestBody LaunchFullCampaignEntity launchFullCampaignEntity, BindingResult bindingResult) {
        if (!bindingResult.hasFieldErrors())
            return new ResponseEntity<>(createCampaignService.launchNewCampaign(launchFullCampaignEntity), HttpStatus.OK);
        return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("view-influencer/add-to-new-campaign")
    public ResponseEntity<Object> addInfluencerToNewCampaign(@RequestBody List<CampaignInfluencerReqEntity> campaignInfluencerReqEntity) {
        return createCampaignService.addInfluencerToNewCampaign(campaignInfluencerReqEntity);
    }

    @GetMapping("get-new-campaign")
    public ResponseEntity<Object> getNewCampaign() {
        return campaignServiceV1.getNewCampaign();
    }

    @GetMapping("get-all-new-campaign")
    ResponseEntity<Object> getAllNewCampaignWithInfluencer() {
        return ResponseEntity.ok(campaignServiceV1.getCampaignInfluencerAndCampaignStatus());
    }

    @GetMapping("campaign/{campaignId}/influencers")
    ResponseEntity<Object> getAllInfluencerByCampaignId(@PathVariable("campaignId") String campaignId) {
        return campaignServiceV1.getAllInfluencerByCampaignId(campaignId);
    }


    @GetMapping("view-campaign-v1/view-influencers")
    public ResponseObj getInfluencerByCampaignId(@RequestParam String id,@RequestParam List<String> proposalStatus){
       LOG.log(Level.INFO,id,proposalStatus);
        return campaignServiceV1.getInfluencersByCampaignId(id,proposalStatus);
    }

    @PutMapping("view-campaign-v1/view-influencers/updateoffer")
    public ResponseObj updateInfluencerOffers(@RequestBody InfluObj influObj) {
        LOG.log(Level.INFO, influObj.toString());
        return campaignServiceV1.updateInfluObj(influObj);
    }

    @DeleteMapping("remove-influencer")
    public ResponseEntity<Object> removeNewInfluencer(@RequestBody RemoveInfluencerReq removeInfluencerReq) {
        return campaignServiceV1.removeNewInfluencer(removeInfluencerReq);
    }

    @PostMapping("view-campaign/copy-new-influencers")
    public ResponseEntity<Object> copyNewInfluencers(@Validated @RequestBody DashBoardModalNew dashBoardModal, BindingResult bindingResult) {
        return campaignServiceV1.copyNewInfluencers(dashBoardModal);
    }

    @GetMapping("campaign/{campaignId}/influencers/export-campaign-csv-new")
    public ResponseEntity<String> exportCampaignCsvNew(@PathVariable UUID campaignId) {
        return campaignServiceV1.exportCampaignCsvNew(campaignId);
    }

    @GetMapping("campaign/{campaignId}/influencers/download-campaign-csv-new/{csvName}")
    public ResponseEntity<byte[]> downloadCampaignCsvNew(@PathVariable UUID campaignId, @PathVariable UUID csvName) {
        return campaignServiceV1.downloadCampaignCsvNew(campaignId, csvName);
    }

    @PostMapping("campaign/{campaignId}/influencers/upload-bulk-influencers-new")
    public ResponseEntity<Object> uploadNewInfluencers(@Validated @ModelAttribute UploadInfluencersRequestModelNew uploadInfluencersModel,
                                                       @PathVariable UUID campaignId) {
        return campaignServiceV1.uploadNewInfluencers(uploadInfluencersModel, campaignId);
    }

    @GetMapping("campaign/download-template/upload-influencers-new")
    public ResponseEntity<byte[]> downloadTemplate() {
        return campaignServiceV1.downloadNewTemplate();
    }


    @GetMapping("brand/view-campaign-v1/get-campaign-influencer")
    public ResponseEntity<Object> getCampaignInfluencerById(String campaignId) {
        return ResponseEntity.ok(campaignEntityServiceV1.getCampaignById(campaignId));
    }
    
    @PostMapping("view-campaign-v1/view-influencers/sendproposal")
    public ResponseObj sendProposal(@RequestBody WorkflowReqObj workflowReqObj){
        return campaignServiceV1.sendProposal(workflowReqObj);
    }

    @PutMapping("view-campaign-v1/view-influencers/brandapproval")
    public ResponseObj brandApproval(@RequestBody WorkflowReqObj workflowReqObj){
        return campaignServiceV1.brandApprovals(workflowReqObj);
    }

    @PutMapping("view-campaign-v1/update-campaign-name")
    public ResponseObj updateCampaignName(@RequestParam String campaignId,@RequestParam String campaignName){
        return campaignServiceV1.editCampaignName(campaignId,campaignName);
    }

    @GetMapping("view-campaign-v1/validate-campaign-name")
    public ResponseObj validateCampaignName(@RequestParam String campaignName,@RequestParam String campaignId){
        return campaignServiceV1.validateCampaignName(campaignName,campaignId);
    }

    @PostMapping(value = "save-campaign-v1-file",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<CampaignEntityV1> saveCampaignNewFile(@RequestParam String campaignEntityV1,
                                            @RequestPart MultipartFile file) {
        ObjectMapper mapper = new ObjectMapper();
        try{

            CampaignEntityV1 campaignEntity= mapper.readValue(campaignEntityV1,CampaignEntityV1.class);
            return campaignServiceV1.saveCampaign(campaignEntity,file);
        }
        catch (Exception e) {
            e.printStackTrace();
            mailService.sendErrorMailDetail(e);
            LOG.log(Level.SEVERE,e.toString());
            LOG.log(Level.SEVERE,e.getMessage());
        }
        CampaignEntityV1 campaignError = new CampaignEntityV1();
        Map errorsObj = new HashMap<String, String>();
        errorsObj.put("campaignError","something went wrong");
        campaignError.setErrorObj(errorsObj);
        return ResponseEntity.ok(campaignError);
    }

    @PostMapping(value = "save-campaign-v1")
    public ResponseEntity<CampaignEntityV1> saveCampaignNew(@RequestParam String campaignEntityV1) {
        ObjectMapper mapper = new ObjectMapper();
        try{

            CampaignEntityV1 campaignEntity= mapper.readValue(campaignEntityV1,CampaignEntityV1.class);
            return campaignServiceV1.saveCampaign(campaignEntity,null);
        }
        catch (Exception e) {
            e.printStackTrace();
            mailService.sendErrorMailDetail(e);
            LOG.log(Level.SEVERE,e.toString());
            LOG.log(Level.SEVERE,e.getMessage());
        }
        CampaignEntityV1 campaignError = new CampaignEntityV1();
        Map errorsObj = new HashMap<String, String>();
        errorsObj.put("campaignError","something went wrong");
        campaignError.setErrorObj(errorsObj);
        return ResponseEntity.ok(campaignError);
    }

    @GetMapping(value = "gettermscondition/{filename}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable String filename){
        byte[] data = storageService.downloadFile(filename);
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
                .ok()
                .contentLength(data.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + filename + "\"")
                .body(resource);
    }

    @DeleteMapping(value = "delete-termscondition")
    public ResponseEntity<ResponseObj> deleteTermsAndCondition(@RequestParam String campaignId,@RequestParam long termId){
      return  ResponseEntity.ok(campaignServiceV1.deleteTermsCondition(campaignId,termId));
    }

}

