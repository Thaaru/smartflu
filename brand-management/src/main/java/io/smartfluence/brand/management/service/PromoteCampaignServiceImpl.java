package io.smartfluence.brand.management.service;

import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.dao.DashboardDao;
import io.smartfluence.brand.management.dao.ExploreInfluencerDao;
import io.smartfluence.brand.management.dao.PromoteCampaignDao;
import io.smartfluence.brand.management.mailbean.MailService;
import io.smartfluence.brand.management.util.SendMailUtil;
import io.smartfluence.cassandra.entities.PromoteInfluencersData;
import io.smartfluence.cassandra.entities.PromoteTargetingDetails;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.primary.PromoteInfluencersDataKey;
import io.smartfluence.cassandra.entities.primary.ReportSearchHistoryKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.PromoteInfluencerStatus;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.constants.ViewType;
import io.smartfluence.modal.brand.exploreinfluencer.GetInfluencersResponseModel;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.brand.promote.PromoteCampaignResponseModel;
import io.smartfluence.modal.brand.promote.PromoteCampaignResponseModel.PromoteCampaignResponseModelBuilder;
import io.smartfluence.modal.social.customlist.AddUsersToList;
import io.smartfluence.modal.social.customlist.ManageCustomList;
import io.smartfluence.modal.social.user.InfluencerContactDetails;
import io.smartfluence.modal.validation.brand.explore.GetInfluencersRequestModel;
import io.smartfluence.modal.validation.brand.promote.PromoteCampaignRequestModel;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.MailDetails;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.proxy.social.SocialDataProxy;
import io.smartfluence.util.DateUtil;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Component
public class PromoteCampaignServiceImpl implements PromoteCampaignService {

	@Autowired
	private MailService mailService;

	@Autowired
	private SocialDataProxy socialDataProxy;

	@Autowired
	private PromoteCampaignDao promoteCampaignDao;

	@Autowired
	private ExploreInfluencerService exploreInfluencerService;

	@Autowired
	private ExploreInfluencerDao exploreInfluencerDao;

	@Autowired
	private DashboardDao dashboardDao;

	@Autowired
	private SendMailUtil sendMailUtil;

	@Autowired
	private ReportsService reportsService;

	@Value("${promote.influencers.limit}")
	private int limitCount;

	@Value("${promote.cron.execute.days}")
	private int nextTrigger;

	@Value("${promote.influencer.threshold}")
	private int thresholdLimit;

	private static final String START = "start";

	private static final String RESUME = "resumed";

	private static final String EMAIL = "email";

	private static final String APPROVE = "approved";

	private static final String REMOVE = "removed";

	private static final String REJECT = "rejected";

	private static final String CUSTOM_LIST_ACTION = "not";

	private static final String SORT_BY_FOLLOWERS = "followers";

	private static final String SORT_BY_AUDIENCE = "audience_relevance";

	private static final String INITIALOUTREACHTEMPLATE = "InitialOutreachToInfluencers";

	private static final String INFLUENCERAPPROVEDTEMPLATE = "InfluencerApproved";

	private static final String INFLUENCERREJECTEDTEMPLATE = "InfluencerRejected";

	private static final String CHANGETARGETINGDETAILSTEMPLATE = "ChangeTargetingDetails";

	private static final String CAMPAIGN_DESCRIPTION = "<Campaign_Description>";

	private static final String BRANDNAME = "<Brand_Name>";

	private static final String INFLUENCERNAME = "<Influencer_Username>";

	private static final String PAYMENT = "<Payment>";

	@Override
	public ResponseEntity<Object> getPromoteCampaigns() {
		log.info("Request to get PromoteCampaigns for Brand");

		if (!ResourceServer.isFreeBrand()) {
			List<PromoteCampaignResponseModel> promoteCampaignResponseModel = Lists.newLinkedList();
			List<BrandCampaignDetailsMysql> listBrandCampaignDetailsMysql = promoteCampaignDao
					.getBrandCampaignDetailsMysqlByBrandIdAndIsPromote(ResourceServer.getUserId().toString(), true);

			if (!listBrandCampaignDetailsMysql.isEmpty())
				setPromoteCampaignsData(listBrandCampaignDetailsMysql, promoteCampaignResponseModel);

			return new ResponseEntity<>(promoteCampaignResponseModel, HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Please subscribe for Higher plan to view promote campaign");
	}

	private void setPromoteCampaignsData(List<BrandCampaignDetailsMysql> listBrandCampaignDetailsMysql,
			List<PromoteCampaignResponseModel> promoteCampaignResponseModel) {
		listBrandCampaignDetailsMysql.forEach(data -> {
			List<PromoteInfluencersData> listPromoteInfluencersData = promoteCampaignDao.
				getPromoteInfluencersDataByCampaignId(UUID.fromString(data.getCampaignId()))
					.stream().filter(p -> p.getStatus().equals(PromoteInfluencerStatus.PENDING)).collect(Collectors.toList());

			promoteCampaignResponseModel.add(PromoteCampaignResponseModel.builder().campaignId(data.getCampaignId())
					.activeUntil(DateUtil.SDF_MM_DD_YYYY.format(data.getActiveUntil()))
					.pendingApproval(listPromoteInfluencersData.isEmpty() ? "NO" : String.valueOf(listPromoteInfluencersData.size()))
					.campaignName(data.getCampaignName()).campaignStatus(data.getCampaignStatus().toString()).build());
		});
	}

	@Override
	public ResponseEntity<Object> getPromoteCampaignDetails(UUID campaignId, String pageType) {
		log.info("Request to get PromoteCampaign details");

		if (!StringUtils.isEmpty(campaignId.toString())) {
			PromoteCampaignResponseModelBuilder promoteCampaignResponseBuilder = PromoteCampaignResponseModel.builder();
			Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetailsMysql = promoteCampaignDao
					.getBrandCampaignDetailsMysqlByCampaignId(campaignId.toString());

			if (optionalBrandCampaignDetailsMysql.isPresent())
				setPromoteCampaignDetails(promoteCampaignResponseBuilder, optionalBrandCampaignDetailsMysql.get(), pageType);

			return new ResponseEntity<>(promoteCampaignResponseBuilder.build(), HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Campaign not available");
	}

	private void setPromoteCampaignDetails(PromoteCampaignResponseModelBuilder promoteCampaignResponseBuilder,
			BrandCampaignDetailsMysql brandCampaignDetailsMysql, String pageType) {

		getBrandCampaignDetails(brandCampaignDetailsMysql, promoteCampaignResponseBuilder, pageType);

		Optional<PromoteTargetingDetails> optionalPromoteTargetingDetails = promoteCampaignDao.
				getPromoteTargetingDetailsByCampaignId(UUID.fromString(brandCampaignDetailsMysql.getCampaignId()));

		if (optionalPromoteTargetingDetails.isPresent())
			getPromoteTargetingDetails(promoteCampaignResponseBuilder, optionalPromoteTargetingDetails.get());
	}

	private void getBrandCampaignDetails(BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			PromoteCampaignResponseModelBuilder promoteCampaignResponseBuilder, String pageType) {
		promoteCampaignResponseBuilder.campaignId(brandCampaignDetailsMysql.getCampaignId())
			.campaignName(brandCampaignDetailsMysql.getCampaignName()).brandDescription(brandCampaignDetailsMysql.getBrandDescription())
			.campaignDescription(brandCampaignDetailsMysql.getCampaignDescription()).postType(brandCampaignDetailsMysql.getPostType())
			.payment(brandCampaignDetailsMysql.getPayment()).product(brandCampaignDetailsMysql.getProduct()).pageType(pageType)
			.productValue(brandCampaignDetailsMysql.getProductValue()).requirements(brandCampaignDetailsMysql.getRequirements())
			.restrictions(brandCampaignDetailsMysql.getRestrictions()).activeUntil(DateUtil.SDF_YYYY_MM_DD.format(brandCampaignDetailsMysql.getActiveUntil()));
	}

	private void getPromoteTargetingDetails(PromoteCampaignResponseModelBuilder promoteCampaignResponseBuilder,
			PromoteTargetingDetails promoteTargetingDetails) {
		promoteCampaignResponseBuilder.comparisonProfile(promoteTargetingDetails.getComparisonProfile())
			.platform(promoteTargetingDetails.getPlatform().toString().toLowerCase()).followersRange(promoteTargetingDetails.getFollowersRange())
			.engagementsRange(promoteTargetingDetails.getEngagementsRange()).audienceIndustry(promoteTargetingDetails.getAudienceIndustry())
			.influencerIndustry(promoteTargetingDetails.getInfluencerIndustry()).audienceGeo(promoteTargetingDetails.getAudienceGeo())
			.influencerGeo(promoteTargetingDetails.getInfluencerGeo()).audienceAge(promoteTargetingDetails.getAudienceAge())
			.influencerAge(promoteTargetingDetails.getInfluencerAge()).audienceGender(promoteTargetingDetails.getAudienceGender())
			.influencerGender(promoteTargetingDetails.getInfluencerGender()).additionalInfo(promoteTargetingDetails.getAdditionalInfo());
	}

	@Override
	public ResponseEntity<Object> getPromoteCampaignInfluencers(UUID campaignId) {
		if (!StringUtils.isEmpty(campaignId.toString()))
			return new ResponseEntity<>(promoteCampaignDao.getPromoteInfluencersDataByCampaignId(campaignId)
					.stream().filter(f -> f.getStatus().equals(PromoteInfluencerStatus.PENDING) || f.getStatus().equals(PromoteInfluencerStatus.APPROVED)
							|| f.getStatus().equals(PromoteInfluencerStatus.REJECTED) || f.getStatus().equals(PromoteInfluencerStatus.CONTENT_PENDING)
							|| f.getStatus().equals(PromoteInfluencerStatus.CONTENT_APPROVED)).collect(Collectors.toList()), HttpStatus.OK);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Campaign not available");
	}

	@Override
	public ResponseEntity<Object> managePromoteCampaign(PromoteCampaignRequestModel request) {
		log.info("Create promote campaign, {}", request);

		if (!ResourceServer.isFreeBrand()) {
			try {
				request.setActiveDate(DateUtil.SDF_MM_DD_YYYY.parse(request.getActiveUntil()));
			
				if (request.getActiveDate().toInstant().isBefore(Instant.now()))
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ActiveUntil must be in the future");

				PromoteCampaignResponseModelBuilder promoteCampaignResponseModel = PromoteCampaignResponseModel.builder();
				if (!request.getIsEdit())
					return newPromoteCampaign(request, ResourceServer.getUserId(), promoteCampaignResponseModel);
				else
					return editPromoteCampaign(request, promoteCampaignResponseModel);
			} catch (ParseException e) {
				e.getStackTrace();
			}
		}
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Please subscribe for Higher plan to create promote campaign");
	}

	private ResponseEntity<Object> newPromoteCampaign(PromoteCampaignRequestModel request, UUID brandId, PromoteCampaignResponseModelBuilder response) {
		Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetailsMysql = promoteCampaignDao
				.getBrandCampaignDetailsMysqlByCampaignNameAndBrandId(request.getCampaignName(), brandId.toString());

		if (!optionalBrandCampaignDetailsMysql.isPresent()) {
			request.setCampaignId(UUID.randomUUID().toString());
			response.responseMessage("Campaign saved successfully").campaignId(request.getCampaignId());

			return new ResponseEntity<>(getMailTemplate(response, saveBrandCampaignDetailsMysql(request, brandId)), HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.CONFLICT).body("This Campaign Name Already Exists");
	}

	private ResponseEntity<Object> editPromoteCampaign(PromoteCampaignRequestModel request, PromoteCampaignResponseModelBuilder response) {
		if (!StringUtils.isEmpty(request.getCampaignId())) {
			Optional<BrandCampaignDetailsMysql> brandCampaignDetailsMysql = promoteCampaignDao
					.getBrandCampaignDetailsMysqlByCampaignId(request.getCampaignId());

			if (brandCampaignDetailsMysql.isPresent()) {
				updateBrandCampaignDetailsMysql(request, brandCampaignDetailsMysql.get());
				savePromoteTargetingDetails(request);
				response.responseMessage("Campaign saved successfully").campaignId(brandCampaignDetailsMysql.get().getCampaignId());

				return new ResponseEntity<>(getMailTemplate(response, brandCampaignDetailsMysql.get()), HttpStatus.OK);
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Campaign not available");
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Campaign not available");
	}

	private BrandCampaignDetailsMysql saveBrandCampaignDetailsMysql(PromoteCampaignRequestModel request, UUID brandId) {
		BrandCampaignDetailsMysql brandCampaignDetailsMysql = new BrandCampaignDetailsMysql();
		brandCampaignDetailsMysql.setCampaignId(request.getCampaignId());
		brandCampaignDetailsMysql.setBrandId(brandId.toString());
		brandCampaignDetailsMysql.setCampaignName(request.getCampaignName());
		brandCampaignDetailsMysql.setCampaignStatus(CampaignStatus.NEW);
		brandCampaignDetailsMysql.setCreatedAt(new Date());
		brandCampaignDetailsMysql.setIsPromote(true);
		brandCampaignDetailsMysql.setInfluencerCount(0);
		updateBrandCampaignDetailsMysql(request, brandCampaignDetailsMysql);
		savePromoteTargetingDetails(request);

		return brandCampaignDetailsMysql;
	}

	private void updateBrandCampaignDetailsMysql(PromoteCampaignRequestModel request, BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		brandCampaignDetailsMysql.setBrandDescription(request.getBrandDescription());
		brandCampaignDetailsMysql.setCampaignDescription(request.getCampaignDescription());
		brandCampaignDetailsMysql.setPostType(request.getPostType());
		brandCampaignDetailsMysql.setPayment(request.getPayment());
		brandCampaignDetailsMysql.setProduct(request.getProduct());
		brandCampaignDetailsMysql.setProductValue(request.getProductValue());
		brandCampaignDetailsMysql.setRequirements(request.getRequirements());
		brandCampaignDetailsMysql.setRestrictions(request.getRestrictions());
		brandCampaignDetailsMysql.setModifiedAt(new Date());
		brandCampaignDetailsMysql.setActiveUntil(request.getActiveDate());

		if (brandCampaignDetailsMysql.getCampaignStatus().equals(CampaignStatus.INPROGRESS))
			brandCampaignDetailsMysql.setNextTrigger(setNextTriggerDate(new Date()));

		promoteCampaignDao.saveBrandCampaignDetailsMysql(brandCampaignDetailsMysql);
	}

	private void savePromoteTargetingDetails(PromoteCampaignRequestModel request) {
		PromoteTargetingDetails promoteTargetingDetails = new PromoteTargetingDetails();
		Optional<PromoteTargetingDetails> optionalPromoteTargetingDetails = promoteCampaignDao.
				getPromoteTargetingDetailsByCampaignId(UUID.fromString(request.getCampaignId()));

		if (optionalPromoteTargetingDetails.isPresent()) {
			promoteTargetingDetails = optionalPromoteTargetingDetails.get();
			promoteTargetingDetails.setModifiedAt(new Date());
		} else {
			promoteTargetingDetails.setCampaignId(UUID.fromString(request.getCampaignId()));
			promoteTargetingDetails.setCreatedAt(new Date());
		}
		setPromoteTargetingDetails(request, promoteTargetingDetails);
		promoteCampaignDao.savePromoteTargetingDetails(promoteTargetingDetails);
	}

	private void setPromoteTargetingDetails(PromoteCampaignRequestModel request,
			PromoteTargetingDetails promoteTargetingDetails) {
		promoteTargetingDetails.setPlatform(reportsService.getPlatform(request.getPlatform()));
		promoteTargetingDetails.setComparisonProfile(request.getComparisonProfile());
		promoteTargetingDetails.setFollowersRange(request.getFollowersRange());
		promoteTargetingDetails.setEngagementsRange(request.getEngagementsRange());
		promoteTargetingDetails.setAudienceIndustry(request.getAudienceIndustry());
		promoteTargetingDetails.setInfluencerIndustry(request.getInfluencerIndustry());
		promoteTargetingDetails.setAudienceGeo(request.getAudienceGeo());
		promoteTargetingDetails.setInfluencerGeo(request.getInfluencerGeo());
		promoteTargetingDetails.setAudienceAge(request.getAudienceAge());
		promoteTargetingDetails.setInfluencerAge(request.getInfluencerAge());
		promoteTargetingDetails.setAudienceGender(request.getAudienceGender());
		promoteTargetingDetails.setInfluencerGender(request.getInfluencerGender());
	}

	private PromoteCampaignResponseModel getMailTemplate(PromoteCampaignResponseModelBuilder response, BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		BrandDetails brandDetails = promoteCampaignDao.getBrandDetails(new UserDetailsKey(UserStatus.APPROVED, ResourceServer.getUserId())).get();
		MailTemplateMaster mailTemplateMaster = promoteCampaignDao.getMailTemplateMasterByTemplateName(INITIALOUTREACHTEMPLATE).get();

		return response.mailTemplate(mailTemplateMaster.getMailBody()
				.replace(CAMPAIGN_DESCRIPTION, brandCampaignDetailsMysql.getCampaignDescription()).replace(INFLUENCERNAME, "Influencer_Name")
				.replace(PAYMENT, getPaymentString(brandCampaignDetailsMysql)).replace(BRANDNAME, brandDetails.getBrandName())
				.replace(mailTemplateMaster.getMailBody()
							.substring(mailTemplateMaster.getMailBody().indexOf("<a"), mailTemplateMaster.getMailBody().lastIndexOf("a>") + 2), "here"))
			.mailSubject(mailTemplateMaster.getMailSubject().replace(BRANDNAME, brandDetails.getBrandName())).build();
	}

	private String getPaymentString(BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		return brandCampaignDetailsMysql.getProduct().equals("PRODUCT_ONLY") ? ""
				: " They’re currently interested in paying $" + brandCampaignDetailsMysql.getPayment()
				+ " for an Instagram " + brandCampaignDetailsMysql.getPostType() + ".";
	}

	@Override
	public ResponseEntity<String> processPromoteCampaign(UUID campaignId, String action) {
		log.info("Start promote campaign {}", campaignId);

		if (!ResourceServer.isFreeBrand()) {
			UUID brandId = ResourceServer.getUserId();

			Optional<BrandDetails> brandDetails = promoteCampaignDao.getBrandDetails
					(new UserDetailsKey(UserStatus.APPROVED, brandId));

			if (brandDetails.isPresent()) {
				Optional<BrandCampaignDetailsMysql> brandCampaignDetailsMysql = promoteCampaignDao.
						getBrandCampaignDetailsMysqlCampaignIdAndBrandId(campaignId.toString(), brandId.toString());

				if (brandCampaignDetailsMysql.isPresent()) {
					if (action.equals(START)) {
						if (brandCampaignDetailsMysql.get().getCampaignStatus().equals(CampaignStatus.INPROGRESS))
							return ResponseEntity.status(HttpStatus.CONFLICT).body("Campaign already started");

						return startPromoteCampaign(campaignId, brandDetails.get(), brandCampaignDetailsMysql.get());
					}
					else
						return changePromoteCampaignStatus(campaignId, brandCampaignDetailsMysql.get(), action);
				}
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Campaign not available");
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Brand details not available");
		}
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Please subscribe for Higher plan to create promote campaign");
	}

	private ResponseEntity<String> startPromoteCampaign(UUID campaignId, BrandDetails brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		Optional<PromoteTargetingDetails> promoteTargetingDetails = promoteCampaignDao.getPromoteTargetingDetailsByCampaignId(campaignId);

		if (promoteTargetingDetails.isPresent()) {
			List<GetInfluencersResponseModel> getInfluencersResponse = Lists.newLinkedList();
			getInfluencersData(brandCampaignDetailsMysql, promoteTargetingDetails.get(), getInfluencersResponse);

			if (getInfluencersResponse.isEmpty())
				return ResponseEntity.status(HttpStatus.CONFLICT).body("Influencers data not found for the given targeting details"
						+ ", please change targeting details and try again");

			brandCampaignDetailsMysql.setCampaignStatus(CampaignStatus.INPROGRESS);
			promoteCampaignDao.saveBrandCampaignDetailsMysql(brandCampaignDetailsMysql);

			processPromoteInfluencersData(brandDetails, brandCampaignDetailsMysql, promoteTargetingDetails.get(), getInfluencersResponse);

			return ResponseEntity.status(HttpStatus.OK).body("Campaign started successfully! Shortly we will send emails to Influencers");
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Targeting details not available");
	}

	private ResponseEntity<String> changePromoteCampaignStatus(UUID campaignId, BrandCampaignDetailsMysql brandCampaignDetailsMysql, String action) {
		brandCampaignDetailsMysql.setModifiedAt(new Date());
		brandCampaignDetailsMysql.setCampaignStatus(action.equals(RESUME) ? CampaignStatus.INPROGRESS : CampaignStatus.PAUSED);

		if (action.equals(RESUME))
			brandCampaignDetailsMysql.setNextTrigger(setNextTriggerDate(new Date()));

		promoteCampaignDao.saveBrandCampaignDetailsMysql(brandCampaignDetailsMysql);

		return ResponseEntity.status(HttpStatus.OK).body("Campaign " + action.toUpperCase() + " successfully!");
	}

	private void getInfluencersData(BrandCampaignDetailsMysql brandCampaignDetailsMysql, PromoteTargetingDetails promoteTargetingDetails,
			List<GetInfluencersResponseModel> getInfluencersResponse) {
		GetInfluencersRequestModel getInfluencersRequest = new GetInfluencersRequestModel();

		getInfluencersRequest.setSkipCount(0);
		getInfluencersRequest.setIsNewSearch(false);
		getInfluencersRequest.setLimitCount(limitCount);
		getInfluencersRequest.setPlatform(promoteTargetingDetails.getPlatform().toString().toLowerCase());

		setCustomListFilter(getInfluencersRequest, brandCampaignDetailsMysql);
		setAudienceIndustryFilter(getInfluencersRequest, promoteTargetingDetails);
		setInfluencerIndustryFilter(getInfluencersRequest, promoteTargetingDetails);
		setAudienceLocationFilter(getInfluencersRequest, promoteTargetingDetails);
		setInfluencerLocationFilter(getInfluencersRequest, promoteTargetingDetails);
		setGenderFilter(getInfluencersRequest, promoteTargetingDetails);
		setAudienceAgeFilter(getInfluencersRequest, promoteTargetingDetails);
		setInfluencerAgeFilter(getInfluencersRequest, promoteTargetingDetails);
		setAudienceRelevanceFilter(getInfluencersRequest, promoteTargetingDetails);
		setFollowersFilter(getInfluencersRequest, promoteTargetingDetails);
		setEngagementsFilter(getInfluencersRequest, promoteTargetingDetails);
		setSortBy(getInfluencersRequest, promoteTargetingDetails);

		exploreInfluencerService.processSearchInfluencersRequest(getInfluencersRequest, getInfluencersResponse, false);
	}

	private void setCustomListFilter(GetInfluencersRequestModel getInfluencersRequest, BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		if (!StringUtils.isEmpty(brandCampaignDetailsMysql.getCustomListId())) {
			getInfluencersRequest.setCustomListId(brandCampaignDetailsMysql.getCustomListId());
			getInfluencersRequest.setCustomListAction(CUSTOM_LIST_ACTION);
		}
	}

	private void setAudienceIndustryFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getAudienceIndustry()))
			getInfluencersRequest.setAudienceBrandName(promoteTargetingDetails.getAudienceIndustry());
	}

	private void setInfluencerIndustryFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getInfluencerIndustry()))
			getInfluencersRequest.setInfluencerBrandName(promoteTargetingDetails.getInfluencerIndustry());
	}

	private void setAudienceLocationFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getAudienceGeo()))
				getInfluencersRequest.setAudienceGeoName(promoteTargetingDetails.getAudienceGeo());
	}

	private void setInfluencerLocationFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getInfluencerGeo()))
			getInfluencersRequest.setInfluencerGeoName(promoteTargetingDetails.getInfluencerGeo());
	}

	private void setGenderFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getAudienceGender()))
			getInfluencersRequest.setAudienceGender(promoteTargetingDetails.getAudienceGender());

		if (!StringUtils.isEmpty(promoteTargetingDetails.getInfluencerGender()))
			getInfluencersRequest.setInfluencerGender(promoteTargetingDetails.getInfluencerGender());
	}

	private void setAudienceAgeFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getAudienceAge()))
			getInfluencersRequest.setAudienceAge(new ArrayList<String>(Arrays.asList(promoteTargetingDetails.getAudienceAge().split(","))));
	}

	private void setInfluencerAgeFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getInfluencerAge()))
			getInfluencersRequest.setInfluencerAge(promoteTargetingDetails.getInfluencerAge());
	}

	private void setAudienceRelevanceFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getComparisonProfile()))
			getInfluencersRequest.setComparisonProfile(promoteTargetingDetails.getComparisonProfile());
	}

	private void setFollowersFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getFollowersRange())) {
			String followers [] = promoteTargetingDetails.getFollowersRange().split("-");
			getInfluencersRequest.setFollowersMin(Integer.parseInt(followers[0].trim()));
			getInfluencersRequest.setFollowersMax(Integer.parseInt(followers[1].trim()));
		}
	}

	private void setEngagementsFilter(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		if (!StringUtils.isEmpty(promoteTargetingDetails.getEngagementsRange())) {
			String engagements [] = promoteTargetingDetails.getEngagementsRange().split("-");
			getInfluencersRequest.setEngagementMin(Integer.parseInt(engagements[0].trim()));
			getInfluencersRequest.setEngagementMax(Integer.parseInt(engagements[1].trim()));
		}
	}

	private void setSortBy(GetInfluencersRequestModel getInfluencersRequest, PromoteTargetingDetails promoteTargetingDetails) {
		getInfluencersRequest.setSortBy(StringUtils.isEmpty(promoteTargetingDetails.getComparisonProfile()) ? SORT_BY_FOLLOWERS : SORT_BY_AUDIENCE);
	}

	private void processPromoteInfluencersData(BrandDetails brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			PromoteTargetingDetails promoteTargetingDetails, List<GetInfluencersResponseModel> getInfluencersResponse) {
		Thread thread = new Thread(() -> startPromoteCampaignProcess(brandDetails, brandCampaignDetailsMysql,
				promoteTargetingDetails, getInfluencersResponse));
		thread.start();
	}

	private void startPromoteCampaignProcess(BrandDetails brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			PromoteTargetingDetails promoteTargetingDetails, List<GetInfluencersResponseModel> getInfluencersResponse) {
		List<String> userIds = new LinkedList<String>();
		List<PromoteInfluencersData> promoteInfluencersList = Lists.newLinkedList();

		getInfluencersResponse.forEach(data -> {
			if(!StringUtils.isEmpty(data)) {
				userIds.add(data.getInfluencerId());
				promoteInfluencersList.add(PromoteInfluencersData.builder()
						.key(PromoteInfluencersDataKey.builder()
								.campaignId(UUID.fromString(brandCampaignDetailsMysql.getCampaignId()))
								.influencerDeepSocialId(data.getInfluencerId())
								.promoteInfluencerId(UUID.randomUUID()).build())
						.influencerName(data.getInfluencerHandle()).url(data.getUrl())
						.profileImage(data.getProfileImage()).accountType(data.getAccountType())
						.followers(data.getFollowers()).engagements(data.getEngagements())
						.audienceCredibility(data.getAudienceCheck()).engagementScore(data.getEngagementScore())
						.status(PromoteInfluencerStatus.NEW).createdAt(new Date()).platform(promoteTargetingDetails.getPlatform()).build());
			}
		});

		brandCampaignDetailsMysql.setModifiedAt(new Date());
		brandCampaignDetailsMysql.setNextTrigger(setNextTriggerDate(new Date()));
		brandCampaignDetailsMysql.setCustomListId(manageBrandCustomLists(brandCampaignDetailsMysql, userIds,
				promoteTargetingDetails.getPlatform().toString().toLowerCase()));

		promoteCampaignDao.saveBrandCampaignDetailsMysql(brandCampaignDetailsMysql);
		promoteCampaignDao.saveAllPromoteInfluencersData(promoteInfluencersList);

		sendEmailsToInfluencers(brandDetails, promoteInfluencersList, brandCampaignDetailsMysql, promoteTargetingDetails);
	}

	private String manageBrandCustomLists(BrandCampaignDetailsMysql brandCampaignDetailsMysql, List<String> userIds, String platform) {
		String listId = StringUtils.isEmpty(brandCampaignDetailsMysql.getCustomListId())
				? createNewBrandCustomList(platform) : brandCampaignDetailsMysql.getCustomListId();

		addInfluencersToCustomList(listId, userIds);

		return listId;
	}

	private String createNewBrandCustomList(String platform) {
		try {
			ManageCustomList manageCustomList = socialDataProxy.createCustomList(platform);

			if (manageCustomList.getSuccess())
				return manageCustomList.getCustomId().getId().toString();
		} catch (Exception e) {
			e.getStackTrace();
			return null;
		}
		return null;
	}

	private void addInfluencersToCustomList(String listId, List<String> userIds) {
		try {
			AddUsersToList addUsersToList = new AddUsersToList();
			addUsersToList.setUserIds(userIds);
			socialDataProxy.addUserToCustomList(listId, addUsersToList);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	private void sendEmailsToInfluencers(BrandDetails brandDetails, List<PromoteInfluencersData> promoteInfluencersList,
			BrandCampaignDetailsMysql brandCampaignDetailsMysql, PromoteTargetingDetails promoteTargetingDetails) {
		Optional<MailTemplateMaster> mailTemplateMaster = promoteCampaignDao.getMailTemplateMasterByTemplateName(INITIALOUTREACHTEMPLATE);

		for (int i = 0; i < promoteInfluencersList.size(); i++) {
			PromoteInfluencersData promoteInfluencersData = promoteInfluencersList.get(i);
			promoteInfluencersData.setInfluencerEmail(getInfluencerContactDetails(promoteInfluencersData.getInfluencerName(),
					promoteInfluencersData.getKey().getInfluencerDeepSocialId(), promoteTargetingDetails.getPlatform().toString().toLowerCase()));

			if (!StringUtils.isEmpty(promoteInfluencersData.getInfluencerEmail())) {
				sendMailUtil.addMailToQueue(constructMail(brandDetails, brandCampaignDetailsMysql, promoteInfluencersData, mailTemplateMaster.get()));
				promoteInfluencersData.setModifiedAt(new Date());
				promoteInfluencersData.setStatus(PromoteInfluencerStatus.SENT);
			} else {
				promoteInfluencersData.setModifiedAt(new Date());
				promoteInfluencersData.setStatus(PromoteInfluencerStatus.NO_CONTACT);
			}
			promoteCampaignDao.updatePromoteInfluencerData(promoteInfluencersData);
		}
		sendMailUtil.sendMail();
	}

	private String getInfluencerContactDetails(String influencerHandle, String influencerId, String platform) {
		try {
			Optional<InstagramDemographics> instagramDemographics = promoteCampaignDao.getInstagramDemographics(influencerHandle);

			if (instagramDemographics.isPresent() && !StringUtils.isEmpty(instagramDemographics.get().getEmail()))
				return instagramDemographics.get().getEmail();
			else {
				InfluencerContactDetails influencerContactDetails = socialDataProxy.getInfluencerContactDetails(influencerId, platform);

				if (influencerContactDetails.getSuccess()) {
					for (int i = 0; i < influencerContactDetails.getUserProfile().getContacts().size(); i++) {
						if (influencerContactDetails.getUserProfile().getContacts().get(i).getType().equalsIgnoreCase(EMAIL))
							return influencerContactDetails.getUserProfile().getContacts().get(i).getValue();
					}
				}
			}
		} catch(Exception e) {
			e.getStackTrace();
			return null;
		}
		return null;
	}

	private MailDetails constructMail(BrandDetails brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			PromoteInfluencersData promoteInfluencersData, MailTemplateMaster mailTemplateMaster) {

		return MailDetails.builder()
				.mailId(UUID.randomUUID().toString()).brandId(brandDetails.getKey().getUserId().toString()).brandEmail(brandDetails.getEmail())
				.brandName(brandDetails.getBrandName()).influencerHandle(promoteInfluencersData.getInfluencerName())
				.influencerEmail(promoteInfluencersData.getInfluencerEmail()).status(4)
				.influencerInviteLink(getInfluencerInviteLink(brandDetails, promoteInfluencersData))
				.mailSubject(mailTemplateMaster.getMailSubject()).mailDescription(mailTemplateMaster.getMailBody())
				.mailTemplateId(mailTemplateMaster.getId()).mappingId(promoteInfluencersData.getKey().getPromoteInfluencerId().toString())
				.campaignId(brandCampaignDetailsMysql.getCampaignId()).campaignName(brandCampaignDetailsMysql.getCampaignName())
				.campaignDescription(brandCampaignDetailsMysql.getCampaignDescription()).postType(brandCampaignDetailsMysql.getPostType())
				.product(brandCampaignDetailsMysql.getProduct()).payment(brandCampaignDetailsMysql.getPayment())
				.isPromote(true).createdAt(new Date()).build();
	}

	private String getInfluencerInviteLink(BrandDetails brandDetails, PromoteInfluencersData promoteInfluencersData) {
		return brandDetails.getKey().getUserId().toString() + '_' + promoteInfluencersData.getKey().getCampaignId()
				+ '_' + promoteInfluencersData.getKey().getInfluencerDeepSocialId();
	}

	@Override
	public void executePromoteCampaignsCron() {
		log.info("Cron running to execute Promote Campaign");
		List<BrandCampaignDetailsMysql> optionalBrandCampaignDetailsMysql = promoteCampaignDao.
				getBrandCampaignDetailsMysqlCampaignStatusAndIsPromote(CampaignStatus.INPROGRESS, true);

		optionalBrandCampaignDetailsMysql.forEach(data -> {
			BrandCampaignDetailsMysql brandCampaignDetailsMysql = data;

			if (validateActiveUntil(brandCampaignDetailsMysql)) {
				if (validateInfluencerThresholdLimit(UUID.fromString(data.getCampaignId()))) {
					Optional<BrandDetails> brandDetails = promoteCampaignDao.getBrandDetails
							(new UserDetailsKey(UserStatus.APPROVED, UUID.fromString(data.getBrandId())));

					if (brandDetails.isPresent())
						executePromoteCampaign(brandDetails.get(), brandCampaignDetailsMysql);
				}
			}
		});
	}

	private boolean validateActiveUntil(BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		if (brandCampaignDetailsMysql.getActiveUntil().toInstant().isBefore(Instant.now())) {
			brandCampaignDetailsMysql.setCampaignStatus(CampaignStatus.CLOSED);
			brandCampaignDetailsMysql.setCustomListId("");
			brandCampaignDetailsMysql.setModifiedAt(new Date());
			promoteCampaignDao.saveBrandCampaignDetailsMysql(brandCampaignDetailsMysql);
	
			if (!StringUtils.isEmpty(brandCampaignDetailsMysql.getCustomListId()))
				cleanUpCustomList(brandCampaignDetailsMysql.getCustomListId());

			return false;
		}
		return true;
	}

	private boolean validateInfluencerThresholdLimit(UUID campaignId) {
		List<PromoteInfluencersData> listPromoteInfluencersData = promoteCampaignDao.getPromoteInfluencersDataByCampaignId(campaignId)
				.stream().filter(f -> f.getStatus().equals(PromoteInfluencerStatus.APPROVED) || f.getStatus().equals(PromoteInfluencerStatus.CONTENT_PENDING)
						|| f.getStatus().equals(PromoteInfluencerStatus.CONTENT_APPROVED)).collect(Collectors.toList());

		if (listPromoteInfluencersData.size() >= thresholdLimit)
			return false;

		return true;
	}

	private void cleanUpCustomList(String customListId) {
		try {
			socialDataProxy.cleanUpCustomList(customListId, true);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	private void executePromoteCampaign(BrandDetails brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		if (validateNextTrigger(brandCampaignDetailsMysql)) {
			Optional<PromoteTargetingDetails> promoteTargetingDetails = promoteCampaignDao.
					getPromoteTargetingDetailsByCampaignId(UUID.fromString(brandCampaignDetailsMysql.getCampaignId()));

			if (promoteTargetingDetails.isPresent()) {
				List<GetInfluencersResponseModel> getInfluencersResponse = Lists.newLinkedList();
				getInfluencersData(brandCampaignDetailsMysql, promoteTargetingDetails.get(), getInfluencersResponse);

				if (!getInfluencersResponse.isEmpty())
					startPromoteCampaignProcess(brandDetails, brandCampaignDetailsMysql, promoteTargetingDetails.get(), getInfluencersResponse);
				else
					changeTargetingDetailsNotificationMail(brandDetails, brandCampaignDetailsMysql);
			}
		}
	}

	private void changeTargetingDetailsNotificationMail(BrandDetails brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		Optional<MailTemplateMaster> mailTemplateMaster = promoteCampaignDao.getMailTemplateMasterByTemplateName(CHANGETARGETINGDETAILSTEMPLATE);

		MailDetails mailDetails = MailDetails.builder().brandEmail(brandDetails.getEmail()).brandName(brandDetails.getBrandName())
				.brandId(brandDetails.getKey().getUserId().toString()).brandName(brandDetails.getBrandName())
				.mappingId(brandCampaignDetailsMysql.getCampaignId()).mailSubject(mailTemplateMaster.get().getMailSubject())
				.campaignName(brandCampaignDetailsMysql.getCampaignName()).mailDescription(mailTemplateMaster.get().getMailBody()).status(64)
				.mailTemplateId(mailTemplateMaster.get().getId()).build();

		mailService.sendPromoteTargetingDetailsNotificationMail(mailDetails);
	}

	private boolean validateNextTrigger(BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		Calendar newCalendar = Calendar.getInstance();
		Calendar oldCalendar = Calendar.getInstance();
		newCalendar.setTime(new Date());
		oldCalendar.setTime(brandCampaignDetailsMysql.getNextTrigger());

		if ((newCalendar.get(Calendar.DAY_OF_MONTH) == oldCalendar.get(Calendar.DAY_OF_MONTH))
				&& newCalendar.get(Calendar.MONTH) == oldCalendar.get(Calendar.MONTH)) {
			return true;
		}
		return false;
	}

	@Override
	public ResponseEntity<String> influencerStatus(UUID campaignId, String influencerDeepSocialId, String action) {
		log.info("Approve/Reject/Remove Promote Campaign Influencer");

		Optional<BrandDetails> brandDetails = promoteCampaignDao.getBrandDetails
				(new UserDetailsKey(UserStatus.APPROVED, ResourceServer.getUserId()));

		if (brandDetails.isPresent()) {
			Optional<BrandCampaignDetailsMysql> brandCampaignDetailsMysql = promoteCampaignDao.
					getBrandCampaignDetailsMysqlByCampaignId(campaignId.toString());

			if (brandCampaignDetailsMysql.isPresent()) {
				Optional<PromoteInfluencersData> optionalPromoteInfluencersData = promoteCampaignDao.
						getPromoteInfluencersDataByCampaignIdAndInfluencerDeepSocialId(campaignId, influencerDeepSocialId);

				if (optionalPromoteInfluencersData.isPresent()) {
					PromoteInfluencersData promoteInfluencersData = optionalPromoteInfluencersData.get();

					managePromoteCampaignInfluencer(promoteInfluencersData, brandDetails, brandCampaignDetailsMysql.get(), action);

					if (!action.equalsIgnoreCase(REMOVE))
						sendInfluencerStatusMail(brandDetails.get(), promoteInfluencersData);

					return ResponseEntity.status(HttpStatus.OK).body("Influencer " + action.toUpperCase() + " successfully!");
				}
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Influencer not available");
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Campaign not available");
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Brand Details not available");
	}

	private void managePromoteCampaignInfluencer(PromoteInfluencersData promoteInfluencersData, Optional<BrandDetails> brandDetails,
			BrandCampaignDetailsMysql brandCampaignDetailsMysql, String action) {
		promoteInfluencersData.setModifiedAt(new Date());

		try {
			if (action.equalsIgnoreCase(APPROVE)) {
				Optional<SocialIdMaster> socialIdMaster = promoteCampaignDao.getSocialIdMasterByHandleAndPlatform
						(promoteInfluencersData.getInfluencerName(), Platforms.INSTAGRAM);

				if (socialIdMaster.isPresent()) {
					UserAccounts userAccounts = dashboardDao.getUserAccounts();

					reportsService.saveReportHistory(ReportSearchHistory.builder()
							.key(ReportSearchHistoryKey.builder().brandId(UUID.fromString(userAccounts.getUserId())).isNew(false)
									.createdAt(new Date()).build()).influencerUsername(socialIdMaster.get().getInfluencerHandle())
							.brandName(userAccounts.getBrandName()).influencerDeepSocialId("").reportId("")
							.viewType(ViewType.PROMOTE).platform(promoteInfluencersData.getPlatform()).build());
				}
				else {
					UUID influencerId = reportsService.saveNewInfluencer(promoteInfluencersData.getKey().getInfluencerDeepSocialId(),
							promoteInfluencersData.getPlatform().name(), ViewType.PROMOTE);
					socialIdMaster = dashboardDao.getSocialIdMasterById(influencerId.toString());
				}
				promoteInfluencersData.setInfluencerSfId(UUID.fromString(socialIdMaster.get().getUserId()));
				promoteInfluencersData.setStatus(PromoteInfluencerStatus.APPROVED);
				processInfluencersToCampaign(brandDetails, socialIdMaster.get(), brandCampaignDetailsMysql);
			} else {
				promoteInfluencersData.setStatus(action.equalsIgnoreCase(REJECT) ? PromoteInfluencerStatus.REJECTED : PromoteInfluencerStatus.REMOVED);

				if (action.equalsIgnoreCase(REJECT) && !StringUtils.isEmpty(promoteInfluencersData.getInfluencerSfId()))
					dashboardDao.removeInfluencerFromCampaign(promoteInfluencersData.getKey().getCampaignId(),
							promoteInfluencersData.getInfluencerSfId(), false);
			}
	
			promoteCampaignDao.updatePromoteInfluencerData(promoteInfluencersData);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	private void processInfluencersToCampaign(Optional<BrandDetails> brandDetails, SocialIdMaster socialIdMaster,
			BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		String influencer = StringUtils.isEmpty(socialIdMaster.getInfluencerId())
				? socialIdMaster.getInfluencerHandle().substring(1) : socialIdMaster.getInfluencerId();
		InfluencerDemographicsMapper demographicsMapper = reportsService.getInfluencerDemographics(socialIdMaster, influencer, ViewType.PROMOTE, true);
		exploreInfluencerDao.processInfluencerToCampaign(brandDetails, brandCampaignDetailsMysql, demographicsMapper);
	}

	private void sendInfluencerStatusMail(BrandDetails brandDetails, PromoteInfluencersData promoteInfluencersData) {
		Optional<MailTemplateMaster> mailTemplateMaster = promoteCampaignDao
				.getMailTemplateMasterByTemplateName(promoteInfluencersData.getStatus().equals(PromoteInfluencerStatus.APPROVED)
						? INFLUENCERAPPROVEDTEMPLATE : INFLUENCERREJECTEDTEMPLATE);

		MailDetails mailDetails = MailDetails.builder().brandEmail(brandDetails.getEmail()).influencerEmail(promoteInfluencersData.getInfluencerEmail())
				.brandId(brandDetails.getKey().getUserId().toString()).brandName(brandDetails.getBrandName())
				.influencerHandle(promoteInfluencersData.getInfluencerName()).mailTemplateId(mailTemplateMaster.get().getId())
				.mappingId(promoteInfluencersData.getKey().getPromoteInfluencerId().toString())
				.mailSubject(mailTemplateMaster.get().getMailSubject()).mailDescription(mailTemplateMaster.get().getMailBody())
				.isApprovalMail(promoteInfluencersData.getStatus().equals(PromoteInfluencerStatus.APPROVED) ? true : false)
				.status(promoteInfluencersData.getStatus().equals(PromoteInfluencerStatus.APPROVED) ? 8 : 16).build();

		mailService.sendPromoteCampaignInfluencerStatusMail(mailDetails);
	}

	private Date setNextTriggerDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, nextTrigger);
		return calendar.getTime();
	}

	@Override
	public ResponseEntity<Object> getPromoteMailTemplate(UUID campaignId) {
		PromoteCampaignResponseModelBuilder promoteCampaignResponseModel = PromoteCampaignResponseModel.builder();
		return ResponseEntity.ok(getMailTemplate(promoteCampaignResponseModel, 
				promoteCampaignDao.getBrandCampaignDetailsMysqlByCampaignId(campaignId.toString()).get()));
	}
}