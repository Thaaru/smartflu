import React from "react";

export default function  ColoredLine({ color }){
  return <hr
      className={color}
      style={{
        color,
        backgroundColor: !color?color:"grey",
        height: 1,
        width: "100%"
      }}
    />
    }