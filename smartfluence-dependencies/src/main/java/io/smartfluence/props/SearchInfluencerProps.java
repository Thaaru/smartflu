package io.smartfluence.props;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@Getter
@Setter
@ToString
@RefreshScope
@Configuration
@ConfigurationProperties
@PropertySource("${search.influencer.properties}")
public class SearchInfluencerProps {

	private Double genderWeight;

	private Double ageWeight65;

	private Double ageWeight45_64;

	private Double ageWeight35_44;

	private Double ageWeight25_34;

	private Double ageWeight18_24;

	private Double ageWeight13_17;

	private String influencerGender;

	private String audienceSource;

	private String sortBy;

	private Boolean hidden;

	private Boolean hasAudienceData;

	private Boolean sponsoredPost;

	private Boolean isVerified;

	private Integer lastPosted;

	private Integer[] accountType;

	private String withContactDefault;

	private String withContactMust;

	private String withContactShould;

	private Double audienceCredibility;

	private String[] audienceCredibilityClass;

	private Integer autoUnhide;
}