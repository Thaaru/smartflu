function rT() {
	if ($('[name="userType"]:checked').val() == 'BRAND') {
		$('#brandname').attr('required', 'required');
		$('#brandname').parent().show();
		$('#firstname').removeAttr('required');
		$('#firstname').parent().hide();
		$('#lastname').removeAttr('required');
		$('#lastname').parent().hide();
	} else {
		$('#brandname').removeAttr('required');
		$('#brandname').parent().hide();
		$('#firstname').attr('required', 'required');
		$('#firstname').parent().show();
		$('#lastname').attr('required', 'required');
		$('#lastname').parent().show();
	}
}

function rV() {
	$('#panelRegister')
			.find('input[type="text"],input[type="password"],select').val('')
	$('form').removeClass('was-validated');
}

rT();

function formCallBack(obj) {
	if (obj.id == 'signInForm')
		signIn(obj)
	else
		signUp(obj);
	return false;
}

function signIn(obj) {

	$(obj).unbind('submit',formSubmit);
	$(obj).submit();
	return;

	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');

	xhr.done(function(d, s, r) {
		resetForms();
		window.location.href=r.getResponseHeader('location');
	});
	xhr.fail(function(x) {
		$('[type="password"]').val('');
		if(x.status==401){
			ERROR('The email or password is not a valid credentials');
		}else{
			ERROR('Our server is facing a technical issue. Please try again after a while')
		}
	});
}

function signUp(obj) {

	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');

	log(xhr);
	xhr.done(function(d, s, r) {
		window.location.href=window.location.origin+'/RegisterSuccessful';
	});
	xhr.fail(function(x) {
		if(x.status==409){
			ERROR('The email is already registered with Smartfluence');
		}else{
			ERROR('Our server is facing a technical issue. Please try again after a while');
		}
		$('[type="password"]').val('');
	});
}