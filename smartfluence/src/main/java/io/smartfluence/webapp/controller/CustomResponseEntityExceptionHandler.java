package io.smartfluence.webapp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.smartfluence.exception.HttpClientErrorException;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(HttpClientErrorException.class)
	protected ResponseEntity<Object> handleRestClientErrorException(HttpClientErrorException ex) {
		return new ResponseEntity<>(ex.getResponseBodyAsByteArray(), ex.getStatusCode());
	}	
}