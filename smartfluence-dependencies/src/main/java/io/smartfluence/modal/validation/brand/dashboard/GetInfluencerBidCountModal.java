package io.smartfluence.modal.validation.brand.dashboard;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class GetInfluencerBidCountModal {

	@NotNull(message = "Enter influencer Id")
	private UUID influencerId;

	@NotNull(message = "Enter bid Id")
	private UUID bidId;
}