package io.smartfluence.webapp.controller.rest.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "products")
public class Products implements Serializable {

    @Id
    @Column(name = "product_id")
    private Long productId;

    @Column(name = "campaign_id")
    private String campaignId;
    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_link")
    private String productLink;
}
