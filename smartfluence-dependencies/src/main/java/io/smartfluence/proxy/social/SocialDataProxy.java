package io.smartfluence.proxy.social;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import io.smartfluence.modal.social.customlist.AddUsersToList;
import io.smartfluence.modal.social.customlist.ManageCustomList;
import io.smartfluence.modal.social.report.InfluencersOverlap;
import io.smartfluence.modal.social.report.ReportData;
import io.smartfluence.modal.social.report.ReportResult;
import io.smartfluence.modal.social.search.request.InfluencersDataRequest;
import io.smartfluence.modal.social.search.response.InfluencersDataResponse;
import io.smartfluence.modal.social.user.InfluencerAutoSuggestions;
import io.smartfluence.modal.social.user.InfluencerContactDetails;
import io.smartfluence.modal.social.user.instagram.RawInstagramMediaInfo;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserFeed;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserInfo;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserStories;
import io.smartfluence.modal.social.user.tiktok.RawTikTokMediaInfo;
import io.smartfluence.modal.social.user.tiktok.RawTikTokUserFeed;
import io.smartfluence.modal.social.user.youtube.RawYouTubeChannelInfo;
import io.smartfluence.modal.social.user.youtube.RawYouTubeVideoInfo;
import io.smartfluence.modal.social.user.youtube.UploadPreview;

@Service
@FeignClient(name = "SocialDataFeignClient", url = "${social.data.api-url}", configuration = SocialDataConfiguration.class)
public interface SocialDataProxy {

	@GetMapping("reports")
	HashMap<String, ArrayList<ReportResult>> getReportsList(
			@RequestParam(value = "limit", defaultValue = "100") Integer limit,
			@RequestParam(value = "platform") String platform, @RequestParam(value = "q") String query);

	@GetMapping("reports/new")
	ReportData createNewReport(@RequestParam(value = "url") String url,
			@RequestParam(value = "subscribe", defaultValue = "0") int subscribe,
			@RequestParam(value = "ignore_removed", defaultValue = "1") int ignoreRemoved,
			@RequestParam(value = "ignore_outdated") Boolean ignoreOutdated,
			@RequestParam(value = "platform") String platform);

	@GetMapping("reports/{report_id}")
	ReportData getReport(@PathVariable("report_id") String reportId,
			@RequestParam(value = "fmt", defaultValue = "json") String downloadFormart);

	@GetMapping("reports/{report_id}")
	byte[] downloadReport(@PathVariable("report_id") String reportId,
			@RequestParam(value = "fmt", defaultValue = "pdf") String downloadFormart);

	@GetMapping("dict/users")
	InfluencerAutoSuggestions getAutoSuggestions(@RequestParam(value = "q") String influencerHandle,
			@RequestParam(value = "limit", defaultValue = "10") Integer limit,
			@RequestParam(value = "type") String type, @RequestParam(value = "platform") String platform);

	@GetMapping("raw/ig/user/info")
	RawInstagramUserInfo getInstagramUserInfo(@RequestParam(value = "url") String url);

	@GetMapping("raw/ig/user/feed")
	RawInstagramUserFeed getInstagramUserFeed(@RequestParam(value = "url") String url,
			@RequestParam(value = "after", required = false) String after);

	@GetMapping("raw/ig/media/info")
	ResponseEntity<RawInstagramMediaInfo> getInstagramMediaInfo(@RequestParam(value = "code") String url);

	@GetMapping("raw/ig/user/stories")
	ResponseEntity<RawInstagramUserStories> getInstagramUserStories(@RequestParam(value = "url") String url);

	@GetMapping("raw/yt/channel/info")
	RawYouTubeChannelInfo getYouTubeChannelInfo(@RequestParam(value = "url") String url);

	@GetMapping("raw/yt/video")
	ResponseEntity<RawYouTubeVideoInfo> getYoutubeVideoInfo(@RequestParam(value = "url") String url);

	@GetMapping("raw/yt/channel/videos")
	ResponseEntity<UploadPreview> getYoutubeChannelFeed(@RequestParam(value = "url") String url, @RequestParam(value = "sorted_by") String sortBy);

	@GetMapping("raw/tt/user/media")
	ResponseEntity<RawTikTokMediaInfo> getTiktokMediaInfo(@RequestParam(value = "url") String url);

	@GetMapping("raw/tt/user/feed")
	ResponseEntity<RawTikTokUserFeed> getTiktokUserFeed(@RequestParam(value = "url") String url);

	@GetMapping("exports/contacts")
	InfluencerContactDetails getInfluencerContactDetails(@RequestParam(value = "url") String url,
			@RequestParam(value = "platform") String platform);

	@PostMapping("search/newv1")
	ResponseEntity<InfluencersDataResponse> searchinfluencers(@RequestParam(value = "auto_unhide", defaultValue = "1") Integer autoUnhide,
			@RequestParam(value = "platform", defaultValue = "instagram") String platform,
			@RequestBody InfluencersDataRequest request);

	@PostMapping("reports/overlap")
	ResponseEntity<InfluencersOverlap> getOverlapData(@RequestParam(value = "urls") String urls,
			@RequestParam(value = "platform", defaultValue = "instagram") String platform,
			@RequestParam(value = "fmt", defaultValue = "json") String fmt,
			@RequestParam(value = "auto_subscribe", defaultValue = "false") Boolean autoSubscribe);

	@PostMapping("lists")
	ManageCustomList createCustomList(@RequestParam(value = "platform") String platform);

	@PostMapping("lists/{list_id}")
	void addUserToCustomList(@PathVariable(value = "list_id") String listId, @RequestBody AddUsersToList request);

	@DeleteMapping("lists/{list_id}")
	void cleanUpCustomList(@PathVariable(value = "list_id") String listId, @RequestParam(value = "full") Boolean full);
}