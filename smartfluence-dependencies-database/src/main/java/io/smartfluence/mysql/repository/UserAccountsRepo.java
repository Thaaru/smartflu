package io.smartfluence.mysql.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import io.smartfluence.mysql.entities.UserAccounts;

@Repository
public interface UserAccountsRepo extends CrudRepository<UserAccounts, String> {

	@Query("select u from UserAccounts u where u.email=:email")
	UserAccounts findByEmail(@Param("email") String email);

	@Query("select u from UserAccounts u where u.verificationCode=:verificationCode")
	UserAccounts findByVC(@Param("verificationCode") String verificationCode);

	@Query("select u from UserAccounts u where u.email=:email and userId<> :brandId")
	Optional<UserAccounts> findByEmailAndUserIdNotEqual(String email, String brandId);

	@Query("select u from UserAccounts u")
	List<UserAccounts> findAllUserAccounts();

	Optional<UserAccounts> findByUserId(String userId);
}