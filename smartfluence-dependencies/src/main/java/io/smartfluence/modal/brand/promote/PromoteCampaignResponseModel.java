package io.smartfluence.modal.brand.promote;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PromoteCampaignResponseModel {

	private String responseMessage;

	private String mailTemplate;

	private String mailSubject;

	private String campaignId;

	private String campaignName;

	private String campaignStatus;

	private String brandDescription;

	private String campaignDescription;

	private String postType;

	private Double payment;

	private String product;

	private Double productValue;

	private String requirements;

	private String restrictions;

	private String activeUntil;

	private String comparisonProfile;

	private String platform;

	private String followersRange;

	private String engagementsRange;

	private String audienceIndustry;

	private String influencerIndustry;

	private String audienceGeo;

	private String influencerGeo;

	private String audienceAge;

	private String influencerAge;

	private String audienceGender;

	private String influencerGender;

	private String pendingApproval;

	private Integer additionalInfo;

	private String pageType;
}