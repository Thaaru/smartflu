package io.smartfluence.mysql.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "social_id_master")
public class SocialIdMaster implements Copyable {

	private static final long serialVersionUID = -2348182576589127061L;

	@Id
	@Column(name = "user_id")
	private String userId;

	@Column(name = "influencer_id")
	private String influencerId;

	@Column(name = "influencer_report_id")
	private String influencerReportId;

	@Column(name = "influencer_handle")
	private String influencerHandle;

	@Enumerated(EnumType.STRING)
	@Column(name = "platform")
	private Platforms platform;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "modified_at")
	private Date modifiedAt;
}