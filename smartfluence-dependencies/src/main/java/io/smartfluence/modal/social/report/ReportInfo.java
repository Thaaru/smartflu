package io.smartfluence.modal.social.report;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportInfo implements Serializable {

	private static final long serialVersionUID = 8393598362169332706L;

	@JsonAlias("report_id")
	private String reportId;

	@JsonAlias("total_followers")
	private Long totalFollowers = 0L;

	@JsonAlias("total_unique_followers")
	private Long totalUniqueFollowers = 0L;
}