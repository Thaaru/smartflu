package io.smartfluence.influencer.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.smartfluence.influencer.entities.CountryMaster;
import io.smartfluence.influencer.entities.CustomResponseEntity;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.entities.StateMaster;
import io.smartfluence.influencer.repository.CountryMasterRepo;
import io.smartfluence.influencer.repository.StateMasterRepo;

@Log4j2
@Service
public class SPCountryStateMasterService {
    @Autowired
    StateMasterRepo stateMasterRepo;

    @Autowired
    CountryMasterRepo countryMasterRepo;

    CustomResponseEntity customResponseEntity = new CustomResponseEntity();
    
    public FullCampaignInfluencerDetails getCountryList(FullCampaignInfluencerDetails fullCampaignInfluencerDetails){
        Logger.getLogger("getCountryList").log(Level.FINE,"Getting the Country List");
        List<CountryMaster> countryMaster = countryMasterRepo.findAll();
        if(!countryMaster.isEmpty()){
            fullCampaignInfluencerDetails.setCountryList(countryMaster);
        }return fullCampaignInfluencerDetails;
    }

    public CustomResponseEntity getStateList(Long countryId){
        Logger.getLogger("getStateList").log(Level.FINE,"Getting the StateList");
        List<StateMaster> stateMasterList =  stateMasterRepo.findStateByCountryId(countryId); if(!stateMasterList.isEmpty()){
            customResponseEntity.setCode("600");
            customResponseEntity.setValue(stateMasterList);
            return customResponseEntity;
        }else{
            customResponseEntity.setCode("615");
            customResponseEntity.setError("No State Available");
            return customResponseEntity;
        }
    }


    
}
