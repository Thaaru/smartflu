package io.smartfluence.cassandra.entities.brand;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandBidDetailsKey;
import io.smartfluence.constants.BidStatus;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_bid_details")
public class BrandBidDetails implements Copyable {

	private static final long serialVersionUID = 8669806055816459509L;

	public BrandBidDetails(BrandBidDetails brandBidDetails) {
		this.key = new BrandBidDetailsKey(brandBidDetails.getKey());
		this.bidAmount = brandBidDetails.getBidAmount();
		this.brandEmail = brandBidDetails.getBrandEmail();
		this.brandIndustry = brandBidDetails.getBrandIndustry();
		this.brandInstagramHandle = brandBidDetails.getBrandInstagramHandle();
		this.brandName = brandBidDetails.getBrandName();
		this.brandStatus = brandBidDetails.getBrandStatus();
		this.generalMessage = brandBidDetails.getGeneralMessage();
		this.includedTag = brandBidDetails.getIncludedTag();
		this.influencerMail = brandBidDetails.getInfluencerMail();
		this.influencerFirstName = brandBidDetails.getInfluencerFirstName();
		this.influencerIndustry = brandBidDetails.getInfluencerIndustry();
		this.influencerHandle = brandBidDetails.getInfluencerHandle();
		this.influencerLastName = brandBidDetails.getInfluencerLastName();
		this.influencerStatus = brandBidDetails.getInfluencerStatus();
		this.postDuration = brandBidDetails.getPostDuration();
		this.postType = brandBidDetails.getPostType();
		this.paymentType = brandBidDetails.getPaymentType();
		this.campaignName = brandBidDetails.getCampaignName();
		this.bidStatus = brandBidDetails.getBidStatus();
		this.isPostUrlPresent = brandBidDetails.getIsPostUrlPresent();
		this.createdAt = brandBidDetails.getCreatedAt();
	}

	@PrimaryKey
	private BrandBidDetailsKey key;

	@Column("bid_amount")
	private Double bidAmount;

	@Column("is_post_url_present")
	@Default
	private Boolean isPostUrlPresent = false;

	@Column("campaign_name")
	private String campaignName;

	@Column("brand_email")
	private String brandEmail;

	@Column("brand_industry")
	private String brandIndustry;

	@Column("brand_instagram_handle")
	private String brandInstagramHandle;

	@Column("brand_name")
	private String brandName;

	@Column("brand_status")
	private UserStatus brandStatus;

	@Column("general_message")
	private String generalMessage;

	@Column("included_tag")
	private String includedTag;

	@Column("affiliate_type")
	private String affiliateType;

	@Column("influencer_email")
	private String influencerMail;

	@Column("influencer_first_name")
	private String influencerFirstName;

	@Column("influencer_industry")
	private String influencerIndustry;

	@Column("influencer_handle")
	private String influencerHandle;

	@Column("influencer_last_name")
	private String influencerLastName;

	@Column("influencer_status")
	private UserStatus influencerStatus;

	@Column("post_duration")
	private String postDuration;

	@Column("payment_type")
	private String paymentType;

	@Column("post_type")
	private String postType;

	@Column("modified_at")
	@LastModifiedDate
	private Date modifiedAt;

	@Column("credit_id")
	private UUID creditId;

	@Column("trans_yr_month")
	private String transYrMonth;

	@Column("created_at")
	private Date createdAt;

	@Column("bid_status")
	private BidStatus bidStatus;

	public Boolean getIsPostUrlPresent() {
		if (this.isPostUrlPresent == null)
			return false;

		return this.isPostUrlPresent;
	}
}