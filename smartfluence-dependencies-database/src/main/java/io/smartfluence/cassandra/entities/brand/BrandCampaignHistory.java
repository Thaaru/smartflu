package io.smartfluence.cassandra.entities.brand;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignHistoryKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_campaign_history")
public class BrandCampaignHistory implements Serializable {

	private static final long serialVersionUID = -3364234565060989282L;

	public BrandCampaignHistory(BrandCampaignHistory brandCampaignHistory) {
		this.key = brandCampaignHistory.getKey();
		this.brandName = brandCampaignHistory.getBrandName();
		this.createdAt = brandCampaignHistory.getCreatedAt();
		this.campaignName = brandCampaignHistory.getCampaignName();
	}

	@PrimaryKey
	private BrandCampaignHistoryKey key;

	@Column("brand_name")
	private String brandName;

	@Column("campaign_name")
	private String campaignName;

	@Column("created_at")
	private Date createdAt;
}