package io.smartfluence.mysql.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import io.smartfluence.mysql.entities.MailTemplateMaster;

public interface MailTemplateMasterRepo extends CrudRepository<MailTemplateMaster, String> {

	List<MailTemplateMaster> findAllByIsHidden(Boolean isHidden);

	Optional<MailTemplateMaster> findByTemplateName(String templateName);
}