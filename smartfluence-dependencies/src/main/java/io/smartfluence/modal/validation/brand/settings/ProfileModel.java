package io.smartfluence.modal.validation.brand.settings;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ProfileModel {

	private String brandName;

	private String firstName;

	private String lastName;

	@NotBlank(message = "Select an industry")
	private String industry;

	@Pattern(regexp = "(https?:\\/\\/)?([0-9a-zA-Z]+(-[0-9a-zA-Z]+)*)(\\.([0-9a-zA-Z]+(-[0-9a-zA-Z]+)*))*\\.[0-9a-zA-Z]{2,3}", message = "Enter valid website URL")
	private String website;

	@NotBlank(message = "Enter your valid email")
	@Email(message = "Enter your valid email")
	private String email;
}