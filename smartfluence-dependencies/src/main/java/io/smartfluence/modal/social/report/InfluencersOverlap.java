package io.smartfluence.modal.social.report;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InfluencersOverlap implements Serializable {

	private static final long serialVersionUID = -615675282933742317L;

	private boolean status;

	@JsonAlias("report_info")
	private ReportInfo reportInfo;

	@JsonAlias("data")
	private List<OverlapData> overlap;
}