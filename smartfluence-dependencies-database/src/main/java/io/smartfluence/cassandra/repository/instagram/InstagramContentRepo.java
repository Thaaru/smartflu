package io.smartfluence.cassandra.repository.instagram;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.instagram.InstagramContent;
import io.smartfluence.cassandra.entities.primary.InfluencerContentKey;

@Repository
public interface InstagramContentRepo extends CassandraRepository<InstagramContent, InfluencerContentKey> {

	List<InstagramContent> findAllByKeyInfluencerHandle(String influencerHandle);
}