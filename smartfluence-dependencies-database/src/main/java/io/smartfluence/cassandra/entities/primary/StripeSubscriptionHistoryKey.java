package io.smartfluence.cassandra.entities.primary;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import io.smartfluence.constants.UserType;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class StripeSubscriptionHistoryKey implements Copyable {

	private static final long serialVersionUID = 1077153540089150430L;

	@PrimaryKeyColumn(name = "user_type", type = PrimaryKeyType.PARTITIONED, ordinal = 1)
	private UserType userType;

	@PrimaryKeyColumn(name = "user_id", type = PrimaryKeyType.PARTITIONED, ordinal = 2)
	private UUID userId;

	@PrimaryKeyColumn(name = "subscription_id", type = PrimaryKeyType.PARTITIONED, ordinal = 3)
	private String subscriptionId;

	@PrimaryKeyColumn(name = "subscription_item_id", type = PrimaryKeyType.PARTITIONED, ordinal = 4)
	private String subscriptionItemId;

	@PrimaryKeyColumn(name = "trans_yr_month", type = PrimaryKeyType.CLUSTERED, ordinal = 1, ordering = Ordering.DESCENDING)
	private String transYrMonth;

	@PrimaryKeyColumn(name = "modified_at", type = PrimaryKeyType.CLUSTERED, ordinal = 2, ordering = Ordering.DESCENDING)
	private Date modifiedAt;
}