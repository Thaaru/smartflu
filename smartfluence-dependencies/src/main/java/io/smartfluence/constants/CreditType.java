package io.smartfluence.constants;

public enum CreditType {
	BID, DOWNLOAD_REPORT, VIEW_INFLUENCER
}