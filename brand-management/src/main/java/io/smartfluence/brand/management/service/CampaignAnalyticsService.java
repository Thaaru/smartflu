package io.smartfluence.brand.management.service;

import java.util.UUID;

import org.springframework.http.ResponseEntity;

public interface CampaignAnalyticsService {

	ResponseEntity<Object> getAnalyticsData(UUID campaignId);

	ResponseEntity<Object> getPostAnalyticsData(UUID campaignId);

	void getPostData();
}