package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.CommissionAffiliateTypeMasterV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommissionAffiliateTypeRepositoryV1 extends JpaRepository<CommissionAffiliateTypeMasterV1,Integer> {
    CommissionAffiliateTypeMasterV1 findByCommAffiTypeId(int commAffiTypeId);
}
