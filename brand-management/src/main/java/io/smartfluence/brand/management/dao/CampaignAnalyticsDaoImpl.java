package io.smartfluence.brand.management.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.service.ReportsService;
import io.smartfluence.cassandra.entities.PostAnalyticsData;
import io.smartfluence.cassandra.entities.PostAnalyticsTriggers;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.entities.instagram.InstagramCountryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.instagram.InstagramIndustryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramStateDemographic;
import io.smartfluence.cassandra.entities.primary.PostAnalyticsDataKey;
import io.smartfluence.cassandra.entities.tiktok.TikTokCountryDemographic;
import io.smartfluence.cassandra.entities.tiktok.TikTokDemographics;
import io.smartfluence.cassandra.entities.youtube.YouTubeCountryDemographic;
import io.smartfluence.cassandra.entities.youtube.YouTubeDemographics;
import io.smartfluence.cassandra.repository.brand.BrandCampaignDetailsRepo;
import io.smartfluence.cassandra.repository.PostAnalyticsDataRepo;
import io.smartfluence.cassandra.repository.PostAnalyticsTriggersRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramCountryDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramIndustryDemographicRepo;
import io.smartfluence.cassandra.repository.instagram.InstagramStateDemographicRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokCountryDemographicRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokDemographicRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeCountryDemographicRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeDemographicRepo;
import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.brand.campaign.analytics.AnalyticsDataModal;
import io.smartfluence.modal.brand.dashboard.PostDataModal;
import io.smartfluence.modal.brand.mapper.InfluencerCountryDemographicMapper;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.repository.SocialIdMasterRepo;

@Repository
@Configuration
public class CampaignAnalyticsDaoImpl implements CampaignAnalyticsDao {

	@Autowired
	private ReportsService reportsService;

	@Autowired
	private SocialIdMasterRepo socialIdMasterRepo;

	@Autowired
	private BrandCampaignDetailsRepo brandCampaignDetailsRepo;

	@Autowired
	private InstagramDemographicRepo instagramDemographicRepo;

	@Autowired
	private InstagramCountryDemographicRepo instagramCountryDemographicRepo;

	@Autowired
	private InstagramStateDemographicRepo instagramStateDemographicRepo;

	@Autowired
	private InstagramIndustryDemographicRepo instagramIndustryDemographicRepo;

	@Autowired
	private YouTubeDemographicRepo youtubeDemographicRepo;

	@Autowired
	private YouTubeCountryDemographicRepo youtubeCountryDemographicRepo;

	@Autowired
	private TikTokDemographicRepo tiktokDemographicRepo;

	@Autowired
	private TikTokCountryDemographicRepo tiktokCountryDemographicRepo;

	@Autowired
	private PostAnalyticsDataRepo postAnalyticsDataRepo;

	@Autowired
	private PostAnalyticsTriggersRepo postAnalyticsTriggersRepo;

	@Value("${post.statistics.trackhours}")
	private int no_of_days;

	@Override
	public Set<BrandCampaignDetails> getBrandCampaignDetailsByBrandIdAndCampaignId(UUID brandId, UUID campaignId) {
		return brandCampaignDetailsRepo.findAllByKeyBrandIdAndKeyCampaignId(brandId, campaignId);
	}

	@Override
	public List<InstagramDemographics> getInstagramDemographicsById(List<String> influencerHandles) {
		return instagramDemographicRepo.findAllById(influencerHandles);
	}

	@Override
	public List<InstagramCountryDemographic> getInstagramCountryDemographicByKey(List<String> influencerHandles) {
		return instagramCountryDemographicRepo.findAllByKeyInfluencerHandleIn(influencerHandles);
	}

	@Override
	public List<InstagramStateDemographic> getInstagramStateDemographicByKey(List<String> instagramHandles) {
		return instagramStateDemographicRepo.findAllByKeyInfluencerHandleIn(instagramHandles);
	}

	@Override
	public List<InstagramIndustryDemographic> getInstagramIndustryDemographicByKey(List<String> instagramHandles) {
		return instagramIndustryDemographicRepo.findAllByKeyInfluencerHandleIn(instagramHandles);
	}

	@Override
	public List<YouTubeDemographics> getYoutubeDemographicsById(List<String> influencerHandles) {
		return youtubeDemographicRepo.findAllById(influencerHandles);
	}

	@Override
	public List<YouTubeCountryDemographic> getYoutubeCountryDemographicByKey(List<String> influencerHandles) {
		return youtubeCountryDemographicRepo.findAllByKeyInfluencerHandleIn(influencerHandles);
	}

	@Override
	public List<TikTokDemographics> getTiktokDemographicsById(List<String> influencerHandles) {
		return tiktokDemographicRepo.findAllById(influencerHandles);
	}

	@Override
	public List<TikTokCountryDemographic> getTiktokCountryDemographicByKey(List<String> tiktokHandles) {
		return tiktokCountryDemographicRepo.findAllByKeyInfluencerHandleIn(tiktokHandles);
	}

	@Override
	public ResponseEntity<Object> getAnalyticsData(UUID campaignId) {
		UUID brandId = ResourceServer.getUserId();
		List<String> influencerIds = new LinkedList<>();

		Set<BrandCampaignDetails> brandCampaignDetails = getBrandCampaignDetailsByBrandIdAndCampaignId(brandId, campaignId);
		brandCampaignDetails.parallelStream().forEach(e -> influencerIds.add(e.getKey().getInfluencerId().toString()));

		return ResponseEntity.ok(getCampaignAnalytics(influencerIds));
	}

	private Set<AnalyticsDataModal> getCampaignAnalytics(List<String> influencerIds) {
		List<String> instagramHandles = new LinkedList<>();
		List<String> youtubeHandles = new LinkedList<>();
		List<String> tiktokHandles = new LinkedList<>();
		Set<AnalyticsDataModal> analyticsDataModal = new HashSet<>();
		List<SocialIdMaster> socialIdMasterList = socialIdMasterRepo.findAllByUserIdIn(influencerIds);

		socialIdMasterList.parallelStream().filter(f -> f.getPlatform().equals(Platforms.INSTAGRAM))
				.forEach(e -> instagramHandles.add(e.getInfluencerHandle()));
		socialIdMasterList.parallelStream().filter(f -> f.getPlatform().equals(Platforms.YOUTUBE))
				.forEach(e -> youtubeHandles.add(e.getInfluencerHandle()));
		socialIdMasterList.parallelStream().filter(f -> f.getPlatform().equals(Platforms.TIKTOK))
				.forEach(e -> tiktokHandles.add(e.getInfluencerHandle()));

		List<InfluencerDemographicsMapper> influencerDemographics = reportsService
				.getAllInfluencerDemographics(instagramHandles, youtubeHandles, tiktokHandles);
		List<InfluencerCountryDemographicMapper> influencerCountries = getInfluencerCountryDemographicData(instagramHandles, youtubeHandles, tiktokHandles);
		List<InstagramStateDemographic> states = getInstagramStateDemographicByKey(instagramHandles);
		List<InstagramIndustryDemographic> industries = getInstagramIndustryDemographicByKey(instagramHandles);

		for (SocialIdMaster socialIdMaster : socialIdMasterList) {
			AnalyticsDataModal modal = new AnalyticsDataModal();
			setDemographics(influencerDemographics, socialIdMaster, modal);
			setCountry(influencerCountries, socialIdMaster, modal);
			setState(states, socialIdMaster, modal);
			setIndustry(industries, socialIdMaster, modal);
			analyticsDataModal.add(modal);
		}

		return analyticsDataModal;
	}

	private List<InfluencerCountryDemographicMapper> getInfluencerCountryDemographicData(List<String> instagramHandles, List<String> youtubeHandles,
			List<String> tiktokHandles) {
		ModelMapper modelMapper = new ModelMapper();
		List<InfluencerCountryDemographicMapper> influencerCountryDemographicsList = new LinkedList<InfluencerCountryDemographicMapper>();

		if (!instagramHandles.isEmpty())
			getInstagramCountryDemographic(instagramHandles, modelMapper, influencerCountryDemographicsList);

		if (!youtubeHandles.isEmpty())
			getYoutubeCountryDemographic(youtubeHandles, modelMapper, influencerCountryDemographicsList);

		if (!tiktokHandles.isEmpty())
			getTiktokCountryDemographic(tiktokHandles, modelMapper, influencerCountryDemographicsList);

		return influencerCountryDemographicsList;
	}

	private void getInstagramCountryDemographic(List<String> instagramHandles, ModelMapper modelMapper,
			List<InfluencerCountryDemographicMapper> influencerCountryDemographicsList) {
		getInstagramCountryDemographicByKey(instagramHandles).forEach(data -> {
			InfluencerCountryDemographicMapper influencerCountryDemographicsMapper = new InfluencerCountryDemographicMapper();
			modelMapper.map(data, influencerCountryDemographicsMapper);
			influencerCountryDemographicsList.add(influencerCountryDemographicsMapper);
		});
	}

	private void getYoutubeCountryDemographic(List<String> youtubeHandles, ModelMapper modelMapper,
			List<InfluencerCountryDemographicMapper> influencerCountryDemographicsList) {
		getYoutubeCountryDemographicByKey(youtubeHandles).forEach(data -> {
			InfluencerCountryDemographicMapper influencerCountryDemographicsMapper = new InfluencerCountryDemographicMapper();
			modelMapper.map(data, influencerCountryDemographicsMapper);
			influencerCountryDemographicsList.add(influencerCountryDemographicsMapper);
		});
	}

	private void getTiktokCountryDemographic(List<String> tiktokHandles, ModelMapper modelMapper,
			List<InfluencerCountryDemographicMapper> influencerCountryDemographicsList) {
		getTiktokCountryDemographicByKey(tiktokHandles).forEach(data -> {
			InfluencerCountryDemographicMapper influencerCountryDemographicsMapper = new InfluencerCountryDemographicMapper();
			modelMapper.map(data, influencerCountryDemographicsMapper);
			influencerCountryDemographicsList.add(influencerCountryDemographicsMapper);
		});
	}

	private void setDemographics(List<InfluencerDemographicsMapper> demographics, SocialIdMaster socialIdMaster,
			AnalyticsDataModal dataModal) {
		List<InfluencerDemographicsMapper> demographicsFiltered = demographics.stream()
				.filter(e -> e.getInfluencerHandle().equals(socialIdMaster.getInfluencerHandle()))
				.collect(Collectors.toList());
		if (!demographicsFiltered.isEmpty()) {
			InfluencerDemographicsMapper demographic = demographicsFiltered.get(0);
			dataModal.setAudienceCredibility(demographic.getCredibility());
			dataModal.setEngagement(demographic.getEngagement());
			dataModal.setFollowers(demographic.getFollowers());
			dataModal.setInfluencerHandle(demographic.getInfluencerHandle());
			dataModal.setInfluencerId(UUID.fromString(socialIdMaster.getUserId()));
			dataModal.setLikes(demographic.getLikes());
			dataModal.setViews(demographic.getViews());
			dataModal.setComments(demographic.getComments());
			dataModal.setPlatfrom(demographic.getPlatform());
			dataModal.setInfluencerLink(StringUtils.isEmpty(demographic.getUrl())
					? "https://www.instagram.com/" + demographic.getInfluencerHandle().substring(1) : demographic.getUrl());
			setAgeRange(dataModal, demographic);
			setGender(dataModal, demographic);
			dataModal.setInfluencerPricing(demographic.getPricing());
		}
	}

	private void setAgeRange(AnalyticsDataModal dataModal, InfluencerDemographicsMapper demographics) {
		Map<String, Double> ageRange = new HashMap<>();
		ageRange.put("13-17", demographics.getAge13To17());
		ageRange.put("18-24", demographics.getAge18To24());
		ageRange.put("25-34", demographics.getAge25To34());
		ageRange.put("35-44", demographics.getAge35To44());
		ageRange.put("45-64", demographics.getAge45To64());
		ageRange.put("65-Any", demographics.getAge65ToAny());
		dataModal.setAgeRange(ageRange);
	}

	private void setGender(AnalyticsDataModal dataModal, InfluencerDemographicsMapper demographics) {
		Map<String, Double> gender = new HashMap<>();
		gender.put("Male", demographics.getMale());
		gender.put("Female", demographics.getFemale());
		dataModal.setGender(gender);
	}

	private Map<String, Double> setCountry(List<InfluencerCountryDemographicMapper> countries, SocialIdMaster socialIdMaster, AnalyticsDataModal modal) {

		List<InfluencerCountryDemographicMapper> countryDemographicsFiltered = countries.stream()
				.filter(e -> e.getKey().getInfluencerHandle().equals(socialIdMaster.getInfluencerHandle()))
				.collect(Collectors.toList());
		Map<String, Double> country = new HashMap<>();

		if (!countryDemographicsFiltered.isEmpty()) {
			for (InfluencerCountryDemographicMapper countryDemographic : countryDemographicsFiltered) {
				country.put(countryDemographic.getKey().getCountry(), countryDemographic.getValue());
			}
		}
		modal.setCountry(country);
		return country;
	}

	private Map<String, Double> setState(List<InstagramStateDemographic> states, SocialIdMaster socialIdMaster, AnalyticsDataModal modal) {
		List<InstagramStateDemographic> statesFiltered = states.stream()
				.filter(e -> e.getKey().getInfluencerHandle().equals(socialIdMaster.getInfluencerHandle()))
				.collect(Collectors.toList());
		Map<String, Double> state = new HashMap<>();

		if (!statesFiltered.isEmpty()) {
			for (InstagramStateDemographic stateDemographic : statesFiltered) {
				state.put(stateDemographic.getKey().getState(), stateDemographic.getValue());
			}
		}
		modal.setState(state);
		return state;
	}

	private Map<String, Double> setIndustry(List<InstagramIndustryDemographic> industries, SocialIdMaster socialIdMaster, AnalyticsDataModal modal) {
		List<InstagramIndustryDemographic> instagramIndustryDemographics = industries.stream()
				.filter(e -> e.getKey().getInfluencerHandle().equals(socialIdMaster.getInfluencerHandle()))
				.collect(Collectors.toList());
		Map<String, Double> industry = new HashMap<>();

		if (!instagramIndustryDemographics.isEmpty()) {
			for (InstagramIndustryDemographic instagramIndustryDemographic : instagramIndustryDemographics) {
				industry.put(instagramIndustryDemographic.getKey().getIndustry(),
						instagramIndustryDemographic.getValue());
			}
		}
		modal.setIndustry(industry);
		return industry;
	}

	@Override
	public List<PostAnalyticsData> getPostAnalyticsDataByBrandIdAndCampaignId(UUID brandId, UUID campaignId) {
		return postAnalyticsDataRepo.findByKeyBrandIdAndKeyCampaignId(brandId, campaignId);
	}

	@Override
	public List<PostAnalyticsTriggers> getPostAnalyticsTriggers(Boolean isTriggerEnd) {
		 return postAnalyticsTriggersRepo.findAllByIsTriggerEnd(isTriggerEnd);
	}

	@Override
	public void updatePostAnalyticsTriggers(PostAnalyticsTriggers postAnalyticsTriggers) {
		if (postAnalyticsTriggers.getPostTimeHourIndex() >= no_of_days)
			postAnalyticsTriggers.setIsTriggerEnd(true);

		if (postAnalyticsTriggers.getNextTrigger() == null) {
			postAnalyticsTriggers.setInitialTrigger(new Date());
			postAnalyticsTriggers.setNextTrigger(new Date());
		}

		postAnalyticsTriggers.setNextTrigger(new Date(postAnalyticsTriggers.getNextTrigger().getTime() + TimeUnit.HOURS.toMillis(1)));
		postAnalyticsTriggers.setPostDataTime(null);

		postAnalyticsTriggersRepo.save(postAnalyticsTriggers);
	}

	@Override
	public void insertPostData(PostAnalyticsTriggers postAnalyticsTriggers, PostDataModal postDataModal, Date updateDate) {
		PostAnalyticsData postAnalyticsData = PostAnalyticsData.builder()
				.key(PostAnalyticsDataKey.builder().brandId(postAnalyticsTriggers.getKey().getBrandId())
						.campaignId(postAnalyticsTriggers.getKey().getCampaignId()).influencerId(postAnalyticsTriggers.getKey().getInfluencerId())
						.postId(postAnalyticsTriggers.getPostId()).postTimeHourIndex(postAnalyticsTriggers.getPostTimeHourIndex()).build())
				.influencerHandle(postAnalyticsTriggers.getInfluencerHandle()).postName(postAnalyticsTriggers.getPostName())
				.caption(postDataModal.getCaption()).noOfComments(postDataModal.getNoOfComments()).userName(postDataModal.getUserName())
				.platform(postAnalyticsTriggers.getPlatform()).noOfLikes(postDataModal.getNoOfLikes()).noOfViews(postDataModal.getNoOfViews())
				.postUrl(postAnalyticsTriggers.getPostUrl()).updatedDate(updateDate).build();

		postAnalyticsDataRepo.save(postAnalyticsData);
	}
}