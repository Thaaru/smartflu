package io.smartfluence.mysql.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.mysql.entities.InfluencerDataQueue;
import io.smartfluence.mysql.entities.primary.InfluencerDataQueueKey;

@Repository
public interface InfluencerDataQueueRepo extends CrudRepository<InfluencerDataQueue, InfluencerDataQueueKey> {

}