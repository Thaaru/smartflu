package io.smartfluence.cassandra.entities;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("influencer_user_details")
public class InfluencerUserDetailsDescription implements Serializable {

	private static final long serialVersionUID = -4937231924188163703L;

	@PrimaryKey("user_id")
	private UUID userId;

	@Column("description")
	private String userDescription;

	@Column("industry")
	private String userIndustry;

	@Column("location")
	private String userLocation;
}