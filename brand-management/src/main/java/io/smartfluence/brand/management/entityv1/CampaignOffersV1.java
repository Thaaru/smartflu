package io.smartfluence.brand.management.entityv1;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_offers_v1")
public class CampaignOffersV1 {
    @EmbeddedId
    CampaignOfferId campaignOfferId;
    @Column(name = "payment_offer")
    private double paymentOffer;
    @Column(name = "commission_value")
    private double commissionValue;
    @Column(name = "commission_value_type")
    private String commissionValueType;
    @Column(name = "commission_affiliate_type")
    private String commissionAffiliateType;
    @Column(name = "commission_affiliate_custom_name")
    private String commissionAffiliateCustomName;
    @Column(name = "commission_affiliate_details")
    private String commissionAffiliateDetails;

    @Column(name = "commission_value_type_id")
    private Integer commissionValueTypeId;
    @Column(name = "commission_affiliate_type_id")
    private Integer commissionAffiliateTypeId;


}
