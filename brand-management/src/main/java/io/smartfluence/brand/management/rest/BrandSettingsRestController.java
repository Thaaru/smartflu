package io.smartfluence.brand.management.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.brand.management.service.SettingsService;
import io.smartfluence.modal.validation.ChangePasswordModal;
import io.smartfluence.modal.validation.brand.settings.PaymentModal;
import io.smartfluence.modal.validation.brand.settings.ProfileModel;
import io.smartfluence.modal.validation.brand.settings.SocialConnectModel;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandSettingsRestController {

	@Autowired
	private SettingsService settingsService;

	@GetMapping("profile-details")
	public ResponseEntity<Object> getProfileDetails() {
		return settingsService.getProfileDetails();
	}

	@GetMapping("email-connection")
	public ResponseEntity<Object> getEmailConnectionSettings() {
		return settingsService.getEmailConnectionSettings();
	}

	@PostMapping("update-profile")
	public ResponseEntity<Object> updateProfileDetails(@Validated @ModelAttribute ProfileModel profileModel) {
		return settingsService.updateProfileDetails(profileModel);
	}

	@PostMapping("change-password")
	public ResponseEntity<Object> changePassword(@Validated @ModelAttribute ChangePasswordModal changePasswordModal) {
		return settingsService.changePassword(changePasswordModal);
	}

	@GetMapping("social-connect")
	public ResponseEntity<Object> getBrandSocialConnect() {
		return settingsService.getBrandSocialConnect();
	}

	@PostMapping("social-connect")
	public ResponseEntity<Object> updateBrandSocialConnect(@Validated @ModelAttribute SocialConnectModel socialConnectModel, BindingResult bindingResult) {
		log.info(socialConnectModel);

		if (!bindingResult.hasFieldErrors())
			return settingsService.updateBrandSocialConnect(socialConnectModel);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("payment-preference")
	public ResponseEntity<Object> getBrandPaymentPreference() {
		return settingsService.getBrandPaymentPreference();
	}

	@PostMapping("payment-preference")
	public ResponseEntity<Object> updateBrandPaymentPreference(@Validated @ModelAttribute PaymentModal paymentModel, BindingResult bindingResult) {
		log.info(paymentModel);

		if (!bindingResult.hasFieldErrors())
			return settingsService.updateBrandPaymentPreference(paymentModel);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("get-settings/{userEmail}/get-tokenURL")
	public ResponseEntity<String> getTokenURL(@PathVariable String userEmail) {
		return settingsService.getTokenURL(userEmail);
	}

	@GetMapping("get-settings/{code}/get-accessToken")
	public ResponseEntity<String> getAccessToken(@PathVariable String code) {
		return settingsService.getAccessToken(code);
	}

	@GetMapping("get-settings/remove-account")
	public ResponseEntity<String> removeAccount() {
		return settingsService.removeAccount();
	}
}