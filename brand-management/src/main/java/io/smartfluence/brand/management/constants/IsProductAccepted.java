package io.smartfluence.brand.management.constants;

public enum IsProductAccepted {
    TRUE, FALSE
}
