package io.smartfluence.webapp.controller.rest.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "post_type_master")
public class PostMaster implements Serializable {

    @Id
    @Column(name = "post_type_id")
    private Long postTypeId;

    @Column(name = "post_type_name")
    private String postTypeName;

}
