package io.smartfluence.util;

import java.util.UUID;

public class VerificationUtil {
	static int UUID4_LENGTH = UUID.randomUUID().toString().length();

	public enum VerificationType {
		REGISTRATION, RESETPASSWORD
	}

	public static String generateVCForRegistartion() {
		return UUID.randomUUID().toString() + '-' + UUID.randomUUID().toString() + '-' + UUID.randomUUID();
	}

	public static Boolean isRegistrationVC(String verificationCode) {
		return UUID.fromString(verificationCode.substring(0, UUID4_LENGTH - 1)).version() == 4
				&& UUID.fromString(verificationCode.substring(UUID4_LENGTH + 1, (2 * UUID4_LENGTH) + 1)).version() == 4;
	}

	public static String generateVCForForgotPassword() {
		return UUID.randomUUID().toString() + '-' + UUID.randomUUID().toString() + '-' + UUID.randomUUID();
	}

	public static Boolean isPasswordVC(String verificationCode) {
		return UUID.fromString(verificationCode.substring(0, UUID4_LENGTH - 1)).version() == 4
				&& UUID.fromString(verificationCode.substring(UUID4_LENGTH + 1, (2 * UUID4_LENGTH) + 1)).version() == 4;
	}
}