$("#product").change(function () {
	if ($("#product").val() == 'YES') {
		$('#productValue').removeAttr("disabled");
		$('#productValueRequired').removeClass("d-none");
	} else {
		$("#productValue").val('');
		$('#productValue').prop("disabled", true);
		$('#productValueRequired').addClass("d-none");
	}
	if ($("#product").val() == 'PRODUCT_ONLY') {
		$("#payment").val('');
		$('#payment').prop("disabled", true);
		$('#productValue').removeAttr("disabled");
		$('#postTypeRequired').addClass("d-none");
		$('#paymentRequired').addClass("d-none");
	} else {
		$('#payment').removeAttr("disabled");
		$('#postTypeRequired').removeClass("d-none");
		$('#paymentRequired').removeClass("d-none");
	}
});





$('input[name=offerType]').on('change', function (e) {
	let checked = e.target.checked;
	if (this.value == 'product') {
		$('.offerTypeCheckBox').prop('checked', false)
	}
	if (this.value != 'product') {
		$('input[value=product]').prop('checked', false)
	}
	$(this).prop('checked', checked)
});

$('textarea').on('change keypress paste', function (e) {
	console.log('e', e.target.value)
	var remainingChar = $(this).attr("data-remainingChar")
	var maxlength = $(this).attr("maxlength")

	if (e.target.value.length > maxlength) {
		return false;
	}
	$(`#${remainingChar}`).html(maxlength - e.target.value.length);
});


$('#createNewPromoteCampaign').addClass("disabled");
$.post({
	url: window.location.origin + '/brand/saveCampaign',
	data: {
		//campaignId : campaignId
		campaignName: campaignName
		//,brandDescription : brandDescription
		//,campaignDescription : campaignDescription
		//,comparisonProfile : comparisonProfile
		//,postType : postType
		//,payment : payment
		//,product : product
		//,productValue : productValue
		//,requirements : requirements
		//,restrictions : restrictions
		//,activeUntil : activeUntil
		//,platform : platform
		//,followersRange : followersRange
		//,engagementsRange : engagementsRange
		//,audienceIndustry : audienceIndustry
		//,influencerIndustry : influencerIndustry
		//,audienceGeo : audienceLocation
		//,influencerGeo : influencerLocation
		//,audienceAge : audienceAgeRanges
		//,influencerAge : influencerAgeRanges
		//,audienceGender : audienceGender
		//,influencerGender : influencerGender
		//,additionalInfo : ''
		//,isEdit : isEdit
	},
}).done(function (data) {
	SUCCESS(data.responseMessage);
	$('#preview').removeClass("d-none");
	mailTemplate = data.mailTemplate;
	mailSubject = data.mailSubject;
	$("#startPromoteCampaign").removeClass("not-saved");
	$("#startPromoteCampaign").attr('onclick', 'processCampaign("' + data.campaignId + '","start")');
	$("#campaignId").val(data.campaignId);
	$('#createNewPromoteCampaign').removeClass("disabled");
}).fail(function (data) {
	if (data.status == 400) {
		WARNING("Mandatory fields required..!");
	} else if (data.status != 500) {
		ERROR(data.responseText);
	} else {
		ERROR("Failed to save Promote Campaign");
	}
	$('#preview').addClass("d-none");
	$("#startPromoteCampaign").addClass("not-saved");
	$('#createNewPromoteCampaign').removeClass("disabled");
});

function formCallBack(obj) {
	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');

	$('.save-campaign').addClass('disabled');
	xhr.done(function (d, s, r) {
		toast('Campaign saved successfully');
		location.reload();
	});
	xhr.fail(function (d, s, r) {
		$('.save-campaign').removeClass('disabled');
		if (!d.responseText == '') {
			toast(d.responseText, 'error');
		} else {
			toast('Failed to save campaign', 'error');
		}
	});
}


