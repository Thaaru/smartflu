import React,{useState} from "react";

export default function LongText({content,limit}){
  const [showAll, setShowAll] = useState(false);

  const showMore = () => setShowAll(true);
  const showLess = () => setShowAll(false);

  if (content.length <= limit) {
    // there is nothing more to showreturn 
   return <div class = "normalText">{content}</div>
  }
  if (showAll) {
    // We show the extended text and a link to reduce itreturn
    return <div class = "normalText"> 
      {content} 
      <div>
      <a style={{"color":"blue","textDecoration":"underline"}} onClick={showLess}>Show less</a>
      </div>
      </div>
  }
  // In the final case, we show a text with ellipsis and a `Read more` buttonconst
  const toShow = content.substring(0, limit);
  return <div class = "normalText"> 
    {toShow} 
    <div>
    <a className="showMoreLess" style={{"textDecoration":"underline"}} onClick={showMore}>Show more</a>
    </div>
    </div>
}