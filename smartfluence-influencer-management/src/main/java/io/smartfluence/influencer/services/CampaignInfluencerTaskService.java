package io.smartfluence.influencer.services;

import io.smartfluence.influencer.entities.CampaignInfluencerTask;
import io.smartfluence.influencer.entities.CampaignInfluencerTaskId;
import io.smartfluence.influencer.entities.CampaignTasks;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.repository.CampaignInfluencerTasksRepo;
import io.smartfluence.influencer.repository.CampaignTaskRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class CampaignInfluencerTaskService {
    @Autowired
    CampaignInfluencerTasksRepo campaignInfluencerTasksRepo;
    @Autowired
    CampaignTaskRepo campaignTaskRepo;



    public FullCampaignInfluencerDetails getInfluencerTasks(String campaignId, String influencerId,
                                                            FullCampaignInfluencerDetails fullCampaignInfluencerDetails) {
        Logger.getLogger("getInfluencerTasks").log(Level.FINE,"getting the InfluencerTasks");
        List<CampaignTasks> campaignTask = campaignTaskRepo.getByCampaignId(campaignId);
        if (!campaignTask.isEmpty()) {
            fullCampaignInfluencerDetails.setDeliverables(campaignTask);
            List<CampaignInfluencerTaskId> tempCampaignTasksList = new ArrayList<>();
            List<CampaignInfluencerTask> campaignInfluencerTasksList = campaignInfluencerTasksRepo.findByCitIdCampaignIdAndCitIdInfluencerId(campaignId,influencerId);
            campaignInfluencerTasksList.forEach(campaignInfluencerTask ->
                tempCampaignTasksList.add(campaignInfluencerTask.getCitId())
            );
            fullCampaignInfluencerDetails.setTasksAcceptedList(tempCampaignTasksList);
        }
        return fullCampaignInfluencerDetails;

    }

    public void updateCampaignInfluencerTaskService(FullCampaignInfluencerDetails fullCampaignInfluencerDetails) {
        Logger.getLogger("updateCampaignInfluencerTaskService").log(Level.FINE,"getting the updateCampaignInfluencerTask");
        if (fullCampaignInfluencerDetails.getTasksAcceptedList() != null) {
                List<CampaignInfluencerTask> campaignInfluencerTasksList = campaignInfluencerTasksRepo.findByCitIdCampaignIdAndCitIdInfluencerId(fullCampaignInfluencerDetails.getCampaignId(), fullCampaignInfluencerDetails.getInfluencerId());
                campaignInfluencerTasksRepo.deleteAll(campaignInfluencerTasksList);
                fullCampaignInfluencerDetails.getTasksAcceptedList().stream().forEach(infTasks ->
                    campaignInfluencerTasksRepo.save(new CampaignInfluencerTask(infTasks))
                );
            }
    }
}
