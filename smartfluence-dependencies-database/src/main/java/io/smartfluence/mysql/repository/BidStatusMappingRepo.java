package io.smartfluence.mysql.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.mysql.entities.BidStatusMapping;

@Repository
public interface BidStatusMappingRepo extends CrudRepository<BidStatusMapping, String> {

}