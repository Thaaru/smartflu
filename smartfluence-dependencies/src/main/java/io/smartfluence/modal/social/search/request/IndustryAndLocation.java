package io.smartfluence.modal.social.search.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IndustryAndLocation {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("weight")
	private Double weight;
}