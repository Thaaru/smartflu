package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.influencer.entities.Address;
import io.smartfluence.influencer.entities.CampaignInfluencerAddressId;

import java.util.List;

@Repository
public interface CampaignInfluencerAddressRepo extends JpaRepository<Address, CampaignInfluencerAddressId>{
    Address findAddressByCampaignInfluencerAddressType(CampaignInfluencerAddressId campaignInfluencerAddressId);
  List<Address> findByCampaignInfluencerAddressTypeCampaignIdAndCampaignInfluencerAddressTypeInfluencerId(String campaignId, String influencerId);
}
