package io.smartfluence.influencer.entities;

public enum OffersType {
    PRODUCT,COMMISSION,PAYMENT,PAYMENT_COMMISSION
}
