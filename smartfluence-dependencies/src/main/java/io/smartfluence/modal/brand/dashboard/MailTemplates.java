package io.smartfluence.modal.brand.dashboard;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MailTemplates {

	private String id;

	private String mailBody;

	private String mailSubject;

	private String templateName;

	private String paymentType;

	private String subPaymentType;

	private Boolean isUserCreated;

	private Boolean isNew;
}