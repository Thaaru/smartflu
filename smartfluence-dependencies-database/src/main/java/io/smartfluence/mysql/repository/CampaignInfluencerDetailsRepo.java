package io.smartfluence.mysql.repository;

import io.smartfluence.mysql.entities.CampaignInfluencerDetails;
import io.smartfluence.mysql.entities.CampaignInfluencerId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CampaignInfluencerDetailsRepo extends JpaRepository<CampaignInfluencerDetails, CampaignInfluencerId>{
   
    CampaignInfluencerDetails findByInfluencerEmail(String email);

    List<CampaignInfluencerDetails> findByCampaignInfluencerIdInfluencerId(String influencerId);
    
}
