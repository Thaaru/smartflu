package io.smartfluence.modal.social.user.tiktok;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokMediaItemInfo {

	@JsonAlias("itemStruct")
	private RawTikTokMediaItemStruct itemStruct;
}