package io.smartfluence.cassandra.entities;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.AudienceReportHistoryKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("audience_report_history")
public class AudienceReportHistory {

	@PrimaryKey
	private AudienceReportHistoryKey key;

	@Column("influencer_user_name")
	private String influencerUserName;

	@Column("service")
	private String service;

	@Column("user_name")
	private String name;

	@Column("brand_name")
	private String brandName;

	@Column("user_type")
	private String describe;

	@Column("no_of_influencers")
	private String noOfInfluencers;
}