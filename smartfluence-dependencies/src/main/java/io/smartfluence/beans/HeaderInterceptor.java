package io.smartfluence.beans;

import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Component
public class HeaderInterceptor implements RequestInterceptor {

	@Override
	public void apply(RequestTemplate template) {
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

		if (requestAttributes == null)
			return;

		HttpServletRequest request = requestAttributes.getRequest();
		if (request == null)
			return;

		Map<String, Collection<String>> headers = new LinkedHashMap<>();
		Enumeration<String> reqHeaders = request.getHeaderNames();

		while(reqHeaders.hasMoreElements()) {
			String name = reqHeaders.nextElement();
			Collection<String> values = Collections.list(request.getHeaders(name));
			headers.put(name,values );
		}
		template.headers(headers);
	}
}