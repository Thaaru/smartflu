package io.smartfluence.cassandra.entities.brand;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.brand.primary.BrandSearchHistoryKey;
import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_search_history")
public class BrandSearchHistory implements Copyable {

	private static final long serialVersionUID = 657999780468959998L;

	@PrimaryKey
	private BrandSearchHistoryKey key;

	@Column("brand_name")
	private String brandName;

	@Column("platfrom")
	private Platforms platform;

	@Column("key_words")
	private String keyWords;

	@Column("comparison_profile")
	private String comparisonProfile;

	@Column("audience_industry")
	private String audienceIndustry;

	@Column("audience_location")
	private String audienceLocation;

	@Column("audience_age")
	private String audienceAge;

	@Column("audience_gender")
	private String audienceGender;

	@Column("followers_range")
	private String followersRange;

	@Column("engagements_range")
	private String engagementsRange;

	@Column("influencer_industry")
	private String influencerIndustry;

	@Column("influencer_location")
	private String influencerLocation;

	@Column("influencer_age")
	private String influencerAge;

	@Column("influencer_gender")
	private String influencerGender;

	@Column("with_contact")
	private String withContact;

	@Column("bio_text")
	private String bioText;

	@Column("sponsored_posts")
	private Boolean sponsoredPosts;

	@Column("is_verified")
	private Boolean isVerified;

	@Column("sort_by")
	private String sortBy;

	@Column("total_results")
	private Long totalResults;

	@Column("is_influencer_mapped")
	private Boolean isInfluencerMapped;

	@Indexed
	@Column("created_at")
	private Date createdAt;
}