package io.smartfluence.modal.utill;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class ResponseObj {
   private String code;
   private Object data;
   private Map<String,String> errors;

   public ResponseObj(String code, Object data) {
      this.code = code;
      this.data = data;
   }

   public ResponseObj(String code, Object data, Map<String, String> errors) {
      this.code = code;
      this.data = data;
      this.errors = errors;
   }


   public ResponseObj() {
   }

   public ResponseObj(String s) {
      this.code = code;
   }
}
