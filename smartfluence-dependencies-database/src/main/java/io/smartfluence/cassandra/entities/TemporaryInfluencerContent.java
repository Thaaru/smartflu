package io.smartfluence.cassandra.entities;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.TemporaryInfluencerContentKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("temporary_influencer_content")
public class TemporaryInfluencerContent {

	@PrimaryKey
	private TemporaryInfluencerContentKey key;

	@Column("value")
	private String value;

	@Column("influencer_handle")
	private String influencerHandle;
}