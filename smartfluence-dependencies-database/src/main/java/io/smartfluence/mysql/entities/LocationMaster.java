package io.smartfluence.mysql.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import io.smartfluence.constants.LocationType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "location_master")
public class LocationMaster implements Serializable {

	private static final long serialVersionUID = 4988397196067731298L;

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "location")
	private String location;

	@Column(name = "code")
	private String code;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private LocationType type;

	@Column(name = "weight")
	private double weight;

	@Column(name = "status")
	private int status;
}