package io.smartfluence.webapp.controller.rest.brand;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;
import com.stripe.model.ExternalAccountCollection;
import com.stripe.model.PlanCollection;
import com.stripe.model.Subscription;

import io.smartfluence.ResourceServer;
import io.smartfluence.modal.validation.ChangePasswordModal;
import io.smartfluence.modal.validation.brand.settings.ProfileModel;
import io.smartfluence.modal.validation.brand.settings.SocialConnectModel;
import io.smartfluence.stripe.StripeManager;
import io.smartfluence.webapp.proxy.BrandManagementServiceProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandSettingsRestController {

	@Autowired
	private StripeManager stripeManager;

	@Autowired
	private BrandManagementServiceProxy brandManagementServiceProxy;

	@PostMapping("update-profile")
	public ResponseEntity<Object> updateProfileDetails(@Validated @ModelAttribute ProfileModel profileModel, BindingResult bindingResult) {
		log.info(profileModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.updateProfileDetails(profileModel.getBrandName(), profileModel.getFirstName(),
					profileModel.getLastName(), profileModel.getIndustry(), profileModel.getWebsite(), profileModel.getEmail());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("change-password")
	public ResponseEntity<Object> changePassword(@Validated @ModelAttribute ChangePasswordModal changePasswordModal, BindingResult bindingResult) {
		log.info(changePasswordModal);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.changePassword(changePasswordModal.getOldPassword(), changePasswordModal.getNewPassword());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("social-connect")
	public ResponseEntity<Object> getBrandSocialConnect() {
		return brandManagementServiceProxy.getBrandSocialConnect();
	}

	@PostMapping("social-connect")
	public ResponseEntity<Object> updateBrandSocialConnect(@Validated @ModelAttribute SocialConnectModel socialConnectModel, BindingResult bindingResult) {
		log.info(socialConnectModel);

		if (!bindingResult.hasFieldErrors())
			return brandManagementServiceProxy.updateBrandSocialConnect(socialConnectModel.getInstagramHandle());

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping(path = "plans", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> plans() {
		PlanCollection planCollection = stripeManager.plans();
		return ResponseEntity.ok(planCollection == null ? null : planCollection.getLastResponse().body());
	}

	@GetMapping(path = "cards", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> cards() {
		ExternalAccountCollection cardCollection = stripeManager.cards(ResourceServer.getUserId());
		return ResponseEntity.ok(cardCollection == null ? null : cardCollection.getLastResponse().body());
	}

	@PostMapping(path = "plans/activate/{planId}")
	public ResponseEntity<Object> activatePlan(@PathVariable String planId) {
		log.info(planId);
		UUID userId = ResourceServer.getUserId();

		if (stripeManager.hasSource(userId)) {
			try {
				Subscription subscription = stripeManager.changeSubscription(userId, planId);
				return ResponseEntity.ok(subscription);
			} catch (StripeException e) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body(e);
			}
		} else
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body("No default payment method found");
	}

	@GetMapping(path = "plans/active", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> activePlan() {
		Subscription subscription = stripeManager.activeSubscription(ResourceServer.getUserId());
		return ResponseEntity.ok(subscription);
	}

	@PostMapping(path = "plans/cancel")
	public ResponseEntity<Object> cancelSubscription() {
		UUID userId = ResourceServer.getUserId();
		if (stripeManager.hasSource(userId)) {
			try {
				Subscription subscription = stripeManager.cancelActiveSubscription(userId, false);
				return ResponseEntity.ok(subscription);
			} catch (StripeException e) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body(e);
			}
		} else
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body("No default payment method found");
	}

	@PostMapping("cards/save")
	public ResponseEntity<Object> saveCard(@RequestParam String stripeToken, @RequestParam(defaultValue = "false", required = false) Boolean defaultSource) {
		ExternalAccount account;
		try {
			account = stripeManager.addCard(ResourceServer.getUserId(), stripeToken, defaultSource);
		} catch (StripeException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(e);
		}
		if (account == null)
			return ResponseEntity.badRequest().body(stripeToken);
		else if (account.getMetadata().containsKey("exists"))
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Card already exists");

		return ResponseEntity.ok(account.getId());
	}

	@PostMapping("cards/remove")
	public ResponseEntity<Object> removeCard(@RequestParam String cardId) {
		ExternalAccount externalAccount;
		try {
			externalAccount = stripeManager.removeCard(ResourceServer.getUserId(), cardId);
			if (externalAccount == null)
				return ResponseEntity.badRequest().body("No card found");
		} catch (StripeException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(e);
		}
		return ResponseEntity.ok(externalAccount.getLastResponse().body());
	}

	@PostMapping("cards/default")
	public ResponseEntity<Object> defaultCard(@RequestParam String cardId) {
		Customer customer;
		try {
			customer = stripeManager.defaultCard(ResourceServer.getUserId(), cardId);
			if (customer == null)
				return ResponseEntity.badRequest().body("No such card found");
		} catch (StripeException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(e);
		}
		return ResponseEntity.ok(customer.toJson());
	}

	@GetMapping("get-settings/{userEmail}/get-tokenURL")
	public ResponseEntity<String> getTokenURL(@PathVariable String userEmail) {
		return brandManagementServiceProxy.getTokenURL(userEmail);
	}

	@GetMapping("get-settings/{code}/get-accessToken")
	public ResponseEntity<String> getAccessToken(@PathVariable String code) {
		return brandManagementServiceProxy.getAccessToken(code);
	}

	@GetMapping("get-settings/remove-account")
	public ResponseEntity<String> removeAccount() {
		return brandManagementServiceProxy.removeAccount();
	}
}