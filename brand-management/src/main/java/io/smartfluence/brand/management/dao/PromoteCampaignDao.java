package io.smartfluence.brand.management.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import io.smartfluence.cassandra.entities.PromoteInfluencersData;
import io.smartfluence.cassandra.entities.PromoteTargetingDetails;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.constants.Platforms;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.SocialIdMaster;

public interface PromoteCampaignDao {

	Optional<BrandDetails> getBrandDetails(UserDetailsKey key);

	void saveBrandCampaignDetailsMysql(BrandCampaignDetailsMysql brandCampaignDetailsMysql);

	Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByCampaignId(String campaignId);

	Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByCampaignNameAndBrandId(String campaignName, String brandId);

	List<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByBrandIdAndIsPromote(String brandId, Boolean isPromote);

	List<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlCampaignStatusAndIsPromote(CampaignStatus campaignStatus, Boolean isPromote);

	Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlCampaignIdAndBrandId(String campaignId, String brandId);

	Optional<PromoteTargetingDetails> getPromoteTargetingDetailsByCampaignId(UUID campaignId);

	void savePromoteTargetingDetails(PromoteTargetingDetails promoteTargetingDetails);

	void updatePromoteInfluencerData(PromoteInfluencersData promoteInfluencersData);

	void saveAllPromoteInfluencersData(List<PromoteInfluencersData> promoteInfluencersData);

	Optional<PromoteInfluencersData> getPromoteInfluencersDataByCampaignIdAndInfluencerDeepSocialId(UUID campaignId,
			String influencerDeepSocialId);

	List<PromoteInfluencersData> getPromoteInfluencersDataByCampaignId(UUID campaignId);

	Optional<InstagramDemographics> getInstagramDemographics(String instagramHandle);

	Optional<MailTemplateMaster> getMailTemplateMasterByTemplateName(String id);

	Optional<SocialIdMaster> getSocialIdMasterById(String influencerId);

	Optional<SocialIdMaster> getSocialIdMasterByHandleAndPlatform(String influencerHandle, Platforms platform);
}