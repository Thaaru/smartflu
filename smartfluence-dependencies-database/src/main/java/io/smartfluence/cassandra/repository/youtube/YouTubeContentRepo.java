package io.smartfluence.cassandra.repository.youtube;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.primary.InfluencerContentKey;
import io.smartfluence.cassandra.entities.youtube.YouTubeContent;

@Repository
public interface YouTubeContentRepo extends CassandraRepository<YouTubeContent, InfluencerContentKey> {

	List<YouTubeContent> findAllByKeyInfluencerHandle(String influencerHandle);
}