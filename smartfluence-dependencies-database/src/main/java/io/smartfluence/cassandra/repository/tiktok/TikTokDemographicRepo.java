package io.smartfluence.cassandra.repository.tiktok;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.tiktok.TikTokDemographics;

@Repository
public interface TikTokDemographicRepo extends CassandraRepository<TikTokDemographics, String> {

	@AllowFiltering
	Optional<TikTokDemographics> findByUserId(UUID userId);
}