package io.smartfluence.cassandra.repository.brand;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.brand.BrandCampaignInfluencerHistory;
import io.smartfluence.cassandra.entities.brand.primary.BrandCampaignInfluencerHistoryKey;

public interface BrandCampaignInfluencerHistoryRepo	extends CassandraRepository<BrandCampaignInfluencerHistory, BrandCampaignInfluencerHistoryKey> {

}