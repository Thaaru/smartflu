package io.smartfluence.modal.brand.campaignv1;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommissionAffiliateMasterEntityV1 {
    private int caId;
    private String caValue;
}
