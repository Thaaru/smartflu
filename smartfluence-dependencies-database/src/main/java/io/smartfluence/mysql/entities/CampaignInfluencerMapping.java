package io.smartfluence.mysql.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_influencer_map")
@NamedEntityGraph(name = "CampaignInfluencerMapping.eagerAll", attributeNodes = @NamedAttributeNode("brandCampaignDetails"))
public class CampaignInfluencerMapping implements Serializable {

	private static final long serialVersionUID = 685693244127647416L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne()
	@JoinColumn(name = "campaign_id")
	private BrandCampaignDetailsMysql brandCampaignDetails;

	@Column(name = "influencer_id")
	private String influencerId;

	@Column(name = "bid_count")
	private Integer bidCount;
}