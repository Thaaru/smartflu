package io.smartfluence.modal.brand.campaignv1;

import lombok.Data;

import java.util.List;

@Data
public class CampInfluRespObj {
    List<InfluObj> influencersList;
    List<CurrencyMasterEntityV1> currencyMaster;
    List<CommissionAffiliateMasterEntityV1> affiliateMaster;
}
