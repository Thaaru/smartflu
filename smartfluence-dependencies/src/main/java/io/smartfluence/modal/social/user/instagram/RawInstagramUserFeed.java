package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawInstagramUserFeed implements Serializable {

	private static final long serialVersionUID = -588655611050539903L;

	@JsonAlias("items")
	private List<RawInstagramUserFeedItems> items;

	@JsonAlias("more_available")
	private boolean moreAvailable;

	@JsonAlias("end_cursor")
	private String endCursor;

	@JsonAlias("status")
	private String status;
}