package io.smartfluence.brand.management.jobs;

import java.util.UUID;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.util.StringUtils;

import feign.FeignException;
import io.smartfluence.brand.management.mailbean.MailService;
import io.smartfluence.brand.management.service.ReportsService;
import io.smartfluence.modal.social.report.ReportData;
import io.smartfluence.mysql.entities.InfluencerDataQueue;
import io.smartfluence.mysql.repository.InfluencerDataQueueRepo;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class InfluencerDataQueueJob extends QuartzJobBean {

	@Autowired
	private ReportsService reportsService;

	@Autowired
	private MailService mailService;

	@Autowired
	private InfluencerDataQueueRepo influencerDataQueueRepo;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		JobDataMap map = context.getJobDetail().getJobDataMap();
		try {
			InfluencerDataQueue influencerDataQueue = (InfluencerDataQueue) map.get("influencerDataQueue");
			log.info("Sending mail for influencer report, {}", influencerDataQueue.getKey().getInfluencer());

			try {
				ReportData reportData = reportsService.createReport(influencerDataQueue.getKey().getInfluencer(), influencerDataQueue.getKey().getViewType(),
						influencerDataQueue.getPlatform().name().toLowerCase(), influencerDataQueue.getKey().getBrandId(), null);
				influencerDataQueue.setInfluencerHandle("@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
						? reportData.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase()
								: reportData.getUserProfile().getHandle()));
				UUID influencerSFId = UUID.fromString(reportsService.saveInfluencerReportData(reportData));
				mailService.sendMailToBrandForInfluencerReport(influencerSFId, influencerDataQueue);
				influencerDataQueueRepo.deleteById(influencerDataQueue.getKey());
			} catch (FeignException fe) {
				if (fe.getMessage().contains("retry_later"))
					fe.getStackTrace();
			} catch (Exception e) {
				e.getStackTrace();
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}
}