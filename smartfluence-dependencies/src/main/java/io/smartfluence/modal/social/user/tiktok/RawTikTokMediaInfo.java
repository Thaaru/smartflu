package io.smartfluence.modal.social.user.tiktok;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokMediaInfo implements Serializable {

	private static final long serialVersionUID = 4052449327150722220L;

	@JsonAlias("success")
	private Boolean success;

	@JsonAlias("media")
	private RawTikTokMedia media;
}