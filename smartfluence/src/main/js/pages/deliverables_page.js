import React, { useState } from "react";
import { FooterComponent } from "../components/footer_component";
import { HeaderComponent } from "../components/header_component";
import ColoredLine from "../components/line_component";
import CollapsiblePanel from "../components/collapsableComponent";
import { IoIosArrowForward, IoIosArrowDown, IoLogoInstagram, IoLogoYoutube } from "react-icons/io";
import { updateCampaignDetailsApi } from "../services/api/campaign_details_api";
import { ACCEPTED_MESSAGE, ERROR_MESSAGE, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";
import { toast } from "react-toastify";
import { getPlatformAndIcon } from "../components/platform_icon";

export default function DeliverablesPage({ data, code, back, next, close, }) {
    const [collapse, setCollapse] = useState(true);
    const [title, setTitle] = useState("Expand All");
    const [icon, setIcon] = useState(IoIosArrowForward);
    const collapseAll = () => {
        setCollapse(!collapse);
        setIcon(state => {
            return state === IoIosArrowForward
                ? IoIosArrowDown
                : IoIosArrowForward;
        });
        setTitle(state => {
            return state === "Expand All" ? "Collapse All" : "Expand All";
        });
    }
    const submitHandler = async (data, loader) => {
        data.deliverablesAcceptedStatus = STEPS_ACCEPTED_STATUS;
        const tempList = [];
        const deliverables = data.deliverables;
        for (let i = 0; i < deliverables.length; i++) {
            const deliverableAcceptedData = {};
            deliverableAcceptedData["campaignTaskId"] = deliverables[i].taskId;
            deliverableAcceptedData["campaignId"] = data.campaignId;
            deliverableAcceptedData["influencerId"] = data.influencerId;
            tempList.push(deliverableAcceptedData);
        }

        data.tasksAcceptedList = tempList;
        loader(false);
        const result = await updateCampaignDetailsApi(data);
        if (result.code == 600) {
            toast.success(ACCEPTED_MESSAGE, { delay: 1000 })
            loader(true);
            next(result.value);
        } else {
            toast.success(ERROR_MESSAGE, { delay: 1000 })
            loader(true);
            data.deliverablesAcceptedStatus = null;
            return null;
        }
    }
    
    return (
        <div class='content-wrapper'>
            <div>
                <div >
                    <HeaderComponent data={data} close={close} headerName={"Deliverables"} stepNo={3} />
                    <ColoredLine color={'secondaryColor'} />
                    <br />
                    <div style={{ "textAlign": "left" }}>
                        <div class="col-md">
                            <p class="normalText">The following are the required tasks for the campaign</p>
                        </div>
                        <div class="col-md">
                            <p class="normalText">Required Tags</p>
                        </div>
                        <div class="col-lg">
                            <div className="col justify-content-around " style={{ backgroundColor: "#F8F4FF", height: "70px", alignContent: "center", textAlign: "left" }}>
                                <div class="col-lg p-3 primaryColor">{data.hashtags != null ? data.hashtags : "No Tags available"}</div>
                            </div>
                        </div>
                        <br />
                        <div class="col-md">
                            <p class="normalText">Selected Tasks</p>
                        </div>
                        <div class="col-md">
                            {getPlatformAndIcon(data).length > 0 && getPlatformAndIcon(data).map((item) =>
                                <CollapsiblePanel data={item} collapse={collapse}>
                                    <span>{item.guidlines}</span>
                                </CollapsiblePanel>
                            )
                            }
                        </div>
                    </div>
                </div>
                <FooterComponent next={next} back={back} data={data} acceptedStatus={data.deliverablesAcceptedStatus} backWardStatus={STEPS_ACCEPTED_STATUS} forwardStatus={STEPS_ACCEPTED_STATUS} callBackfunction={submitHandler} previousScreenStatus={data.productsAcceptedStatus} />
            </div>
        </div>
    );

}
