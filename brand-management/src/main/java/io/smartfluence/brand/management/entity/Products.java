package io.smartfluence.brand.management.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_products_v1")
public class Products implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)

    private Long productId;

    private String campaignId;

    private String productName;

    private String productLink;
}
