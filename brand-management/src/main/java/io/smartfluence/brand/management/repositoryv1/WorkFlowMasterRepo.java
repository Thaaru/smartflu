package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.WorkflowMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkFlowMasterRepo extends JpaRepository<WorkflowMaster,Long> {
}
