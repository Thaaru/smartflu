package io.smartfluence.modal.validation.influencer.settings;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class AboutYouModal {

	@NotBlank(message = "Write Something about yourself")
	private String description;

	@NotBlank(message = "Please select Industry")
	private String industry;

	@NotBlank(message = "Please Select the Location")
	private String location;

	@NotBlank(message = "Please mention the brands that you have worked with")
	private String mentionBrandWorkedWith;

	@NotBlank(message = "Please enter the price")
	private String askingPriceStory2Hr;

	@NotBlank(message = "Please enter the price")
	private String askingPriceStory24Hr;

	@NotBlank(message = "Please enter the price")
	private String askingPricePost24Hr;

	@NotBlank(message = "Please enter the price")
	private String askingPricePost1Week;

	@NotBlank(message = "Please enter the price")
	private String askingPricePostPremanent;
}