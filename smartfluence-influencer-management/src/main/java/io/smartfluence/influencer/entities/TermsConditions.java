package io.smartfluence.influencer.entities;


import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "terms_conditions_v1")
public class TermsConditions {
    @Id
    @Column(name = "term_id")
    private Long termId;
    private String termsCondition;
    private String termsConditionFilePath;
    @Enumerated(EnumType.STRING)
    private TermsType type;
    private boolean isActive;
}
