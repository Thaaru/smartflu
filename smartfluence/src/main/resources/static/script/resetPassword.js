function formCallBack(obj) {
	if (obj.id == 'resetpasswordForm')
		resetPassword(obj);

}
var password = document.getElementById("password"), confirm_password = document
		.getElementById("confirmPassword");

function resetPassword(obj) {
	var verificationCode = getParam('verificationCode');
	if (password.value != confirm_password.value) {
		ERROR("Passwords Don't Match");
	} else {
		var form = $(obj).ajaxSubmit({

			beforeSubmit : function(arr, $form, options) {
				arr.push({
					name : 'verificationCode',
					value : verificationCode
				});

			}
		});
		var xhr = form.data('jqxhr');
	}

}
password.onchange = resetPassword();
confirm_password.onkeyup = resetPassword();
