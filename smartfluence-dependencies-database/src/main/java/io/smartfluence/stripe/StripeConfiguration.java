package io.smartfluence.stripe;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "stripe")
public class StripeConfiguration {

	private String publishableKey = "pk_test_u36ZKkYYQw0ZdzK8O2d14FHd";

	private String secretKey = "sk_test_iPydyUjt9MP8HI7M7hiomQHX";
}