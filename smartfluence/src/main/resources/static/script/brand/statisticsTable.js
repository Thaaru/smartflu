var rowsSelected = [];
var campaignWFSTDT;
var campaignStatus;

$(document).ready(function () {
    $.fn.dataTable.ext.errMode = 'none';
    campaignId = window.location.href.split("/")[5];
    init_statisticsTable(campaignId);

    $(".makePaymentAction").click(function () {
      
        alert("Handler for .change() called.");
    });


    campaignWFSTDT.on('user-select', function (e, dt, type, cell, originalEvent) {
        //if ( originalEvent.target.nodeName.toLowerCase() === 'img' ) {
        e.preventDefault();
        //}
    });
});

var influencersList_su = [];
var currencyMaster = [];
var affiliateMaster = [];
var platformMasters = [];
var postTypeMasters = [];


//table_campaign_statistics
function init_statisticsTable() {
    campaignWFSTDT = $('#table_campaign_statistics').DataTable({
        dom: "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-7 pl-0'><'col-sm-12 col-md-3'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",

        ajax: {
            url: `/brand/view-campaign-v1/view-influencers?proposalStatus=PRODUCT_FULFILLED,TRACK_POST&id=${campaignId}`,
            dataSrc: function (response) {
               
                if (response?.code == 600) {
                   
                    currencyMaster = response.data.currencyMaster;
                    affiliateMaster = response.data.affiliateMaster;
                    influencersList_su = response.data.influencersList;
                    campaignStatus = influencersList_su.find(x => x.campaignStatus)
                  
                    if (campaignStatus.campaignStatus == "ACTIVE") {
                        $("#actions").attr("disabled", true)
                    }
                    if (influencersList_su?.length > 0) {
                        platformMasters = influencersList_su[0].platformMasters;
                        postTypeMasters = influencersList_su[0].postTypeMasters;
                        return influencersList_su;
                    }
                    return [];
                }
                return [];
            }
        },
        autoWidth: false,

        columnDefs: [
            {
                targets: 0,
                width: "15%",
                className: "text-center",
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    //meta.settings.aoData[0]._aData.influencerId
                   
                    return '<input disabled type="checkbox" class="inviteCheckbox">';
                }
            },
            {
                targets: 1,
                width: "22%",
                className: "text-left",
                data: "influencerHandle",
                render:function (data, type, row, meta) {
                   return `<a style=" color: #2f008d" href="/brand/view-influencer/${row.influencerId}?isValid=true&platform=${row.platform}"
                   target="_blank">${data}</a>`
                }
            },
            {
                targets: 2,
                width: "24%",
                className: "text-left",
                data: "tasks",
                render: function (data, type, row, meta) {
                   
                    if (typeof data == "object" && data?.length > 0) {
                        var platformId = data[0].platformId;
                        var postTypeId = data[0].postTypeId;
                        var postCustomValue = data[0].postCustomName
        
                        var postName = postTypeMasters.find(x => x.platformId == platformId && x.postTypeId == postTypeId);

                        var html = ``;
                        // if (obj) {
                        var platform = platformMasters.find(x => x.platformId == platformId);
                        var post = postTypeMasters.find(x => x.postTypeId == postTypeId);
                  
                        var logo = "";
                        if (platform && platform.platformName && post && post.postTypeName) {
                            if (platform.platformName == "Instagram") {
                                logo = `<i class="fab fa-instagram"></i> `
                            } if (platform.platformName == "Youtube") {
                                
                                logo = ` <i class="fab fa-youtube"></i> `
                            } if (platform.platformName == "TikTok") {
                             
                                logo = ` <img style="width: 18px; height: 18px; margin-bottom: 5px;"; src="/static/svgs/brands/tiktok.svg"></img>`
                            }
                            if(post.postTypeName == "Custom"){
                            html += `<a style=" color: #2f008d; font-size: 1.2rem;">${logo}</a>    <span style="padding-left:3px"> ${postCustomValue}</span>`;
                            }else{
                                html += `<a style=" color: #2f008d; font-size: 1.2rem;">${logo}</a>    <span style="padding-left:3px"> ${post.postTypeName}</span>`;
                            }
                            return html;
                        }
                    } else {
                        return '-';
                    }
                }
            },
            {
                targets: 3,
                className: "text-left",
                width: "27%",
                render: function (data, type, row, meta) {
                    var influencerId = row.influencerId;
                    var campaignId = row.campaignId;
                    if (row.proposalStatus == "PRODUCT_FULFILLED") {
                        return `
                     <div>  <input class="w-75" placeholder="Post URL" max=50  id='postUrl_${row.influencerId}'><span style="background-color:red" class="errorMsg" id="errorpostUrl_${row.influencerId}></span><div>`;
                        // onfocusout="statisticsApproval('reject','${campaignId}','${influencerId}','#postUrl_${influencerId}','#errorpostUrl_${influencerId}')" type="text"  id='postUrl_${row.influencerId}'/
                    } if (row.proposalStatus == "TRACK_POST" || campaignStatusFromCampaignWF == "ENDED") {
                        return `
                        <div>  <input disabled max=50 value=${row.postUrl}   type="text"  id='postUrl_${row.influencerId}'/><span style="background-color:red" class="errorMsg" id="errorpostUrl_${row.influencerId}></span><div>`;
                    }
                }
            },
            {
                targets: 4,
                width: "28%",
                data: "proposalStatus",
                className: "text-left",
                render: function (data, type, row, meta) {
                    var obj = {
                        PRODUCT_FULFILLED: "Awaiting Post Link",
                        TRACK_POST: "Post URL Performance"
                    }
                    var status = obj[data];
                    return  `<a style="color: #2f008d";>${status}</a>`
                }

            },
            {
                targets: 5,
                width: "20%",
                className: "text-center",
                render: function (data, type, row, meta) {
                    // console.log("checkbox render", meta.settings.aoData[0]._aData.influencerId);

                    var influencerId = row.influencerId;
                    var campaignId = row.campaignId;
                    var proposalStatus = row.proposalStatus;
                    if (campaignStatusFromCampaignWF == "ENDED") {
                        return `
                        <div class="dropdown">
                        <span class="dot-icon disabled"><i class="fa fa-ellipsis-v"></i></span>
                    </div>
                    `;
                    }

                    else if (proposalStatus == "PRODUCT_FULFILLED") {
                        return `
         <div class="dropdown fulFillProductAction" id=${meta.settings.aoData[0]._aData.influencerId}>
             <span class="dot-icon"><i class="fa fa-ellipsis-v"></i></span>
             <ul class="dropdown-content">
                 <li id="accept" onclick="statisticsApproval('accept','${campaignId}','${influencerId}','#postUrl_${influencerId}','#errorpostUrl_${influencerId}')">Save</li>
             </ul>
         </div>
     ` } else {
                        return `
                            <div class="dropdown fulFillProductAction" id=${meta.settings.aoData[0]._aData.influencerId}>
                                <span class="dot-icon"><i class="fa fa-ellipsis-v"></i></span>
                                <ul class="dropdown-content">
                                    <li ><a target=_blank style="color:black" href=${row.postUrl}>View Post</a></li>
                                </ul>
                            </div>
                        `

                    };
                }
            },
            {
                className: 'control ',
                targets: [0, 2,3, 5],
                orderable: false
            }
        ],
        select: {
            style: 'multi'
        },
        order: [
            [1, 'asc']
        ],
        
        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            var rowId = data.DT_RowId;
            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rowsSelected) !== -1) {
                $(row).find('input[type="checkbox"]').prop(
                    'checked', true);
                $(row).addClass('selected');
            }
        }
    });


    $('#table_campaign_statistics tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');
        // Get row data
        var data = campaignWFSTDT.row($row).data();
        // Get row ID
        var rowId = data.DT_RowId;
        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rowsSelected);
        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rowsSelected.push(rowId);
            // Otherwise, if checkbox is not checked and row ID is in list of
            // selected
            // row IDs
        } else if (!this.checked && index !== -1) {
            rowsSelected.splice(index, 1);
        }
        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(campaignWFSTDT);
        // Prevent click event from propagating to parent
        e.stopPropagation();
    });
}


//api call
function statisticsApproval(status, campaignId, influencerId, postUrlId, errorpostUrlId) {
    // console.log("Event data of postUrl : ", status, " : ", postUrlId);
    if ($(postUrlId).val() == "") {
        toast("Post Url Field is Required", "error");
        return
    } else {
        // console.log($(postUrlId).val(), "else url value");
        var postUrl = $(postUrlId).val()
        var requestFulfillProductData = {
            campaignId: campaignId,
            influencerId: influencerId,
            status: status,
            workflowStatus: "STATUS_UPDATE",
            postUrl: postUrl
        }

        $.ajax({
            url: "/brand/view-campaign-v1/view-influencers/brandapproval",
            type: "PUT",
            data: JSON.stringify(requestFulfillProductData),
            dataType: "json",
            contentType: "application/json",
        }).done(function (data) {
            // data = JSON.parse(data);
            if (data.code == "600") {
                toast("Saved successfully");
                // var someTabTriggerEl = document.querySelector('#statistics-tab');
                // var tab = new bootstrap.Tab(someTabTriggerEl);
                // tab.show();
                campaignWFSTDT.ajax.reload(null, false);
            } else if (data.code == "651") {
                toast("Post URL is invalid","error");
                return
            }
            else {
                toast(data.errors.message);
            }

            //window.location.href = "/brand/view-campaign-v1/"+msg.campaignId;
            // $("#campaignForm").trigger("reset");
        }).fail(function (jqXHR, textStatus) {
            toast("Request Failed " + textStatus, "error")
        });
    }
}

