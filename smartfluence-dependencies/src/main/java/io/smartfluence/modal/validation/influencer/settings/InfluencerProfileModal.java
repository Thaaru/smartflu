package io.smartfluence.modal.validation.influencer.settings;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerProfileModal {

	@NotBlank(message = "Enter your paypal email")
	@Email(message = "Enter your valid paypal emaild")
	private String paypalEmail;

	@NotBlank(message = "Enter your address1")
	private String address1;

	@NotBlank(message = "Enter your address2")
	private String address2;

	@NotBlank(message = "Enter your city")
	private String city;

	@NotBlank(message = "Enter your country")
	private String country;

	private String dob;

	@NotBlank(message = "Enter your gender")
	@Pattern(regexp = "MALE|FEMALE", message = "Enter valid gender")
	private String gender;

	@NotBlank(message = "Enter your state")
	private String state;
}