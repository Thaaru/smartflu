package io.smartfluence.mysql.entities.influencer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignInfluencerProductId implements Serializable {

//    @Column(name = "campaign_id")
    private String campaignId;
//    @Column(name = "influencer_id")
    private String influencerId;
//    @Column(name = "product_id")
    private int productId;
}
