package io.smartfluence.mysql.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import io.smartfluence.constants.BidStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bid_status_mapping")
public class BidStatusMapping implements Serializable {

	private static final long serialVersionUID = 2812003270452567574L;

	@Id
	private String bidId;

	@Column(name = "bid_status")
	@Enumerated(EnumType.STRING)
	private BidStatus bidStatus;
}