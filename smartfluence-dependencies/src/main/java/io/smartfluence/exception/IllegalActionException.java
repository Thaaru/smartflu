package io.smartfluence.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.CONFLICT)
public class IllegalActionException extends RuntimeException {

	private static final long serialVersionUID = 8548479417417598762L;

	public IllegalActionException(String message) {
		super(message);
	}
}