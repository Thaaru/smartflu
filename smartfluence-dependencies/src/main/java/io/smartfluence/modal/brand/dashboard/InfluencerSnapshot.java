package io.smartfluence.modal.brand.dashboard;

import java.util.Date;
import java.util.UUID;

import io.smartfluence.constants.BidStatus;
import io.smartfluence.modal.Copyable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InfluencerSnapshot implements Copyable {

	private static final long serialVersionUID = 2299585990879653040L;

	private String influencerName;

	private String influencerHandle;

	private Date bidDate;

	private String postType;

	private String postDuration;

	private String paymentType;

	private String bidAmount;

	private BidStatus bidStatus;

	private UUID bidId;

	private UUID influencerId;

	private Boolean isPostUrlPresent;

	private String affiliateType;

	private String bidAmountName;

	public Long getBidDateTime() {
		return bidDate == null ? 0 : bidDate.getTime();
	}

	public String getInfluencerLink() {
		return "https://www.instagram.com/" + influencerHandle.substring(1);
	}

	public String getPostDuration() {
		switch (postDuration) {
		case "2H":
			postDuration = "2 hours";
			break;
		case "6H":
			postDuration = "6 hour";
			break;
		case "12H":
			postDuration = "12 hour";
			break;
		case "24H":
			postDuration = "24 hour";
			break;
		case "1W":
			postDuration = "1 week";
			break;
		case "P":
			postDuration = "Permanent";
			break;
		default:
			break;
		}
		return postDuration;
	}
}