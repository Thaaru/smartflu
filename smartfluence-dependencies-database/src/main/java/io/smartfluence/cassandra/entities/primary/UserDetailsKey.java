package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import io.smartfluence.constants.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailsKey implements Serializable {

	private static final long serialVersionUID = -152632305585671937L;

	public UserDetailsKey(UserDetailsKey key) {
		this.userStatus = key.getUserStatus();
		this.userId = key.getUserId();
	}

	@PrimaryKeyColumn(name = "user_status", type = PrimaryKeyType.PARTITIONED)
	@Enumerated(EnumType.STRING)
	private UserStatus userStatus;

	@PrimaryKeyColumn(name = "user_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID userId;
}