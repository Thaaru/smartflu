package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VideoStories implements Serializable {

	private static final long serialVersionUID = -7896843853770390867L;

	@JsonAlias("type")
	private Integer type;

	@JsonAlias("width")
	private Integer width;

	@JsonAlias("height")
	private Integer height;

	@JsonAlias("url")
	private String videoUrl;

	@JsonAlias("id")
	private String id;
}