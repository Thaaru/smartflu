package io.smartfluence.webapp.proxy;

import java.util.List;
import java.util.UUID;

import io.smartfluence.modal.utill.ResponseObj;
import io.smartfluence.modal.validation.brand.explore.CampaignInfluencerReqEntity;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.smartfluence.constants.BidStatus;

@FeignClient(name = "smartfluence-brand-management")
@RibbonClient(name = "smartfluence-brand-management")
public interface BrandManagementServiceProxy {

	@GetMapping("brand/profile-details")
	ResponseEntity<Object> getProfileDetails();

	@GetMapping("brand/email-connection")
	ResponseEntity<Object> getEmailConnectionSettings();

	@GetMapping("brand/social-connect")
	ResponseEntity<Object> getBrandSocialConnect();

	@PostMapping("brand/social-connect")
	ResponseEntity<Object> updateBrandSocialConnect(@RequestParam String instagramHandle);

	@GetMapping("brand/payment-preference")
	ResponseEntity<Object> getBrandPaymentPreference();

	@PostMapping("brand/payment-preference")
	ResponseEntity<Object> updateBrandPaymentPreference(@RequestParam String mailingFullName,
			@RequestParam String mailingAddress, @RequestParam String mailingCity, @RequestParam String mailingZipCode,
			@RequestParam String mailingCountry, @RequestParam String billingFullName,
			@RequestParam String billingAddress, @RequestParam String billingCity, @RequestParam String billingZipCode,
			@RequestParam String billingCountry, @RequestParam String creditCardNumber,
			@RequestParam String cardHolderName, @RequestParam String expiryDate, @RequestParam String cvv,
			@RequestParam boolean mailBill);

	@GetMapping("brand/get-influencers")
	ResponseEntity<Object> getInfluencers(@RequestParam String audienceBrandName, @RequestParam String audienceGeoName, @RequestParam String audienceGender,
			@RequestParam List<String> audienceAge, @RequestParam String comparisonProfile, @RequestParam String influencerGeoName,
			@RequestParam String influencerGender, @RequestParam String influencerAge, @RequestParam Integer followersMin,
			@RequestParam Integer followersMax, @RequestParam Integer engagementMin, @RequestParam Integer engagementMax,
			@RequestParam String keyword, @RequestParam String profileBio, @RequestParam String hashTag, @RequestParam String withContact,
			@RequestParam Boolean verified, @RequestParam Boolean sponsoredPost, @RequestParam String sortBy, @RequestParam String platform,
			@RequestParam Integer limitCount, @RequestParam Integer skipCount, @RequestParam Boolean isNewSearch);

	@GetMapping("brand/view-influencer/{userId}/profile-image")
	ResponseEntity<byte[]> viewInfluencerProfilePic(@PathVariable String userId);

	@GetMapping("brand/view-influencer/{userId}/profile-name")
	ResponseEntity<Object> viewInfluencerProfileName(@PathVariable UUID userId);

	@GetMapping("brand/influencer-demographic-details")
	ResponseEntity<Object> getInfluencerDemographicDetails(@RequestParam String influencerId, @RequestParam boolean isValid,
			@RequestParam String platform, @RequestParam(required = false) UUID searchId);

	@PostMapping("brand/view-influencer/submit-bid")
	ResponseEntity<Object> submitBid(@RequestParam String postType, @RequestParam String postDuration,
			@RequestParam String paymentType, @RequestParam String includedTag, @RequestParam String generalMessage,
			@RequestParam Double bidAmount, @RequestParam String platform, @RequestParam UUID campaignId,
			@RequestParam String affiliateType, @RequestParam String campaignName, @RequestParam UUID mailTemplateId,
			@RequestParam String mailDescription, @RequestParam String mailSubject, @RequestParam String mailCc,
			@RequestParam boolean isValid, @RequestParam String influencerId, @RequestParam String influencerHandle,
			@RequestParam String reportId, @RequestParam boolean reportPresent);

	@PostMapping("brand/view-influencer/send-proposal")
	ResponseEntity<Object> sendProposal(@RequestParam String postType, @RequestParam String postDuration,
			@RequestParam String paymentType, @RequestParam String includedTag, @RequestParam String generalMessage,
			@RequestParam Double bidAmount, @RequestParam String platform, @RequestParam UUID campaignId,
			@RequestParam String affiliateType, @RequestParam String campaignName, @RequestParam UUID mailTemplateId,
			@RequestParam String mailDescription, @RequestParam String mailSubject, @RequestParam String mailCc,
			@RequestParam boolean isValid, @RequestParam String influencerId, @RequestParam String influencerHandle,
			@RequestParam String reportId, @RequestParam boolean reportPresent);

	@GetMapping("brand/view-influencer/{userName}/chartData/{platform}")
	ResponseEntity<Object> getInfluencerChartData(@PathVariable String userName, @PathVariable String platform);

	@GetMapping("brand/view-influencer/{userId}/content/{contentId}")
	ResponseEntity<byte[]> viewInfluencerContentImage(@PathVariable String userId, @PathVariable Integer contentId, @RequestParam String platform);

	@GetMapping("brand/view-influencer/{userId}/stories/{type}")
	ResponseEntity<Object> viewInfluencerInstagramStories(@PathVariable String userId, @PathVariable Integer type);

	@GetMapping("brand/influencer-snapshot")
	ResponseEntity<Object> getInfluencerSnapshot(@RequestParam String strCampaignId);

	@GetMapping("brand/credit-balance")
	ResponseEntity<Object> getCreditBalance();

	@PostMapping("brand/update-profile")
	ResponseEntity<Object> updateProfileDetails(@RequestParam String brandName, @RequestParam String firstName,
			@RequestParam String lastName, @RequestParam String industry, @RequestParam String website, @RequestParam String email);

	@PostMapping("brand/change-password")
	ResponseEntity<Object> changePassword(@RequestParam String oldPassword, @RequestParam String newPassword);

	@PostMapping("brand/cards/save")
	ResponseEntity<Object> saveCard(@RequestParam String stripeToken, @RequestParam(defaultValue = "false", required = false) Boolean defaultSource);

	@PostMapping(path = "brand/plans/activate/{planId}")
	ResponseEntity<Object> activatePlan(@PathVariable String planId);

	@PostMapping("brand/cards/remove")
	ResponseEntity<Object> removeCard(String cardId);

	@PostMapping("brand/cards/default")
	ResponseEntity<Object> defaultCard(String cardId);

	@PostMapping("brand/save-campaign")
	ResponseEntity<Object> saveCampaign(@RequestParam String campaignName);

	@GetMapping("brand/get-campaign")
	ResponseEntity<Object> getCampaign(@RequestParam boolean isAllCampaign);

	@PostMapping("brand/view-influencer/add-to-campaign")
	ResponseEntity<Object> addToCampaign(@RequestParam String campaignName, @RequestParam UUID campaignId,
			@RequestParam boolean isValid, @RequestParam String influencerId, @RequestParam String influencerHandle,
			@RequestParam String reportId, @RequestParam boolean reportPresent, @RequestParam String platform);

//	@PostMapping("brand/view-influencer/add-to-new-campaign")
//	ResponseEntity<Object> addToNewCampaign(@RequestParam String campaignName, @RequestParam UUID campaignId,
//										 @RequestParam boolean isValid, @RequestParam String influencerId, @RequestParam String influencerHandle,
//										 @RequestParam String reportId, @RequestParam boolean reportPresent, @RequestParam String platform);

	@PostMapping("brand/view-influencer/add-to-new-campaign")
	ResponseEntity<Object> addInfluencerToNewCampaign(@RequestBody List<CampaignInfluencerReqEntity> campaignInfluencerReqEntities);


	@GetMapping("brand/campaign-check")
	ResponseEntity<Object> getCampaignCheck(@RequestParam String userIdStr);

	@GetMapping("brand/get-campaign-by-influencer")
	ResponseEntity<Object> getCampaignByInfluencer();

	@GetMapping("brand/get-influencer-by-campaign")
	ResponseEntity<Object> getInfluencerByCampaign(@RequestParam String strCampaignId);

	@PostMapping("brand/view-campaign/{campaignId}/save-status")
	ResponseEntity<Object> saveStatus(@RequestParam BidStatus status, @RequestParam UUID bidId,
			@RequestParam UUID influencerId, @PathVariable UUID campaignId);

	@PostMapping("brand/view-campaign/{campaignId}/remove-influencer")
	ResponseEntity<Object> removeInfluencer(@RequestParam UUID influencerId, @PathVariable UUID campaignId);

	@GetMapping("brand/get-campaign-name")
	HttpEntity<Object> getCampaignName(@RequestParam String strCampaignId);

	@GetMapping("brand/view-influencer/{influencerHandle}/check-influencer")
	ResponseEntity<Object> checkInfluencer(@RequestParam String campaignName, @RequestParam UUID campaignId,
			@PathVariable String influencerHandle, @RequestParam String influencerId, @RequestParam String platform);

	@GetMapping("brand/campaign/{campaignId}/analytics")
	ResponseEntity<Object> getAnalyticsData(@PathVariable UUID campaignId);

	@GetMapping("brand/reports/audience-data")
	ResponseEntity<Object> influencerAudienceDataReport(@RequestParam String influencerId, @RequestParam String influencerHandle,
			@RequestParam String deepSocialInfluencerId, @RequestParam String reportId,
			@RequestParam boolean reportPresent, @RequestParam String platform);

	@GetMapping("brand/reports/{influencerHandle}/audience-data/{reportId}")
	ResponseEntity<byte[]> downloadReport(@PathVariable String influencerHandle, @PathVariable String reportId);

	@GetMapping("brand/search-influencer")
	ResponseEntity<Object> searchInfluencer(@RequestParam String influencerHandle, @RequestParam String deepSocialInfluencerId,
			@RequestParam String platform);

	@PostMapping("brand/view-campaign/save-post-link")
	ResponseEntity<Object> savePostLink(@RequestParam UUID influencerId, @RequestParam String paymentType, @RequestParam String paymentMode,
			@RequestParam String postType, @RequestParam String postDuration, @RequestParam UUID bidId, @RequestParam Double bidAmount,
			@RequestParam String postURL, @RequestParam String postName, @RequestParam boolean isNew, @RequestParam UUID campaignId,
			@RequestParam String platform);

	@GetMapping("brand/get-campaign-post-details")
	HttpEntity<Object> getCampaignPostLinkDetails(@RequestParam String strCampaignId);

	@GetMapping("brand/campaign/{campaignId}/postAnalytics")
	ResponseEntity<Object> getPostAnalyticsData(@PathVariable UUID campaignId);

	@GetMapping("brand/view-campaign/{campaignId}/export-campaign-csv")
	ResponseEntity<String> exportCampaignCsv(@PathVariable UUID campaignId);

	@GetMapping("brand/view-campaign/{campaignId}/download-campaign-csv/{csvName}")
	ResponseEntity<byte[]> downloadCampaignCsv(@PathVariable UUID campaignId, @PathVariable UUID csvName);

	@GetMapping("brand/view-campaign/{campaignId}/get-influencer-bid-count")
	ResponseEntity<Object> getInfluencerBidCount(@RequestParam UUID influencerId, @RequestParam UUID bidId, @PathVariable UUID campaignId);

	@GetMapping("brand/get-suggestions/{influencerHandle}")
	ResponseEntity<Object> getAutoSuggestions(@PathVariable String influencerHandle, @RequestParam String platform);

	@GetMapping("brand/get-utilities/{name}")
	ResponseEntity<Object> getUtilities(@PathVariable String name, @RequestParam String data);

	@GetMapping("brand/get-mail-templates")
	ResponseEntity<Object> getMailTemplates();

	@PostMapping("brand/view-campaign/save-mail-template")
	ResponseEntity<Object> saveMailTemplate(@RequestParam String templateId, @RequestParam String templateBody,
			@RequestParam String templateName, @RequestParam String templateSubject, @RequestParam boolean isNew);

	@PostMapping("brand/view-campaign/send-bulk-mail")
	ResponseEntity<Object> sendBulkMail(@RequestParam UUID campaignId, @RequestParam List<String> influencerIds, @RequestParam UUID templateId);

	@PostMapping("brand/view-campaign/send-bulk-mail-new")
	ResponseEntity<Object> sendBulkMailNew(@RequestParam UUID campaignId, @RequestParam List<String> influencerIds, @RequestParam UUID templateId);

	@GetMapping("brand/get-settings/{userEmail}/get-tokenURL")
	ResponseEntity<String> getTokenURL(@PathVariable String userEmail);

	@GetMapping("brand/get-settings/{code}/get-accessToken")
	ResponseEntity<String> getAccessToken(@PathVariable String code);

	@GetMapping("brand/get-settings/remove-account")
	ResponseEntity<String> removeAccount();

	@PostMapping("brand/view-campaign/{campaignId}/upload-bulk-influencers")
	ResponseEntity<Object> uploadInfluencers(@RequestParam String influencerHandles, @RequestParam List<String> workbookSheets,
			@PathVariable UUID campaignId);

	@GetMapping("brand/view-campaign/download-template/upload-influencers")
	ResponseEntity<byte[]> downloadTemplate();

	@PostMapping("brand/view-campaign/{campaignId}/delete-campaign")
	ResponseEntity<Object> deleteCampaign(@RequestParam UUID campaignId);

	@GetMapping("brand/sf-promote/get-campaigns")
	ResponseEntity<Object> getPromoteCampaigns();

	@GetMapping("brand/sf-promote/view-campaign")
	ResponseEntity<Object> getPromoteCampaignDetails(@RequestParam UUID campaignId, @RequestParam String pageType);

	@GetMapping("brand/sf-promote/get-influencers")
	ResponseEntity<Object> getPromoteCampaignInfluencers(@RequestParam UUID campaignId);

	@PostMapping("brand/sf-promote/manage-promote-campaign")
	ResponseEntity<Object> managePromoteCampaign(@RequestParam String campaignId, @RequestParam String campaignName,
			@RequestParam String brandDescription, @RequestParam String campaignDescription, @RequestParam String postType,
			@RequestParam Double payment, @RequestParam String product, @RequestParam Double productValue, @RequestParam String requirements,
			@RequestParam String restrictions, @RequestParam String activeUntil, @RequestParam String comparisonProfile, @RequestParam String platform,
			@RequestParam String followersRange, @RequestParam String engagementsRange, @RequestParam String audienceIndustry,
			@RequestParam String influencerIndustry, @RequestParam String audienceGeo, @RequestParam String influencerGeo,
			@RequestParam String audienceAge, @RequestParam String influencerAge, @RequestParam String audienceGender,
			@RequestParam String influencerGender, @RequestParam Boolean isEdit);

	@PostMapping("brand/sf-promote/process-campaign")
	ResponseEntity<String> processPromoteCampaign(@RequestParam UUID campaignId, @RequestParam String action);

	@PostMapping("brand/sf-promote/influencer-action")
	ResponseEntity<String> influencerStatus(@RequestParam UUID campaignId, @RequestParam String influencerDeepSocialId, @RequestParam String action);

	@GetMapping("brand/sf-promote/get-promote-mail-template")
	ResponseEntity<Object> getPromoteMailTemplate(@RequestParam UUID campaignId);

	@PostMapping("brand/view-campaign/copy-influencers")
	ResponseEntity<Object> copyInfluencers(@RequestParam UUID campaignId, @RequestParam List<String> influencerIds);

	@PostMapping("brand/view-campaign/edit-campaign-name")
	ResponseEntity<Object> editCampaignName(@RequestParam UUID campaignId, @RequestParam String campaignName);


}