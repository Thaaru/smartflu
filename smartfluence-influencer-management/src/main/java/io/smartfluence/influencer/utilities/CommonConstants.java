package io.smartfluence.influencer.utilities;



public class CommonConstants {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private static final String EXPIRED = "expired";
    public static final String SUCCESS_CODE = "600";
    public static final String EXPIRE_CODE = "601";
    public static final String ERROR_CODE = "604";
    public static final String NO_DATA_CODE = "615";

}
