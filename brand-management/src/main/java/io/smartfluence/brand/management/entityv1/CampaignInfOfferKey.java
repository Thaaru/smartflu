package io.smartfluence.brand.management.entityv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignInfOfferKey implements Serializable {
//    @Id
//    @Column(name = "campaign_id")
    private String campaignId;
//    @Column(name = "influencer_id")
    private String influencerId;
//    @Column(name = "offer_type")
//    @Enumerated(EnumType.STRING)
    private String offerType;
}
