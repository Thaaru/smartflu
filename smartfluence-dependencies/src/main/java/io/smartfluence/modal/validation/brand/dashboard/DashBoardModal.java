package io.smartfluence.modal.validation.brand.dashboard;

import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class DashBoardModal {

	private UUID campaignId;

	private String campaignName;

	private List<String> influencerIds;

	private UUID templateId;
}