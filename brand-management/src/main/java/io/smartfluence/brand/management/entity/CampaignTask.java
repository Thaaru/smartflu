package io.smartfluence.brand.management.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.*;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "campaign_task")
public class CampaignTask implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "campaign_task_id")
    private Long campaignTaskId;

    @Column(name = "campaign_id")
    private String campaignId;

    @Column(name = "platform_id")
    private int platformId;

    @Column(name = "post_type")
    private String postType;

    @Column(name = "guidelines")
    private String guidelines;

    @Column(name = "post_custom_name")
    private String postCustomName;
}
