package io.smartfluence.brand.management.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import io.smartfluence.modal.validation.brand.explore.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import feign.FeignException;
import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.dao.ExploreInfluencerDao;
import io.smartfluence.brand.management.util.SendMailUtil;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.TemporaryInfluencerContent;
import io.smartfluence.cassandra.entities.brand.BrandCreditAllowance;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSearchHistory;
import io.smartfluence.cassandra.entities.brand.primary.BrandSearchHistoryKey;
import io.smartfluence.cassandra.entities.instagram.InstagramContent;
import io.smartfluence.cassandra.entities.instagram.InstagramCountryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.instagram.InstagramIndustryDemographic;
import io.smartfluence.cassandra.entities.instagram.InstagramStateDemographic;
import io.smartfluence.cassandra.entities.primary.InfluencerContentKey;
import io.smartfluence.cassandra.entities.primary.ReportSearchHistoryKey;
import io.smartfluence.cassandra.entities.primary.TemporaryInfluencerContentKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.entities.tiktok.TikTokContent;
import io.smartfluence.cassandra.entities.tiktok.TikTokCountryDemographic;
import io.smartfluence.cassandra.entities.tiktok.TikTokDemographics;
import io.smartfluence.cassandra.entities.youtube.YouTubeContent;
import io.smartfluence.cassandra.entities.youtube.YouTubeCountryDemographic;
import io.smartfluence.cassandra.entities.youtube.YouTubeDemographics;
import io.smartfluence.cassandra.repository.instagram.InstagramContentRepo;
import io.smartfluence.cassandra.repository.tiktok.TikTokContentRepo;
import io.smartfluence.cassandra.repository.youtube.YouTubeContentRepo;
import io.smartfluence.constants.CreditType;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.constants.ViewType;
import io.smartfluence.modal.brand.exploreinfluencer.GetInfluencersResponseModel;
import io.smartfluence.modal.brand.exploreinfluencer.SearchInfluencerResponseModel;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.brand.viewinfluencer.InfluencerDemographicModel;
import io.smartfluence.modal.brand.viewinfluencer.TopInfluencerPost;
import io.smartfluence.modal.brand.viewinfluencer.InfluencerDemographicModel.InfluencerDemographicModelBuilder;
import io.smartfluence.modal.social.report.ReportData;
import io.smartfluence.modal.social.search.request.CodeWeight;
import io.smartfluence.modal.social.search.request.ComparisonProfile;
import io.smartfluence.modal.social.search.request.CustomList;
import io.smartfluence.modal.social.search.request.Filter;
import io.smartfluence.modal.social.search.request.IndustryAndLocation;
import io.smartfluence.modal.social.search.request.InfluencersDataRequest;
import io.smartfluence.modal.social.search.request.Paging;
import io.smartfluence.modal.social.search.request.Range;
import io.smartfluence.modal.social.search.request.Relevance;
import io.smartfluence.modal.social.search.request.SortResponse;
import io.smartfluence.modal.social.search.request.WithContact;
import io.smartfluence.modal.social.search.response.InfluencersDataResponse;
import io.smartfluence.modal.social.user.InfluencerAutoSuggestions;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserFeed;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserFeedItems;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserStories;
import io.smartfluence.modal.social.user.tiktok.RawTikTokAuthor;
import io.smartfluence.modal.social.user.tiktok.RawTikTokUserFeed;
import io.smartfluence.modal.social.user.tiktok.RawTikTokUserVideo;
import io.smartfluence.modal.social.user.youtube.RawYouTubeChannelInfo;
import io.smartfluence.modal.social.user.youtube.UploadPreview;
import io.smartfluence.modal.social.user.youtube.Videos;
import io.smartfluence.mysql.entities.CampaignInfluencerMapping;
import io.smartfluence.mysql.entities.IndustryMaster;
import io.smartfluence.mysql.entities.InfluencerDataQueue;
import io.smartfluence.mysql.entities.LocationMaster;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.entities.primary.InfluencerDataQueueKey;
import io.smartfluence.props.SearchInfluencerProps;
import io.smartfluence.proxy.social.SocialDataProxy;
import io.smartfluence.util.image.ImageProcessor;
import lombok.extern.log4j.Log4j2;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class ExploreInfluencerServiceImpl implements ExploreInfluencerService {

	private static final int STATES_COUNT = 5;

	private static final int INDUSTRY_COUNT = 3;

	private static final int COUNTRY_COUNT = 5;

	private static final double _10_POW_16 = Math.pow(10.0, 16.0);

	private static final String GENDER_FEMALE = "Female";

	private static final String GENDER_MALE = "Male";

	private static final String AGE_65 = "65+";

	private static final String AGE_45_64 = "45-64";

	private static final String AGE_35_44 = "35-44";

	private static final String AGE_25_34 = "25-34";

	private static final String AGE_18_24 = "18-24";

	private static final String AGE_13_17 = "13-17";

	private static final String LOCATION_UTILITY = "location";

	private static final String INDUSTRY_UTILITY = "industry";

	@Autowired
	private ExploreInfluencerDao exploreInfluencerDao;

	@Autowired
	private InstagramContentRepo instagramContentRepo;

	@Autowired
	private YouTubeContentRepo youtubeContentRepo;

	@Autowired
	private TikTokContentRepo tiktokContentRepo;

	@Autowired
	private SocialDataProxy socialDataProxy;

	@Autowired
	private SearchInfluencerProps searchInfluencerProps;

	@Autowired
	private ReportsService reportsService;

	@Autowired
	private SendMailUtil sendMailUtil;

	final static Logger LOG = Logger.getLogger(ExploreInfluencerServiceImpl.class.getName());

	@Override
	public ResponseEntity<Object> submitBid(SubmitBidModel submitBidModel) {
		String paymentType = submitBidModel.getPaymentType();
		if (!paymentType.equals("Affiliate")) {
			String postDuration = submitBidModel.getPostDuration();
			String postType = submitBidModel.getPostType();
			if (postType.isEmpty())
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Select post type");

			if (postDuration.isEmpty() && submitBidModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name()))
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Select post Duration");

			submitBidModel.setAffiliateType(null);
		}
		return exploreInfluencerDao.submitBid(submitBidModel);
	}

	@Override
	public ResponseEntity<Object> sendProposal(SubmitBidModel submitBidModel){
		return exploreInfluencerDao.sendProposal(submitBidModel);
	}

	@Override
	public ResponseEntity<Object> getInfluencerDemographicDetails(String influencerId, boolean isValid, String platform, UUID searchId) {

		try {
			InfluencerDemographicModelBuilder builder = InfluencerDemographicModel.builder().platform(platform);

			if (isValid) {
				boolean checkFreeAccount = reportsService.checkBrandCredit(ResourceServer.getUserId(), influencerId, CreditType.VIEW_INFLUENCER);
				if (checkFreeAccount)
					return getInfluencerDetails(influencerId, checkFreeAccount, null, false, builder, searchId);
				else {
					Optional<BrandCredits> brandCredits = exploreInfluencerDao.getBrandCredits(ResourceServer.getUserId());
					if (brandCredits.isPresent() && brandCredits.get().getCredits() > 0)
						return getInfluencerDetails(influencerId, true, brandCredits, true, builder, searchId);
					else {
						if (ResourceServer.isFreeBrand())
							return new ResponseEntity<>(builder.status(403).message("free").build(), HttpStatus.OK);
						else
							return new ResponseEntity<>(builder.status(403).message("You have run out of credits! "
									+ "Please contact help@smartfluence.io to increase your limit.").build(), HttpStatus.OK);
					}
				}
			} else
				return getReportDetails(influencerId, platform, builder, searchId);
		} catch (MalformedURLException e) {
			e.getStackTrace();
		} catch (IOException e) {
			e.getStackTrace();
		}
		return ResponseEntity.notFound().build();
	}

	private ResponseEntity<Object> getInfluencerDetails(String influencerId, boolean checkFreeAccount, Optional<BrandCredits> brandCredits,
			boolean saveCredits, InfluencerDemographicModelBuilder builder, UUID searchId) throws MalformedURLException, IOException {
		UUID userId;
		try {
			userId = UUID.fromString(influencerId);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
		Optional<SocialIdMaster> socialId = exploreInfluencerDao.getSocialIdMasterById(userId.toString());
		if (socialId.isPresent()) {
			InfluencerDemographicsMapper demographicsMapper = reportsService.getInfluencerDemographics(socialId.get(), null, null, false);
			if (saveCredits)
				reportsService.saveBrandCreditDetails(userId, brandCredits, CreditType.VIEW_INFLUENCER);
			return setInfluencerDetails(socialId.get(), ResourceServer.getUserId(), demographicsMapper, checkFreeAccount, builder,
					socialId.get().getPlatform(), searchId);
		}
		return ResponseEntity.notFound().build();
	}

	private ResponseEntity<Object> setInfluencerDetails(SocialIdMaster socialIdMaster, UUID brandId, InfluencerDemographicsMapper demographicsMapper,
			boolean checkFreeAccount, InfluencerDemographicModelBuilder builder, Platforms platform, UUID searchId)
					throws MalformedURLException, IOException {

		Optional<UserAccounts> userAccounts = exploreInfluencerDao.getUserAccounts(brandId.toString());

		builder.ageRanges(ageRanges(demographicsMapper)).audienceCheck(demographicsMapper.getCredibility())
			.genders(genders(demographicsMapper)).lastestEngagement(lastestEngagement()).avgLikes(demographicsMapper.getLikes())
			.profileImage(extractProfileImage(socialIdMaster, demographicsMapper, platform))
			.engagementRate(demographicsMapper.getEngagementRate())
			.avgComments(demographicsMapper.getComments()).avgViews(demographicsMapper.getViews()).avgDisLikes(demographicsMapper.getDisLikes())
			.followers(demographicsMapper.getFollowers()).engagement(demographicsMapper.getEngagement())
			.influencerHandle(demographicsMapper.getInfluencerHandle()).influencerId(socialIdMaster.getUserId())
			.lastPrice(demographicsMapper.getPricing()).subscribed(checkFreeAccount).isValid(true)
			.topContents(topContents(demographicsMapper.getInfluencerHandle(), platform.name()))
			.deepSocialInfluencerId(StringUtils.isEmpty(socialIdMaster.getInfluencerId()) ? "" : socialIdMaster.getInfluencerId()).status(200);

		reportsService.saveReportHistory(ReportSearchHistory.builder()
			.key(ReportSearchHistoryKey.builder().brandId(brandId).isNew(false).createdAt(new Date()).build())
			.influencerUsername(demographicsMapper.getInfluencerHandle()).brandName(userAccounts.get().getBrandName())
			.influencerDeepSocialId(StringUtils.isEmpty(socialIdMaster.getInfluencerId()) ? "" : socialIdMaster.getInfluencerId())
			.reportId(StringUtils.isEmpty(socialIdMaster.getInfluencerReportId()) ? "" : socialIdMaster.getInfluencerReportId())
			.viewType(ViewType.VIEW).platform(platform).searchId(searchId).build());
		saveBrandSearchHistory(brandId, searchId);

		return new ResponseEntity<>(builder.build(), HttpStatus.OK);
	}

	private ResponseEntity<Object> getReportDetails(String influencerId, String platform, InfluencerDemographicModelBuilder builder, UUID searchId)
			throws MalformedURLException, IOException {
		ReportData body = new ReportData();
		try {
			body = reportsService.getReportData(influencerId, platform, ViewType.VIEW, searchId);
			saveBrandSearchHistory(ResourceServer.getUserId(), searchId);
		} catch (FeignException fe) {
			if (fe.getMessage().contains("retry_later")) {
				builder.status(400).message("We are pulling data for this influencer! "
						+ "We will send out an email with a link to the influencer data in a few minutes.");
				Optional<UserAccounts> userAccounts = exploreInfluencerDao.getUserAccounts(ResourceServer.getUserId().toString());
				createInfluencerReportQueue(InfluencerDataQueue.builder().key(InfluencerDataQueueKey.builder()
						.brandId(userAccounts.get().getUserId()).influencer(influencerId)
						.viewType(ViewType.VIEW).build()).brandEmail(userAccounts.get().getEmail()).searchId(searchId.toString())
						.brandName(userAccounts.get().getBrandName()).platform(reportsService.getPlatform(platform)).build());
				return new ResponseEntity<>(builder.build(), HttpStatus.OK);
			}
			else if (fe.getMessage().contains("empty_audience_data")) {
				builder.status(400).message("Currently we do not have data for this influencer");
				return new ResponseEntity<>(builder.build(), HttpStatus.OK);
			}
			else {
				builder.status(500).message("Something went wrong while processing your request, please try again in some time.");
				return new ResponseEntity<>(builder.build(), HttpStatus.OK);
			}
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}

		UUID influencerSFId = UUID.fromString(reportsService.saveInfluencerReportData(body));
		boolean checkFreeAccount = reportsService.checkBrandCredit(ResourceServer.getUserId(), influencerSFId.toString(), CreditType.VIEW_INFLUENCER);

		if (!checkFreeAccount) {
			Optional<BrandCredits> brandCredits = exploreInfluencerDao.getBrandCredits(ResourceServer.getUserId());
			if (brandCredits.isPresent() && brandCredits.get().getCredits() < 1) {
				if (ResourceServer.isFreeBrand())
					return new ResponseEntity<>(builder.status(403).message("free").build(), HttpStatus.OK);
				else
					return new ResponseEntity<>(builder.status(403).message("You have run out of credits! "
							+ "Please contact help@smartfluence.io to increase your limit.").build(), HttpStatus.OK);
			}
			else {
				checkFreeAccount = true;
				reportsService.saveBrandCreditDetails(influencerSFId, brandCredits, CreditType.VIEW_INFLUENCER);
			}
		}

		setReportDetails(builder, body, influencerSFId, checkFreeAccount);

		return new ResponseEntity<>(builder.build(), HttpStatus.OK);
	}

	private void setReportDetails(InfluencerDemographicModelBuilder builder, ReportData body, UUID influencerSFId,
			boolean checkFreeAccount) throws MalformedURLException, IOException {
		builder.ageRanges(reportsService.getAgeRangesFromSocial(body.getAudienceFollowers().getData().getAudienceAge()))
				.audienceCheck(body.getAudienceFollowers().getData().getAudienceCredibility())
				.genders(reportsService.getGenderFromSocial(body.getAudienceFollowers().getData().getAudienceGender()))
				.lastestEngagement(lastestEngagement()).followers(body.getUserProfile().getFollowers())
				.engagement(body.getUserProfile().getEngagement())
				.engagementRate(body.getUserProfile().getEngagementRate())
				.influencerHandle(StringUtils.isEmpty(body.getUserProfile().getHandle())
						? "@" + body.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase() : "@" + body.getUserProfile().getHandle())
				.influencerId(influencerSFId.toString()).lastPrice(reportsService.getEstimatedPricing(body.getUserProfile().getEngagementRate(),
						body.getUserProfile().getFollowers(), body.getUserProfile().getEngagement(),
						body.getAudienceFollowers().getData().getAudienceCredibility()))
				.subscribed(checkFreeAccount).avgLikes(body.getUserProfile().getAvgLikes()).avgDisLikes(body.getUserProfile().getAvgDislikes())
				.avgComments(body.getUserProfile().getAvgComments()).avgViews(body.getUserProfile().getAvgViews())
				.topContents(topContents("@" + (StringUtils.isEmpty(body.getUserProfile().getHandle())
						? body.getUserProfile().getFullName().replaceAll(" ", "").toLowerCase()
								: body.getUserProfile().getHandle()), body.getUserProfile().getPlatformType()))
				.profileImage(new ImageProcessor(body.getUserProfile().getProfileImage()).processFinalImage())
				.isValid(true).reportPresent(true).reportId(body.getReportInfo().getReportId())
				.deepSocialInfluencerId(body.getUserProfile().getUserId()).status(200);
	}

	private void saveBrandSearchHistory(UUID brandId, UUID searchId) {
		if (!ObjectUtils.isEmpty(searchId)) {
			Optional<BrandSearchHistory> optionalBrandSearchHistory = exploreInfluencerDao
					.getBrandSearchHistoryById(BrandSearchHistoryKey.builder().brandId(brandId).searchId(searchId).build());

			if (optionalBrandSearchHistory.isPresent()) {
				BrandSearchHistory brandSearchHistory = optionalBrandSearchHistory.get();
				brandSearchHistory.setIsInfluencerMapped(true);
				exploreInfluencerDao.saveBrandSearchHistory(brandSearchHistory);
			}
		}
	}

	private byte[] extractProfileImage(SocialIdMaster socialIdMaster, InfluencerDemographicsMapper demographics, Platforms platform) {
		try {
			return new ImageProcessor(demographics.getProfileImage()).processFinalImage();
		} catch (MalformedURLException e) {
			e.getStackTrace();
		} catch (IOException e) {
			try {
				return new ImageProcessor(getInfluencerContent(demographics, platform)).processFinalImage();
			} catch (Exception f) {
				try {
					getUpdatedContent(socialIdMaster, demographics, platform);
					return new ImageProcessor(demographics.getProfileImage()).processFinalImage();
				} catch (IOException ex) {
					return null;
				}
			}
		}
		return null;
	}

	private String getInfluencerContent(InfluencerDemographicsMapper demographics, Platforms platform) {
		if (platform.equals(Platforms.INSTAGRAM)) {
			InfluencerContentKey influencerContentKey = new InfluencerContentKey(demographics.getInfluencerHandle(), 1);
			InstagramContent instagramContent = instagramContentRepo.findById(influencerContentKey).get();
			return instagramContent.getValue();
		}
		else if (platform.equals(Platforms.YOUTUBE)) {
			InfluencerContentKey influencerContentKey = new InfluencerContentKey(demographics.getInfluencerHandle(), 1);
			YouTubeContent youtubeContent = youtubeContentRepo.findById(influencerContentKey).get();
			return youtubeContent.getValue();
		}
		else if (platform.equals(Platforms.TIKTOK)) {
			InfluencerContentKey influencerContentKey = new InfluencerContentKey(demographics.getInfluencerHandle(), 1);
			TikTokContent tiktokContent = tiktokContentRepo.findById(influencerContentKey).get();
			return tiktokContent.getValue();
		}
		return null;
	}

	private void getUpdatedContent(SocialIdMaster socialIdMaster, InfluencerDemographicsMapper demographics, Platforms platform) {
		if (platform.equals(Platforms.INSTAGRAM))
			getInstagramUpdatedContent(demographics);
		else if (platform.equals(Platforms.YOUTUBE))
			getYoutubeUpdatedContent(socialIdMaster, demographics);
		else if (platform.equals(Platforms.TIKTOK))
			getTiktokUpdatedContent(socialIdMaster, demographics);
	}

	private void getInstagramUpdatedContent(InfluencerDemographicsMapper demographics) {
		InstagramDemographics instagramDemographics = exploreInfluencerDao.getInstagramDemographics(demographics.getInfluencerHandle()).get();
		List<InstagramContent> instagramContents = instagramContentRepo.findAllByKeyInfluencerHandle(demographics.getInfluencerHandle());

		RawInstagramUserFeed rawUserFeed = socialDataProxy.getInstagramUserFeed(demographics.getInfluencerHandle(), null);

		for (int i = 0; i < instagramContents.size(); i++) {
			if (i >= rawUserFeed.getItems().size())
				break;

			RawInstagramUserFeedItems rawUserFeedItem = rawUserFeed.getItems().get(i);
			demographics.setProfileImage(rawUserFeedItem.getUser().getProfilePicUrl());
			instagramDemographics.setProfileImage(rawUserFeedItem.getUser().getProfilePicUrl());
			instagramContents.get(i).setValue(rawUserFeedItem.getDisplayUrl());
			instagramContents.get(i).setUrl(getContentUrl("", rawUserFeedItem.getId(), 1));
		}
		exploreInfluencerDao.saveInstagramDemographics(instagramDemographics);
		exploreInfluencerDao.saveInstagramContents(instagramContents);
	}

	private void getYoutubeUpdatedContent(SocialIdMaster socialIdMaster, InfluencerDemographicsMapper demographics) {
		YouTubeDemographics youtubeDemographics = exploreInfluencerDao.getYouTubeDemographics(demographics.getInfluencerHandle()).get();
		List<YouTubeContent> youtubeContents = youtubeContentRepo.findAllByKeyInfluencerHandle(demographics.getInfluencerHandle());

		RawYouTubeChannelInfo rawYouTubeChannelInfo = socialDataProxy.getYouTubeChannelInfo(socialIdMaster.getInfluencerId());

		if (rawYouTubeChannelInfo.getSuccess()) {
			youtubeDemographics.setFollowers(rawYouTubeChannelInfo.getChannel().getFollowers().doubleValue());
			demographics.setFollowers(youtubeDemographics.getFollowers());
			youtubeDemographics.setProfileImage(rawYouTubeChannelInfo.getChannel().getProfileImage());
			demographics.setProfileImage(youtubeDemographics.getProfileImage());

			ResponseEntity<UploadPreview> uploadPreview = socialDataProxy.getYoutubeChannelFeed(socialIdMaster.getInfluencerId(), "dd");

			if (uploadPreview.getStatusCode().is2xxSuccessful()) {
				for (int i = 0; i < uploadPreview.getBody().getVideosList().getVideos().size(); i++) {
					if (i >= uploadPreview.getBody().getVideosList().getVideos().size())
						break;

					Videos videos = uploadPreview.getBody().getVideosList().getVideos().get(i);
					youtubeContents.get(i).setValue(videos.getCover());
					youtubeContents.get(i).setUrl(getContentUrl("", videos.getId(), 2));
				}
			}
		}
		exploreInfluencerDao.saveYoutubeDemographics(youtubeDemographics);
		exploreInfluencerDao.saveYoutubeContents(youtubeContents);
	}

	private void getTiktokUpdatedContent(SocialIdMaster socialIdMaster, InfluencerDemographicsMapper demographics) {
		TikTokDemographics tiktokDemographics = exploreInfluencerDao.getTikTokDemographics(demographics.getInfluencerHandle()).get();
		List<TikTokContent> tiktokContents = tiktokContentRepo.findAllByKeyInfluencerHandle(demographics.getInfluencerHandle());

		ResponseEntity<RawTikTokUserFeed> tiktokUserFeed = socialDataProxy.getTiktokUserFeed(socialIdMaster.getInfluencerId());

		if (tiktokUserFeed.getStatusCode().is2xxSuccessful()) {
			for (int i = 0; i < tiktokUserFeed.getBody().getUserFeed().getItems().size(); i++) {
				if (i >= tiktokUserFeed.getBody().getUserFeed().getItems().size())
					break;

				RawTikTokAuthor author = tiktokUserFeed.getBody().getUserFeed().getItems().get(i).getAuthor();
				tiktokDemographics.setProfileImage(author.getProfilePic());
				demographics.setProfileImage(tiktokDemographics.getProfileImage());
				RawTikTokUserVideo video = tiktokUserFeed.getBody().getUserFeed().getItems().get(i).getVideo();
				tiktokContents.get(i).setValue(video.getCover());
				tiktokContents.get(i).setUrl(getContentUrl("@" + author.getUniqueName(), video.getId(), 3));
			}
		}
		exploreInfluencerDao.saveTiktokDemographics(tiktokDemographics);
		exploreInfluencerDao.saveTiktokContents(tiktokContents);
	}

	private String getContentUrl(String user, String id, int flag) {
		if (flag == 1)
			return "https://www.instagram.com/p/" + id;
		else if (flag == 2)
			return "https://www.youtube.com/watch?v=" + id;
		else if (flag == 3)
			return "https://www.tiktok.com/" + user + "/video/" + id;
		return null;
	}

	private Map<String, Double> ageRanges(InfluencerDemographicsMapper demographics) {
		Map<String, Double> ageRanges = Maps.newLinkedHashMap();
		ageRanges.put(AGE_13_17, demographics.getAge13To17());
		ageRanges.put(AGE_18_24, demographics.getAge18To24());
		ageRanges.put(AGE_25_34, demographics.getAge25To34());
		ageRanges.put(AGE_35_44, demographics.getAge35To44());
		ageRanges.put(AGE_45_64, demographics.getAge45To64());
		ageRanges.put(AGE_65, demographics.getAge65ToAny());
		return ageRanges;
	}

	private Map<String, Double> genders(InfluencerDemographicsMapper demographics) {
		Map<String, Double> genders = Maps.newLinkedHashMap();
		genders.put(GENDER_MALE, demographics.getMale());
		genders.put(GENDER_FEMALE, demographics.getFemale());
		return genders;
	}

	private Map<String, Double> lastestEngagement() {
		return Maps.newLinkedHashMap();
	}

	private List<TopInfluencerPost> topContents(String userName, String platform) {

		if (platform.equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			return setInstagramContents(userName);
		if (platform.equalsIgnoreCase(Platforms.YOUTUBE.name()))
			return setYoutubeContents(userName);
		if (platform.equalsIgnoreCase(Platforms.TIKTOK.name()))
			return setTiktokContents(userName);

		return null;
	}

	private List<TopInfluencerPost> setInstagramContents(String userName) {
		List<TopInfluencerPost> topContents = Lists.newLinkedList();
		List<InstagramContent> instagramContents = instagramContentRepo.findAllByKeyInfluencerHandle(userName);

		for (InstagramContent instagramContent : instagramContents) {
			if (instagramContent.getValue() == null)
				continue;

			topContents.add(TopInfluencerPost.builder().url(instagramContent.getUrl()).value(instagramContent.getKey().getContentRank()).build());
		}
		return topContents;
	}

	private List<TopInfluencerPost> setYoutubeContents(String userName) {
		List<TopInfluencerPost> topContents = Lists.newLinkedList();
		List<YouTubeContent> youtubeContents = youtubeContentRepo.findAllByKeyInfluencerHandle(userName);

		for (YouTubeContent youtubeContent : youtubeContents) {
			if (youtubeContent.getValue() == null)
				continue;

			topContents.add(TopInfluencerPost.builder().url(youtubeContent.getUrl()).value(youtubeContent.getKey().getContentRank()).build());
		}
		return topContents;
	}

	private List<TopInfluencerPost> setTiktokContents(String userName) {
		List<TopInfluencerPost> topContents = Lists.newLinkedList();
		List<TikTokContent> tiktokContents = tiktokContentRepo.findAllByKeyInfluencerHandle(userName);

		for (TikTokContent tiktokContent : tiktokContents) {
			if (tiktokContent.getValue() == null)
				continue;

			topContents.add(TopInfluencerPost.builder().url(tiktokContent.getUrl()).value(tiktokContent.getKey().getContentRank()).build());
		}
		return topContents;
	}

	@Override
	public ResponseEntity<Object> viewInfluencerInstagramStories(String userId, Integer type) {
		InfluencerDemographicModelBuilder builder = InfluencerDemographicModel.builder();

		try {
			ResponseEntity<RawInstagramUserStories> instagramUserStories = socialDataProxy.getInstagramUserStories(userId);

			if (instagramUserStories.getStatusCode().is2xxSuccessful()) {
				if (!StringUtils.isEmpty(instagramUserStories.getBody().getReel())) {
					if ((type & 1) == 1)
						builder.images(getImageStories(instagramUserStories.getBody()));
					if ((type & 2) == 2)
						builder.videos(getVideoStories(instagramUserStories.getBody()));
				}
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return new ResponseEntity<>(builder.build(), HttpStatus.OK);
	}

	private List<byte[]> getImageStories(RawInstagramUserStories instagramStories) {
		List<byte[]> images = new LinkedList<>();

		if(!StringUtils.isEmpty(instagramStories)) {
			instagramStories.getReel().getItems().forEach(img -> {
				if (!StringUtils.isEmpty(img.getImages())) {
					for (int i = 0; i < img.getImages().getCandidates().size();) {
						try {
							images.add(new ImageProcessor(img.getImages().getCandidates().get(i).getImageUrl()).processFinalImage());
						} catch (Exception e) {
							e.getStackTrace();
						}
						break;
					}
				}
			});
		}
		
		return images;
	}

	private List<String> getVideoStories(RawInstagramUserStories instagramStories) {
		List<String> videos = new LinkedList<>();

		if(!StringUtils.isEmpty(instagramStories)) {
			instagramStories.getReel().getItems().forEach(video -> {
				if (!StringUtils.isEmpty(video.getVideos())) {
					for(int i = 0; i < video.getVideos().size();) {
						videos.add(video.getVideos().get(i).getVideoUrl());
						break;
					}
				}
			});
		}
		
		return videos;
	}

	@Override
	public ResponseEntity<Object> getInfluencerChartData(String userName, String platform) {

		InfluencerDemographicModelBuilder builder = InfluencerDemographicModel.builder()
				.topCountries(topCountries(userName, platform));

		if (platform.equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			builder.topIndustries(topIndustries(exploreInfluencerDao.getAllInstagramIndustryByHandle(userName), INDUSTRY_COUNT))
				.topStates(topStates(exploreInfluencerDao.getAllInstagramStateByHandle(userName), STATES_COUNT));

		return new ResponseEntity<>(builder.build(), HttpStatus.OK);
	}

	private Map<String, Double> topCountries(String userName, String platform) {

		if (platform.equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			return getInstagramCountries(userName, COUNTRY_COUNT);
		if (platform.equalsIgnoreCase(Platforms.YOUTUBE.name()))
			return getYoutubeCountries(userName, COUNTRY_COUNT);
		if (platform.equalsIgnoreCase(Platforms.TIKTOK.name()))
			return getTiktokCountries(userName, COUNTRY_COUNT);

		return null;
	}

	private Map<String, Double> getInstagramCountries(String userName, int count) {
		List<InstagramCountryDemographic> countryDemographics = exploreInfluencerDao.getAllInstagramCountryByHandle(userName);

		countryDemographics.sort((a, b) -> {
			Double difference = b.getValue() - a.getValue();
			difference *= _10_POW_16;
			return difference.intValue();
		});
		countryDemographics = countryDemographics.size() > count
				? countryDemographics.subList(0, count) : countryDemographics;

		Map<String, Double> topCountries = Maps.newLinkedHashMap();
		for (InstagramCountryDemographic countryDemographic : countryDemographics) {
			topCountries.put(countryDemographic.getKey().getCountry(), countryDemographic.getValue());
		}
		return topCountries;
	}

	private Map<String, Double> getYoutubeCountries(String userName, int count) {
		List<YouTubeCountryDemographic> countryDemographics = exploreInfluencerDao.getAllYoutubeCountryByHandle(userName);

		countryDemographics.sort((a, b) -> {
			Double difference = b.getValue() - a.getValue();
			difference *= _10_POW_16;
			return difference.intValue();
		});
		countryDemographics = countryDemographics.size() > count
				? countryDemographics.subList(0, count) : countryDemographics;

		Map<String, Double> topCountries = Maps.newLinkedHashMap();
		for (YouTubeCountryDemographic countryDemographic : countryDemographics) {
			topCountries.put(countryDemographic.getKey().getCountry(), countryDemographic.getValue());
		}
		return topCountries;
	}

	private Map<String, Double> getTiktokCountries(String userName, int count) {
		List<TikTokCountryDemographic> countryDemographics = exploreInfluencerDao.getAllTiktokCountryByHandle(userName);

		countryDemographics.sort((a, b) -> {
			Double difference = b.getValue() - a.getValue();
			difference *= _10_POW_16;
			return difference.intValue();
		});
		countryDemographics = countryDemographics.size() > count
				? countryDemographics.subList(0, count) : countryDemographics;

		Map<String, Double> topCountries = Maps.newLinkedHashMap();
		for (TikTokCountryDemographic countryDemographic : countryDemographics) {
			topCountries.put(countryDemographic.getKey().getCountry(), countryDemographic.getValue());
		}
		return topCountries;
	}

	private Map<String, Double> topIndustries(List<InstagramIndustryDemographic> instagramIndustryDemographics, int count) {
		instagramIndustryDemographics.sort((a, b) -> {
			Double difference = b.getValue() - a.getValue();
			difference *= _10_POW_16;
			return difference.intValue();
		});
		instagramIndustryDemographics = instagramIndustryDemographics.size() > count
				? instagramIndustryDemographics.subList(0, count)
				: instagramIndustryDemographics;

		Map<String, Double> topIndustries = Maps.newLinkedHashMap();
		for (InstagramIndustryDemographic industryDemographic : instagramIndustryDemographics) {
			topIndustries.put(industryDemographic.getKey().getIndustry(), industryDemographic.getValue());
		}
		return topIndustries;
	}

	private Map<String, Double> topStates(List<InstagramStateDemographic> instagramStateDemographics, int count) {
		instagramStateDemographics.sort((a, b) -> {
			Double difference = b.getValue() - a.getValue();
			difference *= _10_POW_16;
			return difference.intValue();
		});
		instagramStateDemographics = instagramStateDemographics.size() > count
				? instagramStateDemographics.subList(0, count)
				: instagramStateDemographics;

		Map<String, Double> topStates = Maps.newLinkedHashMap();

		for (InstagramStateDemographic stateDemographic : instagramStateDemographics) {
			topStates.put(stateDemographic.getKey().getState(), stateDemographic.getValue());
		}
		return topStates;
	}

	@Override
	public ResponseEntity<Object> getInfluencers(GetInfluencersRequestModel getInfluencersRequestModel) {

		log.info("Props {}", searchInfluencerProps);
		List<GetInfluencersResponseModel> getInfluencersResponseModel = Lists.newLinkedList();

		if (ResourceServer.isFreeBrand() && getInfluencersRequestModel.getSkipCount() > 0)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please upgrade to a higher plans to see more results, "
					+ "or please contact help@smartfluence.io to increase your limit.");

		try {
			processSearchInfluencersRequest(getInfluencersRequestModel, getInfluencersResponseModel, true);
		} catch (Exception e) {
			e.getStackTrace();
			log.error("Failed to get DeepSocial Data for Search Influencers");
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Failed to get influencers, Please try after sometime");
		}

		return new ResponseEntity<>(getInfluencersResponseModel, HttpStatus.OK);
	}

	@Override
	public void processSearchInfluencersRequest(GetInfluencersRequestModel getInfluencersRequestModel,
			List<GetInfluencersResponseModel> getInfluencersResponseModel, boolean savePicture) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			InfluencersDataRequest request = new InfluencersDataRequest();
			createSearchInfluencerRequest(getInfluencersRequestModel, request);
			log.info("Request {}", mapper.writeValueAsString(request));

			ResponseEntity<InfluencersDataResponse> response = socialDataProxy
					.searchinfluencers(searchInfluencerProps.getAutoUnhide(), getInfluencersRequestModel.getPlatform(), request);
			log.info("Response {}", mapper.writeValueAsString(response));
	
			if(response.getStatusCode().is2xxSuccessful()) {
				UUID searchId = null;
				if (getInfluencersRequestModel.getIsNewSearch())
					searchId = exploreInfluencerDao.saveBrandSearchHistory(ResourceServer.getUserId(), getInfluencersRequestModel, response.getBody());
				processSearchInfluencersResponse(getInfluencersRequestModel, searchId, savePicture, getInfluencersResponseModel, response);
			}
		} catch (JsonProcessingException e) {
			e.getStackTrace();
		}
	}

	private void processSearchInfluencersResponse(GetInfluencersRequestModel getInfluencersRequestModel, UUID searchId, boolean savePicture,
			List<GetInfluencersResponseModel> getInfluencersResponseModel, ResponseEntity<InfluencersDataResponse> response) {
		response.getBody().getAccounts().forEach(data -> {

			if(!StringUtils.isEmpty(data.getAccount().getUser().getUserId())) {
				getInfluencersResponseModel.add(GetInfluencersResponseModel.builder().influencerId(data.getAccount().getUser().getUserId())
						.influencerHandle("@" + (StringUtils.isEmpty(data.getAccount().getUser().getUserName())
								? data.getAccount().getUser().getFullname().replaceAll(" ", "").toLowerCase()
										: data.getAccount().getUser().getUserName()))
								.fullName(data.getAccount().getUser().getFullname())
						.url(data.getAccount().getUser().getUrl()).profileImage(data.getAccount().getUser().getPicture())
						.followers(data.getAccount().getUser().getFollowers()).engagements(data.getAccount().getUser().getEngagements())
						.engagementScore(data.getAccount().getUser().getEngagementRate()).searchId(searchId)
						.audienceCheck(getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name())
								? data.getMatch().getAudienceLikers().getAudienceData().getAudienceCredibility() : 1.0)
						.pricing(reportsService.getEstimatedPricing(data.getAccount().getUser().getEngagementRate(),
								data.getAccount().getUser().getFollowers(), data.getAccount().getUser().getEngagements(),
								getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name())
								? data.getMatch().getAudienceLikers().getAudienceData().getAudienceCredibility() : 1.0))
						.avgViews(data.getAccount().getUser().getAvgViews()).totalResult(response.getBody().getTotal()).build());

				if (savePicture) {
					exploreInfluencerDao.saveTemporaryInfluencerContent(TemporaryInfluencerContent.builder()
							.key(new TemporaryInfluencerContentKey(data.getAccount().getUser().getUserId(), true, 1))
							.value(data.getAccount().getUser().getPicture())
							.influencerHandle("@" + data.getAccount().getUser().getUserName()).build());
				}
			}
		});
	}

	private void createSearchInfluencerRequest(GetInfluencersRequestModel getInfluencersRequestModel, InfluencersDataRequest request) {
		Filter filter = new Filter();
		Paging paging = new Paging();

		setFilterParamsRequest(getInfluencersRequestModel, filter);
		setFilterObjectsRequest(getInfluencersRequestModel, request, filter);

		if(!StringUtils.isEmpty(searchInfluencerProps.getAudienceSource()))
			request.setAudienceSource(searchInfluencerProps.getAudienceSource());

		paging.setLimit(getInfluencersRequestModel.getLimitCount());
		paging.setSkip(getInfluencersRequestModel.getSkipCount());

		request.setFilter(filter);
		request.setPaging(paging);
	}

	private void setFilterParamsRequest(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getKeyword())
				&& getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.YOUTUBE.name()))
			filter.setKeywords(getInfluencersRequestModel.getKeyword());

		if(!StringUtils.isEmpty(getInfluencersRequestModel.getProfileBio()))
			filter.setProfileBio(getInfluencersRequestModel.getProfileBio());

		if(!StringUtils.isEmpty(getInfluencersRequestModel.getHashTag())) {
			Relevance relevance = new Relevance();
			relevance.setValue(getInfluencersRequestModel.getHashTag());
			filter.setRelevance(relevance);
		}

		if(!StringUtils.isEmpty(getInfluencersRequestModel.getSponsoredPost())
				&& getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			filter.setSponsoredPost(getInfluencersRequestModel.getSponsoredPost());

		if(!StringUtils.isEmpty(getInfluencersRequestModel.getVerified()))
			filter.setVerified(getInfluencersRequestModel.getVerified());

		if(!StringUtils.isEmpty(searchInfluencerProps.getHidden())
				&& !getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.YOUTUBE.toString()))
			filter.setHidden(searchInfluencerProps.getHidden());

		if(!StringUtils.isEmpty(searchInfluencerProps.getHasAudienceData()))
			filter.setHasAudienceData(searchInfluencerProps.getHasAudienceData());

		if(!StringUtils.isEmpty(searchInfluencerProps.getLastPosted()))
			filter.setLastPosted(searchInfluencerProps.getLastPosted());

		if(!StringUtils.isEmpty(searchInfluencerProps.getAccountType())
				&& getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			filter.setAccountType(searchInfluencerProps.getAccountType());

		if(!StringUtils.isEmpty(searchInfluencerProps.getAudienceCredibility())
				&& getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			filter.setAudienceCredibility(searchInfluencerProps.getAudienceCredibility());

		if(!StringUtils.isEmpty(searchInfluencerProps.getAudienceCredibilityClass())
				&& getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name()))
			filter.setAudienceCredibilityClass(searchInfluencerProps.getAudienceCredibilityClass());
	}

	private void setFilterObjectsRequest(GetInfluencersRequestModel getInfluencersRequestModel,
			InfluencersDataRequest request, Filter filter) {
		setSortBy(getInfluencersRequestModel, request);
		setFollowersFilter(getInfluencersRequestModel, filter);
		setEngagementFilter(getInfluencersRequestModel, filter);
		setCustomListFilter(getInfluencersRequestModel, filter);
		setAudienceAgeFilter(getInfluencersRequestModel, filter);
		setAudienceRelevance(getInfluencersRequestModel, filter);
		setWithContactFilter(getInfluencersRequestModel, filter);
		setInfluencerAgeFilter(getInfluencersRequestModel, filter);
		setAudienceGenderFilter(getInfluencersRequestModel, filter);
		setAudienceIndustryFilter(getInfluencersRequestModel, filter);
		setAudienceLocationFilter(getInfluencersRequestModel, filter);
		setInfluencerGenderFilter(getInfluencersRequestModel, filter);
		setInfluencerIndustryFilter(getInfluencersRequestModel, filter);
		setInfluencerLocationFilter(getInfluencersRequestModel, filter);
	}

	private void setSortBy(GetInfluencersRequestModel getInfluencersRequestModel, InfluencersDataRequest request) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getSortBy())) {
			SortResponse sort = new SortResponse();
			sort.setField(getInfluencersRequestModel.getSortBy());
			request.setSortResponse(sort);
		}
	}

	private void setFollowersFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		Range followers = new Range();
		followers.setMinValue(getInfluencersRequestModel.getFollowersMin());
		followers.setMaxValue(getInfluencersRequestModel.getFollowersMax());
		filter.setFollowers(followers);
	}

	private void setEngagementFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		Range engagements = new Range();
		engagements.setMinValue(getInfluencersRequestModel.getEngagementMin());
		engagements.setMaxValue(getInfluencersRequestModel.getEngagementMax());
		filter.setEngagements(engagements);
	}

	private void setCustomListFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if (!StringUtils.isEmpty(getInfluencersRequestModel.getCustomListId())) {
			CustomList customList = new CustomList();
			List<CustomList> customListArray = new LinkedList<CustomList>();
			customList.setId(Integer.parseInt(getInfluencersRequestModel.getCustomListId()));
			customList.setAction(getInfluencersRequestModel.getCustomListAction());
			customListArray.add(customList);
			filter.setCustomList(customListArray);
		}
	}

	private void setAudienceAgeFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getAudienceAge())) {
			List<CodeWeight> ageArray = new LinkedList<CodeWeight>();
			getInfluencersRequestModel.getAudienceAge().forEach(data -> {
				CodeWeight age = new CodeWeight();
				age.setCode(data);
				age.setWeight(getRespectiveAgeWeight(data));
				ageArray.add(age);
			});
			filter.setAudienceAge(ageArray);
		}
	}

	private void setAudienceRelevance(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getComparisonProfile())) {
			ComparisonProfile comparisonProfile = new ComparisonProfile();
			comparisonProfile.setValue(getInfluencersRequestModel.getComparisonProfile());

			filter.setComparisonProfile(comparisonProfile);
		}
	}

	private void setWithContactFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(searchInfluencerProps.getWithContactDefault())
				&& !StringUtils.isEmpty(searchInfluencerProps.getWithContactMust())) {
			WithContact withContact = new WithContact();
			List<WithContact> withContactArray = new LinkedList<WithContact>();
			withContact.setType(searchInfluencerProps.getWithContactDefault());
			withContact.setAction(searchInfluencerProps.getWithContactMust());
			withContactArray.add(withContact);

			if(!StringUtils.isEmpty(getInfluencersRequestModel.getWithContact())) {
				withContact = new WithContact();
				withContact.setType(getInfluencersRequestModel.getWithContact().toLowerCase());
				withContact.setAction(searchInfluencerProps.getWithContactShould());
				withContactArray.add(withContact);
			}
			filter.setWithContact(withContactArray);
		}
	}

	private void setInfluencerAgeFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getInfluencerAge())) {
			String influencerAgeArr [] = getInfluencersRequestModel.getInfluencerAge().split("-");
			Range influencerAge = new Range();
			influencerAge.setMinValue(Integer.parseInt(influencerAgeArr[0].trim()));
			influencerAge.setMaxValue(Integer.parseInt(influencerAgeArr[1].trim()));
			filter.setInfluencerAge(influencerAge);
		}
	}

	private void setAudienceGenderFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getAudienceGender())) {
			CodeWeight audienceGender = new CodeWeight();
			audienceGender.setCode(getInfluencersRequestModel.getAudienceGender());
			audienceGender.setWeight(searchInfluencerProps.getGenderWeight());
			filter.setAudienceGender(audienceGender);
		}
	}

	private void setAudienceIndustryFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if (getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name())
				&& !StringUtils.isEmpty(getInfluencersRequestModel.getAudienceBrandName())) {
			Optional<IndustryMaster> industryMaster = exploreInfluencerDao.getIndustryByName(getInfluencersRequestModel.getAudienceBrandName());

			if (industryMaster.isPresent()) {
				IndustryAndLocation industry = new IndustryAndLocation();
				List<IndustryAndLocation> industryArray = new LinkedList<IndustryAndLocation>();
				industry.setId(industryMaster.get().getId());
				industry.setWeight(industryMaster.get().getWeight());
				industryArray.add(industry);
				filter.setAudienceBrand(industryArray);
			}
		}
	}

	private void setAudienceLocationFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getAudienceGeoName())) {
			Optional<LocationMaster> locationMaster = exploreInfluencerDao.getLocationByName(getInfluencersRequestModel.getAudienceGeoName());

			if (locationMaster.isPresent()) {
				IndustryAndLocation location = new IndustryAndLocation();
				List<IndustryAndLocation> locationArray = new LinkedList<IndustryAndLocation>();
				location.setId(locationMaster.get().getId());
				location.setWeight(locationMaster.get().getWeight());
				locationArray.add(location);
				filter.setAudienceGeo(locationArray);
			}
		}
	}

	private void setInfluencerGenderFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		CodeWeight gender = new CodeWeight();
		gender.setCode(StringUtils.isEmpty(getInfluencersRequestModel.getInfluencerGender())
				? searchInfluencerProps.getInfluencerGender() : getInfluencersRequestModel.getInfluencerGender());
		filter.setInfluencerGender(gender);
	}

	private void setInfluencerIndustryFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getInfluencerBrandName())
				&& getInfluencersRequestModel.getPlatform().equalsIgnoreCase(Platforms.INSTAGRAM.name())) {
			Optional<IndustryMaster> industryMaster = exploreInfluencerDao.getIndustryByName(getInfluencersRequestModel.getInfluencerBrandName());

			if (industryMaster.isPresent())
				filter.setInfluencerBrand(new Integer[]{industryMaster.get().getId().intValue()});
		}
	}

	private void setInfluencerLocationFilter(GetInfluencersRequestModel getInfluencersRequestModel, Filter filter) {
		if(!StringUtils.isEmpty(getInfluencersRequestModel.getInfluencerGeoName())) {
			Optional<LocationMaster> locationMaster = exploreInfluencerDao.getLocationByName(getInfluencersRequestModel.getInfluencerGeoName());

			if (locationMaster.isPresent()) {
				IndustryAndLocation location = new IndustryAndLocation();
				List<IndustryAndLocation> locationArray = new LinkedList<IndustryAndLocation>();
				location.setId(locationMaster.get().getId());
				locationArray.add(location);
				filter.setInfluencerGeo(locationArray);
			}
		}
	}

	private Double getRespectiveAgeWeight(String age) {
		Double weight = 0.0;
		switch (age) {
		case AGE_13_17:
			weight = searchInfluencerProps.getAgeWeight13_17();
			break;
		case AGE_18_24:
			weight = searchInfluencerProps.getAgeWeight18_24();
			break;
		case AGE_25_34:
			weight = searchInfluencerProps.getAgeWeight25_34();
			break;
		case AGE_35_44:
			weight = searchInfluencerProps.getAgeWeight35_44();
			break;
		case AGE_45_64:
			weight = searchInfluencerProps.getAgeWeight45_64();
			break;
		case AGE_65:
			weight = searchInfluencerProps.getAgeWeight65();
			break;
		default:
			break;
		}
		return weight;
	}

	@Override
	public ResponseEntity<byte[]> viewInfluencerProfilePic(String influencerId) {

		Optional<TemporaryInfluencerContent> optionalTemporaryInfluencerContent = exploreInfluencerDao
				.getTemporaryInfluencerContent(influencerId, true, 1);

		if (optionalTemporaryInfluencerContent.isPresent()) {
			TemporaryInfluencerContent temporaryInfluencerContent = optionalTemporaryInfluencerContent.get();
			String profileImageLink = temporaryInfluencerContent.getValue();

			ImageProcessor imageProcessor = null;
			try {
				imageProcessor = new ImageProcessor(profileImageLink);
			} catch (Exception e) {
				return ResponseEntity.noContent().build();
			}

			return new ResponseEntity<>(imageProcessor.processFinalImage(), HttpStatus.OK);
		}

		return ResponseEntity.noContent().build();
	}

	@Override
	public ResponseEntity<Object> viewInfluencerProfileName(UUID userId) {
		Optional<SocialIdMaster> socialIdMaster = exploreInfluencerDao.getSocialIdMasterById(userId.toString());

		if (socialIdMaster.isPresent()) {
			UUID brandId = ResourceServer.getUserId();
			Optional<BrandCreditAllowance> optionalBrandCreditAllowance = exploreInfluencerDao.
					getBrandCreditAllowance(brandId, userId, CreditType.BID);
			Map<String, Object> data = new LinkedHashMap<>();
			Object brandCredits = null;

			if (!optionalBrandCreditAllowance.isPresent()) {
				Optional<BrandCredits> optionalBrandCredits = exploreInfluencerDao.getBrandCredits(brandId);
				UserDetailsKey userDetailsKey = new UserDetailsKey(UserStatus.APPROVED, brandId);
				Optional<BrandDetails> optionalBrandDetails = exploreInfluencerDao.getBrandDetails(userDetailsKey);

				if (optionalBrandCredits.isPresent() && optionalBrandCredits.get().getCredits() > 0
						&& optionalBrandCredits.get().getValidUpto().getTime() > System.currentTimeMillis()
						&& optionalBrandDetails.isPresent()) {
					brandCredits = reportsService.saveBrandCreditDetails(userId, optionalBrandCredits, CreditType.BID);
				} else
					return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("No credits available");
			}
			data.put("data", socialIdMaster.get());
			data.put("credit", brandCredits);
			return ResponseEntity.ok(data);
		}
		return ResponseEntity.notFound().build();
	}

	@Override
	public ResponseEntity<byte[]> viewInfluencerContentImage(String userId, int contentId, String platform) {
		return getInfluencerContent(userId, contentId, platform);
	}

	private ResponseEntity<byte[]> getInfluencerContent(String userId, int contentId, String platform) {
		UUID influencerId;
		try {
			influencerId = UUID.fromString(userId);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
		Optional<SocialIdMaster> optionalSocialIdMaster = exploreInfluencerDao.getSocialIdMasterById(influencerId.toString());
		if (optionalSocialIdMaster.isPresent()) {
			SocialIdMaster socialIdMaster = optionalSocialIdMaster.get();
			String influencerHandle = socialIdMaster.getInfluencerHandle();
			InfluencerContentKey influencerContentKey = new InfluencerContentKey(influencerHandle, contentId);

			if (socialIdMaster.getPlatform().equals(Platforms.INSTAGRAM))
				return getInstagramContents(influencerContentKey);
			if (socialIdMaster.getPlatform().equals(Platforms.YOUTUBE))
				return getYoutubeContents(influencerContentKey);
			if (socialIdMaster.getPlatform().equals(Platforms.TIKTOK))
				return getTiktokContents(influencerContentKey);

			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.noContent().build();
	}

	private ResponseEntity<byte[]> getInstagramContents(InfluencerContentKey influencerContentKey) {
		Optional<InstagramContent> instagramContent = instagramContentRepo.findById(influencerContentKey);
		if (instagramContent.isPresent())
			return getContentInBytes(instagramContent.get().getValue());

		return ResponseEntity.noContent().build();
	}

	private ResponseEntity<byte[]> getYoutubeContents(InfluencerContentKey influencerContentKey) {
		Optional<YouTubeContent> youtubeContent = youtubeContentRepo.findById(influencerContentKey);
		if (youtubeContent.isPresent())
			return getContentInBytes(youtubeContent.get().getValue());

		return ResponseEntity.noContent().build();
	}

	private ResponseEntity<byte[]> getTiktokContents(InfluencerContentKey influencerContentKey) {
		Optional<TikTokContent> tiktokContent = tiktokContentRepo.findById(influencerContentKey);
		if (tiktokContent.isPresent())
			return getContentInBytes(tiktokContent.get().getValue());

		return ResponseEntity.noContent().build();
	}

	private ResponseEntity<byte[]> getContentInBytes(String contentLink) {
		ImageProcessor imageProcessor;
		try {
			imageProcessor = new ImageProcessor(contentLink);
		} catch (Exception e) {
			return ResponseEntity.noContent().build();
		}
		// imageProcessor.flip();
		return new ResponseEntity<>(imageProcessor.processFinalImage(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> addToCampaign(CampaignDetail addToCampaign) {
		return exploreInfluencerDao.addToCampaign(addToCampaign);
	}

//	@Override
//	public ResponseEntity<Object> addInfluencerToNewCampaign(List<CampaignInfluencerReqEntity> campaignInfluencerReqEntity) {
//		return exploreInfluencerDao.addInfluencerToNewCampaign(campaignInfluencerReqEntity);
//	}

	@Override
	public ResponseEntity<Object> getCampaignByInfluencer() {
		return exploreInfluencerDao.getCampaignByInfluencer();
	}

	@Override
	public ResponseEntity<Object> checkInfluencer(CampaignDetail campaignDetail, String influencerHandle) {
		Optional<SocialIdMaster> optionalSocialIdMaster = exploreInfluencerDao.getSocialIdMasterByHandleAndPlatform
				(influencerHandle, reportsService.getPlatform(campaignDetail.getPlatform()));

		if (optionalSocialIdMaster.isPresent()) {
			SocialIdMaster socialIdMaster = optionalSocialIdMaster.get();

			Optional<CampaignInfluencerMapping> optionalCampaignInfluencerMapping = exploreInfluencerDao
					.getCampaignInfluencerMapping(campaignDetail.getCampaignId().toString(), socialIdMaster.getUserId());

			if (optionalCampaignInfluencerMapping.isPresent())
				return ResponseEntity.ok(true);
		}
		return ResponseEntity.ok(false);
	}

	@Override
	public ResponseEntity<Object> searchInfluencer(SearchInfluencerRequestModel request) {
		UUID brandId = ResourceServer.getUserId();
		SearchInfluencerResponseModel response = new SearchInfluencerResponseModel();
		Optional<UserAccounts> userAccounts = exploreInfluencerDao.getUserAccounts(brandId.toString());
		log.info("Search Influencer for Handle {} and ID {} for platform {}, requested by {}", request.getInfluencerHandle(),
				response.getDeepSocialInfluencerId(), request.getPlatform(), userAccounts.get().getBrandName());

		if (userAccounts.isPresent()) {
			Optional<SocialIdMaster> socialIdMaster = exploreInfluencerDao.getSocialIdMasterByHandleAndPlatform
					("@" + request.getInfluencerHandle(), reportsService.getPlatform(request.getPlatform()));

			if (socialIdMaster.isPresent()) {
				response.setInfluencerId(socialIdMaster.get().getUserId());
				response.setInfluencerHandle(socialIdMaster.get().getInfluencerHandle());
				response.setDeepSocialInfluencerId(StringUtils.isEmpty(socialIdMaster.get().getInfluencerId())
						? request.getDeepSocialInfluencerId() : socialIdMaster.get().getInfluencerId());
				response.setNewInfluencer(StringUtils.isEmpty(response.getDeepSocialInfluencerId()));

				reportsService.saveReportHistory(ReportSearchHistory.builder()
						.key(ReportSearchHistoryKey.builder().brandId(brandId).isNew(false).createdAt(new Date()).build())
						.influencerUsername(response.getInfluencerHandle()).brandName(userAccounts.get().getBrandName())
						.influencerDeepSocialId(response.getDeepSocialInfluencerId()).reportId(response.getReportId()).viewType(ViewType.SEARCH)
						.platform(reportsService.getPlatform(request.getPlatform())).build());
			} else {
				try {
					ReportData reportData = reportsService.getReportData(StringUtils.isEmpty(request.getDeepSocialInfluencerId())
							? request.getInfluencerHandle() : request.getDeepSocialInfluencerId(), request.getPlatform(), ViewType.SEARCH, null);

					if (StringUtils.isEmpty(reportData.getReportInfo().getReportId()))
						return ResponseEntity.noContent().build();

					response.setInfluencerId(reportsService.saveInfluencerReportData(reportData));
					response.setInfluencerHandle("@" + (StringUtils.isEmpty(reportData.getUserProfile().getHandle())
							? reportData.getUserProfile().getFullName().replace(" ", "").toLowerCase()
									: reportData.getUserProfile().getHandle()));
					response.setReportId(reportData.getReportInfo().getReportId());
					response.setReportPresent(true);
					response.setNewInfluencer(true);
					response.setDeepSocialInfluencerId(reportData.getUserProfile().getUserId());
				} catch (FeignException fe) {
					if (fe.getMessage().contains("retry_later")) {
						String retryMessage = "We are pulling data for this influencer! "
								+ "We will send out an email with a link to the influencer data in a few minutes.";
						createInfluencerReportQueue(InfluencerDataQueue.builder().key(InfluencerDataQueueKey.builder()
								.brandId(userAccounts.get().getUserId()).influencer(StringUtils.isEmpty(request.getDeepSocialInfluencerId())
										? request.getInfluencerHandle() : request.getDeepSocialInfluencerId())
								.viewType(ViewType.VIEW).build()).brandEmail(userAccounts.get().getEmail()).brandName(userAccounts.get().getBrandName())
								.platform(reportsService.getPlatform(request.getPlatform())).build());
						return ResponseEntity.status(fe.status()).body(retryMessage);
					}
					else if (fe.getMessage().contains("empty_audience_data"))
						return ResponseEntity.status(fe.status()).body("Currently we do not have data for this influencer");
					else
						return ResponseEntity.status(fe.status()).body(request.getInfluencerHandle() + ", seems to be an invalid "
									+ request.getPlatform() + " handle.");
				} catch (Exception e) {
					return ResponseEntity.badRequest().body(e.getMessage());
				}
			}
			return ResponseEntity.ok(response);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	private void createInfluencerReportQueue(InfluencerDataQueue influencerDataQueue) {
		Thread thread = new Thread(() -> sendMailUtil.scheduleInfluencerReportMailJob(influencerDataQueue));
		thread.start();
	}

	@Override
	public ResponseEntity<Object> getAutoSuggestions(String influencerHandle, String platform) {
		if (StringUtils.isEmpty(platform))
			platform = Platforms.INSTAGRAM.name().toLowerCase();

		InfluencerAutoSuggestions autoSuggestions = socialDataProxy.getAutoSuggestions(influencerHandle, 5, "search", platform);
		List<Map<String, String>> data = new LinkedList<>();
		autoSuggestions.getData().forEach(e -> {
			Map<String, String> nameAndId = new HashMap<>();
			nameAndId.put("name", StringUtils.isEmpty(e.getUserName()) ? e.getFullName() : e.getUserName());
			nameAndId.put("id", e.getUserId());
			data.add(nameAndId);
		});
		return ResponseEntity.ok(data);
	}

	@Override
	public ResponseEntity<Object> getUtilities(String name, String data) {
		if (data.equals(LOCATION_UTILITY) && !StringUtils.isEmpty(name)) {
			List<Map<String, String>> locationMaster = new LinkedList<Map<String, String>>();
			exploreInfluencerDao.getLocationUtilities(name).forEach(e -> {
				Map<String, String> locationMap = new HashMap<>();
				locationMap.put("name", e.getLocation());
				locationMaster.add(locationMap);
			});
			return new ResponseEntity<>(locationMaster, HttpStatus.OK);
		}
		else if (data.equals(INDUSTRY_UTILITY) && !StringUtils.isEmpty(name)) {
			List<Map<String, String>> industryMaster = new LinkedList<Map<String, String>>();
			exploreInfluencerDao.getIndustryUtilities(name).forEach(e -> {
				Map<String, String> industryMap = new HashMap<>();
				industryMap.put("name", e.getIndustry());
				industryMaster.add(industryMap);
			});
			return new ResponseEntity<>(industryMaster, HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	@Override
	public ResponseEntity<Object> getMailTemplates() {
		return ResponseEntity.ok(exploreInfluencerDao.getMailTemplates());
	}
}