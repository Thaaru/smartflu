package io.smartfluence.modal.brand.viewinfluencer;

import java.util.Date;
import java.util.UUID;

import io.smartfluence.constants.CampaignStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class BrandCampaigns {

	private UUID brandId;

	private CampaignStatus campaignStatus;

	private UUID campaignId;

	private String campaignName;

	private String stringCreatedAt;

	private Date createdAt;

	private Integer influencerCount;

	private Boolean isPromote;
}