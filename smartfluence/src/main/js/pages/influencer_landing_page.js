import React from "react";
import { ProgressBar } from "react-bootstrap"
import LongText from "../components/long_short_text_component";
import {  PRIMARY_COLOR } from "../utils/string_constants";
import { PoweredBy } from "../components/powered_by";
import { getPlatformAndIcon } from "../components/platform_icon";

export default function InfluencerLandingPage({ data,onSubmit }) {

    return (
        <div class='content-wrapper' >
            {
                <div >
                    <div className="col-lg ">
                        <p className="heading">{data.brandName}</p>
                    </div>
                    <div style={{ "textAlign": "left", }} >
                        <div className="col-md" >
                            <p className="heading">Brand Overview</p>
                            <LongText content={ data.brandDescription} limit={200} />
                        </div>
                        <br />
                        <div className="col-md">
                            <p className="heading">Campaign Overview</p>
                            <LongText content={data.campaignDescription} limit={200} />
                        </div>
                        <br />
                        {!getPlatformAndIcon(data) ? null :
                            <div className="col-md">
                                <p className="heading">Required Deliverables</p>
                                {getPlatformAndIcon(data).length > 0 && getPlatformAndIcon(data).map((item) =>
                                    <li className="normalText" style={{ "listStyleType": "none" }}>{item.platformIcon} {`${item.platformName} : ${item.postTypeName}`}</li>)}
                            </div>
                        }
                    </div>
                    <div className="footer">
                        <div className="col-md">
                            <button type="button" className="btn btn-primary btn-lg btn-block" style={{ backgroundColor: PRIMARY_COLOR }} onClick={onSubmit}>Get Started</button>
                        </div>
                        <div className="px-4 mt-4 ">
                            <ProgressBar variant="primary" now={0} style={{ "height": "6px" }} />
                        </div>
                        <PoweredBy/>
                    </div>
                </div>
            }
        </div>
    );
}