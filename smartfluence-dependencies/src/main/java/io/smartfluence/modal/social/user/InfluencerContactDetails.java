package io.smartfluence.modal.social.user;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.smartfluence.modal.social.report.UserProfile;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InfluencerContactDetails implements Serializable {

	private static final long serialVersionUID = 1094604014165017850L;

	@JsonAlias("success")
	private Boolean success;

	@JsonAlias("user_profile")
	private UserProfile userProfile;
}