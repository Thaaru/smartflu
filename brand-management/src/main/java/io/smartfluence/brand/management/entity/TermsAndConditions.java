package io.smartfluence.brand.management.entity;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "terms_conditions")
public class TermsAndConditions implements Serializable {
    @Id
    @Column(name = "term_id")
    private Long termId;

    @Column(name = "terms_condition")
    private String termsCondition;

    @Column(name = "terms_condition_file_path")
    private String termsConditionFilePath;

    @Column(name = "status")
    private String status;

}
