package io.smartfluence.mysql.entities.influencer;

import io.smartfluence.modal.brand.campaignv1.*;
import io.smartfluence.mysql.entities.ProposalStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FullCampaignInfluencerDetails {
    private String campaignId ;
    private String influencerId  ;
    private String  influencerEmail  ;
    private String influencerFirstName;
    private String influencerLastName;
    private String influencerAccountStatus;
    private String influencerPasswordStatus;
    private String platform  ;
    private String brandId  ;
    private String campaignName ;
    private String brandEmail;
    private String brandDescription  ;
    private String campaignDescription  ;
    private Long termsId;
    private String hashtags  ;
    private int maxProductCount  ;
    private TermsConditionsEntityV1 termsConditionDetails;
    private String brandName;
    private List<CampaignInfluencerProductId> productsAcceptedList;
    private List<CampaignProductsEntityV1> products;
    private List<CampaignInfluencerOffers> offers;
    private List<CampaignInfluencerTaskId> tasksAcceptedList;
    private List<CampaignTasksEntityV1> deliverables;
    private List<StateMaster> stateList;
    private List<CountryMaster> countryList;
    private List<PlatformMasterEntityV1> platFormDetailsList;
    private List<PlatformPostTypeEntityV1> platformPostTypeDetailsList;
    private AcceptedStatus productsAcceptedStatus  ;
    private AcceptedStatus offerAcceptedStatus  ;
    private AcceptedStatus deliverablesAcceptedStatus  ;
    private AcceptedStatus shippingAcceptedStatus  ;
    private AcceptedStatus paymentAcceptedStatus  ;
    private AcceptedStatus termsConditionStatus  ;
    private ProposalStatus proposalStatus  ;
    private LocalDateTime productsAcceptedAt  ;
    private LocalDateTime offerAcceptedAt  ;
    private LocalDateTime deliverablesAcceptedAt  ;
    private LocalDateTime shippingAcceptedAt  ;
    private LocalDateTime paymentAcceptedAt  ;
    private LocalDateTime termsConditionAcceptedAt  ;
    private LocalDateTime campaignPaidAt;
    private PaymentMethodType paymentMethodType;
    private String paymentEmailAddress;
    private Address shippingAddress;
    private Address paymentAddress;
}
