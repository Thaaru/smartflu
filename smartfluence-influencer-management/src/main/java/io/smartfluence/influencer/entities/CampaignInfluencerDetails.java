package io.smartfluence.influencer.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_influencer_details_v1")
public class CampaignInfluencerDetails {
    @EmbeddedId
   private CampaignInfluencerId campaignInfluencerId;
    private String influencerEmail;
    private String platform;
    private String paymentEmailAddress;
    private String influencerFirstName;
    private String influencerLastName;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isOfferAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isProductAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isDeliverablesAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isShippingAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isPaymentAccepted;
    @Enumerated(EnumType.STRING)
    private AcceptedStatus isTermsAccepted;
    @Enumerated(EnumType.STRING)
    private ProposalStatus proposalStatus;
    @Enumerated(EnumType.STRING)
    private PaymentMethodType paymentMethodType;
 private LocalDateTime productsAcceptedAt;
 private LocalDateTime offerAcceptedAt;
 private LocalDateTime deliverablesAcceptedAt;
 private LocalDateTime shippingAcceptedAt;
 private LocalDateTime paymentAcceptedAt;
 private LocalDateTime termsAcceptedAt;
 private LocalDateTime campaignPaidAt;

}
