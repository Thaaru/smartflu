package io.smartfluence.brand.management.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "currency_master")
public class CurrencyMaster implements Serializable {
    @Id
    @Column(name = "currency_Id")
    private Long currencyId;

    @Column(name = "currency")
    private String currency;
}
