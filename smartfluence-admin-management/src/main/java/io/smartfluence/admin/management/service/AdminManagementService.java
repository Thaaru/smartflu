package io.smartfluence.admin.management.service;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.modal.admin.DataBaseHistoryModal;
import io.smartfluence.modal.validation.admin.ReportRequestModal;
import io.smartfluence.modal.validation.influencer.settings.AboutYouModal;

public interface AdminManagementService {
	public List<BrandDetails> getBrandList();

	public List<SubscriptionMaster> getSubscriptions();

	public void approveBrand(UUID brandId, SubscriptionType subscriptionType, UUID subscriptionId);

	public void declineBrand(UUID brandId);

	public void saveAboutYou(@Valid AboutYouModal aboutYouModal);

	public void sendInvitation(String[] brandIds);

	public ResponseEntity<Object> updateDb();

	public List<DataBaseHistoryModal> getDBUpdateHistory();

	public ResponseEntity<Object> checkDbUpdateStatus();

	public ResponseEntity<String> exportAdminData();

	public ResponseEntity<Object> exportReportData(ReportRequestModal reportRequestModal);

	public ResponseEntity<byte[]> downloadReport(String flag);
}