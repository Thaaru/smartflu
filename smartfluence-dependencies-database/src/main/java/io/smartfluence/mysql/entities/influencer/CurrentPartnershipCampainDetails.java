package io.smartfluence.mysql.entities.influencer;

import io.smartfluence.mysql.entities.ProposalStatus;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CurrentPartnershipCampainDetails {
    String campaignId;
    String name;
    @Enumerated(EnumType.STRING)
    ProposalStatus proposalStatus;
}
