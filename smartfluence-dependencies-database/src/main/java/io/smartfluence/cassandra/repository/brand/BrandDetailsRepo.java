package io.smartfluence.cassandra.repository.brand;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.UserStatus;

@Repository
public interface BrandDetailsRepo extends CassandraRepository<BrandDetails, UserDetailsKey> {

	Set<BrandDetails> findAllByKeyUserStatusIn(Set<UserStatus> userStatus);

	Optional<BrandDetails> findByKeyUserStatusInAndKeyUserId(Set<UserStatus> userStatus, UUID userId);
}