package io.smartfluence.influencer.controllers;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.smartfluence.influencer.entities.CustomResponseEntity;

@RestController
@RequestMapping("influencer")
public class CampaignInfluencerRestController {

    @Autowired
    FullCampaignInfluencerDetailsService fullCampaignInfluencerDetailsService;

    @Autowired
    SPCountryStateMasterService spCountryStateMasterService;
    
    @Autowired
    CurrentPartnershipService currentPartnershipService;

    @GetMapping("/pub/campaign-influencer-details")
    public CustomResponseEntity getAll(@RequestParam("token") String emailTokenCode){
        return fullCampaignInfluencerDetailsService.getFullCampaignInfluencerDetails(emailTokenCode);
    }

    @PutMapping("/pub/campaign-influencer-details")
    public CustomResponseEntity updateAll(@RequestBody FullCampaignInfluencerDetails fullCampaignInfluencerDetails, @RequestParam("token") String token){
        return fullCampaignInfluencerDetailsService.updateFullCampaignInfluencerDetails(fullCampaignInfluencerDetails,token);
    }

    @GetMapping("/pub/states/{countryId}")
    public CustomResponseEntity getStateList(@PathVariable Long countryId){
        return spCountryStateMasterService.getStateList(countryId);
    }

    @GetMapping("/current-partnership")
    public CustomResponseEntity currentPartnerShipData(){
        return currentPartnershipService.getCurrentPartnershipDetails();
    }
}
