package io.smartfluence.modal.social.user.instagram;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawInstagramMediaInfo implements Serializable {

	private static final long serialVersionUID = 9148113710696525456L;

	@JsonAlias("items")
	private List<RawInstagramMedia> items;

	@JsonAlias("status")
	private String status;
}