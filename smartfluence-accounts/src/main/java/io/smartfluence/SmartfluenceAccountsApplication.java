package io.smartfluence;

import java.security.Principal;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import io.smartfluence.beans.WebContainer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
@EntityScan({ "io.smartfluence.mysql.entities", "io.smartfluence.cassandra.entities" })
@EnableJpaRepositories("io.smartfluence.mysql.repository")
@EnableCassandraRepositories("io.smartfluence.cassandra.repository")
@RestController
public class SmartfluenceAccountsApplication {

	static final int MIN_LOG_ROUNDS = 4;

	static final int MAX_LOG_ROUNDS = 31;

	public static void main(String[] args) {
		SpringApplication.run(SmartfluenceAccountsApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(prefix="smartfluence",name="use-ssl")
	public WebContainer webContainer() {
		return new WebContainer();
	}

	@GetMapping("/user/me")
	public Principal user(Principal principal) {
		return principal;
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(Locale.US);
		localeResolver.setDefaultTimeZone(TimeZone.getTimeZone("UTC"));
		return localeResolver;
	}
}