package io.smartfluence.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.CampaignPostLinkDetails;

public interface CampaignPostLinkDetailsRepo extends CassandraRepository<CampaignPostLinkDetails, String> {

}