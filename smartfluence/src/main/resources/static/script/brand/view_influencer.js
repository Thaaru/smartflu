var ageChart = document.getElementById("ageChart").getContext('2d');
var genderChart = document.getElementById("genderChart").getContext('2d');
var countryChart = document.getElementById("countryChart").getContext('2d');
var stateChart = document.getElementById("stateChart").getContext('2d');
var industryChart = document.getElementById("industryChart").getContext('2d');


function removeCurrentCampName(){
    var platform = $influencerModel.platform;
    var campsbyPlatform = allActiveCampaigns.filter(x => x.platform_name?.toLowerCase() == platform?.toLowerCase());
    $('#invalidCampaign').html("")
      if(campsbyPlatform.length==0){
            $('#campName').find('option').remove().end();
            $('#campName').append(`<option value="" disabled>Select Campaign Name</option>`)
            $('#invalidCampaign').html("Please click <a style='color: #2f008d' href='/brand/new-campaign'><u>here</u></a> to create your new campaign")
        }
        else{
            $('#campName').find('option').remove().end();
            $('#campName').append(`<option value="">Select Campaign Name</option>`)
            campsbyPlatform.forEach((element, index) => {
                $('#campName').append(`<option value="${element.campaign_id}">${element.campaign_name}</option>`)
            });
        }
  }

$(document).ready(function() {
removeCurrentCampName();
    $('[data-toggle="tooltip"]').tooltip();

    $(".left1, .right1").click(function() {
        $('video').trigger('pause');
        console.log("left and right");
    });

    if ($influencerModel.status == 200) {
        getInfluencerChartData();

        if ($influencerModel.platform == 'instagram') {
            $('#influencerInstagramLink').removeClass('d-none');
            $('#influencerInstagramLink').attr("href", "https://www.instagram.com/" + $influencerModel.influencerHandle.substring(1));
            getInstagramStories();
        } else if ($influencerModel.platform == 'youtube') {
            $('#influencerYoutubeLink').removeClass('d-none');
            $('#influencerYoutubeLink').attr("href", "https://www.youtube.com/channel/" + $influencerModel.deepSocialInfluencerId);
        } else if ($influencerModel.platform == 'tiktok') {
            $('#influencerTiktokLink').removeClass('d-none');
            $('#influencerTiktokLink').attr("href", "https://www.tiktok.com/share/user/" + $influencerModel.deepSocialInfluencerId);
        }
    }
});

$("#campName").change(function(event) {
    var campaignId = event.target.value;
    var campaign = allActiveCampaigns.find(x => x.campaign_id == campaignId);
    var toDisable = false;
    if (campaign) {
        var influencerId = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
        var influencerIds =  campaign.influencers.map(x => x.social_influencer_id);
        toDisable = influencerIds.includes(influencerId);
        if(toDisable){
             toast("Influencer already exist in this campaign", "error")
         }
    }
    $('#add_to_campaign').attr('disabled', toDisable);
});

var options = {
    scales: {
        yAxes: [{
            ticks: {
                stepSize: 10,
                min: 0
            },
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
            }
        }],
        xAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
            }
        }]
    }
};

var myGenderChart = new Chart(genderChart, {
    type: 'pie',
    data: {
        labels: ['MALE %', 'FEMALE %'],
        datasets: [{
            label: 'Gender in %',
            data: [($influencerModel.genders['Male'] * 100).toFixed(2),
                ($influencerModel.genders['Female'] * 100).toFixed(2)
            ],
            backgroundColor: ['azure', 'pink'],
            borderColor: ['skyblue', 'red']
        }]
    }
});

var ageData = getBarData($influencerModel.ageRanges);

ageData.datasets[0].label = 'Age in %';

var myAgeChart = new Chart(ageChart, {
    options: options,
    type: 'bar',
    data: ageData
});

function getInfluencerChartData() {

    $.get({
        url: window.location.origin + '/brand/view-influencer/' + $influencerModel.influencerHandle + '/chartData/' + $influencerModel.platform,
    }).done(function(data) {
        setTopCountriesChart(data.topCountries);

        if ($influencerModel.platform == 'instagram') {
            setTopStatesChart(data.topStates);
            setTopIndustriesChart(data.topIndustries);
        }
    }).fail(function() {
        console.log("ChartData not available");
    });
}

function setTopCountriesChart(topCountries) {
    var countryData = getBarData(topCountries);

    countryData.datasets[0].label = 'Countries in %';
    var myCountryChart = new Chart(countryChart, {
        options: options,
        type: 'bar',
        data: countryData
    });
    $('.country-loader').addClass('d-none');
}

function setTopStatesChart(topStates) {
    var stateData = getBarData(topStates);

    stateData.datasets[0].label = 'States in %';
    var myStateChart = new Chart(stateChart, {
        options: options,
        type: 'bar',
        data: stateData
    });
    $('.state-loader').addClass('d-none');
}

function setTopIndustriesChart(topIndustries) {
    var industryData = getBarData(topIndustries);

    industryData.datasets[0].label = 'Interests in %';
    var myIndustryChart = new Chart(industryChart, {
        options: options,
        type: 'bar',
        data: industryData
    });
    $('.interests-loader').addClass('d-none');
}

async function getInstagramStories() {
    var userId;
    if ($influencerModel.reportPresent) {
        userId = $influencerModel.deepSocialInfluencerId;
    } else {
        userId = $influencerModel.influencerHandle.substring(1);
    }

    await $.get({
        url: window.location.origin + '/brand/view-influencer/' + userId + '/stories/' + 1,
    }).done(function(data) {
        if (data.images.length != 0 || data.videos.length != 0) {
            $("#instagramStory").css("border", "5px solid  #cd486b").css("cursor", "pointer");
            loadInstagramStories(data);
        } else {
            $("#instagramStory").removeAttr('data-target');
        }
    }).fail(function() {
        console.log("Data not available for UserStories");
    });
}

var slide = 1;

function loadInstagramStories(data) {
    var storyContent = "";

    if (slide == 1) {

        $.each(data.images, function(index, value) {
            if (data.images.length + 1 != index) {
                storyContent += "<div id='item" + slide +
                    "' class='in-carousel-item' style='display: block;'><p><img src='data:image/jpeg;base64," + value + "'style='width:300px;height:400px;'></img></p>" +
                    "</div>";
                slide++;
            } else {
                storyContent += "<div id='item" + slide +
                    "' class='in-carousel-item' style='display: none;'><p><img src='data:image/jpeg;base64," + value + "'style='width:300px;height:400px;'></p>" +
                    "</div>";
                slide++;
            }
        });

        $.each(data.videos, function(index, value) {
            if (data.videos.length + 1 != index) {
                storyContent += "<div id='item" + slide +
                    "' class='in-carousel-item' style='display: none;'><video width=\"300\" height=\"400\" controls autoplay muted>" +
                    "<source src='" + value + "' type=\"video/ogg\"></video>" +
                    "</div>";
                slide++;
            }
        });
    }
    $('.in-carousel').append(storyContent);
}

function getBarData(json) {
    var data = {};
    var labels = [];
    var dataArr = [];
    for (var i in json) {
        labels.push(i);
        dataArr.push({
            y: (json[i] * 100).toFixed(2)
        });
    }
    data.labels = labels;
    data.datasets = [];
    data.datasets.push({
        borderColor: "#2f008d",
        backgroundColor: "#2f008d",
        borderWidth: 2,
        hoverBorderColor: "#2f008d",
        data: dataArr
    });
    return data;
}

var contentLength;
var successCount;
var contentData = $influencerModel.topContents;

$(function() {
    contentLength = contentData.length;
    buildContent();
});

function buildContent() {
    if (contentData.length == 0) {
        $('.carousel-loader').removeClass('d-none').text('No content available');
        return;
    }
    $('.hidden-carousal .carousel-item').removeClass('active');
    for (var k in contentData) {
        if (Number(k) > 10) {
            break;
        }
        if (k % 2 == 0) {
            $('.carousel-indicators').append('<li data-target="#contentCarousal" data-slide-to="' + (k / 2) + '"></li>')
            $('.hidden-carousal img').first().attr('src', $influencerModel.influencerId + '/content/' + contentData[k].value + '?platform=' + $influencerModel.platform);
            $('.hidden-carousal #left').last().attr('href', contentData[k].url);
            if (k == contentData.length - 1) {
                $('.carousel-inner').append($('.hidden-carousal').html());
                $('#contentCarousal .carousel-item').last().find('.right').remove();
            }
        } else {
            $('.hidden-carousal img').last().attr('src', $influencerModel.influencerId + '/content/' + contentData[k].value + '?platform=' + $influencerModel.platform);
            $('.hidden-carousal #right').first().attr('href', contentData[k].url);
            $('.carousel-inner').append($('.hidden-carousal').html());
        }
    }
    $('.carousel-inner .carousel-item').first().addClass('active');
    $('.carousel-indicators li').first().addClass('active');
    $('.carousel-loader').addClass('d-none');
    // $(document).ready(hideLoader);
}

function viewName(obj) {
    $('[data-toggle="tooltip"]').tooltip('dispose');
    $.get({
        url: window.location.origin + window.location.pathname + '/profile-name'
    }).done(function(data) {
        $('#submitBidButton').removeClass('not-subscribed');
        $('#submitBidButton').tooltip('dispose')
        $('.credit-balance').html(data.credit.credits);

        $('.credit-balance').addClass('shake-horizontal');
        setTimeout(function() {
            $('.credit-balance').removeClass('shake-horizontal');
        }, 500);
        $(obj).parent().scramble(data.data.instagramHandle, function() {
            var element = document.getElementById("submitBidButton");
            element.classList.remove("d-none");
            $('#engagement').addClass('mt-5');
        });
    }).fail(function() {
        $(obj).parent().scramble($(obj).parent().children().first().text(), function() {
            $('#viewNameLabel').html("You dont have sufficient credits");
            $('#viewNameMessage').modal('show');
        });
    });
}

function addProfilestoCampaignReq(event) {
    var campId = $("#campName").val();
    if (campId?.trim() != "") {
        var profileList = [];
        var handlerName = $('#influencerHandle').html();
        var campaignList = {
            "campaignName": $("#campName option:selected").text(),
            "campaignId": $("#campName").val(),
            isValid: $influencerModel.isValid,
            influencerId: $influencerModel.influencerId,
            influencerHandle: $influencerModel.influencerHandle,
            reportId: $influencerModel.reportId,
            reportPresent: $influencerModel.reportPresent,
            platform: $influencerModel.platform
        }
        profileList.push(campaignList)
        if (profileList.length > 0) {
            var options = {
                type: "POST",
                url: "/brand/view-influencer/add-to-new-campaign",
                headers: {
                    "content-type": "Application/json"
                },

                data: JSON.stringify(profileList)
            };

            $.ajax(options).done(function(data) {

            $('#add_to_campaign').addClass('disabled')
                toast("Influencer added Successfully")
                window.location.replace(window.location.origin + '/brand/campaign/'+campId+'/influencers');

            }).fail(function(data) {
                toast("Something went wrong, Please try again later !", "error")

            });
        } else {
            toast("Something went wrong, Please try again later !", "error")
        }
    } else {
        toast("Select Campaign Name", "error")
    }
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};

$('#submitBidButton').click(function() {
    if (!$(this).hasClass('not-subscribed')) {
        $('#SubmitBid').modal();
        var platformSubmitBid = $influencerModel.platform;
        $('#platform-submit-bid').val(platformSubmitBid);
        $("#paymentType").empty();
        $("#postType").empty();
        $("#postDuration").empty();

        var paymentTypeInsta = '<option value="" hidden="true">Select payment type</option>' +
            '<option value="Cash">Cash</option>' +
            '<option value="Product/Service">Product/Service</option>' +
            '<option value="Shoutout">Shoutout</option>' +
            '<option value="Affiliate">Affiliate</option>';

        var paymentTypeYT = '<option value="" hidden="true">Select payment type</option>' +
            '<option value="Cash">Cash</option>' +
            '<option value="Product/Service">Product/Service</option>' +
            '<option value="Affiliate">Affiliate</option>';

        var postTypeInsta = '<option value="" hidden="true">Select post type</option>' +
            '<option value="Image">Image</option>' +
            '<option value="Video">Video</option>' +
            '<option value="IGTV">IGTV</option>' +
            '<option value="Story">Story</option>';

        var postTypeYoutube = '<option value="" hidden="true">Select post type</option>' +
            '<option value="integratedvideo">Integrated Video</option>' +
            '<option value="DedicatedVideo">Dedicated Video</option>' +
            '<option value="PostRoll">Post Roll</option>' +
            '<option value="Intro">Intro</option>' +
            '<option value="Shoutout">Shout Out</option>' +
            '<option value="Other">Other</option>';

        var postTypeTiktok = '<option value="" hidden="true">Select post type</option>' +
            '<option value="SponsoredPost">Sponsored Post</option>';

        var postDurationInsta = '<option value="" hidden="true">Select post duration</option>' +
            '<option id="2H" class="story" value="2H" hidden="hidden">2 Hours</option>' +
            '<option id="6H" class="story" value="6H" hidden="hidden">6 Hours</option>' +
            '<option id="12H" class="story" value="12H" hidden="hidden">12 Hours</option>' +
            '<option id="24H" class="story image video" value="24H" hidden="hidden">24 Hours</option>' +
            '<option id="1W" class="image video" value="1W" hidden="hidden">1 Week</option>' +
            '<option id="P" class="igtv image video" value="P" hidden="hidden">Permanent</option>';

        if (platformSubmitBid == "instagram") {
            $("#paymentType").append(paymentTypeInsta);
            $("#postType").append(postTypeInsta);
            $("#postDuration").append(postDurationInsta);
            $('#postDuration').removeClass("disabled");
            $('#view-postDuration').addClass("col-md-3 md-form pl-0 d-inline-block");
            $('#view-postDuration').removeClass("d-none");
            $('.yt').addClass("col-md-3");
            $('.yt').removeClass("col-md-4");
            $('#pricing').removeClass('d-none');
        }
        if (platformSubmitBid == "youtube" || platformSubmitBid == "tiktok") {
            $("#paymentType").append(paymentTypeYT);
            $('#postDuration').empty();
            $('#postDuration').addClass("disabled");
            $('#view-postDuration').removeClass("col-md-3 md-form pl-0 d-inline-block");
            $('#view-postDuration').addClass("d-none");
            $('.yt').addClass("col-md-4");
            $('.yt').removeClass("col-md-3");
            $('#pricing').addClass('d-none');

            if (platformSubmitBid == "youtube") {
                $("#postType").append(postTypeYoutube);
            } else {
                $("#postType").append(postTypeTiktok);
            }
        }
    } else {
        WARNING('Reveal the influencer\'s handle before submitting a bid')
    }
    resetForms();
})

function backToSearch(e) {
    if (e) {
        e.preventDefault();
    }

    let url = new URL(window.location.origin + '/brand/explore-influencers');
    window.open(url, "_self");
}

history.pushState(null, null, location.href);
window.addEventListener('popstate', function() {
    window.scrollTo(0, document.body.scrollHeight);
    history.pushState(null, null, location.href);
});

// SUBMIT BID START
$('#postType').change(function(event) {
    $('#postType').removeClass("border-danger");
    $('#postTypeInvalid').empty();

    if ($('#postType').val() == "Story") {
        $('#bid').removeClass("border-danger");
        $('#bidAmountInvalid').empty();
    }
});

$('#postDuration').change(function(event) {
    $('#postDuration').removeClass("border-danger");
    $('#postDurationInvalid').empty();

    if ($('#postDuration').val() == "24H" || $('#postDuration').val() == "1W") {
        $('#bid').removeClass("border-danger");
        $('#bidAmountInvalid').empty();
    }
});

$('#paymentType')
    .change(
        function(event) {
            $('#paymentType').removeClass("border-danger");
            $('#bid').removeClass("border-danger");
            $('#bidAmountInvalid').empty();
            $('#paymentTypeInvalid').empty();

            if ($('#paymentType').val() == 'Product/Service' ||
                $('#paymentType').val() == 'Shoutout') {
                $('.not-affiliate').show();
                $('.not-affiliate').addClass('d-inline-block');
                $('.affiliate').addClass('col-md-3');
                $('.affiliate').removeClass('col-md-4');
                $('#payment-non-cash')
                    .html(
                        "Please include the cash equivalent value of the product, service or shoutout you are providing in the bid amount.")
                    .show();
                $('#affiliateSuffixDiv').addClass('d-none');
                $('#bidAmountLabel').html('Bid Amount in $');
            } else if ($('#paymentType').val() == 'Affiliate') {
                $('#bid').val(0);
                $('.not-affiliate').hide();
                $('.not-affiliate').removeClass('d-inline-block');
                $('.affiliate').removeClass('col-md-3');
                $('.affiliate').addClass('col-md-4');
                $('#payment-non-cash')
                    .html(
                        "Smartfluence will recommend an affiliate post schedule for the influencer. Please select whether you will be paying as a % of sales or $ per sale.")
                    .show();
                $('#affiliateSuffixDiv').removeClass('d-none');
                $('#bidAmountLabel').html('Affiliate %');
            } else {
                $('.not-affiliate').show();
                $('.not-affiliate').addClass('d-inline-block');
                $('.affiliate').addClass('col-md-3');
                $('.affiliate').removeClass('col-md-4');
                $('#payment-non-cash').hide();
                $('#affiliateSuffixDiv').addClass('d-none');
                $('#bidAmountLabel').html('Bid Amount in $');
            }
            $('#mailTemplate option').attr('hidden', 'hidden');
            $('#mailTemplate .true').removeAttr('hidden');

            if ($('#paymentType').val() == 'Affiliate') {
                if ($('#affiliateType').val() == '%') {
                    $('#mailTemplate .Affiliate.percentage')
                        .removeAttr('hidden');
                } else if ($('#affiliateType').val() == '$') {
                    $('#mailTemplate .Affiliate.fee').removeAttr(
                        'hidden');
                }
            } else if ($('#paymentType').val() == 'Product/Service') {
                $('#mailTemplate .Product').removeAttr('hidden');
            } else {
                $('#mailTemplate ' +
                        (this.value.trim() == '' ? '' : '.' +
                            this.value.trim().replace(
                                / /g, '_')))
                    .removeAttr('hidden');
            }

            if ($("#platform-submit-bid").val() != 'instagram') {
                $('#view-postDuration').removeClass("col-md-3 md-form pl-0 d-inline-block");
                $('#view-postDuration').addClass("d-none");
                $('.yt').removeClass("col-md-3");
                $('.yt').addClass("col-md-4");
            }

            $('#mailTemplate').val('').change();
            $('#mailTextArea').val('');
            $('#mailSubject').val('');
        });

$('#affiliateType').change(function(event) {
    var affiliateType = $('#affiliateType').val();
    if (affiliateType == "%") {
        $('#bidAmountLabel').html('Affiliate %');
    } else if (affiliateType == "$") {
        $('#bidAmountLabel').html('Affiliate Fee');
    }
    $('#bid').removeClass("border-danger");
    $('#bidAmountInvalid').empty();
    $('#mailTemplate option').attr('hidden', 'hidden');
    $('#mailTemplate .true').removeAttr('hidden');

    if ($('#paymentType').val() == 'Affiliate') {
        if ($('#affiliateType').val() == '%') {
            $('#mailTemplate .Affiliate.percentage')
                .removeAttr('hidden');
        } else if ($('#affiliateType').val() == '$') {
            $('#mailTemplate .Affiliate.fee').removeAttr(
                'hidden');
        }
    } else if ($('#paymentType').val() == 'Product/Service') {
        $('#mailTemplate .Product').removeAttr('hidden');
    } else {
        $('#mailTemplate ' +
                (this.value.trim() == '' ? '' : '.' +
                    this.value.trim().replace(
                        / /g, '_')))
            .removeAttr('hidden');
    }
    $('#mailTemplate').val('').change();
    $('#mailTextArea').val('');
    $('#mailSubject').val('');
});

$('#postType').on('change', function() {
    var postType = $('#postType').val();
    if (postType.length > 0) {
        $('#postDuration').val('');
        $('#postDuration option').attr('hidden', 'hidden');
        $('.' + postType.toLowerCase()).removeAttr('hidden');
    }
})

$('#bid')
    .on(
        'input',
        function() {
            var value = $('#bid').val();
            if ($('#postType').val() == "Story" ||
                ($('#postType').val() == "Image" && ($(
                    '#postDuration').val() == "24H" || $(
                    '#postDuration').val() == "1W"))) {

                if ($('#paymentType').val() == "Affiliate") {
                    var affiliateType = $('#affiliateType').val();

                    if (affiliateType == "%" &&
                        (value <= 0 || value > 100)) {
                        $('#bid').addClass("border-danger");
                        $('#bidAmountInvalid').html(
                            "Enter a valid percentage");
                        bidBoolean = false;
                    } else if (affiliateType == "$" && (value <= 0)) {
                        $('#bid').addClass("border-danger");
                        $('#bidAmountInvalid').html(
                            "Enter a valid fee");
                        bidBoolean = false;
                    } else {
                        $('#bid').removeClass("border-danger");
                        $('#bidAmountInvalid').empty();
                    }
                } else {
                    var value = $('#bid').val();
                    var bid = document.getElementById("bid");
                    bid.setCustomValidity('Enter the bid amount');
                    if (value == "" || Number(value) <= 0) {
                        $('#bid').addClass("border-danger");
                        $('#bidAmountInvalid').html(
                            bid.validationMessage);
                    } else {
                        $('#bid').removeClass("border-danger");
                        $('#bidAmountInvalid').empty();
                    }
                }
            } else {
                if ($('#paymentType').val() == "Affiliate") {
                    var affiliateType = $('#affiliateType').val();
                    if (affiliateType == "%" &&
                        (value <= 0 || value > 100)) {
                        $('#bid').addClass("border-danger");
                        $('#bidAmountInvalid').html(
                            "Enter a valid percentage");
                        bidBoolean = false;
                    } else if (affiliateType == "$" && (value <= 0)) {
                        $('#bid').addClass("border-danger");
                        $('#bidAmountInvalid').html(
                            "Enter a valid fee");
                        bidBoolean = false;
                    } else {
                        $('#bid').removeClass("border-danger");
                        $('#bidAmountInvalid').empty();
                    }
                } else {
                    var value = $('#bid').val();
                    var bid = document.getElementById("bid");
                    var minimumAmount = ($influencerModel.lastPrice * 0.75)
                        .toFixed(0);
                    bid.setCustomValidity('Your Bid should be greater than 0$');
                    if (Number(value) > 0) {
                        $('#bid').removeClass("border-danger");
                        $('#bidAmountInvalid').empty();
                    } else {
                        $('#bid').addClass("border-danger");
                        $('#bidAmountInvalid').html(
                            bid.validationMessage);
                    }
                }
            }
        })

function submitBid() {

    var postTypeVal = $('#postType').val();
    var paymentTypeVal = $('#paymentType').val();
    var postDurationVal = $influencerModel.platform == 'instagram' ? $('#postDuration').val() : '';
    if (paymentTypeVal == 'Affiliate') {
        postTypeVal = null;
        postDurationVal = null;
    }
    var affiliateType = $('#affiliateType').val();
    var campaign = $('#campaign').val().trim();
    var campaignId;


    var opt = $('option[value="' + campaign + '"]');
    if (opt.length) {
        campaignId = opt.attr('id');
    } else {
        campaignId = '';
    }
    var mailTemplate = $("#mailTemplate").val();
    var mailDescription = $('#mailTextArea').val();
    var mailSubject = $('#mailSubject').val();
    var mailCc = $('#mailCc').val();
    mailCc = mailCc.split(' ').join(',');
    $('#skipForNow').addClass('disabled');
    $('.submitBid').addClass('disabled');
    $('#submitBid').addClass('disabled');
    $('.back').addClass('disabled');

    $.post({
        url: window.location.origin + '/brand/view-influencer/submit-bid',
        data: {
            postType: postTypeVal,
            postDuration: postDurationVal,
            paymentType: paymentTypeVal,
            bidAmount: (parseFloat($('#bid').val())).toFixed(2),
            affiliateType: affiliateType,
            campaignName: $('#campaign').val().trim(),
            campaignId: campaignId,
            isValid: $influencerModel.isValid,
            mailTemplateId: mailTemplate,
            mailDescription: mailDescription,
            mailSubject: mailSubject,
            mailCc: mailCc,
            influencerId: $influencerModel.influencerId,
            influencerHandle: $influencerModel.influencerHandle,
            reportId: $influencerModel.reportId,
            reportPresent: $influencerModel.reportPresent,
            platform: $influencerModel.platform
        },
    }).done(function(data) {
        $('#SubmitBid').modal('hide');
        toast(data.data);
        $('#skipForNow').removeClass('disabled');
        $('.submitBid').removeClass('disabled');
        $('.back').removeClass('disabled');
        $('#addToCampaign').removeClass('d-none');
        $('#submitBid').removeClass('disabled');
        resetFormSubmitBid();
    }).fail(function(data) {
        $('#SubmitBid').modal('hide');
        $('#skipForNow').removeClass('disabled');
        $('.submitBid').removeClass('disabled');
        $('.back').removeClass('disabled');
        $('#submitBid').removeClass('disabled');
        resetFormSubmitBid();
        if (data.responseText != null) {
            toast(data.responseText, 'error');
        } else {
            toast('Failed to submid bid', 'error');
        }
    });
}

function resetFormSubmitBid() {
    $('#invalidCampaign').html('');
    $('.submitBid-tab-label').removeClass('skip');
    $('.mailTemplate-label').removeClass('skip');
    $('#payment-non-cash').hide();
    $('.text-danger').html('');
    $('.border-danger').removeClass('border-danger');
    $('#SubmitBid input,#SubmitBid select').val('').change();
    $('#mailTextArea').val('');
}

function createNewCampaign() {
    $('.new-campaign').removeClass('scale-down-center');
    $('.new-campaign').removeClass('d-none');
    $('.new-campaign').addClass('scale-up-center');
}

function closeNewCampaign() {
    $('.new-campaign').removeClass('scale-up-center');
    $('.new-campaign').addClass('scale-down-center');
    var delayInMilliseconds = 350;
    setTimeout(function() {
        $('.new-campaign').addClass('d-none');
    }, delayInMilliseconds);
}

$('#campaign').on('change', function() {
    var campaign = $('#campaign').val().trim();
    $('#skipForNow').removeClass('d-none');
    var opt = $('option[value="' + campaign + '"]');

    if (opt.length) {
        campaignId = opt.attr('id');
    } else {
        campaignId = '';
    }
    if (campaignId != undefined && campaignId != '' && campaignId.length > 0) {
        $.get({
            url: window.location.origin + '/brand/view-influencer/' + $influencerModel.influencerHandle + '/check-influencer',
            data: {
                campaignName: campaign,
                campaignId: campaignId,
                influencerId: $influencerModel.influencerId,
                platform: $influencerModel.platform
            },
        }).done(function(data) {
            if (data == true) {
                $('#skipForNow').addClass('d-none');
                $('#invalidCampaign').removeClass('d-none');
                $('#invalidCampaign').html('Influencer already exists in the campaign. Clicking on Next will take you to submit bid.');
            } else {
                $('#skipForNow').removeClass('d-none');
                $('#invalidCampaign').addClass('d-none');
            }
        }).fail(function(data) {

        });
    }
})

$('#campaign').on('input', 'change', function() {
    var campaign = $('#campaign').val().trim();
    $('#skipForNow').removeClass('d-none');
    var opt = $('option[value="' + campaign + '"]');

    if (opt.length) {
        campaignId = opt.attr('id');
    } else {
        campaignId = '';
    }
    if (campaignId != undefined && campaignId != '' && campaignId.length > 0) {
        $.get({
            url: window.location.origin + '/brand/view-influencer/' + $influencerModel.influencerHandle + '/check-influencer',
            data: {
                campaignName: campaign,
                campaignId: campaignId,
                influencerId: $influencerModel.influencerId,
                platform: $influencerModel.platform
            },
        }).done(function(data) {
            if (data == true) {
                $('#skipForNow').addClass('d-none');
                $('#invalidCampaign').removeClass('d-none');
                $('#invalidCampaign').html('Influencer already exists in the campaign. Clicking on Next will take you to submit bid.');
            } else {
                $('#skipForNow').removeClass('d-none');
                $('#invalidCampaign').addClass('d-none');
            }
        }).fail(function(data) {
            toastr.error("failed");
        });
    }
})

var campaign;

function validateCampaign() {
    campaign = $('#campaign').val().trim();

    var opt = $('option[value="' + campaign + '"]');
    if (opt.length) {
        campaignId = opt.attr('id');
    } else {
        campaignId = '';
    }

    if (campaign == "") {
        $('#invalidCampaign').removeClass('d-none');
        $('#invalidCampaign').html('Enter Campaign Name');
    } else {
        sendEvent('#SubmitBid', 2);
        $('#bid').blur();
    }
}

$('#campaign').on('input', function() {
    $('#invalidCampaign').addClass('d-none');
});

$('#campaign').on('input', createOrSelect).change(createOrSelect);

function createOrSelect() {
    var exists = $('datalist option[value="' + this.value.trim() + '"]').length != 0;
}

var campaign;

function saveCampaign() {
    campaign = $('#campaign').val().trim();
    var campaignId;
    var hasCampaign = $('#campaigns [value="' + campaign + '"]').data('value');

    var opt = $('option[value="' + campaign + '"]');
    if (opt.length) {
        campaignId = opt.attr('id');
    } else {
        campaignId = '';
    }

    if (campaign != "") {
        $('.back-2').addClass('disabled');
        $('.yes').addClass('disabled');
        $('#addToCampaign').addClass('disabled');

        $.post({
            url: window.location.origin + '/brand/view-influencer/add-to-campaign',
            data: {
                campaignName: campaign,
                campaignId: campaignId,
                isValid: $influencerModel.isValid,
                influencerId: $influencerModel.influencerId,
                influencerHandle: $influencerModel.influencerHandle,
                reportId: $influencerModel.reportId,
                reportPresent: $influencerModel.reportPresent,
                platform: $influencerModel.platform
            },
        }).done(function(data) {
            $('#SubmitBid').modal('hide');
            $(".close").click();
            toast('The Influencer has been successfully added to the Campaign!');
            $('.addToCampaignMesage').addClass('d-none');
            $('#submitBid').removeClass('d-none');
            $('#addToCampaign').removeClass('disabled');
        }).fail(function(data) {
            $('#SubmitBid').modal('hide');
            $(".close").click();
            $('#addToCampaign').removeClass('disabled');
            $('.back-2').removeClass('disabled');
            $('.yes').removeClass('disabled');

            if (data.responseText != null) {
                toast(data.responseText, 'error');
            } else {
                toast('Failed to save campaign', 'error');
            }
        });
    }
}

function checkCampaign() {
    var opt = $('option[value="' + campaign + '"]');
    if (opt.length) {
        campaignId = opt.attr('id');
    } else {
        campaignId = '';
    }
}

var submitBidBoolean;

function setSubmitBidTrue() {
    submitBidBoolean = true;

    $('#step3Body').html('')
    var postTypeBoolean = true;
    var postDurationBoolean = true;
    var bidBoolean = true;
    var paymentTypeBoolean = true;
    var campaignBoolean = true;
    var postType = document.getElementById("postType");
    var postDuration = document.getElementById("postDuration");
    var paymentType = $('#paymentType').val();
    var bidAmount = $('#bid').val();
    var affiliateType = $('#affiliateType').val();
    var paymentTypeMessage = 'Select Payment Type';

    if (paymentType == '') {
        $('#paymentType').addClass("border-danger");
        $('#paymentTypeInvalid').html(paymentTypeMessage);
        paymentTypeBoolean = false;
    } else if (paymentType == "Affiliate") {
        var affiliateType = $('#affiliateType').val();
        if (affiliateType == "%" && (bidAmount <= 0 || bidAmount > 100)) {
            $('#bid').addClass("border-danger");
            $('#bidAmountInvalid').html("Enter a valid percentage");
            bidBoolean = false;
        } else if (affiliateType == "$" && (bidAmount <= 0)) {
            $('#bid').addClass("border-danger");
            $('#bidAmountInvalid').html("Enter a valid fee");
            bidBoolean = false;
        }
    }

    if (paymentType != "Affiliate") {
        affiliateType = null;
        postType.setCustomValidity('Select Post Type');
        if (postType.value == '') {
            $('#postType').addClass("border-danger");
            $('#postTypeInvalid').html(postType.validationMessage);
            postTypeBoolean = false;
        }

        if ($influencerModel.platform == 'instagram') {
            postDuration.setCustomValidity('Select Post Duration');
            if (postDuration.value == '') {
                $('#postDuration').addClass("border-danger");
                $('#postDurationInvalid').html(postDuration.validationMessage);
                postDurationBoolean = false;
            }
        }

        if ($('#postType').val() == "Story" ||
            ($('#postType').val() == "Image" && ($('#postDuration')
                .val() == "24H" || $('#postDuration').val() == "1W"))) {
            var bid = document.getElementById("bid");
            bid.setCustomValidity('Enter the bid amount');
            if (bidAmount == "" || Number(bidAmount) <= 0) {
                $('#bid').addClass("border-danger");
                $('#bidAmountInvalid').html(bid.validationMessage);
                bidBoolean = false;
            }
        } else {
            var bid = document.getElementById("bid");
            var minimumAmount = ($influencerModel.lastPrice * 0.75).toFixed(0);
            bid.setCustomValidity('Your Bid should be greater than 0$');
            if (Number(bidAmount) > 0) {
                bidBoolean = true;
            } else {
                $('#bid').addClass("border-danger");
                $('#bidAmountInvalid').html(bid.validationMessage);
                bidBoolean = false;
            }
        }
    }

    if (postTypeBoolean && postDurationBoolean && bidBoolean && paymentTypeBoolean && campaignBoolean) {
        $('#step3Body').html('The Influencer <span style="font-style: oblique;font-weight:bold;">' + $influencerModel.influencerHandle + '</span> will be added to the Campaign <span style="font-style: oblique;font-weight:bold;">' + campaign + '</span> and a bid will be submitted with the following details:');
        $('.submitBidTable').removeClass('d-none');
        if (paymentType == "Affiliate") {
            var affilateType = $('#affiliateType').val();
            var affilateMode;
            $('#postTypeTableLabel').addClass('d-none');
            $('#postDurationTableLabel').addClass('d-none');
            $('#paymentModeTableLabel').removeClass('d-none');
            $('#postTypeTable').addClass('d-none');
            $('#postDurationTable').addClass('d-none');
            $('#paymentModeTable').removeClass('d-none');

            if (affilateType == '$') {
                $('#bidAmountTableLabel').text('Affiliate Fee');
                affilateMode = '$ per Sale';
                $('#bidAmountTable').html('$' + $("#bid").val());
            } else if (affilateType == '%') {
                $('#bidAmountTableLabel').text('Affiliate %');
                affilateMode = '% of Sales';
                $('#bidAmountTable').html($("#bid").val() + '%');
            }
            $('#paymentModeTable').html(affilateMode);
        } else {
            $('#bidAmountTable').html('$' + $("#bid").val());
            $('#postTypeTableLabel').removeClass('d-none');
            $('#paymentModeTableLabel').addClass('d-none');
            $('#postTypeTable').removeClass('d-none');
            $('#paymentModeTable').addClass('d-none');
            $('#bidAmountTableLabel').text('Bid Amount');

            if ($influencerModel.platform != 'instagram') {
                $('#postDurationTableLabel').addClass('d-none');
                $('#postDurationTable').addClass('d-none');
            } else {
                $('#postDurationTableLabel').removeClass('d-none');
                $('#postDurationTable').removeClass('d-none');
            }
        }

        $('#postTypeTable').html($("#postType option:selected").text());
        $('#postDurationTable').html($("#postDuration option:selected").text());
        $('#paymentTypeTable').html($("#paymentType option:selected").text());

        $('.addToCampaignMesage').addClass('d-none');
        $('#submitBid').removeClass('d-none');
        $('#addToCampaign').addClass('d-none');
        sendEvent('#SubmitBid', 3);
        $('#mailCc').val(profileDetails.email);
        $('#mailTemplate .true').removeAttr('hidden');

        if ($('#paymentType').val() == 'Affiliate') {
            if ($('#affiliateType').val() == '%') {
                $('#mailTemplate .Affiliate.percentage').removeAttr('hidden');
                $('#mailTemplate .Affiliate.percentage').prop('selected', true);
                $('#mailTemplate .Affiliate.fee').change();
            } else if ($('#affiliateType').val() == '$') {
                $('#mailTemplate .Affiliate.fee').removeAttr('hidden');
                $('#mailTemplate .Affiliate.fee').prop('selected', true);
                $('#mailTemplate .Affiliate.fee').change();
            }
        } else if ($('#paymentType').val() == 'Product/Service') {
            $('#mailTemplate .Product').removeAttr('hidden');
            $('#mailTemplate .Product').prop('selected', true);
            $('#mailTemplate .Affiliate.fee').change();
        } else {
            $(
                '#mailTemplate ' +
                ($('#paymentType').val().trim() == '' ? '' : '.' +
                    $('#paymentType').val().trim().replace(
                        / /g, '_'))).removeAttr('hidden');
            $(
                '#mailTemplate ' +
                ($('#paymentType').val().trim() == '' ? '' : '.' +
                    $('#paymentType').val().trim().replace(
                        / /g, '_'))).prop('selected', true);
            $('#mailTemplate .Affiliate.fee').change();
        }
        $('#dataStep4Back').attr('onclick', "sendEvent('#SubmitBid', 3)");
    }
}


function setSubmitBidFalse() {
    $('.submitBid-tab-label').addClass('skip');
    $('.mailTemplate-label').addClass('skip');
    submitBidBoolean = false;
    $('.submitBidTable').addClass('d-none');
    $('#step3Body').html('The Influencer <span style="font-style: oblique;font-weight:bold;">' + $influencerModel.influencerHandle + '</span> will be added to the Campaign <span style="font-style: oblique;font-weight:bold;">' + campaign + '</span> without submitting a bid.');
    $('.addToCampaignMesage').removeClass('d-none');
    $('#submitBid').addClass('d-none');
    $('#addToCampaign').removeClass('d-none');
    sendEvent('#SubmitBid', 4);
    $('#dataStep4Back').attr('onclick', "sendEvent('#SubmitBid', 2)");
}

$('.back-2').click(function() {
    $('.submitBid-tab-label').removeClass('skip');
    $('.mailTemplate-label').removeClass('skip');
})



function hideURLParams() {
    var hide = ['isValid', 'platform'];
    for (var name in hide) {
        if (decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1])) {
            history.replaceState(null, document.getElementsByTagName("title")[0].innerHTML, window.location.pathname);
        }
    }
}

function generateReport(influencerId) {
    var influencerHandle = $influencerModel.influencerHandle;
    if (influencerHandle.charAt(0) == '@') {
        influencerHandle = influencerHandle.substr(1);
    }
    var platform = $influencerModel.platform;
    if (platform == 'youtube') {
        var ytPattern = new RegExp('(https?:\/\/)?(www\.)?youtu((\.be)|(be\..{2,5}))\/((user)|(channel))\/.+');
        if (ytPattern.test(influencerHandle)) {
            $('#influencerHandleInvalid').html("Enter a valid data");
            return;
        } else {
            influencerHandle = influencerHandle.substring(influencerHandle.lastIndexOf('/') + 1);
        }
    }
    $('#reportMessageCount').addClass('d-none');
    $('#reportMessageDiv').removeClass('d-none');
    $('#reportMessage').html('Generating report for @' + influencerHandle + ', hold tight this will take a few seconds!');
    $('#reportMessageLoader').removeClass('d-none');
    $('#reportMessage').removeClass('text-danger');
    $('#generateReport').addClass('disabled');

    $.get({
        url: window.location.origin + '/brand/reports/audience-data',
        data: {
            influencerId: $influencerModel.influencerId,
            deepSocialInfluencerId: $influencerModel.deepSocialInfluencerId,
            influencerHandle: influencerHandle,
            reportId: $influencerModel.reportId,
            reportPresent: $influencerModel.reportPresent,
            platform: platform
        },
    }).done(function(data) {
        $('#reportMessageCount').html(0 + '%');
        $('#reportMessageCount').removeClass('d-none');
        $('#reportMessage').html('Download in progress ');
        var reportUrl = data["report_url"];
        downloadFile(reportUrl);
        setTimeout(function() {
            var count = $('#reportMessageCount').html();
            if (parseInt(count) < 40) {
                $('#reportMessageCount').html(20 + '%');
            }
        }, 1000);
    }).fail(function(data) {
        if (data.status == 409) {
            $('#reportMessage').html("You have reached the weekly limit of 50 reports! Please contact help@smartfluence.io to increase your limit.");
        } else if (data.status == 403) {
            WARNING(data.responseText);
            $('#reportMessage').html('');
            $('#reportMessageLoader').addClass('d-none');
            $('#reportMessage').addClass('text-danger');
        } else {
            if (data.responseText.includes("retry_later")) {
                $('#reportMessage').html("We are pulling data for this influencer! Please try again in few minutes");
            } else if (data.responseText.includes("empty_audience_data")) {
                $('#reportMessage').html("Currently we do not have data for this influencer");
            } else {
                $('#reportMessage').html(influencerHandle + " seems to be an invalid Instagram handle. Please try a different username.");
            }
        }
        $('#reportMessageLoader').addClass('d-none');
        $('#generateReport').removeClass('disabled');
        $('#reportMessage').addClass('text-danger');
    });
}

function resetFormSearchModal() {
    $('#influencerHandleInvalid').html('');
    $('#searchInfluencerModalFooter').addClass('d-none');
    $('#searchInfluencerModal input,#searchInfluencerModal select').val('').change();
}

function downloadFile(url) {
    console.log('Download started at ' + new Date());
    setTimeout(function() {
        var count = $('#reportMessageCount').html();
        if (parseInt(count) < 65) {
            $('#reportMessageCount').html(45 + '%');
        }
    }, 4500);
    setTimeout(function() {
        var count = $('#reportMessageCount').html();
        if (parseInt(count) < 85) {
            $('#reportMessageCount').html(65 + '%');
        }
    }, 5500);
    setTimeout(function() {
        var count = $('#reportMessageCount').html();
        if (parseInt(count) < 95) {
            $('#reportMessageCount').html(85 + '%');
        }
    }, 7500);
    fetch(url)
        .then(resp => {
            // $('#reportMessage').html('Download completed 75%');
            $('#reportMessageCount').html(95 + '%');
            console.log('Download completed 75% ' + new Date());
            var filename = 'report.pdf';
            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            var matches = filenameRegex.exec(resp.headers.get('Content-Disposition'));
            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            resp.blob().then(function(b) {
                // $('#reportMessage').html('Download completed 98%');
                console.log('Download completed 98% at ' + new Date());
                const url = window.URL.createObjectURL(b);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.href = url;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                $('#reportMessage').html('Download completed ');
                $('#reportMessageCount').html(100 + '%');
                $('#reportMessageLoader').addClass('d-none');
                console.log('Download completed 100% at ' + new Date());
                window.URL.revokeObjectURL(url);
                $('#generateReport').removeClass('disabled');
            }).catch((e) => {
                $('#reportMessage').html('Download failed, try again later');
                $('#generateReport').removeClass('disabled');
                $('#reportMessageLoader').addClass('d-none');
            });
        }).catch((e) => {
            $('#reportMessage').html('Download failed, try again later');
            $('#generateReport').removeClass('disabled');
            $('#reportMessageLoader').addClass('d-none');
        });
}

$(function() {
    $("#mailTemplate").change(
        function() {
            $('#mailTemplateInvalid').text('');
            if ($("#mailTemplate").val() != '' && $("#mailTemplate").val() != undefined && $("#mailTemplate").val() != null) {
                var element = $(this).find('option:selected');
                var mailDescription = element.attr("data-value");
                var mailSubject = element.attr("data-subject");

                var mailPlatfrom = getPlatform($influencerModel.platform);
                mailDescription = mailDescription.split("<Platform>")
                    .join(mailPlatfrom);
                mailSubject = mailSubject.split("<Brand Name>").join(
                    profileDetails.brandName);
                mailSubject = mailSubject.split("<Influencer Handle>")
                    .join($influencerModel.influencerHandle);
                mailSubject = mailSubject.split("<Estimated Pricing>")
                    .join('$' + Number($influencerModel.lastPrice).toFixed(2));
                mailDescription = mailDescription.split("<Influencer Handle>")
                    .join($influencerModel.influencerHandle);
                mailDescription = mailDescription.split("<Brand Name>").join(
                    profileDetails.brandName);
                mailDescription = mailDescription.split("<Bid Amount>").join(
                    $('#bid').val());
                mailDescription = mailDescription.split("<Post Type>").join(
                    getPostType($('#postType').val()));
                if ($influencerModel.platform == 'instagram') {
                    var postDuration = getPostDuration($('#postDuration').val());
                    mailDescription = mailDescription.split("<Post Duration>")
                        .join(postDuration);
                    var affiliateInstaText = '\nTo maximize your potential earnings, we recommend you to do the following:\n\n' +
                        '\tkeep your unique affiliate link in your bio (direct your audience to your bio)\n' +
                        '\tcreate 1 feed post\n' +
                        '\tcreate 2 stories every week\n';
                    mailDescription = mailDescription.split("<Affiliate Instagram>").join(affiliateInstaText);
                } else {
                    mailDescription = mailDescription.split("<Post Duration>")
                        .join('');
                    mailDescription = mailDescription.split("<Affiliate Instagram>").join('');
                }
                mailDescription = mailDescription.split("<Affilitate %>").join(
                    $('#bid').val());
                mailDescription = mailDescription.split("<Affiliate Fee>")
                    .join($('#bid').val());
                mailDescription = mailDescription.split("<apostrophe>")
                    .join("'");
                $('#mailTextArea').val(mailDescription);
                $('#mailSubject').val(mailSubject);
            }
        });
});

function getPlatform(platform) {
    switch (platform) {
        case "instagram":
            platform = "Instagram";
            break;
        case "youtube":
            platform = "YouTube";
            break;
        case "tiktok":
            platform = "TikTok";
            break;
        default:
            break;
    }
    return platform;
}

function getPostType(postType) {
    switch (postType) {
        case "IntegratedVideo":
            postType = "Integrated Video";
            break;
        case "DedicatedVideo":
            postType = "Dedicated Video";
            break;
        case "PostRoll":
            postType = "Post Roll";
            break;
        case "Shoutout":
            postType = "Shout Out";
            break;
        case "SponsoredPost":
            postType = "Sponsored Post";
            break;
        default:
            break;
    }
    return postType;
}

function getPostDuration(postDuration) {
    switch (postDuration) {
        case "2H":
            postDuration = "2 hours";
            break;
        case "6H":
            postDuration = "6 hour";
            break;
        case "12H":
            postDuration = "12 hour";
            break;
        case "24H":
            postDuration = "24 hour";
            break;
        case "1W":
            postDuration = "1 week";
            break;
        case "P":
            postDuration = "permanent";
            break;
        default:
            break;
    }
    return postDuration;
}

function validateMailContent() {
    var mailTemplate = $("#mailTemplate").val();
    var mailDescription = $('#mailTextArea').val();
    var mailSubject = $('#mailSubject').val();

    if (mailTemplate == '' || mailTemplate == undefined) {
        $('#mailTemplateInvalid').text('Select Mail Template');
        return;
    }
    if (mailDescription == '' || mailDescription == undefined) {
        $('#mailTextAreaInvalid').text('Enter data');
        return;
    }
    if (mailSubject == '' || mailSubject == undefined) {
        $('#mailSubjectInvalid').text('Enter Subject');
        return;
    }
    sendEvent('#SubmitBid', 4);
}

$('#mailTextArea').on('input', function() {
    $('#mailTextAreaInvalid').text('');
});

$('#mailSubject').on('input', function() {
    $('#mailSubjectInvalid').text('');
});