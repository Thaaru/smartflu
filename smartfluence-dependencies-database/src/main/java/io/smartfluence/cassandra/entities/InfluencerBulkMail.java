package io.smartfluence.cassandra.entities;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.InfluencerBulkMailKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("influencer_bulk_mail")
public class InfluencerBulkMail implements Serializable {

	private static final long serialVersionUID = -5684967004348569640L;

	@PrimaryKey
	private InfluencerBulkMailKey key;

	@Column("influencer_handle")
	private String influencerHandle;

	@Column("influencer_email")
	private String influencerEmail;

	@Indexed
	@Column("created_at")
	private Date createdAt;

	@Column("modified_at")
	private Date modifiedAt;
}