package io.smartfluence.modal.validation;

import java.util.TimeZone;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import io.smartfluence.constants.UserType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationModal {

	@NotNull(message = "Select whether you want to register as a brand or an infuencer")
	private UserType userType = UserType.BRAND;

	@NotBlank(message = "Enter your brand name")
	private String brandName;

	@NotBlank(message = "Select an industry")
	private String industry;

	private String firstName;

	private String lastName;

	@Pattern(regexp = "(https?:\\/\\/)?([0-9a-zA-Z]+(-[0-9a-zA-Z]+)*)(\\.([0-9a-zA-Z]+(-[0-9a-zA-Z]+)*))*\\.[0-9a-zA-Z]{2,12}", message = "Enter valid website URL")
	private String website;

	@NotBlank(message = "Enter your valid email")
	@Email(message = "Enter your valid email")
	private String email;

	@NotBlank(message = "Password should contain alteast 8 characters")
	@Size(min = 8, message = "Password should contain alteast 8 characters")
	private String password;

	private String freeSubscriptionCode;

	private Boolean isFreeAccount;

	private TimeZone timeZone;

	public boolean isBrand() {
		//return userType != null && userType.equals(UserType.BRAND);
		return true;
	}

	public boolean isInfluencer() {
		//return userType != null && userType.equals(UserType.INFLUENCER);
		return false;
	}
}