package io.smartfluence.modal.social.customlist;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ManageCustomList implements Serializable {

	private static final long serialVersionUID = -4226970313092296864L;

	@JsonAlias("success")
	private Boolean success;

	@JsonAlias("data")
	private CustomId customId;

	@JsonAlias("inserted_count")
	private Integer insertedCount;

	@JsonAlias("removed_count")
	private Integer removedCount;
}