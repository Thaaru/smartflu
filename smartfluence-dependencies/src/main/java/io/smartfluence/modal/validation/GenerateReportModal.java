package io.smartfluence.modal.validation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GenerateReportModal {

	@NotBlank(message = "Enter the name")
	private String name;

	@NotBlank(message = "Enter the brand name")
	private String brandName;

	@NotBlank(message = "Enter valid email")
	@Email(message = "Enter valid email")
	private String businessEmailAddress;

	@NotBlank(message = "Enter instagram user name")
	private String instagramUserName;

	@NotBlank(message = "select describe")
	private String describe;

	@NotBlank(message = "Select no of influencers")
	private String noOfInfluencers; 
}