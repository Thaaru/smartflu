package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkflowReqObj {
    private String campaignId;
    private String influencerId;
    private String workflowStatus;
    private String status;
    private String trackingId;
    private String postUrl;

}
