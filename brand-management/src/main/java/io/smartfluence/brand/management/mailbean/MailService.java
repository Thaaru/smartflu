package io.smartfluence.brand.management.mailbean;

import com.nylas.NameEmail;
import io.smartfluence.brand.management.config.NoReplyProperties;
import io.smartfluence.brand.management.config.PartnershipProperties;
import io.smartfluence.brand.management.entityv1.BrandInfluObj;
import io.smartfluence.cassandra.entities.MailHistory;
import io.smartfluence.cassandra.entities.brand.BrandBidDetails;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.primary.MailHistoryKey;
import io.smartfluence.cassandra.repository.MailHistoryRepo;
import io.smartfluence.modal.nylas.request.NylasMailRequest;
import io.smartfluence.modal.validation.brand.explore.SubmitBidModel;
import io.smartfluence.mysql.entities.BrandEmailConnection;
import io.smartfluence.mysql.entities.InfluencerDataQueue;
import io.smartfluence.mysql.entities.MailDetails;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.repository.BrandEmailConnectionRepo;
import io.smartfluence.mysql.repository.MailTemplateMasterRepo;
import io.smartfluence.util.NylasUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.io.*;
import java.time.LocalDateTime;
import java.util.*;

@Component
@Configuration
public class MailService {

	@Autowired
	public JavaMailSender javaMailSenderImplNoReply;

	@Autowired
	protected JavaMailSender javaMailSenderImplPartner;

	@Autowired
	private NoReplyProperties noReplyProperties;

	@Autowired
	private PartnershipProperties partnershipProperties;

	@Autowired
	private MailHistoryRepo mailHistoryRepo;

	@Autowired
	private EurekaInstanceConfigBean instanceConfigBean;

	@Autowired
	private NylasUtil nylasUtil;

	@Autowired
	private MailTemplateMasterRepo mailTemplateMasterRepo;

	@Autowired
	private BrandEmailConnectionRepo brandEmailConnectionRepo;

	@Autowired
	TokenUtil tokenUtil;

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@Value("${nylas.app.access.token}")
	private String appAccessToken;

	@Value("${nylas.demo.access.token}")
	private String demoAccessToken;

	private static final String LIVE = "LIVE";

	private static final String DEMO = "DEMO";

	private static final String LOCALHOST = "localhost";

	private static final String APP_SMARTFLUENCE_IO = "app.smartfluence.io";

	private static final String DEMO_SMARTFLUENCE_IO = "demo.smartfluence.io";

	private static final String ADMIN_MAIL_ADDRESS = "admin@smartfluence.io";

	private static final String ADMIN_MAIL_ADDRESS_TEST = "uday.dinesh@smartfluence.io";

	private static final String INFLUENCER_MAIL_ADDRESS_TEST = "uday.dinesh@smartfluence.io";

	private static final String ERROR_MAIL_ADDRESS = "uday.dinesh@smartfluence.io";


	private static final String INFLUENCER_FROM_MAIL_ADDRESS_TEST = "uday.dinesh@smartfluence.io";

	private static final String YEAR = "year";

	private static final String NAME = "Name";

	private static final String MESSAGE = "Message";

	private static final String TIME = "Time";

	private static final String BRAND_NAME = "brandName";

	private static final String MAIL_BODY = "mailBody";

	private static final String BRAND_EMAIL = "brandEmail";

	private static final String BID_AMOUNT = "bidAmount";

	private static final String POST_DURATION = "postDuration";

	private static final String POST_TYPE = "postType";

	private static final String PAYMENT_TYPE = "paymentType";

	private static final String INFLUENCER_NAME = "influencerName";

	private static final String TYPE_HTML = "text/html";

	private static final String SMARTFLUENCE = "Smartfluence";

	private static final String SMARTFLUENCE_DATA = "smartfluence-data";

	private static final String MAIL_TEMPLATE = "mail-template";

	private static final String INFLUENCERNAME = "<Influencer_Username>";

	private static final String CAMPAIGN_NAME = "<Campaign_Name>";

	private static final String CAMPAIGN_DESCRIPTION = "<Campaign_Description>";

	private static final String BRANDNAME = "<Brand_Name>";

	private static final String PAYMENT = "<Payment>";

//	private static final String LINK = "<Link>";

	private static final String PROPOSAL_LINK = "<Link>";

	private static final String PROMOTE_LINK = "<link>";

	private static final String INFLUENCER_REPORT_LINK = "<link>";

	private final Log logger = LogFactory.getLog(getClass());

	private static final String ERRORREPORTMAIL = "errorReportMail.html";

	private static final String SUBMITBIDMAILTEST = "submitBidMailTest.html";

	private static final String PROMOTECAMPAIGNMAIL = "promoteCampaignMail.html";

	private static final String SUBMITBIDMAILTOADMIN = "submitBidmailToAdmin.html";

	private static final String SUBMITBIDCONFIRMATIONMAILTOBRAND = "submitBidConfirmationMailToBrand.html";

	private static final String SUBMITBIDMAILTOINFLUENCERFORSHOUOUT = "submitBidMailToInfluencerForShoutout.html";

	private static final String SUBMITBIDMAILTOINFLUENCERFORCASH = "submitBidMailToInfluencerForCash.html";

	private static final String SUBMITBIDMAILTOINFLUENCERFORPRODUCT = "submitBidMailToInfluencerForProduct.html";

	private static final String SUBMITBIDMAILTOINFLUENCERFORAFFILIATE = "submitBidMailToInfluencerForAffilate.html";

	private static final String SUBMITPROPOSALMAILTEST = "submitProposalMailToInfluencer.html";

	private static final String SUBMITPROPOSALMAILTOADMIN="submitProposalMailToAdmin.html";

	private static final String SUBMITPROPOSALCONFIRMATIONMAILTOBRAND="submitProposalConfirmationMailToBrand.html";

	private Optional<MailTemplateMaster> getMailTemplateMaster(String templateName) {
		return mailTemplateMasterRepo.findByTemplateName(templateName);
	}

	private Optional<BrandEmailConnection> getBrandEmailConnection(String brandId) {
		return brandEmailConnectionRepo.findByBrandIdAndStatus(brandId, 1);
	}

	public void sendSubmitBidMailToInfluencer(MailDetails mailDetails) {
		new Thread(() -> submitBidMailToInfluencer(mailDetails)).start();
	}

	public void sendPromoteCampaignMailToInfluencers(MailDetails mailDetails) {
		new Thread(() -> submitPromoteCampaignMailToInfluencers(mailDetails)).start();
	}

	public void sendProposalMailToInfluencers(MailDetails mailDetails) {
		new Thread(() -> submitProposalMailToInfluencers(mailDetails)).start();
	}

	public void sendPromoteCampaignInfluencerStatusMail(MailDetails mailDetails) {
		new Thread(() -> submitPromoteCampaignInfluencerStatusMail(mailDetails)).start();
	}

	public void sendPromoteTargetingDetailsNotificationMail(MailDetails mailDetails) {
		new Thread(() -> submitPromoteTargetingDetailsNotificationMail(mailDetails)).start();
	}

	public void sendSubmitBidMailToAdmin(BrandBidDetails brandBidDetails) {
		new Thread(() -> submitBidMailToAdmin(brandBidDetails)).start();
	}

	public void sendSubmitBidConfirmationMailToBrand(BrandBidDetails brandBidDetails) {
		new Thread(() -> submitBidConfirmationMailToBrand(brandBidDetails)).start();
	}

	public void sendProposalMailToAdmin(SubmitBidModel submitBidModel, BrandDetails brandDetails) {
		new Thread(() -> submitProposalMailToAdmin(submitBidModel,brandDetails)).start();
	}

	public void sendProposalConfirmationMailToBrand(SubmitBidModel submitBidModel, BrandDetails brandDetails) {
		new Thread(() -> submitProposalConfirmationMailToBrand(submitBidModel,brandDetails)).start();
	}



	public void sendSubmitBidMailToInfluencerForShoutout(BrandBidDetails brandBidDetails) {
		new Thread(() -> submitBidMailToInfluencerForShoutout(brandBidDetails)).start();
	}

	public void sendSubmitBidMailToInfluencerForAffiliate(BrandBidDetails brandBidDetails) {
		new Thread(() -> submitBidMailToInfluencerForAffiliate(brandBidDetails)).start();
	}

	public void sendSubmitBidMailToInfluencerForCash(BrandBidDetails brandBidDetails) {
		new Thread(() -> submitBidMailToInfluencerForCash(brandBidDetails)).start();
	}

	public void sendSubmitBidMailToInfluencerForProduct(BrandBidDetails brandBidDetails) {
		new Thread(() -> submitBidMailToInfluencerForProduct(brandBidDetails)).start();
	}

	public void sendMailToBrandForInfluencerReport(UUID influencerSFId, InfluencerDataQueue influencerDataQueue) {
		new Thread(() -> submitMailToBrandForInfluencerReport(influencerSFId, influencerDataQueue)).start();
	}

	public void sendErrorMail(RuntimeException ex) {
		new Thread(() -> mailForErrorReport(ex)).start();
	}

	public void sendErrorMailDetail(Exception ex) {
		new Thread(() -> mailForErrorReportDetail(ex)).start();
	}

	private void submitBidMailToInfluencer(MailDetails mailDetails) {
		try {
			NylasMailRequest mailRequest = new NylasMailRequest();
			Optional<BrandEmailConnection> brandEmailConnection = getBrandEmailConnection(mailDetails.getBrandId().toString());

			String influencerEmail = getInfluencerMail(mailDetails.getInfluencerEmail());
			mailRequest.setAccessToken(getAccessToken(brandEmailConnection));
			mailRequest.setFromAddress(getFromAddress(mailDetails.getBrandName(), mailDetails.getBrandEmail(), brandEmailConnection));
			mailRequest.setToRecipient(Arrays.asList(new NameEmail(getName(mailDetails.getInfluencerHandle(), 2)
					, influencerEmail)));

			List<NameEmail> nameEmail = new LinkedList<NameEmail>();
			if (instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO))
				nameEmail.add(new NameEmail(SMARTFLUENCE, partnershipProperties.getUsername()));

			if (!StringUtils.isEmpty(mailDetails.getMailCc())) {
				List<String> ccList = Arrays.asList(mailDetails.getMailCc().split("\\s*,\\s*"));

				for (String cc : ccList) {
					nameEmail.add(new NameEmail(getName(cc, 1), cc));
				}
				mailRequest.setCcRecipient(nameEmail);
			}

			mailRequest.setSubject(mailDetails.getMailSubject());
			mailRequest.setMailBody(getSubmitBidMailToInfluencer(mailDetails));

			mailHistoryRepo.save(MailHistory.builder()
					.mailHistoryKey(MailHistoryKey.builder().brandId(UUID.fromString(mailDetails.getBrandId()))
							.createdAt(new Date()).build()).brandName(mailDetails.getBrandName())
					.mailBody(mailDetails.getMailDescription()).mailCc(mailDetails.getMailCc())
					.mailFrom(mailRequest.getFromAddress().getEmail()).mailSubject(mailDetails.getMailSubject())
					.mailTemplateId(UUID.fromString(mailDetails.getMailTemplateId())).mailTo(influencerEmail)
					.status(mailDetails.getStatus()).mappingId(UUID.fromString(mailDetails.getMappingId())).build());
			nylasUtil.sendMail(mailRequest);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitPromoteCampaignMailToInfluencers(MailDetails mailDetails) {
		try {
			NylasMailRequest mailRequest = new NylasMailRequest();

			String influencerInviteLink = getInfluencerInviteLink(mailDetails.getInfluencerInviteLink());
			String influencerEmail = getInfluencerMail(mailDetails.getInfluencerEmail());

			mailRequest.setAccessToken(instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO)
					? appAccessToken : demoAccessToken);
			mailRequest.setFromAddress(getPromoteFromAddress());
			mailRequest.setToRecipient(Arrays.asList(new NameEmail(mailDetails.getInfluencerHandle(), influencerEmail)));
			mailRequest.setSubject(mailDetails.getMailSubject().replace(BRANDNAME, mailDetails.getBrandName()));
			mailRequest.setMailBody(getPromoteCampaignMailBody(mailDetails, influencerInviteLink));

			mailHistoryRepo.save(MailHistory.builder()
					.mailHistoryKey(MailHistoryKey.builder().brandId(UUID.fromString(mailDetails.getBrandId()))
							.createdAt(new Date()).build()).brandName(mailDetails.getBrandName())
					.mailBody(mailRequest.getMailBody()).mailFrom(mailRequest.getFromAddress().getEmail())
					.mailSubject(mailRequest.getSubject()).mailTemplateId(UUID.fromString(mailDetails.getMailTemplateId()))
					.mailTo(influencerEmail).status(mailDetails.getStatus()).mappingId(UUID.fromString(mailDetails.getMappingId())).build());
			nylasUtil.sendMail(mailRequest);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitProposalMailToInfluencers(MailDetails mailDetails) {
		try {
			NylasMailRequest mailRequest = new NylasMailRequest();

			//String influencerInviteLink = mailDetails.getInfluencerInviteLink();

			Optional<BrandEmailConnection> brandEmailConnection = getBrandEmailConnection(mailDetails.getBrandId().toString());

			String influencerEmail = getInfluencerMail(mailDetails.getInfluencerEmail());
			mailRequest.setAccessToken(getAccessToken(brandEmailConnection));
			mailRequest.setFromAddress(getFromAddress(mailDetails.getBrandName(), mailDetails.getBrandEmail(), brandEmailConnection));
			mailRequest.setToRecipient(Arrays.asList(new NameEmail(getName(mailDetails.getInfluencerHandle(), 2)
					, influencerEmail)));

			List<NameEmail> nameEmail = new LinkedList<NameEmail>();
			if (instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO))
				nameEmail.add(new NameEmail(SMARTFLUENCE, partnershipProperties.getUsername()));

			if (!StringUtils.isEmpty(mailDetails.getMailCc())) {
				List<String> ccList = Arrays.asList(mailDetails.getMailCc().split("\\s*,\\s*"));

				for (String cc : ccList) {
					nameEmail.add(new NameEmail(getName(cc, 1), cc));
				}
				mailRequest.setCcRecipient(nameEmail);
			}

			mailRequest.setSubject(mailDetails.getMailSubject());
			mailRequest.setMailBody(getSubmitProposalMailToInfluencer(mailDetails));


			mailHistoryRepo.save(MailHistory.builder()
					.mailHistoryKey(MailHistoryKey.builder().brandId(UUID.fromString(mailDetails.getBrandId()))
							.createdAt(new Date()).build()).brandName(mailDetails.getBrandName())
					.mailBody(mailRequest.getMailBody()).mailFrom(mailRequest.getFromAddress().getEmail())
					.mailSubject(mailRequest.getSubject()).mailTemplateId(UUID.fromString(mailDetails.getMailTemplateId()))
					.mailTo(influencerEmail).status(mailDetails.getStatus()).mappingId(UUID.fromString(mailDetails.getMailId())).build());
			nylasUtil.sendMail(mailRequest);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitPromoteCampaignInfluencerStatusMail(MailDetails mailDetails) {
		try {
			NylasMailRequest mailRequest = new NylasMailRequest();

			String influencerEmail = getInfluencerMail(mailDetails.getInfluencerEmail());
			mailRequest.setAccessToken(instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO)
					? appAccessToken : demoAccessToken);
			mailRequest.setFromAddress(getPromoteFromAddress());
			mailRequest.setToRecipient(Arrays.asList(new NameEmail(mailDetails.getInfluencerHandle(), influencerEmail)));

			if (mailDetails.getIsApprovalMail())
				mailRequest.setCcRecipient(Arrays.asList(new NameEmail(getName(mailDetails.getBrandEmail(), 1), mailDetails.getBrandEmail())));

			mailRequest.setSubject(mailDetails.getMailSubject().replace(BRANDNAME, mailDetails.getBrandName()));
			mailRequest.setMailBody(getPromoteCampaignInfluencerStatusMailBody(mailDetails));

			mailHistoryRepo.save(MailHistory.builder()
					.mailHistoryKey(MailHistoryKey.builder().brandId(UUID.fromString(mailDetails.getBrandId()))
							.createdAt(new Date()).build()).brandName(mailDetails.getBrandName())
					.mailBody(mailRequest.getMailBody()).mailFrom(mailRequest.getFromAddress().getEmail())
					.mailSubject(mailRequest.getSubject()).mailTemplateId(UUID.fromString(mailDetails.getMailTemplateId()))
					.mailTo(influencerEmail).mailCc(mailDetails.getIsApprovalMail() ? mailDetails.getBrandEmail() : "")
					.status(mailDetails.getStatus()).mappingId(UUID.fromString(mailDetails.getMappingId())).build());
			nylasUtil.sendMail(mailRequest);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitPromoteTargetingDetailsNotificationMail(MailDetails mailDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplNoReply.createMimeMessage();

			mimeMessage.setFrom(noReplyProperties.getUsername());
			mimeMessage.setSubject(mailDetails.getMailSubject());
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(mailDetails.getBrandEmail()));
			mimeMessage.setContent(getPromoteTargetingDetailsNotificationMailBody(mailDetails), TYPE_HTML);

			mailHistoryRepo.save(MailHistory.builder()
					.mailHistoryKey(MailHistoryKey.builder().brandId(UUID.fromString(mailDetails.getBrandId()))
							.createdAt(new Date()).build()).brandName(mailDetails.getBrandName())
					.mailBody(mailDetails.getMailDescription()).mailFrom(noReplyProperties.getUsername())
					.mailSubject(mailDetails.getMailSubject()).mailTemplateId(UUID.fromString(mailDetails.getMailTemplateId()))
					.mailTo(mailDetails.getBrandEmail()).status(mailDetails.getStatus())
					.mappingId(UUID.fromString(mailDetails.getMappingId())).build());
			javaMailSenderImplNoReply.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitBidMailToAdmin(BrandBidDetails brandBidDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplNoReply.createMimeMessage();

			setAdminToAddress(mimeMessage);
			mimeMessage.setSubject("[URGENT] Smartfluence: Bid Submitted by " + brandBidDetails.getBrandName());
			mimeMessage.setFrom(noReplyProperties.getUsername());
			mimeMessage.setContent(getSubmitBidMailToAdmin(brandBidDetails), TYPE_HTML);
			javaMailSenderImplNoReply.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitBidConfirmationMailToBrand(BrandBidDetails brandBidDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(brandBidDetails.getBrandEmail()));
			mimeMessage.setSubject("Smartfluence: Bid Confirmation");
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setContent(getSubmitBidConfirmationMailTobrand(brandBidDetails), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitProposalMailToAdmin(SubmitBidModel submitBidModel,BrandDetails brandDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplNoReply.createMimeMessage();

			setAdminToAddress(mimeMessage);
			mimeMessage.setSubject("[URGENT] Smartfluence: Bid Submitted by " + brandDetails.getBrandName());
			mimeMessage.setFrom(noReplyProperties.getUsername());
			mimeMessage.setContent(getSubmitProposalMailToAdmin(submitBidModel,brandDetails), TYPE_HTML);
			javaMailSenderImplNoReply.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitProposalConfirmationMailToBrand(SubmitBidModel submitBidModel,BrandDetails brandDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(brandDetails.getEmail()));
			mimeMessage.setSubject("Smartfluence: Bid Confirmation");
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setContent(getSubmitProposalConfirmationMailTobrand(submitBidModel,brandDetails), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}


	private void submitBidMailToInfluencerForShoutout(BrandBidDetails brandBidDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO,
					new InternetAddress(getInfluencerMail(brandBidDetails.getInfluencerMail())));
			mimeMessage.addRecipient(RecipientType.CC, new InternetAddress(brandBidDetails.getBrandEmail()));
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setSubject("[Partnership Opportunity] Offer from " + brandBidDetails.getBrandName());
			mimeMessage.setContent(getSubmitBidMailToInfluencerForShoutout(brandBidDetails), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitBidMailToInfluencerForAffiliate(BrandBidDetails brandBidDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO,
					new InternetAddress(getInfluencerMail(brandBidDetails.getInfluencerMail())));
			mimeMessage.addRecipient(RecipientType.CC, new InternetAddress(brandBidDetails.getBrandEmail()));
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setSubject("[Partnership Opportunity] Offer from " + brandBidDetails.getBrandName());
			mimeMessage.setContent(getSubmitBidMailToInfluencerForAffiliate(brandBidDetails), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitBidMailToInfluencerForCash(BrandBidDetails brandBidDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO,
					new InternetAddress(getInfluencerMail(brandBidDetails.getInfluencerMail())));
			mimeMessage.addRecipient(RecipientType.CC, new InternetAddress(brandBidDetails.getBrandEmail()));
			mimeMessage.setSubject("[Partnership Opportunity] Offer from " + brandBidDetails.getBrandName());
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setContent(getSubmitBidMailToInfluencerForCash(brandBidDetails), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitBidMailToInfluencerForProduct(BrandBidDetails brandBidDetails) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO,
					new InternetAddress(getInfluencerMail(brandBidDetails.getInfluencerMail())));
			mimeMessage.setSubject("[Partnership Opportunity] Offer from " + brandBidDetails.getBrandName());
			mimeMessage.addRecipient(RecipientType.CC, new InternetAddress(brandBidDetails.getBrandEmail()));
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setContent(getSubmitBidMailToInfluencerForProduct(brandBidDetails), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitMailToBrandForInfluencerReport(UUID influencerSFId, InfluencerDataQueue influencerDataQueue) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplNoReply.createMimeMessage();
			MailTemplateMaster mailTemplateMaster = getMailTemplateMaster("InfluencerReportQueueMail").get();

			mimeMessage.setFrom(noReplyProperties.getUsername());
			mimeMessage.setSubject(mailTemplateMaster.getMailSubject().replaceAll(INFLUENCERNAME, influencerDataQueue.getInfluencerHandle()));
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(influencerDataQueue.getBrandEmail()));
			mimeMessage.setContent(getBrandForInfluencerReportMailBody(influencerSFId, influencerDataQueue, mailTemplateMaster), TYPE_HTML);

			mailHistoryRepo.save(MailHistory.builder()
					.mailHistoryKey(MailHistoryKey.builder().brandId(UUID.fromString(influencerDataQueue.getKey().getBrandId()))
							.createdAt(new Date()).build()).brandName(influencerDataQueue.getBrandName())
					.mailBody(mimeMessage.getContent().toString()).mailFrom(noReplyProperties.getUsername())
					.mailSubject(mailTemplateMaster.getMailSubject()).mailTemplateId(UUID.fromString(mailTemplateMaster.getId()))
					.mailTo(influencerDataQueue.getBrandEmail()).status(128).mappingId(UUID.fromString(influencerDataQueue.getKey().getBrandId())).build());
			javaMailSenderImplNoReply.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void mailForErrorReport(RuntimeException ex) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(INFLUENCER_MAIL_ADDRESS_TEST));
			mimeMessage.addRecipient(RecipientType.CC, new InternetAddress(ADMIN_MAIL_ADDRESS_TEST));
			mimeMessage.setSubject("ATTENTION! - ERROR");
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setContent(getErrorMailReport(ex), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void mailForErrorReportDetail(Exception ex) {
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(ERROR_MAIL_ADDRESS));
			mimeMessage.setSubject("ATTENTION! - ERROR");
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setContent(getErrorMailReportDetails(ex), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}


	private String getSubmitBidMailToInfluencer(MailDetails mailDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITBIDMAILTEST).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, mailDetails.getBrandName())
				.setValue(MAIL_BODY, mailDetails.getMailDescription().replaceAll("(\r\n|\n\r|\r|\n)", "<br>"))
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitProposalMailToInfluencer(MailDetails mailDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITPROPOSALMAILTEST).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, mailDetails.getBrandName())
				.setValue(MAIL_BODY, mailDetails.getMailDescription()
						.replace(PROPOSAL_LINK, "<a style=color: #2f008d; target=_blank; href="+mailDetails.getInfluencerInviteLink()+" >click here</a>").replaceAll("(\r\n|\n\r|\r|\n)", "<br>"))
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getPromoteCampaignMailBody(MailDetails mailDetails, String influencerInviteLink) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + PROMOTECAMPAIGNMAIL).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(MAIL_BODY, mailDetails.getMailDescription().replaceAll(INFLUENCERNAME, mailDetails.getInfluencerHandle())
						.replace(CAMPAIGN_DESCRIPTION, mailDetails.getCampaignDescription())
						.replace(PAYMENT, getPaymentString(mailDetails)).replaceAll(BRANDNAME, mailDetails.getBrandName())
						.replace(PROMOTE_LINK, influencerInviteLink).replaceAll("(\r\n|\n\r|\r|\n)", "<br>"))
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getPromoteCampaignInfluencerStatusMailBody(MailDetails mailDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + PROMOTECAMPAIGNMAIL).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(MAIL_BODY, mailDetails.getMailDescription().replaceAll(INFLUENCERNAME, mailDetails.getInfluencerHandle())
						.replaceAll(BRANDNAME, mailDetails.getBrandName()).replaceAll("(\r\n|\n\r|\r|\n)", "<br>"))
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getPromoteTargetingDetailsNotificationMailBody(MailDetails mailDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + PROMOTECAMPAIGNMAIL).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(MAIL_BODY, mailDetails.getMailDescription().replaceAll(CAMPAIGN_NAME, mailDetails.getCampaignName())
						.replaceAll(BRANDNAME, mailDetails.getBrandName()).replaceAll("(\r\n|\n\r|\r|\n)", "<br>"))
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitBidMailToAdmin(BrandBidDetails brandBidDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITBIDMAILTOADMIN).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandBidDetails.getBrandName())
				.setValue(BRAND_EMAIL, brandBidDetails.getBrandEmail())
				.setValue(BID_AMOUNT, String.format("%.2f", brandBidDetails.getBidAmount()))
				.setValue(POST_DURATION, postDuration(brandBidDetails.getPostDuration()))
				.setValue(POST_TYPE, brandBidDetails.getPostType())
				.setValue(PAYMENT_TYPE, brandBidDetails.getPaymentType())
				.setValue(INFLUENCER_NAME, brandBidDetails.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitBidConfirmationMailTobrand(BrandBidDetails brandBidDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITBIDCONFIRMATIONMAILTOBRAND).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandBidDetails.getBrandName())
				.setValue(INFLUENCER_NAME, brandBidDetails.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitProposalMailToAdmin(SubmitBidModel submitBidModel,BrandDetails brandDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITPROPOSALMAILTOADMIN).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandDetails.getBrandName())
				.setValue(BRAND_EMAIL, brandDetails.getEmail())
//				.setValue(BID_AMOUNT, String.format("%.2f", brandBidDetails.getBidAmount()))
//				.setValue(POST_DURATION, postDuration(brandBidDetails.getPostDuration()))
//				.setValue(POST_TYPE, brandBidDetails.getPostType())
//				.setValue(PAYMENT_TYPE, brandBidDetails.getPaymentType())
				.setValue(INFLUENCER_NAME, submitBidModel.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitProposalConfirmationMailTobrand(SubmitBidModel submitBidModel,BrandDetails brandDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITPROPOSALCONFIRMATIONMAILTOBRAND).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandDetails.getBrandName())
				.setValue(INFLUENCER_NAME, submitBidModel.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitBidMailToInfluencerForShoutout(BrandBidDetails brandBidDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITBIDMAILTOINFLUENCERFORSHOUOUT).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandBidDetails.getBrandName())
				.setValue(BID_AMOUNT, String.format("%.2f", brandBidDetails.getBidAmount()))
				.setValue(POST_DURATION, postDuration(brandBidDetails.getPostDuration()))
				.setValue(POST_TYPE, brandBidDetails.getPostType())
				.setValue(INFLUENCER_NAME, brandBidDetails.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitBidMailToInfluencerForAffiliate(BrandBidDetails brandBidDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITBIDMAILTOINFLUENCERFORAFFILIATE).getInputStream());
		String bidAmount = brandBidDetails.getAffiliateType() != null && brandBidDetails.getAffiliateType().equals("%")
				? String.format("%.2f", brandBidDetails.getBidAmount()) + "%"
				: "$" + String.format("%.2f", brandBidDetails.getBidAmount());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandBidDetails.getBrandName()).setValue(BID_AMOUNT, bidAmount)
				.setValue(POST_DURATION, postDuration(brandBidDetails.getPostDuration()))
				.setValue(POST_TYPE, brandBidDetails.getPostType())
				.setValue(INFLUENCER_NAME, brandBidDetails.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitBidMailToInfluencerForCash(BrandBidDetails brandBidDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITBIDMAILTOINFLUENCERFORCASH).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandBidDetails.getBrandName())
				.setValue(BID_AMOUNT, String.format("%.2f", brandBidDetails.getBidAmount()))
				.setValue(POST_DURATION, postDuration(brandBidDetails.getPostDuration()))
				.setValue(POST_TYPE, brandBidDetails.getPostType())
				.setValue(INFLUENCER_NAME, brandBidDetails.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getSubmitBidMailToInfluencerForProduct(BrandBidDetails brandBidDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITBIDMAILTOINFLUENCERFORPRODUCT).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandBidDetails.getBrandName())
				.setValue(BID_AMOUNT, String.format("%.2f", brandBidDetails.getBidAmount()))
				.setValue(POST_DURATION, postDuration(brandBidDetails.getPostDuration()))
				.setValue(POST_TYPE, brandBidDetails.getPostType())
				.setValue(INFLUENCER_NAME, brandBidDetails.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getBrandForInfluencerReportMailBody(UUID influencerSFId, InfluencerDataQueue influencerDataQueue, MailTemplateMaster mailTemplateMaster) {
		return new MessageBuilder(mailTemplateMaster.getMailBody().replaceAll(INFLUENCERNAME, influencerDataQueue.getInfluencerHandle())
				.replaceAll(BRANDNAME, influencerDataQueue.getBrandName()).replaceAll("<apostrophe>", "'")
				.replace(INFLUENCER_REPORT_LINK, getViewInfluencerLink(influencerSFId, influencerDataQueue))
				.replace("<year>", getDateTimeValues(1).toString())).getMessage();
	}

	private String getErrorMailReport(RuntimeException ex) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + ERRORREPORTMAIL).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(MESSAGE, convertStackTraceToString(ex))
				.setValue(NAME, setErrorPlatform())
				.setValue(TIME, new Date().getTime())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getErrorMailReportDetails(Exception ex) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + ERRORREPORTMAIL).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(MESSAGE,convertStackTraceToString(ex))
				.setValue(NAME, setErrorPlatform())
				.setValue(TIME, new Date().getTime())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private static String convertStackTraceToString(Exception ex)
	{
			StackTraceElement[] stackTrace = ex.getStackTrace();
			String traceInfo = "";
			String stackDetails="";
			for (StackTraceElement trace : stackTrace) {
				traceInfo = trace.getClassName() + "."
						+ trace.getMethodName() + ":" + trace.getLineNumber()
						+ "(" + trace.getFileName() + ")";
				stackDetails = stackDetails + trace.toString() + "\n\t\t";
			}
			return "Error: "+ ex.toString()+ " <br> Message: "+ ex.getMessage()+  " <br> Trace: "+traceInfo+" <br> Detail Trace : "+ stackDetails;
		}



	private String getPaymentString(MailDetails mailDetails) {
		return mailDetails.getProduct().equals("PRODUCT_ONLY")
				? "" : " They’re currently interested in paying $" + mailDetails.getPayment() + " for an Instagram " + mailDetails.getPostType() + ".";
	}

	private String getViewInfluencerLink(UUID influencerSFId, InfluencerDataQueue influencerDataQueue) {
		String platform = influencerDataQueue.getPlatform().name().toLowerCase();
		String url = StringUtils.isEmpty(influencerDataQueue.getSearchId())
				? "/brand/view-influencer/" + influencerSFId + "?isValid=true&platform=" + platform
						: "/brand/view-influencer/" + influencerSFId + "?isValid=true&platform=" + platform + "&id=" + influencerDataQueue.getSearchId();
		if (instanceConfigBean.getHostname().toLowerCase().trim().equals(LOCALHOST))
			return "http://" + instanceConfigBean.getHostname().toLowerCase().trim() + url;
		else
			return "https://" + instanceConfigBean.getHostname().toLowerCase().trim() + url;
	}

	private String getInfluencerInviteLink(String influencerInviteLink) {
		String url = ":9000/accept-offer?inviteLink=" + influencerInviteLink;
		if (instanceConfigBean.getHostname().toLowerCase().trim().equals(LOCALHOST))
			return "http://" + instanceConfigBean.getHostname().toLowerCase().trim() + url;
		else
			return "https://" + instanceConfigBean.getHostname().toLowerCase().trim() + url;
	}

	private String setErrorPlatform() {
		return instanceConfigBean.getHostname().toLowerCase();
	}

	private void setAdminToAddress(MimeMessage mimeMessage) throws MessagingException, AddressException {
		if (instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO))
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(ADMIN_MAIL_ADDRESS));
		else
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(ADMIN_MAIL_ADDRESS_TEST));
	}

	private String postDuration(String postDuration) {
		switch (postDuration) {
		case "2H":
			postDuration = "2 hours";
			break;
		case "6H":
			postDuration = "6 hour";
			break;
		case "12H":
			postDuration = "12 hour";
			break;
		case "24H":
			postDuration = "24 hour";
			break;
		case "1W":
			postDuration = "1 week";
			break;
		case "P":
			postDuration = "permanent";
			break;
		default:
			break;
		}
		return postDuration;
	}

	private String readFile(InputStream file) {
		StringBuilder builder = new StringBuilder();
		try (BufferedReader r = new BufferedReader(new InputStreamReader(file))) {
			r.lines().forEach(builder::append);
		} catch (Exception e) {
			logger.error(e);
		}
		return builder.toString();
	}

	private Object getDateTimeValues(int flag) {

		if (flag == 0)
			return LocalDateTime.now();

		else if (flag == 1)
			return LocalDateTime.now().getYear();

		else if (flag == 2)
			return LocalDateTime.now().getDayOfMonth();

		else if (flag == 3)
			return LocalDateTime.now().getMonthValue();

		else if (flag == 4)
			return LocalDateTime.now().getDayOfWeek();

		else if (flag == 5)
			return LocalDateTime.now().getMonth();

		return "";
	}

	private String getInfluencerMail(String influencerEmail) {
		if (instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO))
			return influencerEmail;
		else
			return INFLUENCER_MAIL_ADDRESS_TEST;
	}

	private String getName(String name, int flag) {

		if (flag == 1) {
			String emailName [] = name.split("@");
			name = emailName[0];
		}
		if (flag == 2) {
			name = name.substring(1, name.length());
		}

		return name;
	}

	private NameEmail getFromAddress(String brandName, String brandEmail, Optional<BrandEmailConnection> brandEmailConnection) {
		if (instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO) && !brandEmailConnection.isPresent())
			return new NameEmail(SMARTFLUENCE, partnershipProperties.getUsername());

		else if (!instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO) && !brandEmailConnection.isPresent())
			return new NameEmail(getName(INFLUENCER_FROM_MAIL_ADDRESS_TEST, 1), INFLUENCER_FROM_MAIL_ADDRESS_TEST);

		else
			return new NameEmail(brandName, brandEmail);
	}

	private NameEmail getPromoteFromAddress() {
		if (instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO))
			return new NameEmail(SMARTFLUENCE, partnershipProperties.getUsername());

		else
			return new NameEmail(getName(INFLUENCER_FROM_MAIL_ADDRESS_TEST, 1) , INFLUENCER_FROM_MAIL_ADDRESS_TEST);
	}

	private String getAccessToken(Optional<BrandEmailConnection> brandEmailConnection) {
		String accessToken = instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO) ? appAccessToken : demoAccessToken;

		if (brandEmailConnection.isPresent())
			accessToken = brandEmailConnection.get().getAccessToken();

		return accessToken;
	}

	public void sendInviteMailToInfluencer(BrandInfluObj brandBidDetails){
		try {
			MimeMessage mimeMessage = javaMailSenderImplPartner.createMimeMessage();
			mimeMessage.setRecipient(MimeMessage.RecipientType.TO,
					new InternetAddress(getInfluencerMail(brandBidDetails.getInfluencerMail())));
			mimeMessage.addRecipient(MimeMessage.RecipientType.CC, new InternetAddress(brandBidDetails.getBrandEmail()));
			mimeMessage.setSubject("[Partnership Opportunity] Offer from " + brandBidDetails.getBrandName());
			mimeMessage.setFrom(partnershipProperties.getUsername());
			mimeMessage.setContent(getInviteMailToInfluencerForCash(brandBidDetails), TYPE_HTML);
			javaMailSenderImplPartner.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private String getInviteMailToInfluencerForCash(BrandInfluObj brandBidDetails) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + SUBMITBIDMAILTOINFLUENCERFORCASH).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, brandBidDetails.getBrandName())
				.setValue(BID_AMOUNT, String.format("%.2f", 200.224))
				.setValue(POST_DURATION, postDuration(brandBidDetails.getPostDuration()))
				.setValue(POST_TYPE, brandBidDetails.getPostType())
				.setValue(INFLUENCER_NAME, brandBidDetails.getInfluencerHandle())
				.setValue(YEAR, getDateTimeValues(1))
				.setValue("URL", getInfluencerURL(brandBidDetails));
		return messageBuilder.getMessage();
	}

	public String getInfluencerURL(BrandInfluObj brandInfluObj){
		String jwtToken = tokenUtil.getJWTToken(brandInfluObj.getCampaignId(), brandInfluObj.getInfluencerId());
		String url="https://"+eurekaInstanceConfigBean.getHostname()+"/influencer/" +
				"pub/campaign_details?token="+jwtToken;
		return url;
	}

	class MessageBuilder {
		private StringBuilder builder;

		public MessageBuilder() {
			builder = new StringBuilder();
		}

		public MessageBuilder(String message) {
			builder = new StringBuilder();
			if (message != null)
				builder.append(message);
		}

		public MessageBuilder append(CharSequence charSequence) {
			builder.append(charSequence);
			return this;
		}

		public MessageBuilder setValue(String key, Object value) {
			int index = builder.indexOf(':' + key + ':');
			while (index != -1) {
				builder.replace(builder.indexOf(':' + key + ':'), builder.indexOf(':' + key + ':') + key.length() + 2,
						value == null ? "N/A" : String.valueOf(value));
				index = builder.indexOf(':' + key + ':');
			}
			return this;
		}

		public String getMessage() {
			return builder.toString();
		}
	}
}