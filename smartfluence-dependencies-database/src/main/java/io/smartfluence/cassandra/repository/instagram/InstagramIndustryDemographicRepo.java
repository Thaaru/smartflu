package io.smartfluence.cassandra.repository.instagram;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.instagram.InstagramIndustryDemographic;
import io.smartfluence.cassandra.entities.instagram.primary.InstagramIndustryKey;

@Repository
public interface InstagramIndustryDemographicRepo extends CassandraRepository<InstagramIndustryDemographic, InstagramIndustryKey> {

	List<InstagramIndustryDemographic> findAllByKeyInfluencerHandle(String influencerHandle);

	List<InstagramIndustryDemographic> findAllByKeyInfluencerHandleIn(List<String> influencerHandles);
}