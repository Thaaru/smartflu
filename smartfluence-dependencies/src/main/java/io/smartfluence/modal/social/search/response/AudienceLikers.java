package io.smartfluence.modal.social.search.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudienceLikers implements Serializable {

	private static final long serialVersionUID = -5297919440879340538L;

	@JsonAlias("data")
	private AudienceData audienceData;
}