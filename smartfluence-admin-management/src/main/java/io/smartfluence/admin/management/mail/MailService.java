package io.smartfluence.admin.management.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.Optional;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.smartfluence.beans.SmartfluenceConfig;
import io.smartfluence.constants.UserType;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.MailTemplateMasterRepo;

@Component
public class MailService {

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@Autowired
	private SmartfluenceConfig smartfluenceConfig;

	@Autowired
	private MailProperties mailProperties;

	@Autowired
	private MailTemplateMasterRepo mailTemplateMasterRepo;

	private final Log logger = LogFactory.getLog(getClass());

	private static final String YEAR = "year";

	private static final String TEXT_HTML = "text/html";

	private static final String BRAND_NAME = "brandName";

	private static final String SMARTFLUENCE_DATA = "smartfluence-data";

	private static final String MAIL_TEMPLATE = "mail-template";

	private static final String INVITE_MAIL = "inviteMail.html";

	private static final String DECLINED_MAIL = "declinedMail.html";

	private static final String APPROVEDMAILFORTRIAL = "approvedMailForTrial.html";

	private static final String DB_UPDATE_COMPLETED_MAIL = "dbUpdatedCompletedMail.html";

	private static final String APPROVEDMAILFORSMARTFLUENCE = "approvedMailForSmartfluence.html";

	private Optional<MailTemplateMaster> getMailTemplateMaster(String templateName) {
		return mailTemplateMasterRepo.findByTemplateName(templateName);
	}

	public void sendApprovedMailForTrail(UserAccounts userAccounts, Boolean newUser) {
		String homeLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).port(-1).build().encode().toUriString();
		new Thread(() -> approvedMailForTrail(userAccounts, homeLink, newUser)).start();
	}

	public void sendApprovedMailForSmartfluence(UserAccounts userAccounts, Boolean newUser) {
		String homeLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).port(-1).build().encode().toUriString();
		new Thread(() -> approvedMailForSmartfluence(userAccounts, homeLink, newUser)).start();
	}

	public void sendApprovedMailForSmartfluenceFree(UserAccounts userAccounts, Boolean newUser) {
		String homeLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).port(-1).build().encode().toUriString();
		new Thread(() -> approvedMailForSmartfluenceFree(userAccounts, homeLink, newUser)).start();
	}

	public void sendDeclinedMail(UserAccounts userAccounts) {
		new Thread(() -> declinedMail(userAccounts)).start();
	}

	public void sendInviteMail(UserAccounts userAccounts) {
		String inviteLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.port(smartfluenceConfig.getInviteUrl().getPort()).path("login")
				.host(smartfluenceConfig.getInviteUrl().getHost()).queryParam("inviteCode", userAccounts.getInviteId())
				.build().encode().toUriString();
		new Thread(() -> inviteMail(userAccounts, inviteLink)).start();
	}

	public void sendUpdateCompletedMail(File tempCSVFile) {
		new Thread(() -> updateCompletedMail(tempCSVFile)).start();
	}

	private void approvedMailForTrail(UserAccounts userAccounts, String homeLink, Boolean newUser) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			if (newUser) {
				mimeMessage.setSubject("Welcome to Smartfluence!");
			} else {
				mimeMessage.setSubject("Smartfluence Platform");
			}
			mimeMessage.setContent(getApprovedMailForTrail(userAccounts, homeLink), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void approvedMailForSmartfluence(UserAccounts userAccounts, String homeLink, Boolean newUser) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			if (newUser) {
				mimeMessage.setSubject("Welcome to Smartfluence!");
			} else {
				mimeMessage.setSubject("Smartfluence Platform");
			}
			mimeMessage.setContent(getApprovedMailForSmartfluence(userAccounts, homeLink), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void approvedMailForSmartfluenceFree(UserAccounts userAccounts, String homeLink, Boolean newUser) {
		try {
			MailTemplateMaster mailTemplateMaster = getMailTemplateMaster("ApprovedMailSmartfluenceFree").get();
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			if (newUser)
				mimeMessage.setSubject(mailTemplateMaster.getMailSubject().substring(0, mailTemplateMaster.getMailSubject().indexOf(",")));
			else
				mimeMessage.setSubject(mailTemplateMaster.getMailSubject().substring(mailTemplateMaster.getMailSubject().indexOf(",") + 1,
						mailTemplateMaster.getMailSubject().length()));

			mimeMessage.setContent(getApprovedMailForSmartfluenceFree(userAccounts, homeLink, mailTemplateMaster), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void declinedMail(UserAccounts userAccounts) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			mimeMessage.setSubject("Smartfluence: Issue with Registration");
			mimeMessage.setContent(getDeclinedMail(userAccounts), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void inviteMail(UserAccounts userAccounts, String inviteLink) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			mimeMessage.setSubject("Smartfluence: Facebook Login");
			mimeMessage.setContent(getInviteMail(userAccounts, inviteLink), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void updateCompletedMail(File tempCSVFile) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setTo(smartfluenceConfig.getDbUpdatedCompletedMail());
			helper.setFrom(mailProperties.getUsername());
			helper.setSubject("Smartfluence: DataBase Updated Completed");
			helper.setText(getupdateCompletedMail(), true);
			helper.addAttachment("SF_FAILED_INFLUENCERS.csv", tempCSVFile);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private String getApprovedMailForTrail(UserAccounts userAccounts, String homeLink) throws IOException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + APPROVEDMAILFORTRIAL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue("link", homeLink)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getApprovedMailForSmartfluence(UserAccounts userAccounts, String homeLink) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + APPROVEDMAILFORSMARTFLUENCE).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue("link", homeLink)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getApprovedMailForSmartfluenceFree(UserAccounts userAccounts, String homeLink,
			MailTemplateMaster mailTemplateMaster) throws IOException {
		String contentText = mailTemplateMaster.getMailBody();
		contentText = contentText.replace("<brandName>", getName(userAccounts))
						.replace("<link>", homeLink).replaceAll("<apostrophe>", "'")
						.replace("<year>", getDateTimeValues(1).toString());
		return new MessageBuilder(contentText).getMessage();
	}

	public static void main(String[] args) {
		String cj = "Kirupa<o>Shankar";
		cj = cj.replace("<o>", " ");
		System.out.println(cj);
	}

	private String getDeclinedMail(UserAccounts userAccounts) throws IOException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + DECLINED_MAIL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getInviteMail(UserAccounts userAccounts, String inviteLink) throws IOException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + INVITE_MAIL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue("inviteLink", inviteLink)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getupdateCompletedMail() throws IOException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + DB_UPDATE_COMPLETED_MAIL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getName(UserAccounts userAccounts) {
		return userAccounts.getUserType().equals(UserType.BRAND) ? userAccounts.getBrandName()
				: userAccounts.getFirstName() + ' ' + userAccounts.getLastName();
	}

	private String readFile(InputStream file) {
		StringBuilder builder = new StringBuilder();
		try (BufferedReader r = new BufferedReader(new InputStreamReader(file))) {
			r.lines().forEach(builder::append);
		} catch (Exception e) {
			logger.error(e);
		}
		return builder.toString();
	}

	private Object getDateTimeValues(int flag) {

		if (flag == 0)
			return LocalDateTime.now();

		else if (flag == 1)
			return LocalDateTime.now().getYear();

		else if (flag == 2)
			return LocalDateTime.now().getDayOfMonth();

		else if (flag == 3)
			return LocalDateTime.now().getMonthValue();

		else if (flag == 4)
			return LocalDateTime.now().getDayOfWeek();

		else if (flag == 5)
			return LocalDateTime.now().getMonth();

		return "";
	}

	class MessageBuilder {
		private StringBuilder builder;

		public MessageBuilder() {
			builder = new StringBuilder();
		}

		public MessageBuilder(String message) {
			builder = new StringBuilder();
			if (message != null)
				builder.append(message);
		}

		public MessageBuilder append(CharSequence charSequence) {
			builder.append(charSequence);
			return this;
		}

		public MessageBuilder setValue(String key, Object value) {
			int index = builder.indexOf(':' + key + ':');
			while (index != -1) {
				builder.replace(builder.indexOf(':' + key + ':'), builder.indexOf(':' + key + ':') + key.length() + 2,
						value == null ? "N/A" : String.valueOf(value));
				index = builder.indexOf(':' + key + ':');
			}
			return this;
		}

		public String getMessage() {
			return builder.toString();
		}
	}
}