import axios from 'axios';

import { getResponseData } from './get_response_data';


export class serviceCall {

  async postCall({ url, body, headers }) {
    var res = await this.httpCall({
      serviceCallCb: async () =>
        await axios.post(
          url,
          body,
          headers),
    });
    var response = getResponseData(res);
    return response;
    return res;
  }

  async putCall({ url, body }) {
    var res = await this.httpCall({
      serviceCallCb: async () =>
        await axios.put(
          url,
          body,
          this.getAxiosObject()
        )
    });
    var response = getResponseData(res);
    return response;
    return res;
  }

  async getCall({ url, body }) {
    console.log('getCall');
    var res = await this.httpCall({
      serviceCallCb: async () =>
        !body
          ? await axios.get(url, this.getAxiosObject())
          : await axios.get(url, body, this.getAxiosObject()),
    });

    var response = getResponseData(res);
    return response;
  }

  async deleteCall({ url, body }) {
    var res = await this.httpCall({
      serviceCallCb: async () =>
        !body
          ? await axios.delete(url, this.getAxiosObject())
          : await axios.delete(url, body, this.getAxiosObject()),
    });
    return res;
  }

  async httpCall({ serviceCallCb }) {
    console.log('httpCall');
    const isNetConnected = true;
    try {
      if (isNetConnected) {
        var res = await serviceCallCb();
        if (res.status == 200) {
          console.log(res.data);
          return res.data
        } else {
          return {
            code: 604,
            data: {
              value: null
            },
            error: ""
          };
        }
      } else {
        return {
          code: 604,
          data: {
            value: null
          },
          error: ""
        };
      }
    } catch (e) {
      if (e.message == "Network Error") {
        return {
          code: 603,
          data: {
            value: null
          },
          error: e.message
        };
      }
      else if (e.response.status == 401) {
        return {
          code: 604,
          data: {
            value: null
          },
          error: e.message
        };
      } else if (e.response.status == 417) {
        return {
          code: 604,
          data: {
            value: null
          },
          error: e.message
        };
      }
      return {
        code: 604,
        data: {
          value: null
        },
        error: e.message
      };
    }
  }

  getHeader() {
    return {
      "Content-type": "application/json",
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*",

    };
  }



  getAxiosObject() {
    return {
      headers: this.getHeader(),
      // withCredentials: true,
    };
  }




}