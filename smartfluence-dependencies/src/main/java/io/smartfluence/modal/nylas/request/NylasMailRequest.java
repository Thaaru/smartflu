package io.smartfluence.modal.nylas.request;

import java.util.List;

import com.nylas.NameEmail;

import lombok.Data;

@Data
public class NylasMailRequest {

	private String subject;

	private String mailBody;

	private NameEmail fromAddress;

	private String accessToken;

	private List<NameEmail> toRecipient;

	private List<NameEmail> replyRecipient;

	private List<NameEmail> ccRecipient;

	private List<NameEmail> bccRecipient;

	private String fileName;

	private byte[] file;
}