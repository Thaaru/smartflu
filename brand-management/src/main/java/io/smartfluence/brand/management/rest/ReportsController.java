package io.smartfluence.brand.management.rest;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.brand.management.service.ReportsService;
import io.smartfluence.modal.validation.brand.explore.SearchInfluencerRequestModel;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class ReportsController {

	@Autowired
	private ReportsService reportsService;

	@GetMapping("reports/audience-data")
	public ResponseEntity<Object> influencerAudienceDataReport(@Validated @ModelAttribute SearchInfluencerRequestModel searchInfluencerRequestModel) {
		log.info("AD Reports requested for {}", searchInfluencerRequestModel.getInfluencerHandle());
		return reportsService.influencerAudienceDataReport(searchInfluencerRequestModel);
	}

	@GetMapping("reports/{influencerHandle}/audience-data/{reportId}")
	public ResponseEntity<byte[]> downloadReport(@PathVariable String influencerHandle, @PathVariable String reportId) {
		log.info("AD Report {} download requested  for {}", reportId, influencerHandle);
		return reportsService.downloadReport(influencerHandle, reportId);
	}

	@GetMapping("view-campaign/{campaignId}/export-campaign-csv")
	public ResponseEntity<String> exportCampaignCsv(@PathVariable UUID campaignId) {
		return reportsService.exportCampaignCsv(campaignId);
	}

	@GetMapping(path = "view-campaign/{campaignId}/download-campaign-csv/{csvName}")
	public ResponseEntity<byte[]> downloadCampaignCsv(@PathVariable UUID campaignId, @PathVariable UUID csvName) {
		return reportsService.downloadCampaignCsv(campaignId, csvName);
	}

	@GetMapping(path = "view-campaign/download-template/upload-influencers")
	public ResponseEntity<byte[]> downloadTemplate() {
		return reportsService.downloadTemplate();
	}
}