package io.smartfluence.brand.management.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "post_type_master")
public class PostMaster {

    @Id
    @Column(name = "post_type_id")
    private Long postTypeId;

    @Column(name = "post_type_name")
    private String postTypeName;


}
