package io.smartfluence.brand.management.rest;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.brand.management.service.DashboardService;
import io.smartfluence.modal.validation.brand.dashboard.DashBoardModal;
import io.smartfluence.modal.validation.brand.dashboard.GetInfluencerBidCountModal;
import io.smartfluence.modal.validation.brand.dashboard.MailTemplateModal;
import io.smartfluence.modal.validation.brand.dashboard.RemoveInfluencerFromCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SaveCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SavePostLink;
import io.smartfluence.modal.validation.brand.dashboard.SaveStatusModel;
import io.smartfluence.modal.validation.brand.dashboard.UploadInfluencersRequestModel;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class DashboardRestController {

	@Autowired
	private DashboardService dashboardService;

	@GetMapping("influencer-snapshot")
	public ResponseEntity<Object> getInfluencerSnapshot(@RequestParam String strCampaignId) {
		log.info(strCampaignId);
		UUID campaignId;
		try {
			campaignId = UUID.fromString(strCampaignId);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
		return dashboardService.getInfluencerSnapshot(campaignId);
	}

	@GetMapping("credit-balance")
	public ResponseEntity<Object> getCreditBalance() {
		return dashboardService.getCreditBalance();
	}

	@GetMapping("get-campaign")
	public ResponseEntity<Object> getCampaign(@RequestParam boolean isAllCampaign) {
		return dashboardService.getCampaign(isAllCampaign);
	}

	@PostMapping("save-campaign")
	public ResponseEntity<Object> saveCampaign(@ModelAttribute SaveCampaignModel saveCampaignModel) {
		return dashboardService.saveCampaign(saveCampaignModel);
	}

	@GetMapping("get-influencer-by-campaign")
	public ResponseEntity<Object> getInfluencerByCampaign(@RequestParam String strCampaignId) {
		log.info(strCampaignId);
		UUID campaignId;
		try {
			campaignId = UUID.fromString(strCampaignId);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
		return dashboardService.getInfluencerByCampaign(campaignId);
	}

	@PostMapping("view-campaign/{campaignId}/save-status")
	public ResponseEntity<Object> saveStatus(@Validated @ModelAttribute SaveStatusModel saveStatusModel,
			@PathVariable UUID campaignId) {
		return dashboardService.saveStatus(saveStatusModel, campaignId);
	}

	@PostMapping("view-campaign/{campaignId}/remove-influencer")
	public ResponseEntity<Object> removeInfluencer(
			@Validated @ModelAttribute RemoveInfluencerFromCampaignModel removeInfluencerFromCampaignModel,
			@PathVariable UUID campaignId) {
		return dashboardService.removeInfluencer(removeInfluencerFromCampaignModel, campaignId);
	}

	@GetMapping("get-campaign-name")
	public ResponseEntity<Object> getCampaignName(@RequestParam String strCampaignId) {
		log.info(strCampaignId);
		UUID campaignId;
		try {
			campaignId = UUID.fromString(strCampaignId);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
		return dashboardService.getCampaignName(campaignId);
	}

	@PostMapping("view-campaign/save-post-link")
	public ResponseEntity<Object> savePostLink(@Validated @ModelAttribute SavePostLink savePostLink) {
		return dashboardService.savePostLink(savePostLink);
	}

	@GetMapping("get-campaign-post-details")
	public ResponseEntity<Object> getCampaignPostLinkDetails(@RequestParam String strCampaignId) {
		log.info(strCampaignId);
		UUID campaignId;
		try {
			campaignId = UUID.fromString(strCampaignId);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
		return dashboardService.getCampaignPostLinkDetails(campaignId);
	}

	@GetMapping("view-campaign/{campaignId}/get-influencer-bid-count")
	public ResponseEntity<Object> getInfluencerBidCount(@Validated @ModelAttribute GetInfluencerBidCountModal campaignBidCountModal,
			@PathVariable UUID campaignId) {
		return dashboardService.getInfluencerBidCount(campaignBidCountModal, campaignId);
	}

	@PostMapping("view-campaign/save-mail-template")
	public ResponseEntity<Object> saveMailTemplate(@Validated @ModelAttribute MailTemplateModal mailTemplateModal) {
		return dashboardService.saveMailTemplate(mailTemplateModal);
	}

	@PostMapping("view-campaign/send-bulk-mail")
	public ResponseEntity<Object> sendBulkMail(@Validated @ModelAttribute DashBoardModal dashBoardModal) {
		return dashboardService.sendBulkMail(dashBoardModal);
	}

	@PostMapping("view-campaign/send-bulk-mail-new")
	public ResponseEntity<Object> sendBulkMailNew(@Validated @ModelAttribute DashBoardModal dashBoardModal) {
		return dashboardService.sendBulkMailNew(dashBoardModal);
	}

	@PostMapping("view-campaign/{campaignId}/upload-bulk-influencers")
	public ResponseEntity<Object> uploadInfluencers(@Validated @ModelAttribute UploadInfluencersRequestModel uploadInfluencersModel,
			@PathVariable UUID campaignId) {
		return dashboardService.uploadInfluencers(uploadInfluencersModel, campaignId);
	}

	@PostMapping("view-campaign/{campaignId}/delete-campaign")
	public ResponseEntity<Object> deleteCampaign(@PathVariable UUID campaignId) {
		log.info("Request for delete campaign for {}", campaignId);
		return dashboardService.deleteCampaign(campaignId);
	}

	@PostMapping("view-campaign/copy-influencers")
	public ResponseEntity<Object> copyInfluencers(@Validated @ModelAttribute DashBoardModal dashBoardModal) {
		return dashboardService.copyInfluencers(dashBoardModal);
	}

	@PostMapping("view-campaign/edit-campaign-name")
	public ResponseEntity<Object> editCampaignName(@Validated @ModelAttribute DashBoardModal dashBoardModal) {
		return dashboardService.editCampaignName(dashBoardModal);
	}
}