package io.smartfluence.util;

import java.text.SimpleDateFormat;

public class DateUtil {

	public static final SimpleDateFormat SDF_YYYY_MM = new SimpleDateFormat("yyyy-MM");

	public static final SimpleDateFormat SDF_MM_DD_YYYY = new SimpleDateFormat("MM-dd-yyyy");

	public static final SimpleDateFormat SDF_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
}