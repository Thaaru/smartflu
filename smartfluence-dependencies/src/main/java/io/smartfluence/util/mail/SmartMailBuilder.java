package io.smartfluence.util.mail;

import java.time.LocalDateTime;

public class SmartMailBuilder {

	private static final String MAIL_HEAD = "<!DOCTYPE html><html><head> <meta content=\"text/html;charset=UTF-8\" http-equiv=\"Content-Type\"></head><body style=\"margin: 8px;\"> <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <thead> <tr> <th style=\"height: 65px;border-bottom: 1px solid black;font-family: Helvetica, Arial, sans-serif;\" align=\"center\"><img src=\"https://app.smartfluence.io/static/img/smartfluence-logo.png\"></th> </tr></thead> <tbody> <tr> <td> <table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding-left: 5px;padding-right: 5px;padding-bottom: 10px;\"> <tbody>";

	private static final String MAIL_TAIL = "<tr align=\"center\"> <td style=\"font-size: 12px; color: #6d7880; padding-top: 16px; padding-bottom: 16px;\"> <p>The Smartfluence Team</p><span style=\"display: inline-block; padding-left: 15px; padding-right: 15px;\"><a style=\"text-decoration: none;mix-blend-mode: difference;color: #009e83;font-family: Helvetica, Arial, sans-serif;\" href=\"http://www.facebook.com/smartfluence\" target=\"_blank\"><img src=\"https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png\" style=\"display:block\" height=\"24\" width=\"24\"></a></span> <span style=\"display: inline-block; padding-left: 15px; padding-right: 15px;\"><a style=\"text-decoration: none;mix-blend-mode: difference;color: #009e83;font-family: Helvetica, Arial, sans-serif;\" href=\"http://instagram.com/smartfluence\" target=\"_blank\"><img src=\"https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png\" style=\"display:block\" height=\"24\" width=\"24\"></a></span> <span style=\"display: inline-block; padding-left: 15px; padding-right: 15px;\"><a style=\"text-decoration: none;mix-blend-mode: difference;color: #009e83;font-family: Helvetica, Arial, sans-serif;\" href=\"http://www.twitter.com/smartfluence\" target=\"_blank\"><img src=\"https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png\" style=\"display:block\" height=\"24\" width=\"24\"></a></span> <span style=\"display: inline-block; padding-left: 15px; padding-right: 15px;\"><a style=\"text-decoration: none;mix-blend-mode: difference;color: #009e83;font-family: Helvetica, Arial, sans-serif;\" href=\"https://www.linkedin.com/company/smartfluence/\" target=\"_blank\"><img src=\"https://cdn-images.mailchimp.com/icons/social-block-v2/color-linkedin-48.png\" style=\"display:block\" height=\"24\" width=\"24\"></a></span> </td></tr></tbody> </table> </td></tr><tr style=\"background: #009e83;\"> <td> <table width=\"460\" height=\"55\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <tbody> <tr valign=\"center\"> <td width=\"350\" valign=\"center\"> <p style=\"color: #ffffff;font-size: 9px;font-family: Verdana, Arial, Helvetica, sans-serif;text-align: center;\" >&copy; " + LocalDateTime.now().getYear() + " Smartfluence</p><p style=\"color: #ffffff;font-size: 9px;font-family: Verdana, Arial, Helvetica, sans-serif;text-align: center;\"> By using this site, you agree to the <a style=\"text-decoration: none;mix-blend-mode: difference;color: #009e83;font-family: Helvetica, Arial, sans-serif;\" target=\"_blank\" href=\"https://www.smartfluence.io/terms\">Terms of Use</a></p></td></tr></tbody> </table> </td></tr></tbody> </table></body></html>";

	private MailContent salutationContent;

	private MailContent infoContent;

	private MailContent messageContent=new Message(SmartMailBuilder.this);

	private StringBuilder mailContent = new StringBuilder();

	private SmartMailBuilder() {

	}

	public static SmartMailBuilder builder() {
		return new SmartMailBuilder();
	}

	public MailContent salutation() {
		if (salutationContent == null)
			salutationContent = new Salutation(SmartMailBuilder.this);

		return salutationContent;
	}

	public MailContent info() {
		if (infoContent == null)
			infoContent = new Info(SmartMailBuilder.this);

		return infoContent;
	}

	public MailContent message() {
		if (messageContent == null)
			messageContent = new Message(SmartMailBuilder.this);

		return messageContent;
	}

	public SmartMailBuilder and() {
		return SmartMailBuilder.this;
	}

	public String build() {
		mailContent.setLength(0);
		mailContent.append(MAIL_HEAD);

		if (salutationContent != null)
			mailContent.append(salutationContent.content());

		if (infoContent != null)
			mailContent.append(infoContent.content());

		if (messageContent != null)
			mailContent.append(messageContent.content());

		mailContent.append(MAIL_TAIL);
		return mailContent.toString();
	}
}