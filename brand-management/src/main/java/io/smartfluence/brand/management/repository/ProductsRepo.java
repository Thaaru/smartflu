package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.Products;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductsRepo extends CrudRepository<Products, Long> {
    List<Products> findByCampaignId(String campaignId);
}
