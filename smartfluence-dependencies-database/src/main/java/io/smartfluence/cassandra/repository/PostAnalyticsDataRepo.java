package io.smartfluence.cassandra.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.PostAnalyticsData;
import io.smartfluence.cassandra.entities.primary.PostAnalyticsDataKey;

public interface PostAnalyticsDataRepo extends CassandraRepository<PostAnalyticsData, PostAnalyticsDataKey> {

	List<PostAnalyticsData> findByKeyBrandIdAndKeyCampaignId(UUID brandId, UUID campaignId);

	List<PostAnalyticsData> findAllByKeyBrandIdAndKeyCampaignIdAndKeyInfluencerId(UUID brandId, UUID campaignId, UUID influencerId);
}