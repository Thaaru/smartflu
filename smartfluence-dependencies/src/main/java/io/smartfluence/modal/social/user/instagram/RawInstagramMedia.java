package io.smartfluence.modal.social.user.instagram;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawInstagramMedia {

	@JsonAlias("display_url")
	private String displayUrl;

	@JsonAlias("like_count")
	private Long likeCount;

	@JsonAlias("comment_count")
	private Long commentCount;

	@JsonAlias("view_count")
	private Long viewCount;

	@JsonAlias("caption")
	private Caption caption;

	@JsonAlias("user")
	private RawInstagramUser user;
}