package io.smartfluence.cassandra.entities.brand.primary;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class BrandCreditHistoryKey implements Serializable {

	private static final long serialVersionUID = 660102253687081470L;

	public BrandCreditHistoryKey(BrandCreditHistoryKey brandCreditHistoryKey) {
		this.brandId = brandCreditHistoryKey.getBrandId();
		this.transYrMonth = brandCreditHistoryKey.getTransYrMonth();
		this.creditId = brandCreditHistoryKey.getCreditId();
	}

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.PARTITIONED)
	private UUID brandId;

	@PrimaryKeyColumn(name = "trans_yr_month", type = PrimaryKeyType.CLUSTERED,ordinal=1)
	private String transYrMonth;

	@PrimaryKeyColumn(name = "credit_id", type = PrimaryKeyType.CLUSTERED,ordinal=2)
	private UUID creditId;

	@PrimaryKeyColumn(name = "created_at", type = PrimaryKeyType.CLUSTERED,ordinal=3)
	private Date createdAt;
}