package io.smartfluence.cassandra.entities;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.StripeSubscriptionHistoryKey;
import io.smartfluence.constants.SubscriptionStatus;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("stripe_subscription_history")
public class StripeSubscriptionHistory implements Copyable {

	private static final long serialVersionUID = 6161269776651307249L;

	@PrimaryKey
	private StripeSubscriptionHistoryKey key;

	@Column("brand_name")
	private String brandName;

	@Column("subscription_name")
	private String subscriptionName;

	@Column("subscription_type")
	private SubscriptionType subscriptionType;

	@Column("credits")
	private Integer credits;

	@Column("validity")
	private Integer validity;

	@Column("validity_type")
	private String validityType;

	@Column("subscription_date")
	private Date subscriptionDate;

	@Column("reset_credit")
	private boolean resetCredit;

	@Column("renewable")
	private boolean renewable;

	@Column("created_at")
	private Date createdAt;

	@Column("status")
	private SubscriptionStatus status;
}