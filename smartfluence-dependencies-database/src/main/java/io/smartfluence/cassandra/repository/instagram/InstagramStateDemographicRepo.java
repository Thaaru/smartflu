package io.smartfluence.cassandra.repository.instagram;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.instagram.InstagramStateDemographic;
import io.smartfluence.cassandra.entities.instagram.primary.InstagramStateKey;

@Repository
public interface InstagramStateDemographicRepo extends CassandraRepository<InstagramStateDemographic, InstagramStateKey> {

	List<InstagramStateDemographic> findAllByKeyInfluencerHandle(String influencerHandle);

	List<InstagramStateDemographic> findAllByKeyInfluencerHandleIn(List<String> influencerHandles);
}