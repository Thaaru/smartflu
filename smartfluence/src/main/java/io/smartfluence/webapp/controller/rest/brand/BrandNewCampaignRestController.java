package io.smartfluence.webapp.controller.rest.brand;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.smartfluence.modal.brand.campaignv1.CampaignEntityV1;
import io.smartfluence.modal.brand.campaignv1.InfluObj;
import io.smartfluence.modal.brand.campaignv1.WorkflowReqObj;
import io.smartfluence.modal.utill.ResponseObj;
import io.smartfluence.webapp.controller.rest.entity.FullCampaignDetailEntity;
import io.smartfluence.webapp.controller.rest.entity.LaunchFullCampaignEntity;
import io.smartfluence.webapp.proxy.NewBrandCampManagementServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import io.smartfluence.webapp.controller.rest.entity.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandNewCampaignRestController {

    @Autowired
    private NewBrandCampManagementServiceProxy newBrandCampManagementServiceProxy;

    final static Logger LOG=Logger.getLogger(BrandNewCampaignRestController.class.getName());

    @PostMapping("saveNewCampaign")
    public ResponseEntity<Object> saveCampaign(@RequestBody FullCampaignDetailEntity fullCampaignDetailEntity, Model model) {
        LOG.log(Level.INFO,"inside the saveCampaign");
        LOG.log(Level.INFO,fullCampaignDetailEntity.toString());
        return newBrandCampManagementServiceProxy.saveCampaign(fullCampaignDetailEntity);
    }

    @PostMapping("launchNewCampaign")
    public ResponseEntity<Object> launchCampaign(@Validated  @RequestBody LaunchFullCampaignEntity launchFullCampaignEntity) {
        LOG.log(Level.INFO,"launch");
        return newBrandCampManagementServiceProxy.launchCampaign(launchFullCampaignEntity);
    }

//    @PostMapping(value = "save-new-campaign-v1")
//    public ResponseEntity<CampaignEntityV1> saveCampaignNew(@RequestBody CampaignEntityV1 campaignEntityV1){
//        LOG.log(Level.INFO,"save new campaign"+campaignEntityV1);
//        return newBrandCampManagementServiceProxy.saveCampaignNew(campaignEntityV1);
//    }

    @DeleteMapping("delete-campaign")
    public ResponseEntity<Object> deleteCampaign(@RequestParam String id, @RequestParam String status){
        return newBrandCampManagementServiceProxy.deleteCampaignById(id,status);
    }

    @GetMapping("view-campaign-v1/view-influencers")
    public ResponseEntity<Object> getInfluencerByCampaignId(@RequestParam String id, @RequestParam List<String> proposalStatus){
        LOG.log(Level.INFO,id,proposalStatus);
        return newBrandCampManagementServiceProxy.getInfluencerByCampaignId(id,proposalStatus);
    }

    @PostMapping(value = "save-new-campaign-v1")
    public ResponseEntity<CampaignEntityV1> saveCampaignNew(@RequestParam("campaignEntityV1") String campaignEntity,@RequestParam(value = "file",required = false) MultipartFile file) {
        LOG.log(Level.INFO, campaignEntity);

        ObjectMapper mapper = new ObjectMapper();
        try{
            CampaignEntityV1 campaignEntityObj= mapper.readValue(campaignEntity,CampaignEntityV1.class);
            if(Objects.nonNull(file)){
                if(file.isEmpty()){
                    LOG.log(Level.SEVERE,"file was empty");
                    //send error response stating that file is not present
                    return getErrorResp("file shouldn't be empty",campaignEntityObj);
                }else if(!Objects.equals(file.getContentType(),"application/pdf")){
                    //send error response stating that file should be pdf only
                    LOG.log(Level.SEVERE,"file should be pdf only and file format given is "+file.getContentType());
                    return getErrorResp("uploaded file should be pdf only",campaignEntityObj);
                }
                return newBrandCampManagementServiceProxy.saveCampaignNewFile(campaignEntity,file);
            }
//            if(Objects.equals(campaignEntityObj.getTermType(),"custom")){
//
//                if(Objects.nonNull(file)){
//                    //call proxy with file
//                    if(file.isEmpty()){
//                        LOG.log(Level.SEVERE,"file was empty");
//                        //send error response stating that file is not present
//                        return getErrorResp("file shouldn't be empty");
//                    }else if(!Objects.equals(file.getContentType(),"application/pdf")){
//                        //send error response stating that file should be pdf only
//                        LOG.log(Level.SEVERE,"file should be pdf only and file format given is "+file.getContentType());
//                        return getErrorResp("uploaded file should be pdf only");
//                    }
//
//                    return newBrandCampManagementServiceProxy.saveCampaignNewFile(campaignEntity,file);
//                }else{
//                    return newBrandCampManagementServiceProxy.saveCampaignNew(campaignEntity);
//                    //call proxy without file
////                    boolean rel =validateTermsAndCondition(campaignEntityObj,file);
////                    if(rel){
////                        //call proxy without file
////                        return newBrandCampManagementServiceProxy.saveCampaignNew(campaignEntity);
////                    }else{
////                        //send error response stating that file is not present
////                        return getErrorResp("file  shouldn't be empty");
////                    }
//                }
//            }
        else{
                //call proxy without file
                return newBrandCampManagementServiceProxy.saveCampaignNew(campaignEntity);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
            LOG.log(Level.SEVERE,e.toString());
            LOG.log(Level.SEVERE,e.getMessage());
        }
        CampaignEntityV1 campaignError = new CampaignEntityV1();
        Map errorsObj = new HashMap<String, String>();
        errorsObj.put("campaignError","something went wrong");
        campaignError.setErrorObj(errorsObj);
        return ResponseEntity.ok(campaignError);
    }

    public ResponseEntity<CampaignEntityV1> getErrorResp(String message,CampaignEntityV1 campaignEntityV1){
        Map errorsObj = new HashMap<String, String>();
        errorsObj.put("campaignError", message);
        campaignEntityV1.setErrorObj(errorsObj);
        return ResponseEntity.ok(campaignEntityV1);
    }


    public boolean validateFile(CampaignEntityV1 campaignEntityV1,MultipartFile file,String type){
        if(Objects.equals(type,"custom")) {
            if (file.isEmpty()) {
                return validateTermsAndCondition(campaignEntityV1);
            } else {
                return true;
            }
        }else{
            return true;
        }
    }


    public boolean validateTermsAndCondition(CampaignEntityV1 campaignEntityV1,MultipartFile file){
        if(file==null){
            if(campaignEntityV1.getTermsCondition()==null){
                return false;
            }else{
                if(campaignEntityV1.getTermsCondition().getTermId()==null){
                    return false;
                }else{
                    return true;
                }
            }
        }else{
            return false;
        }

    }

    public boolean validateTermsAndCondition(CampaignEntityV1 campaignEntityV1){
        if(campaignEntityV1.getTermsCondition()==null){
            return false;
        }else{
            if(campaignEntityV1.getTermsCondition().getTermId()==null){
                return false;
            }else{
                return true;
            }
        }
    }



//    @PostMapping(value = "save-new-campaign-v1",headers ={MediaType.MULTIPART_FORM_DATA_VALUE,MediaType.APPLICATION_JSON_VALUE})
//    public ResponseEntity<CampaignEntityV1> saveCampaignNew(@RequestBody CampaignEntityV1 campaignEntityV1) {
////        LOG.log(Level.INFO, file.getOriginalFilename());
//        LOG.log(Level.INFO, campaignEntityV1.toString());
////        return ResponseEntity.ok(new CampaignEntityV1());
//        return ResponseEntity.ok(new CampaignEntityV1());
////        return newBrandCampManagementServiceProxy.saveCampaignNew(campaignEntityV1,file);
//    }


    @DeleteMapping("remove-influencer")
    public ResponseEntity<Object> removeNewInfluencer(@RequestBody RemoveInfluencerReq removeInfluencerReq) {
        LOG.log(Level.INFO,removeInfluencerReq.toString());
        return newBrandCampManagementServiceProxy.removeNewInfluencer(removeInfluencerReq);
    }

    @PostMapping("view-campaign/copy-new-influencers")
    public ResponseEntity<Object> copyNewInfluencers(@Validated @RequestBody DashBoardModalNew dashBoardModal, BindingResult bindingResult) {
        LOG.log(Level.INFO,dashBoardModal.toString());
        if (!bindingResult.hasFieldErrors())
            return newBrandCampManagementServiceProxy.copyNewInfluencers(dashBoardModal);
        return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping("campaign/{campaignId}/influencers/export-campaign-csv-new")
    ResponseEntity<String> exportCampaignCsvNew(@PathVariable UUID campaignId){
        LOG.log(Level.INFO,"export campaign CSV called");
        return newBrandCampManagementServiceProxy.exportCampaignCsvNew(campaignId);
    }

    @GetMapping("campaign/{campaignId}/influencers/download-campaign-csv-new/{csvName}")
    ResponseEntity<byte[]> downloadCampaignCsvNew(@PathVariable UUID campaignId, @PathVariable UUID csvName){
        LOG.log(Level.INFO,"Download Campaign Csv New called" );
        return newBrandCampManagementServiceProxy.downloadCampaignCsvNew(campaignId,csvName);
    }

    @PostMapping("campaign/{campaignId}/influencers/upload-bulk-influencers-new")
    public ResponseEntity<Object> uploadNewInfluencers(@Validated @ModelAttribute UploadInfluencersRequestModelNew uploadInfluencersModel,
                                                    BindingResult bindingResult, @PathVariable UUID campaignId) {
        LOG.log(Level.INFO,uploadInfluencersModel.toString());
        if (!bindingResult.hasFieldErrors())
            return newBrandCampManagementServiceProxy.uploadNewInfluencers(uploadInfluencersModel.getInfluencerHandles(),
                    uploadInfluencersModel.getWorkbookSheets(), campaignId);

        return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
    }
    @GetMapping("campaign/download-template/upload-influencers-new")
    public ResponseEntity<byte[]> downloadNewTemplate() {
        log.info("Download New template called" );
        return newBrandCampManagementServiceProxy.downloadNewTemplate();
    }

    @PostMapping("view-campaign-v1/view-influencers/sendproposal")
    public ResponseEntity<Object> sendProposal(@RequestBody WorkflowReqObj workflowReqObj){
        return ResponseEntity.ok(newBrandCampManagementServiceProxy.sendProposal(workflowReqObj));
    }

    @PutMapping("view-campaign-v1/view-influencers/updateoffer")
    public ResponseEntity<Object> updateInfluencerOffers(@RequestBody InfluObj influObj){
        return ResponseEntity.ok(newBrandCampManagementServiceProxy.updateInfluencerOffers(influObj));
    }

    @PutMapping("view-campaign-v1/view-influencers/brandapproval")
    public ResponseEntity<Object> brandApproval(@RequestBody WorkflowReqObj workflowReqObj){
        LOG.log(Level.INFO, workflowReqObj.toString());
        return ResponseEntity.ok(newBrandCampManagementServiceProxy.brandApproval(workflowReqObj));
    }

    @PutMapping("view-campaign-v1/update-campaign-name")
    public ResponseEntity<ResponseObj> updateCampaignName(@RequestParam String campaignId, @RequestParam String campaignName)
    {
        LOG.log(Level.INFO,"in web app update campaign name endpoint campaingid="+campaignId+" campaignName="+campaignName);
        return ResponseEntity.ok(newBrandCampManagementServiceProxy.updateCampaignName(campaignId,campaignName));
    }

    @GetMapping("view-campaign-v1/validate-campaign-name")
    public ResponseEntity<ResponseObj> validateCampaignName(@RequestParam String campaignName,@RequestParam String campaignId){
        LOG.log(Level.INFO,"in web app validate campaign name endpoint campaignName="+campaignName);
        return ResponseEntity.ok(newBrandCampManagementServiceProxy.
                validateCampaignName(campaignName,campaignId));
    }

    @GetMapping(value = "gettermscondition/{filename}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable String filename){
        return newBrandCampManagementServiceProxy.downloadFile(filename);
    }

    @DeleteMapping(value = "delete-termscondition")
    public ResponseEntity<ResponseObj> deleteTermsAndCondition(@RequestParam String campaignId,@RequestParam long termId){
        return newBrandCampManagementServiceProxy.deleteTermsAndCondition(campaignId,termId);
    }

    @GetMapping("get-all-new-campaign")
    ResponseEntity<Object> getAllNewCampaignWithInfluencer(){
        return newBrandCampManagementServiceProxy.getAllNewCampaignWithInfluencer();
    }
}
