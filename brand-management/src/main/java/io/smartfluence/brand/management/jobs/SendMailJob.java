package io.smartfluence.brand.management.jobs;

import java.util.Objects;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import io.smartfluence.brand.management.mailbean.MailService;
import io.smartfluence.mysql.entities.MailDetails;
import io.smartfluence.mysql.repository.MailDetailsRepo;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class SendMailJob extends QuartzJobBean {

	@Autowired
	private MailService mailService;

	@Autowired
	private MailDetailsRepo mailDetailsRepo;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		JobDataMap map = context.getJobDetail().getJobDataMap();
		try {
		   MailDetails mailDetails = (MailDetails) map.get("mailDetailsData");
		   log.info("Sending mail for {}", mailDetails.getInfluencerEmail());
		   if (mailDetails.getIsPromote()) {
			  log.info("Cron running for Promote Campaign");
			  mailService.sendPromoteCampaignMailToInfluencers(mailDetails);
		   }else if(Objects.nonNull(mailDetails.getIsNew())){
			  if(mailDetails.getIsNew()){
				 log.info("Cron running for send proposal-new campaign ");
				 mailService.sendProposalMailToInfluencers(mailDetails);
			  }else{
				 if (!mailDetailsRepo.findAllByCampaignId(mailDetails.getCampaignId()).isEmpty()) {
					log.info("Cron running for Bulk mail");
					mailService.sendSubmitBidMailToInfluencer(mailDetails);
				 }
			  }
		   }
		   else {
			  if (!mailDetailsRepo.findAllByCampaignId(mailDetails.getCampaignId()).isEmpty()) {
				 log.info("Cron running for Bulk mail");
				 mailService.sendSubmitBidMailToInfluencer(mailDetails);
			  }
		   }
		   mailDetailsRepo.delete(mailDetails);
		} catch (Exception e) {
		   e.getStackTrace();
		}
	 }  
}