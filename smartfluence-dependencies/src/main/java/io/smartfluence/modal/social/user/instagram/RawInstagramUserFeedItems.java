package io.smartfluence.modal.social.user.instagram;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawInstagramUserFeedItems {

	@JsonAlias("display_url")
	private String displayUrl;

	@JsonAlias("video_url")
	private String videoUrl;

	@JsonAlias("like_count")
	private String likeCount;

	@JsonAlias("comment_count")
	private String commentCount;

	@JsonAlias("code")
	private String id;

	@JsonAlias("user")
	private RawInstagramUser user;
}