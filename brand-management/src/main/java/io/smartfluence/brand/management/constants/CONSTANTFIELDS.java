package io.smartfluence.brand.management.constants;

public class CONSTANTFIELDS {
    public static String INFLUENCER_ACCEPTED="INFLUENCER_ACCEPTED";
    public static String BRAND_REJECTED="BRAND_REJECTED";
    public static String NOT_PAID="NOT_PAID";
    public static String PAID="PAID";
    public static String PRODUCT_FULFILLED="PRODUCT_FULFILLED";
    public static String INVITE_SENT="INVITE_SENT";
    public static String INVITE_NOT_SENT="INVITE_NOT_SENT";
    public static String ACCEPT="accept";
    public static String REJECT="reject";
    public static String ACTIVE="ACTIVE";
    public static String INACTIVE="INACTIVE";
    public static String CLOSED="CLOSED";
    public static String INPROGRESS="INPROGRESS";
    public static String TRACK_POST="TRACK_POST";
    public static String PRODUCTOFFER="PRODUCT";
    public static String COMMISSIONOFFER="COMMISSION";
    public static String PAYMENTOFFER="PAYMENT";
    public static String SAMEPAYMENTOFFER="Same Payment offer type repeated twice";
    public static String SAMECOMMISSIONOFFER="Same Commission offer type repeated twice";
    public static String SAMEPRODUCTOFFER="Same product offer type repeated twice";
    public static String MESSAGE="message";
    public static String CONDITION="cond";
    public static String ACCEPTED="accepted";
    public static String NOTACCEPTED="notaccepted";
    public static String MAXTWOOFFERS="should select max 2 offers only";
    public static String INVALIDPAYMENTOFFER="Payment offer type is invalid when product offer type is already choosen";
    public static String INVALIDPRODUCTOFFER="Product offer type is invalid when payment offer type is already choosen";
    public static String INVALIDOFFER="invalid offer type";
    public static String  INVALIDPAYMENTFIELD="Invalid Payment Offer Value";
    public static String  INVALIDCOMMISSIONFIELD="Invalid commission offer";
}
