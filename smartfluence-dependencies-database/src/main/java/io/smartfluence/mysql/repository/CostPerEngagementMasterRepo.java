package io.smartfluence.mysql.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import io.smartfluence.mysql.entities.CostPerEngagementMaster;

public interface CostPerEngagementMasterRepo extends CrudRepository<CostPerEngagementMaster, Integer> {

	@Query("select u from CostPerEngagementMaster u where u.startRange< :engRate and u.endRange>= :engRate")
	public Optional<CostPerEngagementMaster> findByStartRangeAndEndRange(Double engRate);
}