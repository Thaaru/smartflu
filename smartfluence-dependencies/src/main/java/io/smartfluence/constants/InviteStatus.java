package io.smartfluence.constants;

public enum InviteStatus {
	FREE, INVITED, UNINVITED, REGISTERED
}