package io.smartfluence.cassandra.repository.brand;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.brand.BrandMailTemplates;
import io.smartfluence.cassandra.entities.brand.primary.BrandMailTemplatesKey;

public interface BrandMailTemplatesRepo extends CassandraRepository<BrandMailTemplates, BrandMailTemplatesKey> {

	List<BrandMailTemplates> findByKeyBrandId(UUID brandId);
}