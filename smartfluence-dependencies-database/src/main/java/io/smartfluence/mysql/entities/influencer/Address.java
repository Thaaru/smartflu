package io.smartfluence.mysql.entities.influencer;

import lombok.*;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "campaign_influencer_address_v1")

public class Address {
    @EmbeddedId
    private CampaignInfluencerAddressId campaignInfluencerAddressType;
    private String fullName;
    private String addressLine1;
    private String addressLine2;
    private Long countryId;
    private Long StateId;
    private String cityName;
    private String zipcode;
    private String phoneNumber;

}
