package io.smartfluence.modal.validation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerDataModal {

	@NotBlank(message = "Please Enter UserName")
	private String influencerHandle;

	@Email(message = "Enter valid business email")
	@NotBlank(message = "Enter valid business email")
	private String email;

	@NotBlank(message = "Please Enter Mandatory field(s)")
	private String grecaptcha;

	@NotBlank(message = "Please Enter platform")
	private String platform;
}