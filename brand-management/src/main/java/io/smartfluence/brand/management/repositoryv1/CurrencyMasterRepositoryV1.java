package io.smartfluence.brand.management.repositoryv1;

import io.smartfluence.brand.management.entityv1.CurrencyMasterV1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyMasterRepositoryV1 extends JpaRepository<CurrencyMasterV1,Integer> {
    CurrencyMasterV1 findByCurrencyId(int currencyId);
}
