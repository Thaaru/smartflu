package io.smartfluence.beans;

import java.io.IOException;
import java.net.URL;

import javax.servlet.annotation.ServletSecurity.TransportGuarantee;

import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.core.io.ClassPathResource;

public class WebContainer implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {

	@Override
	public void customize(ConfigurableServletWebServerFactory factory) {
		TomcatServletWebServerFactory tomcat = (TomcatServletWebServerFactory) factory;
		tomcat.addConnectorCustomizers((connector) -> {
			Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();

			try {
				ClassPathResource keystoreResource = new ClassPathResource("smartfluence.p12");
				URL keystoreUrl = keystoreResource.getURL();
				String keystoreLocation = keystoreUrl.toString();
				connector.setScheme("https");
				connector.setSecure(true);
				protocol.setSSLEnabled(true);
				protocol.setKeystoreFile(keystoreLocation);
				protocol.setKeystorePass("$$$sm@rtC3rt$$$");
			} catch (IOException ex) {
				throw new IllegalStateException(
						"can't access keystore: [" + "keystore" + "] or truststore: [" + "keystore" + "]", ex);
			}
		});
		tomcat.addContextCustomizers((context) -> {
			context.addConstraint(securityConstraint());
		});
	}

	private SecurityConstraint securityConstraint() {
		SecurityConstraint securityConstraint = new SecurityConstraint();
		securityConstraint.setUserConstraint(TransportGuarantee.CONFIDENTIAL.name());
		SecurityCollection collection = new SecurityCollection();
		collection.addPattern("/*");
		securityConstraint.addCollection(collection);
		return securityConstraint;
	}
}