package io.smartfluence.brand.management.service;

import java.util.UUID;

import org.springframework.http.ResponseEntity;

import io.smartfluence.modal.validation.brand.promote.PromoteCampaignRequestModel;

public interface PromoteCampaignService {

	ResponseEntity<Object> getPromoteCampaigns();

	ResponseEntity<Object> getPromoteCampaignDetails(UUID campaignId, String pageType);

	ResponseEntity<Object> getPromoteCampaignInfluencers(UUID campaignId);

	ResponseEntity<Object> managePromoteCampaign(PromoteCampaignRequestModel promoteCampaignRequestModel);

	ResponseEntity<String> processPromoteCampaign(UUID campaignId, String action);

	ResponseEntity<String> influencerStatus(UUID campaignId, String influencerDeepSocialId, String action);

	void executePromoteCampaignsCron();

	ResponseEntity<Object> getPromoteMailTemplate(UUID campaignId);
}