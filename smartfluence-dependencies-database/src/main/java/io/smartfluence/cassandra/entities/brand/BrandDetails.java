package io.smartfluence.cassandra.entities.brand;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.InviteStatus;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("brand_details")
public class BrandDetails implements Copyable {

	public BrandDetails(BrandDetails brandDetails) {
		this.key = new UserDetailsKey(brandDetails.getKey());
		this.brandName = brandDetails.getBrandName();
		this.industry = brandDetails.getIndustry();
		this.email = brandDetails.getEmail();
		this.instagramHandle = brandDetails.getInstagramHandle();
		this.website = brandDetails.getWebsite();
		this.timeZone = brandDetails.getTimeZone();
		this.subscriptionName = brandDetails.getSubscriptionName();
		this.subscriptionId = brandDetails.getSubscriptionId();
		this.subscriptionType = brandDetails.getSubscriptionType();
		this.validUpto = brandDetails.getValidUpto();
		this.inviteStatus = brandDetails.getInviteStatus();
		this.modifiedAt = new Date();
		this.createdAt = brandDetails.getCreatedAt();
	}

	private static final long serialVersionUID = -4937231924188163703L;

	@PrimaryKey
	private UserDetailsKey key;

	@Column("brand_name")
	private String brandName;

	@Column("industry")
	private String industry;

	@Column("email")
	private String email;

	@Column("instagram_handle")
	private String instagramHandle;

	@Column("website")
	private String website;

	@Column("time_zone")
	private String timeZone;

	@Column("subscription_name")
	private String subscriptionName;

	@Column("subscription_id")
	private UUID subscriptionId;

	@Column("subscription_type")
	private SubscriptionType subscriptionType;

	@Column("valid_upto")
	private Date validUpto;

	@Column("invite_status")
	private InviteStatus inviteStatus;

	@LastModifiedDate
	@Column("modified_at")
	private Date modifiedAt;

	@Column("created_at")
	private Date createdAt;

	public InviteStatus getInviteStatus() {
		if (this.inviteStatus == null) {
			return InviteStatus.UNINVITED;
		}
		return this.inviteStatus;
	}
}