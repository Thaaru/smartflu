package io.smartfluence.influencer.services;

import io.smartfluence.influencer.entities.CampaignTasks;
import io.smartfluence.influencer.entities.FullCampaignInfluencerDetails;
import io.smartfluence.influencer.entities.PlatformMaster;
import io.smartfluence.influencer.repository.PlatformMasterRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Log4j2
@Service
public class PlatformMasterService {
    @Autowired
    PlatformMasterRepo platformMasterRepo;
 
    public FullCampaignInfluencerDetails getPlatformMasterService(List<CampaignTasks> campaignTaskList, FullCampaignInfluencerDetails fullCampaignInfluencerDetails){
        Logger.getLogger("getPlatformMasterService").log(Level.FINE,"Getting the Platform Master Service");
        List<PlatformMaster> platformMasterList = new ArrayList<>();
        campaignTaskList.forEach(campaignTasks -> {
            PlatformMaster platformMaster = platformMasterRepo.findByPlatformId(campaignTasks.getPlatformId());
            if(platformMaster!=null){
                platformMasterList.add(platformMaster);
            }
        });
        if(!platformMasterList.isEmpty()){
            fullCampaignInfluencerDetails.setPlatFormDetailsList(platformMasterList);
        }
        return fullCampaignInfluencerDetails;
    }
}
