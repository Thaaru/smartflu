package io.smartfluence.cassandra.entities;

import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.ReportSearchHistoryKey;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.ViewType;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("report_search_history")
public class ReportSearchHistory implements Copyable {

	private static final long serialVersionUID = -7771374426414359364L;

	public ReportSearchHistory(ReportSearchHistory reportSearchHistory) {
		this.key = reportSearchHistory.getKey();
		this.influencerUsername = reportSearchHistory.getInfluencerUsername();
		this.influencerDeepSocialId = reportSearchHistory.getInfluencerDeepSocialId();
		this.reportId = reportSearchHistory.getReportId();
		this.brandName = reportSearchHistory.getBrandName();
	}

	@PrimaryKey
	private ReportSearchHistoryKey key;

	@Column("influencer_username")
	private String influencerUsername;

	@Column("influencer_deep_social_id")
	private String influencerDeepSocialId;

	@Column("report_id")
	private String reportId;

	@Column("brand_name")
	private String brandName;

	@Column("view_type")
	private ViewType viewType;

	@Column("platform")
	private Platforms platform;

	@Indexed
	@Column("search_id")
	private UUID searchId;

	@Column("influencer_name")
	private String influencerName;
}