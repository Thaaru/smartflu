var EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

function log(msg) {
	console.log(msg);
}

$.ajaxSetup({
	error: function(jqxhr, settings, thrownError) {
		if (jqxhr.status == 401) {
			console.log(jqxhr);
			setTimeout(function() {
				window.location.href = window.location.origin;
			}, 1000)
		}
	}
});

String.prototype.lpad = function(padString, length) {
	var str = this;
	while (str.length < length)
		str = padString + str;
	return str;
}

var coinSound = new Audio('/static/assets/coin_sound.mp3');

$('.password-toggle').on('click', function() {
	$(this).children().toggleClass("fa-eye fa-eye-slash");
	var input = $($(this).attr("data-toggle"));
	if (input.attr("type") == "password") {
		input.attr("type", "text");
	} else {
		input.attr("type", "password");
	}
});

function formSubmit(e) {
	e.preventDefault();
	var isValid = true;
	$(this).find('input,select,textarea').each(function() {
		if ($(this).is('[required]:invalid')) {
			isValid = false;
		}
	});
	if (!isValid) {
		$(this).addClass('was-validated');
	}
	if (isValid && formCallBack) {
		formCallBack(this);
	}
	return false;
}

$(document).ready(function() {
	$('form.needs-validation').bind('submit',formSubmit);
})

function resetForms() {
	$('form').each(function() {
		this.reset();
		$(this).removeClass('was-validated');
	})
}

$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("active");

	var imgElement = document.getElementById("logo");
	var src = "";
	src = imgElement.src.replace(/^.*[\\\/]/, '');
	imgElement.src = src === 'new-logo-pink.png' ? '/static/img/new_Icon.png' : '/static/img/new-logo-pink.png';

	if (src === 'new-logo-pink.png') {
		$('#logo').css('width','45px');
		$('#logo').css('height','45px');
		$('.explore-form').css('padding-left','80px');
	} else {
		$('#logo').css('width','200px');
		$('#logo').css('height','30px');
		$('#logo').css('padding-right','3.5px');
		$('.explore-form').css('padding-left','216px');
	}
});

$('#sidebar-wrapper').on('transitionend', function() {
	$('.slider.slider-horizontal').each(
		function() {
			var slider = $(this)
			slider.width(slider.parent().width()
					- slider.parent().find('.slider-left').width()
					- slider.parent().find('.slider-right').width() - 30);
		});
});


$("#sidebar a").click(function() {
	$("#sidebar a").removeClass('active');
	$(this).addClass('active');
	if ($("#wrapper").hasClass('active')) {
		$("#menu-toggle").click();
	}
})

toastr.options = {
	"closeButton": true,
	"debug": false,
	"newestOnTop": true,
	"progressBar": false,
	"positionClass": "toast-top-right",
	"preventDuplicates": true,
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "2000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}

var SUCCESS = toastSuccess;
var ERROR = toastError;
var WARNING = toastWarning;
var INFO = toastInfo;

String.prototype.contains = function(char) {
	return this.indexOf(char) != -1
};

var currentUrl = new URL(window.location.href);

function getParam(param) {
	if (typeof param == 'string')
		return currentUrl.searchParams.get(param);
	return null;
}

function hrefLinker() {
	var url = currentUrl;
	if (url.hash && url.hash.length > 0)
		$('[href="' + url.hash + '"]').click();
}

$(document).ready(function() {
	hrefLinker();
	customSlider();
});

function customSlider() {
	var size = 30;
	$('.range-field input[type="range"]').each(function() {
		var suffix = $(this).attr('data-suffix') == undefined ? '' : $(this).attr('data-suffix');
		$(this).parent().prev('span').text(numberFunction($(this).attr('min')) + suffix);
		$(this).parent().next('span').text(numberFunction($(this).attr('max')) +suffix);
		var rightVal = 'calc(' + ((this.value / this.max) * 100) + '% - 12px)';
		$(this).parent().children('.thumb').css('left', rightVal);
		$(this).parent().find('.value').text(numberFunction(this.value) + suffix);
	});
	$('.range-field input[type="range"]').mousedown(function() {
		var suffix = $(this).attr('data-suffix') == undefined ? '' : $(this).attr('data-suffix');
		var rightVal = 'calc(' + ((this.value / this.max) * 100) + '% - 12px)';
		$(this).parent().children('.thumb').css('left', rightVal);
		$(this).parent().find('.value').text(numberFunction(this.value) + suffix);
		$(this).parent().children('.thumb').css({
			top: -size + 3,
			width: size,
			height: size
		});
	}).on('input', function() {
		var suffix=$(this).attr('data-suffix') == undefined ? '' : $(this).attr('data-suffix');
		var rightVal = 'calc(' + ((this.value / this.max) * 100) + '% - 12px)';
		$(this).parent().children('.thumb').css('left', rightVal);
		$(this).parent().find('.value').text(numberFunction(this.value)+ suffix);
	});

	$('.range-field input[type="range"]').mouseout(function() {
		$(this).parent().children('.thumb').css({
			top: 0,
			width: 0,
			height: 0
		})
	});
}

function numberFunction(val) {
	var temp = Number(val);
	var prefix;
	if (temp < 1000) {
		prefix = '';
	} else if (temp < 1000000) {
		prefix = 'K';
		temp = (temp / 1000).toFixed(0);
	} else if (temp < 1000000000) {
		prefix = 'M';
		temp = (temp / 1000000).toFixed(0);
	} else {
		prefix = 'B';
		temp = (temp / 1000000000).toFixed(0);
	}
	temp=wNumb({thousand:','}).to(temp);
	var finalFormat = temp+prefix;
	return finalFormat;
}

numberFormatEvent()

var maxValue = 1000000000;
function numberFormatEvent() {
	$('.number-format').on('keydown', function(e) {
		var allowedKeys = ['delete', 'backspace', 'tab', 'numlock', 'shift', 'end', 'home', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 'f12'];
		var numberKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'];
		var digit = 6;
		var decimal = 2;
		var maxDigit = 1000000000;
		var maxDecimal = 99;

		if (!isNaN(Number($(this).attr('digit')))) {
			digit = Number($(this).attr('digit'));
		}
		if (!isNaN(Number($(this).attr('decimal')))) {
			decimal = Number($(this).attr('decimal'));
		}
		var maxValue;
		if (!isNaN(Number($(this).attr('max')))) {
			maxValue = '' + $(this).attr('max');
		}else{
			maxValue = maxDigit+'';
		}
		maxDigit = Number(maxValue.indexOf('.') == -1 ? maxValue : maxValue.substring(0, maxValue.lastIndexOf('.')));
		maxDecimal = Number(maxValue.indexOf('.') == -1 ? '0' : maxValue.substring(maxValue.lastIndexOf('.') + 1));
		digit = (maxDigit > 0 && ((maxDigit + '').length) > digit) ? (maxDigit + '').length : digit;
		decimal = (maxDecimal > 0 && ((maxDecimal + '').length) > decimal) ? (maxDecimal + '').length : decimal;

		var pressedKey = e.key.toLowerCase();
		//
		if (allowedKeys.indexOf(pressedKey) == -1) {
			e.preventDefault();
			return;
		}
		if (pressedKey == '.' && (this.value.indexOf('.') != -1 || decimal == 0)) {
			e.preventDefault();
			return;
		}
		if (pressedKey == '.' && this.value.length == 0) {
			this.value = '0';
		}

		var value = this.value;
		var digitPart = value.indexOf('.') == -1 ? value : value.substring(0, value.lastIndexOf('.'));
		var decimalPart = value.indexOf('.') == -1 ? '0' : value.substring(value.lastIndexOf('.') + 1);

		if (numberKeys.indexOf(pressedKey) != -1) {
			if (value.indexOf('.') == -1) {
				digitPart += pressedKey;
			} else {
				decimalPart += pressedKey;
			}
			digitPart = digitPart;
			decimalPart = decimalPart;
			if ((maxDecimal + '').length < (decimalPart + '').length) {
				var padding = (decimalPart + '').length - (maxDecimal + '').length;
				for (var i = 0; i < padding; i++) {
					maxDecimal += '0';
				}
			}
			if (value.indexOf('.') == -1 && pressedKey != '.' && (digitPart + '').length > digit) {
				e.preventDefault();
				return;
			}
			if ((decimalPart + '').length > decimal) {
				e.preventDefault();
				return;
			}
			if (value.indexOf('.') == -1 && pressedKey != '.' && Number(digitPart) > Number(maxDigit)) {
				e.preventDefault();
				return;
			}
			if (Number(digitPart) == Number(maxDigit) && Number(decimalPart) > Number(maxDecimal)) {
				e.preventDefault();
				return;
			}
		}
	}).on('click', function(e) {
		this.selectionStart = this.selectionEnd = (this.value+'').length;
	}).on('focus', function(e) {
		this.selectionStart = this.selectionEnd = (this.value+'').length;
	}).on('paste', function(e) {
		e.preventDefault();
	}).on('blur', function(e) {

		if (!isNaN(Number($(this).attr('max')))) {
			maxValue = Number($(this).attr('max'));
		}
		var defaultValue = 0;
		if (!isNaN(Number($(this).attr('default'))) && Number($(this).attr('default')) <= maxValue) {
			defaultValue = $(this).attr('default');
		} else if ($(this).attr('default') == '') {
			defaultValue = '';
		}
		if (this.value.endsWith('.'))
			this.value = this.value + 0;
		if (Number(this.value) == 0) {
			this.value = defaultValue;
		}
	});
}

$.fn.scramble=function(newText,callback) {
	if(newText&&newText!=null&&typeof newText=='string') {
		this.each(function() {
			var textScrambler=new Scrambler(this);
			if(typeof callback !=='function') {
				callback=undefined;
			}
			textScrambler.scramble(newText).then(callback);
		});
	}
}

class Scrambler {

	constructor(el) {
		this.el = el
		this.update = this.update.bind(this)
		this.chars = 'abcdefghijklmnopqrstuvwxyz!@#$*?ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$*?01234567890!@#$*?';
	}
	scramble(newText) {
		const oldText = this.el.innerText
		const length = Math.max(oldText.length, newText.length)
		const promise = new Promise((resolve) => this.resolve = resolve)
		this.queue = []
		for (let i = 0; i < length; i++) {
			const from = oldText[i] || ''
			const to = newText[i] || ''
			const start = Math.floor(Math.random() * 40)
			const end = start + Math.floor(Math.random() * 40)
			this.queue.push({ from, to, start, end })
		}
		cancelAnimationFrame(this.frameRequest)
		this.frame = 0
		this.update()
		return promise
	}
	update() {
		let output = ''
		let complete = 0
		for (let i = 0, n = this.queue.length; i < n; i++) {
			let { from, to, start, end, char } = this.queue[i]
			if (this.frame >= end) {
				complete++
				output += to
			} else if (this.frame >= start) {
				if (!char || Math.random() < 0.28) {
					char = this.randomChar()
					this.queue[i].char = char
				}
				output += `<span class="scramble-progress">${char}</span>`
			} else {
				output += from
			}
		}
		this.el.innerHTML = output
		if (complete === this.queue.length) {
			this.resolve()
		} else {
			this.frameRequest = requestAnimationFrame(this.update)
			this.frame++
		}
	}
	randomChar() {
		return this.chars[Math.floor(Math.random() * this.chars.length)]
	}
}

function getCookie(key) {
	var value = null;
	var cookies = document.cookie.split(';');
	for(var c in cookies) {
		var entry = cookies[c].split('=');
		if (entry[0].trim() == key) {
			value = entry[1];
			break;
		}
	}
	return value;
}

var lastScrollTop = 0;
$(document).scroll(function(event) {
	var st = $(this).scrollTop();
	if (st > lastScrollTop) {
		$(document).trigger('scrolldown',event);
		$(window).trigger('scrolldown',event);
	} else {
		$(document).trigger('scrollup',event);
		$(window).trigger('scrollup',event);
	}
	lastScrollTop = st;
});

+function($) {
	'use strict';

	var modals = $('.modal.multi-step');

	modals.each(function(idx, modal) {
		var $modal = $(modal);
		var $bodies = $modal.find('div.modal-body');
		var total_num_steps = $bodies.length;
		var $progress = $modal.find('.m-progress');
		$progress.find('input').click(function(e) {
			e.preventDefault();
		})

		var reset_on_close = $modal.attr('reset-on-close') === 'true';

		function reset() {
			$modal.find('.step').hide();
			$modal.find('[data-step]').hide();
			$progress.find('input').prop('checked',false).parent().css({background:'none'});
			$progress.find('#progress-step-1').parent().css({background:'#b5b5b510'});
		}

		function updateProgress(current, total) {

			for (var i = 0; i < current; i++) {
				$progress.find('#progress-step-'+i).prop('checked',true).parent().css({background:'azure'});
			}
			$progress.find('#progress-step-'+current).parent().css({background:'rgba(181, 181, 181, 0.25)'});
		}

		function goToStep(step) {
			reset();
			var to_show = $modal.find('.step-' + step);
			if (to_show.length === 0) {
				// at the last step, nothing else to show
				return;
			}
			to_show.show();
			var current = parseInt(step, 10);
			updateProgress(current, total_num_steps);
			findFirstFocusableInput(to_show).focus();
		}

		function findFirstFocusableInput(parent) {
			var candidates = [parent.find('input'), parent.find('select'),
							parent.find('textarea'),parent.find('button')],
				winner = parent;
			$.each(candidates, function() {
				if (this.length > 0) {
					winner = this[0];
					return false;
				}
			});
			return $(winner);
		}

		function bindEventsToModal($modal) {
			var data_steps = [];
			$('[data-step]').each(function() {
				var step = $(this).data().step;
				if (step && $.inArray(step, data_steps) === -1) {
					data_steps.push(step);
				}
			});

			$.each(data_steps, function(i, v) {
				$modal.on('next.m.' + v, {step: v}, function(e) {
					goToStep(e.data.step);
				});
			});
		}

		function initialize() {
			reset();
			updateProgress(1, total_num_steps);
			$modal.find('.step-1').show();

			bindEventsToModal($modal, total_num_steps);
			$modal.data({
				total_num_steps: $bodies.length,
			});
			if (reset_on_close) {
				// Bootstrap 2.3.2
				$modal.on('hidden', function () {
					reset();
					$modal.find('.step-1').show();
				})
				// Bootstrap 3
				$modal.on('hidden.bs.modal', function () {
					reset();
					$modal.find('.step-1').show();
				})
			}
		}
		$modal.on('hidden.bs.modal', function () {
			reset();
			updateProgress(0, total_num_steps); // / add thies line to reset to step 1.

			$modal.find('.step-1').show();
			$modal.find('form').each(function() {
				this.reset();
				$(this).removeClass('was-validated');
			})
		});
		initialize();
	})
}(jQuery);


$.fn.numberAnimate=function(noAnim,decimalplace) {
	$(this).each(function() {
		var text = $(this).text()!=''?$(this).text():$(this).attr('data-value');
		var number = 0;
		var target = 0;
		var decimalPlaces = 0;

		if (!isNaN(Number(text))) {
			target = Number(text);
		}
		if (decimalplace == undefined) {
			decimalPlaces = $(this).attr('data-decimal') == 'true' ? 2 : 0;
		} else {
			decimalPlaces = decimalplace;
		}
		if (noAnim == undefined || noAnim != false) {
			$(this).countup({
				decimalPlaces:decimalPlaces,
				endVal:target
			});
		} else {
			var value = (target.toLocaleString(undefined, {maximumFractionDigits: decimalPlaces}));

			if (decimalPlaces != 0 ) {
				var value = parseFloat(value).toFixed(decimalPlaces);
			}
			$(this).text(value);
		}
	})
};

var isEqual = function (value, other) {

	// Get the value type
	var type = Object.prototype.toString.call(value);

	// If the two objects are not the same type, return false
	if (type !== Object.prototype.toString.call(other)) return false;

	// If items are not an object or array, return false
	if (['[object Array]', '[object Object]'].indexOf(type) < 0) return value===other;

	// Compare the length of the length of the two items
	var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
	var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
	if (valueLen !== otherLen) return false;

	// Compare two items
	var compare = function (item1, item2) {

		// Get the object type
		var itemType = Object.prototype.toString.call(item1);

		// If an object or array, compare recursively
		if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
			if (!isEqual(item1, item2)) return false;
		}

		// Otherwise, do a simple comparison
		else {

			// If the two items are not the same type, return false
			if (itemType !== Object.prototype.toString.call(item2)) return false;

			// Else if it's a function, convert to a string and compare
			// Otherwise, just compare
			if (itemType === '[object Function]') {
				if (item1.toString() !== item2.toString()) return false;
			} else {
				if (item1 !== item2) return false;
			}
		}
	};

	// Compare properties
	if (type === '[object Array]') {
		for (var i = 0; i < valueLen; i++) {
			if (compare(value[i], other[i]) === false) return false;
		}
	} else {
		for (var key in value) {
			if (value.hasOwnProperty(key)) {
				if (compare(value[key], other[key]) === false) return false;
			}
		}
	}

	// If nothing failed, return true
	return true;
};

Number.prototype.countDecimals = function () {
	if (Math.floor(this.valueOf()) === this.valueOf()) return 0;
	return this.toString().split(".")[1].length || 0;
}

function isEmpty(obj) {
	for ( var key in obj) {
		if (obj.hasOwnProperty(key))
			return false;
	}
	return true;
}