package io.smartfluence.modal.social.user.youtube;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawYouTubeChannelInfo implements Serializable {

	private static final long serialVersionUID = 6029070425446455895L;

	@JsonAlias("success")
	private Boolean success;

	@JsonAlias("channel_info")
	private RawYouTubeChannel channel;
}