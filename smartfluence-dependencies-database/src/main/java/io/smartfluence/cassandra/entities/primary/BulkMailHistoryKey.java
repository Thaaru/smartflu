package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class BulkMailHistoryKey implements Serializable {

	private static final long serialVersionUID = 4308851716171201202L;

	public BulkMailHistoryKey(BulkMailHistoryKey key) {
		this.brandId = key.getBrandId();
		this.bulkMailId = key.getBulkMailId();
	}

	@PrimaryKeyColumn(name = "brand_Id", type = PrimaryKeyType.PARTITIONED)
	private UUID brandId;

	@PrimaryKeyColumn(name = "bulk_mail_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID bulkMailId;
}