package io.smartfluence.beans;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Component
public class CORSFilter extends org.springframework.web.filter.CorsFilter {

	private static final List<String> DEFAULT_PERMIT_ALL = Collections.unmodifiableList(Arrays.asList("*"));

	public CORSFilter() {
		super(corsConfigurationSource());
	}

	public static CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration cors = new CorsConfiguration();
		cors.applyPermitDefaultValues();
		cors.setAllowedMethods(DEFAULT_PERMIT_ALL);
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", cors);

		return source;
	}
}