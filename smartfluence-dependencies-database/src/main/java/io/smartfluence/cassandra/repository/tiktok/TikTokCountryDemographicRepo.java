package io.smartfluence.cassandra.repository.tiktok;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.primary.InfluencerCountryKey;
import io.smartfluence.cassandra.entities.tiktok.TikTokCountryDemographic;

@Repository
public interface TikTokCountryDemographicRepo extends CassandraRepository<TikTokCountryDemographic, InfluencerCountryKey> {

	List<TikTokCountryDemographic> findAllByKeyInfluencerHandle(String influencerHandle);

	List<TikTokCountryDemographic> findAllByKeyInfluencerHandleIn(List<String> influencerHandles);
}