package io.smartfluence.webapp.controller;

import java.security.Principal;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.EnumerationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.smartfluence.beans.service.SharedBeanService;
import io.smartfluence.modal.brand.campaignv1.CampaignEntityV1;
import io.smartfluence.stripe.StripeManager;
import io.smartfluence.webapp.proxy.BrandManagementServiceProxy;
import lombok.extern.log4j.Log4j2;

import io.smartfluence.webapp.proxy.NewBrandCampManagementServiceProxy;

@Log4j2
@Controller
@RequestMapping("brand")
public class BrandView {

    @Autowired
    @SuppressWarnings("unused")
    private StripeManager stripeManager;

    @Autowired
    private SharedBeanService beanService;

    @Autowired
    private BrandManagementServiceProxy brandManagementServiceProxy;

    @Autowired
    private NewBrandCampManagementServiceProxy newBrandCampManagementServiceProxy;

    private static final String ACTIVE = "active";

    @GetMapping("campaign")
    public String brandDashboard(Model model, Principal principal, HttpServletRequest header) {
        model.addAttribute("campaign", ACTIVE);
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("campaigns", newBrandCampManagementServiceProxy.getCampaignNew().getBody());
        return "Brand/Campaign";
    }

    @GetMapping("campaign/{campaignId}/workflow")
    public String workflow(Model model, Principal principal, HttpServletRequest header, @PathVariable("campaignId") String campaignId) {
        model.addAttribute("campaign", ACTIVE);
        model.addAttribute("allActiveCampaigns", newBrandCampManagementServiceProxy.getAllNewCampaignWithInfluencer().getBody());
        model.addAttribute("influencers", newBrandCampManagementServiceProxy.getAllInfluencerByNewCampaignId(campaignId).getBody());
        model.addAttribute("profileDetails", brandManagementServiceProxy.getProfileDetails().getBody());
        model.addAttribute("mailTemplates", brandManagementServiceProxy.getMailTemplates().getBody());
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
//        model.addAttribute("campaigns", newBrandCampManagementServiceProxy.getCampaignEntityByCampaignId(campaignId));
        model.addAttribute("campaignObj", newBrandCampManagementServiceProxy.getCampaignEntityByCampaignId(campaignId).getBody());
        return "Brand/Workflow";
    }

    @GetMapping("makePayments")
    public String makePayments(Model model, Principal principal, HttpServletRequest header) {
        return "Brand/MakePayments";
    }

    @GetMapping("fulfillProducts")
    public String fulfillProducts(Model model, Principal principal, HttpServletRequest header) {
        return "Brand/FulfillProducts";
    }

    @GetMapping("statusUpdate")
    public String statusUpdate(Model model, Principal principal, HttpServletRequest header) {
        return "Brand/StatusUpdate";
    }

    @GetMapping("reviewProposal")
    public String reviewProposal(Model model, Principal principal, HttpServletRequest header) {
        return "Brand/ReviewPropsal";
    }

    @GetMapping("/getCampaignById/{campaignId}")
    public String getCampaignById(Model model, @PathVariable("campaignId") UUID campaignId) {
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("campaignObj", newBrandCampManagementServiceProxy.getCampaignById(campaignId));
        return "Brand/NewCampaign";
    }

    @GetMapping("create")
    public String createCampaign(Model model) {
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("createForm", newBrandCampManagementServiceProxy.createCampaign());
        return "Brand/NewCampaign";
    }


    @GetMapping("new-campaign")
    public String getCampaignForm(Model model) {
        CampaignEntityV1 campaignEntityV1 = newBrandCampManagementServiceProxy.getCampaignForm().getBody();
        model.addAttribute("campaignObj", campaignEntityV1);
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        return "Brand/NewCampaign";
    }

    @GetMapping("campaign/{id}/details")
    public String campaignDetailsByid(@PathVariable String id, Model model) {
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("campaignObj", newBrandCampManagementServiceProxy.
                getCampaignEntityByCampaignId(id).getBody());
        return "Brand/CampaignDetails";
    }


    @GetMapping("explore-influencers")
    public String brandExploreInfluencersView(Model model, Principal principal, HttpServletRequest header) {
        log.info(EnumerationUtils.toList(header.getHeaderNames()));
        model.addAttribute("explore", ACTIVE);
        model.addAttribute("industries", beanService.getIndustryList().getBody());
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("allActiveCampaigns", newBrandCampManagementServiceProxy.getAllNewCampaignWithInfluencer().getBody());
        model.addAttribute("campaigns", brandManagementServiceProxy.getCampaign(false).getBody());
        model.addAttribute("mailTemplates", brandManagementServiceProxy.getMailTemplates().getBody());
        model.addAttribute("profileDetails", brandManagementServiceProxy.getProfileDetails().getBody());
        return "Brand/SearchInfluencer";
    }

    @GetMapping("view-influencer/{influencerId}")
    public String brandViewInfluencerView(Model model, @PathVariable String influencerId, @RequestParam boolean isValid, @RequestParam String platform,
                                          @RequestParam(value = "id", required = false) UUID searchId) {
        model.addAttribute("explore", ACTIVE);
        model.addAttribute("influencerModel", brandManagementServiceProxy.getInfluencerDemographicDetails(influencerId, isValid, platform, searchId).getBody());
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("allActiveCampaigns", newBrandCampManagementServiceProxy.getAllNewCampaignWithInfluencer().getBody());
        model.addAttribute("campaigns", brandManagementServiceProxy.getCampaignByInfluencer().getBody());
        model.addAttribute("mailTemplates", brandManagementServiceProxy.getMailTemplates().getBody());
        model.addAttribute("profileDetails", brandManagementServiceProxy.getProfileDetails().getBody());

        return "Brand/ViewInfluencer";
    }

    @GetMapping("settings")
    public String brandSettingsView(Model model) {
        model.addAttribute("settings", ACTIVE);
        model.addAttribute("industries", beanService.getIndustryList().getBody());
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("profileDetails", brandManagementServiceProxy.getProfileDetails().getBody());
        model.addAttribute("emailConnectionDetails", brandManagementServiceProxy.getEmailConnectionSettings().getBody());
        return "Brand/Settings";
    }

    @GetMapping("view-campaign/{strCampaignId}")
    public String brandViewCampaign(Model model, @PathVariable String strCampaignId) {
        model.addAttribute("campaign", ACTIVE);
        model.addAttribute("profileDetails", brandManagementServiceProxy.getProfileDetails().getBody());
        model.addAttribute("allActiveCampaigns", newBrandCampManagementServiceProxy.getAllNewCampaignWithInfluencer().getBody());
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("influencers", brandManagementServiceProxy.getInfluencerByCampaign(strCampaignId).getBody());
        model.addAttribute("campaignDetails", brandManagementServiceProxy.getCampaignName(strCampaignId).getBody());
        model.addAttribute("campaigns", brandManagementServiceProxy.getCampaign(false).getBody());
        model.addAttribute("snapshots", brandManagementServiceProxy.getInfluencerSnapshot(strCampaignId).getBody());
        model.addAttribute("campaignPostLinkDetails", brandManagementServiceProxy.getCampaignPostLinkDetails(strCampaignId).getBody());
        model.addAttribute("mailTemplates", brandManagementServiceProxy.getMailTemplates().getBody());

        return "Brand/ViewCampaign";
    }

    @GetMapping("campaign/{campaignId}/influencers")
    public String getAllInfluencerByNewCampaignId(Model model, @PathVariable("campaignId") String campaignId) {
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("allActiveCampaigns", newBrandCampManagementServiceProxy.getAllNewCampaignWithInfluencer().getBody());
        model.addAttribute("influencers", newBrandCampManagementServiceProxy.getAllInfluencerByNewCampaignId(campaignId).getBody());
        model.addAttribute("campaignObj", newBrandCampManagementServiceProxy.getCampaignEntityByCampaignId(campaignId).getBody());
        model.addAttribute("profileDetails", brandManagementServiceProxy.getProfileDetails().getBody());
        model.addAttribute("mailTemplates", brandManagementServiceProxy.getMailTemplates().getBody());
        return "Brand/ViewNewCampaign";
    }


    @GetMapping("analytics")
    public String campaignAnalytics(Model model, @RequestParam(required = false) String campaignId,
                                    @RequestParam(required = false) String influencerId) {
        model.addAttribute("analytics", ACTIVE);
        model.addAttribute("campaigns", brandManagementServiceProxy.getCampaign(true).getBody());
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());

        return "Brand/CampaignAnalytics";
    }

    @GetMapping("sf-promote")
    public String brandPromoteCampaigns(Model model) {
        model.addAttribute("promote", ACTIVE);
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("profileDetails", brandManagementServiceProxy.getProfileDetails().getBody());
        model.addAttribute("promoteCampaigns", brandManagementServiceProxy.getPromoteCampaigns().getBody());

        return "Brand/PromoteHome";
    }

    @GetMapping("sf-promote/new-campaign")
    public String createPromoteCampaign(Model model) {
        model.addAttribute("promote", ACTIVE);
        model.addAttribute("industries", beanService.getIndustryList().getBody());
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());

        return "Brand/ManagePromoteCampaign";
    }

    @GetMapping("sf-promote/{campaignId}/manage-campaign/{pageType}")
    public String brandPromoteCampaignDetails(Model model, @PathVariable UUID campaignId, @PathVariable String pageType) {
        model.addAttribute("promote", ACTIVE);
        model.addAttribute("industries", beanService.getIndustryList().getBody());
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("promoteMail", brandManagementServiceProxy.getPromoteMailTemplate(campaignId).getBody());
        model.addAttribute("promoteViewCampaign", brandManagementServiceProxy.getPromoteCampaignDetails(campaignId, pageType).getBody());

        return "Brand/ManagePromoteCampaign";
    }

    @GetMapping("sf-promote/{campaignId}/get-influencers")
    public String brandPromoteCampaignInfluencers(Model model, @PathVariable UUID campaignId) {
        model.addAttribute("promote", ACTIVE);
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("promoteViewInfluencers", brandManagementServiceProxy.getPromoteCampaignInfluencers(campaignId).getBody());
        model.addAttribute("promoteViewCampaign", brandManagementServiceProxy.getPromoteCampaignDetails(campaignId, "").getBody());

        return "Brand/PromoteInfluencerManagement";
    }

    @GetMapping("view-campaign-v1/{id}")
    public String getCampaignByid(@PathVariable String id, Model model) {
        model.addAttribute("creditBalance", brandManagementServiceProxy.getCreditBalance().getBody());
        model.addAttribute("campaignObj", newBrandCampManagementServiceProxy.
                getCampaignEntityByCampaignId(id).getBody());
        return "Brand/NewCampaign";
    }
}
