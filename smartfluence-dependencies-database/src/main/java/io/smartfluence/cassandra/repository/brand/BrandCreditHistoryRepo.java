package io.smartfluence.cassandra.repository.brand;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.brand.BrandCreditHistory;
import io.smartfluence.cassandra.entities.brand.primary.BrandCreditHistoryKey;

@Repository
public interface BrandCreditHistoryRepo extends CassandraRepository<BrandCreditHistory, BrandCreditHistoryKey> {

}