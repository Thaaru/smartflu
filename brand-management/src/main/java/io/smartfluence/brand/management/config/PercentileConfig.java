package io.smartfluence.brand.management.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RefreshScope
@Configuration
@ConfigurationProperties
@PropertySource("percentile.properties")
public class PercentileConfig {

	private Map<String, Double> industry;

	private Map<String, Double> age;

	private Map<String, Double> gender;

	private Map<String, Double> location;

	private List<String> advancedLocation = new ArrayList<>();

	private List<String> advancedLocationFilter = new ArrayList<>();
}