package io.smartfluence.mysql.entities.influencer;

public enum OffersType {
    PRODUCT,COMMISSION,PAYMENT,PAYMENT_COMMISSION
}
