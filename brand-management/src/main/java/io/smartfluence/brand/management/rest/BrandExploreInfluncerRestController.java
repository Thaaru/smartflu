package io.smartfluence.brand.management.rest;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.smartfluence.brand.management.service.ExploreInfluencerService;
import io.smartfluence.modal.validation.brand.explore.CampaignDetail;
import io.smartfluence.modal.validation.brand.explore.GetInfluencersRequestModel;
import io.smartfluence.modal.validation.brand.explore.SearchInfluencerRequestModel;
import io.smartfluence.modal.validation.brand.explore.SubmitBidModel;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandExploreInfluncerRestController {

	@Autowired
	private ExploreInfluencerService exploreInfluencerService;

	@GetMapping("get-influencers")
	public ResponseEntity<Object> getInfluencers(@Validated @ModelAttribute GetInfluencersRequestModel getInfluencersRequestModel) {
		return exploreInfluencerService.getInfluencers(getInfluencersRequestModel);
	}

	@GetMapping("view-influencer/{userId}/profile-image")
	public ResponseEntity<byte[]> viewInfluencerProfilePic(@PathVariable String userId) {
		return exploreInfluencerService.viewInfluencerProfilePic(userId);
	}

	@GetMapping("view-influencer/{userId}/profile-name")
	public ResponseEntity<Object> viewInfluencerProfileName(@PathVariable UUID userId) {
		return exploreInfluencerService.viewInfluencerProfileName(userId);
	}

	@GetMapping("influencer-demographic-details")
	public ResponseEntity<Object> getInfluencerDemographicDetails(@RequestParam String influencerId, @RequestParam boolean isValid,
			@RequestParam String platform, @RequestParam(required = false) UUID searchId) {
		log.info("Viewing details for influencer {} for platform {}", influencerId, platform);
		return exploreInfluencerService.getInfluencerDemographicDetails(influencerId, isValid, platform, searchId);
	}

	@PostMapping("view-influencer/submit-bid")
	public ResponseEntity<Object> submitBid(@Validated @ModelAttribute SubmitBidModel submitBidModel) {
		return exploreInfluencerService.submitBid(submitBidModel);
	}

	@PostMapping("view-influencer/send-proposal")
	public ResponseEntity<Object> sendProposal(@Validated @ModelAttribute SubmitBidModel submitBidModel) {
		return exploreInfluencerService.sendProposal(submitBidModel);
	}

	@GetMapping("view-influencer/{userName}/chartData/{platform}")
	public ResponseEntity<Object> getInfluencerChartData(@PathVariable String userName, @PathVariable String platform) {
		return exploreInfluencerService.getInfluencerChartData(userName, platform);
	}

	@GetMapping("view-influencer/{userId}/content/{contentId}")
	public ResponseEntity<byte[]> viewInfluencerContentImage(@PathVariable String userId,
			@PathVariable Integer contentId, @RequestParam String platform) {
		return exploreInfluencerService.viewInfluencerContentImage(userId, contentId, platform);
	}

	@GetMapping("view-influencer/{userId}/stories/{type}")
	public ResponseEntity<Object> viewInfluencerInstagramStories(@PathVariable String userId, @PathVariable Integer type) {
		return exploreInfluencerService.viewInfluencerInstagramStories(userId, type);
	}

	@PostMapping("view-influencer/add-to-campaign")
	public ResponseEntity<Object> addToCampaign(@Validated @ModelAttribute CampaignDetail addToCampaign) {
		return exploreInfluencerService.addToCampaign(addToCampaign);
	}



	@GetMapping("get-campaign-by-influencer")
	public ResponseEntity<Object> getCampaignByInfluencer() {
		return exploreInfluencerService.getCampaignByInfluencer();
	}

	@GetMapping("view-influencer/{influencerHandle}/check-influencer")
	public ResponseEntity<Object> checkInfluencer(@Validated @ModelAttribute CampaignDetail campaignDetail, @PathVariable String influencerHandle) {
		return exploreInfluencerService.checkInfluencer(campaignDetail, influencerHandle);
	}

	@GetMapping("search-influencer")
	public ResponseEntity<Object> searchInfluencer(@Validated @ModelAttribute SearchInfluencerRequestModel searchInfluencerRequestModel) {
		return exploreInfluencerService.searchInfluencer(searchInfluencerRequestModel);
	}

	@GetMapping("get-suggestions/{influencerHandle}")
	public ResponseEntity<Object> getSuggestions(@PathVariable String influencerHandle, @RequestParam String platform) {
		return exploreInfluencerService.getAutoSuggestions(influencerHandle, platform);
	}

	@GetMapping("get-utilities/{name}")
	public ResponseEntity<Object> getUtilities(@PathVariable String name, @RequestParam String data) {
		return exploreInfluencerService.getUtilities(name, data);
	}

	@GetMapping("get-mail-templates")
	public ResponseEntity<Object> getMailTemplates() {
		return exploreInfluencerService.getMailTemplates();
	};
}