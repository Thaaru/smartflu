package io.smartfluence.modal.validation.brand.dashboard;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import io.smartfluence.constants.BidStatus;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SaveStatusModel {

	@NotNull(message = "Enter Status")
	private BidStatus status;

	@NotNull(message = "Enter Bid Id")
	private UUID bidId;

	@NotNull(message = "Enter influencer Id")
	private UUID influencerId;
}