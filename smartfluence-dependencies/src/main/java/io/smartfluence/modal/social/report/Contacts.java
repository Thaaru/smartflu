package io.smartfluence.modal.social.report;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contacts implements Serializable {

	private static final long serialVersionUID = 325768890354068197L;

	@JsonAlias("type")
	private String type;

	@JsonAlias("value")
	private String value;

	@JsonAlias("formatted_value")
	private String formattedValue;
}