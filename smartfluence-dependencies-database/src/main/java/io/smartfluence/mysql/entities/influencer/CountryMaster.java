package io.smartfluence.mysql.entities.influencer;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "country_master")
public class CountryMaster {
    @Id
    private Long countryId;
    private String countryName;
}
