package io.smartfluence.modal.brand.dashboard;

import java.util.Date;
import java.util.UUID;

import io.smartfluence.modal.Copyable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BrandCampaignInfluencerList implements Copyable {

	private static final long serialVersionUID = 287090189285924780L;

	private String influencerHandle;

	private UUID influencerId;

	private String campaignName;

	private UUID campaignId;

	private Double engagement;

	private Double followers;

	private Double audienceCredibility;

	private Double influencerPricing;

	private Integer bidCount;

	private Date createdAt;

	private String influencerEmail;

	private String platform;

	private String influencerLink;

	private String influencerName;
}