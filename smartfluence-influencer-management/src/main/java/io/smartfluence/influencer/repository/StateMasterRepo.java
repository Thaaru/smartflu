package io.smartfluence.influencer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.influencer.entities.StateMaster;

@Repository
public interface StateMasterRepo extends JpaRepository<StateMaster,Long>{
    List<StateMaster> findStateByCountryId(Long countryId);
    StateMaster findStateByStateId(Long stateId);
}
