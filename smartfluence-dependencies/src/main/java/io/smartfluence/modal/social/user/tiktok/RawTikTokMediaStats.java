package io.smartfluence.modal.social.user.tiktok;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTikTokMediaStats {

	@JsonAlias("diggCount")
	private Long likes;

	@JsonAlias("commentCount")
	private Long comments;

	@JsonAlias("playCount")
	private Long views;

	@JsonAlias("shareCount")
	private Long shareCount;
}