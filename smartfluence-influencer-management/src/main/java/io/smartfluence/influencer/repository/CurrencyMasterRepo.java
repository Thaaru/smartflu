package io.smartfluence.influencer.repository;

import io.smartfluence.influencer.entities.CurrencyMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyMasterRepo extends JpaRepository<CurrencyMasterEntity,Long> {
    CurrencyMasterEntity findByCurrencyId(Long currencyId);
}
