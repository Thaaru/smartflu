package io.smartfluence.brand.management.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import io.smartfluence.brand.management.constants.ProposalStatus;
import io.smartfluence.brand.management.entity.CampaignInfluencerKey;
import io.smartfluence.brand.management.entityv1.BrandInfluObj;
import io.smartfluence.brand.management.entityv1.CampaignInfluencerV1Entity;
import io.smartfluence.brand.management.mailbean.MailService;
import io.smartfluence.brand.management.repository.CampaignInfluencerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import io.smartfluence.ResourceServer;
import io.smartfluence.brand.management.dao.DashboardDao;
import io.smartfluence.brand.management.dao.ExploreInfluencerDao;
import io.smartfluence.brand.management.util.SendMailUtil;
import io.smartfluence.cassandra.entities.BulkMailHistory;
import io.smartfluence.cassandra.entities.CampaignPostLinkDetails;
import io.smartfluence.cassandra.entities.InfluencerBulkMail;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.brand.BrandBidDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandMailTemplates;
import io.smartfluence.cassandra.entities.brand.primary.BrandMailTemplatesKey;
import io.smartfluence.cassandra.entities.primary.BulkMailHistoryKey;
import io.smartfluence.cassandra.entities.primary.InfluencerBulkMailKey;
import io.smartfluence.cassandra.entities.primary.ReportSearchHistoryKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.constants.ViewType;
import io.smartfluence.modal.brand.dashboard.BrandCampaignInfluencerList;
import io.smartfluence.modal.brand.dashboard.CampaignDetails;
import io.smartfluence.modal.brand.dashboard.CampaignPostLinkModal;
import io.smartfluence.modal.brand.dashboard.DashBoardResponseModal;
import io.smartfluence.modal.brand.dashboard.DashBoardResponseModal.DashBoardResponseModalBuilder;
import io.smartfluence.modal.brand.dashboard.InfluencerSnapshot;
import io.smartfluence.modal.brand.dashboard.MailTemplates;
import io.smartfluence.modal.brand.dashboard.UploadInfluencersResponseModel;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.brand.viewinfluencer.BrandCampaigns;
import io.smartfluence.modal.social.user.instagram.RawInstagramMediaInfo;
import io.smartfluence.modal.social.user.tiktok.RawTikTokMediaInfo;
import io.smartfluence.modal.social.user.youtube.RawYouTubeVideoInfo;
import io.smartfluence.modal.validation.brand.dashboard.DashBoardModal;
import io.smartfluence.modal.validation.brand.dashboard.GetInfluencerBidCountModal;
import io.smartfluence.modal.validation.brand.dashboard.MailTemplateModal;
import io.smartfluence.modal.validation.brand.dashboard.RemoveInfluencerFromCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SaveCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SavePostLink;
import io.smartfluence.modal.validation.brand.dashboard.SaveStatusModel;
import io.smartfluence.modal.validation.brand.dashboard.UploadInfluencersRequestModel;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.CampaignInfluencerMapping;
import io.smartfluence.mysql.entities.MailDetails;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.proxy.social.SocialDataProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Configuration
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	private DashboardDao dashboardDao;

	@Autowired
	private ExploreInfluencerDao exploreInfluencerDao;

	@Autowired
	private SocialDataProxy socialDataProxy;

	@Autowired
	private SendMailUtil sendMailUtil;

	@Autowired
	private ReportsService reportsService;

	@Autowired
	CampaignInfluencerRepo campaignInfluencerRepo;

	@Autowired
	MailService mailService;

	private static final String BRAND_NAME = "<Brand Name>";

	private static final String ESTIMATED_PRICING = "<Estimated Pricing>";

	private static final String INFLUENCER_HANDLE = "<Influencer Handle>";

	private static final int MAX_INFLUENCER_PER_UPLOAD = 100;

	private static final String UPLOAD_SUCCESS = "SUCCESS";

	private static final String UPLOAD_ERROR = "ERROR";

	private static final String NAME = "Influencer Username";

	private static final String PLATFORM = "Platform";

	private static final String DEFAULT_NAME = "smartfluence";

	final static Logger LOG=Logger.getLogger(DashboardServiceImpl.class.getName());

	@Override
	public ResponseEntity<Object> getCreditBalance() {
		UUID brandId = ResourceServer.getUserId();
		Optional<BrandCredits> brandCredits = dashboardDao.getBrandCreditsById(brandId);
		Integer creditBalance;

		if (brandCredits.isPresent())
			creditBalance = brandCredits.get().getCredits();
		else
			creditBalance = null;

		return ResponseEntity.ok(creditBalance);
	}

	@Override
	public ResponseEntity<Object> getInfluencerSnapshot(UUID campaignId) {
		UUID brandId = ResourceServer.getUserId();
		Set<InfluencerSnapshot> influencerSnapshots = new HashSet<>();
		if (brandId != null)
			reportsService.generateInfluencerSnapshot(campaignId, brandId, influencerSnapshots);

		List<InfluencerSnapshot> sortedInfluencerSnapshot = influencerSnapshots.stream()
				.sorted(Comparator.comparing(InfluencerSnapshot::getBidDate).reversed()).collect(Collectors.toList());
		return ResponseEntity.ok(sortedInfluencerSnapshot);
	}

	@Override
	public ResponseEntity<Object> saveCampaign(SaveCampaignModel saveCampaignModel) {
		return dashboardDao.saveCampaign(saveCampaignModel);
	}

	@Override
	public ResponseEntity<Object> getCampaign(boolean isAllCampaign) {
		UUID brandId = ResourceServer.getUserId();
		List<BrandCampaigns> brandCampaigns = new LinkedList<BrandCampaigns>();
		List<BrandCampaignDetailsMysql> brandCampaignDetails = dashboardDao.getBrandCampaignDetailsMysql(brandId, isAllCampaign);

		if (!brandCampaignDetails.isEmpty()) {
			for (BrandCampaignDetailsMysql brandCampaignDetail : brandCampaignDetails) {
				Date date = brandCampaignDetail.getCreatedAt();
				LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				Integer year = localDate.getYear();
				String month = localDate.getMonth().toString();
				Integer day = localDate.getDayOfMonth();
				String string = month.substring(0, 3);
				String createdAt = day.toString() + "-" + string + "-" + year.toString().substring(2);
				brandCampaigns.add(BrandCampaigns.builder().brandId(brandId)
						.campaignId(UUID.fromString(brandCampaignDetail.getCampaignId()))
						.campaignName(brandCampaignDetail.getCampaignName())
						.campaignStatus(brandCampaignDetail.getCampaignStatus()).stringCreatedAt(createdAt)
						.createdAt(brandCampaignDetail.getCreatedAt()).isPromote(brandCampaignDetail.getIsPromote())
						.influencerCount(brandCampaignDetail.getInfluencerCount()).build());
			}
			List<BrandCampaigns> sortedBrandCampaigns = brandCampaigns.stream()
					.sorted(Comparator.comparing(BrandCampaigns::getCreatedAt).reversed()).collect(Collectors.toList());
			return ResponseEntity.ok(sortedBrandCampaigns);
		}
		return ResponseEntity.ok(brandCampaigns);
	}

	@Override
	public ResponseEntity<Object> getInfluencerByCampaign(UUID campaignId) {
		UUID brandId = ResourceServer.getUserId();
		List<BrandCampaignInfluencerList> brandBrandCampaignInfluencerList = new LinkedList<>();
		reportsService.generateBrandCampaignInfluencerList(campaignId, brandId, brandBrandCampaignInfluencerList);

		return ResponseEntity.ok(brandBrandCampaignInfluencerList.stream()
				.sorted(Comparator.comparing(BrandCampaignInfluencerList::getCreatedAt).reversed()).collect(Collectors.toList()));
	}

	@Override
	public ResponseEntity<Object> saveStatus(SaveStatusModel saveStatusModel, UUID campaignId) {
		return dashboardDao.saveStatus(saveStatusModel, campaignId);
	}

	@Override
	public ResponseEntity<Object> removeInfluencer(RemoveInfluencerFromCampaignModel removeInfluencerFromCampaignModel, UUID campaignId) {
		UUID influencerId = removeInfluencerFromCampaignModel.getInfluencerId();
		boolean notFound = false;
		notFound = dashboardDao.removeInfluencerFromCampaign(campaignId, influencerId, notFound);

		if (notFound)
			return ResponseEntity.notFound().build();

		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<Object> getCampaignName(UUID campaignId) {
		Optional<BrandCampaignDetailsMysql> brandCampaignDetails = dashboardDao.getBrandCampaignDetailsMysqlByCampaignId(campaignId.toString());

		if (brandCampaignDetails.isPresent()) {
			return ResponseEntity.ok(CampaignDetails.builder().campaignName(brandCampaignDetails.get().getCampaignName())
							.influencerCount(brandCampaignDetails.get().getInfluencerCount()).build());
		}
		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<Object> savePostLink(SavePostLink savePostLink) {
		Character last = savePostLink.getPostURL().charAt(savePostLink.getPostURL().length() - 1);
		if (last.equals('/'))
			savePostLink.setPostURL(savePostLink.getPostURL().substring(0, savePostLink.getPostURL().length() - 1));

		Optional<CampaignPostLinkDetails> postDetails = dashboardDao.getPostDetails(savePostLink.getPostURL());

		if (!postDetails.isPresent()) {
			try {
				Optional<SocialIdMaster> socialIdMaster = dashboardDao.getSocialIdMasterById(savePostLink.getInfluencerId().toString());

				if (socialIdMaster.isPresent()) {
					if (validatePostURL(socialIdMaster.get(), savePostLink)) 
						return dashboardDao.savePostLink(savePostLink, socialIdMaster.get());

					return ResponseEntity.status(HttpStatus.CONFLICT).body("Please make sure that the post URL added, matches with the influencer!");
				}
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Failed to add post, please contact help@smartfluence.io");
			} catch (Exception e) {
				e.getStackTrace();
				log.error("Failed to get DeepSocial Data");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The link is invalid. Please provide a valid link!");
			}
		}
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body("This post is already being tracked. Please provide another post!");
	}


	@Override
	public ResponseEntity<Object> validatePostLinkNew(SavePostLink savePostLink) {
		Character last = savePostLink.getPostURL().charAt(savePostLink.getPostURL().length() - 1);
		if (last.equals('/'))
			savePostLink.setPostURL(savePostLink.getPostURL().substring(0, savePostLink.getPostURL().length() - 1));

			try {
				Optional<SocialIdMaster> socialIdMaster = dashboardDao.getSocialIdMasterById(savePostLink.getInfluencerId().toString());

				if (socialIdMaster.isPresent()) {
					if (validatePostURL(socialIdMaster.get(), savePostLink))
						return ResponseEntity.ok(HttpStatus.OK);

					return ResponseEntity.status(HttpStatus.CONFLICT).body("Please make sure that the post URL added, matches with the influencer!");
				}
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Failed to add post, please contact help@smartfluence.io");
			} catch (Exception e) {
				e.getStackTrace();
				log.error("Failed to get DeepSocial Data");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The link is invalid. Please provide a valid link!");
			}
//		}
//		return ResponseEntity.status(HttpStatus.CONFLICT)
//				.body("This post is already being tracked. Please provide another post!");
	}

	private boolean validatePostURL(SocialIdMaster socialIdMaster, SavePostLink savePostLink) {
		if (socialIdMaster.getPlatform().equals(Platforms.INSTAGRAM))
			return validateInstagramPostURL(socialIdMaster, savePostLink);
		else if (socialIdMaster.getPlatform().equals(Platforms.YOUTUBE))
			return validateYoutubePostURL(socialIdMaster, savePostLink);
		else if (socialIdMaster.getPlatform().equals(Platforms.TIKTOK))
			return validateTiktokPostURL(socialIdMaster, savePostLink);

		return false;
	}

	private boolean validateInstagramPostURL(SocialIdMaster socialIdMaster, SavePostLink savePostLink) {
		ResponseEntity<RawInstagramMediaInfo> response = socialDataProxy
				.getInstagramMediaInfo(savePostLink.getPostURL().substring(savePostLink.getPostURL().lastIndexOf("/") + 1));

		if (response.getStatusCode().is2xxSuccessful()) {
			for (int i = 0; i < response.getBody().getItems().size(); i++) {
				if (socialIdMaster.getInfluencerHandle()
						.equalsIgnoreCase("@" + response.getBody().getItems().get(i).getUser().getUserName()))
					return true;
			}
		}
		return false;
	}

	private boolean validateYoutubePostURL(SocialIdMaster socialIdMaster, SavePostLink savePostLink) {
		ResponseEntity<RawYouTubeVideoInfo> response = socialDataProxy.getYoutubeVideoInfo(savePostLink.getPostURL());

		if (response.getStatusCode().is2xxSuccessful()) {
			if (response.getBody().getVideo().getChannelId().equals(socialIdMaster.getInfluencerId()))
				return true;
		}
		return false;
	}

	private boolean validateTiktokPostURL(SocialIdMaster socialIdMaster, SavePostLink savePostLink) {
		ResponseEntity<RawTikTokMediaInfo> response = socialDataProxy.getTiktokMediaInfo(savePostLink.getPostURL());

		if (response.getStatusCode().is2xxSuccessful()) {
			if (response.getBody().getMedia().getItemInfo().getItemStruct().getAuthor().getId().equals(socialIdMaster.getInfluencerId()))
				return true;
		}
		return false;
	}

	@Override
	public ResponseEntity<Object> getCampaignPostLinkDetails(UUID campaignId) {
		UUID brandId = ResourceServer.getUserId();
		List<CampaignPostLinkModal> campaignPostLinkModal = new LinkedList<>();
		reportsService.generateCampaignPostLinkDetails(campaignPostLinkModal, brandId, campaignId);

		return ResponseEntity.ok(campaignPostLinkModal);
	}

	@Override
	public ResponseEntity<Object> getInfluencerBidCount(GetInfluencerBidCountModal campaignBidCountModal, UUID campaignId) {
		UUID brandId = ResourceServer.getUserId();
		List<BrandBidDetails> setBrandBidDetails = dashboardDao.getBrandBidDetailsByKey(brandId, campaignId, campaignBidCountModal.getInfluencerId());
		Integer bidNumber = 1;
		String influencer = setBrandBidDetails.stream().findFirst().isPresent()
				? setBrandBidDetails.stream().findFirst().get().getInfluencerHandle() : "Post";

		if (!setBrandBidDetails.isEmpty()) {
			List<BrandBidDetails> orderedBrandBidDetails = setBrandBidDetails.stream()
					.sorted(Comparator.comparing(BrandBidDetails::getCreatedAt)).collect(Collectors.toList());
			for (BrandBidDetails bidDetails : orderedBrandBidDetails) {
				if (bidDetails.getKey().getBidId().equals(campaignBidCountModal.getBidId()))
					break;

				bidNumber++;
			}
		}
		Map<String, String> postName = new HashMap<String, String>();
		postName.put("postName", influencer + "-" + bidNumber.toString());

		return ResponseEntity.ok(postName);
	}

	@Override
	public ResponseEntity<Object> saveMailTemplate(MailTemplateModal mailTemplateModal) {
		if (mailTemplateModal.getIsNew())
			return createNewTemplate(mailTemplateModal);
		else {
			BrandMailTemplates brandMailTemplate = getMailTemplate(mailTemplateModal);
			if (Objects.nonNull(brandMailTemplate)) {
				if (!brandMailTemplate.getKey().getTemplateName().equals(mailTemplateModal.getTemplateName())) {
					Optional<BrandMailTemplates> optionalBrandMailTemplates = dashboardDao
							.getBrandMailTemplatesByName(mailTemplateModal.getTemplateName());
					if (optionalBrandMailTemplates.isPresent())
						return ResponseEntity.status(HttpStatus.CONFLICT).body("Template Name Already Exist");
					else
						dashboardDao.deleteMailTemplate(brandMailTemplate.getKey());
				}
				return dashboardDao.updateMailTemplate(mailTemplateModal, brandMailTemplate.getCreatedAt());
			} else
				return createNewTemplate(mailTemplateModal);
		}
	}

	private BrandMailTemplates getMailTemplate(MailTemplateModal mailTemplateModal) {
		List<BrandMailTemplates> brandMailTemplatesList = dashboardDao.getAllBrandMailTemplates();
		for (BrandMailTemplates brandMailTemplates : brandMailTemplatesList) {
			if (brandMailTemplates.getTemplateId().equals(UUID.fromString(mailTemplateModal.getTemplateId())))
				return brandMailTemplates;
		}
		return null;
	}

	private ResponseEntity<Object> createNewTemplate(MailTemplateModal mailTemplateModal) {
		Optional<BrandMailTemplates> optionalBrandMailTemplates = dashboardDao
				.getBrandMailTemplatesByName(mailTemplateModal.getTemplateName());
		if (optionalBrandMailTemplates.isPresent())
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Template Name Already Exist");
		else {
			UUID brandId = ResourceServer.getUserId();
			BrandMailTemplates brandMailTemplates = BrandMailTemplates.builder()
					.key(BrandMailTemplatesKey.builder().brandId(brandId).templateName(mailTemplateModal.getTemplateName())
							.build())
					.templateId(UUID.randomUUID()).templateBody(mailTemplateModal.getTemplateBody())
					.templateSubject(mailTemplateModal.getTemplateSubject()).createdAt(new Date()).build();
			dashboardDao.saveBrandMailTemplates(brandMailTemplates);
			return ResponseEntity.ok(brandMailTemplates);
		}
	}

	@Override
	public ResponseEntity<Object> sendBulkMail(DashBoardModal dashBoardModal) {
		List<InfluencerDemographicsMapper> influencerDemographics = dashboardDao.getAllInfluencerDemographics(dashBoardModal.getInfluencerIds());
		UserAccounts userAccounts = dashboardDao.getUserAccounts();

		if (Objects.nonNull(influencerDemographics)) {
			UUID bulkMailId = UUID.randomUUID();
			List<MailTemplates> mailTemplates = exploreInfluencerDao.getMailTemplates();
			MailTemplates mailTemplate = mailTemplates.parallelStream()
					.filter(e -> UUID.fromString(e.getId()).equals(dashBoardModal.getTemplateId())).findAny()
					.orElse(null);

			BulkMailHistory bulkMailHistory = BulkMailHistory.builder().key(BulkMailHistoryKey.builder()
						.brandId(UUID.fromString(userAccounts.getUserId())).bulkMailId(bulkMailId).build())
					.influencersCount(influencerDemographics.size())
					.createdAt(new Date()).build();

			dashboardDao.saveBulkMailHistory(bulkMailHistory);

			for (int i = 0; i < influencerDemographics.size(); i++) {
				InfluencerDemographicsMapper influencerDemographic = influencerDemographics.get(i);

				InfluencerBulkMail influencerBulkMail = InfluencerBulkMail.builder().key(InfluencerBulkMailKey.builder()
						.bulkMailId(bulkMailId).influencerBulkMailId(UUID.randomUUID()).build())
						.influencerHandle(influencerDemographic.getInfluencerHandle())
						.influencerEmail(influencerDemographic.getEmail())
						.createdAt(new Date()).build();

				dashboardDao.saveInfluencerBulkMail(influencerBulkMail);

				if (!StringUtils.isEmpty(influencerDemographic.getEmail()))
					sendMailUtil.addMailToQueue(constructMail(dashBoardModal.getCampaignId(), userAccounts, mailTemplate, influencerDemographic,
							influencerBulkMail.getKey().getInfluencerBulkMailId()));
			}
			sendMailUtil.sendMail();
		}
		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<Object> sendBulkMailNew(DashBoardModal dashBoardModal) {
		List<InfluencerDemographicsMapper> influencerDemographics = dashboardDao.getAllInfluencerDemographics(dashBoardModal.getInfluencerIds());
		UserAccounts userAccounts = dashboardDao.getUserAccounts();

		if (Objects.nonNull(influencerDemographics)) {
			UUID bulkMailId = UUID.randomUUID();
			List<MailTemplates> mailTemplates = exploreInfluencerDao.getMailTemplates();
			MailTemplates mailTemplate = mailTemplates.parallelStream()
					.filter(e -> UUID.fromString(e.getId()).equals(dashBoardModal.getTemplateId())).findAny()
					.orElse(null);

			BulkMailHistory bulkMailHistory = BulkMailHistory.builder().key(BulkMailHistoryKey.builder()
							.brandId(UUID.fromString(userAccounts.getUserId())).bulkMailId(bulkMailId).build())
					.influencersCount(influencerDemographics.size())
					.createdAt(new Date()).build();

			dashboardDao.saveBulkMailHistory(bulkMailHistory);

			for (int i = 0; i < influencerDemographics.size(); i++) {
				InfluencerDemographicsMapper influencerDemographic = influencerDemographics.get(i);

				InfluencerBulkMail influencerBulkMail = InfluencerBulkMail.builder().key(InfluencerBulkMailKey.builder()
								.bulkMailId(bulkMailId).influencerBulkMailId(UUID.randomUUID()).build())
						.influencerHandle(influencerDemographic.getInfluencerHandle())
						.influencerEmail(influencerDemographic.getEmail())
						.createdAt(new Date()).build();

				dashboardDao.saveInfluencerBulkMail(influencerBulkMail);

				if (!StringUtils.isEmpty(influencerDemographic.getEmail())){
					Optional<CampaignInfluencerV1Entity> campaignInfluencerDetails = campaignInfluencerRepo
							.findByCampaignInfluencerId(new CampaignInfluencerKey(dashBoardModal.getCampaignId().toString(),
									influencerDemographic.getUserId().toString()));
					if(campaignInfluencerDetails.isPresent()){
						CampaignInfluencerV1Entity campaignInfluencerEntity = campaignInfluencerDetails.get();
						sendMailUtil.addMailToQueue(constructMailNew(dashBoardModal.getCampaignId(), userAccounts, mailTemplate, influencerDemographic,
								influencerBulkMail.getKey().getInfluencerBulkMailId()));
						campaignInfluencerEntity.setProposalStatus(String.valueOf(ProposalStatus.INVITE_SENT));
						campaignInfluencerRepo.save(campaignInfluencerEntity);
					}else{
						LOG.log(Level.SEVERE,"campaign influencer details not present for that influencerid:-"+influencerDemographic.getUserId()+" and campaignId:-"+dashBoardModal.getCampaignId() +" Time:-"+ LocalDateTime.now());
					}

				}else{
					LOG.log(Level.SEVERE,"While sending the email to the influencer,Influencer Email  doesn't exist for that  influencerhandle:-"+influencerDemographic.getInfluencerHandle()+" brandId:-"+ResourceServer.getUserId()+"" +
							" campaignId:-"+dashBoardModal.getCampaignId()+" time:-"+LocalDateTime.now());
				}
			}
			sendMailUtil.sendMail();
		}
		return ResponseEntity.ok().build();
	}


	private MailDetails constructMail(UUID campaignId, UserAccounts userAccounts, MailTemplates mailTemplate,
			InfluencerDemographicsMapper influencerDemographic, UUID influencerBulkMailId) {

		String mailBody = mailTemplate.getMailBody().replaceAll(BRAND_NAME, userAccounts.getBrandName())
				.replaceAll(INFLUENCER_HANDLE, influencerDemographic.getInfluencerHandle())
				.replaceAll(ESTIMATED_PRICING, String.format("%.2f", influencerDemographic.getPricing()));

		String mailSubject = mailTemplate.getMailSubject().replaceAll(BRAND_NAME, userAccounts.getBrandName())
				.replaceAll(INFLUENCER_HANDLE, influencerDemographic.getInfluencerHandle())
				.replaceAll(ESTIMATED_PRICING, String.format("%.2f", influencerDemographic.getPricing()));

		return MailDetails.builder().mailId(UUID.randomUUID().toString()).brandId(userAccounts.getUserId()).brandEmail(userAccounts.getEmail())
				.brandName(userAccounts.getFirstName() + ' ' + userAccounts.getLastName()).influencerHandle(influencerDemographic.getInfluencerHandle())
				.influencerEmail(influencerDemographic.getEmail()).mailCc(userAccounts.getEmail()).mailSubject(mailSubject).mailDescription(mailBody)
				.mailTemplateId(mailTemplate.getId()).status(2).mappingId(influencerBulkMailId.toString()).isPromote(false).createdAt(new Date())
				.campaignId(campaignId.toString()).build();
	}

	private MailDetails constructMailNew(UUID campaignId, UserAccounts userAccounts, MailTemplates mailTemplate,
									  InfluencerDemographicsMapper influencerDemographic, UUID influencerBulkMailId) {
		BrandInfluObj brandInfluObj=new BrandInfluObj();
		brandInfluObj.setCampaignId(campaignId.toString());
		brandInfluObj.setInfluencerId(influencerDemographic.getUserId().toString());
		String influencerTokenURL = mailService.getInfluencerURL(brandInfluObj);
		String mailBody = mailTemplate.getMailBody().replaceAll(BRAND_NAME, userAccounts.getBrandName())
				.replaceAll(INFLUENCER_HANDLE, influencerDemographic.getInfluencerHandle())
				.replaceAll(ESTIMATED_PRICING, String.format("%.2f", influencerDemographic.getPricing()));

		String mailSubject = mailTemplate.getMailSubject().replaceAll(BRAND_NAME, userAccounts.getBrandName())
				.replaceAll(INFLUENCER_HANDLE, influencerDemographic.getInfluencerHandle())
				.replaceAll(ESTIMATED_PRICING, String.format("%.2f", influencerDemographic.getPricing()));

		return MailDetails.builder().mailId(UUID.randomUUID().toString()).brandId(userAccounts.getUserId()).brandEmail(userAccounts.getEmail())
				.brandName(userAccounts.getFirstName() + ' ' + userAccounts.getLastName()).influencerHandle(influencerDemographic.getInfluencerHandle())
				.influencerEmail(influencerDemographic.getEmail()).mailCc(userAccounts.getEmail()).mailSubject(mailSubject).mailDescription(mailBody)
				.mailTemplateId(mailTemplate.getId()).status(2).mappingId(influencerBulkMailId.toString()).isPromote(false).createdAt(new Date())
				.campaignId(campaignId.toString()).isNew(true).influencerInviteLink(influencerTokenURL).build();
	}

	@Override
	public ResponseEntity<Object> uploadInfluencers(UploadInfluencersRequestModel request, UUID campaignId) {
		UploadInfluencersResponseModel response = new UploadInfluencersResponseModel();
		try {
			if (validateUploadInfluencers(request, response)) {

				UUID brandId = ResourceServer.getUserId();
				JSONObject wookBookJson = new JSONObject(request.getInfluencerHandles());

				Optional<BrandDetails> brandDetails = dashboardDao.getBrandDetails(new UserDetailsKey(UserStatus.APPROVED, brandId));

				if (brandDetails.isPresent()) {
					Optional<BrandCampaignDetailsMysql> brandCampaignDetailsMysql = dashboardDao
							.getBrandCampaignDetailsByCampaignIdAndBrandId(campaignId.toString(), brandId.toString());

					if (brandCampaignDetailsMysql.isPresent()) {
						UserAccounts userAccounts = dashboardDao.getUserAccounts();

						for (int i = 0; i < request.getWorkbookSheets().size(); i++) {
							Set<String> influencersNotAdded = new HashSet<String>();
							Set<String> influencersNotFound = new HashSet<String>();
							Set<String> influencersPulling = new HashSet<String>();
							JSONArray sheetJsonArray = wookBookJson.getJSONArray(request.getWorkbookSheets().get(i));

							if (sheetJsonArray.length() <= MAX_INFLUENCER_PER_UPLOAD) {
								for (int j = 0; j < sheetJsonArray.length(); j++) {
									JSONObject influencers = new JSONObject(sheetJsonArray.get(j).toString());

									if (influencers.has(NAME) && influencers.has(PLATFORM)) {
										String influencerHandle = influencers.getString(NAME).startsWith("@")
												? influencers.getString(NAME).substring(1) : influencers.getString(NAME);
										influencerHandle = influencerHandle.toLowerCase();
										String platform = influencers.getString(PLATFORM);

										if (!platform.equalsIgnoreCase(Platforms.INSTAGRAM.name()) && !platform.equalsIgnoreCase(Platforms.YOUTUBE.name())
												&& !platform.equalsIgnoreCase(Platforms.TIKTOK.name()))
											continue;
										if (influencerHandle.equalsIgnoreCase(DEFAULT_NAME))
											continue;

										Optional<SocialIdMaster> socialIdMaster = dashboardDao.getSocialIdMasterByHandleAndPlatform
												("@" + influencerHandle, reportsService.getPlatform(platform.toLowerCase()));

										if (!socialIdMaster.isPresent()) {
											try {
												UUID userid = reportsService.saveNewInfluencer(influencerHandle, platform, ViewType.UPLOAD);
												socialIdMaster = dashboardDao.getSocialIdMasterById(userid.toString());
											} catch (Exception e) {
												if (e.getMessage().contains("retry_later")) {
													influencersPulling.add(influencerHandle);
													response.setInfluencersPulling(influencersPulling.toString());
												} else {
													influencersNotFound.add(influencerHandle);
													response.setInfluencersNotFound(influencersNotFound.toString());
												}
												continue;
											}
										}
										else {
											reportsService.saveReportHistory(ReportSearchHistory.builder()
													.key(ReportSearchHistoryKey.builder().brandId(UUID.fromString(userAccounts.getUserId())).isNew(false)
															.createdAt(new Date()).build())
													.influencerUsername(socialIdMaster.get().getInfluencerHandle())
													.brandName(userAccounts.getBrandName()).influencerDeepSocialId("").reportId("")
													.viewType(ViewType.UPLOAD).platform(reportsService.getPlatform(platform.toLowerCase())).build());
										}

										Optional<CampaignInfluencerMapping> campaignInfluencerMapping = dashboardDao.
												getBrandCampaignDetailsByCampaignIdAndInfluencerId(campaignId.toString(),
														socialIdMaster.get().getUserId().toString());

										if (!campaignInfluencerMapping.isPresent()) {
											processInfluencersToCampaign(brandDetails, socialIdMaster.get(), brandCampaignDetailsMysql.get());
											response.setUploadStatus(UPLOAD_SUCCESS);
											response.setResponseMessage("Influencers were successfully added to the campaign");
										}
										else {
											influencersNotAdded.add(influencerHandle);
											response.setInfluencersNotAdded(influencersNotAdded.toString());
											continue;
										}
									} else if (j == 0) {
										response.setUploadStatus(UPLOAD_ERROR);
										response.setResponseMessage("Please upload valid template");
										break;
									}
								}
							} else {
								response.setUploadStatus(UPLOAD_ERROR);
								response.setResponseMessage("Not more than 100 Influencers can be processed during upload! "
										+ "Contact us for any assistance.");
								break;
							}
						}
					} else {
						response.setUploadStatus(UPLOAD_ERROR);
						response.setResponseMessage("Campaign not found");
					}
				} else {
					response.setUploadStatus(UPLOAD_ERROR);
					response.setResponseMessage("Brand details not found");
				}
			}
		} catch (JSONException e) {
			e.getStackTrace();
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	private boolean validateUploadInfluencers(UploadInfluencersRequestModel request, UploadInfluencersResponseModel response) {
		if (!ResourceServer.isFreeBrand()) {
			if (request.getWorkbookSheets().size() == 1)
				return true;
			else {
				response.setUploadStatus(UPLOAD_ERROR);
				response.setResponseMessage("Only one sheet is allowed while uploading influencers");
				return false;
			}
		} else {
			response.setUploadStatus(UPLOAD_ERROR);
			response.setResponseMessage("Please subscribe for Higher plans to upload influencers into campaign "
					+ "or please contact help@smartfluence.io to upgrade your plan");
			return false;
		}
	}

	private void processInfluencersToCampaign(Optional<BrandDetails> brandDetails, SocialIdMaster socialIdMaster,
			BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		InfluencerDemographicsMapper demographicsMapper = reportsService.getInfluencerDemographics(socialIdMaster, null, null, false);
		exploreInfluencerDao.processInfluencerToCampaign(brandDetails, brandCampaignDetailsMysql, demographicsMapper);
	}

	@Override
	public ResponseEntity<Object> deleteCampaign(UUID campaignId) {
		Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetails = dashboardDao.getBrandCampaignDetailsMysqlByCampaignId(campaignId.toString());

		DashBoardResponseModalBuilder dashBoardResponseModalBuilder = DashBoardResponseModal.builder();

		if (ResourceServer.isFreeBrand())
			return new ResponseEntity<>(dashBoardResponseModalBuilder.status(403).message("Please subscribe for Higher plans to delete a campaign "
							+ "or please contact help@smartfluence.io to upgrade your plan").build(), HttpStatus.OK);

		if (optionalBrandCampaignDetails.isPresent()) {
			BrandCampaignDetailsMysql brandCampaignDetailsMysql = optionalBrandCampaignDetails.get();

			if (brandCampaignDetailsMysql.getIsPromote())
				return new ResponseEntity<>(dashBoardResponseModalBuilder.status(409).message("Can't delete promote campaign").build(), HttpStatus.OK);

			if (brandCampaignDetailsMysql.getCampaignStatus().equals(CampaignStatus.CLOSED))
				return new ResponseEntity<>(dashBoardResponseModalBuilder.status(409).message("Campaign is already deleted.!").build(), HttpStatus.OK);

			brandCampaignDetailsMysql.setCampaignStatus(CampaignStatus.CLOSED);
			brandCampaignDetailsMysql.setModifiedAt(new Date());
			dashboardDao.saveBrandCampaignDetailsMysql(brandCampaignDetailsMysql);
			dashboardDao.deleteBrandCampaignDetailsMySqlByCampaignId(campaignId.toString());

			return new ResponseEntity<>(dashBoardResponseModalBuilder.status(200).message("Your Campaign was Deleted Successfully").build(), HttpStatus.OK);
		}
		return ResponseEntity.notFound().build();
	}

	@Override
	public ResponseEntity<Object> copyInfluencers(DashBoardModal dashBoardModal) {
		UUID brandId = ResourceServer.getUserId();
		DashBoardResponseModalBuilder dashBoardResponseModalBuilder = DashBoardResponseModal.builder();

		if (ResourceServer.isFreeBrand())
			return new ResponseEntity<>(dashBoardResponseModalBuilder.status(403)
					.message("Please subscribe for Higher plans to copy influencers from campaign"
							+ "or please contact help@smartfluence.io to upgrade your plan").build(), HttpStatus.OK);

		Optional<BrandDetails> brandDetails = dashboardDao.getBrandDetails(new UserDetailsKey(UserStatus.APPROVED, brandId));

		if (brandDetails.isPresent()) {
			Optional<BrandCampaignDetailsMysql> brandCampaignDetailsMysql = dashboardDao
					.getBrandCampaignDetailsMysqlByCampaignId(dashBoardModal.getCampaignId().toString());

			if (brandCampaignDetailsMysql.isPresent()) {
				List<InfluencerDemographicsMapper> influencerDemographics = dashboardDao.getAllInfluencerDemographics(dashBoardModal.getInfluencerIds());
				influencerDemographics.forEach(data -> {
					Optional<BrandCampaignDetails> brandCampaignDetails = dashboardDao.getBrandCampaignDetailsByBrandIdAndCampaignIdAndInfluencerId
							(brandId, UUID.fromString(brandCampaignDetailsMysql.get().getCampaignId()), data.getUserId());

					if (!brandCampaignDetails.isPresent())
						exploreInfluencerDao.processInfluencerToCampaign(brandDetails, brandCampaignDetailsMysql.get(), data);
				});
				return new ResponseEntity<>(dashBoardResponseModalBuilder.status(200).message("Influencers Copied Successfully").build(), HttpStatus.OK);
			}
		}

		return ResponseEntity.notFound().build();
	}

	@Override
	public ResponseEntity<Object> editCampaignName(DashBoardModal dashBoardModal) {
		DashBoardResponseModalBuilder dashBoardResponseModalBuilder = DashBoardResponseModal.builder();

		if (ResourceServer.isFreeBrand())
			return new ResponseEntity<>(dashBoardResponseModalBuilder.status(403).message("Please subscribe for Higher plans to edit campaign name"
							+ "or please contact help@smartfluence.io to upgrade your plan").build(), HttpStatus.OK);

		Optional<BrandCampaignDetailsMysql> optionalBrandCampaignDetailsMysql = dashboardDao
				.getBrandCampaignDetailsMysqlByCampaignId(dashBoardModal.getCampaignId().toString());

		if (optionalBrandCampaignDetailsMysql.isPresent()) {
			BrandCampaignDetailsMysql brandCampaignDetailsMysql = optionalBrandCampaignDetailsMysql.get();

			if (brandCampaignDetailsMysql.getCampaignName().equals(dashBoardModal.getCampaignName()))
				return new ResponseEntity<>(dashBoardResponseModalBuilder.status(200).message("Campaign Name changed successfully").build(), HttpStatus.OK);

			UUID brandId = ResourceServer.getUserId();
			Optional<BrandCampaignDetailsMysql> validateCampaignName = dashboardDao
					.getBrandCampaignDetailsByCampaignNameAndBrandId(dashBoardModal.getCampaignName(), brandId.toString());

			if (validateCampaignName.isPresent())
				return new ResponseEntity<>(dashBoardResponseModalBuilder.status(409).message("This Campaign Name Already Exists").build(), HttpStatus.OK);

			brandCampaignDetailsMysql.setCampaignName(dashBoardModal.getCampaignName());

			return changeCampaignName(brandId,  dashBoardResponseModalBuilder, brandCampaignDetailsMysql);
		}

		return ResponseEntity.notFound().build();
	}

	private ResponseEntity<Object> changeCampaignName(UUID brandId,
			DashBoardResponseModalBuilder dashBoardResponseModalBuilder, BrandCampaignDetailsMysql brandCampaignDetailsMysql) {
		Set<BrandCampaignDetails> brandCampaignDetails = dashboardDao
				.getBrandCampaignDetailsByBrandIdAndCampaignId(brandId, UUID.fromString(brandCampaignDetailsMysql.getCampaignId()));

		brandCampaignDetails.forEach(data -> {
			data.setCampaignName(brandCampaignDetailsMysql.getCampaignName());
			dashboardDao.saveBrandCampaignDetails(data);
		});
		dashboardDao.saveBrandCampaignDetailsMysql(brandCampaignDetailsMysql);

		return new ResponseEntity<>(dashBoardResponseModalBuilder.status(200).message("Campaign Name changed successfully").build(), HttpStatus.OK);
	}
}