package io.smartfluence.mysql.entities.influencer;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "campaign_products_v1")
public class CampaignProducts {
    @Id
//    @Column(name = "product_id")
    private Long productId;

//    @Column(name = "campaign_id")
    private String campaignId;
//    @Column(name = "product_name")
    private String productName;

//    @Column(name = "product_link")
    private String productLink;
}
