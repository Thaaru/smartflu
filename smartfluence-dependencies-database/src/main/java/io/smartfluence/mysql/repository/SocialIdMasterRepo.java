package io.smartfluence.mysql.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.constants.Platforms;
import io.smartfluence.mysql.entities.SocialIdMaster;

@Repository
public interface SocialIdMasterRepo extends JpaRepository<SocialIdMaster, String> {

	List<SocialIdMaster> findAllByUserIdIn(List<String> influencerIds);

	List<SocialIdMaster> findAllByUserId(String influencerIds);


	Optional<SocialIdMaster> findAllByInfluencerIdIn(List<String> influencerIds);

	Optional<SocialIdMaster> findByInfluencerId(String influencerId);

	Optional<SocialIdMaster> findByUserIdIn(String influencerId);

	Optional<SocialIdMaster> findByInfluencerHandleAndPlatform(String influencerHandle, Platforms platform);

	Optional<SocialIdMaster> findByInfluencerIdOrInfluencerReportId(String influencerId, String influencerReportId);
}