package io.smartfluence.cassandra.repository;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import io.smartfluence.cassandra.entities.DatabaseUpdateHistory;

public interface DatabaseUpdateHistoryRepo extends CassandraRepository<DatabaseUpdateHistory, UUID> {

}