package io.smartfluence.brand.management.entityv1;


import io.smartfluence.modal.brand.campaignv1.IsActive;
import io.smartfluence.modal.brand.campaignv1.TermsTypeEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "terms_conditions_v1")
public class TermsConditionsV1 {
    @Id
    @Column(name = "term_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long termId;
    @Column(name = "terms_condition")
    private String termsCondition;
    @Column(name = "terms_condition_file_path")
    private String termsConditionFilePath;
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TermsTypeEntity type;
    @Column(name = "is_active")
    @Enumerated(EnumType.STRING)
    private IsActive isActive;
    @Column(name = "file_name")
    private String fileName;

    public TermsConditionsV1(String termsConditionFilePath, TermsTypeEntity type, IsActive isActive,String fileName) {
        this.termsConditionFilePath = termsConditionFilePath;
        this.type = type;
        this.isActive = isActive;
        this.fileName=fileName;
    }
}
