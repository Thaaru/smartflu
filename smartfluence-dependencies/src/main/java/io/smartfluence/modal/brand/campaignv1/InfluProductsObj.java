package io.smartfluence.modal.brand.campaignv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class InfluProductsObj {
    private String campaignId;
    private String influencerId;
    private long productId;
    private String productName;
    private String productLink;
}
