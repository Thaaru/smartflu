package io.smartfluence.modal.brand.exploreinfluencer;

import lombok.Data;

@Data
public class SearchInfluencerResponseModel {

	private String influencerId;

	private String deepSocialInfluencerId;

	private String reportId = "";

	private String influencerHandle;

	private boolean newInfluencer = false;

	private boolean reportPresent = false;
}