package io.smartfluence.influencer.entities;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "brand_campaign_details_v1")
public class BrandCampaignDetails {
    @Id
    @Column(name = "campaign_id")
    private String campaignId;
    @Column(name = "brand_id")
    private String brandId;
    @Column(name = "brand_description")
    private String brandDescription;
    @Column(name = "campaign_name")
    private String campaignName;
    @Column(name = "campaign_description")
    private String campaignDescription;
    @Column(name = "campaign_status")
    @Enumerated(EnumType.STRING)
    private CampaignStatus campaignStatus;
    @Column(name="hashtags")
    private String hashTags;
    @Column(name = "restrictions")
    private String restrictions;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;
    @Column(name="max_product_count")
    private int maxProductCount;
    @Column(name="terms_id")
    private Long termsId;
    @Column(name = "campaign_end_date")
    private LocalDateTime endDate;
    @Column(name = "influencer_count")
    private int influencerCount;
}
