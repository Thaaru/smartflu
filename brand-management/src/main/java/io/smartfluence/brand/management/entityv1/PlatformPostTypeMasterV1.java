package io.smartfluence.brand.management.entityv1;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "platform_post_type_master")
public class PlatformPostTypeMasterV1 {
    @Id
    @Column(name = "post_type_id")
    private Long postTypeId;
    @Column(name = "post_type_name")
    private String postTypeName;
    @Column(name = "platform_id")
    private int platformId;
}
