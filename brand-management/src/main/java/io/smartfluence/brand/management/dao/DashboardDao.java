package io.smartfluence.brand.management.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.cassandra.entities.BulkMailHistory;
import io.smartfluence.cassandra.entities.CampaignPostLinkDetails;
import io.smartfluence.cassandra.entities.InfluencerBulkMail;
import io.smartfluence.cassandra.entities.brand.BrandBidDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignDetails;
import io.smartfluence.cassandra.entities.brand.BrandCampaignInfluencerHistory;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandMailTemplates;
import io.smartfluence.cassandra.entities.brand.primary.BrandMailTemplatesKey;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.brand.mapper.InfluencerDemographicsMapper;
import io.smartfluence.modal.validation.brand.dashboard.MailTemplateModal;
import io.smartfluence.modal.validation.brand.dashboard.SaveCampaignModel;
import io.smartfluence.modal.validation.brand.dashboard.SavePostLink;
import io.smartfluence.modal.validation.brand.dashboard.SaveStatusModel;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.CampaignInfluencerMapping;
import io.smartfluence.mysql.entities.SocialIdMaster;
import io.smartfluence.mysql.entities.UserAccounts;

public interface DashboardDao {

	Optional<BrandCredits> getBrandCreditsById(UUID brandId);

	List<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysql(UUID brandId, boolean isAllCampaign);

	Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByCampaignId(String campaignId);

	List<BrandBidDetails> getBrandBidDetailsByKey(UUID brandId, UUID campaignId, UUID influencerId);

	void saveBrandMailTemplates(BrandMailTemplates brandMailTemplates);

	@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
	ResponseEntity<Object> saveCampaign(SaveCampaignModel saveCampaignModel);

	@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
	ResponseEntity<Object> saveStatus(SaveStatusModel saveStatusModel, UUID campaignId);

	ResponseEntity<Object> savePostLink(SavePostLink savePostLink, SocialIdMaster socialIdMaster);

	ResponseEntity<Object> savePostLinkNew(SavePostLink savePostLink, SocialIdMaster socialIdMaster);


	Optional<BrandMailTemplates> getBrandMailTemplatesByName(String templateName);

	ResponseEntity<Object> updateMailTemplate(MailTemplateModal mailTemplateModal, Date createdAt);

	List<BrandMailTemplates> getAllBrandMailTemplates();

	void deleteMailTemplate(BrandMailTemplatesKey brandMailTemplatesKey);

	List<InfluencerDemographicsMapper> getAllInfluencerDemographics(List<String> influencerId);

	UserAccounts getUserAccounts();

	Optional<CampaignPostLinkDetails> getPostDetails(String postURL);

	Optional<SocialIdMaster> getSocialIdMasterById(String influencerId);

	Optional<SocialIdMaster> getSocialIdMasterByHandleAndPlatform(String influencerHandle, Platforms platform);

	Optional<BrandDetails> getBrandDetails(UserDetailsKey key);

	Optional<InstagramDemographics> getInstagramDemographics(String instagramHandle);

	Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsByCampaignIdAndBrandId(String campaignId, String brandId);

	Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsByCampaignNameAndBrandId(String campaignName, String brandId);

	Optional<CampaignInfluencerMapping> getBrandCampaignDetailsByCampaignIdAndInfluencerId(String campaignId, String influencerId);

	void saveBrandCampaignDetails(BrandCampaignDetails brandCampaignDetails);

	void saveBrandCampaignInfluencerHistory(BrandCampaignInfluencerHistory brandCampaignInfluencerHistory);

	void saveCampaignInfluencerMapping(CampaignInfluencerMapping campaignInfluencerMapping);

	void saveBrandCampaignDetailsMysql(BrandCampaignDetailsMysql brandCampaignDetailsMysql);

	void saveBulkMailHistory(BulkMailHistory bulkMailHistory);

	void saveInfluencerBulkMail(InfluencerBulkMail influencerBulkMail);

	boolean removeInfluencerFromCampaign(UUID campaignId, UUID influencerId, boolean notFound);

	Set<BrandCampaignDetails> getBrandCampaignDetailsByBrandIdAndCampaignId(UUID brandId, UUID campaignId);

	void deleteBrandCampaignDetailsMySqlByCampaignId(String campaignId);

	Optional<BrandCampaignDetails> getBrandCampaignDetailsByBrandIdAndCampaignIdAndInfluencerId(UUID brandId, UUID campaignId, UUID influencerId);
}