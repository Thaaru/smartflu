package io.smartfluence.modal.brand.campaign.analytics;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PostAnalyticsModal {

	private UUID brandId;

	private String influencerHandle;

	private UUID influencerId;

	private Integer engagement;

	private Integer likes;

	private Integer comments;

	private Integer views;

	private String updatedData;

	private UUID postId;

	private String postName;

	private String platform;
}