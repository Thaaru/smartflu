package io.smartfluence.cassandra.entities;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("campaign_post_link_details")
public class CampaignPostLinkDetails implements Copyable {

	private static final long serialVersionUID = 798112753310092344L;

	public CampaignPostLinkDetails(CampaignPostLinkDetails campaignPostLinkDetails) {
		this.createdAt = campaignPostLinkDetails.getCreatedAt();
		this.modifiedAt = campaignPostLinkDetails.getModifiedAt();
		this.postUrl = campaignPostLinkDetails.getPostUrl();
		this.postName = campaignPostLinkDetails.getPostName();
		this.influencerHandle = campaignPostLinkDetails.getInfluencerHandle();
		this.postUrl = campaignPostLinkDetails.getPostUrl();
		this.brandId = campaignPostLinkDetails.getBrandId();
		this.campaignId = campaignPostLinkDetails.getCampaignId();
		this.influencerId = campaignPostLinkDetails.getInfluencerId();
	}

	@PrimaryKey("post_url")
	private String postUrl;

	@Column("post_id")
	private UUID postId;

	@Column("brand_id")
	private UUID brandId;

	@Column("campaign_id")
	private UUID campaignId;

	@Column("influencer_id")
	private UUID influencerId;

	@Column("created_at")
	private Date createdAt;

	@Column("post_name")
	private String postName;

	@Column("influencer_handle")
	private String influencerHandle;

	@LastModifiedDate
	@Column("modified_at")
	private Date modifiedAt;
}