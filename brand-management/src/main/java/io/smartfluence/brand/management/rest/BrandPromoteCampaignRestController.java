package io.smartfluence.brand.management.rest;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.smartfluence.brand.management.service.PromoteCampaignService;
import io.smartfluence.modal.validation.brand.promote.PromoteCampaignRequestModel;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandPromoteCampaignRestController {

	@Autowired
	private PromoteCampaignService promoteCampaignService;

	@GetMapping("sf-promote/get-campaigns")
	public ResponseEntity<Object> getPromoteCampaigns() {
		return promoteCampaignService.getPromoteCampaigns();
	}

	@GetMapping("sf-promote/view-campaign")
	public ResponseEntity<Object> getPromoteCampaignDetails(@RequestParam UUID campaignId, @RequestParam String pageType) {
		return promoteCampaignService.getPromoteCampaignDetails(campaignId, pageType);
	}

	@GetMapping("sf-promote/get-influencers")
	public ResponseEntity<Object> getPromoteCampaignInfluencers(@RequestParam UUID campaignId) {
		return promoteCampaignService.getPromoteCampaignInfluencers(campaignId);
	}

	@PostMapping("sf-promote/manage-promote-campaign")
	public ResponseEntity<Object> managePromoteCampaign(@Validated @ModelAttribute PromoteCampaignRequestModel promoteCampaignRequestModel) {
		log.info("Create Promote Campaign");
		return promoteCampaignService.managePromoteCampaign(promoteCampaignRequestModel);
	}

	@PostMapping("sf-promote/process-campaign")
	public ResponseEntity<String> processPromoteCampaign(@RequestParam UUID campaignId, @RequestParam String action) {
		log.info("Process Promote Campaign");
		return promoteCampaignService.processPromoteCampaign(campaignId, action);
	}

	@PostMapping("sf-promote/influencer-action")
	public ResponseEntity<String> influencerStatus(@RequestParam UUID campaignId, @RequestParam String influencerDeepSocialId,
			@RequestParam String action) {
		log.info(action + " Influencer");
		return promoteCampaignService.influencerStatus(campaignId, influencerDeepSocialId, action);
	}

	@GetMapping("sf-promote/get-promote-mail-template")
	public ResponseEntity<Object> getPromoteMailTemplate(@RequestParam UUID campaignId) {
		return promoteCampaignService.getPromoteMailTemplate(campaignId);
	}
}