package io.smartfluence.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.nylas.Accounts;
import com.nylas.Draft;
import com.nylas.File;
import com.nylas.Files;
import com.nylas.HostedAuthentication;
import com.nylas.NylasAccount;
import com.nylas.NylasApplication;
import com.nylas.NylasClient;
import com.nylas.RequestFailedException;
import com.nylas.Scope;

import io.smartfluence.modal.nylas.request.NylasMailRequest;
import io.smartfluence.modal.nylas.response.AccessTokenResponse;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class NylasUtil {

	@Autowired
	private EurekaInstanceConfigBean instanceConfigBean;

	private static final String APP_SMARTFLUENCE_IO = "app.smartfluence.io";

	private static final String DEMO_SMARTFLUENCE_IO = "demo.smartfluence.io";

	private static final String RESPONSE_TYPE = "code";

	private static final String STATE = "true";

	private static final String CLIENT_ID = "f45x5kzgq0qetnpjgy0vd8aqf";

	private static final String CLIENT_SECRET = "7ng1p3e6zv7bgu67waufecfk0";

	private String getCallBackURL(String hostname) {

		switch (hostname) {
		case APP_SMARTFLUENCE_IO:
			hostname = "https://app.smartfluence.io/brand/settings";
			break;
		case DEMO_SMARTFLUENCE_IO:
			hostname = "https://demo.smartfluence.io/brand/settings";
			break;
		default:
			hostname = "http://localhost/brand/settings";
			break;
		}
		return hostname;
	}

	private String encodeDecodeAccessCode(String accessCode, int flag) throws UnsupportedEncodingException {
		if (flag == 1)
			return Base64.getEncoder().encodeToString(accessCode.getBytes("utf-8"));
		else
			return new String(Base64.getDecoder().decode(accessCode), "utf-8");
	}

	public String getTokenUrl(String userEmail) {
		String callBack = getCallBackURL(instanceConfigBean.getHostname().toLowerCase().trim());

		NylasClient nylas = new NylasClient();
		NylasApplication application = nylas.application(CLIENT_ID, CLIENT_SECRET);
		HostedAuthentication authentication = application.hostedAuthentication();

		return authentication.urlBuilder()
				.redirectUri(callBack)
				.responseType(RESPONSE_TYPE)
				.scopes(Scope.EMAIL_SEND, Scope.EMAIL_DRAFTS, Scope.EMAIL_READ_ONLY)
				.loginHint(userEmail)
				.state(STATE)
				.buildUrl();
	}

	public void getAccessToken(String token, AccessTokenResponse response) throws IOException, RequestFailedException {
		NylasClient nylas = new NylasClient();
		NylasApplication application = nylas.application(CLIENT_ID, CLIENT_SECRET);
		HostedAuthentication authentication = application.hostedAuthentication();

		NylasAccount account = nylas.account(authentication.fetchToken(token).getAccessToken());
		response.setAccessToken(encodeDecodeAccessCode(account.getAccessToken(), 1));
		response.setAccountEmailAddesss(account.fetchAccountByAccessToken().getEmailAddress());
		response.setAccountId(account.fetchAccountByAccessToken().getId());
		response.setAccountName(account.fetchAccountByAccessToken().getName());
		response.setAccountProvider(account.fetchAccountByAccessToken().getProvider());
		response.setAccountSyncState(account.fetchAccountByAccessToken().getSyncState());
		log.info("Response for access Token => " + response);
	}

	public void sendMail(NylasMailRequest request) throws IOException, RequestFailedException {
		Draft draft = new Draft();
		NylasClient nylas = new NylasClient();

		NylasAccount account = nylas.account(encodeDecodeAccessCode(request.getAccessToken(), 2));
		Files files = account.files();

		if (!StringUtils.isEmpty(request.getFile())) {
			File upload = files.upload(request.getFileName(), "application/pdf", request.getFile());
			draft.attach(account.files().get(upload.getId()));
		}

		draft.setSubject(request.getSubject());
		draft.setTo(request.getToRecipient());
		draft.setBody(request.getMailBody());
		draft.setFrom(request.getFromAddress());
		draft.setCc(request.getCcRecipient());
		draft.setReplyTo(request.getReplyRecipient());
		draft.setBcc(request.getBccRecipient());

		account.drafts().send(draft);
		log.info("Mail sent successfully....!");
	}

	public void removeAccount(String accountId) throws IOException, RequestFailedException {
		NylasClient nylas = new NylasClient();
		NylasApplication application = nylas.application(CLIENT_ID, CLIENT_SECRET);
		Accounts accounts = application.accounts();

		accounts.revokeAllTokensForAccount(accountId, null);
		accounts.downgrade(accountId);
		log.info("Account removed successfully....!");
	}

	public void upgradeAccount(String accountId) throws IOException, RequestFailedException {
		NylasClient nylas = new NylasClient();
		NylasApplication application = nylas.application(CLIENT_ID, CLIENT_SECRET);
		Accounts accounts = application.accounts();

		accounts.upgrade(accountId);
		log.info("Account upgraded successfully....!");
	}
}