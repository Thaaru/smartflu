package io.smartfluence.cassandra.entities;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.PostAnalyticsDataKey;
import io.smartfluence.constants.Platforms;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("post_analytics_data")
public class PostAnalyticsData implements Copyable {

	private static final long serialVersionUID = 4691269244045889812L;

	public PostAnalyticsData(PostAnalyticsData postAnalyticsData) {
		this.key = new PostAnalyticsDataKey(postAnalyticsData.getKey());
		this.postUrl = postAnalyticsData.getPostUrl();
		this.userName = postAnalyticsData.getUserName();
		this.caption = postAnalyticsData.getCaption();
		this.noOfLikes = postAnalyticsData.getNoOfLikes();
		this.noOfComments = postAnalyticsData.getNoOfComments();
		this.noOfViews = postAnalyticsData.getNoOfViews();
		this.updatedDate = postAnalyticsData.getUpdatedDate();
	}

	@PrimaryKey
	private PostAnalyticsDataKey key;

	@Column("post_url")
	private String postUrl;

	@Column("user_name")
	private String userName;

	@Column("caption")
	private String caption;

	@Column("no_of_likes")
	private Integer noOfLikes;

	@Column("no_of_comments")
	private Integer noOfComments;

	@Default
	@Column("no_of_views")
	private Integer noOfViews = 0;

	@Column("updated_date")
	private Date updatedDate;

	@Column("influencer_handle")
	private String influencerHandle;

	@Column("post_name")
	private String postName;

	@Column("platform")
	private Platforms platform;
}