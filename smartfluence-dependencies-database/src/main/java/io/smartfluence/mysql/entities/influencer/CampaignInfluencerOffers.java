package io.smartfluence.mysql.entities.influencer;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "campaign_influencer_offers_v1")
public class CampaignInfluencerOffers {
   @EmbeddedId
   private CampaignInfluencerOfferId campaignInfluencerOfferType;
   @Column(name = "payment_offer")
   private Double paymentOffer;
   @Column(name = "commission_value")
   private Double commissionValue;
   @Column(name = "commission_value_type")
   private String commissionValueType;
   @Column(name = "commission_affiliate_type")
   private String commissionAffiliateType;
   @Column(name = "commission_affiliate_custom_name")
   private String commissionAffiliateCustomName;
   @Column(name = "commission_affiliate_details")
   private String commissionAffiliateDetails;
   @Column(name="commission_value_type_id")
   private Long commissionValueTypeId;
   @Column(name = "commission_affiliate_type_id")
private Long commissionAffiliateTypeId;
}
