package io.smartfluence.servlet;

import java.io.IOException;
import java.net.URL;

import javax.servlet.annotation.ServletSecurity.TransportGuarantee;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(prefix = "smartfluence", name = "use-ssl")
public class WebContainer implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {

	@Override
	public void customize(ConfigurableServletWebServerFactory factory) {
		TomcatServletWebServerFactory tomcat = (TomcatServletWebServerFactory) factory;
		tomcat.addConnectorCustomizers((connector) -> {
			connector.setScheme("http");
			connector.setRedirectPort(443);
			connector.setSecure(false);
		});
		tomcat.addAdditionalTomcatConnectors(secureConnector());
		tomcat.addContextCustomizers((context) -> {
			context.addConstraint(securityConstraint());
		});
	}

	private Connector secureConnector() {
		Connector connector = new Connector(Http11NioProtocol.class.getName());
		Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
		try {

			connector.setScheme("https");
			connector.setSecure(true);
			connector.setPort(443);
			protocol.setSSLEnabled(true);
			ClassPathResource keystoreResource = new ClassPathResource("smartfluence.p12");
			URL keystoreUrl = keystoreResource.getURL();
			String keystoreLocation = keystoreUrl.toString();
			protocol.setKeystoreFile(keystoreLocation);
			protocol.setKeystorePass("$$$sm@rtC3rt$$$");
			return connector;
		} catch (IOException ex) {
			throw new IllegalStateException(
					"can't access keystore: [" + "keystore" + "] or truststore: [" + "keystore" + "]", ex);
		}
	}

	private SecurityConstraint securityConstraint() {
		SecurityConstraint securityConstraint = new SecurityConstraint();
		securityConstraint.setUserConstraint(TransportGuarantee.CONFIDENTIAL.name());
		SecurityCollection collection = new SecurityCollection();
		collection.addPattern("/*");
		securityConstraint.addCollection(collection);
		return securityConstraint;
	}
}