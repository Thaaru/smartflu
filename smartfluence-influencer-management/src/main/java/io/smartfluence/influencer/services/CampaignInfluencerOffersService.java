 package io.smartfluence.influencer.services;

 import io.smartfluence.influencer.entities.*;
 import io.smartfluence.influencer.repository.CampaignInfluenceroffersRepo;
 import io.smartfluence.influencer.repository.CommissionAffiliateTypeRepo;
 import io.smartfluence.influencer.repository.CurrencyMasterRepo;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Service;

 import java.util.List;

 @Service
 public class CampaignInfluencerOffersService {


     @Autowired
     CampaignInfluenceroffersRepo campaignInfluenceroffersRepo;


     @Autowired
     CommissionAffiliateTypeRepo commissionAffiliateTypeRepo;

     @Autowired
     CurrencyMasterRepo currencyMasterRepo;




     public FullCampaignInfluencerDetails getCampaignInfluencerOffer(FullCampaignInfluencerDetails fullCampaignInfluencerDetails, String campaignId, String influencerId){
        List<CampaignInfluencerOffers> campaignOfferList = campaignInfluenceroffersRepo.findByCampaignInfluencerOfferTypeCampaignIdAndCampaignInfluencerOfferTypeInfluencerId(campaignId,influencerId);
        if(!campaignOfferList.isEmpty()){
        campaignOfferList.forEach(campaignInfluencerOffers -> {
                if(campaignInfluencerOffers.getCampaignInfluencerOfferType().getOfferType().equals(OffersType.COMMISSION)) {
                    CommissionAffiliateTypeMasterEntity commissionAffiliateTypeMasterEntity = commissionAffiliateTypeRepo.findBycommAffiTypeId(campaignInfluencerOffers.getCommissionAffiliateTypeId());
                    CurrencyMasterEntity currencyMasterEntity = currencyMasterRepo.findByCurrencyId(campaignInfluencerOffers.getCommissionValueTypeId());
                    campaignInfluencerOffers.setCommissionAffiliateType(commissionAffiliateTypeMasterEntity.getCommAffiName());
                    campaignInfluencerOffers.setCommissionValueType(currencyMasterEntity.getCurrency());
                }
            });
            fullCampaignInfluencerDetails.setOffers(campaignOfferList);
        }
        return fullCampaignInfluencerDetails;
    }
 }
