package io.smartfluence.brand.management.repository;

import io.smartfluence.brand.management.entity.CommissionAffiliateTypeMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommissionAffiliateTypeRepo extends JpaRepository<CommissionAffiliateTypeMaster, Integer> {
}
