package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class AudienceReportHistoryKey implements Serializable{

	private static final long serialVersionUID = 5979439327996348203L;

	@PrimaryKeyColumn(name = "email_id", type = PrimaryKeyType.PARTITIONED)
	private String businessEmailAddress;

	@PrimaryKeyColumn(name = "influencer_deep_social_id", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private String influencerDeepSocialId;

	@Indexed
	@PrimaryKeyColumn(name = "created_at", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private Date createdAt;
}