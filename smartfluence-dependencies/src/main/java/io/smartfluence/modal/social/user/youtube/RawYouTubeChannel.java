package io.smartfluence.modal.social.user.youtube;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawYouTubeChannel implements Serializable {

	private static final long serialVersionUID = -6656922238200657432L;

	@JsonAlias("channel_id")
	private String channelId;

	@JsonAlias("is_removed")
	private Boolean isRemoved;

	@JsonAlias("is_hidden")
	private Boolean isHidden;

	@JsonAlias("is_verified")
	private Boolean isVerified;

	@JsonAlias("fullname")
	private String fullName;

	@JsonAlias("picture")
	private String profileImage;

	@JsonAlias("description")
	private String description;

	@JsonAlias("country")
	private String country;

	@JsonAlias("followers")
	private Long followers;

	@JsonAlias("total_views")
	private Long totalViews;

	@JsonAlias("links")
	private String[] links;
}