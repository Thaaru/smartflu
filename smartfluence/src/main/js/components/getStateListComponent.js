import React from "react";
import { getStateListApi } from "../services/api/campaign_details_api";

const getStateList = async(countryId,setState)=>{
    if(countryId!=null){
    const responeData = await getStateListApi(countryId);
 
    const stateList = responeData.value;
   
    setState(stateList);
    }
   

}

export default getStateList;