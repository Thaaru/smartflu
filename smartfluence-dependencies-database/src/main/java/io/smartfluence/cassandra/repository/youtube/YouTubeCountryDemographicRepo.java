package io.smartfluence.cassandra.repository.youtube;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.primary.InfluencerCountryKey;
import io.smartfluence.cassandra.entities.youtube.YouTubeCountryDemographic;

@Repository
public interface YouTubeCountryDemographicRepo extends CassandraRepository<YouTubeCountryDemographic, InfluencerCountryKey> {

	List<YouTubeCountryDemographic> findAllByKeyInfluencerHandle(String influencerHandle);

	List<YouTubeCountryDemographic> findAllByKeyInfluencerHandleIn(List<String> influencerHandles);
}