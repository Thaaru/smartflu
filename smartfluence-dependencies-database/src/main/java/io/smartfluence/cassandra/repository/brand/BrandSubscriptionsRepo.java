package io.smartfluence.cassandra.repository.brand;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.brand.BrandSubscriptions;
import io.smartfluence.cassandra.entities.brand.primary.BrandSubscriptionsKey;

@Repository
public interface BrandSubscriptionsRepo extends CassandraRepository<BrandSubscriptions, BrandSubscriptionsKey> {

	Set<BrandSubscriptions> findAllByKeyBrandIdInAndKeyActive(Set<UUID> userIds, boolean active);

	Set<BrandSubscriptions> findAllByKeyBrandIdAndKeyActiveIsTrue(UUID userId);

	Optional<BrandSubscriptions> findByKeyBrandIdAndKeyActiveIsTrue(UUID brandId);
}