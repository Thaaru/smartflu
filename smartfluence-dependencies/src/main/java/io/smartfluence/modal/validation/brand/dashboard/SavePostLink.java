package io.smartfluence.modal.validation.brand.dashboard;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SavePostLink {

	@NotNull(message = "Enter InfluencerId")
	private UUID influencerId;

	@NotNull(message = "Enter Payment Type")
	private String paymentType;

	private String paymentMode;

	private String postType;

	private String postDuration;

	private UUID bidId;

	@NotNull(message = "Enter BidAmount/Affiliate")
	private Double bidAmount;

	@NotNull(message = "Enter Post URL")
	private String postURL;

	@NotNull(message = "Enter Post name")
	private String postName;

	private boolean isNew;

	private UUID campaignId;

	@NotBlank(message = "Enter Platform")
	private String platform;

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean isNew) {
		this.isNew = isNew;
	}

}