package io.smartfluence.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import io.smartfluence.modal.zerobounce.response.ZeroBounceResponse;
import io.smartfluence.modal.zerobounce.response.ZeroBounceResponse.ZeroBounceResponseBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

@Component
@Configuration
public class ZeroBounceUtil {

	@Value("${zeroBounce.api.url}")
	private String baseURL;

	@Value("${zeroBounce.api.key}")
	private String apiKey;

	@Value("${zeroBounce.api.timeout}")
	private int timeoutinseconds;

	private final DateFormat dateTimeMillisFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS", Locale.ENGLISH);

	public HttpClient initilizeZeroBounce() {
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeoutinseconds * 1000).build();
		return HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
	}

	public int getCredits() throws IOException, URISyntaxException {
		try {
			HttpClient httpClient = initilizeZeroBounce();
			URI uri = new URIBuilder(baseURL + "/getcredits")
					.addParameter("api_key", apiKey)
					.build();

			HttpGet httpGet = new HttpGet(uri);
			HttpResponse httpResponse = httpClient.execute(httpGet);

			if (httpResponse.getStatusLine().getStatusCode() != 200)
				throw new IllegalStateException(httpResponse.getStatusLine().getStatusCode() + " - " + EntityUtils.toString(httpResponse.getEntity()));
			else
				return new JSONObject(EntityUtils.toString(httpResponse.getEntity())).getInt("Credits");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public ZeroBounceResponse validate(String email, String ip) throws IOException, URISyntaxException {
		HttpClient httpClient = initilizeZeroBounce();
		URI uri = new URIBuilder(baseURL + "/validate")
				.addParameter("api_key", apiKey)
				.addParameter("email", email)
				.addParameter("ip_address", ip)
				.build();

		HttpGet httpGet = new HttpGet(uri);
		ZeroBounceResponseBuilder zeroBounceResponse = ZeroBounceResponse.builder();
		try {
			HttpResponse httpResponse = httpClient.execute(httpGet);
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				JSONObject jsonObject = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));

				zeroBounceResponse.emailAddress(jsonObject.isNull("address") ? null : jsonObject.getString("address"))
						.status(jsonObject.isNull("status") ? null : jsonObject.getString("status"))
						.subStatus(jsonObject.isNull("sub_status") ? null : jsonObject.getString("sub_status"))
						.account(jsonObject.isNull("account") ? null : jsonObject.getString("account"))
						.freeEmail(jsonObject.isNull("free_email") ? null : jsonObject.getBoolean("free_email"))
						.smtpProvider(jsonObject.isNull("smtp_provider") ? null : jsonObject.getString("smtp_provider"))
						.processedAt(jsonObject.isNull("processed_at")
								? null : dateTimeMillisFormat.parse(jsonObject.getString("processed_at")));

				return zeroBounceResponse.build(); 
			} else
				throw new IllegalStateException(httpResponse.getStatusLine().getStatusCode() + " - " + EntityUtils.toString(httpResponse.getEntity()));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException ex) {
			return zeroBounceResponse.emailAddress(email.toLowerCase())
				.status("Unknown").subStatus("timeout_exceeded").build();
		} catch (Exception ex) {
			return zeroBounceResponse.emailAddress(email.toLowerCase())
					.status("Unknown").subStatus("exception_occurred").build();
		}
		return null;
	}
}