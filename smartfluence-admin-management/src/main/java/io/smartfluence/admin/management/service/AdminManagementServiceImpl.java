package io.smartfluence.admin.management.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang.SerializationUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.opencsv.CSVWriter;

import io.smartfluence.admin.database.CassandraDatabaseConnector;
import io.smartfluence.admin.database.GetCassandraData;
import io.smartfluence.admin.management.dao.AdminManagementDao;
import io.smartfluence.admin.management.mail.MailService;
import io.smartfluence.cassandra.entities.DatabaseUpdateHistory;
import io.smartfluence.cassandra.entities.ReportSearchHistory;
import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSubscriptions;
import io.smartfluence.cassandra.entities.brand.primary.BrandSubscriptionsKey;
import io.smartfluence.cassandra.entities.instagram.InstagramContent;
import io.smartfluence.cassandra.entities.instagram.InstagramDemographics;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.constants.DatabaseUpdateStatus;
import io.smartfluence.constants.InviteStatus;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.exception.IllegalActionException;
import io.smartfluence.modal.admin.DataBaseHistoryModal;
import io.smartfluence.modal.admin.ReportResponseModal;
import io.smartfluence.modal.admin.ReportResponseModal.ReportResponseModalBuilder;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserFeed;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserFeedItems;
import io.smartfluence.modal.social.user.instagram.RawInstagramUserInfo;
import io.smartfluence.modal.validation.admin.ReportRequestModal;
import io.smartfluence.modal.validation.influencer.settings.AboutYouModal;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.UserAccountsRepo;
import io.smartfluence.proxy.social.SocialDataProxy;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class AdminManagementServiceImpl implements AdminManagementService {

	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");

	private static final String CSV_EXTENTION = ".csv";

	private static final String XLSX_EXTENTION = ".xlsx";

	private static final String BRAND_REPORT = "SF_Admin_Module";

	private static final String[] ADMIN_MODULE_HEADERS = {"Brand Name", "First Name", "Last Name", "Email Id",
			"Status", "Subscription Name", "Created On"};

	private static final String USAGE_REPORT = "SF_Usage_Report";

	private static final String[] USAGE_REPORT_HEADERS = {"Brand Name", "Influencer Id", "Influencer Name", "Platform", "View Type",
			"IsNew", "Created On"};

	private static final String MAIL_HISTORY_REPORT = "SF_Email_Report";

	private static final String[] MAIL_HISTORY_REPORT_HEADERS = {"Brand Name", "Mail Body", "Mail Subject", "Mail CC", "Mail From", "Mail To",
			"Status", "Created On"};

	private static final String SEARCH_HISTORY_REPORT = "SF_Search_Report";

	private static final String[] SEARCH_HISTORY_REPORT_HEADERS = {"Search_Id", "Brand Name", "Audience Age", "Audience Gender", "Audience Industry",
			"Audience Location", "Bio Text", "Comparison Profile", "Engagements Range", "Followers Range", "Influencer Age", "Influencer Gender",
			"Influencer Industry", "Influencer Location", "Is_Verified", "Key Words", "Platform", "Sort By", "Sponsored Post", "With Contact", "Created On"};

	private static final String[] MAPPING_INFLUENCERS_HEADERS = {"Search_Id", "Brand Name", "Influencer Id", "Influencer Name", "Platform", "View Type",
			"IsNew", "Created On"};

	@Autowired
	private AdminManagementDao adminManagementDao;

	@Autowired
	private SocialDataProxy socialDataProxy;

	@Autowired
	private MailService mailService;

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Autowired
	private GetCassandraData getCassandraData;

	static final ObjectMapper objectMapper = new ObjectMapper();

	private void validateBrandStatus(UserAccounts userAccounts, String value) {
		if (!userAccounts.isBrand())
			throw new IllegalActionException("The account that you are trying to " + value + " is not a brand");

		if (userAccounts.getUserStatus().equals(UserStatus.REGISTERED))
			throw new IllegalActionException("The account that you are trying to " + value + " is not verified");
	}

	@Override
	public List<BrandDetails> getBrandList() {
		Set<BrandDetails> brandDetailsSlice = adminManagementDao.getBrandDetailsByUserStatus(
				Sets.newHashSet(UserStatus.REGISTERED, UserStatus.PENDING, UserStatus.APPROVED, UserStatus.DECLINED, UserStatus.INACTIVE));

		if (!brandDetailsSlice.isEmpty())
			return Lists.newArrayList(brandDetailsSlice);
		else
			return Lists.newArrayList();
	}

	@Override
	public List<SubscriptionMaster> getSubscriptions() {
		return adminManagementDao.getAllSubscriptionMaster().stream().filter(s -> s.isActive()).collect(Collectors .toList());
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void approveBrand(UUID brandId, SubscriptionType subscriptionType, UUID subscriptionId) {
		Optional<UserAccounts> optionalUserAccounts = adminManagementDao.getUserAccounts(brandId.toString());

		if (optionalUserAccounts.isPresent()) {
			UserAccounts userAccounts = optionalUserAccounts.get();
			validateBrandStatus(userAccounts, "approve");
			BrandSubscriptions brandSubscriptions = createSubscription(brandId, subscriptionId, subscriptionType,
					userAccounts);
			updateApproveStatus(userAccounts, brandSubscriptions);
		} else
			throw new IllegalActionException("The account that you are trying to approve is not found");
	}

	private BrandSubscriptions createSubscription(UUID brandId, UUID subscriptionId, SubscriptionType subscriptionType,
			UserAccounts userAccounts) {
		SubscriptionMaster master = adminManagementDao.getSubscriptionMaster(subscriptionId, subscriptionType);
		Optional<BrandSubscriptions> optionalOldBrandSubscriptions = adminManagementDao.getBrandSubscriptionsByBrandIdAndIsActive(brandId);

		BrandSubscriptions brandSubscriptions = BrandSubscriptions.builder().brandName(userAccounts.getBrandName())
				.credits(master.getCredits()).renewable(master.isRenewable()).resetCredit(master.isResetCredit())
				.subscriptionDate(new Date()).subscriptionName(master.getSubscriptionName())
				.subscriptionType(subscriptionType).validity(master.getValidity())
				.validityType(master.getValidityType()).key(BrandSubscriptionsKey.builder().active(true)
						.brandId(brandId).subscriptionId(subscriptionId).transactionId(UUID.randomUUID()).build())
				.createdAt(new Date()).build();

		if (optionalOldBrandSubscriptions.isPresent()) {
			BrandSubscriptions oldBrandSubscriptions = optionalOldBrandSubscriptions.get();
			BrandSubscriptions updateOldBrandSubscriptions = new BrandSubscriptions(oldBrandSubscriptions);
			updateOldBrandSubscriptions.getKey().setActive(false);
			updateOldBrandSubscriptions.setModifiedAt(new Date());
			adminManagementDao.deleteBrandSubscriptions(oldBrandSubscriptions);
			adminManagementDao.saveBrandSubscriptions(updateOldBrandSubscriptions);
		}
		adminManagementDao.saveBrandSubscriptions(brandSubscriptions);

		adminManagementDao.saveBrandCredits(BrandCredits.builder().brandId(brandId).credits(master.getCredits())
				.createdAt(new Date()).validUpto(Date.from(getLastDateOfSubscription(brandSubscriptions).toInstant()))
				.build());

		return brandSubscriptions;
	}

	private void updateApproveStatus(UserAccounts userAccounts, BrandSubscriptions brandSubscriptions) {

		Optional<BrandDetails> optionalBrandDetails = adminManagementDao.getBrandDetailsByUserStatusAndUserId(
				Sets.newHashSet(UserStatus.APPROVED, UserStatus.DECLINED, UserStatus.INACTIVE, UserStatus.PENDING),
				UUID.fromString(userAccounts.getUserId()));

		if (optionalBrandDetails.isPresent()) {
			BrandDetails brandDetails = optionalBrandDetails.get();
			BrandDetails updateBrandDetails = objectMapper.convertValue(SerializationUtils.clone(brandDetails),
					BrandDetails.class);
			updateBrandDetails.setSubscriptionId(brandSubscriptions.getKey().getSubscriptionId());
			updateBrandDetails.setSubscriptionType(brandSubscriptions.getSubscriptionType());
			updateBrandDetails.setSubscriptionName(brandSubscriptions.getSubscriptionName());
			updateBrandDetails.setValidUpto(Date.from(getLastDateOfSubscription(brandSubscriptions).toInstant()));
			updateBrandDetails.setKey(
					objectMapper.convertValue(SerializationUtils.clone(brandDetails.getKey()), UserDetailsKey.class));
			updateBrandDetails.getKey().setUserStatus(UserStatus.APPROVED);
			updateBrandDetails.setModifiedAt(new Date());

			adminManagementDao.deleteBrandDetails(brandDetails);
			adminManagementDao.saveBrandDetails(updateBrandDetails);

			userAccounts.setUserStatus(UserStatus.APPROVED);
			adminManagementDao.saveUserAccounts(userAccounts);
			Boolean newUser;

			if (optionalBrandDetails.get().getKey().getUserStatus().equals(UserStatus.PENDING))
				newUser = true;
			else
				newUser = false;

			if (brandSubscriptions.getSubscriptionName().equals("Trial"))
				mailService.sendApprovedMailForTrail(userAccounts, newUser);
			else if (brandSubscriptions.getSubscriptionName().equals("Free_Account"))
				mailService.sendApprovedMailForSmartfluenceFree(userAccounts, newUser);
			else
				mailService.sendApprovedMailForSmartfluence(userAccounts, newUser);
		}
	}

	private ZonedDateTime getLastDateOfSubscription(BrandSubscriptions subscriptions) {
		Date date = subscriptions.getModifiedAt() == null ? subscriptions.getCreatedAt()
				: subscriptions.getModifiedAt();
		ZonedDateTime zoneDateTime = date.toInstant().atZone(ZoneId.systemDefault()).withHour(0).withMinute(0)
				.withSecond(0).withNano(0);
		switch (subscriptions.getValidityType()) {
		case DAY:
			zoneDateTime = zoneDateTime.plusDays(subscriptions.getValidity());
			break;
		case MONTH:
			zoneDateTime = zoneDateTime.plusMonths(subscriptions.getValidity());
			break;
		case YEAR:
			zoneDateTime = zoneDateTime.plusYears(subscriptions.getValidity());
			break;
		case HOUR:
			zoneDateTime = zoneDateTime.plusHours(subscriptions.getValidity());
			break;
		case MINUTE:
			zoneDateTime = zoneDateTime.plusMinutes(subscriptions.getValidity());
			break;
		default:
			break;
		}
		return zoneDateTime;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void declineBrand(UUID brandId) {
		Optional<UserAccounts> brandDetails = adminManagementDao.getUserAccounts(brandId.toString());

		if (brandDetails.isPresent()) {
			UserAccounts userAccounts = brandDetails.get();
			validateBrandStatus(userAccounts, "decline");
			updateDeclineStatus(userAccounts);
		} else
			throw new IllegalActionException("The account that you are trying to decline is not found");
	}

	private void updateDeclineStatus(UserAccounts userAccounts) {

		Optional<BrandDetails> optionalBrandDetails = adminManagementDao.getBrandDetailsByUserStatusAndUserId(
				Sets.newHashSet(UserStatus.APPROVED, UserStatus.DECLINED, UserStatus.INACTIVE, UserStatus.PENDING),
				UUID.fromString(userAccounts.getUserId()));
		if (optionalBrandDetails.isPresent()) {
			BrandDetails brandDetails = optionalBrandDetails.get();
			BrandDetails updateBrandDetails = objectMapper.convertValue(SerializationUtils.clone(brandDetails),
					BrandDetails.class);

			updateBrandDetails.getKey().setUserStatus(UserStatus.DECLINED);
			if (!brandDetails.getKey().getUserStatus().equals(updateBrandDetails.getKey().getUserStatus())) {
				adminManagementDao.deleteBrandDetails(brandDetails);
				adminManagementDao.saveBrandDetails(updateBrandDetails);
			}
		}
		userAccounts.setUserStatus(UserStatus.DECLINED);
		adminManagementDao.saveUserAccounts(userAccounts);
		mailService.sendDeclinedMail(userAccounts);
	}

	@Override
	public void saveAboutYou(@Valid AboutYouModal aboutYouModal) {

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendInvitation(String[] brandIds) {
		for (String brandId : brandIds) {
			UUID inviteId = UUID.randomUUID();
			Optional<UserAccounts> optionalUserAccounts = adminManagementDao.getUserAccounts(brandId);
			if (optionalUserAccounts.isPresent()) {
				UserAccounts userAccounts = optionalUserAccounts.get();
				Optional<BrandDetails> optionalBrandDetails = adminManagementDao.getBrandDetails(new UserDetailsKey(userAccounts.getUserStatus()
						, UUID.fromString(userAccounts.getUserId())));
				if (optionalBrandDetails.isPresent()) {
					BrandDetails brandDetails = optionalBrandDetails.get();
					userAccounts.setInviteId(inviteId.toString());
					userAccounts.setInviteStatus(InviteStatus.INVITED);
					adminManagementDao.saveUserAccounts(userAccounts);
					brandDetails.setInviteStatus(userAccounts.getInviteStatus());
					adminManagementDao.saveBrandDetails(brandDetails);
					mailService.sendInviteMail(userAccounts);
				}
			}
		}
	}

	@Override
	public ResponseEntity<Object> updateDb() {
		DatabaseUpdateHistory databaseUpdateHistory = adminManagementDao.saveInDBHistory();
		Thread thread = new Thread(() -> updateDatabase(databaseUpdateHistory.getId()));
		thread.start();
		return ResponseEntity.ok(
				DataBaseHistoryModal.builder().startDate(DATE_FORMATTER.format(databaseUpdateHistory.getStartDate()))
						.id(databaseUpdateHistory.getId()).status(databaseUpdateHistory.getStatus().name())
						.endDate(databaseUpdateHistory.getEndDate() != null
								? DATE_FORMATTER.format(databaseUpdateHistory.getEndDate())
								: "NA")
						.build());
	}

	private void updateDatabase(UUID updateId) {
		log.info("Database Update Started for the id {} at {}", updateId, DATE_FORMATTER.format(new Date()));
		List<InstagramDemographics> instagramDemographics = adminManagementDao.getInfluencers();
		List<InstagramContent> contents = new ArrayList<InstagramContent>();
		try {
			List<String[]> entries = new LinkedList<>();
			instagramDemographics.forEach(e -> {
				try {
					List<InstagramContent> instagramContents = adminManagementDao
							.getInstagramContent(e.getInfluencerHandle());
					log.info("Getting DeepSocial Data for influencer Handle {}", e.getInfluencerHandle());
					RawInstagramUserInfo rawUserInfo = socialDataProxy.getInstagramUserInfo(e.getInfluencerHandle().substring(1));
					RawInstagramUserFeed rawUserFeed = socialDataProxy.getInstagramUserFeed(String.valueOf(rawUserInfo.getUser().getPk()),
							null);
					List<RawInstagramUserFeedItems> rawUserFeedItems = rawUserFeed.getItems();
					e.setProfileImage(rawUserInfo.getUser().getProfilePicUrl());

					for (int i = 0; i < instagramContents.size(); i++) {
						if (i >= rawUserFeed.getItems().size()) {
							break;
						}
						RawInstagramUserFeedItems rawUserFeedItem = rawUserFeedItems.get(i);
						InstagramContent instagramContent = instagramContents.get(i);
						instagramContent.setValue(rawUserFeedItem.getDisplayUrl());
					}
					contents.addAll(instagramContents);
				} catch (Exception e2) {
					String[] newRow = { e.getInfluencerHandle(), e.getUserId().toString(), e2.getLocalizedMessage() };
					entries.add(newRow);
				}
			});

			adminManagementDao.updateDB(instagramDemographics, contents, updateId);
			mailService.sendUpdateCompletedMail(createCsvFile(entries));
			log.info("Database Update Completed for the id {} at {}", updateId, DATE_FORMATTER.format(new Date()));
		} catch (IOException e1) {
			log.info("Error in updating the database for the id {}", updateId);
			e1.getStackTrace();
		}
	}

	private File createCsvFile(List<String[]> entries) throws IOException {
		String[] header = { "InfluencerHandle", "InfluencerId", "ErrorMessage" };
		File tempCSVFile = File.createTempFile("failedInfluencers", ".csv");
		CSVWriter writer = new CSVWriter(new FileWriter(tempCSVFile));
		entries.add(header);
		Collections.reverse(entries);
		writer.writeAll(entries);
		writer.flush();
		writer.close();
		return tempCSVFile;
	}

	@Override
	public List<DataBaseHistoryModal> getDBUpdateHistory() {
		List<DatabaseUpdateHistory> databaseUpdateHistoryList = adminManagementDao.getDBUpdateHistory();
		List<DataBaseHistoryModal> dataBaseHistoryModalList = new LinkedList<>();
		if (!databaseUpdateHistoryList.isEmpty()) {
			for (DatabaseUpdateHistory dataBaseHistory : databaseUpdateHistoryList) {
				String startDate = DATE_FORMATTER.format(dataBaseHistory.getStartDate());
				String endDate = dataBaseHistory.getEndDate() != null
						? DATE_FORMATTER.format(dataBaseHistory.getEndDate())
						: "NA";
				DataBaseHistoryModal dataBaseHistoryModal = new DataBaseHistoryModal(dataBaseHistory.getId(), startDate,
						endDate, dataBaseHistory.getStatus().name());
				dataBaseHistoryModalList.add(dataBaseHistoryModal);
			}
		}
		return dataBaseHistoryModalList;
	}

	@Override
	public ResponseEntity<Object> checkDbUpdateStatus() {
		List<DatabaseUpdateHistory> databaseUpdateHistories = adminManagementDao.getDBUpdateHistory();
		for (DatabaseUpdateHistory databaseUpdateHistory : databaseUpdateHistories) {
			if (databaseUpdateHistory.getStatus().equals(DatabaseUpdateStatus.INPROGRESS))
				return ResponseEntity.ok(true);
		}
		return ResponseEntity.ok(false);
	}

	@Override
	public ResponseEntity<String> exportAdminData() {
		File csvFile = new File(getFileName(BRAND_REPORT));
		List<BrandDetails> brandDetailsList = brandDetailsRepo.findAll();
		List<UserAccounts> userAccountsList = userAccountsRepo.findAllUserAccounts();

		try (FileWriter out = new FileWriter(csvFile);
				CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(ADMIN_MODULE_HEADERS))) {
			for (int i = 0; i < brandDetailsList.size(); i++) {
				BrandDetails brandDetails = brandDetailsList.get(i);

				for (int j = 0; j < userAccountsList.size(); j++) {
					UserAccounts userAccounts = userAccountsList.get(j);

					if (brandDetails.getKey().getUserId().toString().equals(userAccounts.getUserId())) {
						printer.printRecord(userAccounts.getBrandName(), userAccounts.getFirstName(), userAccounts.getLastName(),
								userAccounts.getEmail(), userAccounts.getUserStatus(), brandDetails.getSubscriptionName(),
								getDate(userAccounts.getCreatedAt()));
						break;
					}
				}
			}
			printer.flush();
			log.info("File path is {}", csvFile.getAbsolutePath());
			return ResponseEntity.ok(BRAND_REPORT);
		}  catch (IOException e) {
			log.error("Error occurred due to {}", e);
			return ResponseEntity.badRequest().build();
		}
	}

	@Override
	public ResponseEntity<Object> exportReportData(ReportRequestModal reportRequestModal) {
		ReportResponseModalBuilder builder = ReportResponseModal.builder();

		if (reportRequestModal.getReportType().equals("usage_history") || reportRequestModal.getReportType().equals("mail_history"))
			return generateReportData(builder, reportRequestModal);
		else if (reportRequestModal.getReportType().equals("search_history"))
			return generateSearchHistoryReport(builder, reportRequestModal);
		else
			return new ResponseEntity<>(builder.status(403).message("Please select reportType").build(), HttpStatus.OK);
	}

	private ResponseEntity<Object> generateReportData(ReportResponseModalBuilder builder, ReportRequestModal reportRequestModal) {
		String report = reportRequestModal.getReportType().equals("usage_history")
				? USAGE_REPORT : MAIL_HISTORY_REPORT;
		String[] reportHeaders = reportRequestModal.getReportType().equals("usage_history")
				? USAGE_REPORT_HEADERS : MAIL_HISTORY_REPORT_HEADERS;

		File file = new File(getFileName(report));
		try (FileWriter out = new FileWriter(file); CassandraDatabaseConnector db = new CassandraDatabaseConnector();
				CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(reportHeaders))) {
			Boolean dataAvaliable = false;
			ResultSet resultSet = getCassandraData.getData(db, formQueryString(reportRequestModal));

			if (reportRequestModal.getReportType().equals("usage_history"))
				dataAvaliable = setUsageReportData(printer, resultSet);
			else
				dataAvaliable = setMailHistoryData(printer, resultSet);

			printer.flush();
			log.info("File path is {}", file.getAbsolutePath());

			if (!dataAvaliable)
				return new ResponseEntity<>(builder.status(403).message("No data found for this criteria").build(), HttpStatus.OK);
			else
				return new ResponseEntity<>(builder.status(200).flag(report).message("File is ready to download").build(), HttpStatus.OK);
		}  catch (IOException e) {
			log.error("Error occurred due to {}", e);
			return ResponseEntity.badRequest().build();
		}
	}

	private Boolean setUsageReportData(CSVPrinter printer, ResultSet resultSet) throws IOException {
		Boolean dataAvaliable = false;
		for (Row data : resultSet) {
			dataAvaliable = true;
			printer.printRecord(data.getString("brand_name"), data.getString("influencer_deep_social_id"), data.getString("influencer_username"),
					data.getString("platform"), data.getString("view_type"), data.getBool("is_new"), getFormatedDate(data.getTimestamp("created_at")));
		}
		return dataAvaliable;
	}

	private Boolean setMailHistoryData(CSVPrinter printer, ResultSet resultSet) throws IOException {
		Boolean dataAvaliable = false;
		for (Row data : resultSet) {
			dataAvaliable = true;
			printer.printRecord(data.getString("brand_name"), data.getString("mail_body"), data.getString("mail_subject"),
					data.getString("mail_cc"), data.getString("mail_from"), data.getString("mail_to"), getMappingStatus(data.getInt("status")),
					getFormatedDate(data.getTimestamp("created_at")));
		}
		return dataAvaliable;
	}

	private ResponseEntity<Object> generateSearchHistoryReport(ReportResponseModalBuilder builder, ReportRequestModal reportRequestModal) {
		File file = new File(getFileName(SEARCH_HISTORY_REPORT));
		try (FileOutputStream fileOut = new FileOutputStream(file);
				CassandraDatabaseConnector db = new CassandraDatabaseConnector(); Workbook workbook = new XSSFWorkbook();) {
			int rowNum = 1;
			Boolean dataAvaliable = false;
			Sheet searchHistorySheet = null;
			Sheet mappingInfluencerSheet = null;
			Set<UUID> searchId = new HashSet<UUID>();
			ResultSet resultSet = getCassandraData.getData(db, formQueryString(reportRequestModal));

			for (Row data : resultSet) {
				dataAvaliable = true;

				if (rowNum == 1)
					searchHistorySheet = createHeaders("Search_History", SEARCH_HISTORY_REPORT_HEADERS, workbook);
				org.apache.poi.ss.usermodel.Row row = searchHistorySheet.createRow(rowNum++);

				if (data.getBool("is_influencer_mapped"))
					searchId.add(data.getUUID("search_id"));
				setSearchHistoryData(data, row);
			}

			if (!searchId.isEmpty())
				getMappingInfluencersData(workbook, mappingInfluencerSheet, searchId);

			if (!dataAvaliable)
				return new ResponseEntity<>(builder.status(403).message("No data found for this criteria").build(), HttpStatus.OK);
			else {
				workbook.write(fileOut);
				return new ResponseEntity<>(builder.status(200).flag(SEARCH_HISTORY_REPORT).message("File is ready to download").build(), HttpStatus.OK);
			}
		} catch (Exception e) {
			log.error("Error occurred due to {}", e);
			return ResponseEntity.badRequest().build();
		}
	}

	private void getMappingInfluencersData(Workbook workbook, Sheet mappingInfluencerSheet, Set<UUID> searchId) {
		int rowNum = 1;
		for (UUID uuid : searchId) {
			List<ReportSearchHistory> reportSearchHistory = adminManagementDao.findAllBySearchId(uuid);

			for (ReportSearchHistory reportData : reportSearchHistory) {
				if (rowNum == 1)
					mappingInfluencerSheet = createHeaders("Mapping_Influencers", MAPPING_INFLUENCERS_HEADERS, workbook);
				org.apache.poi.ss.usermodel.Row row = mappingInfluencerSheet.createRow(rowNum++);
				setMappingInfluencerData(reportData, row);
			}
		}
	}

	private Sheet createHeaders(String sheetName, String[] headers, Workbook workbook) {
		Sheet sheet = workbook.createSheet(sheetName);
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(headers[i]);
			cell.setCellStyle(headerCellStyle);
		}
		return sheet;
	}

	private void setSearchHistoryData(Row data, org.apache.poi.ss.usermodel.Row row) {
		row.createCell(0).setCellValue(data.getUUID("search_id").toString());
		row.createCell(1).setCellValue(data.getString("brand_name"));
		row.createCell(2).setCellValue(data.getString("audience_age"));
		row.createCell(3).setCellValue(data.getString("audience_gender"));
		row.createCell(4).setCellValue(data.getString("audience_industry"));
		row.createCell(5).setCellValue(data.getString("audience_location"));
		row.createCell(6).setCellValue(data.getString("bio_text"));
		row.createCell(7).setCellValue(data.getString("comparison_profile"));
		row.createCell(8).setCellValue(data.getString("engagements_range"));
		row.createCell(9).setCellValue(data.getString("followers_range"));
		row.createCell(10).setCellValue(data.getString("influencer_age"));
		row.createCell(11).setCellValue(data.getString("influencer_gender"));
		row.createCell(12).setCellValue(data.getString("influencer_industry"));
		row.createCell(13).setCellValue(data.getString("influencer_location"));
		row.createCell(14).setCellValue(data.getBool("is_verified"));
		row.createCell(15).setCellValue(data.getString("key_words"));
		row.createCell(16).setCellValue(data.getString("platfrom"));
		row.createCell(17).setCellValue(data.getString("sort_by"));
		row.createCell(18).setCellValue(data.getBool("sponsored_posts"));
		row.createCell(19).setCellValue(data.getString("with_contact"));
		row.createCell(20).setCellValue(getFormatedDate(data.getTimestamp("created_at")));
	}

	private void setMappingInfluencerData(ReportSearchHistory data, org.apache.poi.ss.usermodel.Row row) {
		row.createCell(0).setCellValue(data.getSearchId().toString());
		row.createCell(1).setCellValue(data.getBrandName());
		row.createCell(2).setCellValue(data.getInfluencerDeepSocialId());
		row.createCell(3).setCellValue(data.getInfluencerUsername());
		row.createCell(4).setCellValue(data.getPlatform().name());
		row.createCell(5).setCellValue(data.getViewType().name());
		row.createCell(6).setCellValue(data.getKey().getIsNew());
		row.createCell(7).setCellValue(getFormatedDate(data.getKey().getCreatedAt()));
	}

	private String formQueryString(ReportRequestModal reportRequestModal) {
		String fromDate = reportRequestModal.getFromDate() + "T00:00:00";
		String toDate = reportRequestModal.getToDate() + "T23:59:59";
		String queryString = " where created_at >= '" + fromDate + "' and created_at <= '" + toDate + "' allow filtering";

		if (reportRequestModal.getReportType().equals("usage_history"))
			return "select * from report_search_history" + queryString;
		else if (reportRequestModal.getReportType().equals("mail_history"))
			return "select * from mail_history" + queryString;
		else if (reportRequestModal.getReportType().equals("search_history"))
			return "select * from brand_search_history" + queryString;
		return null;
	}

	@Override
	public ResponseEntity<byte[]> downloadReport(String flag) {
		String fileName = getFileName(flag);
		File file = new File(fileName);

		if (file.exists()) {
			try {
				byte[] csv = Files.readAllBytes(file.toPath());
				Files.delete(file.toPath());

				return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
						.header(HttpHeaders.SET_COOKIE, "fileDownload=true; Path=/").body(csv);
			} catch (IOException e) {
				e.getStackTrace();
			}
		}
		return ResponseEntity.notFound().build();
	}

	private String getFileName(String flag) {
		switch (flag) {
		case BRAND_REPORT:
			return BRAND_REPORT + CSV_EXTENTION;
		case USAGE_REPORT:
			return USAGE_REPORT + CSV_EXTENTION;
		case MAIL_HISTORY_REPORT:
			return MAIL_HISTORY_REPORT + CSV_EXTENTION;
		case SEARCH_HISTORY_REPORT:
			return SEARCH_HISTORY_REPORT + XLSX_EXTENTION;
		default:
			break;
		}
		return null;
	}

	private String getMappingStatus(int status) {
		switch (status) {
		case 1:
			return "BID";
		case 2:
			return "BULK";
		case 4:
			return "PROMOTE INITIAL OUTREACH";
		case 8:
			return "PROMOTE APPROVED";
		case 16:
			return "PROMOTE REJECTED";
		case 32:
			return "PROMOTE BRAND NOTIFICATION";
		case 64:
			return "CHANGE TARGETING DETAILS";
		case 128:
			return "INFLUENCER REPORT";
		default:
			break;
		}
		return null;
	}

	private String getDate(Object value) {
		return String.valueOf(value).split(" ")[0];
	}

	private String getFormatedDate(Date value) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(value);
		return cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
	}

	public static void main(String[] args) throws ParseException {
		String dateStr = "Mon Oct 04 18:19:18 IST 2021";
		SimpleDateFormat format = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		Date check = format.parse(dateStr);


		Calendar cal = Calendar.getInstance();
		cal.setTime(check);
		String formatedDate = cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);

	}
}