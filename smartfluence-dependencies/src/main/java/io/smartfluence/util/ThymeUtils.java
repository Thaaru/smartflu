package io.smartfluence.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ThymeUtils {

	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##", DecimalFormatSymbols.getInstance());

	public static String formatNumber(Object value) {
		Double temp = Double.valueOf(value.toString());
		String prefix;
		if (temp < 1000) {
			prefix = "";
		} else if (temp < 1000000) {
			prefix = "K";
		} else if (temp < 1000000000) {
			prefix = "M";
		} else {
			prefix = "B";
		}
		switch (prefix) {
		case "K":
			temp = (temp / 1000.0);
			break;
		case "M":
			temp = (temp / 1000000.0);
			break;
		case "B":
			temp = (temp / 1000000000.0);
			break;
		default:
			break;
		}
		return DECIMAL_FORMAT.format(temp) + prefix;
	}

	public static String getDate(Object value) {
		String date [] = String.valueOf(value).split("T");

		return date[0];
	}

	public static String getPlatform(Object value) {
		String platform = null;
		switch (value.toString()) {
		case "instagram":
			platform = "Instagram";
			break;
		case "youtube":
			platform = "YouTube";
			break;
		case "tiktok":
			platform = "TikTok";
			break;
		default:
			break;
		}
		return platform;
	}

	private ThymeUtils() {

	}
}