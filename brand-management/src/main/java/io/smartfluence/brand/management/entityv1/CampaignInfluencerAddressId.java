package io.smartfluence.brand.management.entityv1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;


    @Embeddable
    @EqualsAndHashCode
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class CampaignInfluencerAddressId implements Serializable {
        private String campaignId;
        private String influencerId;
        @Enumerated(EnumType.STRING)
        @Column(name = "address_type")
        private AddressType addressType;
    }

