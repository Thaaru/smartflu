package io.smartfluence.cassandra.entities;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import io.smartfluence.cassandra.entities.primary.PromoteInfluencersDataKey;
import io.smartfluence.constants.Platforms;
import io.smartfluence.constants.PromoteInfluencerStatus;
import io.smartfluence.modal.Copyable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table("promote_influencers_data")
public class PromoteInfluencersData implements Copyable {

	private static final long serialVersionUID = 6735450605144738323L;

	@PrimaryKey
	private PromoteInfluencersDataKey key;

	@Column("influencer_name")
	private String influencerName;

	@Column("influencer_email")
	private String influencerEmail;

	@Column("url")
	private String url;

	@Column("profile_image")
	private String profileImage;

	@Column("account_type")
	private Integer accountType;

	@Column("followers")
	private Double followers;

	@Column("engagements")
	private Double engagements;

	@Column("engagement_score")
	private Double engagementScore;

	@Column("audience_credibility")
	private Double audienceCredibility;

	@Indexed
	@Column("status")
	private PromoteInfluencerStatus status;

	@Column("influencer_sf_id")
	private UUID influencerSfId;

	@Column("platfrom")
	private Platforms platform;

	@Indexed
	@Column("created_at")
	private Date createdAt;

	@Column("modified_at")
	private Date modifiedAt;
}