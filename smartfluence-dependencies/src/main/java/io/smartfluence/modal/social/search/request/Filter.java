package io.smartfluence.modal.social.search.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Filter {

	@JsonProperty("audience_age")
	private List<CodeWeight> audienceAge;

	@JsonProperty("audience_brand_category")
	private List<IndustryAndLocation> audienceBrand;

	@JsonProperty("audience_gender")
	private CodeWeight audienceGender;

	@JsonProperty("audience_geo")
	private List<IndustryAndLocation> audienceGeo;

	@JsonProperty("audience_relevance")
	private ComparisonProfile comparisonProfile;

	@JsonProperty("engagements")
	private Range engagements;

	@JsonProperty("views")
	private Range views;

	@JsonProperty("followers")
	private Range followers;

	@JsonProperty("brand_category")
	private Integer[] influencerBrand;

	@JsonProperty("gender")
	private CodeWeight influencerGender;

	@JsonProperty("age")
	private Range influencerAge;

	@JsonProperty("geo")
	private List<IndustryAndLocation> influencerGeo;

	@JsonProperty("lang")
	private List<CodeWeight> influencerlang;

	@JsonProperty("keywords")
	private String keywords;

	@JsonProperty("relevance")
	private Relevance relevance;

	@JsonProperty("text")
	private String profileBio;

	@JsonProperty("is_hidden")
	private Boolean hidden;

	@JsonProperty("is_verified")
	private Boolean verified;

	@JsonProperty("account_type")
	private Integer[] accountType;

	@JsonProperty("has_ads")
	private Boolean sponsoredPost;

	@JsonProperty("with_contact")
	private List<WithContact> withContact;

	@JsonProperty("audience_credibility")
	private Double audienceCredibility;

	@JsonProperty("audience_credibility_class")
	private String[] audienceCredibilityClass;

	@JsonProperty("list")
	private List<CustomList> customList;

	@JsonProperty("last_posted")
	private Integer lastPosted;

	@JsonProperty("has_audience_data")
	private Boolean hasAudienceData;
}