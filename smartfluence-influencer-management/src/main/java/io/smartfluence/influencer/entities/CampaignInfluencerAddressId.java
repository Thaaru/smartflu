package io.smartfluence.influencer.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Embeddable
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignInfluencerAddressId implements Serializable{
    private String campaignId;
    private String influencerId;
    @Enumerated(EnumType.STRING)
    @Column(name = "address_type")
    private AddressType addressType;
}
