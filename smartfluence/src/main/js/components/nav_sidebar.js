import axios from "axios";
import React, { useState } from "react";
import { Button, Container, Offcanvas } from "react-bootstrap";
import { BsFillPeopleFill, BsList, BsPower } from 'react-icons/bs'
import { IoIosCloseCircle } from "react-icons/io";
import { toast } from "react-toastify";
import { login, logout } from "../utils/http_urls";
import { BiMenuAltLeft } from "react-icons/bi";
import { SMARTFLUENCE_LOGO } from "../utils/string_constants";
export default function NavSideBarComponent({ children }) {
  const [show, setShow] = useState(false);
  const closeSidebar = () => setShow(false);
  const showSidebar = () => setShow(true);

  const logoutHandler = async () => {
    await axios.post(logout()).then(() => {
      window.location.assign(login());
    }).catch((e) => {
      toast.error("Something went wrong try again later", { delay: 1000 })
    })
  }

  return (
    <>
      <nav class="navbar navbar-expand-lg" style={{ "backgroundColor": "#2F008D", "height": "65px" }}>
        <div class="row" style={{ "justifyContent": "space-between", "width": "100%", "paddingLeft": "10px" }}>
          <BiMenuAltLeft onClick={showSidebar} size={25} color="white" />
          <div className="heading" style={{ color: 'white' }}>Smartfluence</div>
          <div></div>
        </div>
      </nav>
      <Offcanvas placement="start" backdrop='false' show={show} onHide={closeSidebar}>
        <Offcanvas.Header >
          <Offcanvas.Title >
            <div className="mt-2 col-md-12">
              <div className="row" style={{ justifyContent: "space-between" }}>
                <img src={SMARTFLUENCE_LOGO} height="40px" />
                <IoIosCloseCircle size={30} onClick={closeSidebar} />
              </div>
            </div>
          </Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body >
          <div  >
            <div>
              <div class="col-md-6 mb-4" style={{ padding: 0 }}>
                <div class="list-group-flush" >
                  <div class="list-group-item">
                    <p class="mb-2 normalText primaryColor" style={{ "cursor": "pointer", "fontSize": "20px" }}><BsFillPeopleFill size={25} />{"   "}Partnerships</p>
                  </div>
                  <div class="list-group-item">
                    <p class="mb-0 normalText" style={{ "cursor": "pointer", "fontSize": "20px" }} onClick={logoutHandler}> <BsPower size={25} />{"   "}Log Out</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Offcanvas.Body>
        <div className="footer" style={{ position:'absolute', bottom:'10px', right:'24px' }}>
                <li style={{ "listStyleType": "none", textAlign:'right' }}>Powered by <img src={SMARTFLUENCE_LOGO} height="25px" /></li>
              </div>
      </Offcanvas>
    </>
  );
}