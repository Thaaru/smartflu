$('.mini-loader').hide();
(function() {
	'use strict';
	var brandToClass = {
		'unknown' : '<i class="cc cc-card"></i>',
		'visa' : '<i class="cc cc-visa"></i>',
		'mastercard' : '<i class="cc cc-mastercard"></i>',
		'unionpay' : '<i class="cc cc-unionpay"></i>',
		'amex' : '<i class="cc cc-amex"></i>',
		'discover' : '<i class="cc cc-discover"></i>',
		'diners' : '<i class="cc cc-diners"></i>',
		'jcb' : '<i class="cc cc-jcb"></i>',
		'error' : '<i class="cc cc-error"></i>',
	}

	function setBrandIcon(brand) {
		var brandIconElement = document.getElementById('brand-icon');
		var pfClass = brandToClass['unknown'];

		if (brand in brandToClass) {
			pfClass = brandToClass[brand];
		}
		$(brandIconElement).html(pfClass);
	}

	if ($('.sf-stripe').length > 0) {
		var elements = stripe.elements({
			locale : 'auto'
		});

		var inputs = document.querySelectorAll('.stripe-card .input');
		Array.prototype.forEach.call(inputs, function(input) {
			input.addEventListener('focus', function() {
				input.classList.add('focused');
			});
			input.addEventListener('blur', function() {
				input.classList.remove('focused');
			});
			input.addEventListener('keyup', function() {
				if (input.value.length === 0) {
					input.classList.add('empty');
				} else {
					input.classList.remove('empty');
				}
			});
		});

		var elementStyles = {
			base : {
				color : '#32325D',
				fontWeight : 500,
				fontSize : '16px',
				fontSmoothing : 'antialiased',

				'::placeholder' : {
					color : '#b8b8b8',
				},
				':-webkit-autofill' : {
					color : '#e39f48',
				},
			},
			invalid : {
				color : '#E25950',

				'::placeholder' : {
					color : '#b8b8b8',
				},
			},
		};

		var elementClasses = {
			focus : 'focused',
			empty : 'empty',
			invalid : 'invalid',
		};

		var cardNumber = elements.create('cardNumber', {
			style : elementStyles,
			classes : elementClasses
		});
		cardNumber.mount('#stripe-card');

		var cardExpiry = elements.create('cardExpiry', {
			style : elementStyles,
			classes : elementClasses
		});
		cardExpiry.mount('#stripe-expiry');
		var cardCvc = elements.create('cardCvc', {
			style : elementStyles,
			classes : elementClasses
		});
		cardCvc.mount('#stripe-cvc');

		cardNumber.on('change', function(event) {
			console.log(event);
			if (event.brand) {
				setBrandIcon(event.brand);
			}
			if (event.error) {
				setBrandIcon('error');
			}
		});

		$('#btn_savecard').on('click', function(e) {

			$('.mini-loader').show();
			var owner = {
				owner : {
					name : $('#card-name').val(),
				},
			};
			stripe.createSource(cardNumber, owner).then(function(res) {
				if (res.error) {
					$('.mini-loader').hide();
					ERROR(res.error.message);
				} else {
					stripeTokenHandler(res.source);
				}
			})
		});
	}

	$('.card-brand-data').each(function() {
		var data = $(this).attr('data');
		if (data != null) {
			var brand = data.toLowerCase();
			$(this).html('<i class="cc cc-' + brand + '"></i>');
		}
	});
})();

$('.btn_removecard').on('click', function(e) {
	removeCard($(this));
});

$('.btn_carddefault').on('click', function(e) {
	e.preventDefault();
	makeDefault($(this));
});

function removeCard(obj) {
	var cardId = obj.attr('data-card-id');
	$('#paymentModalLabel').html('Remove card');
	$('#paymentModalMsg').html('Are you sure that you want to remove this card?');
	$('#paymentModalConfirm').attr('onclick',"removeCardConfirm('"+cardId+"')");
	$('#paymentModal').modal();
}

function removeCardConfirm(cardId){
	$('.mini-loader').show();
	$.post({
		url : 'cards/remove',
		data : {
			cardId : cardId
		}
	}).done(function(res) {
		SUCCESS('Card removed successfully!');
		setTimeout(function() {
			location.reload();
		}, 1000)
	}).fail(function(res) {
		$('.mini-loader').hide();
		if (!res.responseJSON || res.responseJSON == null) {
			ERROR(res.responseText);
		} else {
			ERROR(res.responseJSON.message);
		}
	});
}

function makeDefault(obj) {
	var cardId = obj.attr('data-card-id');
	$('#paymentModalLabel').html('Default card');
	$('#paymentModalMsg').html('Are you sure that you want to make this card, the default mode of payment?');
	$('#paymentModalConfirm').attr('onclick',"makeDefaultConfirm('"+cardId+"')");
	$('#paymentModal').modal();
}

function makeDefaultConfirm(cardId){
	$('.mini-loader').show();
	$.post({
		url : 'cards/default',
		data : {
			cardId : cardId
		}
	}).done(function(res) {
		SUCCESS('Card changed to default successfully!');
		setTimeout(function() {
			location.reload();
		}, 1000)
	}).fail(function(res) {
		$('.mini-loader').hide();
		if (!res.responseJSON || res.responseJSON == null) {
			ERROR(res.responseText);
		} else {
			ERROR(res.responseJSON.message);
		}
	});
}

function stripeTokenHandler(source) {
	var defaultSource = $('#makeDefault').is(':checked');

	$.post({
		url : 'cards/save',
		data : {
			stripeToken : source.id,
			defaultSource : defaultSource
		}
	}).done(function(res) {
		SUCCESS('Card added successfully!');
		setTimeout(function() {
			location.reload();
		}, 1000)
	}).fail(function(res) {
		$('.mini-loader').hide();
		if (!res.responseJSON || res.responseJSON == null) {
			ERROR(res.responseText);
		} else {
			ERROR(res.responseJSON.message);
		}
	})
}

function getPayment() {

}