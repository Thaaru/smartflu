package io.smartfluence.brand.management.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stripe.exception.StripeException;
import com.stripe.model.ExternalAccount;

import io.smartfluence.ResourceServer;
import io.smartfluence.stripe.StripeManager;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("brand")
public class BrandSettingPaymentRestController {

	@Autowired
	private StripeManager stripeManager;

	@PostMapping(path = "plans/activate/{planId}")
	public ResponseEntity<Object> activate(@PathVariable String planId) {
		log.info(planId);
		return null;
	}

	@PostMapping(path="cards/save", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> saveCard(@RequestParam String stripeToken,
			@RequestParam(defaultValue = "false", required = false) Boolean defaultSource) throws StripeException {
		ExternalAccount account = stripeManager.addCard(ResourceServer.getUserId(), stripeToken,defaultSource);

		if (account == null)
			return ResponseEntity.badRequest().body(stripeToken);

		return ResponseEntity.ok(account.getLastResponse().body());
	}
}