import React, { useState } from "react";
import ColoredLine from "./line_component";
import { PRIMARY_COLOR, SECONDARY_COLOR, STEPS_ACCEPTED_STATUS } from "../utils/string_constants";
import { PoweredBy } from "./powered_by";
import { CProgressBar } from "./progress_bar";
import { loaderMethod } from "./loader";
import { ArrowButton } from "./arrow_button";


export const FooterComponent = ({ data, previousScreenStatus, acceptedStatus, backWardStatus, forwardStatus, callBackfunction, submitStatus, next, back }) => {
    const [loaderComponent, setLoaderComponent] = useState(true);

    const loaderOpen = (status) => {
        if (status == true) {
            setLoaderComponent(status);
        } else {
            setLoaderComponent(status)
        }
    }

    return (
        <>
            <div className="footer">
                <ColoredLine color='secondaryColor' />

                <div className="row" style={{ justifyContent: 'center', alignItems: 'center', }}>
                    <ArrowButton status={backWardStatus} navigator="backward" buttonHandler ={back}/>
                    {
                        submitStatus != null ?
                            <div class="col-4 ">
                                <button type="button" class="btn btn-outline-secondary acceptButton" disabled={previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS || submitStatus != data.maxProductCount ? true : false : false} style={{ borderWidth: "2px", "borderColor": previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS || submitStatus != data.maxProductCount ? SECONDARY_COLOR : PRIMARY_COLOR : SECONDARY_COLOR, color: previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS || submitStatus != data.maxProductCount ? "white" : PRIMARY_COLOR : "white", "backgroundColor": previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS || submitStatus != data.maxProductCount ? SECONDARY_COLOR : null : SECONDARY_COLOR }} onClick={previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS ? null : () => { callBackfunction(data, loaderOpen); } : null}>
                                    {loaderComponent == true ? acceptedStatus == STEPS_ACCEPTED_STATUS ? "Accepted" : "Accept" : loaderMethod()}
                                </button>
                            </div> :
                            <div class="col-4 ">
                                <button type="button" class="btn btn-outline-secondary acceptButton" style={{ borderWidth: "2px", "borderColor": previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS ? SECONDARY_COLOR : PRIMARY_COLOR : SECONDARY_COLOR, color: previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS ? "white" : PRIMARY_COLOR : "white", "backgroundColor": previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS ? SECONDARY_COLOR : null : SECONDARY_COLOR }} onClick={previousScreenStatus == STEPS_ACCEPTED_STATUS ? acceptedStatus == STEPS_ACCEPTED_STATUS ? null : () => { callBackfunction(data, loaderOpen); } : null}>
                                    {loaderComponent == true ? acceptedStatus == STEPS_ACCEPTED_STATUS ? "Accepted" : "Accept" : loaderMethod()}
                                </button>
                            </div>
                    }
                    <ArrowButton status={forwardStatus} navigator="forward" buttonHandler={next}/>
                </div>
                <CProgressBar data={data} />
                <PoweredBy />
            </div>
        </>
    );
}