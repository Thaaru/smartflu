package io.smartfluence.webapp.controller.rest.influencer;


import io.smartfluence.mysql.entities.influencer.FullCampaignInfluencerDetails;
import io.smartfluence.webapp.proxy.InfluencerManagementServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("influencer")
public class InfluencerRestController {

	@Autowired
	private InfluencerManagementServiceProxy influencerManagementServiceProxy;

     @GetMapping("pub")
	 public String influencerConnectView() {
         return influencerManagementServiceProxy.getPubPage();
	 }
	
	@GetMapping("/pub/campaign-influencer-details")
	public ResponseEntity<Object> getCampaignDetails(@RequestParam("token") String emailTokenCode) {
		return influencerManagementServiceProxy.getCampaignInfluencerDetails(emailTokenCode);
	}

	@PutMapping("/pub/campaign-influencer-details")
	public ResponseEntity<Object> updateCampaignDetails(@RequestBody FullCampaignInfluencerDetails fullCampaignInfluencerDetails, @RequestParam("token") String emailTokenCode) {
		return influencerManagementServiceProxy.updateCampaignInfluencerDetails(fullCampaignInfluencerDetails,emailTokenCode);
	}

	@GetMapping("/pub/states/{countryId}")
	public ResponseEntity<Object> getStateList(@PathVariable Long countryId){
		return influencerManagementServiceProxy.getStatesDetails(countryId);
	}

	@GetMapping("/current-partnership")
	public ResponseEntity<Object> getcurrentpartnership() {
		return influencerManagementServiceProxy.getcurrentpartnership();
	}

	@GetMapping("/listHeaders")
	public ResponseEntity<Map<String, String>> listAllHeaders(@RequestHeader Map<String, String> headers) {
		return new ResponseEntity<Map<String, String>>(headers, HttpStatus.OK);
	}
}