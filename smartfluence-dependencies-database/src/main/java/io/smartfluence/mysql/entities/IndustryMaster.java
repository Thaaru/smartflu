package io.smartfluence.mysql.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "industry_master")
public class IndustryMaster implements Serializable {

	private static final long serialVersionUID = -6835298356294860503L;

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "industry")
	private String industry;

	@Column(name = "weight")
	private double weight;

	@Column(name = "status")
	private int status;
}