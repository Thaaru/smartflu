package io.smartfluence.webapp.proxy;

import feign.Headers;
import io.smartfluence.FeignConfiguration;
import io.smartfluence.modal.brand.campaignv1.CampaignEntityV1;
import io.smartfluence.modal.brand.campaignv1.InfluObj;
import io.smartfluence.modal.brand.campaignv1.WorkflowReqObj;
import io.smartfluence.modal.utill.ResponseObj;
import io.smartfluence.webapp.controller.rest.entity.DashBoardModalNew;
import io.smartfluence.webapp.controller.rest.entity.RemoveInfluencerReq;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@FeignClient(name = "smartfluence-brand-management")
@RibbonClient(name = "smartfluence-brand-management")

public interface NewBrandCampManagementServiceProxy {

    @GetMapping("brand/new-campaign")
    ResponseEntity<CampaignEntityV1> getCampaignForm();

    @PostMapping("brand/saveNewCampaign")
    ResponseEntity<Object> saveCampaign(Object object);

    @GetMapping("brand/getCampaignNew")
    ResponseEntity<String> createCampaign();

    @GetMapping("brand/view-campaign-v1")
    ResponseEntity<Object> getCampaignNew();

    @PostMapping("brand/launchNewCampaign")
    ResponseEntity<Object> launchCampaign(Object object);

    @GetMapping("brand/getCampaignById/{campaignId}")
    ResponseEntity<Object> getCampaignById(@PathVariable UUID campaignId);

    @GetMapping("brand/view-campaign-v1/{id}")
    ResponseEntity<CampaignEntityV1> getCampaignEntityByCampaignId(@PathVariable String id);

//    @PostMapping("brand/save-campaign-v1")
//    ResponseEntity<CampaignEntityV1> saveCampaignNew(CampaignEntityV1 campaignEntityV1);

    @DeleteMapping("brand/delete-campaign")
    ResponseEntity<Object> deleteCampaignById(@RequestParam("id") String id,@RequestParam("status") String status);

    @GetMapping("brand/campaign/{campaignId}/influencers")
    ResponseEntity<Object> getAllInfluencerByNewCampaignId(@PathVariable("campaignId") String campaignId);

    @GetMapping("brand/get-new-campaign")
    ResponseEntity<Object> getNewCampaign();

    @GetMapping("brand/get-all-new-campaign")
    ResponseEntity<Object> getAllNewCampaignWithInfluencer();

    @GetMapping("brand/view-campaign-v1/view-influencers")
    public ResponseEntity<Object> getInfluencerByCampaignId(@RequestParam String id,
                                                            @RequestParam List<String> proposalStatus);

    @PostMapping(value = "brand/save-campaign-v1-file",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity<CampaignEntityV1> saveCampaignNewFile(@RequestParam String campaignEntityV1, @RequestPart MultipartFile file);

    @PostMapping(value = "brand/save-campaign-v1",produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CampaignEntityV1> saveCampaignNew(@RequestParam String campaignEntityV1);

//    @PostMapping(value = "brand/save-campaign-v1",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//    ResponseEntity<CampaignEntityV1> saveCampaignNew(@RequestParam("file") MultipartFile file);

    @DeleteMapping("brand/remove-influencer")
    ResponseEntity<Object> removeNewInfluencer(@RequestBody RemoveInfluencerReq removeInfluencerReq);

    @PostMapping("brand/view-campaign/copy-new-influencers")
    ResponseEntity<Object> copyNewInfluencers(@RequestBody DashBoardModalNew dashBoardModalNew);

    @GetMapping("brand/campaign/{campaignId}/influencers/export-campaign-csv-new")
    ResponseEntity<String> exportCampaignCsvNew(@PathVariable UUID campaignId);


    @GetMapping("brand/campaign/{campaignId}/influencers/download-campaign-csv-new/{csvName}")
    ResponseEntity<byte[]> downloadCampaignCsvNew(@PathVariable UUID campaignId, @PathVariable UUID csvName);

    @PostMapping("brand/campaign/{campaignId}/influencers/upload-bulk-influencers-new")
    ResponseEntity<Object> uploadNewInfluencers(@RequestParam String influencerHandles, @RequestParam List<String> workbookSheets,
                                             @PathVariable UUID campaignId);

    @GetMapping("brand/campaign/download-template/upload-influencers-new")
    ResponseEntity<byte[]> downloadNewTemplate();

    @PostMapping("brand/view-campaign-v1/view-influencers/sendproposal")
    public ResponseObj sendProposal(@RequestBody WorkflowReqObj workflowReqObj);

    @PutMapping("brand/view-campaign-v1/view-influencers/updateoffer")
    public ResponseObj updateInfluencerOffers(@RequestBody InfluObj influObj);

    @PutMapping("brand/view-campaign-v1/view-influencers/brandapproval")
    public ResponseObj brandApproval(@RequestBody WorkflowReqObj workflowReqObj);

    @PutMapping("brand/view-campaign-v1/update-campaign-name")
    public ResponseObj updateCampaignName(@RequestParam String campaignId, @RequestParam String campaignName);

    @GetMapping("brand/view-campaign-v1/validate-campaign-name")
    public ResponseObj validateCampaignName(@RequestParam String campaignName,@RequestParam String campaignId);

    @GetMapping(value = "brand/gettermscondition/{filename}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable String filename);

    @DeleteMapping(value = "brand/delete-termscondition")
    public ResponseEntity<ResponseObj> deleteTermsAndCondition
            (@RequestParam String campaignId,@RequestParam long termId);

}
