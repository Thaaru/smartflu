package io.smartfluence.cassandra.entities.primary;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@PrimaryKeyClass
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ReportSearchHistoryKey implements Serializable {

	private static final long serialVersionUID = 1014203030146993280L;

	public ReportSearchHistoryKey(ReportSearchHistoryKey key) {
		this.brandId = key.getBrandId();
		this.isNew = key.getIsNew();
		this.createdAt = key.getCreatedAt();
	}

	@PrimaryKeyColumn(name = "brand_id", type = PrimaryKeyType.PARTITIONED)
	private UUID brandId;

	@PrimaryKeyColumn(name = "is_new", type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private Boolean isNew;

	@Indexed
	@PrimaryKeyColumn(name = "created_at", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private Date createdAt;
}