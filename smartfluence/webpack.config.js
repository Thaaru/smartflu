var path = require('path');
const WatchExternalFilesPlugin = require('webpack-watch-files-plugin').default;

module.exports = {
    mode: 'production',
    devtool: false,
    cache: false,
    entry: './src/main/js/App.js',
    module: {
        rules: [
            {
                test: path.join(__dirname, '.'),
                exclude: [/node_modules/],
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"]
                    }
                }]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif|eot|otf|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {}
                    }
                ]
            }
        ]
    },
    output: {
        path: __dirname,
        filename: './src/main/resources/static/built/bundle.js'
    },
    plugins: [
        new WatchExternalFilesPlugin({
          files: [
            './src/main/js/**/*.js',
          ],
          verbose: true,
        })
      ]
}
