package io.smartfluence.influencer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.smartfluence.influencer.entities.CampaignInfluencerProductId;
import io.smartfluence.influencer.entities.CampaignInfluencerProducts;

import java.util.List;

public interface CampaignInfluencerProductsRepo extends JpaRepository<CampaignInfluencerProducts, CampaignInfluencerProductId>{
 List<CampaignInfluencerProducts> findByCipIdCampaignIdAndCipIdInfluencerId(String campaignId,String influencerId);
   }
