package io.smartfluence.influencer.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "campaign_influencer_offers_v1")
public class CampaignInfluencerOffers {
   @EmbeddedId
   private CampaignInfluencerOfferId campaignInfluencerOfferType;
   private Double paymentOffer;
   private Double commissionValue;
   private String commissionValueType;
   private String commissionAffiliateType;
   private String commissionAffiliateCustomName;
   private String commissionAffiliateDetails;
   private Long commissionValueTypeId;
private Long commissionAffiliateTypeId;
}
