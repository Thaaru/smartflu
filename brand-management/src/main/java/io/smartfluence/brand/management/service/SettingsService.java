package io.smartfluence.brand.management.service;

import org.springframework.http.ResponseEntity;

import io.smartfluence.modal.validation.ChangePasswordModal;
import io.smartfluence.modal.validation.brand.settings.PaymentModal;
import io.smartfluence.modal.validation.brand.settings.ProfileModel;
import io.smartfluence.modal.validation.brand.settings.SocialConnectModel;

public interface SettingsService {

	ResponseEntity<Object> getBrandSocialConnect();

	ResponseEntity<Object> updateBrandSocialConnect(SocialConnectModel socialConnectModel);

	ResponseEntity<Object> getBrandPaymentPreference();

	ResponseEntity<Object> updateBrandPaymentPreference(PaymentModal paymentModel);

	ResponseEntity<Object> getProfileDetails();

	ResponseEntity<Object> updateProfileDetails(ProfileModel profileModel);

	ResponseEntity<Object> changePassword(ChangePasswordModal changePasswordModal);

	ResponseEntity<String> getTokenURL(String userEmail);

	ResponseEntity<String> getAccessToken(String code);

	ResponseEntity<Object> getEmailConnectionSettings();

	ResponseEntity<String> removeAccount();
}