package io.smartfluence.influencer.entities;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "commission_affiliate_type_master")
public class CommissionAffiliateTypeMasterEntity {
    @Id
    private Long commAffiTypeId;
    private String commAffiName;
}
