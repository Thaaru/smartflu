import React from "react";
import { ProgressBar } from "react-bootstrap";
import progressStatus from "./progressbar_status";

export  const CProgressBar = ({data}) => {
    return <div className="px-4 mt-4" style={{ "textAlign": "left" }}>
    <ProgressBar variant="primary" style={{ "height": "10px" }} isloading={false}now={progressStatus(data)}/>
    <p className="status-text mt-1">{progressStatus(data)}% Completed</p>
</div>
}